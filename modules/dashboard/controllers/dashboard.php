<?php defined('SYSPATH') or die('No direct script access');

//Almost all functions will take as parameter, the id of the agency we are dealing with
//in order not to interfere with another's stuff.

class Dashboard_Controller extends Admin_Controller {

	//public $template = 'template/admin_template';

	public function __construct()
	{	
		parent::__construct();
		$this->admin = Authlite::instance()->get_admin();
		$this->session = Session::instance();
		//if (!empty($this->admin)) $this->key = $this->admin->agency_id;
		
	}  

	
	public function index()
	{	
		//ensure a logged in user can't land on the signup page
		if (empty($this->admin)) 
		{
			url::redirect('admin/login');
		}
		$this->dashboard();
	}
	
	public function dashboard()
	{
		Authlite::check_admin();
		//Authlite::verify_referer();
	
	
		$today = date('Y-m-d');
		$agency_id = $this->agency_id;
		$this->template->title = Kohana::lang('backend.all_buses');;
		$parent_id = get::_parent($this->agency_id)->id;	



		$total_buses_count = Bus_Model::count_all($agency_id);
		$available_buses_count = Bus_Model::count_available($agency_id);
		$unavailable_buses_count = Bus_Model::count_unavailable($agency_id);
		$rentals_today_count = Income_Model::count_bus_rentals_today($agency_id);
		$all_buses = Bus_Model::get_all($agency_id);
		
		$current_schedules_count = Schedule_Model::count_current_schedules($agency_id);
		$checked_out_today_count = Schedule_Model::count_checked_out_today($agency_id);
		
		$parcels_unsent_today_count = Parcel_Model::count_unsent_today($agency_id);
		$parcels_sent_today_count = Parcel_Model::count_sent_today($agency_id);
		$parcels_total_outgoing_today_count = Parcel_Model::count_total_outgoing_today($agency_id);
		$parcels_total_incoming_today_count = Incoming_parcel_Model::count_total_incoming_today($agency_id);
		
		$free_tickets_issued_today_count = Ticket_detail_Model::count_free_issued_today($agency_id);
		//var_dump($free_tickets_issued_today_count);exit;
		$tickets_total_issued_today_count = Ticket_detail_Model::count_total_issued_today($agency_id);
		
		$total_parcel_income = Parcel_Model::get_parcel_income_by_date($agency_id,$today);
		$total_schedule_income = Schedule_Model::get_departed_schedule_income_by_date($agency_id,$today);
		
		//$total_schedule_income = Schedule_Model::get_schedule_income_by_date($agency_id,$today);
		
		$expired_documents = Bus_requirements_status_Model::get_all_expired_requirements($agency_id);
		$no_of_expired_documents = Bus_requirements_status_Model::count_all_expired_requirements($agency_id);
		$no_of_expiring_documents = Bus_requirements_status_Model::count_agency_expiring_requirements($agency_id);
		
		$bus_rentals_today = Income_Model::get_income_from_rentals_today($agency_id);
		$other_income_total = Income_Model::get_other_income_excluding_rentals_by_date_total($agency_id,$today);
		$total_rental_income = 0;
		foreach($bus_rentals_today as $rental){
			$total_rental_income += $rental->amount;
		}
		
		//count number of each type of bus
		$seats_and_buses = array();
		foreach($all_buses as $bus){
			//create multi array with number of seats as key and bus numbers as value. later, count
			$seats_and_buses[$bus->bus_seats][]=$bus->bus_number;
		}
		
		$seats_and_number_of_buses=array();
		foreach($seats_and_buses as $bus_seats=>$bus_list){
			$seats_and_number_of_buses[$bus_seats] = count($bus_list);
		}
		
		
		//Get total for each expense category
		//create an array to hold the various expense categories totals
		$all_expense_categories = Expense_category_Model::get_all($this->agency_id);
		$all_expense_categories_array = array();
		
		//Initialize an array with all active expense categories and set them to zero;
		//Later for each category, just update total amount.
		
		foreach($all_expense_categories as $category){
			$all_expense_categories_array[$category->name]=0;
		}
			
		$expenses_today = Expense_Model::get_expenses_by_date($agency_id,$today);
		foreach ($expenses_today as $exp)
		{
			$all_expense_categories_array[$exp->name] += $exp->amount;
		}
	
		$expenses_total = 0;
		foreach($all_expense_categories_array as $cat_name=>$cat_total){
			$expenses_total += $cat_total;
		}
	
	
	
		//build chart data
		$seven_days_back = strtotime("-1 week");
		$seven_days_back =date('Y-m-d',$seven_days_back);
		$current_time = time();
		//$chart_filename = "dashboard_chart". $current_time.".txt";
		$chart_filename = "dashboard_chart.txt";
		get::build_line_chart_data($agency_id,$seven_days_back,$today,$chart_filename);
		
	
	
		
		//var_dump($no_of_expiring_documents);exit;
	
		
		$view = new View('dashboard');
		$view->chart_filename = $chart_filename;
		
		$view->expired_documents = $expired_documents;
		$view->no_of_expired_documents = $no_of_expired_documents;
		$view->no_of_expiring_documents = $no_of_expiring_documents;
		
		$view->rentals_today_count = $rentals_today_count;
		$view->available_buses_count = $available_buses_count;
		$view->unavailable_buses_count = $unavailable_buses_count;
		$view->total_buses_count = $total_buses_count;
		$view->seats_and_buses = $seats_and_buses;
		$view->seats_and_number_of_buses = $seats_and_number_of_buses;
		
		$view->current_schedules_count = $current_schedules_count;
		$view->checked_out_today_count = $checked_out_today_count;
		$view->total_schedule_income_today = $total_schedule_income;
		$view->other_income_total = $other_income_total;
		
		$view->parcels_unsent_today_count = $parcels_unsent_today_count;
		$view->parcels_sent_today_count = $parcels_sent_today_count;
		$view->parcels_total_outgoing_today_count = $parcels_total_outgoing_today_count;
		$view->parcels_total_incoming_today_count = $parcels_total_incoming_today_count;
		$view->income_from_parcels_registered_today = $total_parcel_income;
		
		$view->free_tickets_issued_today_count = $free_tickets_issued_today_count;
		$view->tickets_total_issued_today_count = $tickets_total_issued_today_count;
		$view->income_from_bus_rentals_today = $total_rental_income;
		$view->expense_category_totals = $all_expense_categories_array;
		$view->expenses_total = $expenses_total;
		$view->total_income_today = get::agency_overall_income_by_date($agency_id,$today);
		
		
		
		
		$this->template->content = $view;
	}
			
		
		}