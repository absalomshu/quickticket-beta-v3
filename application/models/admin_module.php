<?php defined('SYSPATH') or die('No direct script access');

class Admin_Module_Model extends ORM{


	
	public function get_allowed_main_module_itemns($admin_name)
	{
		$delete=ORM::factory('admin_module')->where('AdminID',$admin_name)->find();
		$delete->Deleted = 1;
		$delete->DeletedBy = strtoupper($deleted_by);
		$delete->save();
		
		$notice="Admin deleted";
		return $notice;	
	}

	public function update_admin_module($admin_name, $module_id, $value)
	{	
		$admin_module = ORM::FACTORY('admin_module')->where('MainModuleID',$module_id)->where('AdminID',$admin_name)->find();
		$admin_module->IsAllowed = $value;
		$admin_module->save();
	}
	
	public function update_admin_module_parcel_types($admin_name, $manage_mails, $manage_baggage)
	{	
		$admin_module = ORM::FACTORY('admin_module')->where('AdminID',$admin_name)->where('MainModuleID','4')->find();
		$admin_module->ManageMails = $manage_mails;
		$admin_module->ManageBaggage = $manage_baggage;
		$admin_module->save();
	}	
	
	public function add_admin_modules($admin_name, $module_id, $value)
	{	
		$admin_module = ORM::FACTORY('admin_module');
		$admin_module->AdminID = $admin_name;
		$admin_module->MainModuleID = $module_id;
		$admin_module->IsAllowed = $value;
		$admin_module->save();
	}
	
	public function admin_is_allowed_main_module($admin_name, $main_module_id)
	{
		$admin_module=ORM::factory('admin_module')->where('AdminID',$admin_name)->where('MainModuleID',$main_module_id)->orderby('MainModuleID','asc')->find();
		return $admin_module->IsAllowed;	
	}
	
	public function admin_is_allowed_manage_mails($admin_name)
	{
		$admin_module=ORM::factory('admin_module')->where('AdminID',$admin_name)->where('MainModuleID','4')->find();
		return $admin_module->ManageMails;	
	}
	
	public function admin_is_allowed_manage_baggage($admin_name)
	{
		$admin_module=ORM::factory('admin_module')->where('AdminID',$admin_name)->where('MainModuleID','4')->find();
		return $admin_module->ManageBaggage;	
	}
}
