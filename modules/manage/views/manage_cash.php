<?php defined('SYSPATH') or die('No direct script access'); ?> 
	

 
	<div class="row-fluid ">
	  	<div class="span12 hero-unit prof-unit" >
		
		<ul class="nav nav-tabs" id="myTab">
					
					<li class="active" ><a href="#cash-to-bank"><i class="icon-align-justify"></i> <?=Kohana::lang('backend.cash_to_bank')?></a></li>
		</ul>
		
		<div class="tab-content">
			 
			
			
			
			
			
			
			<div class="tab-pane active" id="cash-to-bank">
				<div class="span8 offset2">
				
					<legend><?=Kohana::lang('backend.cash_to_bank')?></legend>						
						<table class="table table-striped table-hover table-condensed">
					<tr>
						
						<th><?=Kohana::lang('backend.date')?></th>
						<th><?=Kohana::lang('backend.branch')?></th>
						<th><?=Kohana::lang('backend.bank_name')?></th>
						<th><?=Kohana::lang('backend.account_number')?></th>
						<th><?=Kohana::lang('backend.amount')?></th>
						<th><?=Kohana::lang('backend.done_by')?></th>
												
					</tr>
					<?php foreach ($ctb as $trx):?>
						<tr>	
							<td><?=date("d-m-Y",strtotime($trx->date_incurred))?></td>
							<td><?=get::agency_name($trx->agency_id)?></td>
							<td><?=$trx->bank_name?></td>
							<td><?=$trx->account_number?></td>
							<td><?=number_format($trx->amount)?></td>
							<td><?=$trx->created_by?></td>
							
						</tr>
						<?php endforeach; ?>
				</table>	
			<?php echo $this->pagination;?>	
						
						
						
						
						
						
				</div>	
					
			</div>
					
		</div>
		
			
		
			  
		</div>
		
		</div>
	  <div class="clear"></div>

<script type="text/javascript">
	$(document).ready(function(){
			$('#timepicker').timepicker({
				minuteStep:30,
				//defaultTime: '8:00 PM'
			});
			
			$('.input-append.date').datepicker({
				format: "dd-mm-yyyy",
				todayBtn: "linked",
				autoclose: true,
				todayHighlight: true
			});
			
			
				$(function () {
				$('#myTab a').click(function (e) {
					  e.preventDefault();
					  $(this).tab('show');  
			})
			  })
			
	})
	</script>