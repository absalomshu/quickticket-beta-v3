<?php  defined('SYSPATH') or die('No direct script access');

//Language
$lang=array(
// Home page
	'adm_backend'	       => 'Adminstrative Backend',
	'logout'                 => 'Logout',
	'online_booking'            => 'Bus ticket booking and reservations',
	'follow'           => 'Follow us',
	'about'          => 'About us',
	'terms'        => 'Terms',
	'help'        => 'Help',
	'privacy'        => 'Privacy policy',
	'all_rights'        => 'All rights reserved',
	'home'        => 'Home',
	'past_schedule'        => 'Past schedule',
	'search_results'        => 'Search results',
	'admin_home'        => 'Admin\s Home',
	'man_home'        => 'Manager\'s Home',
	'view_sched'        => 'View schedule',
	'expenses'        => 'Expenses',
	'schedules'        => 'Schedules',
	'parcels'        => 'Parcels',
	'buses'        => 'Buses',
	'current'        => 'Current',
	'departed'        => 'Past',
	'bus_no'        => 'Bus number',
	'from'        => 'From',
	'To'        => 'To',
	'to'        => 'to',
	'departure_time'        => 'Departure time',
	'departed_at'        => 'Departed at',
	'sched_for'        => 'Schedule for',
	'start_loading'        => 'Start loading',
	'loading'        => 'Loading',
	'edit'        => 'Edit',
	'from'        => 'From',
	'to'        => 'to',
	'time'        => 'Time',
	'bus_type'        => 'Bus type',
	'seater'        => 'Seater',
	'ticket_price'        => 'Ticket price',
	'done'        => 'Done',
	'checkout'        => 'Checkout',
	'seat'        => 'Seat ',
	'seats'        => 'Seats',
	'requests'        => 'Online reservation requests',
	'checkout_seat'        => 'Check out a seat',
	'client_name'        => 'Passenger\'s name',
	'client_id'        => 'ID card n&#176;',
	'client_phone'        => 'Passenger\'s phone number',
	'client_email'        => 'Passenger\'s email',
	'spec_drop'        => 'Special drop',
	'drop_price'        => 'Drop price',
	'reserve_seat'        => 'Reserve a seat',
	'email'        => 'Email',
	'id_card'        => 'ID card no',
	'seats_av'        => 'Seats (Available)',
	'reserve'        => 'reserve',
	'res_requests'        => 'Reservation requests',
	'state'        => 'State',
	'reserved'        => 'Reserved',
	'incoming'        => 'Incoming',
	'sent'        => 'Sent',
	'parcel_details'        => 'Details for parcel',
	'sent_from'        => 'Sent from',
	'sent_on'        => 'Sent on',
	'for'        => 'For',
	'coll_name'        => 'Collector\'s name',
	'coll_id'        => 'Collector\'s ID card No.',
	'coll_phone'        => 'Collector\'s phone No.',
	'deliver'        => 'Deliver',
	'collected_on'        => 'Collected on',
	'edit_sched_for'        => 'Edit schedule for',
	'next'        => 'Next',
	'back_expenses'        => 'Back to expenses',
	'details_expense'        => 'Details for expense',
	'amount'        => 'Amount',
	'purpose'        => 'Purpose',
	'authorised_by'        => 'Authorised by',
	'add_expense'        => 'Add expense',
	'exp_list'        => 'Expenses list',
	'check_exp'        => 'Check expenses',
	'reg_at'        => 'Registered at',
	'which_bus_exp'        => 'Check expenses',
	'select_bus'        => 'Search saved buses',
	'enter_bus_search'        => 'Enter a bus number to search',
	'search_results'        => 'Search results',
	'exp_for'        => 'Expenses for',
	'welcome_admin'        => 'Welcome administrator. Log in.',
	'qt_login'        => 'QuickTicket ',
	'create_schedule'        => 'Create a new schedule',
	'existing_buses'        => 'Buses',
	'create'        => 'Create',
	'unreg_bus'        => 'Unregistered bus',
	'recently_checkedout'        => 'Recently checked out',
	'incoming_parcels'        => 'Incoming parcels',
	'see_parcels'        => 'See all parcels',
	'for'        => 'For',
	'sent_on'        => 'Sent on',
	'reg_parcels'        => 'Register parcels',
	'rec_details'        => 'Enter receiver\'s details',
	'price'        => 'Price',
	'register'        => 'Register',
	'incoming'        => 'Incoming',
	'owner'        => 'Owner',
	'status'        => 'Status',
	'receive'        => 'Receive',
	'received'        => 'Received',
	'deliver'        => 'Deliver',
	'delivered'        => 'Delivered',
	'sent'        => 'Sent',
	'rec_del'        => 'Received & delivered',
	'checkedout_at'        => 'Checked out',
	'statistics'        => 'Statistics',
	'total_filled_seats'        => 'Total filled seats',
	'regular_seats'        => 'Regular seats',
	'spec_drops'        => 'Special drops',
	'empty_seats'        => 'Empty seats',
	'amt_spec_drops'        => 'Am\'t from special drops',
	'price_per_ticket'        => 'Price per ticket',
	'parcel_deliveries'        => 'Parcels delivered',
	'no_parcel_deliveries'        => 'No parcel deliveries',
	'worth'        => 'worth',
	'pending_for'        => 'Pending (current) schedules for ',
	'checkedout_for'        => 'Checked out schedules for',
	'summary_for'        => 'Summary for',
	'from_departed'        => 'From departed buses ONLY',
	'number_of'        => 'Number of',
	'total_ticket_sales'        => 'Total from ticket sales',
	'total_from_parcels'        => 'Total from parcels',
	'schedule_for'        => 'Schedule for',
	'taken_seats'        => 'Already taken seats',
	'occupant'        => 'Passenger\'s name',
	'bus_plan'        => 'Bus plan',
	'reserve_seat'        => 'Reserve a seat',
	'branch_activity_date'        => 'Branches\' activity by date',
	'current_schedules'        => 'Current schedules',
	'all_profiles'        => 'All company profiles',
	'name'        => 'Name',
	'phone'        => 'Phone',
	'search'        => 'Search',
	'add_bus'        => 'Add bus',
	'all_buses'        => 'All buses',
	'register'        => 'Register',
	'drivers_name'        => 'Driver\'s name',
	'number_seats'        => 'Number of seats',
	'driver'        => 'Driver',
	'edit_bus'        => 'Edit bus',
	'save'        => 'Save',
	'cancel'        => 'Cancel',
	'current_scheds_from'        => 'Current schedules: From',
	'see_all'        => 'See all',
	'login'        => 'Login',
	'incorrect_credentials'        => 'Incorrect username and/or password.',
	'username_taken'        => 'Username taken. Try another.',
	'same_origin_destination'        => 'The origin and destination cannot be the same',
	'exists_in_list'        => "already exists, please select it fromt the list.",
	'new_bus'        => 'New bus ',
	'is_registered'        => ' has been registered',
	'complete_schedule'        => 'Complete schedule',
	'already_checkedout'        => 'This schedule has already been checked out, hence cannot be edited further.',
	'sorry_seat'        => "Sorry, seat",
	'given_out'        =>  "has been given out. Please select another",
	'seat_reserved'        => 'This seat has been reserved',
	'res_confirmed'        => "Your reservation with ",
	'is_confirmed'        => " is confirmed.",
	'hello'        => 'Hello ',
	'client_confirmed'        => "The passenger has been confirmed.",
	'deleted_res'        => "The passenger has been deleted from the reservation list.",
	'client_deleted'        => "The passenger has been deleted.",
	'now_empty'        => " is now empty.",
	'error_reserved'        => 'This bus still has reserved seats. You cannot check out a bus with reserved seats. Please resolve this and try again. ',
	'err_empty_bus'        => 'You can\'t check out an empty bus',
	'bus_departed'        => 'Success. This bus has been confirmed as departed. ',
	'display_past'        =>  'Display of a past schedule',
	'welcome_backend'        => 'Welcome to your backend',
	'edit_company'        => 'Edit company details',
	'bus_exists'        => 'This bus already exists.',
	'bus_registered'        => 'New bus registered',
	'bus_modified'        => 'Bus information modified',
	'deleted'        => 'deleted',
	'past_sched_display'        => 'Display of a past schedule',
	'register_expense'        => 'Register an expense',
	'expense_added'        => 'Expense has been added',
	'check_expense'        => 'Check expense for a bus',
	'no_expense'        => 'No expense records match your search ',
	'parcel_deposited'        => "A parcel has been deposited for you at ",
	'inform_you'        => "You will be informed when it's available in your town.",
	'parcel_registered'        => "The parcel has been registered",
	'all_parcels'        => "See all parcels",
	'parcel_available'        => "Your parcel is now available at ",
	'collect_it'        => "Please, pass by to collect it.",
	'parcel_received'        => "The parcel has been received.",
	'marked_delivered'        => "The parcel has been marked as 'delivered'.",
	'no_reservation'        => "No passenger reservation request for the moment.",
	'delete'		 => 'Delete',
	'confirm_checkout'       	 => 'Are you sure you want to check this bus out?',
	'reject_reserve'        => 'Do you want to reject this reservation',
	'confirm_emptyseat'        => 'Do you want to empty this seat',
	'seat_checked_out'        => 'checked out',
	'bus_checked_out'        => 'checked out',
	'door'        => 'Door',
	'change_lang'        => 'Français',
	'username'        => 'Username',
	'password'        => 'Password',
	'add_parcel'        => 'Add parcel',
	'refuse_reserve'        => 'Are you sure you want to cancel this reservation?',
	'rec_name'        => 'Receiver\'s name',
	'rec_phone'        => 'Receiver\'s phone',
	'desc_parcel'        => 'Describe the parcel',
	'spent_on'        => 'What was this spent on?',
	'settings'        => 'Settings',
	'auto_generate_ticket'        => 'Generate tickets automatically',
	'tickets'        => 'Tickets',
	'print'        => 'Print',
	'preferences_saved'        => 'Your preferences have been saved',
	'free_tickets'        => 'Free tickets',
	'search_by_date'        => 'Search by date',
	'total'        => 'Total',
	'exp_on'        => 'All expenses on ',
	'registered_by'        => 'Registered by',
	'created_by'        => 'Created by',
	'vip_bus'        => 'VIP Bus',
	'sms_sent_today'        => 'SMS sent today',
	'total_sms_sent'        => 'Total SMS sent',
	'sms'        => 'SMS',
	'back_parcels'        => 'Back to parcels',
	'coll_by'        => 'Collected by',
	'at'        => 'at',
	'refresh_stats'        => 'Refresh statistics',
	'sms_management'        => 'SMS management',
	'departed_buses'        => 'Buses checked out',
	'total_expenditure'        => 'Total expenditure',
	'idc'        => 'IDC',
	'activity_by_period'        => 'Activity',
	'summary_from'        => 'Summary from ',
	'expenses_sched'        => 'Expenses for this schedule ',
	'no_current'        => 'No current schedule ',
	'no_departed'        => 'No past schedule ',
	'check'        => 'Check',
	'on'        => 'On',
	'net_income'        => 'Net income',
	'additional_infos'        => 'Additional ticket information',
	'background_image'        => 'Ticket background image',
	'company_infos'        => 'Company information',
	'users'        => 'Users',
	'general'        => 'General',
	'add_admin'        => 'Add admin',
	'back'        => '<- Back',
	'free_ticket'        => 'Free ticket',
	'motorboys_name'        => 'Motor boy',
	'other_information'        => 'Other information',
	'bus_name'        => 'Bus name',
	'back_buses'        => 'Back to buses',
	'confirm_start_loading'        => 'SMS will be sent to notify passengers.',
	'serial_no'        => 'Serial number',
	'AllowScheduleManagement'        => 'Permission to manage schedules',
	'AllowParcelManagement'        => 'Permission to manage parcels',
	'AllowBusManagement'        => 'Permission to manage buses',
	'AllowExpenseManagement'        => 'Permission to manage expenses',
	'To'        => 'To',
	'register_expense'        => 'Register an expense',
	'bus'        => 'Bus ',
	'delete_schedule'        => 'Delete this schedule? All related expenses will be deleted. Tickets will be moved to tickets pending.',
	'ticket_type_1'=>'Ticket type: 1 (Wide/Horizontal)',
	'ticket_type_2'=>'Ticket type: 2 (Oblong/Vertical)',
	'percentage'        => 'Percentage',
	'advanced'        => 'Advanced',
	'fixed_amount'        => 'Fixed amount',
	'fixed_expenses'        => 'Fixed expenses',
	'add'        => 'Add',
	'bus_requirements'        => 'Bus requirements',
	'maintain'        => 'Maintenance',
	'maintenance'        => 'Maintenance',
	'reminders'        => 'Reminders',
	'add_reminder'        => 'Add reminder',
	'due_date'        => 'Due date',
	'start_reminder'        => 'Start reminder',
	'only_me'        => 'Visible to me only',
	'all_reminders'        => 'All reminders',
	'reminder_type'        => 'Reminder type',
	'reminder_details'        => 'Reminder details',
	'reminder_schedule'        => 'Reminder schedule',
	'back_reminders'        => 'Back to reminders',
	'view'        => 'View',
	'schedule'        => 'Schedule',
	'check_parcels'        => 'Check parcels',
	'parcels_for'        => 'Parcels summary for',
	'select_date'        => 'By date',
	'all'        => 'All',
	'admin_activity'        => 'Administrator\'s activity',
	'all_activity'        => 'All activity',
	'reports'        => 'Reports',
	'admins'        => 'Administrators',
	'admin'        => 'Administrator',
	'back_reports'        => 'Back to reports',
	'ticket_sales'        => 'Ticket sales',
	'clients_who_missed_schedule'        => 'Passengers who missed schedule',
	'tickets_pending'        => 'Tickets pending',
	'dates'        => 'Dates',
	'task'        => 'Task',
	'view_fixed_expense'        => 'View fixed expense',
	'back_settings'        => 'Back to settings',
	'edit_admin'        => 'Edit administrator',
	'change_password'        => 'Edit administrator',
	'module_access'        => 'Module access rights',
	'dashboard_items'        => 'Dashboard items',
	'new_password'        => 'New password',
	'day'        => 'Day',
	'month'        => 'Month',
	'one_time'        => 'One time',
	'recurring'        => 'Recurring',
	'start_on'        => 'Starting  on',
	'remind_on'        => 'Remind on',
	'remind_every'        => 'Remind every',
	'none'        => 'None',
	'checked_out'        => 'Checked out',
	'schedule_date'        => 'Schedule date',
	'ticket_will_be'        => 'Ticket will be added as free ticket. Select schedule and seat number',
	'select'        => 'Select',
	'add_to_schedule'        => 'Add to schedule',
	'confirm_free_ticket'        => 'Confirm free ticket',
	'confirm_delete'        => 'Are you sure you want to delete this item',
	'add_fixed_expense'        => 'Add fixed expense',
	'mark_absent'        => 'Mark absent? Seat will be emptied and ticket moved to tickets pending',
	'total_income'        => 'Total income',
	'total_expenses'        => 'Total expenses',
	'checked_out_by'        => 'Checked out by',
	'start_date'        => 'Start date',
	'end_date'        => 'End date',
	'rentals'        => 'Rentals',
	'add_bus_rental'        => 'Add bus rental',
	'online_settings'        => 'Online settings',
	'ticket_name'        => 'Ticket name',
	'online'        => 'Online',
	'online_purchases'        => 'Online purchases',
	'purchases_online'        => 'Online purchases',
	'make_available'        => 'Make available',
	'make_unavailable'        => 'Make unavailable',
	'description'        => 'Description',
	'rented_to'        => 'Rented to',
	'other_income'        => 'Other income',
	'bus_rental'        => 'Bus rental',
	'saved'        => 'Saved',
	'timeline'        => 'Timeline view',
	'setting_saved'        => 'Setting saved',
	'confirm_delete_user'        => 'Confirm delete of this administrator',
	'schedule_id'        => 'Schedule ID',
	'check_schedules'        => 'Check schedules',
	'outgoing'        => 'Outgoing',
	'schedule_or_bus'        => 'Schedule/Bus',
	'sender'        => 'Sender',
	'sender_name'        => 'Sender\'s name',
	'receiver'        => 'Receiver',
	'bus_activity'        => 'Bus report',
	'output_format'        => 'Output format',
	'parcel_id'        => 'Parcel n&#176;',
	'mail'        => 'Mail',
	'baggage'        => 'Baggage',
	'all_parcels_on_this_schedule'        => 'Parcels on this schedule',
	'collection'        => 'Collection',
	'date'        => 'Date',
	'collection_amount_mismatch'        => 'Amount collected cannot be more than available balance',
	'todays_net_income'        => 'Today\'s net income',
	'collections_for'        => 'Collections for',
	'recent_collections'        => 'Recent collections',
	'admin_collections'        => 'Collections',
	'client_destination'        => 'Passenger destination',
	'details'        => 'Details',
	'remote_tickets'        => 'Remote tickets',
	'purchase_date'        => 'Date',
	'schedule_requested'        => 'Schedule requested',
	'passenger_name'        => 'Passenger\'s name',
	'cash'        => 'Cash',
	'back_schedules'        => 'Back to schedules',
	'remote_ticket_saved'        => 'Remote ticket saved',
	'remote_tickets_desc'        => 'Remote tickets: Tickets paid for at one branch to be used at another branch',
	'add_cash_to_bank'        => 'Add cash-to-bank transfer',
	'cash_to_bank'        => 'Cash-to-bank',
	'bank_name'        => 'Bank name',
	'account_number'        => 'Account number',
	'done_by'        => 'Done by',
	'back_cash'        => 'Back to cash',
	'scheduleID'        => 'ScheduleID',
	'original_schedule'        => 'Original schedule',
	'branch'        => 'Branch',
	'sent_to'        => 'Sent to',
	'amount_paid'        => 'Amount paid',
	'back_online_purchases'        => 'Back to online purchases',
	'back_remote_tickets'        => 'Back to remote tickets',
	'modified_by'        => 'Modified by',
	'welcome_user'        => 'Enter your email and password ',
	'sign_in'        => 'Sign in',
	'status'        => 'Status',
	'available'        => 'Available',
	'unavailable'        => 'Unavailable',
	'no_incoming_parcel'        => 'No incoming parcel',
	'no_sent_parcel'        => 'No sent parcel',
	'mark_sent'        => 'Mark as sent',
	'departure'        => 'Departure',
	'online_purchase_history'        => 'Online purchase history',
	'bus_number'        => 'Bus number',
	'Home'        => 'Home',
	'Schedules'        => 'Schedules',
	'Expenses'        => 'Expenses',
	'Parcels'        => 'Parcels',
	'Buses'        => 'Buses',
	'Reminders'        => 'Reminders',
	'Reports'        => 'Reports',
	'Cash'        => 'Cash',
	'Settings'        => 'Settings',
	'create_schedules'        => 'Create schedules',
	'past_schedules'        => 'Past schedules',
	'schedules_for_town'        => 'Schedules for particular town',
	'all_current_schedules'        => 'All current schedules',
	'send_sms_passengers'        => 'Send SMS to passengers',
	'yes'        => 'Yes',
	'no'        => 'No',
	'boarding'        => 'Boarding',
	'delayed'        => 'Delayed',
	'print_ticket'        => 'Print ticket',
	'mark_present'        => 'Mark present',
	'mark_absent'        => 'Mark absent',
	'empty_seat'        => 'Delete ticket',
	'confirm'        => 'Confirm',
	'start_boarding'        => 'Start boarding',
	'reason'        => 'Reason',
	'free_ticket'        => 'Free',
	'f_for_free'        => 'F',
	'welcome_backend'        => 'Welcome to your backend',
	'manage_tickets'        => 'Manage tickets',
	'online_purchase_history'        => 'Online purchase history',
	'check_out_schedule'        => 'Check out schedule',
	'start_boarding'        => 'Start boarding schedule',
	'bus_delayed'        => 'Bus marked as delayed',
	'viewing_all_schedules'        => 'Viewing all schedules',
	'delete_admin'        => 'Delete admin',
	'details_current_schedule'        => 'Details for current schedule',
	'details_past_schedule'        => 'Details for past schedule',
	'schedule_current_or_unfound'        => 'Schedule is current or not found',
	'everything_pushed_online'        => 'Everything pushed online',
	'parcels_downloaded'        => 'parcels downloaded. Parcels up-to-date',
	'purchases_downloaded'        => 'purchases downloaded. Purchases up-to-date',
	'remote_tickets_downloaded'        => 'remote tickets downloaded. Remote tickets up-to-date',
	'remote_tickets_online'        => 'Remote tickets online',
	'cash_to_bank_online'        => 'All cash-to-bank transactions online',
	'add_cash_to_bank'        => 'Add cash-to-bank',
	'free'        => 'Free',
	'balance'        => 'Balance',
	'available_balance'        => 'Available balance',
	'previous_balance'        => 'Previous balance',
	'amount_collected'        => 'Amount collected',
	'collected_by'        => 'Collected by',
	'administrator'        => 'Administrator',
	'anytime'        => 'Anytime',
	'credit_card'        => 'Credit card',
	'bank_account'        => 'Bank account',
	'agency'        => 'Travel agency',
	'edit_schedule'        => 'Edit schedule',
	'client_marked_absent'        => 'Passenger marked absent. View in \'Schedules->Tickets pending\'',
	'seat_no' =>'Seat number',
	'reject_reservation' =>'Do you want to reject this reservation?',
	'confirm_reservation' =>'Do you want to confirm this reservation?',
	'reservations_downloaded'        => 'reservation(s) downloaded',
	'login_with_unics'        => 'Login with UNICS',
	'cash_interbranch'        => 'Cash interbranch',
	'send'        => 'Send',
	'other'        => 'Other',
	'date_incurred'        => 'Date incurred',
	'send_cash_interbranch'        => 'Send cash interbranch',
	'add_other_income'        => 'Add other income',
	'outgoing_parcels'        => 'Outgoing parcels',
	'add_collection'        => 'Add collection',
	'back_other_income'        => 'Back to other income',
	'collection_saved'        => 'Collection saved',
	'balances'        => 'Balances',
	'income_online'        => 'All income online',
	'save_changes'        => 'Save changes',
	'modified_by'        => 'Modified by',
	'modify'        => 'Modify',
	'verified_by'        => 'Verified by',
	'disburse_net_income'        => 'Disburse net income',
	'auto_disburse_cash'        => 'Auto-disburse cash',
	'confirm_auto_disburse'        => 'Do you confirm that ',
	'confirm_auto_disburse_middle'        => ' FCFA has been sent to ',
	'confirm_auto_disburse_ending'        => '? NB: This is not reversible.',
	'auto_disburse_cash_explanation'        => 'After checkout of a schedule, the admin can instantly disburse the net income to the selected branch.',
	'select_valid_bus'        => 'Schedule cannot be checked out. Select a valid bus.',
	'edit_passenger'        => 'Edit passenger details',
	'passenger_edited'        => 'Passenger edited',
	'send_sms'        => 'Send SMS',
	'favourite'        => 'Favourite',
	'added_to_favourites'        => 'Passenger contact details saved.',
	'add_to_favourites'        => 'Add contact to favourites? This will save the passengers contact details.',
	'contact_exists'        => 'Contact already exists in favourites list.',
	'send_sms_to_passengers'        => 'Send SMS to passengers of ',
	'all_parcels_via__bus'        => 'All parcels sent via bus number  ',
	'by_bus_number'        => 'By bus number  ',
	'receiver_phone'        => 'Receiver phone  ',
	'parcel_modified'        => 'Parcel modified',
	'add_parcel_to_schedule'        => 'When the schedule is checked out, the parcel will be marked as \'sent\'  ',
	'passengers'        => 'Passengers',
	'check_buses'        => 'Check buses',
	'passenger_activity'        => 'Passenger activity',
	'search_name'        => 'Enter a name to search',
	'in'        => 'In',
	'print_statistics'        => 'Print statistics?',
	'edit_expense'        => 'Edit expense',
	'print_with_statistics'        => 'Do you want to include statistics?',
	'expense_modified'        => 'Expense modified',
	'travel_history'        => 'Travel history for',
	'expense_categories'        => 'Expense categories',
	'category'        => 'Category',
	'category_name'        => 'Category name',
	'add_expense_category'        => 'Add expense category',
	'category_exists'        => 'A category with the name already exists. Please change the name and try again.',
	'default_no_delete'        => 'Default category cannot be deleted.',
	'income_and_expenses'        => 'Income & expenses',
	'income_and_expenses_report'        => 'Income & expenses report',
	'route_activity'        => 'Route report',
	'town_from'        => 'Town from',
	'town_to'        => 'Town to',
	'routes'        => 'Routes',
	'income'        => 'Income',
	'net'        => 'Net',
	'destination'        => 'Destination',
	'documents'        => 'Documents',
	'availability'        => 'Availability',
	'serial'        => 'Serial',
	'by_client_name'        => 'By client name',
	'by_current_schedule'        => 'By current schedule',
	'by_schedule_id'        => 'By schedule ID',
	'back_routes'        => 'Back to route report',
	'checked_out_at'        => 'Checked out at',
	'agency_from'        => 'Agency from',
	'company'        => 'Company',
	'incoming_schedules_downloaded'        => 'incoming schedules updated',
	'sched_incoming_today'        => 'Schedules incoming today',
	'administrators'        => 'Administrators',
	'edit_expense_category'        => 'Edit expense category',

);
