<?php defined('SYSPATH') or die('No direct script access'); ?>
	
		
		
		<div class="row-fluid  span12 hero-unit prof-unit reload" style="margin:5px 0 0px 0;">	
		
			
			
			<div class="no-print">
			
			<ul class="nav nav-tabs">
					<li > <a href="<?=url::site('buses/check_buses')?>"><i class="icon-arrow-left"></i> <?=Kohana::lang('backend.back_buses')?></a></li>
					<button class="view" onClick="window.print()"><i class="icon-print"></i> <?=Kohana::lang('backend.print')?></button>
			</ul>
			</div>
			<div class="  print-area" >
								
								
				<legend>
				<div class="print-head">
						<?php $admin=Authlite::instance()->get_admin();?>
						<?=strtoupper(get::_parent($admin->agency_id)->name);?><br/>
						<?=get::agency_name($admin->agency_id);?>
					</div>
				Report: All buses - Income and expenses ranking	<br/>
					Start date: <?=date("d-m-Y",strtotime($start_date))?> &nbsp;&nbsp;&nbsp;&nbsp; 
					End date: <?=date("d-m-Y",strtotime($end_date))?>&nbsp;&nbsp;&nbsp;&nbsp;
				</legend>
				<div class="span4" style="margin:5px 0 0 0;">
				<div class="heading">Expenses [High to low]</div>
					<table class="table table-striped table-condensed">
						<tr><th><?=Kohana::lang('backend.bus_number')?></th><th class="text-right"><?=Kohana::lang('backend.amount')?></th></tr>
						<?php 
						
						foreach ($bus_and_expenses as $bus_number => $total_expenses)
						{?>
						<tr>
							<td><?=$bus_number?></td>
							<td class="text-right"><?=number_format($total_expenses)?></td>
						</tr>
						<?php }?>
					</table>
				</div>
				<div class="span4">
				<div class="heading">Income [High to low]</div>
					<table class="table table-striped table-condensed">
						<tr><th><?=Kohana::lang('backend.bus_number')?></th><th class="text-right"><?=Kohana::lang('backend.amount')?></th></tr>
						<?php 
						foreach ($bus_and_income as $bus_number => $total_income)
						{?>
						<tr>
							<td><?=$bus_number?></td>
							<td class="text-right"><?=number_format($total_income)?></td>
						</tr>
						<?php }?>
					</table>
				</div>
				
				<div class="span4">
				<div class="heading">Net [High to low]</div>
					<table class="table table-striped table-condensed">
						<tr><th><?=Kohana::lang('backend.bus_number')?></th>
						<th class="text-right"><?=Kohana::lang('backend.amount')?></th></tr>
						<?php 
						foreach ($bus_and_net as $bus_number => $total_net)
						{?>
						<tr>
							<td><?=$bus_number?></td>
							<td class="text-right"><?=number_format($total_net)?></td>
						</tr>
						<?php }?>
					</table>
				</div>
					
					
				
				<div class="clear">&nbsp;<br/><br/><br/><br/><br/>
				</div>
				
				<!--NET SUMMARY TABLE-->
				<table class="table table-condensed table-striped ">
					<tr>
						<th class="span2">GRAND TOTAL</th>
						<td>&nbsp;</td>
						<th class="text-right" >EXPENSES</th>
						<th class="text-right">INCOME</th>
					</tr>
					
					<tr>		
						<td></td>
						<th></th>
						<th class="span2 text-right"><?=number_format($overall_expenses)?></th>
						<th class="span2 text-right"><?=number_format($overall_income)?></th>
					</tr>
					<tr>		
						<td></td>
						<th></th>
						<th class="span2 text-right">NET</th>
						<th class="span2 text-right"><?=number_format($overall_net)?> </th>
					</tr>
				</table>
					
				<div>Printed by: <?=STRTOUPPER($this->admin->username);?></div> 
				<div>Printed on: <?=date('d-m-Y h:i:s A')?></div>
				<!--END OF NET SUMMARY-->
				
			</div>

		
		
		
		
		
		
		</div>
