<?php defined('SYSPATH') or die('No direct script access');

class Ticket_Detail_Model extends ORM{

	public function add_ticket($agency_id, $schedule_id, $seat_number, $client_name, $client_phone, $client_idc, $client_email, $price, $missed_status=0, $free_ticket, $free_ticket_reason, $created_by, $is_remote=0, $remote_agency_from=0, $remote_agency_to=0, $remote_ticket_outgoing=0)
	{	
		$ticket = ORM::FACTORY('ticket_detail');
		$ticket->AgencyID = $agency_id;
		$ticket->ScheduleID = $schedule_id;
		$ticket->SeatNumber = $seat_number;
		$ticket->ClientName = $client_name;
		$ticket->ClientPhone = $client_phone;
		$ticket->ClientEmail = $client_email;
		$ticket->ClientIDC = $client_idc;
		$ticket->Price = $price;
		$ticket->MissedStatus = $missed_status;
		$ticket->FreeTicket = $free_ticket;
		$ticket->FreeTicketReason = $free_ticket_reason;
		$ticket->IsRemote = $is_remote;
		//If it's remote, it was created to be used elsewhere. Specified in the next line
		$ticket->RemoteTicketOutgoing = $remote_ticket_outgoing;
		$ticket->RemoteTicketAgencyFrom = $remote_agency_from;
		$ticket->RemoteTicketAgencyTo = $remote_agency_to;
		$ticket->CreatedBy = strtoupper($created_by);
		
		
		//Update the admin's available balance
		$admin = ORM::FACTORY('admin')->where('username',$created_by)->find();
		
		$admin->available_balance = $admin->available_balance + $price;
		
		$admin->save();
	
		$ticket->save();
		return $ticket->id;
	}
	
	public function get_ticket($agency_id, $schedule_id, $seat_number)
	{
		$ticket=ORM::factory('ticket_detail')->where('AgencyID',$agency_id)->where('SeatNumber',$seat_number)->where('ScheduleID',$schedule_id)->find();
		return $ticket;
	}
	
	public function update_ticket($agency_id, $schedule_id, $seat_number, $client_name, $client_phone, $client_email, $client_idc)
	{
		$ticket=ORM::factory('ticket_detail')->where('AgencyID',$agency_id)->where('SeatNumber',$seat_number)->where('ScheduleID',$schedule_id)->find();
		$ticket->ClientName = $client_name;
		$ticket->ClientPhone = $client_phone;
		$ticket->ClientEmail = $client_email;
		$ticket->ClientIDC = $client_idc;
		$ticket->save();
		
		return $ticket;
	}
	
	public function count_total_issued_today($agency_id)
	{	$db = new Database();
		$today = date('Y-m-d');
		$rows = $db->query("SELECT * FROM ticket_details WHERE AgencyID= ".$agency_id." AND ticket_details.CreatedOn >= '$today'  AND  ticket_details.CreatedOn <= '$today' + INTERVAL 1 DAY");
		return (count($rows));
		//return $count;	
		
	}
	
	public function count_free_issued_today($agency_id)
	{	$db = new Database();
		$today = date('Y-m-d');
		$rows = $db->query("SELECT * FROM ticket_details WHERE AgencyID= ".$agency_id." AND FreeTicket='1'  AND ticket_details.CreatedOn >= '$today'  AND  ticket_details.CreatedOn <= '$today' + INTERVAL 1 DAY");
		
		return count($rows);	
	}
	
	
	public function get_agency_tickets_by_period_by_admin($agency_id,$start_date,$end_date, $admin){
		$db = new Database();
		//NB, THIS QUERY IS VERY IMPORTANT. SINCE THE START AND END ARE DATES, AND CREATED ON IS A TIMSTAMP, IF YOU COMPARE, YOU'LL ONLY GET RESULTS WITH 
		//TIMESTAMPS THAT END IN 00:00:00 E.G. BETWEEN 17TH AND 17TH OF THE SAME MONTH, A TIMESTAMP WITH SAY 12:23:00 WILL NOT APPEAR IN RESULT.
		//HENCE INTERVAL IS USED.
		
		//$tickets = $db->query("SELECT * FROM ticket_details WHERE AgencyID= ".$agency_id." AND CreatedBy= '".$admin."'  AND CreatedOn >= '$start_date'  AND  CreatedOn <= '$end_date' + INTERVAL 1 DAY ORDER BY CreatedOn ASC");
		//ALSO SINCE THERE ARE TICKETS WITH NO SCHEDULES, LIKE REMOTE TICKETS AND SO, WE MUST USE LEFT JOIN, AS A NORMAL JOIN WILL RETURN NO RESULTS.
		//A LEFT JOIN RETURNS RESULTS FROM THE LEFT TABLE
		$tickets = $db->query("SELECT * FROM ticket_details LEFT JOIN schedules on schedules.id=ticket_details.ScheduleID WHERE AgencyID= ".$agency_id." AND ticket_details.CreatedBy= '".$admin."'  AND ticket_details.CreatedOn >= '$start_date'  AND  ticket_details.CreatedOn <= '$end_date' + INTERVAL 1 DAY ORDER BY ticket_details.CreatedOn ASC");
		return $tickets;
	}
	
	public function get_agency_tickets_total_by_period_by_admin($agency_id,$start_date,$end_date,$admin){
		$tickets=Ticket_detail_Model::get_agency_tickets_by_period_by_admin($agency_id,$start_date,$end_date,$admin);
		//Income from schedule
		$tickets_income_total = 0;
		foreach($tickets as $ticket)
		{	//Don't add the price for free tickets
			if($ticket->FreeTicket =='0'){
				$tickets_income_total += $ticket->Price;
			}
		}
		
		return $tickets_income_total;
	}
	
	public function search_passenger_by_name($agency_id, $passenger_name)
	{
		$db = new Database();
		$tickets = $db->query("SELECT * FROM ticket_details LEFT JOIN schedules on schedules.id=ticket_details.ScheduleID WHERE AgencyID= ".$agency_id." AND ticket_details.ClientName LIKE '%".$passenger_name."%' GROUP BY "."ClientIDC");
		return $tickets;
	}
	public function get_passenger_list($agency_id,$filename)
	{
		$db = new Database();
		//ORDER BY  'ClientName' (WITHOUT GROUP BY LINE) DESC WILL GIVE YOU THE COLUMN NAMES BUT NO ORDER OR GROUPING
		$result = $db->query("SELECT  'Name', 'IDC',  'Phone', 'Email'
			UNION ALL 
			SELECT  `ClientName` ,  `ClientIDC`,`ClientPhone` ,`ClientEmail`
			FROM ticket_details
GROUP BY `ClientIDC`
			ORDER BY  `Name` ASC
			INTO OUTFILE  '".$filename."'
			FIELDS TERMINATED BY  ','
			ENCLOSED BY  '\"'
			LINES TERMINATED BY  '\\n'");
		return $result;
	}
	
	public function passenger_activity($agency_id, $passenger_name, $passenger_id)
	{
		$db = new Database();
		$tickets = $db->query("SELECT * FROM ticket_details LEFT JOIN schedules on schedules.id=ticket_details.ScheduleID WHERE AgencyID= ".$agency_id." AND ticket_details.ClientName = '".$passenger_name."' AND ticket_details.ClientIDC = '".$passenger_id."' ORDER BY schedules.departure_date DESC");
		return $tickets;
	}
	public function get_agency_tickets_by_period_all_admins($agency_id,$start_date,$end_date)
	{
		$db = new Database();
		//Same rules as above. Now, for all admins
		$tickets = $db->query("SELECT * FROM ticket_details LEFT JOIN schedules on schedules.id=ticket_details.ScheduleID WHERE AgencyID= ".$agency_id."  AND ticket_details.CreatedOn >= '$start_date'  AND  ticket_details.CreatedOn <= '$end_date' + INTERVAL 1 DAY ORDER BY ticket_details.CreatedOn ASC");
		return $tickets;
	}
	
	public function get_agency_outgoing_remote_tickets_by_period_all_admins($agency_id,$start_date,$end_date)
	{
		$db = new Database();
		//Same rules as above. Now, for all admins
		$tickets = $db->query("SELECT * FROM ticket_details LEFT JOIN schedules on schedules.id=ticket_details.ScheduleID WHERE AgencyID= ".$agency_id."  AND ticket_details.CreatedOn >= '$start_date' AND ticket_details.RemoteTicketOutgoing = '1' AND  ticket_details.CreatedOn <= '$end_date' + INTERVAL 1 DAY ORDER BY ticket_details.CreatedOn ASC");
		return $tickets;
	}
	public function get_parent_outgoing_remote_tickets_by_period_all_admins($parent_id,$start_date,$end_date)
	{
		$db = new Database();
		//Same rules as above. Now, for all admins
		$tickets = $db->query("SELECT * FROM ticket_details LEFT JOIN schedules on schedules.id=ticket_details.ScheduleID JOIN agencies on agencies.id=ticket_details.AgencyID WHERE agencies.parent_id= ".$parent_id."  AND ticket_details.CreatedOn >= '$start_date' AND ticket_details.RemoteTicketOutgoing = '1' AND  ticket_details.CreatedOn <= '$end_date' + INTERVAL 1 DAY ORDER BY ticket_details.CreatedOn ASC");
		return $tickets;
	}
	
	public function get_all($agency_id)
	{
		$tickets=ORM::factory('ticket_detail')->where('AgencyID',$agency_id)->find_all();
		return $tickets;	
	}
	public function get_all_remote_active($agency_id)
	{
		$tickets=ORM::factory('ticket_detail')->where('AgencyID',$agency_id)->where('IsRemote','1')->find_all();
		return $tickets;	
	}
	public function get_all_remote_incoming_pending($agency_id)
	{
		$tickets=ORM::factory('ticket_detail')->where('AgencyID',$agency_id)->where('IsRemote','1')->where('RemoteTicketIncoming','1')->find_all();
		return $tickets;	
	}
	
	public function get_all_for_schedule($agency_id, $schedule_id)
	{
		$tickets=ORM::factory('ticket_detail')->where('AgencyID',$agency_id)->where('ScheduleID',$schedule_id)->find_all();
		return $tickets;	
	}

	public function get_all_unpushed_outgoing_remote_tickets($agency_id)
	{
		$tickets=ORM::factory('ticket_detail')->where('AgencyID',$agency_id)->where('IsRemote','1')->where('RemoteTicketOutgoing','1')->where('Online','0')->find_all();
		return $tickets;	
	}
	
	public function get_all_missed($agency_id)
	{
		//$tickets=ORM::factory('ticket_detail')->where('AgencyID',$agency_id)->where('MissedStatus','1')->find_all();
		$tickets = $this->db->select('ticket_details.id,  ticket_details.Price, schedules.status AS schedule_status, MissedStatus,to,departure_time, departure_date, ClientName, bus_number, checked_out_time, towns.name AS town_name_to')->from('ticket_details')->join('schedules','schedules.id=ticket_details.ScheduleID')->join('towns','towns.id=schedules.to')->where('AgencyID',$agency_id)->where('MissedStatus','1')->get();		
		return $tickets;	
	}
	
	public function get_all_pending($agency_id)
	{
		//$tickets=ORM::factory('ticket_detail')->where('AgencyID',$agency_id)->where('MissedStatus','1')->find_all();
		$tickets = $this->db->select('ticket_details.id,ticket_details.Price, schedules.status AS schedule_status, IsPending, MissedStatus,to,departure_time, departure_date, ClientName, bus_number, checked_out_time, towns.name AS town_name_to')->from('ticket_details')->join('schedules','schedules.id=ticket_details.ScheduleID')->join('towns','towns.id=schedules.to')->where('AgencyID',$agency_id)->where('IsPending','1')->get();		
		return $tickets;	
	}

	public function mark_absent($agency_id, $schedule_id, $seat_number)
	{
		//to pick the right ticket, you must get the one where the missed status is 0. All others might be people who were attributed this seat, then marked absent
	
		$ticket=ORM::factory('ticket_detail')->where('AgencyID',$agency_id)->where('SeatNumber',$seat_number)->where('ScheduleID',$schedule_id)->where('MissedStatus','0')->find();
		$ticket->MissedStatus = '1';
		$ticket->IsPending = '1';
		$ticket->save();
		
		$notice="Client marked as absent";
		return $notice;	
	}
	
	public function mark_present($agency_id, $schedule_id, $seat_number)
	{
		
		$ticket=ORM::factory('ticket_detail')->where('AgencyID',$agency_id)->where('SeatNumber',$seat_number)->where('ScheduleID',$schedule_id)->find();
		$ticket->MissedStatus = '0';
		$ticket->IsPending = '0';
		$ticket->save();
		
		$notice="Client marked as present";
		return $notice;	
	}
	
	public function mark_present_by_ticket_id($ticket_id)
	{
		
		$ticket=ORM::factory('ticket_detail')->where('id',$ticket_id)->find();
		$ticket->MissedStatus = '0';
		$ticket->IsPending = '0';
		$ticket->save();
		
		$notice="Client marked as present";
		return $notice;	
	}
	
	public function delete_ticket($agency_id, $schedule_id, $seat_number)
	{
		$ticket=ORM::factory('ticket_detail')->where('AgencyID',$agency_id)->where('SeatNumber',$seat_number)->where('ScheduleID',$schedule_id)->find();
		
		//Update the admin's available balance
		$admin = ORM::FACTORY('admin')->where('username',$ticket->CreatedBy)->find();
		$admin->available_balance = $admin->available_balance - $ticket->Price;
		
		$admin->save();
		$ticket->delete();
		$ticket->save();
		$notice="Ticket deleted";
		return $notice;
		
	}
	public function delete_ticket_by_id($ticket_id)
	{
		$ticket=ORM::factory('ticket_detail')->where('id',$ticket_id)->find();
		
		
		//Update the admin's available balance
		$admin = ORM::FACTORY('admin')->where('username',$ticket->CreatedBy)->find();
		
		$admin->available_balance = $admin->available_balance - $ticket->Price;
		$admin->save();
		
		//this must come below else the ticket is deleted a the admin cannot be found for his account balance to be reduced
		$ticket->delete();
		$ticket->save();
		
		$notice="Ticket deleted";
		return $notice;
		
	}	
	
	public function delete_ticket_by_id_without_reducing_balance($ticket_id)
	{
		$ticket=ORM::factory('ticket_detail')->where('id',$ticket_id)->find();
		//this must come below else the ticket is deleted a the admin cannot be found for his account balance to be reduced
		$ticket->delete();
		$ticket->save();
		
		$notice="Ticket deleted";
		return $notice;
		
	}
	

}
