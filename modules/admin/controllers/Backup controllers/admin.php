<?php defined('SYSPATH') or die('No direct script access');

//set default timezone, otherwise it's UTC which is an hour late
date_default_timezone_set('Africa/Douala');

//Almost all functions will take as parameter, the id of the agency we are dealing with
//in order not to interfere with another's stuff.

class Admin_Controller extends Template_Controller 
{
	public $template = 'template/admin_template';
	//public $uri_cont = 'http://api.wasamundi.com/v2/texto/';
	//public $uri_cont = 'http://quickticket.co/send_sms';
	//public $uri_cont = 'http://localhost:4001/quickticket/send_sms';
	protected $session;
	protected $agency_id;
	public $agency_code;
	protected $language;
	public $lang;
	
	public function __construct()
	{	
		parent::__construct();
		
		$this->admin = Authlite::instance()->get_admin();
		
		
		$this->session = Session::instance();
		//$this->current_page = url::current();
		
		//If the user hasn't changed language, use his default language	
		$this->lang = $this->session->get('lang');;
		//language settings have to be different for logged in and not logged in.
		if(empty($this->lang))
		{	
			$this->lang = array('en_US', 'English_United States');
		}
		
		Kohana::config_set('locale.language', $this->lang);
		
		//if there's an admin
		if (!empty($this->admin)) 
		{		
			$admin_default_lang = $this->admin->LanguageID;
			
			//if something has been done about the language, use the recently set language, else use the admin's default from the db
			if(!empty($this->lang))
			{
				Kohana::config_set('locale.language', $this->lang);
			}
			else
			{
				if($admin_default_lang == "en"){ $admin_default_lang = array('en_US', 'English_United States'); }
				elseif($admin_default_lang == "fr"){ $admin_default_lang = array('fr_FR', 'French_France'); }
				Kohana::config_set('locale.language', $admin_default_lang);
			}
			$this->agency_id = $this->admin->agency_id;
		}
	}  
	
	
	public function index()
	{	
		//ensure a logged in user can't land on the signup page
		Authlite::check_admin();
		//Decide whether admin is a branch manager, general manager or regular admin and redirect appropriately
		if($this->admin->admin_group_id == 3)
		{	
			url::redirect('control/main');
		}
		elseif($this->admin->admin_group_id == 4){
			url::redirect('manage/main');
		}
		else
		{
		//now if it's an admin, redirect depending on whether he's allowed to manage parcels/schedules/buses etc
		//the conditions in the menu only check which LINKS he can see and access
			//if he's a schedule manager, send him directly home
			if($this->admin->AllowScheduleManagement)
			{
				url::redirect('admin/main');
			}elseif($this->admin->AllowParcelManagement)
			{
				url::redirect('parcels/all/incoming');
			}elseif($this->admin->AllowBusManagement)
			{
				url::redirect('buses');
			}elseif($this->admin->AllowExpenseManagement)
			{	
				url::redirect('expenses');
			}
		}
		
	}
	
	public function login() 
	{	
		if (!empty($this->admin)) 
		{
			url::redirect('admin/main/'.$this->admin->agency_id);
		} 
		//$this->auto_render=false;
		$notice='';
		$this->template->title = Kohana::lang('backend.login');
		
		if($_POST) 
		{	
			
			$post=new Validation($_POST);
			$post->add_rules("username", "required",'valid::standard_text');
			$post->add_rules("password", "required");
			if ($post->validate())
			{
				$username = $_POST['username'];
				$password = $_POST['password'];
				
				$admin = Authlite::instance()->admin_login($username, $password);
				
				if(!$admin)
				{	
					//$notice="Sorry, incorrect username and password combination!";
					$this->session->set('notice', array('message'=>Kohana::lang('backend.incorrect_credentials'),'type'=>'error'));
				}
				else
				{
				
					//this is already done in the construct, but for some reason, does not pick the admin for
					//the first time till you refresh. so it is done here again 
					$this->admin = Authlite::instance()->get_admin();
					$admin_agency_id = $this->admin->agency_id;
					url::redirect('admin');
				}
			}else 
			{
                $errors=$post->errors('errors');
                $notice="";
                foreach($errors as $error) 
				{
                    $notice .= $error;
                }
				$this->session->set('notice', array('message'=>$notice,'type'=>'error'));
            }
		}
		$view=new View('admin_login');
		$view->notice=$notice;
		$this->template->content=$view;
	}
	

	
	public function main() 
	{	
		Authlite::check_admin();
		Authlite::verify_referer();
		$permitted_levels = array(2);
		Authlite::check_access($permitted_levels);
		//If he's not a schedule manager, by no means should he access main.
		if(!($this->admin->AllowScheduleManagement))
		{
			$this->logout();
		}
		
		$agency_id = $this->agency_id;
		//$admin_group = $this->admin->admin_group_id;
		//Authlite::check_agency_integrity($agency_id);
		$this->template->title = 'Welcome to your backend';
		$view=new View ('admin_main');
		$towns = get::all_towns();
		$current_schedules=get::ten_agency_current_schedules($agency_id);
		$departed_schedules=get::ten_agency_departed_schedules($agency_id);
		$parcels = get::ten_agency_incoming_parcels($agency_id);
		$parent = get::_parent($agency_id);
		//used for parcels
		$siblings = get::agency_siblings($agency_id,$parent);
		
		//$parent_buses = get::all_parent_buses($parent);
		$parent_buses = get::all_buses($agency_id, $parent->id, 4);
		$date=date('d-m-Y');
		
		$view->current_schedules=$current_schedules;
		$view->departed_schedules=$departed_schedules;
		$view->towns=$towns;
		$view->siblings=$siblings;
		$view->parent_buses=$parent_buses;
		
		$view->parcels=$parcels;
		$view->agency_id=$agency_id;
		$this->template->content=$view;
		
	}
	
	public function logout() 
	{
		Authlite::instance()->logout(true);
		url::redirect('admin');
	}
	
	public function create_schedule()
	{	
		Authlite::check_admin();
		Authlite::verify_referer();
		$permitted_levels = array(2);
		Authlite::check_access($permitted_levels);
		$this->auto_render = TRUE;
				
		$schedule = ORM::FACTORY('schedule');
		if($_POST){
			//no_csrf::check(1);
			
			// by default, set unregistered bus to zero, so it's not undefined
			$unregistered_bus=false;
			
			$post = new Validation($_POST);
			$admins_town = get::admins_town($this->admin->agency_id);
			//$post->add_rules('from','required');
			$post->add_rules('to','required');
			
			$post->add_rules('departure_time','required');
			$post->add_rules('departure_date','required','valid::alpha_dash');
			$post->add_rules('ticket_price','required','valid::digit');
			
			//if it's an unregistered bus, we need the bus number and number of seats, else we know it
			if(isset($_POST['unregistered_bus_check']))
			{
				//create a variable which we easily check each time to know if it's an unregistered bus
				$unregistered_bus=true;
				$post->add_rules('unregistered_bus_number','required','valid::alpha_numeric');
				$post->add_rules('unregistered_bus_seats','required');
			}else
			
			{  	//else something must be chosen from the dropdown.
				$post->add_rules('bus_number_and_seats','required');
			}

			if ($post->validate())
			{		
					$from = $admins_town->id;
					$to = $_POST['to'];
				
				//if it's an unregistered bus, save the new values entered, else save the old
				//remove all spaces from bus numbers before saving so that during search, NW123AB is NOT different from NW 123 AB
				if($unregistered_bus){
					$bus_number = strtoupper(str_replace(" ","", $_POST['unregistered_bus_number']));
					$bus_seats = $_POST['unregistered_bus_seats'];
				}else{
					//bus number and seats are passed as a single string separated by a semicolon, hence separate them here
					$bus_number_and_seats = explode(";", $_POST['bus_number_and_seats']);
					$bus_number = $bus_number_and_seats[0];
					$bus_seats = $bus_number_and_seats[1];
					
				}
															
					//convert time to 24hour for storage in sql time format, and for comparison in case of sorting
					$departure_time = date("H:i",strtotime($_POST['departure_time']));
					$departure_date = date("Y-m-d",strtotime($_POST['departure_date']));
					//ensure origin and destination are not the same
					if ($from == $to)
					{
						$this->session->set('notice', array('message'=>Kohana::lang('same_origin_destination'),'type'=>'error'));
						url::redirect('admin/main/'.$this->admin->agency_id);
					}
					else
					{
						
						//IF IT'S UNREGISTERED, CHECK IF WHAT IS ENTERED HASN'T BEEN REGISTERED BEFORE, IF SO, MAKE THEM PICK FROM THE LIST (REDIRECT) 
						
						if($unregistered_bus)
						{
							$bus_exists = ORM::FACTORY('bus')->where('bus_number',$bus_number)->count_all();
							if($bus_exists){
								$this->session->set('notice', array('message'=>$bus_number . Kohana::lang('backend.exists_in_list'),'type'=>'error'));
								url::redirect('admin/main/'.$this->admin->agency_id);
							}
							
							//CREATE A NEW BUS if it doesn't exist,
							$new_bus = ORM::FACTORY('bus');
							$new_bus->agency_id = $this->agency_id;
							$new_bus->bus_number = $bus_number;
							$new_bus->bus_seats = $bus_seats;
							$new_bus->CreatedBy = strtoupper($this->admin->username);
							$new_bus->save();
							$this->session->set('notice', array('message'=>Kohana::lang('backend.new_bus').  $bus_number . Kohana::lang('backend.is_registered'),'type'=>'success'));
							
						}
						
						$schedule->agency_id = $this->admin->agency_id;
						//$schedule->agency_code = $agency_code;
						$schedule->bus_number = strtoupper($bus_number);
						$schedule->bus_seats = $bus_seats;
						$schedule->from = $from;
						$schedule->to = $to;
						$schedule->status = 'current';
						$schedule->departure_date = $departure_date;
						$schedule->departure_time = $departure_time;
						$schedule->ticket_price = $_POST['ticket_price'];
						$schedule->CreatedBy = strtoupper($this->admin->username);;	
						$schedule->save();	
						url::redirect('admin/complete_schedule/'.$schedule->id);	
					}			
				}
			else
			{
				$errors=$post->errors('errors');
				$notice="";
					foreach($errors as $error) 
					{
						$notice.=$error."<br />";
					}
					$this->session->set('notice', array('message'=>$notice,'type'=>'error'));
					url::redirect('admin/main/'.$this->admin->agency_id);
			}
		}
			
	}
	
	 function edit_schedule($schedule_id)
	 {		
		Authlite::check_admin();
		//Authlite::check_integrity($schedule_id);
		$permitted_levels = array(2);
		Authlite::check_access($permitted_levels);
		Authlite::verify_referer();
		
		$this->template->title='Edit schedule';
		$schedule=get::schedule($schedule_id);
		$towns = get::all_towns();
			
		if($_POST)
		{
			//no_csrf::check();
			$post = new Validation($_POST);
			$post->add_rules('to','required');
			$post->add_rules('bus_number','required','valid::alpha_numeric');
			$post->add_rules('bus_seats','required');
			$post->add_rules('departure_time','required');
			$post->add_rules('departure_date','required','valid::alpha_dash');
			$post->add_rules('ticket_price','required','valid::digit');
			
			if ($post->validate())
			{
				$from = get::admins_town($this->admin->agency_id)->id;
				$to = $_POST['to'];
				$bus_number = strtoupper(str_replace(" ","", $_POST['bus_number']));
				$bus_seats = $_POST['bus_seats'];
				$departure_time = date("H:i",strtotime($_POST['departure_time']));
				$departure_date = date("Y-m-d",strtotime($_POST['departure_date']));
				
				$schedule->agency_id = $this->admin->agency_id;
				//$schedule->agency_code = $agency_code;
				//make bus numbers uppercase
				$schedule->bus_number = $bus_number;
				$schedule->bus_seats = $bus_seats;
				$schedule->from = $from;
				$schedule->to = $to;
				$schedule->status = 'current';
				$schedule->departure_date = $departure_date;
				$schedule->departure_time = $departure_time;
				$schedule->ticket_price = $_POST['ticket_price'];
				$schedule->save();	
				url::redirect('admin/complete_schedule/'.$schedule->id);
			}
			else
			{
				$errors=$post->errors('errors');
				$notice="";
					foreach($errors as $error) 
					{
						$notice.=$error."<br />";
					}
					$this->session->set('notice', array('message'=>$notice,'type'=>'error'));
			}
		}
					
		$view=new View('admin_edit_schedule');
		$view->schedule=$schedule;
		$view->towns=$towns;
		$this->template->content=$view;
	}
	
	
	function complete_schedule($schedule_id)
	 {					
			Authlite::check_admin();
			$permitted_levels = array(2);
			Authlite::check_access($permitted_levels);
			//Authlite::check_integrity($schedule_id);
			Authlite::verify_referer();
			
			$date = date("Y-m-d");
			$this->template->title=Kohana::lang('backend.complete_schedule');
			
			//first check if the schedule is departed, and if so, send him to past_schedule
			//departed schedules cannot be modified, so if it's checked out and admin tries to go back, it shouldn't work
			$status = get::schedule_status($schedule_id);
			if ($status == 'departed')
			{
				$this->session->set('notice', array('message'=>Kohana::lang('backend.already_checkedout'),'type'=>'error'));
				url::redirect('admin/past_schedule/'.$schedule_id);
			}
			
			$towns = get::all_towns();
			//an array to hold seats and corresponding occupants
			$schedule=get::schedule($schedule_id);
			
			//get all expenses already registered for this schedule. i.e.bus & date
			//$expenses_for_schedule = get::expenses_for_bus_and_date($this->agency_id, $schedule->bus_number, $date);
			
			//rather use the schedule id for this purpose. It's more accurate
			$expenses_for_schedule = get::expenses_for_schedule($this->agency_id, $schedule_id);
			
			//this gets the entire object
			$client_reservations=get::client_reservations($schedule_id);
			
			//this gets only the json with the reserved seats details
			$client_reservation_seat_infos=json_decode($client_reservations->reserved_seats,true);
			
			//$reserve = ORM::factory('reservation')->where('schedule_id',$schedule_id)->find();
			//first check if the field in the db is not empty. If it is, create the array. If not, decode the array and populate			
			if (empty ($schedule->reserved_seats))
			{
				$seats_and_reservers = array();			
			}
			else
			{
				$seats_and_reservers = json_decode($schedule->reserved_seats,true); //true converts the object to an array
			}
			//first check if the field in the db is not empty. If it is, create the array. If not, decode the array and populate			
			if (empty ($schedule->seat_occupants))
			{
				$seats_and_occupants = array();			
			}
			else
			{
				$seats_and_occupants = json_decode($schedule->seat_occupants,true);
			}
			
			if($_POST)
			{	
				//if the submit is coming from the check out form
				if(isset($_POST['check_out_form']))
				{	
					//no_csrf::check(1);
					//variable for special drop and free tickets
					$special_drop=false;
					$free_ticket=false;
					$post = new Validation($_POST);
					//if it;s a free ticket
					if(isset($_POST['free_ticket'])){$free_ticket=true;}
					//if it is a special drop, the price is required
					if(isset($_POST['spec_drop_check']))
					{
						//create a variable which we easily check each time to know if it's a special drop
						$special_drop=true;
						$post->add_rules('special_drop_price','required','valid::digit');
						$post->add_rules('special_drop_town','required');
					}					
					$post->add_rules('client_name','required');
					$post->add_rules('seat_number','required');
					$post->add_rules('client_email','valid::email');
					$post->add_rules('client_phone','valid::digit');
					$post->add_rules('client_idc','valid::digit');
					
					
					if ($post->validate())
					{
						
						$client_name = $_POST['client_name'];
						$client_phone = $_POST['client_phone'];
						$client_email = $_POST['client_email'];
						$seat_number = $_POST['seat_number'];
						$client_idc = $_POST['client_idc'];
						
						if($special_drop){
							$special_drop_price = $_POST['special_drop_price'];
							$special_drop_town = $_POST['special_drop_town'];
							}
						
						//THE POPULATION!
						//Seat number is key, client's array is value
						//first check if the seat is occupied, possibly in another tab
						if(isset($seats_and_occupants[$seat_number])){
							$this->session->set('notice', array('message'=>Kohana::lang('backend.sorry_seat'). $seat_number . Kohana::lang('backend.given_out'),'type'=>'error'));
							url::redirect('admin/complete_schedule/'.$schedule_id);	
						}
						//int converts the boolean value of free ticket into an integer
						if($special_drop){
							$seats_and_occupants[$seat_number] = array('name'=>$client_name,'phone'=>$client_phone,'email'=>$client_email,'sd_price'=>$special_drop_price,'sd_town'=>$special_drop_town,'idc'=>$client_idc,'free_ticket'=>(int)$free_ticket);
						}else{
							$seats_and_occupants[$seat_number] = array('name'=>$client_name,'phone'=>$client_phone,'email'=>$client_email,'idc'=>$client_idc,'free_ticket'=>(int)$free_ticket);
						}
						//var_dump($seats_and_occupants);exit;
						//when it's through, json encode and save back to db
						$schedule->seat_occupants = json_encode($seats_and_occupants);
						$schedule->save();
						
						$this->session->set('notice', array('message'=>Kohana::lang('backend.seat')." <b>".$seat_number."</b> ".Kohana::lang('backend.seat_checked_out'),'type'=>'success'));
						$parent = strtoupper(get::_parent($this->agency_id)->name);
						$from = get::town($schedule->from);
						
						//the ticket price and destination might be variable depending whether it's a regula seat or special drop
						//this should be reflected on the ticket
						
						
						if($special_drop){
							$to = get::town($special_drop_town);
							$ticket_price = $special_drop_price;
						}else{
							$to = get::town($schedule->to); 
							$ticket_price = $schedule->ticket_price;
							}
						//use the parent id to check which ticket to use	
						$parent_id = get::_parent($this->admin->agency_id)->id;
					
						//check the parent agency and generate the ticket 
						//if it's type 2, use type 2. Else use default ticket
						//i
						$ticket_type_1 = settings::ticket_type_1();
						$ticket_type_2 = settings::ticket_type_2();
						if($ticket_type_2==1){
							
							$ticket = printer::print_ticket_type_2($parent, $schedule->departure_time, $schedule->departure_date, $client_name, $client_phone, $seat_number, $from, $to, $schedule->bus_number, $ticket_price);	
						
						}else{
							
							$ticket = printer::print_ticket($parent, $schedule->departure_time, $schedule->departure_date, $client_name, $client_phone, $seat_number, $from, $to, $schedule->bus_number, $ticket_price);	
						}
						//first check if the option to auto-generate tickets is set
						$auto_ticket = settings::auto_ticket();
						
							
						if($auto_ticket == 1)
						{ 	
							//open the ticket in a new window
							echo "<script>
									win=window.open('$ticket','Print ticket','width=900,height=400');
									win.focus();
									
								  </script>
								  ";
							//DO NOT MODIFY THESE LINES. THEY DO THE REDIRECT ENSURING THAT PAGE REFRESH IS NOT POSSIBLE HERE, NOR THE BACK BUTTON  
							echo '<script>
									window.location ='.$schedule_id.'; 
							</script>'; 
						} 	
					}
					else
					{
						$errors=$post->errors('errors');
						$notice="";
							foreach($errors as $error) 
							{
								$notice.=$error."<br />";
							}
							$this->session->set('post', $_POST);
							$this->session->set('notice', array('message'=>$notice,'type'=>'error'));
					}
				
				}
				
				//if the submit is coming from the reserve seat form
				if(isset($_POST['reserve_seat_form']))
				{
					//no_csrf::check(3);
					$post = new Validation($_POST);
					$post->add_rules('res_name','required');
					$post->add_rules('res_phone','valid::digit','length[9]');
					$post->add_rules('res_idc','valid::digit');
					$post->add_rules('res_seat','required');
					
					if ($post->validate())
					{	
						//print_r($reserve->seat_occupants);exit;
						$res_name = $_POST['res_name'];
						$res_phone = $_POST['res_phone'];
						$res_email = $_POST['res_email'];
						$res_seat = $_POST['res_seat'];
						$res_idc = $_POST['res_idc'];
							
							
							//first check if the seat is occupied, possibly in another tab
						if(isset($seats_and_occupants[$res_seat]))
						{
								$this->session->set('notice', array('message'=> Kohana::lang('backend.sorry_seat'). $res_seat. Kohana::lang('backend.given_out'),'type'=>'error'));
								url::redirect('admin/complete_schedule/'.$schedule_id);	
						} 
							
						//on the schedule table, we enter the reserver's details in a multidimensional array
						//in the second level of the array, 1->reserver's name, 2->reserver's phone, 3->reserver's email
						$seats_and_reservers[$res_seat] = array('name'=>$res_name,'phone'=>$res_phone,'email'=>$res_email,'idc'=>$res_idc);
						$schedule->reserved_seats = json_encode($seats_and_reservers);
						$schedule->save();
						
						$this->session->set('notice', array('message'=>Kohana::lang('backend.seat_reserved'),'type'=>'success'));
						url::redirect('admin/complete_schedule/'.$schedule_id);	
						//need to modify this section		
					}
					else
					{
						$errors=$post->errors('errors');
						$notice="";
							foreach($errors as $error) 
							{
								$notice.=$error."<br />";
							}
							$this->session->set('post', $_POST);
							$this->session->set('notice', array('message'=>$notice,'type'=>'error'));
							//url::redirect('admin/complete_schedule/'.$schedule_id."#reservation-tab");	
					}
				}
		if (isset($_POST['res_to_occ']))
		{
			//no_csrf::check(2);
			//convert a reserved seat to occupied.

			$details = unserialize($_POST['arr']);
			
			//the seat number is the key of the current array
			$seat_number = $_POST['res_to_occ'];
			$client_name = $details[$seat_number]['name'];
			$client_phone = $details[$seat_number]['phone'];
			$client_email = $details[$seat_number]['email'];
			$client_idc = $details[$seat_number]['idc'];
			//print_r($details[$seat_number]);exit;
			
			//first check if the seat is already occupied
			if(isset($seats_and_occupants[$seat_number]))
			{
				$this->session->set('notice', array('message'=>Kohana::lang('backend.sorry_seat') .$res_seat. Kohana::lang('backend.given_out'),'type'=>'error'));
				url::redirect('admin/complete_schedule/'.$schedule_id);	
			}	
			//$seats_and_occupants = json_decode($schedule->seat_occupants,true);
			$seats_and_occupants[$seat_number] = array('name'=>$client_name,'phone'=>$client_phone,'email'=>$client_email,'idc'=>$client_idc);
			//when it's through, json encode and save back to db
				
											
			$schedule->seat_occupants = json_encode($seats_and_occupants);

				//if the request was from the frontend, remove it from the reservation_requests table	
				if (isset($_POST['request_from_frontend']))
				{
					//$client_reservations=json_decode($client_reservations);
					unset($client_reservation_seat_infos[$seat_number]);
					$client_reservations->reserved_seats = json_encode($client_reservation_seat_infos);
					$client_reservations->save();
					
					//then save the schedule
					$schedule->save();
					
				}else
				{
					//else remove that entry from the seats and reservers column
					unset($seats_and_reservers[$seat_number]);
					$schedule->reserved_seats = json_encode($seats_and_reservers);
					$schedule->save();
				}
								
			$parent = strtoupper(get::_parent($this->agency_id)->name);				
			//send confirmation SMS	if he had a phone
			if(!empty($client_phone))
			{
				$msg = Kohana::lang('backend.hello').$client_name. ". ".Kohana::lang('backend.res_confirmed'). $parent. Kohana::lang('backend.is_confirmed')." Bus: ". $schedule->bus_number ." ".Kohana::lang('backend.time').": ".date("g:i A",strtotime($schedule->departure_time));
				$to = '237'.$client_phone;
				//$result = sms::send($msg,$to,$this->agency_id);
				$result = sms::save($msg,$to,$this->agency_id);
			}
							
					$from = get::town($schedule->from);
					$to = get::town($schedule->to);
					$ticket_price = $schedule->ticket_price;
					
					$ticket = printer::print_ticket($parent, $schedule->departure_time, $schedule->departure_date, $client_name, $client_phone, $seat_number, $from, $to, $schedule->bus_number, $ticket_price);	
					//first check if the option to auto-generate tickets is set
					$auto_ticket = settings::auto_ticket();
					if($auto_ticket == 1)
					{ 	
						//open the ticket in a new window
						echo "<script>
								win=window.open('$ticket','Print ticket','width=900,height=400');
								win.focus();
								
							  </script>
							  ";
						//DO NOT MODIFY THESE LINES. THEY DO THE REDIRECT ENSURING THAT PAGE REFRESH IS NOT POSSIBLE HERE, NOR THE BACK BUTTON  
						echo '<script>
								window.location ='.$schedule_id.'; 
						</script>'; 
					} 	
			
			
			
			//$to = "23796362464";
			//$result = "SMS is disabled";
			
			$this->session->set('notice', array('message'=>Kohana::lang('backend.client_confirmed')."<br/>Notice: ".@$result,'type'=>'success'));
			//$this->session->set('notice', array('message'=>"The client has been confirmed. <br/>Notice: ".$result,'type'=>'success'));
		}
		
		if (isset($_POST['reject_reserve']))
		{
			$details = unserialize($_POST['arr']);
			//the seat number is the key of the current array
			$seat_number = $_POST['reject_reserve'];
			
			//if the request was from the frontend, remove it from the appropriate column in the reservation_requests table		
			if (isset($_POST['request_from_frontend']))
			{
				//$client_reservations=json_decode($client_reservations);
				unset($client_reservation_seat_infos[$seat_number]);
				$client_reservations->reserved_seats = json_encode($client_reservation_seat_infos);
				$client_reservations->save();
							
			}else
			{
				unset($seats_and_reservers[$seat_number]);
				$schedule->reserved_seats = json_encode($seats_and_reservers);
				$schedule->save();
				$this->session->set('notice', array('message'=>Kohana::lang('backend.deleted_res'),'type'=>'success'));
			}
		}
		
		if (isset($_POST['empty_seat']))
		{

			$details = unserialize($_POST['arr1']);
			//the seat number is the key of the current array
			//$seat_number = key($details);
			$seat_number = $_POST['empty_seat'];
				
			//$seats_and_occupants = json_decode($schedule->seat_occupants,true);
			//$seats_and_occupants[$seat_number] = array('name'=>$client_name,'phone'=>$client_phone,'email'=>$client_email);
			//when it's through, json encode and save back to db
						//$schedule->seat_occupants = json_encode($seats_and_occupants);
			//then remove that entry from the seats and reservers column
			unset($seats_and_occupants[$seat_number]);
			$schedule->seat_occupants = json_encode($seats_and_occupants);
			$schedule->save();
			$this->session->set('notice', array('message'=>Kohana::lang('backend.client_deleted').Kohana::lang('backend.seat').$seat_number." ".Kohana::lang('backend.now_empty'),'type'=>'success'));
		}
		
		if(isset($_POST['print_single_ticket']))
		{
			$details = unserialize($_POST['arr1']);
			//the seat number is the key of the current array
			$seat_number = $_POST['print_single_ticket'];

			$ticket = printer::fetch_ticket($schedule->departure_date,$schedule->bus_number,$seat_number);
			
			//open the ticket in a new window
			echo "<script>
					win=window.open('$ticket','Print ticket','width=900,height=400');
					win.focus();
				  </script>";
		}

			
		}
		
			$view=new View('admin_complete_schedule');
			$view->schedule=$schedule;
			$view->expenses_for_schedule=$expenses_for_schedule;
			//var_dump($schedule->bus_seats);exit;
			//$view->client_reservations=json_decode($client_reservations->reserved_seats,true);
			$view->client_reservations=$client_reservation_seat_infos;
			$view->towns=$towns;
			$view->seats_taken = count($seats_and_occupants);
			$view->seats_left = ($schedule->bus_seats - count($seats_and_occupants));
			$view->seats_and_occupants = $seats_and_occupants;
			$view->seats_and_reservers = $seats_and_reservers;
			$this->template->content=$view;
			
	}
	
	
	
	public function checkout_schedule($schedule_id)
	{	
		Authlite::check_admin();
		Authlite::verify_referer();
		
		$this->template->title='Check out a schedule';
		$schedule = get::schedule($schedule_id);
		
		//check if there are reserved seats on this bus. this MUST be declared empty or converted to 'occupied'
		// [] is left over when reserved seats are converted to occupied.
		if ($schedule->reserved_seats !=='[]' && $schedule->reserved_seats !=='')
		{
			$this->session->set('notice', array('message'=>Kohana::lang('backend.error_reserved'),'type'=>'error'));
			url::redirect('admin/complete_schedule/'.$schedule_id);
		}
		
		// now ensure an empty bus can't be checked out	
		elseif(empty($schedule->seat_occupants))
		{
			$this->session->set('notice', array('message'=>Kohana::lang('backend.err_empty_bus'),'type'=>'error'));
			url::redirect('admin/complete_schedule/'.$schedule_id);
		}
		
		else
		{		
			$seats_and_occupants = json_decode($schedule->seat_occupants,true);
			//number of seats and occupants
			//calculate the amount from special drops
			$special_drop_total = 0;
			//count the number of special drops
			$special_drop_count = 0;
		
			foreach($seats_and_occupants as $s)
			{
			//get the special drop price, and if it's not defined, MOVE ON
			if(isset($s['sd_price'])){
				$special_drop_total += @$s['sd_price'];
				$special_drop_count++;}
			}

				
			//number of seats and occupants
			$no_so = count($seats_and_occupants);
			//regular seats are those paid for at normal price (as opposed to special drops)				
			//speculated normal cost of seats taken by special drops called amount lost
			$amount_lost = ($schedule->ticket_price) * $special_drop_count;
			//total is number of seats occupied times ticket price, minus speculated normal cost of special drops(lost), plus actual amount got from the special drops		
			$schedule->total_amount = ($no_so * $schedule->ticket_price) - $amount_lost + $special_drop_total;
			
			
			$schedule->status = 'departed';
			$schedule->CheckedOutBy = strtoupper($this->admin->username);
			//save the current time
			$schedule->checked_out_time = date("H:i");
			$schedule->save();
			
			$this->session->set('notice', array('message'=>Kohana::lang('backend.bus_departed'),'type'=>'success'));
			url::redirect('admin/past_schedule/'.$schedule_id);
		}	
	}
	
	
	public function start_loading_schedule($schedule_id)
	{	//var_dump($_SESSION);exit;
		Authlite::check_admin();
		//Authlite::check_integrity($schedule_id);
		Authlite::verify_referer();
		
		$this->template->title='Start loading schedule';
		$schedule = get::schedule($schedule_id);
		
		$seats_and_occupants = json_decode($schedule->seat_occupants,true);
		$schedule->loading = 1;
		$schedule->save();
		
		$i = 0;
		foreach ($seats_and_occupants as $key=>$value)
		{	
			if(!empty($value['phone']))
			{
				//send notification SMS			
				$msg = "Hi ". $value['name'] .", your bus will take off in a few moments. Thanks for trusting ". get::_parent_name($this->agency_id).".";
				$to = '237'. $value['phone'];
				
				$result = sms::save($msg,$to,$this->agency_id);
				$i++;
			}
		}
			$this->session->set('notice', array('message'=>"$i SMS sent",'type'=>'success'));
			url::redirect('admin/complete_schedule/'.$schedule_id);		
	}
	
	
	public function checkout_seat($schedule_id)
	{
		Authlite::check_admin();
		Authlite::verify_referer();
		
		//no_csrf::check();
		$view = new View('admin_complete_schedule');
		$this->template->content=$view;

	}
	
	
	
	public function past_schedule($schedule_id)
	{
		Authlite::check_admin();	
		Authlite::verify_referer();
		//Authlite::check_integrity($schedule_id);
		
		$this->template->title = Kohana::lang('backend.display_past');
		
		$schedule = get::schedule($schedule_id);
		$expenses_for_schedule = get::expenses_for_schedule($this->agency_id,$schedule_id);
		
		$seats_and_reservers = json_decode($schedule->reserved_seats,true);
		$seats_and_occupants = json_decode($schedule->seat_occupants,true);
		$view = new View('admin_past_schedule');
		
		//number of seats and occupants
		//calculate the amount from special drops
		$special_drop_total = 0;
		//count the number of special drops
		$special_drop_count = 0;
		//count the number of free tickets
		$free_tickets_price = 0;
		$free_ticket_count = 0;
		
		foreach($seats_and_occupants as $s)
		{
			//get the special drop price, and if it's not defined, MOVE ON
			if(isset($s['sd_price']))
			{
				$special_drop_total += @$s['sd_price'];
				$special_drop_count++;
			}
			//if the value of free ticket is 1
			if(isset($s['free_ticket']) && $s['free_ticket']==1)
			{
			//if it's also a special drop, get the price to know how much is lost
				if(isset($s['sd_price'])){
					$free_tickets_price += @$s['sd_price'];}
				else{
					$free_tickets_price += $schedule->ticket_price;
				}
				$free_ticket_count++;
			}
		}

		//number of seats and occupants
		$no_so = count($seats_and_occupants);
		//for now we count only occupants as reservers will be converted to occupants upon payment

		//regular seats are those paid for at normal price (as opposed to special drops)
		$regular_seats = $no_so - $special_drop_count;
		
		
		$view->seats_and_occupants = $seats_and_occupants;
		$view->seats_and_reservers = $seats_and_reservers;
		$view->no_so = $no_so;
		$view->empty_seats = $schedule->bus_seats - $no_so;
		$view->ticket_price = $schedule->ticket_price;
		
		//speculated normal cost of seats taken by special drops called amount lost
		$amount_lost = ($schedule->ticket_price) * $special_drop_count;
		//total is number of seats occupied times ticket price, minus speculated normal cost of special drops(lost), plus actual amount got from the special drops		
		$view->total = ($no_so * $schedule->ticket_price) - $amount_lost + $special_drop_total - $free_tickets_price;
		$view->schedule = $schedule;
		$view->free_ticket_count = $free_ticket_count;
		$view->special_drop_count = $special_drop_count;
		$view->special_drop_total = $special_drop_total;
		$view->regular_seats = $regular_seats;
		$view->expenses_for_schedule = $expenses_for_schedule;
		$this->template->content = $view;

	}
	
	/*
	public function control($agency_id, $user_group=3) 
	{	
		Authlite::check_admin();
		Authlite::verify_referer();
		$permitted_levels = array(3,4);
		Authlite::check_access($permitted_levels);
		Authlite::check_agency_integrity($agency_id);
		//print_r($not);exit;
		//print_r($this->admin->access_level);exit;
		$this->template->title = Kohana::lang('backend.welcome_backend');
		$view=new View ('admin_control');
		$towns = get::all_towns();
		$admin_town =get::admins_town($this->admin->agency_id);
		$parent_id = get::_parent($this->admin->agency_id);
		//$current_schedules = get::all_parent_current_schedules($parent_id);
		//$departed_schedules = get::all_parent_departed_schedules($parent_id);


	
		//now, if it's a branch manager, limit his query to his town
		//show all only for general manager
		if ($user_group == 3){
			$current_schedules=get::ten_agency_current_schedules($agency_id);
			$departed_schedules=get::ten_agency_departed_schedules($agency_id);
			}
		if ($user_group == 4){
			$current_schedules=get::all_parent_current_schedules($parent_id);
			$departed_schedules=get::all_parent_departed_schedules($parent_id);
			}
		$date=date('d-m-Y');
		
		$view->current_schedules=$current_schedules;
		$view->departed_schedules=$departed_schedules;
		
		$view->towns=$towns;
		$this->template->content=$view;
	} */
	
	
	/*
	public function schedule_by_date()
	{	
		Authlite::check_admin();
		Authlite::verify_referer();
		
		$agency_id = $this->agency_id;
		$parent_id = get::_parent($agency_id)->id;
		
		//$type: either parent or agency. Helps distinguish which the BM or GM sees.
		$type = $this->admin->admin_group_id;
		$this->template->title = Kohana::lang('backend.welcome_backend');
		if($_POST)
		{
			$post = new Validation($_POST);
			$post->add_rules('date','required');
			if ($post->validate())
			{	
				
			$date = $_POST['date'];
			//$schedules = ORM::FACTORY('schedule')->where('agency_id',$agency_id)->where('departure_date',$date)->find_all();
			
			//if it's a GM, type 2
			if ($type==4)
			{
				$_30seats = ORM::FACTORY('schedule')->join('agencies','agencies.id','schedules.agency_id')->where('parent_id',$parent_id)->where('departure_date',$date)->where('status','departed')->where('bus_seats',30)->count_all();
				$_55seats = ORM::FACTORY('schedule')->join('agencies','agencies.id','schedules.agency_id')->where('parent_id',$parent_id)->where('departure_date',$date)->where('status','departed')->where('bus_seats',55)->count_all();
				$_70seats = ORM::FACTORY('schedule')->join('agencies','agencies.id','schedules.agency_id')->where('parent_id',$parent_id)->where('departure_date',$date)->where('status','departed')->where('bus_seats',70)->count_all();
				
				$departed_schedules = get::all_parent_departed_schedules_by_date($parent_id,$date);				
				$current_schedules = get::all_parent_current_schedules_by_date($parent_id,$date); 
			}
			
			else{
			$_30seats = ORM::FACTORY('schedule')->where('agency_id',$agency_id)->where('departure_date',$date)->where('status','departed')->where('bus_seats',30)->count_all();
			$_55seats = ORM::FACTORY('schedule')->where('agency_id',$agency_id)->where('departure_date',$date)->where('status','departed')->where('bus_seats',55)->count_all();
			$_70seats = ORM::FACTORY('schedule')->where('agency_id',$agency_id)->where('departure_date',$date)->where('status','departed')->where('bus_seats',70)->count_all();
			
			$departed_schedules = get::all_agency_departed_schedules_by_date($agency_id,$date);				
			$current_schedules = get::all_agency_current_schedules_by_date($agency_id,$date); }
			
			$parcels=get::all_agency_parcels_by_date($agency_id,$date);

			//Calculate total amount expected from departed buses.
			$total = 0;
			foreach($departed_schedules as $ds){
				$seats_and_occupants = json_decode($ds->seat_occupants,true);
				$no_so = count($seats_and_occupants);
				$total += $no_so * $ds->ticket_price;
			}
			}
			else
			{
				$errors=$post->errors('errors');
				$notice="";
					foreach($errors as $error) 
					{
						$notice.=$error."<br />";
					}
					$this->session->set('notice', array('message'=>$notice,'type'=>'error'));
					url::redirect('admin/main/'.$this->admin->agency_id);
			}
	}
		$view = new View('admin_schedule_by_date');
		$view->departed_schedules = $departed_schedules;
		$view->current_schedules = $current_schedules;
		$view->parcels=$parcels;
		$view->_30seats = $_30seats;
		$view->_55seats = $_55seats;
		$view->_70seats = $_70seats;
		$view->total = $total;
		$view->date = $date;
			
		$this->template->content = $view;
		
	}	*/
	
	
	//type is either departed or current
	public function all_schedules($type, $sort="sort_by_date")
	{	
		Authlite::check_admin();
		$permitted_levels = array(2,3,4);
		Authlite::check_access($permitted_levels);
		Authlite::verify_referer();
		
		$agency_id = $this->agency_id;
		
		//do the sorting
		if ($sort == "sort_by_time"){$orderby = "departure_time";}
		elseif ($sort == "sort_by_bus"){$orderby = "bus_number";}
		elseif ($sort == "sort_by_destination"){$orderby = "to";}
		elseif ($sort == "sort_by_origin"){$orderby = "from";}
		elseif ($sort == "sort_by_time"){$orderby = "departure_time";}
		elseif ($sort == "sort_by_date"){$orderby = "departure_date";}
		//default orderby, to avoid bug when none is specified
		else($orderby = "departure_date");
		
		//pagination
		$per_page = 10;
		if ($type == 'current')
		{
			$all= get::all_agency_current_schedules($agency_id);
		}
		else
		{
			$all= get::all_agency_departed_schedules($agency_id);
		}
		
		$total = count($all);
		$this->pagination = new Pagination(array(
			// 'base_url'    => 'welcome/pagination_example/page/', // base_url will default to current uri
			'uri_segment'    => 'page', // pass a string as uri_segment to trigger former 'label' functionality
			'total_items'    => $total, // use db count query here of course
			'items_per_page' => $per_page , // it may be handy to set defaults for stuff like this in config/pagination.php
			'style'          => 'punbb', // pick one from: classic (default), digg, extended, punbb, or add your own!
			//'query_string'   => "ORM::factory('schedule')->where('agency_id',$agency_id)->where('status','current')->find_all()"
));
		if ($type == 'current')
		{
			//$schedules = get::all_agency_current_schedules($agency_id);
			$schedules=ORM::factory('schedule')->where('agency_id',$agency_id)->where('status','current')->where('Deleted','0')->limit($per_page,$this->pagination->sql_offset)->orderby($orderby,'desc')->find_all();			
		}else
		{
			//$schedules = get::all_agency_departed_schedules($agency_id);
			$schedules=ORM::factory('schedule')->where('agency_id',$agency_id)->where('status','departed')->where('Deleted','0')->limit($per_page,$this->pagination->sql_offset)->orderby($orderby,'desc')->find_all();			
		}
		
		$this->template->title = 'Viewing all schedules';		
		$view = new View('admin_all_schedules');
		$view->schedules = $schedules;
		$this->template->content = $view;
	
	}
	
	
	public function res_req()
	{	
		Authlite::check_admin();
		Authlite::verify_referer();
		
		$schedule_id = 12;		
		return $schedule_id;		
		$client_reservations=get::client_reservations($schedule_id);		
		$view = new View('admin_reservation_requests_div');	
		$view->client_reservations=json_decode($client_reservations->reserved_seats,true);
	}
	
	public function safe_delete_schedule($schedule_id)
	{		
			Authlite::check_admin();
			Authlite::verify_referer();
			
			$this->template->title="Delete admin";
			
			//First delete the schedule
			$notice = Schedule_Model::safe_delete_schedule($this->agency_id,$schedule_id);

			//Then delete the expenses related to that schedule.
			$related_expenses = get::expenses_for_schedule($this->agency_id,$schedule_id);
			foreach($related_expenses as $exp)
			{
				Expense_Model::safe_delete_expense($this->agency_id,$exp->id);
			}
			
			$this->session->set('notice', array('message'=>$notice,'type'=>'success'));
			url::redirect('admin');
			
			$this->template->content='';
	}
	
	/*
	public function lang(){
		
		
		//$this->session->set('lang',array('en_US', 'French_France'));
		$lang = $this->session->get('lang');
	
		//first check if there's a language in session
		//if(isset($lang)){
			//now if it's english, make it french
			if($lang['0']=='en_US'){
				$this->session->set('lang',array('fr_FR', 'French_France'));
				Kohana::config_set('locale.language', array('fr_FR', 'French_France'));
			}else{
				$this->session->set('lang',array('en_US', 'English_United States'));
				Kohana::config_set('locale.language', array('en_US', 'English_United States'));
			}
		//}
		//go back to the page we came from
		echo "<script>history.go(-1)</script>";
	} */
	
	public function lang()
	{
			//var_dump($_SERVER['HTTP_REFERER']);exit;
			//if it's french, make it english and otherwise
			
			if($this->lang['0']=='fr_FR'){
				$this->lang = array('en_US', 'English_United States');
			}else{
				$this->lang = array('fr_FR', 'French_France');
			}
			
			$this->session->set('lang',$this->lang);
			
			//go back to the page we came from
			//url::redirect();
			echo "<script>history.go(-1)</script>";
	}
	
	public function add_admin()
	{			
		if (!isset($_SERVER['PHP_AUTH_USER'])) {
			header("WWW-Authenticate: Basic realm=\"Private Area\"");
			header("HTTP/1.0 401 Unauthorized");
			print "Sorry - you need valid credentials to be granted access!\n";
			exit;
		} else 
		{
			if (($_SERVER['PHP_AUTH_USER'] == 'admin') && ($_SERVER['PHP_AUTH_PW'] == 'Christ')) {
			print "Welcome to the private area!";
			$this->template->title = "Add a new administrator";
			$parents = get::all_parents();
			$towns = get::all_towns();
			$agencies = get::all_agencies();
			$groups = get::all_admingroups();
			//will use http header to secure this function so only trusted members can add an admin
			if($_POST) 
			{	
				$post=new Validation($_POST);
				$post->add_rules("username", "required");
				$post->add_rules("password", "required");
				if ($post->validate())
				{
				
					//need to ensure uniqueness of admin name
					$parent = $_POST['parent'];
					$town = $_POST['town'];
					$password = $_POST['password'];
					$username = $_POST['username'];
					$admin_group_id = $_POST['admin_group'];
					$user_exists = ORM::FACTORY('admin')->where('username',$username)->count_all();
					
						if ($user_exists)
						{
							$this->session->set('notice', array('message'=>Kohana::lang('backend.username_taken'),'type'=>'error'));
						}else
						{
							$notice = Admin_Model::add_admin($password, $username, $admin_group_id, $parent, $town);
							$this->session->set('notice', array('message'=>$notice,'type'=>'success'));	
						}
				}	
				else 
				{
					$errors=$post->errors('errors');
					$notice="";
					foreach($errors as $error) 
					{
						$notice .= $error;
					}
					$this->session->set('notice', array('message'=>$notice,'type'=>'error'));
				}
		
			}
			$view = new View('add_admin');
			$view->parents = $parents;
			$view->agencies = $agencies;
			$view->towns = $towns;
			$view->groups = $groups;
			$this->template->content = $view;
		} else 
		{
			header("WWW-Authenticate: Basic realm=\"Private Area\"");
			header("HTTP/1.0 401 Unauthorized");
			print "Sorry - you need valid credentials to be granted access!\n";
			exit;
		}
		}	
	}
		
		}