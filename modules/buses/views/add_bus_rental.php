<?php defined('SYSPATH') or die('No direct script access'); ?> 

 
	
	<div class="row-fluid">	
		
		<div class="span12 hero-unit prof-unit">
		
		<ul class="nav nav-tabs">
					<li > <a href="<?=url::site('buses/all')?>"><i class="icon-arrow-left"></i> <?=Kohana::lang('backend.back_buses')?></a></li>
		</ul>
		
		
		<div class="row-fluid">	
		
			<form action="<?=url::site('buses/add_bus_rental/'.$bus_number)?>" method="POST" name="theform" class="span8 offset2">
				<legend><?=Kohana::lang('backend.add_bus_rental')?></legend>
						
						<div class="">
							<div class="span4"><?=Kohana::lang('backend.bus_no')?>: </div>
							<div class="span8"><input type="text"  name="bus_no" value="<?=$bus_number?>" disabled></div>
						</div>
						
						<div class="">
							<div class="span4"><?=Kohana::lang('backend.rented_to')?><span class="red"> *</span>: </div>
							<div class="span8"><input type="text"  name="rented_to" required ></div>
						</div>
						
						<div class="" >
							<div class="span4"><?=Kohana::lang('backend.from')?>: </div>
								<div class="input-append date span8">
									<input type="text" name = 'start_date' class="span5 datepicker"  value="<?=date('d-m-Y')?>" id="dp1" required>
									<span class="add-on"><i class="icon-calendar"></i></span>
								</div>
						</div>
						
						<div class="" >
							<div class="span4"><?=Kohana::lang('backend.to')?>: </div>
								<div class="input-append date span8">
									<input type="text" name = 'end_date' class="span5 datepicker"  value="<?=date('d-m-Y')?>" id="dp1" required>
									<span class="add-on"><i class="icon-calendar"></i></span>
								</div>
						</div>
						
						
						
						<div class="">
							<div class="span4"><?=Kohana::lang('backend.amount')?>: <span class="red"> *</span></div>
							<div class="span8 controls input-append"><input class="span5 IsAmount" type="text"  name="amount" required><span class="add-on"> FCFA</span></div>
						</div>
						
						<div class="">
											<div class="span4"><?=Kohana::lang('backend.description')?>: </div>
											<div class="span8"><textarea placeholder="<?=Kohana::lang('backend.description')?>"  name="description"></textarea></div>
						</div>
								
						
					
						<div class="clear"></div>
						
						<div class="form-actions">
							<div class="span4"></div>
							<div class="span8"><button class="btn btn-info " type="submit"><?=Kohana::lang('backend.register')?></button></div>
						</div>
						
									
						
				</form>
		
		
			
			<div class="span6">
			
				
				
			</div>	
			
				
				
		</div>
	
		
		</div>
	
    </div> 

	
	