<?php defined('SYSPATH') or die('No direct script access');

class Admin_Dashboard_Item_Model extends ORM{


	
	public function get_all($admin_name)
	{
		$items=ORM::factory('admin_dashboard_item')->where('AdminID',$admin_name)->find_all();
		
		return $items;	
	}
	
	public function admin_is_allowed_dashboard_item($admin_name, $dashboard_item_id)
	{
		$admin_dashboard_item=ORM::factory('admin_dashboard_item')->where('AdminID',$admin_name)->where('DashboardItemID',$dashboard_item_id)->find();
		return $admin_dashboard_item->IsAllowed;	
	}
	
	public function get_dashboard_item($admin_name, $dashboard_item_id)
	{
		$admin_dashboard_item=ORM::factory('admin_dashboard_item')->where('AdminID',$admin_name)->where('DashboardItemID',$dashboard_item_id)->find();
		return $admin_dashboard_item;	
	}
	
	public function update_admin_dashboard_item($admin_name, $item_id, $value, $restricted_town_id)
	{	
		$item = ORM::FACTORY('admin_dashboard_item')->where('DashboardItemID',$item_id)->where('AdminID',$admin_name)->find();
		$item->IsAllowed = $value;
		$item->RestrictedTownID = $restricted_town_id;
		$item->save();
	}	
	
	public function add_dashboard_items($admin_name, $dashboard_item_id, $value)
	{	
		$admin_dashboard_item = ORM::FACTORY('admin_dashboard_item');
		$admin_dashboard_item->AdminID = $admin_name;
		$admin_dashboard_item->DashboardItemID = $dashboard_item_id;
		$admin_dashboard_item->IsAllowed = $value;
		$admin_dashboard_item->save();
	}
}
