<?php defined('SYSPATH') or die('No direct script access');

class Payment_Controller extends Home_Controller {
	
	
	public function details($purchase_id) 
	{	
		$user=User_Authlite::instance()->get_user();
		$this->template->title = 'Payment';
		
		
		
		
		//If it's an open ticket, the query to get details is different, as the join for normal tickets won't work here, since it's based on schedule id
		//which open tickets don't have
		$is_open_ticket = Purchase_Model::is_open_ticket($purchase_id);
		if($is_open_ticket)
		{
			$purchase_details = Purchase_Model::get_full_ot_details($purchase_id);
		}else
		{
			$purchase_details = Purchase_Model::get_full_details($purchase_id);
		}
		$view=new View ('payment_details');
		$view->purchase = $purchase_details;
		$view->is_open_ticket = $is_open_ticket;
		
		//$view->towns = $towns;
		$this->template->content=$view;
	}
	
	public function complete_payment() 
	{	
		//if the submit is coming from the reserve seat form
				if($_POST)
				{
					//no_csrf::check(3);
					$post = new Validation($_POST);
					$post->add_rules('purchase_id','required');
					$post->add_rules('amount_payable','required');
					//$post->add_rules('res_phone','valid::digit','length[9]');
					//$post->add_rules('res_idc','valid::digit');
					//$post->add_rules('res_seat','required');
					
					if ($post->validate())
					{	
						//print_r($reserve->seat_occupants);exit;
						$purchase_id = $_POST['purchase_id'];
						$amount_payable = $_POST['amount_payable'];
						 
						 
						//To complete payment, mark the purchase 'active' and add the amount paid
						Purchase_Model::mark_active($purchase_id,$amount_payable);	
						
						$this->session->set('notice', array('message'=>'Payment confirmed. Kindly present yourself at the agency with your ID card only.','type'=>'success'));
						url::redirect('home');	
					}
					else
					{
						$errors=$post->errors('errors');
						$notice="";
							foreach($errors as $error) 
							{
								$notice.=$error."<br />";
							}
							$this->session->set('post', $_POST);
							$this->session->set('notice', array('message'=>$notice,'type'=>'error'));
							url::redirect('payment/details/'.$purchase_id);	
					}		
	
		
	}}
	
	
	/*
	public function logout() 
	{
		User_Authlite::instance()->logout(true);
		url::redirect('home');
	} */
	
	/*
	public function signup()
	{
		
		$this->template->title = "Signup";
		//will use http header to secure this function so only trusted members can add an admin
		if($_POST) 
		{	
			$post=new Validation($_POST);
			$post->add_rules("name", "required");
			$post->add_rules("email", "required");
			$post->add_rules("password", "required");
			if ($post->validate())
			{
				
				//need to ensure uniqueness of admin name
				$name = $_POST['name'];
				$email = $_POST['email'];
				$password = $_POST['password'];
				//$username = $_POST['username'];
				//$admin_group_id = $_POST['admin_group'];
				$email_exists = ORM::FACTORY('user')->where('email',$email)->count_all();
				
					if ($email_exists){
						$this->session->set('notice', array('message'=>'This email is taken. Try another.','type'=>'error'));
					}else{
						$notice = User_Model::add_user($name, $email, $password);
						$this->session->set('notice', array('message'=>'Welcome '.$name.', please confirm your email address by simply clicking the link that has been sent to your mail box.','type'=>'success'));
						//then log him in at once
						$user = User_Authlite::instance()->login($email, $password);
						}

			}	
			else 
			{
                $errors=$post->errors('errors');
                $notice="";
                foreach($errors as $error) 
				{
                    $notice .= $error;
                }
				$this->session->set('notice', array('message'=>$notice,'type'=>'error'));
            }
		
		}
		$view = new View('home');
		//$view->parents = $parents;
		//$view->agencies = $agencies;
		$view->agencies = get::all_agencies();
		$view->towns = get::all_towns();
		//$view->groups = $groups;
		$this->template->content = $view;
	
	
	}
		
	function search_schedule()
	{
		$this->template->title='Search for a schedule';
		if($_GET)
		{
			$post = new Validation($_GET);

				if ($post->validate())
				{	
					$from = $_GET['from'];
					$to = $_GET['to'];
					$departure_time = $_GET['departure_time'];
					$departure_date = date("Y-m-d",strtotime($_GET['departure_date']));
					
					//ensure origin and destination are not the same
					if ($from == $to)
					{
						$this->session->set('notice', array('message'=>'The origin and destination cannot be the same','type'=>'error'));
						url::redirect('home');
					}
					else
					{	
						if ($departure_time=='any'){
							//if user selects any time, search only by origin and destination and ensure departed buses don't come up
							$results = ORM::factory('schedule')->where('from',$from)->where('to',$to)->where('departure_date',$departure_date)->where('status','current')->find_all();
						}else{
							$results = ORM::factory('schedule')->where('from',$from)->where('to',$to)->where('departure_time',$departure_time)->where('departure_date',$departure_date)->where('status','current')->find_all();
						}
						
					}			
				}
				else
				{
					$errors=$post->errors('errors');
					$notice="";
						foreach($errors as $error) 
						{
							$notice.=$error."<br />";
						}
						$this->session->set('notice', array('message'=>$notice,'type'=>'error'));
						//url::redirect('admin/main/'.$this->admin->agency_id);
				}
		}
		$view = new View('search');
		$view->town_from = $from;
		$view->town_to = $to;
		$view->open_ticket_agencies = Agency_Model::get_open_ticket_agencies_from_town($from);
		
		$view->results = $results;
		$this->template->content = $view;
		
	}
	
	function schedule_details($schedule_id)
	{	
		$this->session->set('last_visited_uri',$this->uri->string());
		$this->template->title = "Particular schedule";
		$schedule = ORM::factory('schedule')->where('id',$schedule_id)->find();
		
		$seats_and_occupants = json_decode($schedule->seat_occupants,true);
		$seats_and_reservers = json_decode($schedule->reserved_seats,true); //true converts the object to an array

		if($_POST)
		{	
			$post = new Validation($_POST);
			$post->add_rules('res_name','required');
			$post->add_rules('res_phone','required');
			$post->add_rules('res_seat','required');
			
			
			
			if ($post->validate())
			{	
				
				$client_name = $_POST['res_name'];
				$client_phone = $_POST['res_phone'];
				$client_email = $_POST['res_email'];
				$client_seat = $_POST['res_seat'];
				$client_idc = $_POST['res_idc'];
				
				if(isset($_POST['reserve_seat']))
				{	
					$client_reservations = get::client_reservations($schedule_id);
					//first check if the reservation_request in the db is not empty. If it is, create the array. If not, decode as array and populate			
					if (empty ($client_reservations->reserved_seats))
					{
						$reservation_request = array();			
					}
					else
					{
						$reservation_request = json_decode($client_reservations->reserved_seats,true); //true converts the object to an array
					}
					//on the reservation_request table, we enter the reserver's details in a multidimensional array
					//in the second level of the array, 1->reserver's name, 2->reserver's phone, 3->reserver's email
					
					//we need to get information about this schedule to know if the seat is occupied or not.
					//This is to prevent the error when a client reserves a seat, and hence it's not yet allocated to him, so the seat shows at
					//the frontend. then it's confirmed at the backend. 
					//they should not be able to reserve if the seat was given out, but they didn't refresh hence still see it as available
					$schedule=get::schedule($schedule_id);
					if (empty ($schedule->seat_occupants))
						{
							$seats_and_occupants = array();			
						}
						else
						{
							$seats_and_occupants = json_decode($schedule->seat_occupants,true);
						}
					if(isset($seats_and_occupants[$client_seat])){
						//echo"<script> alert('Sorry, seat $client_seat has been taken. Please select another');</script>";
						$this->session->set('notice', array('message'=>"Sorry, seat $client_seat has been taken. Please select another",'type'=>'error'));
						url::redirect('home/schedule_details/'.$schedule_id);
					
					}
					
					$reservation_request[$client_seat] = array('name'=>$client_name,'phone'=>$client_phone,'email'=>$client_email,'idc'=>$client_idc);
					//print_R($reservation_request);exit;
					$client_reservations->reserved_seats = json_encode($reservation_request);
					$client_reservations->schedule_id = $schedule_id;
					$client_reservations->save();
					
					$this->session->set('notice', array('message'=>"Your request has been made. An admin will verify and you will be notified",'type'=>'success'));
					url::redirect('home/schedule_details/'.$schedule_id);	
				}
				if(isset($_POST['purchase_ticket']))
				{	
					Purchase_Model::add_purchase($schedule->agency_id, $schedule_id, $client_name, $client_phone, $amount_paid=$schedule->ticket_price);
					$this->session->set('notice', array('message'=>"Purchase confirmed",'type'=>'success'));
					url::redirect('home/schedule_details/'.$schedule_id);
				}
			
			
			}else
			{
				$errors=$post->errors('errors');
				$notice="";
					foreach($errors as $error) 
					{
						$notice.=$error."<br />";
					}
					$this->session->set('notice', array('message'=>$notice,'type'=>'error'));
					url::redirect('home/schedule_details/'.$schedule_id);	
			}
		}
		$view = new View('schedule_details');
		$view->schedule = $schedule;
		$view->seats_and_occupants = $seats_and_occupants;
		$view->seats_and_reservers = $seats_and_reservers;
		$this->template->content = $view;
	} */
	
	function purchase_open_ticket()
	{	//$this->session->set('last_visited_uri',$this->uri->string());
		
		$this->session->set('last_visited_uri',$this->uri->string());
		//User_Authlite::check_login();
		
		$this->template->title = "Purchase open ticket";
		
		if($_POST)
		{	
			$post = new Validation($_POST);
			$post->add_rules('client_name','required');
			$post->add_rules('client_phone','required');
			
			if ($post->validate())
			{	
				
				$client_name = $_POST['client_name'];
				$client_phone = $_POST['client_phone'];
				$agency_from = $_POST['agency_from'];
				
				if(isset($_POST['purchase_ticket']))
				{	
					Purchase_Model::add_purchase($agency_from, $schedule_id=0, $client_name, $client_phone, $amount_paid='');
					$this->session->set('notice', array('message'=>"Purchase confirmed",'type'=>'success'));
					url::redirect('home/schedule_details/'.$schedule_id);
				}
			
			
			}else
			{
				$errors=$post->errors('errors');
				$notice="";
					foreach($errors as $error) 
					{
						$notice.=$error."<br />";
					}
					$this->session->set('notice', array('message'=>$notice,'type'=>'error'));
					url::redirect('home/purchase_open_ticket');	
			}
		}
		$view = new View('search');
		//$view->schedule = $schedule;
		//$view->seats_and_occupants = $seats_and_occupants;
		//$view->seats_and_reservers = $seats_and_reservers;
		$this->template->content = $view;
	}
	
	function reserve_seat($schedule_id)
	{
		$this->template->title = "Purchase ticket";
		
		if($_POST)
		{	
			$client_reservations = get::client_reservations($schedule_id);
			//first check if the reservation_request in the db is not empty. If it is, create the array. If not, decode as array and populate			
			if (empty ($client_reservations->reserved_seats))
			{
				$reservation_request = array();			
			}
			else
			{
				$reservation_request = json_decode($client_reservations->reserved_seats,true); //true converts the object to an array
			}
		
			$post = new Validation($_POST);
			$post->add_rules('res_name','required');
			$post->add_rules('res_phone','required');
			$post->add_rules('res_seat','required');
			
			if ($post->validate())
			{	
				$res_name = $_POST['res_name'];
				$res_phone = $_POST['res_phone'];
				$res_email = $_POST['res_email'];
				$res_seat = $_POST['res_seat'];
				$res_idc = $_POST['res_idc'];
				
				if(isset($_POST['reserve_seat']))
				{
					//on the reservation_request table, we enter the reserver's details in a multidimensional array
					//in the second level of the array, 1->reserver's name, 2->reserver's phone, 3->reserver's email
					
					//we need to get information about this schedule to know if the seat is occupied or not.
					//This is to prevent the error when a client reserves a seat, and hence it's not yet allocated to him, so the seat shows at
					//the frontend. then it's confirmed at the backend. 
					//they should not be able to reserve if the seat was given out, but they didn't refresh hence still see it as available
					$schedule=get::schedule($schedule_id);
					if (empty ($schedule->seat_occupants))
						{
							$seats_and_occupants = array();			
						}
						else
						{
							$seats_and_occupants = json_decode($schedule->seat_occupants,true);
						}
					if(isset($seats_and_occupants[$res_seat])){
						//echo"<script> alert('Sorry, seat $res_seat has been taken. Please select another');</script>";
						$this->session->set('notice', array('message'=>"Sorry, seat $res_seat has been taken. Please select another",'type'=>'error'));
						url::redirect('home/schedule_details/'.$schedule_id);
					
					}
					
					$reservation_request[$res_seat] = array('name'=>$res_name,'phone'=>$res_phone,'email'=>$res_email,'idc'=>$res_idc);
					//print_R($reservation_request);exit;
					$client_reservations->reserved_seats = json_encode($reservation_request);
					$client_reservations->schedule_id = $schedule_id;
					$client_reservations->save();
					
					$this->session->set('notice', array('message'=>"Your request has been made. An admin will verify and you will be notified",'type'=>'success'));
					url::redirect('home/schedule_details/'.$schedule_id);	
				}
				if(isset($_POST['purchase_ticket']))
				{	
					Purchase_Model::add_purchase($agency_id, $schedule_id, $client_name, $client_phone, $amount_paid);
					url::redirect('home/schedule_details/'.$schedule_id);
				}
			else
			{
				$errors=$post->errors('errors');
				$notice="";
					foreach($errors as $error) 
					{
						$notice.=$error."<br />";
					}
					$this->session->set('notice', array('message'=>$notice,'type'=>'error'));
					url::redirect('home/schedule_details/'.$schedule_id);	
			}
			
			}
		}
		$view = new View('schedule_details');
		//$view->schedules = $schedules;
		$this->template->content = $view;
		
	}
	
	function add_purchase()
	{
		
		if($_POST)
		{	
									
					$post = new Validation($_POST);
					$post->add_rules('res_name','required');
					$post->add_rules('res_phone','required');
					$post->add_rules('res_seat','required');
					
					if ($post->validate())
					{	
						//print_r($reserve->seat_occupants);exit;
						$res_name = $_POST['res_name'];
						$res_phone = $_POST['res_phone'];
						$res_email = $_POST['res_email'];
						$res_seat = $_POST['res_seat'];
						$res_idc = $_POST['res_idc'];

						Purchase_Model::add_purchase($agency_id, $schedule_id, $client_name, $client_phone, $amount_paid);
						
						$this->session->set('notice', array('message'=>"Purchase confirmed.",'type'=>'success'));
						url::redirect('home/schedule_details/'.$schedule_id);	
									
					}
					else
					{
						$errors=$post->errors('errors');
						$notice="";
						foreach($errors as $error) 
						{
							$notice.=$error."<br />";
						}
						$this->session->set('notice', array('message'=>$notice,'type'=>'error'));
						url::redirect('home/schedule_details/'.$schedule_id);	
					}
				
				
		}
		
	}
	
	
	
	}
	

	
