<?php defined('SYSPATH') or die('No direct script access');

class Parcels_Controller extends Admin_Controller 
{
	public function __construct()
	{	
		parent::__construct();
		$this->admin = Authlite::instance()->get_admin();
		$this->session = Session::instance();
		//if (!empty($this->admin)) $this->key = $this->admin->agency_id;
		
	}  

	
	public function index()
	{	
		//ensure a logged in user can't land on the signup page
		if (empty($this->admin)) 
		{
			url::redirect('admin/login');
		}
	}
	
	
	public function register_parcel()
	{	
		Authlite::check_admin();
		Authlite::verify_referer();
		//Authlite::check_agency_integrity($agency_id);
		$this->template->title = '';
		$agency_id = $this->agency_id;
		$agency_name = get::agency_name($agency_id);
		
		if($_POST)
		{	
			//var_dump($_POST);EXIT;
			//no_csrf::check();
			$post = new Validation($_POST);
			$post->add_rules('receiver_name','required');
			$post->add_rules('agency_to','required');
			$post->add_rules('receiver_phone','valid::digit','length[9]');
			$post->add_rules('parcel_description','required');
			$post->add_rules('price','required','valid::alpha_numeric');
			
			if ($post->validate())
			{	
				
				$type = $_POST['parcel_type'];
				$sender_name = $_POST['sender_name'];
				$receiver_name = $_POST['receiver_name'];
				$receiver_phone = $_POST['receiver_phone'];
				$description = htmlentities($_POST['parcel_description'],ENT_QUOTES);
				$to = $_POST['agency_to'];
				$price = $_POST['price'];
				
				Parcel_Model::add_parcel($type, $sender_name, $receiver_name, $receiver_phone, $description, $receiver_name, $agency_id, $to, $price, $this->admin->username);
						
					
					//send confirmation SMS			
					$msg = Kohana::lang('backend.hello').$receiver_name.". ".Kohana::lang('backend.parcel_deposited').$agency_name.". " .Kohana::lang('backend.inform_you');
					$to = "237".$receiver_phone;
					//$to = "23796362464";
					
					//send_sms::index();
					//this doesn't keep track of SMS dispatcching. Does direct sending by calling WAsamundi API. No records. Works perfectly though.
					//$result = sms::send($msg,$to,$this->agency_id);
					
					//this keeps records.
					
					//if a phone number is set, save to db for SMS to be sent
					if(!empty($receiver_phone))
					{	
						$result = sms::save($msg,$to,$this->agency_id);
					}
					
					$this->session->set('notice', array('message'=>Kohana::lang('backend.parcel_registered')." (<a href='". url::site('parcels/all/sent'). "'>". Kohana::lang('backend.all_parcels')."</a>). <br/> "  .@$result ,'type'=>'success'));
					url::redirect('parcels/all/sent');	
							
			}
			else
			{
				$errors=$post->errors('errors');
				$notice="";
					foreach($errors as $error) 
					{
						$notice.=$error."<br />";
					}
					$this->session->set('notice', array('message'=>$notice,'type'=>'error'));
					//set the user's post in session so he doesn't re-enter stuff if some is wrong
					$this->session->set('post', $_POST);
					url::redirect('parcels/register_parcel');
			}
		}
		
		
		$view = new View('admin_parcels');
		$is_allowed_manage_mails = Admin_Module_Model::admin_is_allowed_manage_mails($this->admin->username);
		$is_allowed_manage_baggage = Admin_Module_Model::admin_is_allowed_manage_baggage($this->admin->username);
		$view->is_allowed_manage_mails = $is_allowed_manage_mails;
		$view->is_allowed_manage_baggage = $is_allowed_manage_baggage;
		$parent = get::_parent($agency_id);
		$siblings = get::agency_siblings($agency_id,$parent);	
		
		$view->siblings = $siblings;
		$this->template->content=$view;
	
	}
	
	public function edit($parcel_id)
	{	
		Authlite::check_admin();
		Authlite::verify_referer();
		//Authlite::check_agency_integrity($agency_id);
		$this->template->title = 'Edit parcel';
		$agency_id = $this->agency_id;
		$agency_name = get::agency_name($agency_id);
		
		if($_POST)
		{	
			//var_dump($_POST);EXIT;
			//no_csrf::check();
			$post = new Validation($_POST);
			$post->add_rules('receiver_name','required');
			$post->add_rules('agency_to','required');
			$post->add_rules('receiver_phone','valid::digit','length[9]');
			$post->add_rules('parcel_description','required');
			$post->add_rules('price','required','valid::alpha_numeric');
			
			if ($post->validate())
			{	
				
				$type = $_POST['parcel_type'];
				$sender_name = $_POST['sender_name'];
				$receiver_name = $_POST['receiver_name'];
				$receiver_phone = $_POST['receiver_phone'];
				$description = htmlentities($_POST['parcel_description'],ENT_QUOTES);
				$to = $_POST['agency_to'];
				$price = $_POST['price'];
				
				Parcel_Model::edit_parcel($parcel_id, $type, $sender_name, $receiver_name, $receiver_phone, $description, $receiver_name, $agency_id, $to, $price, $this->admin->username);
						
					
					//send confirmation SMS			
					//$msg = Kohana::lang('backend.hello').$receiver_name.". ".Kohana::lang('backend.parcel_deposited').$agency_name.". " .Kohana::lang('backend.inform_you');
					//$to = "237".$receiver_phone;
					//$to = "23796362464";
					
					//send_sms::index();
					//this doesn't keep track of SMS dispatcching. Does direct sending by calling WAsamundi API. No records. Works perfectly though.
					//$result = sms::send($msg,$to,$this->agency_id);
					
					//this keeps records.
					
					//if a phone number is set, save to db for SMS to be sent
					/*if(!empty($receiver_phone))
					{	
						$result = sms::save($msg,$to,$this->agency_id);
					} */
					
					$this->session->set('notice', array('message'=>Kohana::lang('backend.parcel_modified')." (<a href='". url::site('parcels/all/sent'). "'>". Kohana::lang('backend.all_parcels')."</a>). <br/> "  .@$result ,'type'=>'success'));
					url::redirect('parcels/all/sent');	
							
			}
			else
			{
				$errors=$post->errors('errors');
				$notice="";
					foreach($errors as $error) 
					{
						$notice.=$error."<br />";
					}
					$this->session->set('notice', array('message'=>$notice,'type'=>'error'));
					//set the user's post in session so he doesn't re-enter stuff if some is wrong
					$this->session->set('post', $_POST);
					url::redirect('parcels/edit_parcel/'.$parcel_id);
			}
		}
		
		
		$view = new View('admin_edit_parcel');
		$is_allowed_manage_mails = Admin_Module_Model::admin_is_allowed_manage_mails($this->admin->username);
		$is_allowed_manage_baggage = Admin_Module_Model::admin_is_allowed_manage_baggage($this->admin->username);
		$view->is_allowed_manage_mails = $is_allowed_manage_mails;
		$view->is_allowed_manage_baggage = $is_allowed_manage_baggage;
		$parent = get::_parent($agency_id);
		$siblings = get::agency_siblings($agency_id,$parent);	
		
		$view->parcel = Parcel_Model::get($agency_id,$parcel_id);
		$view->siblings = $siblings;
		$this->template->content=$view;
	
	}
	
	
	//type is either sent or incoming
	public function all($type="sent") 
	{	
		Authlite::check_admin();
		Authlite::verify_referer();
		
		$agency_id = $this->agency_id;
		
		//pagination
		$per_page = 15;
		
		$outgoing= Parcel_Model::get_all_agency_outgoing_parcels($agency_id);
		
		$incoming= get::all_agency_incoming_parcels($agency_id);
				
		$sent_total = count($outgoing);
		$incoming_total = count($incoming);
		
		$this->sent_pagination = new Pagination(array(
			// 'base_url'    => 'welcome/pagination_example/page/', // base_url will default to current uri
			'uri_segment'    => 'page', // pass a string as uri_segment to trigger former 'label' functionality
			'total_items'    => $sent_total, // use db count query here of course
			'items_per_page' => $per_page , // it may be handy to set defaults for stuff like this in config/pagination.php
			'style'          => 'punbb', // pick one from: classic (default), digg, extended, punbb, or add your own!
			//'query_string'   => "ORM::factory('schedule')->where('agency_id',$agency_id)->where('status','current')->find_all()"
		));$this->incoming_pagination = new Pagination(array(
			// 'base_url'    => 'welcome/pagination_example/page/', // base_url will default to current uri
			'uri_segment'    => 'page', // pass a string as uri_segment to trigger former 'label' functionality
			'total_items'    => $incoming_total, // use db count query here of course
			'items_per_page' => $per_page , // it may be handy to set defaults for stuff like this in config/pagination.php
			'style'          => 'punbb', // pick one from: classic (default), digg, extended, punbb, or add your own!
			//'query_string'   => "ORM::factory('schedule')->where('agency_id',$agency_id)->where('status','current')->find_all()"
		));
		
		
		$this->template->title = Kohana::lang('backend.welcome_backend');
		//If not specified, shoe incoming parcels
		
			//$parcels = get::all_agency_sent_parcels($agency_id);
			//$sent_parcels=ORM::factory('parcel')->where('from',$agency_id)->orderby('sent_date','DESC')->limit($per_page,$this->sent_pagination->sql_offset)->find_all();
			$sent_parcels=$this->db->select('parcels.id, schedule_id, receiver_name, receiver_phone, description, state, sent_date,registered_date, to, towns.name AS town_name_to, from, CreatedBy, CreatedOn')->from('parcels')->join('agencies','agencies.id = parcels.to')->join('towns','towns.id = agencies.town_id')->where('from',$agency_id)->orderby('registered_date','DESC')->limit($per_page,$this->sent_pagination->sql_offset)->get();
		
			//$parcels = get::all_agency_incoming_parcels($agency_id);
			//$incoming_parcels=ORM::factory('incoming_parcel')->where('to',$agency_id)->orderby('sent_date','DESC')->limit($per_page,$this->incoming_pagination->sql_offset)->find_all();
			$incoming_parcels=$this->db->select('incoming_parcels.id, bus_number, schedule_id, receiver_name, receiver_phone, description, state, sent_date, to, from, global_parcel_id,towns.name as town_name_from')->from('incoming_parcels')->join('agencies','agencies.id = incoming_parcels.from')->join('towns','towns.id = agencies.town_id')->where('to',$agency_id)->orderby('sent_date','DESC')->limit($per_page,$this->incoming_pagination->sql_offset)->get();
			
		$current_schedules=Schedule_Model::get_all_agency_current_schedules($agency_id);
		
		$view = new View('admin_parcels');
		$view->sent_parcels = $sent_parcels;
		
		$view->current_schedules = $current_schedules;
		
		$view->incoming_parcels = $incoming_parcels;
		
		$this->template->content = $view;

	}
	
	public function receive($parcel_id)
	{	
		Authlite::check_admin();
		Authlite::verify_referer();
		$agency_name = get::agency_name($this->agency_id);
		$this->template->title = '';
		#$parcel = get::parcel($parcel_id);
		$parcel = get::incoming_parcel($parcel_id);
		$parcel->state = 'received';
		
		//send confirmation SMS			
		$msg = Kohana::lang('backend.hello').$parcel->receiver_name.". ".Kohana::lang('backend.parcel_available').$agency_name.". ".Kohana::lang('backend.collect_it');
		$to = "237".$parcel->receiver_phone;
		//$to = "23796362464";
		
		//Send tries to send automatically, and will fail unretrievable in absence of connection
		//$result = sms::send($msg,$to,$this->agency_id);
		
		//save saves to offline db, pushes to online db, then tries to send.
		//first check it's not a parcel without phone number
		if(!empty($parcel->receiver_phone))
		{
		$result = sms::save($msg,$to,$this->agency_id);
		}
		
		$this->session->set('notice', array('message'=>Kohana::lang('backend.parcel_received')."<br/> Notice: "  .@$result ,'type'=>'success'));			
		//save here coz when done above, the SMS isn't sent due to the redirect immediately after
		$parcel->save();
		url::redirect('parcels/all/incoming/'.$this->admin->agency_id);
	}	
	
	
	public function deliver($parcel_id)
	{	
		Authlite::check_admin();
		Authlite::verify_referer();
		$this->template->title = '';
		#$parcel = get::parcel($parcel_id);
		$parcel = get::incoming_parcel($parcel_id);
	
		if($_POST)
		{
			$post = new Validation($_POST);
			if ($post->validate())
			{	
				$collector_name = $_POST['collector_name'];
				$collector_idc = $_POST['collector_idc'];
				$collector_phone = $_POST['collector_phone'];
				
				$collector_info = array('name'=>$collector_name, 'idc'=>$collector_idc, 'phone'=>$collector_phone);
				
				//json encode and save
				$parcel->collected_by = json_encode($collector_info);
				$parcel->collected_on = date('Y-m-d H:i:s');
				$parcel->state = 'delivered';
				$parcel->save();
					
					$this->session->set('notice', array('message'=>Kohana::lang('backend.marked_delivered'),'type'=>'success'));
					url::redirect('parcels/all/incoming/'.$this->admin->agency_id);

					//if required, send email to notify sender that parcel has been delivered.
					//Hence the parcel needs some variable set at the sending agency if that's the case
			}else
			{
				$errors=$post->errors('errors');
				$notice="";
				foreach($errors as $error) 
				{
					$notice.=$error."<br />";
				}
				$this->session->set('notice', array('message'=>$notice,'type'=>'error'));
			}
		}
		
		$view = new View('admin_deliver_parcel');
		$view->parcel = $parcel;
		$this->template->content = $view;
	}
	
	
	public function view($parcel_id)
	{	
		Authlite::check_admin();
		Authlite::verify_referer();
		
		$this->template->title = '';
		
		$parcel = get::parcel($parcel_id);
	
		if($_POST)
		{
			$post = new Validation($_POST);
			if ($post->validate())
			{	
				$collector_name = $_POST['collector_name'];
				$collector_idc = $_POST['collector_idc'];
				$collector_phone = $_POST['collector_phone'];
				
				$collector_info = array('name'=>$collector_name, 'idc'=>$collector_idc, 'phone'=>$collector_phone);
				
				//json encode and save
				$parcel->collected_by = json_encode($collector_info);
				$parcel->collected_on = date('Y-m-d H:i:s');
				$parcel->state = 'delivered';
				$parcel->save();
				
				$this->session->set('notice', array('message'=>Kohana::lang('backend.marked_delivered'),'type'=>'success'));
				url::redirect('parcels/all/incoming/'.$this->admin->agency_id);

				//if required, send email to notify sender that parcel has been delivered.
				//Hence the parcel needs some variable set at the sending agency if that's the case
			}else
			{
				$errors=$post->errors('errors');
				$notice="";
				foreach($errors as $error) 
				{
					$notice.=$error."<br />";
				}
				$this->session->set('notice', array('message'=>$notice,'type'=>'error'));
			}
		}
		
		$view = new View('admin_view_parcel');
		$view->parcel = $parcel;
		$this->template->content = $view;
	}
	
	
	public function view_incoming($parcel_id)
	{	
		Authlite::check_admin();
		Authlite::verify_referer();
		$this->template->title = '';
		
		$parcel = get::incoming_parcel($parcel_id);
	
		if($_POST)
		{
			$post = new Validation($_POST);
			if ($post->validate())
			{	
				$collector_name = $_POST['collector_name'];
				$collector_idc = $_POST['collector_idc'];
				$collector_phone = $_POST['collector_phone'];
				
				$collector_info = array('name'=>$collector_name, 'idc'=>$collector_idc, 'phone'=>$collector_phone);
				
				//json encode and save
				$parcel->collected_by = json_encode($collector_info);
				$parcel->collected_on = date('Y-m-d H:i:s');
				$parcel->state = 'delivered';
				$parcel->save();
					
					$this->session->set('notice', array('message'=>Kohana::lang('backend.marked_delivered'),'type'=>'success'));
					url::redirect('parcels/all/incoming/'.$this->admin->agency_id);

					//if required, send email to notify sender that parcel has been delivered.
					//Hence the parcel needs some variable set at the sending agency if that's the case
			}else
			{
				$errors=$post->errors('errors');
				$notice="";
				foreach($errors as $error) 
				{
					$notice.=$error."<br />";
				}
				$this->session->set('notice', array('message'=>$notice,'type'=>'error'));
			}
		}
		
		$view = new View('admin_view_incoming_parcel');
		$view->parcel = $parcel;
		//Set this variable to the view for incoming parcels, so that the print option is not displayed.
		$view->is_incoming = 1;
		$this->template->content = $view;
	}
	
	public function print_ticket()
	{	
		Authlite::check_admin();
		Authlite::verify_referer();
		$this->template->title = '';
		$agency_id=$this->agency_id;
		
	
		if($_POST)
		{
			$post = new Validation($_POST);
			if ($post->validate())
			{	
				$parcel_id = $_POST['parcel_id'];
				$parcel = get::parcel($parcel_id);
				$from = get::agency_town_name($parcel->from);
				$to = get::agency_town_name($parcel->to);
				$sender_name = $parcel->sender_name;
				$receiver_name = $parcel->receiver_name;
				//$parent = get::_parent_name($agency_id);
				//var_dump($parent);
				$price = number_format($parcel->price);
				$receiver_phone = $parcel->receiver_phone;
				if($receiver_phone=='0'){$receiver_phone = 'N/A';}
				$created_by = $parcel->CreatedBy;
				$created_on = date("d-m-Y g:i A",strtotime($parcel->CreatedOn));;
				//var_dump($parcel);exit;
				
				$ticket = printer::print_parcel_ticket($agency_id, $parcel_id, $from, $to, $price, $receiver_name, $receiver_phone, $sender_name, $created_by, $created_on );	
				//var_dump($ticket);exit;
				
				//open the ticket in a new window
				echo "<script>
						win=window.open('$ticket','Print ticket','width=900,height=600');
						win.focus();
						
					  </script>
					  ";
				//DO NOT MODIFY THESE LINES. THEY DO THE REDIRECT ENSURING THAT PAGE REFRESH IS NOT POSSIBLE HERE, NOR THE BACK BUTTON  
				echo '<script>
						window.location = view/'.$parcel_id.'; 
				</script>'; 
					
				
			}else
			{
				$errors=$post->errors('errors');
				$notice="";
				foreach($errors as $error) 
				{
					$notice.=$error."<br />";
				}
				$this->session->set('notice', array('message'=>$notice,'type'=>'error'));
			}
		}
		
		$view = new View('admin_view_parcel');
		$view->parcel = $parcel;
		$this->template->content = $view;
	}
	
	public function check_parcels()
	{	
		Authlite::check_admin();
		Authlite::verify_referer();
		$agency_id = $this->agency_id;
		$all_buses = Bus_Model::get_all($agency_id);
	
		$this->template->title = Kohana::lang('backend.check_parcels');		
		
		$current_schedules=Schedule_Model::get_all_agency_current_schedules($agency_id);
			
		$view = new View('admin_parcels');
		$view->current_schedules = $current_schedules;
		$view->all_buses = $all_buses;
		$this->template->content = $view;
		
		if($_POST)
		{
			
			$post = new Validation($_POST);
			//bus number is whichever comes from the form, either the existing or the one to be searched
			//if it's the search, then it shouldn't be empty
			
			if(isset($_POST['by_date']))
			{
				$post->add_rules('date','required');
				$date = date("Y-m-d",strtotime($_POST['date']));	
				
				if ($post->validate())
				{		
					$sent_parcels = Parcel_Model::get_outgoing_parcels_by_date($agency_id,$date);
					$incoming_parcels = Incoming_Parcel_Model::get_incoming_parcels_by_date($agency_id,$date);
					$count_incoming = count($incoming_parcels);
					$count_sent = count($sent_parcels);
				}
				else
				{
					$errors=$post->errors('errors');
					$notice="";
					foreach($errors as $error) 
					{
						$notice.=$error."<br />";
					}
					$this->session->set('notice', array('message'=>$notice,'type'=>'error'));
					url::redirect('expenses');	
				}
				$view = new View('admin_check_parcels');
				$view->date = $date;
				$view->sent_parcels = $sent_parcels;
			
				$view->incoming_parcels = $incoming_parcels;
				$view->count_sent = $count_sent;
				$view->count_incoming = $count_incoming;
				$this->template->content = $view;
			}
			if(isset($_POST['by_bus_number']))
			{
				//$post->add_rules('date','required');
				
				$post->add_rules('start_date','required');
				$post->add_rules('end_date','required');
				$post->add_rules('bus_number','required');
				
				if ($post->validate())
				{		
			
					$bus_number = $_POST['bus_number'];	
					
					//Convert dates as such if not the SQL CAST will not work correctly.
					$start_date = date("Y-m-d",strtotime($_POST['start_date']));
					$end_date = date("Y-m-d",strtotime($_POST['end_date']));
					
					
					//$outgoing_parcels = Parcel_Model::get_outgoing_parcels_by_bus_number($agency_id,$bus_number);
					$outgoing_parcels = Parcel_Model::get_outgoing_parcels_by_bus_number_and_period($agency_id,$bus_number,$start_date,$end_date);
					//$incoming_parcels = Incoming_Parcel_Model::get_incoming_parcels_by_current_schedule($agency_id,$current_schedule_id);
					//$count_incoming = count($incoming_parcels);
					$count_outgoing = count($outgoing_parcels);
				}
				else
				{
					$errors=$post->errors('errors');
					$notice="";
					foreach($errors as $error) 
					{
						$notice.=$error."<br />";
					}
					$this->session->set('notice', array('message'=>$notice,'type'=>'error'));
					url::redirect('expenses');	
				}
				$view = new View('admin_parcels_by_bus_number');
				$view->bus_number = $bus_number;
				$view->count_outgoing = $count_outgoing;
				$view->outgoing_parcels = $outgoing_parcels;
				$view->start_date = $start_date;
				$view->end_date = $end_date;
			
				$view->all_buses = $all_buses;
				$this->template->content = $view;
			}
			if(isset($_POST['by_current_schedule']))
			{
				//$post->add_rules('date','required');
				$current_schedule_id = $_POST['current_schedule_id'];	
				
				
				if ($post->validate())
				{		
					$outgoing_parcels = Parcel_Model::get_outgoing_parcels_by_current_schedule($agency_id,$current_schedule_id);
					//$incoming_parcels = Incoming_Parcel_Model::get_incoming_parcels_by_current_schedule($agency_id,$current_schedule_id);
					//$count_incoming = count($incoming_parcels);
					$count_outgoing = count($outgoing_parcels);
				}
				else
				{
					$errors=$post->errors('errors');
					$notice="";
					foreach($errors as $error) 
					{
						$notice.=$error."<br />";
					}
					$this->session->set('notice', array('message'=>$notice,'type'=>'error'));
					url::redirect('expenses');	
				}
				$view = new View('admin_parcels_by_current_schedule');
				//$view->date = $date;
				$view->outgoing_parcels = $outgoing_parcels;
			
				$view->count_outgoing = $count_outgoing;
				$this->template->content = $view;
			}
			if(isset($_POST['by_schedule_id']))
			{
				//format of schedule ID is SCAAAXXXX
				//where AAA is agency_id and XXX is local scheduleID
				
				//first ensure the first 2 chars are SC and the next 3 chars must be the agency's id and total length is not less than 6 chars 
				if(strtoupper(substr($_POST['schedule_id'],0,2)) != 'SC'  OR substr($_POST['schedule_id'],2,3) != $this->agency_id OR strlen($_POST['schedule_id']) < 6)
				{
					$this->session->set('notice', array('message'=>'Invalid scheduleID','type'=>'error'));
					url::redirect('parcels/check_parcels');
				}
				//Start from position 5 as the first 2 chars are sc and the next 3 are the agency id
				$schedule_id = substr($_POST['schedule_id'],5);	
				//var_dump($schedule_id);exit;
				
				
				if ($post->validate())
				{		
					$outgoing_parcels = Parcel_Model::get_outgoing_parcels_by_schedule_id($agency_id,$schedule_id);
					//$incoming_parcels = Incoming_Parcel_Model::get_incoming_parcels_by_current_schedule($agency_id,$current_schedule_id);
					//$count_incoming = count($incoming_parcels);
					$count_outgoing = count($outgoing_parcels);
				}
				else
				{
					$errors=$post->errors('errors');
					$notice="";
					foreach($errors as $error) 
					{
						$notice.=$error."<br />";
					}
					$this->session->set('notice', array('message'=>$notice,'type'=>'error'));
					url::redirect('expenses');	
				}
				$view = new View('admin_parcels_by_schedule_id');
				//$view->date = $date;
				$view->outgoing_parcels = $outgoing_parcels;
			
				$view->count_outgoing = $count_outgoing;
				$this->template->content = $view;
			}
		}
			
		
	}
	
	public function add_to_schedule($parcel_id)
	{	
		Authlite::check_admin();
		Authlite::verify_referer();
		$agency_id = $this->agency_id;
		
		if($_POST)
		{	
			$post = new Validation($_POST);
			$post->add_rules('schedule_id','required');
			
			if ($post->validate())
			{		
				$schedule_id = $_POST['schedule_id'];
				//Make sure the parcel and schedule are bound for the same town
				$parcel = Parcel_Model::get($this->agency_id, $parcel_id);
				$schedule = Schedule_Model::get($schedule_id);
			
				$parcel_to_town = get::agency_town($parcel->to);
				$schedule_to_town = $schedule->to;
					
				if($parcel_to_town == $schedule_to_town)
				{	
					//We also add the bus number, though redundant, for easy tracking of the parcel at the receiving branch.
					Parcel_Model::add_to_schedule($parcel_id, $schedule_id, $schedule->bus_number);
					$this->session->set('notice', array('message'=>'Parcel added to schedule/bus','type'=>'success'));
				}else
				{
					$this->session->set('notice', array('message'=>'Parcel and schedule/bus must be going to the same town','type'=>'error'));
				}
			}
			else
			{
				$errors=$post->errors('errors');
				$notice="";
				foreach($errors as $error) 
				{
					$notice.=$error."<br />";
				}
				$this->session->set('notice', array('message'=>$notice,'type'=>'error'));
			}
			url::redirect('parcels/all/sent');	
			//$this->template->content = $view;
		}
	}
	
	public function client_search()
	{	
		Authlite::check_admin();
		Authlite::verify_referer();
		$this->template->title = Kohana::lang('backend.passenger_search');
		
		if($_POST)
		{
			if(isset($_POST['by_name']))
			{
				$post = new Validation($_POST);
				$post->add_rules('passenger_name','required');
				if ($post->validate())
				{	
					$passenger_name=$_POST['passenger_name'];
					//$search_incoming=$_POST['search_incoming'];
					//$search_outgoing=$_POST['search_outgoing'];
					
					if($_POST['parcel_classification']=='search_incoming'){
						$incoming_search_results=Incoming_Parcel_Model::search_client_by_name($this->agency_id,$passenger_name);
						$view = new View('incoming_parcel_search_results');
						$view->incoming_search_results = $incoming_search_results;
					}
					
					elseif($_POST['parcel_classification']=='search_outgoing'){
						$outgoing_search_results=Parcel_Model::search_client_by_name($this->agency_id,$passenger_name);
						$view = new View('outgoing_parcel_search_results');
						$view->outgoing_search_results = $outgoing_search_results;
					}
					
				}
				else
				{
					$errors=$post->errors('errors');
					$notice="";
						foreach($errors as $error) 
						{
							$notice.=$error."<br />";
						}
						$this->session->set('notice', array('message'=>$notice,'type'=>'error'));
						url::redirect('reports');
				}
				
				
				
				$view->passenger_name = $passenger_name;
			}
		}
		$this->template->content = $view;
	}	
	
	
	public function client_activity($passenger_name,$passenger_idc)
	{	
		Authlite::check_admin();
		Authlite::verify_referer();
		$this->template->title = Kohana::lang('backend.reports_for_bus');
		
		$search_results=Ticket_Detail_Model::passenger_activity($this->agency_id,$passenger_name,$passenger_idc);				
		
		$view = new View('passenger_activity');
		$view->search_results = $search_results;
		$view->passenger_name = $passenger_name;
		$view->passenger_idc = $passenger_idc;
		$this->template->content = $view;
	}
	
	
	public function mark_as_sent($parcel_id)
	{
		Authlite::check_admin();
		Authlite::verify_referer();
		$notice=Parcel_Model::mark_as_sent($this->agency_id,$parcel_id);
		$this->session->set('notice', array('message'=>$notice,'type'=>'success'));
		url::redirect('parcels/all/sent');
	}
	
	public function delete($parcel_id)
	{		
		Authlite::check_admin();
		Authlite::verify_referer();
		
		//$this->template->title=Kohana::lang('backend.delete_admin');
		
		Parcel_Model::delete_parcel($this->agency_id,$parcel_id);
		
		
		$this->session->set('notice', array('message'=>Kohana::lang('backend.deleted'),'type'=>'success'));
		url::redirect('parcels/all/sent');
		
		$this->template->content='';
	}
	

		
}
		