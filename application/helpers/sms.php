<?php
/**
 * Description: wasaTEXTO API v2
 * Send SMS
 * @package wasamundi.com
 * @subpackage:api
 * @license wasamundi labs 2011
 * @author Wasamundi Team
 */
//prevent the script from crashing after the default 30 seconds max execution time
ini_set("max_execution_time", "600000");
 
class Sms_Core
{
	

	
	public function send($msg, $to, $agency_id) {
		//$host = 'http://localhost:4001/quickticket/send_sms';
		$host = 'http://dev.quickticket.cm/send_sms';
		$api_user = "Quickticket";
		$api_key = 'Srbx0Mpi';
		$from = 'QuickTicket';
		
		
		$msg = urlencode($msg);
		$uri = "agency_id=".$agency_id."&to=".$to."&msg=".$msg;
		
		
		$data = curl_init($host); //Initialize the cURL session
		//Set the URL //Ask cURL to return the contents in a variable 
		//instead of simply echoing them to  the browser.	
		curl_setopt($data, CURLOPT_POSTFIELDS, $uri);
		curl_setopt($data, CURLOPT_RETURNTRANSFER, 1);
		//curl_setopt($data, CURLOPT_FOLLOWLOCATION, 1);
		//Execute the cURL session
		$response = curl_exec($data);
		
		//Close cURL session
		curl_close($data);
		//var_dump($response);exit;
		$result = json_decode($response);
		
		return $result;	
	}
	
	public function save($msg,$to,$agency_id){
		$sms = ORM::FACTORY('sms');
		$sms->agency_id = $agency_id;
		$sms->phone = $to;
		
		//Don't url encode here as it's done by the sender online. Else it's done twice and messages come with +
		//$sms->message = urlencode($msg);
		$sms->message = $msg;
		$sms->online = 0;
		if($sms->save())
		{
			return "Saved";
		}else
		{
			return "Error. Please try again";
		};
		
	
	}
	
	public function get_history($agency_id){
		$onlinedb = new Database('online_db');
		$total_sms = $onlinedb->from('sms_history')->where('agency_id' , $agency_id)->getwhere();
		return $total_sms;
	}
	
	public function get_sent_history($agency_id){
		$onlinedb = new Database('online_db');
		$sent_sms = $onlinedb->from('sms_history')->where('agency_id' , $agency_id)->where('delivered','1')->getwhere();
		return $sent_sms;
	}
	
	public function get_sent_count($agency_id){
		$onlinedb = new Database('online_db');
		$sent_sms = $onlinedb->from('sms_history')->where('agency_id' , $agency_id)->where('delivered','1')->getwhere();
		return count($sent_sms);
	}
	
	
	
	//Get the number of SMS sent today.
	public function get_today_count($agency_id){
		//date of today
		$today=date('d-m-Y');
		
		$onlinedb = new Database('online_db');
		$sent_sms = $onlinedb->from('sms_history')->where('agency_id' , $agency_id)->where('delivered','1')->getwhere();
		//var_dump($sent_sms);exit;
		$count = 0;
		foreach($sent_sms as $sms)
		{	//var_dump($sms->date);exit;
			$sent_date = date('d-m-Y',strtotime($sms->date));
			if ($sent_date == $today)
			{
				$count++;
			}
		}
		return $count;
	}
	
	public function get_unpushed_sms(){
		//check if an SMS has been pushed online or not
		$unpushed = ORM::factory('sms')->where('online','0')->orderby('date','DESC')->find_all();	
		return $unpushed;
	}
	
	
	
}