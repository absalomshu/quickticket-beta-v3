<?php defined('SYSPATH') or die('No direct script access');

class Dashboard_Item_Model extends ORM{


	
	public function get_all()
	{
		$items=ORM::factory('dashboard_item')->find_all();
		
		return $items;	
	}

	
	
	
	public function update_admin_module($admin_name, $module_id, $value)
	{	
		$admin_module = ORM::FACTORY('admin_module')->where('MainModuleID',$module_id)->where('AdminID',$admin_name)->find();
		$admin_module->IsAllowed = $value;
		$admin_module->save();
	}	
	
	public function add_admin_modules($admin_name, $module_id, $value)
	{	
		$admin_module = ORM::FACTORY('admin_module');
		$admin_module->AdminID = $admin_name;
		$admin_module->MainModuleID = $module_id;
		$admin_module->IsAllowed = $value;
		$admin_module->save();
	}
}
