<?php defined('SYSPATH') or die('No direct script access'); ?> 
	
	<div class="notice">
		<?php
			//determine what type of notice to display if at all
			$notice = $this->session->get_once('notice');
				if(!empty($notice)){ 
					if($notice['type'] == 'success'){?><div class="alert general success"><a class="close" data-dismiss="alert" href="#">&times;</a><?php echo $notice['message']; ?></div><?php }?>
				<?	if($notice['type'] == 'error'){?><div class="alert general error"><a class="close" data-dismiss="alert" href="#">&times;</a><?php echo $notice['message'];?></div><?php }?>
		<?}?>
	</div>

		
	<div class="row-fluid " >
	  
						
	  	<div class="span12 hero-unit prof-unit" >
				<ul class="nav nav-tabs">
					<li > <a href="<?=url::site('reminders/all')?>"><i class="icon-arrow-left"></i> <?=Kohana::lang('backend.back_reminders')?></a></li>
				</ul>
		
				<form action="<?=url::site('reminders/edit_reminder/'.$reminder->id)?>" method="POST" enctype="multipart/form-data" class="span7">
				<legend><?=Kohana::lang('backend.reminder_details')?></legend>
						<div class="">
							<div class="span4 text-right"><?=Kohana::lang('backend.task')?>: </div>
							<div class="span8"><?=$reminder->Name?></div>
						</div>
						<?php if($reminder->BusNumber){?>
						<div class="">
							<div class="span4 text-right"><?=Kohana::lang('backend.bus_no')?>: </div>
							<div class="span8">
								<?=$reminder->BusNumber?>
							</div>
						</div>
						<?php }?>
						
						<?php if($reminder->Amount){?>
						<div class="">
							<div class="span4 text-right"><?=Kohana::lang('backend.amount')?>: </div>
							<div class="span8">
								<?=number_format($reminder->Amount)?>
							</div>
						</div>
						<?php }?>
						
						
						<div class="">
							<div class="span4 text-right"><?=Kohana::lang('backend.schedule')?>: </div>
							<div class="span8">
								<?php 
									if(empty($reminder->StartDate)){echo "Remind on ".date("d-m-Y", strtotime($reminder->DueDate));}
									else{
										echo "Remind every <b>".$reminder->IntervalValue."</b>";
										if($reminder->IntervalType == "M"){ echo " <b> months</b>, ";}else { echo " <b> days</b>,";}
										echo " starting on <b>". date("d-m-Y", strtotime($reminder->StartDate))."</b>";
										}
								?>
							</div>
						</div>
						
					
						
						<div class="">
							<div class="span4 text-right"><?=Kohana::lang('backend.other_information')?>: </div>
							<div class="span8"><?=$reminder->Description?></div>
						</div>
						<!--
						<div class="">
							<div class="span4"><?=Kohana::lang('backend.only_me')?>: </div>
							<div class="span8">
								<input type="hidden" name="is_personal" value="0" />
								<label class="checkbox"><input type="checkbox" name="is_personal" value="1" <?php if($reminder->IsPersonal == 1){echo 'checked = "checked"';}?> disabled /></label>
								
							</div>
						</div>-->
						
												<div class="clear"></div>
<br/>
						
					
										
						<div class="clear"><br/><br/></div>
						
						
						
						
						<div class="form-actions ">
							<div class="span4">&nbsp; </div>
							<div class="span8">
								<a href="<?=url::site('reminders/delete_reminder/'.$reminder->id)?>"class="btn btn-info " onClick="return confirm('<?=Kohana::lang('backend.confirm_delete')?>?');"><?=Kohana::lang('backend.delete')?></a>
								<a href="<?=url::site('reminders/all')?>" class="btn" type="submit"> <?=Kohana::lang('backend.cancel')?></a>
							</div>
							
						</div>
									
				
						
				</form>
					<form action="<?=url::site('settings/add_bus_requirement')?>" method="POST" class="span4" >		
						   <div class="">
								<legend><?=Kohana::lang('backend.reminder_schedule')?></legend>	
								<table class="table table-striped table-hover table-condensed">
									
									<?php foreach ($reminder_schedules as $rem_schedule):?>
										<tr>	
											<td><i class="icon-file"></i> <?=date("d-m-Y", strtotime($rem_schedule->DueDate))?>
												
												<?php 
													//if completed, don't show the tick
													if($rem_schedule->IsCompleted)
													{ ?>
												
												<span class="view">Done by <?=$rem_schedule->ModifiedBy?></span>
													
												<?php	}
														else{
												?>
												
												<a  href="#confirm-complete" data-toggle="modal" class="view btn"><i class="icon-ok"></i> Mark complete</a></td>
												 <!-- Modal -->
													<div id="confirm-complete" class="modal hide fade" tabindex="-1" role="dialog" aria-labelledby="myModalLabel" aria-hidden="true">
													  <div class="modal-header">
														<button type="button" class="close" data-dismiss="modal" aria-hidden="true">×</button>
														<h3 id="myModalLabel">Complete reminder </h3>
													  </div>
													  <div class="modal-body">
														<p>Create an expense?</p>
													  </div>
													  <div class="modal-footer">
														<!--Only show yes if the reminder has an amount-->
														<?php if($reminder->Amount){?>
														<a href="<?=url::site('reminders/complete_reminder_schedule_add_expense/'.$rem_schedule->id)?>"class="btn btn-primary">Yes</a>
														<?php }?>
														<a href="<?=url::site('reminders/complete_reminder_schedule/'.$rem_schedule->id)?>"class="btn">No</a>
														<a class="btn" data-dismiss="modal" aria-hidden="true">Cancel</a>
													  </div>
													</div>
												
												<? } ?>
											</tr>
									<?php endforeach; ?>
								</table>
							
								
								<!--<a class="btn" href="<?=url::site('settings/add_bus_requirement')?>" ><?=Kohana::lang('backend.add')?></a>-->
							</div>
						</form>
			 
		</div>
		
		</div>
	  <div class="clear"></div>

<script type="text/javascript">
	$(document).ready(function(){
			$('#timepicker').timepicker({
				minuteStep:30,
				//defaultTime: '8:00 PM'
			});
			
			
			$('.input-append.date').datepicker({
				format: "dd-mm-yyyy",
				todayBtn: "linked",
				autoclose: true,
				todayHighlight: true
			});
			
			
				$(function () {
				$('#myTab a').click(function (e) {
					  e.preventDefault();
					  $(this).tab('show');  
			})
			  })
			  
		
			  
			
	})
	</script>
