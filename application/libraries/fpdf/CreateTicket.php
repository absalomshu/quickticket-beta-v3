<?php
require('fpdf.php');
class PDF extends FPDF
{
// Page header
function Header()
{
	// Logo
	$this->Image('application/libraries/fpdf/logo.png',5,2,12);
	// Arial bold 15
	$this->SetFont('Arial','B',11);
	
	// Move to the right
	$this->SetY(5);$this->Cell(90);
	// Title
	$this->Cell(30,10,'MUSANGO BUS SERVICE',0,0,'R');
	$this->Cell(10,10,'MBS',1,0,'C');
	// Line break
	$this->SetY(15);
	// The underlying line	
	$this->Cell(2,0,'____________________________________________________________',0,1);
	
		$this->Image('application/libraries/fpdf/bus.png',30,17,85);

	
	// Line break
	$this->SetY(17);$this->Cell(5);
	$this->Cell(25,8,'Time: 8:00 PM',0,0,'L');
	$this->Cell(67);
	$this->Cell(0,8,'Date: 11/10/2013 ',0,1,'L');
	
	
	// Line break
	$this->SetY(28);$this->Cell(5);
	$this->Cell(25,8,'Name: SHU ABSALOM NGWABINKAA ',0,0,'L');
	
	// Line break
	$this->SetY(35);$this->Cell(5);
	$this->Cell(25,8,'Phone: 23796362464',0,0,'L');
	$this->Cell(50);
	$this->Cell(0,8,'Seat: 60 ',0,1,'L');
	
	
	// Line break
	$this->SetY(42);$this->Cell(5);
	$this->Cell(25,8,'From: Yaounde',0,0,'L');
	$this->Cell(50);
	$this->Cell(0,8,'To: Buea ',0,1,'L');
	
	// Line break
	$this->SetY(49);$this->Cell(5);
	$this->Cell(25,8,'Vehicle: CE 989 NW',0,0,'L');
	$this->Cell(50);
	$this->Cell(0,8,'Price: 4000FRS ',0,1,'L');
	
	$this->SetFont('Arial','','7');
	$this->Cell(25,8,'In case you miss your schedule, management should be informed within 6 hours after which this ticket is valid for 30 days.',0,0,'L');
	$this->SetY(60);
	$this->Cell(130,8,' We are not responsible for your unpaid and undeclared luggage.',0,0,'C');
	
	//FOOTER
	// Position at 1.5 cm from bottom
	$this->SetY(-10);
	// Arial italic 8
	$this->SetFont('Arial','I',8);
	// Page number
	$this->Cell(0,10,'www.quickticket.co',0,0,'C');


}


}
$page_size = array('150','75');
// Instanciation of inherited class

//echo "<script>window.open('".$pdf->Output()."')</script>";
//$pdf->Output();


?>
