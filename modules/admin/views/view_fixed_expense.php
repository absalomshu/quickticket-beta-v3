<?php defined('SYSPATH') or die('No direct script access'); ?> 
<!DOCTYPE html>


		
		<div class="notice">
			<?php
				//determine what type of notice to display if at all
				$notice = $this->session->get_once('notice');
					if(!empty($notice)){ 
						if($notice['type'] == 'success'){?><div class="alert general success"><a class="close" data-dismiss="alert" href="#">&times;</a><?php echo $notice['message']; ?></div><?php }?>
					<?	if($notice['type'] == 'error'){?><div class="alert general error"><a class="close" data-dismiss="alert" href="#">&times;</a><?php echo $notice['message'];?></div><?php }?>
			<?}?>
		</div>
			
	  
	  <div class="row-fluid"> 	
	  
	  <div class="span12 hero-unit prof-unit "  >
	 <ul class="nav nav-tabs">
					<li > <a href="<?=url::site('settings')?>"><i class="icon-arrow-left"></i> <?=Kohana::lang('backend.back_settings')?></a></li>
		</ul>
	  
		<form action="<?=url::site('settings/view_fixed_expense/'.$fixed_expense->id)?>" method="POST" class="span8 offset2">
		<legend><?=Kohana::lang('backend.view_fixed_expense')?></legend>
				<div class="heading"><?=Kohana::lang('backend.percentage')?><!--<a href="#" class="view">See all activity</a>--></div>
				<div class="rule"><hr/></div>
				
				
					<div class="span4" ><?=Kohana::lang('backend.name');?>:</div>
					<div class="span4"><?=Kohana::lang('backend.percentage');?>:</div>
					<div class="span2"></div>
						
					<div class="span4">
						<input type="text" name='expense_name' value="<?php if($fixed_expense->IsPercentage){echo $fixed_expense->Name;}?>" />
					</div>	
					
					<div class="input-append span4">
						<input type="text" name="expense_value" class="span6 IsAmount" value="<?php if($fixed_expense->IsPercentage){echo $fixed_expense->Value;}?>"/>
						<span class="add-on"> % </span>
					</div>	
					
					<div class="span4" style="margin-left:0px;">
						<button class="btn  btn-info" type="submit" name="expense_is_percent"> <?=Kohana::lang('backend.save');?></button>
						<a href="<?=url::site('settings')?>" class="btn" type="submit"> <?=Kohana::lang('backend.cancel')?></a>
					</div>
					
				<div class="clear"></clear><br/>
				<div class="rule"><hr/></div>
				<div class="heading"><?=Kohana::lang('backend.fixed_amount')?></div>
				
				
				
					<div class="span4" ><?=Kohana::lang('backend.name');?>:</div>
					<div class="span4"><?=Kohana::lang('backend.amount');?>:</div>
					<div class="span2"></div>
						
					<div class="span4">
						<input type="text" name='expense_name2' value="<?php if($fixed_expense->IsFixedAmount){echo $fixed_expense->Name;}?>" />
					</div>	
					
					<div class="input-append span4">
						<input type="text" name = 'expense_value2' class="span6 IsAmount" value="<?php if($fixed_expense->IsFixedAmount){echo $fixed_expense->Value;}?>" />
						<span class="add-on"> FCFA </span>
					</div>	
					
					<div class="span4" style="margin-left:0px;">
						<button class="btn  btn-info" type="submit" name="expense_is_amount" > <?=Kohana::lang('backend.save');?></button>
						<a href="<?=url::site('settings')?>" class="btn" type="submit"> <?=Kohana::lang('backend.cancel')?></a>
					</div>
					
					
		</form>	
					
					
			

		</div>

