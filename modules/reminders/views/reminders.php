<?php defined('SYSPATH') or die('No direct script access'); ?> 
	
	
 
	<div class="row-fluid ">
	  	<div class="span12 hero-unit prof-unit" >
		
		<ul class="nav nav-tabs" id="myTab">
					<li ><a href="#add-bus"><i class="icon-plus"></i> <?=Kohana::lang('backend.add_reminder')?></a></li>
					<li class="active"><a href="#buses"><i class="icon-list"></i> <?=Kohana::lang('backend.all_reminders')?></a></li>
		</ul>
		
		<div class="tab-content">
			<div class="tab-pane" id="add-bus">
				<form action="<?=url::site('reminders/all')?>" method="POST" enctype="multipart/form-data" class="span8 offset2" autocomplete="off">
						<legend><?=Kohana::lang('backend.add_reminder')?></legend>
						
						
						
						<div class="">
							<div class="span4 text-right"><?=Kohana::lang('backend.task')?><span class="red"> *</span>: </div>
							<div class="span8"><input type="text"  name="name" required></div>
						</div>
						
						<!--<div class="">
							<div class="span4 text-right"><?=Kohana::lang('backend.bus_no')?>: </div>
							<div class="span8">
							<select name="bus_number" id="inputType">
									<option value=""> <?=Kohana::lang('backend.none')?></option>
									<?php foreach($all_buses as $bus){?>
										<option value="<?=$bus->bus_number?>"> <?=$bus->bus_number?></option>
										
									<?php }?>
							</select>
							</div>
						</div>-->
						
						<div class="">
							<div class="span4 text-right"><?=Kohana::lang('backend.bus_no')?>: </div>
							<div class="span8">
								<select name="bus_number[]" id="bus_number_multiselect" multiple="multiple">
								<!--<option value=""> <?=Kohana::lang('backend.none')?></option>-->
									<?php foreach($all_buses as $bus){?>
										<option value="<?=$bus->bus_number?>"> <?=$bus->bus_number?></option>
										
									<?php }?>
								</select>
							</div>						
						</div>						
						
						
						
						
						<div class="">
							<div class="span4 text-right"><?=Kohana::lang('backend.amount')?>: </div>
							<div class="span8"><input type="text"  name="amount" class="IsAmount"></div>
						</div>
						<div class="clear"></div><br/>
						
						<div class="">
							<div class="span4 text-right"><input type="radio" checked="checked" name="reminder_type" value="occurs_once" id="occurs_once_button"/> <?=Kohana::lang('backend.one_time')?>: </div>
							<div class="span8" >
							<div class="span3"><?=Kohana::lang('backend.remind_on')?></div>
							<div class="span4">
								<div class="input-append date" >
									<input type="text" name = 'due_date' class="span9 datepicker"  value="<?=date('d-m-Y')?>" id="occurs_once">
									<span class="add-on"><i class="icon-calendar"></i></span>
								</div>
							</div>
							</div>
						</div>
						<div class="clear"><br/></div>
						<div class="">
							<div class="span4 text-right"><input type="radio" name="reminder_type" value="recurring" id="recurring_button"/> <?=Kohana::lang('backend.recurring')?>: </div>
							<div class="span8" >
								<div class="span3"><?=Kohana::lang('backend.remind_every')?></div>
								<div class="span2"><input type="text"  name="interval_value" class="span12" id="recurring1"></div>
								<div class="span5">
									<select name="interval_type" id="recurring3" class="span7">
										<option value="D"> <?=Kohana::lang('backend.day')?>(s)</option>
										<option value="M"> <?=Kohana::lang('backend.month')?>(s)</option>
										
									</select>
								</div>
								
							</div>
						</div>
						
						<div class="">
							<div class="span4">&nbsp;</div>
							<div class="span8" >	
								<div class="span3"> <?=Kohana::lang('backend.start_on')?></div>
								<div class="span4">
									<div class="input-append date">
										<input type="text" name = 'start_date' class="span9 datepicker"  value="<?=date('d-m-Y')?>" id="recurring2" >
										<span class="add-on"><i class="icon-calendar"></i></span>
									</div>
								</div>
							</div>
						</div>
						
						
						<div class="clear"></div><br/>
						
						<div class="">
							<div class="span4 text-right"><?=Kohana::lang('backend.other_information')?>: </div>
							<div class="span8"><textarea  name="description" style="height:100px;"></textarea></div>
						</div>
						
						<!--<div class="">
							<div class="span4"><?=Kohana::lang('backend.only_me')?>: </div>
							<div class="span8">
								<input type="hidden" name="is_personal" value="0" />
								<label class="checkbox"><input type="checkbox" name="is_personal" value="1" /></label>
								
							</div>
						</div>-->
						
										
						<div class="clear"></div>
						<div class="clear"><br/><br/></div>
						
						<div class="form-actions">
							<div class="span4">&nbsp; </div>
							<div class="span8">
								<button class="btn btn-info " type="submit"><?=Kohana::lang('backend.register')?></button>
								<a href="<?=url::site('reminders/all')?>" class="btn" type="submit"> <?=Kohana::lang('backend.cancel')?></a>
							</div>
							
						</div>
						
									
						
				</form>
					<!--end the first tab-->
			</div>  
			 
			<div class="tab-pane active" id="buses">
				<table class="table table-striped table-hover table-condensed">
					<tr>
						<th></th>
						<th><?=Kohana::lang('backend.task')?></th>
						<th><i class="icon-bus"></i> <?=Kohana::lang('backend.bus_no')?></th>
						
						<th><?=Kohana::lang('backend.dates')?></th>
						<th>Actions</th>
						
					</tr>
					<?php foreach ($all_reminders as $reminder){?>
						<tr>		
							<td>
							<?php 
								
								 //Pick from the reminder schedules, the date of the last uncompleted.
								$reminder_sched=ORM::factory('reminder_schedule')->where('AgencyID',$this->agency_id)->where('ReminderID',$reminder->id)->where('IsCompleted','0')->find();
								$pending_schedule_exists=ORM::factory('reminder_schedule')->where('AgencyID',$this->agency_id)->where('ReminderID',$reminder->id)->where('IsCompleted','0')->limit(1)->count_all();
								$total_pending_reminders=0;
								if($pending_schedule_exists)
								{
									$date = date_create(date("d-m-Y",strtotime($reminder_sched->DueDate)));
									$today = date_create(date("Y-m-d"));
									$interval=date_diff($today,$date);
									$days_left = $interval->format('%R%a');
									
									//If it's 3 days to task, show orange mark
									if($days_left >0 AND $days_left <=3)
									{ ?>
										<span class="badge badge-warning"><?php //$days_left?>&nbsp;</span>
									<?php }
									elseif($days_left <=0)
									{ $total_pending_reminders += 1; ?>
										<span class="badge badge-important"><?php //$days_left?>&nbsp;</span>
									<?php }
								} 

							?>
							</td>
							<td><a href="<?=url::site('reminders/view_reminder/'.$reminder->id)?>"><?=$reminder->Name?></a></td>
							<td><?=$reminder->BusNumber?></a></td>
							<!-- if(empty) verifies if a database value is null.-->
							<td><?php if(empty($reminder->DueDate)) {echo "Start: " .date("d-m-Y",strtotime($reminder->StartDate));}else{echo "Due: ".date("d-m-Y",strtotime($reminder->DueDate));}?></td>
							<td><a href="<?=url::site('reminders/view_reminder/'.$reminder->id)?>"><i class="icon-edit"></i> <?=Kohana::lang('backend.view')?></td>
						
					
						</tr>
						<?php } ?>
				</table>	
			<?php echo $this->pagination;?>	  	
			</div>
				
			
				
					
		</div>
		
			
		
			  
		</div>
		
		</div>
	  <div class="clear"></div>

<script type="text/javascript">
	$(document).ready(function(){
			if($("#occurs_once_button").is(':checked'))
			{//On page load, hide the recurring fields
				$('#recurring1').prop('disabled',true);
				$('#recurring2').prop('disabled',true);
				$('#recurring3').prop('disabled',true);
			}
			
			$('#occurs_once_button').change(function(){
				if(this.checked)
				{
					//if he selects once time reminder, disactivate recurrent and disable fields
					$('#recurring1').prop('disabled',true);
					$('#recurring2').prop('disabled',true);
					$('#recurring3').prop('disabled',true);
					$('#occurs_once').prop('disabled',false);
			
				}
			});
			$('#recurring_button').change(function(){
				if(this.checked)
				{
					//if he selects recurrent, disactivate one time reminder
					$('#occurs_once').prop('disabled',true);
					$('#recurring1').prop('disabled',false);
					$('#recurring2').prop('disabled',false);
					$('#recurring3').prop('disabled',false);
			
				}
			});
				$(function () {
					$('#myTab a').click(function (e) {
						  e.preventDefault();
						  $(this).tab('show');  
					})
				  });
				  
				   $('#bus_number_multiselect').multiselect({
						includeSelectAllOption: true,
						buttonClass: 'btn btn-multiselect',
						allSelectedText: 'All buses'
					});
			  
		
			  
		
			  
			
	})
	</script>
	
	<!-- Initialize the plugin: -->
<script type="text/javascript">
    $(document).ready(function() {
       
    });
</script>
