<?php defined('SYSPATH') or die('No direct script access');

//set default timezone, otherwise it's UTC which is an hour late
date_default_timezone_set('Africa/Douala');

//Almost all functions will take as parameter, the id of the agency we are dealing with
//in order not to interfere with another's stuff.

class Control_Controller extends Admin_Controller {

	//public $template = 'template/admin_template';
	//public $uri_cont = 'http://api.wasamundi.com/v2/texto/';
	//protected $session;
	//protected $agency_id;
	//public $agency_code;
	

	
	public function index()
	{	
	
		//ensure a logged in user can't land on the signup page
		if (empty($this->admin)) 
		{	
			url::redirect('admin/login');
		}
		else
		{	
			//Decide whether admin is a branch manager, general manager or regular admin and redirect appropriately
			if($this->admin->admin_group_id == 3 OR $this->admin->admin_group_id == 4)
			{
				$this->main($this->admin->agency_id, $user_group=$this->admin->admin_group_id);
				//url::redirect('admin/control/'.$this->admin->agency_id);
			}
			else
			{
				url::redirect('admin/main');
			}
		}
	}
	
	public function login() 
	{	
		if (!empty($this->admin)) 
		{
				url::redirect('admin/main/'.$this->admin->agency_id);
		}
		//$this->auto_render=false;
		$notice='';
		$this->template->title="login";
		
		if($_POST) 
		{	
			
			$post=new Validation($_POST);
			$post->add_rules("username", "required",'valid::alpha_numeric');
			$post->add_rules("password", "required");
			if ($post->validate())
			{
				$username = $_POST['username'];
				$password = $_POST['password'];
				
				
				$admin = Authlite::instance()->admin_login($username, $password);
				
				if(!$admin){	
					//$notice="Sorry, incorrect username and password combination!";
					$this->session->set('notice', array('message'=>Kohana::lang('backend.incorrect_credentials'),'type'=>'error'));

				}
				else{
				
					//this is already done in the construct, but for some reason, does not pick the admin for
					//the first time till you refresh. so it is done here again 
					$this->admin = Authlite::instance()->get_admin();
					$admin_agency_id = $this->admin->agency_id;
					//url::redirect('admin/main/'.$admin_agency_id);
					url::redirect('admin');
			}}else 
			{
                $errors=$post->errors('errors');
                $notice="";
                foreach($errors as $error) 
				{
                    $notice .= $error;
                }
				$this->session->set('notice', array('message'=>$notice,'type'=>'error'));
            }
		}
		$view=new View('admin_login');
		$view->notice=$notice;
		$this->template->content=$view;
	}
	

	
	

	
	public function logout() 
	{
		Authlite::instance()->logout(true);
		url::redirect('admin');
	}
	



	public function past_schedule($schedule_id)
	{
		Authlite::check_admin();	
		Authlite::verify_referer();
		//Authlite::check_integrity($schedule_id);
		
		$this->template->title = Kohana::lang('backend.display_past');
		
		$schedule = get::schedule($schedule_id);
		$expenses_for_schedule = get::expenses_for_schedule($this->agency_id,$schedule_id);
		
		$seats_and_reservers = json_decode($schedule->reserved_seats,true);
		$seats_and_occupants = json_decode($schedule->seat_occupants,true);
		$view = new View('admin_past_schedule');
		
		//number of seats and occupants
		//calculate the amount from special drops
		$special_drop_total = 0;
		//count the number of special drops
		$special_drop_count = 0;
		//count the number of free tickets
		$free_tickets_price = 0;
		$free_ticket_count = 0;
		
		foreach($seats_and_occupants as $s)
		{
			//get the special drop price, and if it's not defined, MOVE ON
			if(isset($s['sd_price']))
			{
				$special_drop_total += @$s['sd_price'];
				$special_drop_count++;
			}
			//if the value of free ticket is 1
			if(isset($s['free_ticket']) && $s['free_ticket']==1)
			{
			//if it's also a special drop, get the price to know how much is lost
				if(isset($s['sd_price'])){
					$free_tickets_price += @$s['sd_price'];}
				else{
					$free_tickets_price += $schedule->ticket_price;
				}
				$free_ticket_count++;
			}
		}

		//number of seats and occupants
		$no_so = count($seats_and_occupants);
		//for now we count only occupants as reservers will be converted to occupants upon payment

		//regular seats are those paid for at normal price (as opposed to special drops)
		$regular_seats = $no_so - $special_drop_count;
		
		
		$view->seats_and_occupants = $seats_and_occupants;
		$view->seats_and_reservers = $seats_and_reservers;
		$view->no_so = $no_so;
		$view->empty_seats = $schedule->bus_seats - $no_so;
		$view->ticket_price = $schedule->ticket_price;
		
		//speculated normal cost of seats taken by special drops called amount lost
		$amount_lost = ($schedule->ticket_price) * $special_drop_count;
		//total is number of seats occupied times ticket price, minus speculated normal cost of special drops(lost), plus actual amount got from the special drops		
		$view->total = ($no_so * $schedule->ticket_price) - $amount_lost + $special_drop_total - $free_tickets_price;
		$view->schedule = $schedule;
		$view->free_ticket_count = $free_ticket_count;
		$view->special_drop_count = $special_drop_count;
		$view->special_drop_total = $special_drop_total;
		$view->regular_seats = $regular_seats;
		$view->expenses_for_schedule = $expenses_for_schedule;
		$this->template->content = $view;

	}
	
	
	public function main($current_branch=0) 
	{	
		//var_dump($_SESSION);EXIT;
		$permitted_levels = array(3);
		Authlite::check_access($permitted_levels);
		$user_group = $this->admin->admin_group_id;
		$agency_id = $this->agency_id;
		Authlite::check_admin();
		//Authlite::check_agency_integrity($agency_id);
		Authlite::verify_referer();
		
		//print_r($not);exit;
		//print_r($this->admin->access_level);exit;
		$this->template->title = Kohana::lang('backend.welcome_backend');
		$view=new View ('control_main');
		
		$towns = get::all_towns();
		$admin_town =get::admins_town($this->admin->agency_id);
		$parent_id = get::_parent($this->admin->agency_id)->id;
		$all_children = get::all_children($parent_id);
		//$current_schedules = get::all_parent_current_schedules($parent_id);
		//$departed_schedules = get::all_parent_departed_schedules($parent_id);


		//print_r($parent_id);exit;
		//now, if it's a branch manager, limit his query to his town
		//show all only for general manager
		if ($user_group == 3){
			$current_schedules=get::ten_agency_current_schedules($agency_id);
			$departed_schedules=get::ten_agency_departed_schedules($agency_id);
			}
		if ($user_group == 4){
			//enable the GM to surf various agencies by setting a session variable that determines which particular branch to process
			//if 0, then he's viewing the whole company.
			if($current_branch == 0){
			$current_schedules=get::all_parent_current_schedules($parent_id);
			$departed_schedules=get::all_parent_departed_schedules($parent_id);
			}else
			{	//die('not parent');
				$current_schedules=get::ten_agency_current_schedules($current_branch);
				$departed_schedules=get::ten_agency_departed_schedules($current_branch);
			}
			
			}
		$date=date('d-m-Y');
		
		//var_dump($departed_schedules);	exit;
		
		$view->current_schedules=$current_schedules;
		$view->departed_schedules=$departed_schedules;
		//set current branch in session as it's needed in the submenu, which is not part of the view.
		$this->session->set('current_branch', $current_branch);
		$view->current_branch=$current_branch;
		//Needs to be global here, else these variables will not be available in the sub-menu
		$view->set_global('children',$all_children);
		//$view->set_global('current_branch',$current_branch);
		$view->towns=$towns;
		$view->user_group=$user_group;
		//$this->template->children=$all_children;
		$this->template->content=$view;
		
	}
	
		
	
	//$type: either parent or agency. Helps distinguish which the BM or GM sees.
	public function schedule_by_date()
	{	
		Authlite::check_admin();
		Authlite::verify_referer();
		
		$agency_id = $this->agency_id;
		$parent_id = get::_parent($agency_id)->id;
		$type = $this->admin->admin_group_id;
		
		//Authlite::check_agency_integrity($agency_id);
		$this->template->title = Kohana::lang('backend.welcome_backend');
		
		if($_POST)
		{
			$post = new Validation($_POST);
			$post->add_rules('date','required');
			if ($post->validate())
			{	
				
			$date = date("Y-m-d",strtotime($_POST['date']));
			//$schedules = ORM::FACTORY('schedule')->where('agency_id',$agency_id)->where('departure_date',$date)->find_all();
			
			/*if it's a GM, type 2
			if ($type==4){
			$_30seats = ORM::FACTORY('schedule')->join('agencies','agencies.id','schedules.agency_id')->where('parent_id',$parent_id)->where('departure_date',$date)->where('status','departed')->where('bus_seats',30)->count_all();
			$_35seats = ORM::FACTORY('schedule')->join('agencies','agencies.id','schedules.agency_id')->where('parent_id',$parent_id)->where('departure_date',$date)->where('status','departed')->where('bus_seats',35)->count_all();
			$_39seats = ORM::FACTORY('schedule')->join('agencies','agencies.id','schedules.agency_id')->where('parent_id',$parent_id)->where('departure_date',$date)->where('status','departed')->where('bus_seats',39)->count_all();
			$_55seats = ORM::FACTORY('schedule')->join('agencies','agencies.id','schedules.agency_id')->where('parent_id',$parent_id)->where('departure_date',$date)->where('status','departed')->where('bus_seats',55)->count_all();
			$_70seats = ORM::FACTORY('schedule')->join('agencies','agencies.id','schedules.agency_id')->where('parent_id',$parent_id)->where('departure_date',$date)->where('status','departed')->where('bus_seats',70)->count_all();
			
			//print_r("HAPPY");exit;
			$departed_schedules = get::all_parent_departed_schedules_by_date($parent_id,$date);				
			$current_schedules = get::all_parent_current_schedules_by_date($parent_id,$date); 
			$parcels=get::all_parent_parcels_by_date($parent_id,$date);
			}*/
			
			//else{
			$_30seats = ORM::FACTORY('schedule')->where('agency_id',$agency_id)->where('departure_date',$date)->where('status','departed')->where('bus_seats',30)->count_all();
			$_35seats = ORM::FACTORY('schedule')->where('agency_id',$agency_id)->where('departure_date',$date)->where('status','departed')->where('bus_seats',35)->count_all();
			$_39seats = ORM::FACTORY('schedule')->where('agency_id',$agency_id)->where('departure_date',$date)->where('status','departed')->where('bus_seats',39)->count_all();
			$_55seats = ORM::FACTORY('schedule')->where('agency_id',$agency_id)->where('departure_date',$date)->where('status','departed')->where('bus_seats',55)->count_all();
			$_70seats = ORM::FACTORY('schedule')->where('agency_id',$agency_id)->where('departure_date',$date)->where('status','departed')->where('bus_seats',70)->count_all();
						
			$departed_schedules = get::all_agency_departed_schedules_by_date($agency_id,$date);				
			$current_schedules = get::all_agency_current_schedules_by_date($agency_id,$date);
			$parcels=get::all_agency_parcels_by_date($agency_id,$date);
			
			//Need to calculate the total expenses for that day
			$agency_expenses_by_date = get::agency_expenses_by_date($agency_id,$date);
			$total_expenditure = 0;
			foreach($agency_expenses_by_date as $exp )
			{
				$total_expenditure += $exp->amount;
			}
			
			//}

			//Calculate total amount expected from departed buses.
			$total = 0;
			foreach($departed_schedules as $ds){
				$total += $ds->total_amount;
			}
			}
			else
			{
				$errors=$post->errors('errors');
				$notice="";
					foreach($errors as $error) 
					{
						$notice.=$error."<br />";
					}
					$this->session->set('notice', array('message'=>$notice,'type'=>'error'));
					url::redirect('admin/main/'.$this->admin->agency_id);
			}
	}
	$view = new View('control_schedule_by_date');
	$view->departed_schedules = $departed_schedules;
	$view->current_schedules = $current_schedules;
	$view->parcels=$parcels;
	$view->total_expenditure=$total_expenditure;
	$view->_30seats = $_30seats;
	$view->_35seats = $_35seats;
	$view->_39seats = $_39seats;
	$view->_55seats = $_55seats;
	$view->_70seats = $_70seats;
	$view->total = $total;
	$view->date = $date;
		
	$this->template->content = $view;
	
	}		
	
	//$type: either parent or agency. Helps distinguish which the BM or GM sees.
	public function activity_by_period()
	{	
		Authlite::check_admin();
		Authlite::verify_referer();
		
		$agency_id = $this->agency_id;
		$parent_id = get::_parent($agency_id)->id;
		$type = $this->admin->admin_group_id;

		//Authlite::check_agency_integrity($agency_id);
		$this->template->title = Kohana::lang('backend.welcome_backend');
		
		if($_POST)
		{
			$post = new Validation($_POST);
			$post->add_rules('start_date','required');
			$post->add_rules('end_date','required');
			if ($post->validate())
			{	
				
			$start_date = date("Y-m-d",strtotime($_POST['start_date']));
			$end_date = date("Y-m-d",strtotime($_POST['end_date']));
			
			//$schedules = ORM::FACTORY('schedule')->where('agency_id',$agency_id)->where('departure_date',$date)->find_all();
			
			//if it's a GM, type 2
			/*
			if ($type==4)
			{
				$_30seats = ORM::FACTORY('schedule')->join('agencies','agencies.id','schedules.agency_id')->where('parent_id',$parent_id)->where('departure_date',$date)->where('status','departed')->where('bus_seats',30)->count_all();
				$_35seats = ORM::FACTORY('schedule')->join('agencies','agencies.id','schedules.agency_id')->where('parent_id',$parent_id)->where('departure_date',$date)->where('status','departed')->where('bus_seats',35)->count_all();
				$_39seats = ORM::FACTORY('schedule')->join('agencies','agencies.id','schedules.agency_id')->where('parent_id',$parent_id)->where('departure_date',$date)->where('status','departed')->where('bus_seats',39)->count_all();
				$_55seats = ORM::FACTORY('schedule')->join('agencies','agencies.id','schedules.agency_id')->where('parent_id',$parent_id)->where('departure_date',$date)->where('status','departed')->where('bus_seats',55)->count_all();
				$_70seats = ORM::FACTORY('schedule')->join('agencies','agencies.id','schedules.agency_id')->where('parent_id',$parent_id)->where('departure_date',$date)->where('status','departed')->where('bus_seats',70)->count_all();
				
				//print_r("HAPPY");exit;
				$departed_schedules = get::all_parent_departed_schedules_by_date($parent_id,$date);				
				$current_schedules = get::all_parent_current_schedules_by_date($parent_id,$date); 
				$parcels=get::all_parent_parcels_by_date($parent_id,$date);
			}
			
			*/
			
			
				$period_parcels=get::agency_parcels_by_period($agency_id,$start_date,$end_date);
				$period_expenses=get::agency_expenses_by_period($agency_id,$start_date,$end_date);
				$period_schedules=get::agency_schedules_by_period($agency_id,$start_date,$end_date);
				//$all_activity=get::agency_activity_by_period($agency_id,$start_date,$end_date);
								
				//calculate the total expenses for that period
				$total_expenditure = 0;
				foreach($period_expenses as $exp )
				{
					$total_expenditure += $exp->amount;
				}
			
				//Calculate total amount expected from departed buses for the period. DEPARTED BUSES ONLY
				$total = 0;
				foreach($period_schedules as $ds)
				{
					$total += $ds->total_amount;
				}
			}
			else
			{
				$errors=$post->errors('errors');
				$notice="";
					foreach($errors as $error) 
					{
						$notice.=$error."<br />";
					}
					$this->session->set('notice', array('message'=>$notice,'type'=>'error'));
					url::redirect('admin/main/'.$this->admin->agency_id);
			}
	}
	$view = new View('control_activity_by_period');
	//$view->departed_schedules = $departed_schedules;
	//$view->current_schedules = $current_schedules;
	$view->parcels=$period_parcels;
	$view->total_expenditure=$total_expenditure;
	$view->period_parcels = $period_parcels;
	$view->period_expenses = $period_expenses;
	$view->period_schedules = $period_schedules;
	$view->total = $total;
	$view->start_date = $start_date;
	$view->end_date = $end_date;
		
	$this->template->content = $view;
	
	}	
	
	//type is either departed or current
	public function all_schedules($type)
	{	
		Authlite::check_admin();
		Authlite::verify_referer();
		$agency_id = $this->agency_id;
		
		//pagination
		$per_page = 10;
		if ($type == 'current'){
			$all= get::all_agency_current_schedules($agency_id);}else{
			$all= get::all_agency_departed_schedules($agency_id);}
		$total = count($all);
		$this->pagination = new Pagination(array(
			// 'base_url'    => 'welcome/pagination_example/page/', // base_url will default to current uri
			'uri_segment'    => 'page', // pass a string as uri_segment to trigger former 'label' functionality
			'total_items'    => $total, // use db count query here of course
			'items_per_page' => $per_page , // it may be handy to set defaults for stuff like this in config/pagination.php
			'style'          => 'punbb', // pick one from: classic (default), digg, extended, punbb, or add your own!
			//'query_string'   => "ORM::factory('schedule')->where('agency_id',$agency_id)->where('status','current')->find_all()"
));
		if ($type == 'current'){
			//$schedules = get::all_agency_current_schedules($agency_id);
			$schedules=ORM::factory('schedule')->where('agency_id',$agency_id)->where('status','current')->where('Deleted','0')->limit($per_page,$this->pagination->sql_offset)->find_all();			
		}else{
			//$schedules = get::all_agency_departed_schedules($agency_id);
			$schedules=ORM::factory('schedule')->where('agency_id',$agency_id)->where('status','departed')->where('Deleted','0')->limit($per_page,$this->pagination->sql_offset)->find_all();			
		}
		
		$this->template->title = 'Viewing all schedules';		
		$view = new View('control_all_schedules');
		$view->schedules = $schedules;
		$this->template->content = $view;
	
	}
	
		
		
		}