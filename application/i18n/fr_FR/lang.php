<?php  defined('SYSPATH') or die('No direct script access');
//Langue
$lang=array(
'tag_find' => 'Cherchez des entreprises. ',
'tag_discover' => 'Interagissez avec, et Partagez',
'tag_info' => 'des informations sur vos entreprises favoris au Cameroun.',
'last_visited' => 'derni�re visit�s',
'most_visited' => 'les plus visit�s',
'most_recent' => 'les plus r�cents',
'testimonials' => 'testimonials',
'most_trending' => 'trending',
'search' => 'rechercher',
'visits' => 'visites',
'featured_businesses' => 'featured businesses',
'categories' => 'Cat�gories',
'more' => '... plus',
'login' => 'Connexion',
'sign_in' => 'Connexion',
'signup' => 'Inscription',
'find_any_business' => 'Des entreprises ',
'find' => 'Recherchez des',
'des' => 'Des',
'use_wasamundi'=>'Recherchez des avis, recommendations et directions pour le meilleurs h�tels, restaurants, bars, bo�tes de nuit, boutiques, �coles et autres dans le r�gions et villes du Cameroon',
'browse_businesses'=>'Recherchez des entreprises dans les r�gions et villes du Cameroon par cat�gorie. ',
'use_category'=>'Recherchez des entreprises dans les r�gions et villes du Cameroon par cat�gorie. ',
'use_category'=>'Use Wasamundi Category Listing to browse Businesses in regions and towns of Cameroon by categories ',
'use_category2'=>'Recherchez des entreprises et services dans les villes de   ',

'browse_businesses'=>'Rechercher des entreprises dans ',
'region_of'=>' du Cameroon. ',
'search_for'=>'Recherchez des entreprises dans les villes du Cameroon',
'key_words'=>'h�tels, restaurants, boissons, voyage,sant�, pharmacie, cliniques, �ducation, �coles',
'search_results'=>'R�sultats de recherche pour',
'businesses_starting'=>'Entreprises commen�ant par la lettre ',
'in'=>'dans',
'did_you_mean'=>'Vouliez-vous dire',
'letter' => 'lettre',
'exact_match' => 'R�sultat exact:',
'search_any' => 'Recherchez une entreprise, un service ou un produit dans la ville de  ',
'reviews_recommendations' => ' avis, photos, descriptions, directions et recommendations. ',
'sorry_no_business' => 'D�sol�, aucune correspondance trouv�e pour ',
'reviews' => 'Commentaire(s)',
'visits' => 'visite(s)',
'contact_us' => 'Contactez-nous',
'socialize' => 'SOCIALIZER AVEC NOUS',
'recommend' => 'Confiez cette entreprise',
'votes' => 'votes',
'home' => 'Accueil',
'business_listing' => ' liste des entreprises: ',
'follow_us' => 'Follow us!',
'regions' => 'R�gions',
'about_us' => '� propos de',
'terms_of' => 'Termes',
'privacy' => 'Confidentialit� ',
'tour' => 'Tour',
'contact' => 'Contact',
'blog' => 'Blog',
'rss' => 'RSS feeds',
'featured' => 'Pr�sent� sur',
'all_rights' => 'Tous droits r�serv�s',
'category_listing' => 'Liste des categories',
'edit_profile' => 'Modifiez votre profil',
'logout' => 'D�connexion',
'create_and' => 'Creez un compte pour commenter et recommender',
'login_using' => 'Connexion facebook',
'name' => 'Nom',
'username' => 'Nom d\'utilisateur',
'email' => 'Email',
'password' => 'Mot de passe',
'phone' => 'T�l�phone',
'find_any' => 'Cherchez des entreprises.  ',
'review_and' => 'Partagez & Recommendez',
'join_now' => 'Entrez!',
'reviewed_on' => 'a comment� sur',
'latest_reviews' => 'Derniers commentaires',
'visits' => 'VISITES',
'location' => 'LOCATION',
'rainy_day' => 'Pluie',
'message' => 'Message',
'inbox' => 'Bo�te de r�ception',
'view_inbox' => 'Bo�te de r�ception',
'read' => 'Lus',
'delete' => 'Supprimer',
'report' => 'Signaler abus',
'outbox' => 'Bo�te d\'envoi',
'view_outbox' => 'Bo�te d\'envoi',
'message_from' => 'Message de ',
'send_reply' => 'Envoyez une r�ponse �',
'msg_title' => 'Titre du message',
'msg_content' => 'Contenu du message',
'login_to' => 'Connectez-vous pour voir vos m�ssages',
'message_from' => 'M�ssage de',
'message_deleted' => 'M�ssage supprim�',
'marked_as' => 'Message signal� comme abus',
'please_try' => 'Veuillez r�essayer plus tard',
'msg_sent' => 'Message envoy�',
'send_msg_to' => 'Envoyez un message �',
'in_unread' => 'Bo�te de r�ception: Non lus',
'out_unread' => 'Bo�te d\'envoi: Non lus',
'search_mail' => 'Rechercher dans les messages ...',
'send_bulk' => 'Envoyer des notifications en masse',
'create_account' => 'Cr�ez un compte',
'join_wasa' => 'Rejoinez Wasamundi',
'your_profile' => 'Votre profil a �t� mis � jour',
'not_specified' => 'pas pr�cis�',
'update_profile' => 'Mise � jour profil',
'password_modified' => 'Votre mot de passe a �t� modifi�',
'password_mismatch' => 'Les mots de passe ne correspondent pas',
'oops' => 'Oops, mot de passe incorrect ',
'edit_password' => 'Modifiez votre mot de passe',
'pic_saved' => 'Photo sauvegard�',
'enter_email' => 'Saissisez votre email pour r�cup�rer votre mot de passe',
'change_number' => 'Num�ro indisponible',
'email_taken' => 'Email indisponible',
'username_taken' => 'Nom d\'utilisateur indisponible.',
'username_email' => 'Saissisez votre nom d\'utilisateur ou email et mot de passe',
'' => 'Connectez-vous �',
'account_suspended' => 'Compte suspendu. Veuillez contacter l\'administrateur .',
'repeat_password' => 'R�p�ter mot de passe',
'change_field' => 'Modifier n\'importe quel champ neccessaire',
'account_settings' => 'Param�tres de compte',
'change_password' => 'Changer votre mot de passe',
'gender' => 'sexe',
'street' => 'Rue',
'country' => 'Pays',
'town' => 'Ville',
'preferred_lang' => 'Langue pr�f�r�',
'update_photo' => 'Mise � jour photo',
'change_password' => 'Changez votre mot de passe',
'new_password' => 'Nouveau mot de pass',
'create_start' => 'Cr�ez un compte et commencez � commenter et recommender',
'login_using' => 'Connexion facebook',
'or' => 'OU',
'forgot_password' => 'Mot de passe <br/> oubli�?',
'enter_details' => 'Saissisez votre email ou nom d\'utilisateur et mot de passe',
'on' => 'sur',
'be_fan' => 'Devenir fan  ',
'socialize_small' => 'Socializer avec nous',
'follow_us_small' => 'Suivez nous ',
'notmatch'=>'Nom d\'utilisateur/email/mot de passe invalide. Veuillez essayer � nouveau',
'enter_username' => 'Saissisez votre nom d\'utilisateur ou email',
'enter_password' => 'Saissisez votre mot de passe',
'editsuccess'=>'Vous avez modifi� avec succ�s votre profil',
'editprofile' =>"Modifier votre profil",
'oldnewpass'=>'Entrez votre ancien mot de passe et de nouveaux',
'password'=>array('required'=>'The password field is required', 'length'=>'Your password must be between 6 to 30 characters'),
'password'=>'Mot de passe',
'or_small'=>'ou',
'newpassword' => 'Nouveau mot de passe',
'newpasswordretype'=>'Retaper mot de passe',
'submit'=>'Soumettre',
'changeheading'=>'Changer tous les champs n�cessaires.',
'upload'=> 'Envoyer',
'labelname'=>'Nom',
'contactnumber'=>'Num�ro de contact',
'street'=>'Rue',
'town'=>'Ville',
'country'=>'Pays',
'changepassword'=>'Changer mot de passe',
'headingfeedback'=>'Vous �tes sur le point de d�poser un r�action pour rendre votre exp�rience de meilleure qualit�.',
'labelemail'=>'Email',
 'labelfeedback'=>'Reaction',
'passwordmodified'=>'Votre mot de passe a �t� modifi�',
'passwordnomatch'=>'Oops, les mots de passe ne correspondent pas.',
'passwordcorrect'=>'Oops, entrez votre mot de passe correct',
'editpassword' =>'Modifier le mot de passe',
'photosave'=> 'Votre photo a bien �t� enregistr�',
'photoupdate'=>'Votre photo de profil a �t� mis � jour',
'newpasssent' =>'Votre nouveau mot de passe a �t� envoy� � votre e-mail',
'emailnotfound'=>'Oops, e-mail ne se trouve pas dans notre ',
'ban' =>'Ce compte est interdit. Contactez-nous pour plus d\'informations',
'usertaken'=>'Nom d\'utilisateur d�j� pris',
'emailtaken' =>'Email d�j� pris',
'successcreate3'=>'Un e-mail avec vos informations de connexion a �t� transmis � votre compte e-mail.',
'successcreate2'=> 'Inscription r�ussie.',
'sucesscreate1'=>'Salut',
'regsuccess' =>'Inscription r�ussie',
'addpropic' =>'Ajouter un profil',
'picture' =>'Photo',
'youcan' => 'Vous pouvez',
'continueedit' =>'Continuez de modifier votre profil ',
'understand' =>'Comprendre comment utiliser',
'gohome' =>'Cliquez ici pour commencer � naviguer sur le lieu',
'enter_name' => 'Saissisez votre nom',
'enter_email2' => 'Saissisez votre email',
'popular_places'=>'lieux ',
'pop_place_desc'=>'Sites touristiques, des attractions, des march�s et des endroits populaires dans les villes de ',
'description'=>'Description',
'no_suggestions'=>'Aucune suggestion trouv�e',
'advanced_search'=>'Cliquez pour la recherche avanc�e',
'results_for' => 'R�sultats pour la',
'no_match' => 'Aucun r�sultat trouv�. ',
'dis_results' => 'Affichage des r�sultats',
'of'=>'de',
'sort_by' => 'trier par: ',
'social' => 'sociale',
'alphabetic' => 'alphab�tique',
'response_time' => 'temps de r�ponse: ',
'post' => 'Postez',
'add_review' => 'Que pensez-vous?...',
'verified' => 'Verifi�',
'send_message' => 'Envoyez votre message',
'your_message' => 'Votre message', 
'rem_id' => 'Ne N\'oubliez sur le web',
'your_phone' => 'Votre num�ro de telephone',
'your_email' => 'Votre addresse email',
'contact_this' => 'Contactez cette entreprise',
'fixed_phone' => 'Num�ro fixe',
'phonenum' => 'Mobile',
'postalcode' => 'B.P. ',
'whatwedo' => 'Nos services: ',
'website' => 'Site Web',
'team' => 'Equipe',
'close'=>'fermer',
'welcome'=>'Bienvenu',
'reset_token'=>'Le jeton de r�initialisation n\'existe pas. Veuillez reessayer.',
'pwd_reset'=>'Mot de passe r�initialiser. Vous pouvez maintenant vous connecter.',
'reset_pwd'=>'R�initialiser votre mot de passe <br/>',
'pwd_request_reset'=>'Demande de r�initialisation de mot de passe',
'pwd_msg'=>'<p> Vous avez demand� un nouveau mot de passe. <p/><p> Si vous n\'avez pas demand� une r�initialisation ignorer ce lien. </ p> <p> Cliquez sur le lien pour r�initialiser:',
'terms'=>'Termes',
'feedback_thanks'=>'Merci. Nous prendrons vos commentaires en consid�ration.',	
'feedback_error'=>'Certains de vos d�tails sont incorrects. Veuillez v�rifiez et r�essayez.',
'send_feedback'=>'Vos r�actions',
'we_love'=>'Nous aimerions entendre vos r�actions',
'ur_name'=>'Quelle est votre nom',
'ur_email'=>'Quelle est votre email',
'ur_observe'=>'Vos commentaires',
'send'=>'envoyer',
'useto_reset_password'=>'Entrez votre email ci-dessous et les instructions pour r�initialiser votre mot de passe seront envoy�s � votre bo�te.',
'name'=>'Votre nom',
'your_observation'=>'Vos observations',
'ur_observe'=>'Dites-nous ce que vous pensez. � propos de la fonctionnalit�, de design, de couleurs et tout autres endroits que nous pourrions am�liorer pour vous..  ',
'you_reported'=>'Vous avez signal� cette photo. L\'administrateur doit r�agir en 0-24 heures. Merci.',
'check_entity'=>'V�rifiez l\'entit� et r�agissez dans les 24 heures!',
'on_this'=>'sur cette URL',
'flagged_photo'=>'a signal� la photo',
'msg_sent_to'=>'Votre message a  �t� envoy� a: <br>',
'guest'=>'Guest name',
'guestname'=>'Saissisez votre nom',
'firstaddphoto'=>'Soyez le premier a telecharger une photo pour cette entit�',
'saysomething'=>'Description pour la photo',
'guest_salut'=>'Salut',
'status'=>'Statut',
'verified'=>'Verifi�',
'userame'=>'Nom d\'utilisateur',
''=>'',
''=>'',


// Home page categories
'categoris' => array(
						'wasa-hotel' => array('h�tels', 'hotels'),
						'wasa-hostel' => array('mini-cit�s', 'mini-cites'),
						'wasa-tourism' => array('voyage / tourisme','voyage-et-tourisme'),
						'wasa-bar' => array('bars/restau', 'restaurants-et-bars'),
						'wasa-shopping' => array('shopping', 'shopping'),
						'wasa-health' => array('sant�', 'pharmacies'),
					),

	//rating and review messages
	'ratingmsg' => 'Merci. Il semble que vous avez deja evalu� .',
	'ratingmsg1' => 'Vous dites que nous sommes',
	'ratingmsg2' =>  'Merci pour votre evaluation!',
	'reviewmsg' => 'Oops! Vous avez d�j� post� ca.',
	'reviewmsg1' => 'Votre opinion compte sur Wasamundi.com.',
	
	//user uploads
	'guest_salut' => "Salut",
	'guest' => 'Visiteur',
	'photodescription' => 'Decrivez cette photo',
	'saysomething' => 'Que pensez-vous',
	'addphoto' => 'Ajouter une photo',
	'firstaddphoto' => 'Vous etes le premier � avoir ajout� une photo pour cette entreprise.', 
	'guestname' => ' Quel est votre nom?',
	'uploadnow'=> 'Chargez maintenant!',

	//contact on business page
	'contactthanks'=>'Merci! Votre message a �t� envoy�. Nous prendrons contact avec vous bient�t.',
	'contactdetails' =>'D�tails',
	'pleaselogin' => 'S\'il vous pla�t connectez-vous pour voir ces d�tails',
	
	//business profile navigation
	'b_description'=>'description',
	'b_contact'=>'contact',
	'b_photo'=>'photo',
	'b_review'=>'avis',
	
	
);
