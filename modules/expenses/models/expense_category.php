<?php defined('SYSPATH') or die('No direct script access');

class Expense_category_Model extends ORM{

	public function add_category($agency_id,$category_name, $category_description, $admin)
	{	
		$expense_category = ORM::FACTORY('expense_category');
		$expense_category->agency_id = $agency_id;
		$expense_category->description = $category_description;
		$expense_category->name = $category_name;
		$expense_category->created_by = $admin;
		$expense_category->save();
			
	}
	
	public function add_default_category($agency_id)
	{	
		$expense_category = ORM::FACTORY('expense_category');
		$expense_category->id = '1';
		$expense_category->agency_id = $agency_id;
		$expense_category->description = 'Default expense category';
		$expense_category->name = 'General';
		$expense_category->created_by = 'SYS';
		$expense_category->save();
			
	}
	
	public function reset_default_category($agency_id)
	{	
		$default_category = ORM::FACTORY('expense_category')->where('agency_id',$agency_id)->where('id','1')->find();
		var_dump($default_category);exit;
		$default_category->agency_id = $agency_id;
		//For now, let them be able to modify the description
		//$expense_category->description = 'Default expense category';
		$default_category->name = 'General';
		$default_category->created_by = 'SYS';
		$default_category->save();
			
	}
	
	public function edit_category($agency_id, $category_id, $category_name, $category_description, $admin)
	{	
		$expense_category = ORM::FACTORY('expense_category')->where('agency_id',$agency_id)->where('id',$category_id)->where('deleted','0')->find();
		$expense_category->name = $category_name;
		$expense_category->modified_by = $admin;
		$expense_category->description = $category_description;
		$expense_category->save();
		
	}
	
	public function get_all($agency_id)
	{
		$expense_categories=ORM::factory('expense_category')->where('agency_id',$agency_id)->where('deleted','0')->find_all();
		return $expense_categories;	
	}
	
	public function get($agency_id, $category_id)
	{
		$expense_category=ORM::factory('expense_category')->where('agency_id',$agency_id)->where('id',$category_id)->find();
		return $expense_category;	
	}
	
	//safe delete so old expenses can still track the parent category
	public function safe_delete_category($agency_id, $category_id, $admin)
	{

		$expense_category=ORM::factory('expense_category')->where('agency_id',$agency_id)->where('id',$category_id)->find();
		$expense_category->deleted = 1;
		$expense_category->modified_by = $admin;
		
		//append deleted category to the name so the actual name can be reused
		$expense_category->name .= ' - DELETED CATEGORY';
		$expense_category->save();
		
		$notice="Expense category deleted";
		return $notice;
		
	}
	
	public function get_agency_expenses_by_period_by_admin($agency_id,$start_date,$end_date,$admin)
	{
		$db = new Database();
		$expenses = $db->query("SELECT * FROM expenses WHERE agency_id= ".$agency_id." AND CreatedBy= '".$admin."' AND deleted = '0' AND date_incurred BETWEEN CAST('$start_date' AS DATE) AND CAST('$end_date' AS DATE) ORDER BY date_incurred ASC");
		return $expenses;
	}
	
	public function get_agency_expenses_by_period_by_bus($agency_id,$start_date,$end_date,$bus_number)
	{
		$db = new Database();
		$expenses = $db->query("SELECT * FROM expenses WHERE agency_id= ".$agency_id." AND bus_number= '".$bus_number."' AND deleted = '0' AND date_incurred BETWEEN CAST('$start_date' AS DATE) AND CAST('$end_date' AS DATE) ORDER BY date_incurred ASC");
		return $expenses;
	}
	

}
