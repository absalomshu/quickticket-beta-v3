<?php defined('SYSPATH') or die('No direct script access'); ?>
	
    <div class="container">
		
		
		<div class="row-fluid  span12 hero-unit prof-unit reload" style="margin:5px 0 0px 0;">	
		
			
			
			<div class="span12" >
			<div class="no-print">
			<?php 
			//instantiate variable to total the prices in the loop below
				$parcel_total = 0;
				foreach ($parcels as $parcel)
				{
				$parcel_total += $parcel->price;
				}
			?>
			<ul class="nav nav-tabs">
					<li > <a href="<?=url::site('manage/reports')?>"><i class="icon-arrow-left"></i> <?=Kohana::lang('backend.back_reports')?></a></li>
				</ul>
			</div>
			<div class="span8 offset2 print-area" >
			
				
				<legend><?=Kohana::lang('backend.summary_from')?> <?=date("d-m-Y",strtotime($start_date))." to ".date("d-m-Y",strtotime($end_date))." for ".get::agency_name($agency_id)?><button class="view" onClick="window.print()"><i class="icon-print"></i> <?=Kohana::lang('backend.print')?></button></legend>
				<table class="table table-bordered table-condensed">
					<tr><th><?=Kohana::lang('backend.schedules')?></th></tr>
					<?php 
					foreach ($period_schedules as $sched)
					{?>
					<tr>
						<td class="span2"><?=date("d-m-Y",strtotime($sched->departure_date))?></td>
						<td><a href='#' class="IsSchedule" data-title="<?='SC'.$sched->agency_id.$sched->id?>" data-content="<?=$sched->bus_number." ( ".get::town($sched->from). "  -->  ". get::town($sched->to)." ) ".date("d-m-Y",strtotime($sched->departure_date))."  ".date("g:i A", strtotime($sched->departure_time))?>" ><?=$sched->bus_number?></a>   <?="(".get::town($sched->from). "-". get::town($sched->to).")"?></td>
						<td></td>
						<td class="text-right"><?=number_format($sched->total_amount)?></td>
					</tr>
					<?php }?>
					<?php
					//Now take account of remote tickets
					foreach ($remote_outgoing_tickets as $ticket)
					{
					?>
					<tr>
						<td class="span2"></td>
						<td><?=$ticket->ClientName?> <i>(Remote ticket)</i></td>
						<td></td>
						<td class="text-right"><?=number_format($ticket->Price)?></td>
					</tr>
					<?php 
					//add to total from schedules
					$total += $ticket->Price;
					}?>
					<tr>		
						<td class="span2"></td>
						<th ><?=Kohana::lang('backend.total_ticket_sales')?></th>
						<td class="span2"></td>
						<th class="span2 text-right"><?=number_format($total)?> FCFA</th>
					</tr>
				</table>
				
					
					
				<table class="table table-bordered table-condensed">	
					<tr><th><?=Kohana::lang('backend.parcels')?></th></tr>
					<tr><th colspan='4'><?=Kohana::lang('backend.mail')?></th></tr>
					<?php 
					foreach ($period_parcels as $parcel) 
					{
					if($parcel->type == 'mail')
						{
						?>
						<tr>
							<td class="span2"><?=date("d-m-Y",strtotime($parcel->sent_date))?></td>
							<td><?=$parcel->description?></td>
							<td></td>
							<td class="text-right"><?=number_format($parcel->price)?></td>
						</tr>
						<?php 
						} }?>	
					<tr><th colspan='4'><?=Kohana::lang('backend.baggage')?></th></tr>	
					<?php 
					foreach ($period_parcels as $parcel){
					if($parcel->type == 'baggage')
						{
						?>
						<tr>
							<td class="span2"><?=date("d-m-Y",strtotime($parcel->sent_date))?></td>
							<td><?=$parcel->description?></td>
							<td></td>
							<td class="text-right"><?=number_format($parcel->price)?></td>
						</tr>
						<?php 
						}
					}?>
					<tr>		
						<td class="span2"></td>
						<th><?=Kohana::lang('backend.total_from_parcels')?></th>
						<td class="span2"></td>
						<th class="span2 text-right"><?=number_format($parcel_total)?> FCFA</th>
					</tr>
				</table>
				<table class="table table-bordered table-condensed">	
					<tr><th><?=Kohana::lang('backend.other_income')?></th></tr>
					<?php 
					$income_total = 0;
					foreach ($period_income as $income)
					{?>
					<tr>
						<td class="span2"><?=date("d-m-Y",strtotime($income->date_incurred))?></td>
						<td><?=Kohana::lang("backend.$income->purpose")." - ". $income->description." : ". $income->bus_number?></td>
						<td></td>
						<td class="text-right"><?=number_format($income->amount)?></td>
					</tr>
					<?php $income_total += $income->amount; }?>
					
					
					<tr>		
						<td class="span2"></td>
						<th><?=Kohana::lang('backend.total_income')?></th>
						<td class="span2"></td>
						<th class="span2 text-right"><?=number_format($income_total)?> FCFA</th>
					</tr>
				</table>
				<table class="table table-bordered table-condensed">
					<tr><th><?=Kohana::lang('backend.expenses')?></th></tr>
					<?php
					foreach ($period_expenses as $exp)
					{?>
					<tr>
						<td class="span2"><?=date("d-m-Y",strtotime($exp->date_incurred))?></td>
						<td><?=$exp->purpose?><span class="tiny-text"><?php if($exp->bus_number){echo " ($exp->bus_number)";}?></span></td>
						<td class="text-right" ><?=number_format($exp->amount)?></td>
						<td></td>
					</tr>
					<?php }?>
					<tr>		
						<td></td>
						<th><?=Kohana::lang('backend.total_expenditure')?></th>
						<th class="span2 text-right"><?=number_format($total_expenditure)?> FCFA</th>
						<td class="span2"></td>
					</tr>
				</table>
				<table class="table table-bordered table-condensed">					
					<tr>		
						<td><h3><?=Kohana::lang('backend.net_income')?></h3></th>
						<td class="text-right"><h3><?=number_format($total + $parcel_total + $income_total - $total_expenditure)?> FCFA</h3></td>
					</tr>
				</table>
			</div>
		</div>
			
		
		
		
		
		
		
		</div>
    </div> 
