<?php defined('SYSPATH') or die('No direct script access');

/**
 * Description:  
 
 *
 * @package		 quickticket
 * @subpackage 	 Helper
 * @author 		 Absalom Shu
 * @copyright    (c) 2013
 * @license 	 quickticket
 */
 //There are some functions that vary in the online version from the offline version.
 //they should be put here to avoid conflict
class Manage_Core
{	
	

	public function expenses_for_bus($bus_number){
	
		$bus_expenses = ORM::factory('expense')->where('bus_number',$bus_number)->where('deleted','0')->find_all();		
		return $bus_expenses;
	}
	
	
	public function get_expenses_for_schedule($schedule_id)
	{
		//Don't use agency ID coz the BM should see for all sibling agencies.
		$sched_expenses = ORM::factory('expense')->where('schedule_id',$schedule_id)->find_all();		
		return $sched_expenses;
	}
	
	public function get_cash_to_bank_for_parent($parent_id){
		$all_cash_to_bank = ORM::factory('cash_to_bank')->join('agencies','agencies.id','cash_to_banks.agency_id')->where('agencies.parent_id',$parent_id)->where('deleted','0')->orderby('created_on','DESC')->find_all();	
		return $all_cash_to_bank;
	}
	
	public function get_cash_to_bank_for_agency($agency_id)
	{
		$all_cash_to_bank=ORM::factory('cash_to_bank')->where('agency_id',$agency_id)->where('deleted','0')->orderby('date_incurred','DESC')->find_all();
		return $all_cash_to_bank;
	}
	
	public function get_all_expenses_for_agency($agency_id)
	{
		$expenses=ORM::factory('expense')->where('agency_id',$agency_id)->where('deleted','0')->orderby('date_incurred','DESC')->find_all();
		return $expenses;
	}
	public function get_all_expenses_for_parent($parent_id)
	{
		$expenses=ORM::factory('expense')->join('agencies','agencies.id','expenses.agency_id')->where('agencies.parent_id',$parent_id)->where('deleted','0')->orderby('date_incurred','DESC')->find_all();
		return $expenses;
	}
	
	public function get_active_purchases_for_agency($agency_id)
	{
		$purchases=ORM::factory('online_purchase')->where('agency_id',$agency_id)->where('status','active')->find_all();
		return $purchases;	
	}
	
	public function get_active_purchases_for_parent($parent_id)
	{
		$purchases=ORM::factory('purchase')->join('agencies','agencies.id','purchases.agency_id')->where('agencies.parent_id',$parent_id)->where('agency_id',$agency_id)->where('status','active')->find_all();
		return $purchases;	
	}
	
	public function get_all_buses_for_parent($parent_id){
		$all_cash_to_bank = ORM::factory('bus')->join('agencies','agencies.id','buses.agency_id')->where('agencies.parent_id',$parent_id)->where('deleted','0')->orderby('bus_number','DESC')->find_all();	
		return $all_cash_to_bank;
	}
	
	public function get_parent_expenses_by_period_by_bus($parent_id,$start_date,$end_date,$bus_number)
	{
		$db = new Database();
		$expenses = $db->query("SELECT * FROM expenses JOIN agencies ON agencies.id = expenses.agency_id WHERE agencies.parent_id= ".$parent_id." AND bus_number= '".$bus_number."' AND deleted = '0' AND date_incurred BETWEEN CAST('$start_date' AS DATE) AND CAST('$end_date' AS DATE) ORDER BY date_incurred ASC");
		return $expenses;
	}
	
	public function get_parent_schedules_by_period_by_bus($parent_id,$start_date,$end_date, $bus_number)
	{
		$db = new Database();
		$schedules = $db->query("SELECT * FROM schedules JOIN agencies ON agencies.id = schedules.agency_id WHERE agencies.parent_id= ".$parent_id." AND bus_number= '".$bus_number."'  AND deleted = '0' AND departure_date BETWEEN CAST('$start_date' AS DATE) AND CAST('$end_date' AS DATE) AND status = 'departed' ORDER BY departure_date ASC");
		return $schedules;
	}
	
	public function get_parent_income_by_period_by_bus($parent_id,$start_date,$end_date,$bus_number)
	{
		$db = new Database();
		$income = $db->query("SELECT * FROM incomes JOIN agencies ON agencies.id = incomes.agency_id  WHERE  agencies.parent_id= ".$parent_id." AND bus_number = '".$bus_number."' AND deleted = '0' AND date_incurred BETWEEN CAST('$start_date' AS DATE) AND CAST('$end_date' AS DATE) ORDER BY date_incurred ASC");
		return $income;
	}
	
	public function get_ten_parent_current_schedules($parent_id){
			
		//active records used coz ORM seems trouble some 
		$db = new Database();
		$schedule_agency =$db->from('schedules')->join('agencies','agencies.id','schedules.agency_id','departure_date')->where('status','current')->where('deleted','0')->where('agencies.parent_id',$parent_id)->orderby('schedules.departure_date','desc')->orderby('schedules.departure_time','desc')->limit(10)->get();
		return $schedule_agency;
	}
	
	public function get_ten_parent_departed_schedules($parent_id){
			
		//active records used coz ORM seems trouble some 
		$db = new Database();
		$schedule_agency =$db->select('schedules.id AS schedID','schedules.bus_number','schedules.from','schedules.to','schedules.id','schedules.checked_out_time','departure_date')->from('schedules')->join('agencies','agencies.id','schedules.agency_id')->where('status','departed')->where('agencies.parent_id',$parent_id)->orderby('departure_date','desc')->limit(10)->get();
		return $schedule_agency;
	}
	
	
	
}
