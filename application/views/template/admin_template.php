<!DOCTYPE html>
<html lang="en">

	<head>
	   <!-- <meta http-equiv="content-type" content="text/html; charset=iso-8859-1" /> -->
		<title><?=$title?> - <?=Kohana::lang('backend.online_booking')?> | QuickTicket</title>
		<link rel="shortcut icon" href="<?=url::base()?>images/favicon.ico" type="image/x-icon" />
		<link rel="icon" href="<?=url::base()?>images/favicon.ico" type="image/x-icon" />
		
		<meta name="viewport" content="width=device-width, initial-scale=1.0">
		<meta name="description" content="Travel agency management">
		<meta name="keywords" content="travel, ticket, online, travel-agency">
		<meta name="author" content="@absalomshu">
		
		<link href="<?=url::base()?>css/bootstrap.css" rel="stylesheet">
		<link href="<?=url::base()?>css/metro_styles.css" rel="stylesheet">
		<link href="<?=url::base()?>css/style.css" rel="stylesheet">
		
		<link href="<?=url::base()?>css/datepicker.css" rel="stylesheet">
		<link href="<?=url::base()?>css/bootstrap-timepicker.min.css" rel="stylesheet"/>
		<link rel="stylesheet" href="<?=url::base()?>css/print.css" type="text/css" media="print">
		<link rel="canonical" href="http://www.quickticket.com" />
		<!--<link href="<?=url::base()?>css/bootstrap-responsive.css" rel="stylesheet">-->
		
		<script src="<?=url::base()?>js/jquery.js"></script>
		
							

<link rel="stylesheet" href="<?=url::base()?>css/bootstrap-multiselect.css" type="text/css"/>					
		
		
		<script type="text/javascript">
			function noBack()
			 {
				 //window.history.forward()
			 }
			noBack();
			window.onload = noBack;
			window.onpageshow = function(evt) { if (evt.persisted) noBack() }
			window.onunload = function() { void (0) }
		</script>
		
		
	</head>
	
	<!--oncontextmenu="return false" disables right-click on page.-->
	<body oncontextmenu="return true">
		
		<?=render::admin_header();?>
		<?php if($this->uri->segment(2) != "login"){?>	
			<!--slightly different template for login page-->
			<div id="sidebar-left" class="left-menu no-print span3" >
				<?php render::menu();?>
			</div>
			<div class="container span9" >
				<?=render::notices();?>
				<?=render::sub_menu();?>
				<?=$content?>
				<div class="footer " >
					<?=render::template_footer();?>
				</div>
			</div> 
		
		<?php }else{ ?>
			<div class="container span12 auto-center">
				<?=render::notices();?>
				<?=$content?>
				<div class="footer " >
					<?=render::template_footer();?>
				</div>
			</div> 
		
		<?php }?>
		<?php //render::breadcrumb();?>
	
		
	</body>
</html>
