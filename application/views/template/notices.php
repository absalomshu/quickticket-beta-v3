<?php defined('SYSPATH') or die('No direct script access'); ?>

	<div class="notice">
		<?php
			//determine what type of notice to display if at all
			//if it's on the complete schedule page, the notice has to be displayed twice. Above and below, so don't delete (get instead of get_once)
			if($this->uri->segment(2)=="complete_schedule")
			{
				$notice = $this->session->get('notice');
			}else{
				$notice = $this->session->get_once('notice');
				}
				if(!empty($notice)){ 
					if($notice['type'] == 'success'){?><div class="alert general success"><a class="close" data-dismiss="alert" href="#">&times;</a><?php echo $notice['message']; ?></div><?php }?>
				<?	if($notice['type'] == 'error'){?><div class="alert general error"><a class="close" data-dismiss="alert" href="#">&times;</a><?php echo $notice['message'];?></div><?php }?>
		<?}?>
	</div>