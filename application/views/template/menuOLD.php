<?php defined('SYSPATH') or die('No direct script access'); 


	if ($this->uri->segment(2) != 'login'  AND $this->uri->segment(2) != 'add_admin' AND $this->uri->segment(1) != 'manage'){
	$admin=Authlite::instance()->get_admin(); 

	$agency_id = $admin->agency_id;
	?>  <!--no-print prevents this section from creating whitespace when printing any page -->
		<div class="row-fluid no-print">
			<div class="navbar">
			  <div class="navbar-inner">
				<ul class="nav bold">
					
					
					<?php /*foreach($main_modules as $module)
					{ 	
						//Check if the admin is allowed to use that module
						$is_allowed_main_module = Admin_Model::admin_is_allowed_main_module($admin->username,$module->id);
						?>
							<li <?php if($is_allowed_main_module){if($this->uri->segment(1) == strtolower($module->Name)) {?> class="active"<?php }?>><a href="<?=url::site("$module->Url")?>"><i class="icon-home"></i> <?=$module->Name?> </a> <?php } ?></li> 

					<?php } */ ?>
					
					 <li  <?php if($_SESSION['ModuleAccess']['1']) {if($this->uri->segment(2) == 'main') {?> class="active"<?php }?>><a href="<?=url::site('admin')?>"><i class="icon-home"></i> <?=Kohana::lang('backend.home');?> </a> <?php }?></li> 
				  <li  <?php if($_SESSION['ModuleAccess']['2']){ if($this->uri->segment(2) == 'all_schedules') {?> class="active"<?php }?>><a href="<?=url::site('admin/all_schedules/current')?>"> <i class="icon-list"></i> <?=Kohana::lang('backend.schedules');?><?php if($this->total_pending_tickets) {?> <span class="badge badge-important"><?=$this->total_pending_tickets;?><?php }}?></span></a></li>					
					<li  <?php if($_SESSION['ModuleAccess']['3']) { if($this->uri->segment(1) == 'expenses') {?> class="active"<?php }?>><a href="<?=url::site('expenses/all')?>"><i class="icon-wrench"></i> <?=Kohana::lang('backend.expenses');}?></a></li>
		

				  <li  <?php if($_SESSION['ModuleAccess']['4'])  { if($this->uri->segment(1) == 'parcels') {?> class="active"<?php }?>><a href="<?=url::site('parcels/all/incoming')?>"><i class="icon-gift"></i> <?=Kohana::lang('backend.parcels');}?></a></li>
				  <li  <?php if($_SESSION['ModuleAccess']['5']) { if ($this->uri->segment(1) == 'buses') {?> class="active"<?php }?>><a href="<?=url::site('buses/all')?>"><i class="icon-bus"></i> <?=Kohana::lang('backend.buses');}?></a></li>
				  <?php //var_dump($this->total_pending_reminders);exit; ?>				  
				<li  <?php if($_SESSION['ModuleAccess']['6']) {if($this->uri->segment(1) == 'reminders') {?> class="active"<?php }?>><a href="<?=url::site('reminders/all')?>"><i class="icon-check"></i> <?=Kohana::lang('backend.reminders');?> <?php if($this->total_pending_reminders) {?> <span class="badge badge-important"><?=$this->total_pending_reminders;?><?php }}?></span></a></li>
				<li  <?php if($_SESSION['ModuleAccess']['7']) { if($this->uri->segment(1) == 'reports') {?> class="active"<?php }?>><a href="<?=url::site('reports')?>"><i class="icon-info-sign"></i> <?=Kohana::lang('backend.reports');}?></a></li>
				<li  <?php if($_SESSION['ModuleAccess']['8']) { if($this->uri->segment(1) == 'cash') {?> class="active"<?php }?>><a href="<?=url::site('cash/collections')?>"><i class="icon-money"></i> <?=Kohana::lang('backend.cash');}?></a></li>
				<li  <?php if($_SESSION['ModuleAccess']['9']) { if($this->uri->segment(1) == 'settings') {?> class="active"<?php }?>><a href="<?=url::site('settings')?>"><i class="icon-cog"></i> <?=Kohana::lang('backend.settings');}?></a></li>
				
				</ul>
				
			  </div>
			</div>
					
		</div>
		<?php }
		//Now, the menu for the MANAGE section online, since all us the same menu
		
		if ($this->uri->segment(1) == 'manage'){ ?>
			<div class="row-fluid">
			<div class="navbar">
			  <div class="navbar-inner">
				<ul class="nav bold">
				  <li  <?php if($this->uri->segment(2) == 'main') {?> class="active"<?php }?>><a href="<?=url::site('manage')?>"><i class="icon-home"></i> <?=Kohana::lang('backend.home')?> </a> </li> 
				  <li  <?php if($this->uri->segment(2) == 'expenses') {?> class="active"<?php }?>><a href="<?=url::site('manage/expenses')?>"><i class="icon-wrench"></i></i> <?=Kohana::lang('backend.expenses')?> </a> </li> 
				  <li  <?php if($this->uri->segment(2) == 'online_purchases') {?> class="active"<?php }?>><a href="<?=url::site('manage/online_purchases')?>"><i class="icon-shopping-cart"></i></i> <?=Kohana::lang('backend.online_purchases')?> </a> </li> 
				  <li  <?php if($this->uri->segment(2) == 'cash') {?> class="active"<?php }?>><a href="<?=url::site('manage/cash')?>"><i class="icon-money"></i></i> <?=Kohana::lang('backend.cash')?> </a> </li> 
				  <li  <?php if($this->uri->segment(2) == 'reports') {?> class="active"<?php }?>><a href="<?=url::site('manage/reports')?>"><i class="icon-info-sign"></i></i> <?=Kohana::lang('backend.reports')?> </a> </li> 
				  
				  <li  <?php if($this->uri->segment(2) == 'online_settings') {?> class="active"<?php }?>><a href="<?=url::site('manage/online_settings')?>"><i class="icon-cog"></i> <?=Kohana::lang('backend.online_settings')?> </a> </li> 
		
				</ul>
				
			  </div>
			</div>
					
		</div>
		
		
		<?php
		}
		?>