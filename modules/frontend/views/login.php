<?php defined('SYSPATH') or die('No direct script access'); ?> 
<!DOCTYPE html>
	

		<?php
		//determine what type of notice to display if at all
		$notice = $this->session->get_once('notice');
			if(!empty($notice)){ 
				if($notice['type'] == 'success'){?><div class="alert general success"><a class="close" data-dismiss="alert" href="#">&times;</a><?php echo $notice['message']; }?></div>
			<?	if($notice['type'] == 'error'){?><div class="alert short error"><a class="close" data-dismiss="alert" href="#">&times;</a><?php echo $notice['message']; }?></div>
		<?}?>
		<br/><br/>
		<div class="container">
		<div class="span3 "></div>
		<div class="span5 hero-unit prof-unit center"><br/>
			<h20>&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;<?=Kohana::lang('backend.welcome_user')?></h20>
				<form method="POST" autocomplete="off" action="<?=url::site('home/login')?>" class="form-inline" >
					<!--This is a specific hack to prevent browser from remembering login and password, as autocomplete is often by-passed-->
					<input style="display:none">
					<input type="password" style="display:none">
					
					<div class="span4 spacious">
						<div class="control-group">
						<label class="control-label lab" for="inputType">Email</label>
							<div class="controls large">
										<input type="text" placeholder="<?=Kohana::lang('backend.email')?>" style="width:300px;height:30px;" name="email" required/>
							</div>
						</div>
					</div>	
					<br/>
			
					<div class="span4 spacious">
						<div class="control-group">
						<label class="control-label lab" for="inputType">Password</label>
							<div class="controls large">
							<input type="password" autocomplete="off" class="input-small" placeholder="<?=Kohana::lang('backend.password')?>" style="width:300px;height:30px;" name="password" required >&nbsp;&nbsp;
							</div>
						</div>
					</div>
					<div class="span4 spacious">
						<div class="control-group">
						<label class="control-label lab" for="inputType"></label>
							<div class="controls large">
							<button class="btn btn-info" type="submit" class="btn btn-large "><?=Kohana::lang('backend.sign_in')?></button>	
							  <a href="<?=url::site('home/forgot_password')?>">Forgot password</a>
							</div>
						</div>
					</div>
						
				</form> 
			
		</div>
		</div>
		
