<?php  defined('SYSPATH') or die('No direct script access');
//Language
$lang=array(
// Home page

	'contact_us_page'=>'<p>

			<p><b>Feel free to Contact us with questions, about data, partnership, proposals and others.</b><br/>
			</p>
			
			<p><b>Office</b><br/>
			First Floor, Ngeke Plaza<br/>
			Clerks Quarters, Buea.<br/>
			CIDR Suite 001, Room 2. <br/>
			P.O. Box 58 Buea.<br/>
			South West Region.<br/>
			Republic of Cameroon. 
			</p>
			
			<p><b>Engineer\'s Lab</b><br/>
			Razel Quarters,<br/>
			Sandpit, Buea.<br/>
			South west Region.<br/>
			Republic of Cameroon. 
			</p>
			
			<p><b>Phone</b><br/>
			Fix: 00237 333-227-27 <br/>
			Mobile 00237 949-943-04, 00237 755-981-06<br/>
			</p>

			<p><b>You can write us directly at</b><br/>
			info@wasamundi.com, support@wasamundi.com
			</p>		
			</p><br />	',

	'our_mission_page'=>'<p>
		<b>Our Mission</b><br/> 
		Our ultimate mission is to help people, discover and connect with local businesses in their communities.
		
		<p><b>You should know</b></br> 
		At Wasamundi, you can find businesses, products, services and people in Cameroon.
		You can talk about the businesses you have been to, rate, add pictures for and share these on social media.
		You can add places, add photos for, talk about and share these on social media.
		Business owners can control their businesses.
		We currently serve local businesses across more than 5 Regions of Cameroon.: Buea, Limbe, Yaounde, Bamenda, Douala among others.
		Thanks to our love for innovation and experimentation at Wasamundi, we have been able to develop several first class products and services for our community and the world.
		</p>
		
		<p><b>Our Products or Apps</b><br/>
			<a href="http://wasahostel.com" target="_blank">wasaHOSTEL</a>, an online platform simplifying search for student housing.<br/>
			<a href="http://wasatexto.com" target="_blank">wasaTEXTO</a>, links peer to peer and businesses to consumers through instant text message updates worldwide.<br/>
			<a href="http://wasa.me" target="_blank">wasaME</a>, is a url shortener which provides businesses on wasamundi with a simplified unique web ID.
			Wasamundi also constructs websites for other companies.
		</p>

		
		Wasamundi.com is a product of Wasamundi, which was formed, in February, 2011.		
		</p>	<br />',
		
	'terms_page'=>'
	<p>
		
		
		
				
		<p><b>Terms of Service</b><br/>
		PLEASE READ THESE TERMS OF USE ("AGREEMENT" OR "TERMS OF SERVICE") CAREFULLY BEFORE USING THE WEBSITE AND SERVICES OFFERED BY wasamundi.com. ("WasaMundi"). THIS AGREEMENT SETS FORTH THE LEGALLY BINDING TERMS AND CONDITIONS FOR YOUR USE OF THE WEBSITE AT HTTP://WWW.WASAMUNDI.COM (THE "SITE") AND ALL SERVICES PROVIDED BY WasaMundi ON THE SITE. 
		By using the Site in any manner, including but not limited to visiting or browsing the Site, you (the "user" or "you") agree to be bound by this Agreement, including those additional terms and conditions and policies referenced herein and/or available by hyperlink. This Agreement applies to all users of the Site, including without limitation users who are entities, normal users, contributors of content, information and other materials or services on the Site. If you have any questions, please refer to the Contact Us section of the Site.
		WasaMundi is a venue to allow users, entities who comply with WasaMundi\'s policies to Browse, Chat with, Message Existing Entities and People in their town or respective towns where WasaMundi has indexed. WasaMundi aims to present a new way to move around your town. WasaMundi cannot guarantee the true identity, age, and nationality of a user. WasaMundi encourages you to communicate directly with the entities through the tools available on the Site. 
		</p>
		
		<p><b>User Eligibility</b><br/>
		Age:
		WasaMundi\'s services are available to users who are of the right age to use the internet as specified by the country which the user finds him/her self of which we shall call Lawfully Eligible. You represent and warrant that you are Lawfully Eligible and that all registration information you submit is accurate and truthful. WasaMundi may, in its sole discretion, refuse to offer access to or use of the Site to any person or entity and change its eligibility criteria at any time. This provision is void where prohibited by law and the right to access the Site is revoked in such jurisdictions. 
		<br/>Compliance:
		You agree to comply with all Local laws regarding online conduct and acceptable content. You are responsible for all applicable taxes. In addition, you must abide by WasaMundi\'s policies as stated in the Agreement as well as all other operating rules, policies and procedures that may be published from time to time on the Site by WasaMundi, each of which is incorporated herein by reference and each of which may be updated by WasaMundi from time to time without notice to you: 
		<br/>Privacy Policy:
		In addition, some services offered through the Site may be subject to additional terms and conditions promulgated by WasaMundi from time to time; your use of such services is subject to those additional terms and conditions, which are incorporated into this Agreement by this reference. 
		<br/>Password:
		Keep your password secure. You are fully responsible for all activity, liability and damage resulting from your failure to maintain password confidentiality. You agree to immediately notify WasaMundi of any unauthorized use of your password or any breach of security. You also agree that WasaMundi cannot and will not be liable for any loss or damage arising from your failure to keep your password secure. 
		<br/>Account Information:
		You agree not to provide your username and password information in combination to any other party other than WasaMundi without WasaMundi \'s express written permission. Entities, users have the right to keep their account information up-to-date and accurate at all times, including a valid email address. 
		<br/>Right to Refuse Service:
		User registration services are not available to temporarily or indefinitely suspended WasaMundi users. WasaMundi reserves the right, in WasaMundi\'s sole discretion, to ban Users who have been reported as Nuisance or Spammers. WasaMundi reserves the right to refuse service to anyone, for any reason, at any time. 
		</p>

		<p><b>Information Control</b><br/>
		WasaMundi controls the Content provided by users that is made available on WasaMundi. WasaMundi reserves the right after investigation to Remove any thing that is offensive to the public, including flagged reviews, Reported spam. Any Review that is flagged has up to 24hours before the proper action is taken. Please use caution, common sense, and practice safe surfing when using WasaMundi. While we try our very best to avoid errors, we cannot be held responsible for any outdated or wrong information on this website. WasaMundi shall do its best to verify entity before confirming them as deemed for formal transaction. 
		</p>

		<p><b>Privacy</b><br/>
		Except as provided in WasaMundi\'s Privacy Policy WasaMundi will not sell or disclose your personal information (as defined in the Privacy Policy) to third parties without your explicit consent. WasaMundi stores and processes Content on computers located in the United States that are protected by physical as well as technological security. 
		</p>

		<p><b>No Guarantee</b><br/>
		WasaMundi does not guarantee continuous, uninterrupted access to the Site, and operation of the Site may be interfered with by numerous factors outside WasaMundi\'s control. 
		</p>

		<p><b>Severability</b><br/>
		If any provision of this Agreement is held unenforceable, then such provision will be modified to reflect the parties\' intention. All remaining provisions of this Agreement shall remain in full force and effect. WasaMundi\'s Service WasaMundi reserves the right to modify or terminate the WasaMundi service for any reason, without notice, at any time. WasaMundi reserves the right to alter these Terms of Use or other Site policies at any time, so please review the policies frequently. If WasaMundi makes a material change WasaMundi will notify you here, by email, by means of a notice on our home page, or other places Wasamundi deems appropriate. What constitutes a "material change" will be determined at WasaMundi\'s sole discretion, in good faith, and using common sense and reasonable judgment. 
		</p>

		<p><b>Choice of Law</b><br/>
		This Agreement shall in all respects be interpreted and construed with and by the laws of the Republic of Cameroon. 
		</p>

		<p><b>Notices</b><br/>
		Except as explicitly stated otherwise, any notices shall be given by email to the email address you provide to WasaMundi (either during the registration process or when your email address changes or other circumstances as a service demands). Notice shall be deemed given 24 hours after email is sent, unless the sending party is notified that the email address is invalid. 
		</p>

		<p><b>Miscellaneous</b><br/>
		If there is any dispute about or involving the Site and/or the Service, these Terms of Service of WasaMundi, you agree that any such dispute will be governed by the laws of the Republic of Cameroon without regard to its conflict of law provisions
		The Terms of Service contain the entire agreement between you and WasaMundi regarding the use of the Site, and supersede any prior agreement between you and WasaMundi on such subject matter. The parties acknowledge that no reliance is placed on any representation made but not expressly contained in these Terms of Service.
		The failure of WasaMundi to exercise or enforce any right or provision of the Terms of Service shall not constitute a waiver of such right or provision. The failure of either party to exercise in any respect any right provided for herein shall not be deemed a waiver of any further rights hereunder.
		If any provision of the Terms of Service is found to be unenforceable or invalid, that provision shall be limited or eliminated to the minimum extent necessary so that the Terms of Service shall otherwise remain in full force and effect and enforceable.
		The Terms of Service are not assignable, transferable or sublicensable by you except with WasaMundi\'s prior written consent, but may be assigned or transferred by WasaMundi to any third party without restriction and without notice to you. Any assignment attempted to be made in violation of the Terms of Service shall be void.
		The clause headings in the Terms of Service are for convenience only and have no legal or contractual effect.
		</p>
		<br />
		
				
	
	',
	'privacy_page'=>'
		<p>

			<p><b>What This Privacy Policy Covers</b><br/>
			This privacy policy covers the use of the web site located at http://www.wasamundi.com and the services provided by WasaMundi.com. ("WasaMundi"), including how WasaMundi treats personal information that WasaMundi collects and receives. Pursuant to WasaMundi\'s Terms of Use, you must be country eligible(or \'Lawfully Eligible\') to use this site or have permission from a legal guardian.. This policy does not apply to the practices of third parties that WasaMundi does not own or control, or individuals that WasaMundi does not employ or manage. 
			Effective Date: 10/10/2010. 
			Information Collection and How it is Used
			</p>
			
			<p><b>General Information:</b><br/>
			WasaMundi will not sell or disclose your name or email address to third parties without your explicit consent, except as specified in this privacy policy. Your username, WasaMundi Id or alias is displayed throughout WasaMundi (and so available to the public) and is connected to all of your WasaMundi activity. Other people can see your reviews, ratings and associated comments. WasaMundi collects personal information when you register to use WasaMundi services or to post content on WasaMundi. At registration you are asked for your email address and certain other optional information. If you would like to obtain a Verified account, WasaMundi will ask for your entity address. WasaMundi automatically receives and records information on our server logs from your browser, including your IP address, cookies, and the pages you request. This data is only used in the aggregate. 
			Service-related Announcements:
			WasaMundi will send you a welcome/confirmation email when you sign up for WasaMundi\'s services. Receipt of this email is required for using WasaMundi\'s services. WasaMundi will send you service-related announcements on rare occasions when it is necessary to do so. WasaMundi will also send entities who are not yet indexed on the Venue email notifications when a user attempts to leave a message to the entity on the venue. Administrative Messages WasaMundi may periodically email you with administrative messages. WasaMundi will not sell or rent your personal information to third parties for their marketing purposes without your explicit consent. Your information may be combined with information WasaMundi collects from other companies and used to improve and personalize services, content and advertising. If you don\'t wish to receive administrative messages or participate in these programs, simply opt-out by unsubscribe. 
			</p>

			<p><b>Choice/Opt-out</b><br/>
			If you no longer wish to receive administrative messages, you may opt-out by unsubscribe. Please note that if you do not wish to receive administrative messages from us, those notices will still govern your use of the WasaMundi website and service, and you are responsible for reviewing such notices for changes. 
			</p>

			<p><b>Chat</b><br/>
			If you use chat service on this site, you should be aware that any personally identifiable information you submit there can be read, collected, or used by others or could be used to send you unsolicited messages. WasaMundi is not responsible for the personally identifiable information you choose to submit here. 
			Editing and Deleting Account Information
			You can change your Wasamundi Account Information (except your username or email), at any time. Wasamundi may send you certain communications relating to the Wasamundi service, such as service-related announcements and administrative messages that are considered part of your Wasamundi account. If your personally identifiable information changes you may correct or update it by making the change on Your Wasamundi profile Page. 
			</p>
			
			<p><b>Security</b><br/>
			The security of your personal information is important to us. Your WasaMundi account information is protected by a password for your privacy and security. You need to work to protect against unauthorized access to your password and to your computer by signing off once you have finished using a shared computer. WasaMundi follows generally accepted industry standards to protect the personal information submitted to us, both during transmission and once WasaMundi receives it. No method of transmission over the Internet, or method of electronic storage, is 100% secure. Therefore, while WasaMundi strives to use commercially acceptable means to protect your personal information, WasaMundi cannot guarantee its absolute security. If you have any questions about security on WasaMundi Web site, you can email WasaMundi at support@wasamundi.com. 
			</p>
			
			<p><b>Changes to this Privacy Policy</b><br/>
			If the privacy policy is changed, WasaMundi will post changes so that you are aware of those changes. WasaMundi reserves the right to modify this privacy statement at any time, so please review it frequently. If WasaMundi makes material changes to this policy you will be notified here, by email, or other places WasaMundi deems appropriate. 
			</p>
			
			<p><b>Questions</b><br/>
			If you have questions or suggestions you can contact WasaMundi Support Team via our Help or Contact Us or write to support@wasamundi.com.
			</p>		
			
	</p>
	
	
	
	',
	
	
);
