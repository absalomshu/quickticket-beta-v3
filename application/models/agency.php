<?php defined('SYSPATH') or die('No direct script access');

class Agency_Model extends ORM{
	
	public function get_open_ticket_agencies_from_town($town_id)
	{
		$agencies=ORM::factory('agency')->where('allow_open_tickets','1')->where('town_id',$town_id)->find_all();
		
		return $agencies;
	}
	
	public function get_open_ticket_agencies_from_and_to_town($town_from,$town_to)
	{
		$open_ticket_agencies=ORM::factory('agency')->join('open_tickets','open_tickets.agency_id = agencies.id')->where('allow_open_tickets','1')->where('town_from',$town_from)->where('town_to',$town_to)->find_all();
		
		return $open_ticket_agencies;
	}


}
