<?php defined('SYSPATH') or die('No direct script access'); ?> 


	<?php
		//determine what type of notice to display if at all
		$notice = $this->session->get_once('notice');
			if(!empty($notice)){ 
				if($notice['type'] == 'success'){?><div class="alert general success"><a class="close" data-dismiss="alert" href="#">&times;</a><?php echo $notice['message']; }?></div>
			<?	if($notice['type'] == 'error'){?><div class="alert general error"><a class="close" data-dismiss="alert" href="#">&times;</a><?php echo $notice['message']; }?></div>
		<?}?>
    <div class="container">
	  	
	<div class="row-fluid">	
		
		<div class="span12  hero-unit prof-unit">
		
		<ul class="nav nav-tabs">
					<li > <a href="<?=url::site('expenses/all')?>"><i class="icon-arrow-left"></i> <?=Kohana::lang('backend.back_expenses')?></a></li>
		</ul>											
					
				<?php foreach($expense as $expense):
				//It's a join, so use for each
			//var_dump($expense);exit;
				?>
					<form action="<?=url::site('expenses/edit/'.$expense->id)?>" method="POST" name="theform" class="span8 offset2">
						<legend><?=Kohana::lang('backend.edit_expense')?></legend>
							<!--<div class="" >
								<div class="span4 text-right">Date: </div>
									<div class="input-append date span8">
										<input type="text" name = 'date_incurred' class="span5 datepicker"  value="<?=date('d-m-Y')?>" id="dp1" required>
										<span class="add-on"><i class="icon-calendar"></i></span>
									</div>
							</div>-->
							
							<div class="">
								<div class="span4 text-right"><?=Kohana::lang('backend.bus_no')?>: </div>
								<div class="span8">
								<select name="bus_number" id="inputType">
										<option value=""> None</option>
										<?php foreach($all_buses as $bus){?>
											<option value="<?=$bus->bus_number?>" <?php if($bus->bus_number==$expense->bus_number){echo "selected";}?>> <?=$bus->bus_number?></option>
											
										<?php }?>
								</select>
								</div>
							</div>
							
							<div class="">
								<div class="span4 text-right"><?=Kohana::lang('backend.authorised_by')?>: </div>
								<div class="span8"><input type="text"  name="authorised_by" value="<?=$expense->authorised_by?>"></div>
							</div>
							
							<div class="">
								<div class="span4 text-right"><?=Kohana::lang('backend.amount')?>: <span class="red"> *</span></div>
								<div class="span8 controls input-append"><input class="span5 IsAmount" type="text"  name="amount" value="<?=$expense->amount?>" required><span class="add-on"> FCFA</span></div>
							</div>
							
							<div class="">
								<div class="span4 text-right"><?=Kohana::lang('backend.purpose')?>: <span class="red"> *</span></div>
								<div class="span8"><textarea placeholder="<?=Kohana::lang('backend.spent_on')?>"  name="purpose"  required ><?=$expense->purpose?></textarea></div>
							</div>
							<div class="">
							<div class="span4 text-right"><?=Kohana::lang('backend.category')?>: </div>
							<div class="span8">
							<select name="category_id" id="inputType">
									<?php foreach($all_expense_categories as $category){
										
										
										?>
										<option value="<?=$category->id?>" <?php if($category->id==$expense->category_id){echo "selected";}?>> <?=$category->name?></option>
										
									<?php }?>
							</select>
							</div>
						</div>
							
							<div class="clear"></div>
							
							<div class="form-actions">
								<div class="span4"></div>
								<div class="span8">
									<button class="btn btn-info " type="submit"><?=Kohana::lang('backend.save')?></button>
									<a href="<?=url::site('expenses/all')?>" class="btn" type="submit"> <?=Kohana::lang('backend.cancel')?></a>
								</div>
							</div>
					</form>
					<?php endforeach;				?>
					
						
						
							
						

						<!--end the first tab -->
				</div>
						
		</div>
	
		
		</div>
