<?php defined('SYSPATH') or die('No direct script access');

class Bus_Model extends ORM
{

	public function add_bus($agency_id, $bus_number, $serial_number, $bus_name, $bus_seats, $driver, $motorboy, $vip, $other_infos, $bus_photo)
	{
		$bus = ORM::FACTORY('bus');	
		$bus->agency_id = $agency_id;
		$bus->bus_number = $bus_number;
		$bus->serial_number = $serial_number;
		$bus->bus_name = $bus_name;
		$bus->bus_seats = $bus_seats;
		$bus->driver = $driver;
		$bus->motorboy = $motorboy;
		$bus->vip = (int)$vip;
		$bus->other_infos = $other_infos;
		$bus->photo = $bus_photo;
		$bus->save();	
	}
	
	public function edit_bus($agency_id, $bus_number, $serial_number, $bus_name, $bus_seats, $driver, $motorboy, $vip, $other_infos, $bus_photo)
	{
		$bus=ORM::factory('bus')->where('bus_number',$bus_number)->where('agency_id',$agency_id)->find();
		$bus->bus_name = $bus_name;
		$bus->serial_number = $serial_number;
		$bus->bus_seats = $bus_seats;
		$bus->driver = $driver;
		$bus->motorboy = $motorboy;
		//integer NOT boolean. Boolean when false, will leave an empty vip field instead of a 0
		$bus->vip = (int)$vip;
		$bus->other_infos = $other_infos;
		$bus->photo = $bus_photo;
		$bus->save();	
	}
	
	public function make_available($agency_id, $bus_number)
	{
		$bus=ORM::factory('bus')->where('bus_number',$bus_number)->where('agency_id',$agency_id)->find();
		$bus->is_available = '1';
		
		$bus->save();	
	}
	public function make_unavailable($agency_id, $bus_number)
	{
		$bus=ORM::factory('bus')->where('bus_number',$bus_number)->where('agency_id',$agency_id)->find();
		$bus->is_available = '0';
		
		$bus->save();	
	}
	
	public function get_all($agency_id)
	{
		$buses=ORM::factory('bus')->where('agency_id',$agency_id)->where('deleted','0')->find_all();
		return $buses;	
	}
	
	public function count_all($agency_id)
	{
		$buses=ORM::factory('bus')->where('agency_id',$agency_id)->find_all();
		return count($buses);	
	}
	
	
	public function get_available($agency_id)
	{
		$buses=ORM::factory('bus')->where('agency_id',$agency_id)->where('is_available','1')->find_all();
		return $buses;	
	}
	
	public function get_unavailable($agency_id)
	{
		$buses=ORM::factory('bus')->where('agency_id',$agency_id)->where('is_available','0')->find_all();
		return $buses;	
	}
	
	public function count_available($agency_id)
	{
		$count=ORM::factory('bus')->where('agency_id',$agency_id)->where('is_available','1')->find_all();
		return count($count);	
	}
	
	public function count_unavailable($agency_id)
	{
		$buses=ORM::factory('bus')->where('agency_id',$agency_id)->where('is_available','0')->find_all();
		return count($buses);	
	}
	public function is_vip($agency_id, $bus_number)
	{
		$bus=ORM::factory('bus')->where('agency_id',$agency_id)->where('bus_number',$bus_number)->find();
		return $bus->vip;	
	}
	

}
