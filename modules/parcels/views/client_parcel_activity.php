<?php defined('SYSPATH') or die('No direct script access'); ?>
	
    <div class="container">
		
		
		<div class="row-fluid  span12 hero-unit prof-unit reload" style="margin:5px 0 0px 0;">	
		
			
			
			<div class="span12" >
			<div class="no-print">
			
			<ul class="nav nav-tabs">
					<li > <a href="<?=url::site('reports')?>"><i class="icon-arrow-left"></i> <?=Kohana::lang('backend.back_reports')?></a></li>
				</ul>
			</div>
			<div class="span10 offset1 print-area" >
			
				
				<legend><?=Kohana::lang('backend.summary_for'). " <i>\"".$passenger_name."\"</i>  IDC : <i>"; 
				if(!empty($passenger_idc)){echo $passenger_idc;};echo"</i>"?></b>
				<button class="view" onClick="window.print()"><i class="icon-print"></i> <?=Kohana::lang('backend.print')?></button>
				</legend>
				<table class="table table-bordered table-condensed">
					<tr>
						<th>Date</th>
						<th>Name</th>
						<th>IDC</th>
						<th >Phone</th>
						<th >From</th>
						<th >To</th>
						<th >Ticket price</th>
						<th >Remark</th>
					
					</tr>
					<?php 
					
					$ticket_total=0;
					foreach ($search_results as $result)
					{
						$ticket_total += $result->Price;
						?>
					<tr>
						<td><?=date("d-m-Y",strtotime($result->departure_date));?></td>
						<td><?=$result->ClientName?></td>
						<td><?php if(!empty($result->ClientIDC)){echo $result->ClientIDC;}?></td>
						<td><?php if(!empty($result->ClientPhone)){echo $result->ClientPhone;}?></td>
						<td><?=get::town($result->from)?></td>
						<td><?=get::town($result->to)?></td>
						<td><?=$result->Price?></td>
						<td>
							<?php 
								if($result->MissedStatus){echo "<b>Missed</b>";}
								if($result->FreeTicket){echo "<b>Free ticket</b>";}
								if($result->FreeTicketReason){echo "- " .$result->FreeTicketReason;}
							?>
						</td>
					
			
					</tr>
					<?php }?>
				
						<tr>
						
							<th colspan=6><?=Kohana::lang('backend.total')?></th>
							<th class="span2 text-right" ><?=number_format($ticket_total)?> FCFA</th>
							<th></th>
						</tr>
				</table>
				
					
			
				
			</div>
		</div>
			
		
		
		
		
		
		
		</div>
    </div> 
