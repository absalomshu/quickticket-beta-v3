<?php defined('SYSPATH') or die('No direct script access'); 

	//Forever prevent caching of the header as it contains balances that need to be updated constantly
	header("Last-Modified: " . gmdate("D, d M Y H:i:s") . " GMT");
	header("Cache-Control: no-store, no-cache, must-revalidate"); // HTTP/1.1
	header("Cache-Control: post-check=0, pre-check=0", false);
	header("Pragma: no-cache"); // HTTP/1.0
	header("Expires: Sat, 26 Jul 1997 05:00:00 GMT"); // Date in the past

	
$admin=Authlite::instance()->get_admin(); 
			
	
	
	if (empty($admin)) {?>
	
	<div class="admin-head-small">
		<div class="admin-head-content">
			<div class="logo">
				<a href="#"><img src="<?=url::base()?>images/logo-white.png" alt="QuicTicket"/></a>
			</div>
		
			<div class="header-right">
				<div class="agency-info">
					<?=Kohana::lang('backend.adm_backend')?>
				</div>
			</div>
		</div>
	</div>
	
	<?php }

	else {?>
	<!--prevent from taking up space once printing-->

	<div class="admin-head-small no-print">
		<div class="admin-head-content">
			<div class="logo">
				<a href="#"><img src="<?=url::base()?>images/logo-white.png" alt="QuickTicket"/></a>
			</div>
			
			<div class="header-right">
				<div class="agency-info">
					<?php 
					//If it is a General Manager, don't show a town. Just show the parent agency name.
					
					if($admin->admin_group_id == 4){
						echo get::_parent($admin->agency_id)->name;
					}else{
						echo get::agency_name($admin->agency_id);
					}
					
					?>
				</div>
				<div class="user-info">
					<span title="<?=number_format($admin->available_balance)?> FCFA" >
					
					
						<div class="dropdown">
						  <a class="dropdown-toggle white" data-toggle="dropdown" href="#"><i class="icon-user" > </i> <?=$admin->username?>  <b class="caret"></b></a>
						  <ul class="dropdown-menu pull-right" role="menu" aria-labelledby="dLabel">
							
							
							<li> <a><?=Kohana::lang('backend.administrator')?></a></li>
							<li> <a><?=Kohana::lang('backend.balance')?>:  <b><?=number_format($admin->available_balance)?> FCFA</b></a></li>
							<li class="divider"></li >
							<li><a href="<?=url::site('admin/logout')?>"><?=Kohana::lang('backend.logout')?></a></li >
							
						  </ul>
						</div>
					<!--<span class="white"><a href="<?=url::site('admin/logout')?>"><?=Kohana::lang('backend.logout')?></a></span>-->
				</div>
				
			</div>
		</div>
	</div>
	<!--This is the header that should appear when printing any document.
		Made to have the same color as the background so it doesn't show on the normal
	-->
	<div class="print-area" style="position:absolute; left:0px; color:#f8f8f8; font-size:16px;" >
		<?php //strtoupper(get::agency_name($admin->agency_id))?>
	</div>

<?php }?>

