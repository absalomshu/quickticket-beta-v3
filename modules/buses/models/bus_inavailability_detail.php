<?php defined('SYSPATH') or die('No direct script access');

class Bus_inavailability_detail_Model extends ORM
{

	public function add_start($agency_id, $bus_number, $start_date, $reason, $admin)
	{	
		$bus = ORM::FACTORY('bus_inavailability_detail');	
		$bus->agency_id = $agency_id;
		$bus->bus_number = $bus_number;
		$bus->reason = $reason;
		$bus->start_date = $start_date;
		$bus->created_by = $admin;
		$bus->start_time = date('H:i:s');
		$bus->save();	
	}
	
	public function add_end($agency_id, $bus_number, $end_date,$admin)
	{	
		$old_end_date=null;
		$bus = ORM::FACTORY('bus_inavailability_detail')->where('agency_id',$agency_id)->where('bus_number',$bus_number)->where('end_date',$old_end_date)->find();	
		$bus->agency_id = $agency_id;
		$bus->bus_number = $bus_number;
		$bus->end_date = $end_date;
		$bus->end_time = date('H:i:s');
		$bus->modified_by = $admin;
		$bus->save();	
	}

	public function get_all_for_bus($agency_id, $bus_number, $start_date, $end_date)
	{
		$db = new Database();
		$details = $db->query("SELECT * FROM bus_inavailability_details WHERE agency_id= ".$agency_id." AND start_date BETWEEN CAST('$start_date' AS DATE) AND CAST('$end_date' AS DATE) ORDER BY start_date ASC");
		return $details;
		
		
		//$details=ORM::factory('bus_inavailability_detail')->where('agency_id',$agency_id)->where('bus_number',$bus_number)->find_all();
		//return $details;	
	}
	public function get_current($agency_id, $bus_number)
	{	$old_end_date=null;
		$details=ORM::factory('bus_inavailability_detail')->where('agency_id',$agency_id)->where('bus_number',$bus_number)->where('end_date',$old_end_date)->find();
		return $details;	
	}
	
	public function count_all($agency_id)
	{
		$buses=ORM::factory('bus')->where('agency_id',$agency_id)->find_all();
		return count($buses);	
	}
	
	/*
	public function get_available($agency_id)
	{
		$buses=ORM::factory('bus')->where('agency_id',$agency_id)->where('is_available','1')->find_all();
		return $buses;	
	}
	
	public function get_unavailable($agency_id)
	{
		$buses=ORM::factory('bus')->where('agency_id',$agency_id)->where('is_available','0')->find_all();
		return $buses;	
	}
	
	public function count_available($agency_id)
	{
		$count=ORM::factory('bus')->where('agency_id',$agency_id)->where('is_available','1')->find_all();
		return count($count);	
	}
	
	public function count_unavailable($agency_id)
	{
		$buses=ORM::factory('bus')->where('agency_id',$agency_id)->where('is_available','0')->find_all();
		return count($buses);	
	}
	public function is_vip($agency_id, $bus_number)
	{
		$bus=ORM::factory('bus')->where('agency_id',$agency_id)->where('bus_number',$bus_number)->find();
		return $bus->vip;	
	}
	
*/
}
