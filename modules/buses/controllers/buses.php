<?php defined('SYSPATH') or die('No direct script access');

//Bus number is the primary key, as no two buses can have the same number
//ORM seems to require that an id column be available for a table to be updated, hence id is used for bus_number

class Buses_Controller extends Admin_Controller 
{
		
	protected $logo_directory = 'images/ticket/agency-logos/';
	//public $template = 'template/admin_template';
	
	
	public function __construct()
	{	
		parent::__construct();
		//$this->admin = Authlite::instance()->get_admin();
		$this->session = Session::instance();
		//if (!empty($this->admin)) $this->key = $this->admin->agency_id;
		
	}  
	
	
	public function index()
	{	
		//ensure a logged in user can't land on the signup page
		if (empty($this->admin)) 
		{
			url::redirect('admin/login');
		}else
		{
			$this->all();
		}
	}
	
	
	
	public function all()
	{
		Authlite::check_admin();
		Authlite::verify_referer();
	
	
		$agency_id = $this->agency_id;
		$this->template->title = Kohana::lang('backend.all_buses');;
		$parent_id = get::_parent($this->agency_id)->id;	



		//$rentals = Income_Model::count_bus_rentals_today($agency_id);
		//var_dump($rentals);exit;		
		
		//pagination
		$per_page = 15;		
		$all_buses = get::all_buses($agency_id, $parent_id, 4);				
		$all_buses_total = count($all_buses);
		
		$this->pagination = new Pagination(array(
			// 'base_url'    => 'welcome/pagination_example/page/', // base_url will default to current uri
			'uri_segment'    => 'page', // pass a string as uri_segment to trigger former 'label' functionality
			'total_items'    => $all_buses_total, // use db count query here of course
			'items_per_page' => $per_page , // it may be handy to set defaults for stuff like this in config/pagination.php
			'style'          => 'punbb', // pick one from: classic (default), digg, extended, punbb, or add your own!
			//'query_string'   => "ORM::factory('schedule')->where('agency_id',$agency_id)->where('status','current')->find_all()"
		));
			
		$all_buses_per_page=ORM::factory('bus')->where('agency_id',$agency_id)->where('deleted','0')->orderby('bus_number','DESC')->limit($per_page,$this->pagination->sql_offset)->find_all();
		$view = new View('all_buses');
		$view->all_buses = $all_buses_per_page;
		$this->template->content = $view;
	}
	
	public function rentals()
	{
		Authlite::check_admin();
		Authlite::verify_referer();
		
		$agency_id = $this->agency_id;
		$this->template->title = Kohana::lang('backend.all_buses');;
		$parent_id = get::_parent($this->agency_id)->id;		
		
		//pagination
		$per_page = 15;		
		$all_buses = ORM::factory('income')->where('agency_id',$agency_id)->where('purpose','bus_rental')->orderby('date_incurred','DESC')->find_all();				
		$all_buses_total = count($all_buses);
		
		$this->pagination = new Pagination(array(
			// 'base_url'    => 'welcome/pagination_example/page/', // base_url will default to current uri
			'uri_segment'    => 'page', // pass a string as uri_segment to trigger former 'label' functionality
			'total_items'    => $all_buses_total, // use db count query here of course
			'items_per_page' => $per_page , // it may be handy to set defaults for stuff like this in config/pagination.php
			'style'          => 'punbb', // pick one from: classic (default), digg, extended, punbb, or add your own!
			//'query_string'   => "ORM::factory('schedule')->where('agency_id',$agency_id)->where('status','current')->find_all()"
		));
			
		$all_bus_rentals_per_page=ORM::factory('income')->where('agency_id',$agency_id)->where('purpose','bus_rental')->orderby('start_date','DESC')->limit($per_page,$this->pagination->sql_offset)->find_all();
		$view = new View('all_buses');
		$view->all_rentals = $all_bus_rentals_per_page;
		$this->template->content = $view;
	}
	
	
	function add_bus()
	{	
		Authlite::check_admin();
		Authlite::verify_referer();

		$this->template->title=Kohana::lang('backend.add_bus');
		$photo_directory = 'images/buses/';
		
		//$db = new Database();
		$bus = ORM::FACTORY('bus');	
		if($_POST){
			$post = new Validation($_POST);
			$post->add_rules('bus_number','required','valid::alpha_numeric');
			$post->add_rules('serial_number','valid::alpha_numeric');
			
			if ($post->validate())
			{
				//remove white spaces and check is number exists
				$bus_seats = $_POST['bus_seats'];
				$serial_number = $_POST['serial_number'];
				$bus_number = str_replace(" ","" ,strtoupper($_POST['bus_number']));
				$driver = $_POST['driver'];
				$bus_name = $_POST['bus_name'];
				$motorboy = $_POST['motorboy'];
				$other_infos = $_POST['other_infos'];
				$vip = $_POST['vip'];
				//intialize so if there's no photo, the model doesn't return an error
				$bus_photo = "";
				
				$bus_exists = ORM::FACTORY('bus')->where('bus_number',$bus_number)->where('deleted','0')->count_all($this->agency_id);
				//ensure the bus number is unique
				if ($bus_exists)
				{
					$this->session->set('notice', array('message'=>Kohana::lang('backend.bus_exists'),'type'=>'error'));
				}else
				{	
					//if a picture is posted and it's not nameless
					if($_FILES['bus_photo']['error'] == 0 AND strlen($_FILES['bus_photo']['name'] )>0)
					{
						$files = new Validation($_FILES);
						$files->add_rules('bus_photo', 'upload::valid', 'upload::required', 'upload::type[gif,jpg,png]', 'upload::size[5M]');
						if ($files->validate())
						{	
							$photo = $_FILES['bus_photo'];
							
							// Temporary file name - the name of the company without whitespaces
							$filename = upload::save($photo, strtolower(str_replace(" ",'', $bus_number)));
							
							// Resize, sharpen, and save the image
							Image::factory($filename)
								->resize(100, 100, Image::WIDTH)
								->save($photo_directory.basename($filename).'.jpg');
								//$bus->photo = $photo_directory.basename($filename).'.jpg';
								$bus_photo = $photo_directory.basename($filename).'.jpg';
							// Remove the temporary file
							unlink($filename);
							
						}
						
					}
				
				
					Bus_Model::add_bus($this->agency_id, $bus_number, $serial_number, $bus_name, $bus_seats, $driver, $motorboy, $vip, $other_infos, $bus_photo);
					$this->session->set('notice', array('message'=>Kohana::lang('backend.bus_registered'),'type'=>'success'));
				}
			}
				else
				{
					$errors=$post->errors('errors');
					$notice="";
						foreach($errors as $error) 
						{
							$notice.=$error."<br />";
						}
						$this->session->set('notice', array('message'=>$notice,'type'=>'error'));
				}
			
				url::redirect('buses/all');
			}
		
		$view=new View('all_buses');
		$view->bus=$bus;
		//$view->logo = $logo;
		$this->template->content=$view;
			
	}

	
	function edit_bus($bus_number)
	 {
			Authlite::check_admin();
			Authlite::verify_referer();
			//Authlite::check_integrity($schedule_id);
			$this->template->title=Kohana::lang('backend.edit_company');
			$bus = get::bus($bus_number);	
			$photo_directory = 'images/buses/';
			$current_photo=get::bus_photo($bus_number);

		if($_POST)
		{
			$post = new Validation($_POST);
			$post->add_rules('serial_number','valid::alpha_numeric');
			
			if ($post->validate())
			{	
				$bus_seats = $_POST['bus_seats'];
				$serial_number = $_POST['serial_number'];
				$driver = $_POST['driver'];
				$vip = $_POST['vip'];
				$other_infos = $_POST['other_infos'];
				$motorboy = $_POST['motorboy'];
				$bus_name = $_POST['bus_name'];
				
				//iniialize
				$bus_photo='';
				
				//if a picture is posted and it's not nameless
				if($_FILES['bus_photo']['error'] == 0 AND strlen($_FILES['bus_photo']['name'] )>0)
				{
					$files = new Validation($_FILES);
					$files->add_rules('bus_photo', 'upload::valid', 'upload::required', 'upload::type[gif,jpg,png]', 'upload::size[5M]');
					if ($files->validate())
					{	
						$photo = $_FILES['bus_photo'];
						
						// Temporary file name - the name of the company without whitespaces
						$filename = upload::save($photo, strtolower(str_replace(" ",'', $bus_number)));
						
						// Resize, sharpen, and save the image
						Image::factory($filename)
							->resize(100, 100, Image::WIDTH)
							->save($photo_directory.basename($filename).'.jpg');
							$bus_photo = $photo_directory.basename($filename).'.jpg';
						// Remove the temporary file
						unlink($filename);
						
					}
						
				}
					
					Bus_Model::edit_bus($this->agency_id, $bus_number, $serial_number, $bus_name, $bus_seats, $driver, $motorboy, $vip, $other_infos, $bus_photo);
					
					
					$this->session->set('notice', array('message'=>Kohana::lang('backend.bus_modified'),'type'=>'success'));
			}
			else
			{
				$errors=$post->errors('errors');
				$notice="";
					foreach($errors as $error) 
					{
						$notice.=$error."<br />";
					}
					$this->session->set('notice', array('message'=>$notice,'type'=>'error'));
			}
				
			url::redirect('buses/all');
		}
		
			
		$view=new View('edit_bus');
		$view->bus=$bus;
		$view->current_photo=$current_photo;
		//$view->logo = $logo;
		$this->template->content=$view;		
	}
	
	function maintain($bus_number)
	 {
			Authlite::check_admin();
			Authlite::verify_referer();
			$this->template->title=Kohana::lang('backend.edit_company');
			$bus = get::bus($bus_number);	
			$photo_directory = 'images/buses/';
			$current_photo=get::bus_photo($bus_number);
			$bus_requirements=Bus_Requirement_Model::get_all($this->agency_id);
			$reminders_for_bus=Reminder_Model::get_reminders_for_bus($this->agency_id, $bus_number);

		if($_POST)
		{
			$post = new Validation($_POST);
			if ($post->validate()){	
					//for each requirment he selected, set value to 1 in db
					foreach($_POST['all_requirements'] as $requirement_id=>$value){	
						//Bus_Requirements_Status_Model::update_status($this->agency_id, $bus_number, $requirement_id, $value);
						
						
						//If the date is set, format it, if it's zero, don't. Else it'll insert january 1st 1970
						if($_POST['expiry_dates'][$requirement_id]){
							$expiry_date = date("Y-m-d",strtotime($_POST['expiry_dates'][$requirement_id]));
						}else{
							$expiry_date=NULL;
						}
						//Also, if the value is zero, cancel the date
						if($value==0){
							$expiry_date=NULL;
						}
						Bus_Requirements_Status_Model::update_status($this->agency_id, $bus_number, $requirement_id, $value, $expiry_date);
					}
					//for each requirment, add the expiry date
					foreach($_POST['expiry_dates'] as $requirement_id=>$expiry_date){	
						
					}
					
					$this->session->set('notice', array('message'=>Kohana::lang('backend.bus_modified'),'type'=>'success'));
			}
			else
			{
				$errors=$post->errors('errors');
				$notice="";
					foreach($errors as $error) 
					{
						$notice.=$error."<br />";
					}
					$this->session->set('notice', array('message'=>$notice,'type'=>'error'));
			}
				
			url::redirect('buses/maintain/'.$bus_number);
		}
		
			
		$view=new View('maintain_bus');
		$view->bus_requirements=$bus_requirements;
		$view->bus=$bus;
		$view->reminders_for_bus=$reminders_for_bus;
		$this->template->content=$view;		
	}
	
	public function add_bus_rental($bus_number)
	{	
		Authlite::check_admin();
		Authlite::verify_referer();
		
		$agency_id = $this->agency_id;
		$parent_id = get::_parent($agency_id)->id;
		$admin_group_id = $this->admin->admin_group_id;
		$this->template->title=Kohana::lang('backend.register_expense');
		
	
		if($_POST)
		{	
			
			$post = new Validation($_POST);
			$post->add_rules('rented_to','required');
			$post->add_rules('start_date','required');
			$post->add_rules('end_date','required');
			$post->add_rules('amount','required','valid::numeric');
						
			if ($post->validate())
			{	
				$bus_number = strtoupper(str_replace(" ","",$bus_number));
				$amount = $_POST['amount'];
				$rented_to = $_POST['rented_to'];
				$description = htmlentities($_POST['description'],ENT_QUOTES);
				$start_date = date("Y-m-d",strtotime($_POST['start_date']));
				$end_date = date("Y-m-d",strtotime($_POST['end_date']));
				
				if(strtotime($start_date) > strtotime($end_date)){
						$this->session->set('notice', array('message'=>'Start date must be earlier than end date','type'=>'error'));
						url::redirect('buses/add_bus_rental/'.$bus_number);
					}

				$CreatedBy = strtoupper($this->admin->username);			
				
				//Add income
				Income_Model::register_bus_rental($this->agency_id, $bus_number, $rented_to, $amount, $description, $start_date, $end_date, strtoupper($this->admin->username));
				
				//Make bus
				
				$this->session->set('notice', array('message'=>Kohana::lang('backend.saved') ,'type'=>'success'));
				url::redirect('buses/all');	
							
			}
			else
			{
				$errors=$post->errors('errors');
				$notice="";
					foreach($errors as $error) 
					{
						$notice.=$error."<br />";
					}
					$this->session->set('notice', array('message'=>$notice,'type'=>'error'));
					url::redirect('expenses');	
			}
		}
		
		$view = new View('add_bus_rental');
		$view->bus_number = $bus_number;
		$this->template->content = $view;
	
	}
	
	
	public function delete_bus($bus_number)
	{	
		//DO NOT USE DELETED. SEE QT MANUAL
		Authlite::check_admin();
		Authlite::verify_referer();
		
		$this->template->title='';
		$bus = get::bus($bus_number);
		//$bus->deleted = 1;
		$bus->delete();
		//$bus->save();
		
		$this->session->set('notice', array('message'=>Kohana::lang('backend.bus'). "<b>$bus_number</b>". Kohana::lang('backend.deleted'),'type'=>'success'));
		url::redirect('buses/all');
	}
	
	public function make_available($bus_number)
	{	
		Authlite::check_admin();
		Authlite::verify_referer();
		$date = date('Y-m-d');
		
		$this->template->title='';
		$notice = Bus_Model::make_available($this->agency_id,$bus_number);
		//keep track of unavailabilty period
		Bus_inavailability_detail_Model::add_end($this->agency_id, $bus_number, $date,strtoupper($this->admin->username));
		
		$this->session->set('notice', array('message'=>Kohana::lang('backend.saved'),'type'=>'success'));
		url::redirect('buses/all');
	}
	
	public function make_unavailable($bus_number)
	{	
		Authlite::check_admin();
		Authlite::verify_referer();
		$date = date('Y-m-d');
		
		$this->template->title='';
		
		if($_GET)
		{
			$post = new Validation($_GET);
			if ($post->validate()){
				$reason = $_GET['reason'];
				//$bus_number = $_GET['bus_number'];
				//VAR_DUMP($bus_number);EXIT;
				Bus_Model::make_unavailable($this->agency_id,$bus_number);
				Bus_inavailability_detail_Model::add_start($this->agency_id, $bus_number, $date,$reason, strtoupper($this->admin->username));
				
				$this->session->set('notice', array('message'=>Kohana::lang('backend.saved'),'type'=>'success'));
				url::redirect('buses/all');
							
			}
			else
			{
				$errors=$post->errors('errors');
				$notice="";
					foreach($errors as $error) 
					{
						$notice.=$error."<br />";
					}
					$this->session->set('notice', array('message'=>$notice,'type'=>'error'));
			}
				
			url::redirect('buses/maintain/'.$bus_number);
		}
		
		
		
	
	}
	
	
	public function check_buses()
	{	
		Authlite::check_admin();
		Authlite::verify_referer();
		$this->template->title = Kohana::lang('backend.reports_for_bus');
		
		$parent_id = get::_parent($this->agency_id)->id;
		$view= new View('all_buses');
		//$all_buses = get::all_buses($this->agency_id, $parent_id, 4);	
		$all_buses = Bus_Model::get_all($this->agency_id);	
		$view->all_buses=$all_buses;
		
		if($_POST)
		{
			
			if(isset($_POST['by_period']))
			{
				$post = new Validation($_POST);
				$post->add_rules('start_date','required');
				$post->add_rules('end_date','required');
				$post->add_rules('bus_number','required');
				if ($post->validate())
				{	
					
					$start_date = date("Y-m-d",strtotime($_POST['start_date']));
					$end_date = date("Y-m-d",strtotime($_POST['end_date']));
					
					if(strtotime($start_date) > strtotime($end_date)){
						$this->session->set('notice', array('message'=>'Start date must be earlier than end date','type'=>'error'));
						url::redirect('buses/check_buses');
					}
					
					$bus_number=$_POST['bus_number'];
					$display_option=$_POST['display_options'];
					
				
					//$period_parcels=Parcel_Model::get_agency_parcels_by_period($agency_id,$start_date,$end_date,$admin);
					$period_expenses=Expense_Model::get_agency_expenses_by_period_by_bus($this->agency_id,$start_date,$end_date,$bus_number);
					$period_schedules=Schedule_Model::get_agency_schedules_by_period_by_bus($this->agency_id,$start_date,$end_date,$bus_number);
					$period_income=Income_Model::get_agency_income_by_period_by_bus($this->agency_id,$start_date,$end_date,$bus_number);
					
					//To display timeline of events by date/chronological transaction history, create array, put ALL el'ts inside, then sort by date
					$all_activity = array();
					foreach($period_expenses as $exp)
					{
						$all_activity[] = array('date'=>strtotime($exp->date_incurred), 'description'=>$exp->purpose, 'amount'=>$exp->amount, 'type'=>'debit', 'admin'=>$exp->CreatedBy);
					}
					foreach($period_schedules as $schedule)
					{
						$all_activity[] = array('date'=>strtotime($schedule->checked_out_on), 'description'=>'Schedule check out', 'amount'=>$schedule->total_amount, 'type'=>'credit', 'admin'=>$schedule->CheckedOutBy);
					}
					foreach($period_income as $income)
					{
						$all_activity[] = array('date'=>strtotime($income->date_incurred), 'description'=>$income->purpose, 'amount'=>$income->amount, 'type'=>'credit', 'admin'=>$income->CreatedBy);
					}
					
					//Compare function to sort the array by datetime
					function cmp($a, $b)
					{
						if ($a['date'] == $b['date']) {
							return 0;
						}
						return ($a['date'] < $b['date']) ? -1 : 1;
					}
					usort($all_activity, "cmp");
					

					
					
					
					//calculate the total expenses for that period
					$total_expenditure = 0;
					foreach($period_expenses as $exp )
					{
						$total_expenditure += $exp->amount;
					}
				
					//Calculate total amount expected from departed buses for the period. DEPARTED BUSES ONLY
					$total = 0;
					foreach($period_schedules as $ds)
					{
						$total += $ds->total_amount;
					}
					
					$income_total = 0;
					foreach($period_income as $income)
					{
						$income_total += $income->amount;
					}
				}
				else
				{
					$errors=$post->errors('errors');
					$notice="";
						foreach($errors as $error) 
						{
							$notice.=$error."<br />";
						}
						$this->session->set('notice', array('message'=>$notice,'type'=>'error'));
						url::redirect('admin/main/'.$this->admin->agency_id);
				}
				
				if($display_option=='grouped')
				{
					$view = new View('activity_by_period_by_bus');
				}else{
					$view = new View('activity_by_period_by_bus_timeline');
				}
				//$view->parcels=$period_parcels;
				$view->total_expenditure=$total_expenditure;
				$view->period_income = $period_income;
				$view->period_expenses = $period_expenses;
				$view->period_schedules = $period_schedules;
				$view->total = $total;
				$view->income_total = $income_total;
				$view->start_date = $start_date;
				$view->end_date = $end_date;
				$view->all_activity = $all_activity;
				$view->bus_number = $bus_number;
			}
			
			if(isset($_POST['compare_buses']))
			{
				$post = new Validation($_POST);
				$post->add_rules('start_date','required');
				$post->add_rules('end_date','required');
				if ($post->validate())
				{	
					
					$start_date = date("Y-m-d",strtotime($_POST['start_date']));
					$end_date = date("Y-m-d",strtotime($_POST['end_date']));
					
					if(strtotime($start_date) > strtotime($end_date)){
						$this->session->set('notice', array('message'=>'Start date must be earlier than end date','type'=>'error'));
						url::redirect('buses/check_buses');
					}
				
					$period_expenses=Expense_Model::agency_expenses_by_period($this->agency_id,$start_date,$end_date);
					$period_schedules=Schedule_Model::agency_schedules_by_period($this->agency_id,$start_date,$end_date);
					$period_income=Income_Model::get_agency_income_by_period($this->agency_id,$start_date,$end_date);
					
					$all_buses_array = array();
					
					
					//create 3 arrays for expenses, income and net, per bus
					$bus_and_income = array();
					$bus_and_expenses = array();
					$bus_and_net = array();
					
					$overall_schedule_income_total = 0;
					$overall_other_income_total = 0;
					$overall_expenses_total = 0;
					$overall_net_total = 0;
					
					//foreach bus, calculate total schedule income, other income and expenses for the period
					foreach($all_buses as $bus){
						
						//calculate the total expenses for that period
						$expenses_total = 0;
						foreach($period_expenses as $exp ){
							//only add for the corresponding bus
							if($exp->bus_number == $bus->bus_number){
								$expenses_total += $exp->amount;
								$overall_expenses_total += $exp->amount;
							}
						}
						$bus_and_expenses[$bus->bus_number]=$expenses_total;
						
						//Calculate total amount expected from departed buses for the period. DEPARTED BUSES ONLY
						$schedule_income_total = 0;
						foreach($period_schedules as $ds){
							//only add for the corresponding bus
							if($ds->bus_number == $bus->bus_number){
								$schedule_income_total += $ds->total_amount;
								$overall_schedule_income_total += $ds->total_amount;
								
							}
						}
						
						//Calculate other income
						$other_income_total = 0;
						foreach($period_income as $income){
							//only add for the corresponding bus
							if($income->bus_number == $bus->bus_number){
								$other_income_total += $income->amount;
								$overall_other_income_total += $income->amount;
								
							}
							
						}
						$bus_and_income[$bus->bus_number]=$other_income_total + $schedule_income_total;
						$bus_and_net[$bus->bus_number]=$other_income_total + $schedule_income_total;
						//var_dump($bus->bus_number.",".$schedule_income_total);
						
					}
					
				}
				else
				{
					$errors=$post->errors('errors');
					$notice="";
						foreach($errors as $error) 
						{
							$notice.=$error."<br />";
						}
						$this->session->set('notice', array('message'=>$notice,'type'=>'error'));
						url::redirect('admin/main/'.$this->admin->agency_id);
				}
				
				$view = new View('bus_ranking');
				
				
				//sort from highest to lowest values
				arsort($bus_and_income);arsort($bus_and_expenses);arsort($bus_and_net);
				
				$view->bus_and_income=$bus_and_income;
				$view->bus_and_expenses=$bus_and_expenses;
				$view->bus_and_net=$bus_and_net;
				
				$view->overall_expenses=$overall_expenses_total;
				$view->overall_income=$overall_schedule_income_total + $overall_other_income_total;
				
				$view->overall_net=$overall_schedule_income_total + $overall_other_income_total-$overall_expenses_total;
				
				//$view->period_income = $period_income;
				//$view->period_expenses = $period_expenses;
				//$view->period_schedules = $period_schedules;
				//$view->total = $total;
				//$view->income_total = $income_total;
				$view->start_date = $start_date;
				$view->end_date = $end_date;
				//$view->all_activity = $all_activity;
				//$view->bus_number = $bus_number;
			}
			
			if(isset($_POST['inavailability']))
			{
				$post = new Validation($_POST);
				$post->add_rules('start_date','required');
				$post->add_rules('end_date','required');
				if ($post->validate())
				{	
					
					$start_date = date("Y-m-d",strtotime($_POST['start_date']));
					$end_date = date("Y-m-d",strtotime($_POST['end_date']));
					
					if(strtotime($start_date) > strtotime($end_date)){
						$this->session->set('notice', array('message'=>'Start date must be earlier than end date','type'=>'error'));
						url::redirect('buses/check_buses');
					}
					$bus_number=$_POST['bus_number'];
					$availabilities=Bus_inavailability_detail_Model::get_all_for_bus($this->agency_id, $bus_number, $start_date,$end_date);
				
				}
				else
				{
					$errors=$post->errors('errors');
					$notice="";
						foreach($errors as $error) 
						{
							$notice.=$error."<br />";
						}
						$this->session->set('notice', array('message'=>$notice,'type'=>'error'));
						//url::redirect('buses/'.$this->admin->agency_id);
				}
				
				$view = new View('bus_inavailability_details');
				
			
				$view->availabilities = $availabilities;
				$view->start_date = $start_date;
				$view->end_date = $end_date;
				//$view->bus_number = $all_activity;
				$view->bus_number = $bus_number;
			}
		}
		
		$this->template->content = $view;
	
	}	
	
			
}