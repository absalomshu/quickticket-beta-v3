<?php defined('SYSPATH') or die('No direct script access');

class Bus_Requirement_Model extends ORM{

	public function add_requirement($agency_id, $name)
	{	
		$requirement = ORM::FACTORY('bus_requirement');
		$requirement->AgencyID = $agency_id;
		$requirement->Name = $name;
	
		$requirement->save();
	}
	
	public function edit_bus_requirement($agency_id, $expense_name, $expense_value, $expense_is_percent, $expense_is_amount, $expense_id)
	{	
		$expense = ORM::FACTORY('bus_requirement')->where('AgencyID',$agency_id)->where('id',$expense_id)->find();
		$expense->AgencyID = $agency_id;
		$expense->Name = $expense_name;
		$expense->Value = $expense_value;
		$expense->IsPercentage = $expense_is_percent;
		$expense->IsFixedAmount = $expense_is_amount;
		$expense->save();
	}
	
	public function get_all($agency_id)
	{
		$fixed_expenses=ORM::factory('bus_requirement')->where('AgencyID',$agency_id)->find_all();
		return $fixed_expenses;	
	}
	
	public function get_requirement($agency_id,$requirement_id)
	{

		$requirement=ORM::factory('bus_requirement')->where('AgencyID',$agency_id)->where('id',$requirement_id)->find();
		return $requirement;
		
	}
	public function delete_requirement($agency_id,$requirement_id)
	{
		
		$requirement=ORM::factory('bus_requirement')->where('id',$requirement_id)->find();
		$requirement->delete();
		$requirement->save();
		
		$notice="Requirment deleted";
		return $notice;
		
	}
	

}
