<?php defined('SYSPATH') or die('No direct script access'); ?> 


	<?php
		//determine what type of notice to display if at all
		$notice = $this->session->get_once('notice');
			if(!empty($notice)){ 
				if($notice['type'] == 'success'){?><div class="alert general success"><a class="close" data-dismiss="alert" href="#">&times;</a><?php echo $notice['message']; }?></div>
			<?	if($notice['type'] == 'error'){?><div class="alert general error"><a class="close" data-dismiss="alert" href="#">&times;</a><?php echo $notice['message']; }?></div>
		<?}?>
    <div class="container">
		<?php
		//set a variable depending on whether the parcel has been delivered or not
		//also check if the current admin is the sender. if so, disable the form , as he's not the recieving agency, hence shouldn't be allowed to fill
		//if it has, disable all input fields through javascript
		
		
		if($parcel->state == "delivered" || $parcel->from ==$this->admin->agency_id){
			$is_delivered = true;
		}else{ $is_delivered = false;}
		
		?>
	  	
	<div class="row-fluid">	
		
		<div class="span12 hero-unit prof-unit">
		
		<ul class="nav nav-tabs">
					<li class="active"> <a href="<?=url::site('parcels/all/incoming')?>"><?=Kohana::lang('backend.incoming')?></a></li>
					<li><a href="<?=url::site('parcels/all/sent')?>"> <?=Kohana::lang('backend.sent')?></a></li>
		</ul>
		
		<div class="row-fluid">	
		
			<div class="span6 hero-unit prof-unit">
				
				<div class="heading"><?=Kohana::lang('backend.parcel_details')?>: #1222211</div>
				
				<div class="simple-form span12">
								<div class="sf_label"><?=Kohana::lang('backend.sent_from')?>: </div>
								<div class="sf_text"><?=get::town($parcel->from);?></div>
				</div>
				<div class="simple-form span12">
								<div class="sf_label"><?=Kohana::lang('backend.sent_on')?>: </div>
								<div class="sf_text"><?=date("d-m-Y",strtotime($parcel->sent_date))?></div>
				</div>
				<div class="simple-form span12">
								<div class="sf_label"><?=Kohana::lang('backend.for')?>: </div>
								<div class="sf_text"><?=$parcel->receiver_name;?></div>
				</div>
				<div class="simple-form span12">
								<div class="sf_label">Description: </div>
								<div class="sf_text"><?=$parcel->description;?></div>
				</div>
				
			
			</div>
		
		
			
			<div class="span6">
			
			<div class="heading"><?php if(!$is_delivered){ echo "Who is collecting this parcel?";}else{echo "Collected by";}?></div>
				<?php 
					$collected_by = json_decode($parcel->collected_by);
					//var_dump($collected_by);exit;
				?>
				<form action="<?=url::site('parcels/deliver/'.$parcel->id)?>" method="POST" >
								<div class="clear"></div>

					<div class="simple-form span12">
								<div class="sf_label"><?=Kohana::lang('backend.coll_name')?>: </div>
								<div class="sf_input"><input type="text"  name="collector_name" <?php if($is_delivered){echo "value='". @$collected_by->name."' disabled";}?>></div>
					</div>
					<div class="simple-form span12">
								<div class="sf_label"><?=Kohana::lang('backend.coll_id')?>: </div>
								<input type="text"  name="collector_idc" <?php if($is_delivered){echo "value='". @$collected_by->idc. "' disabled";}?>>
					</div>				
					<div class="simple-form span12">
								<div class="sf_label"><?=Kohana::lang('backend.coll_phone')?>: </div>
								<div class="sf_input"><input type="text"  name="collector_phone" <?php if($is_delivered){echo "value='".@$collected_by->phone. "' disabled";}?>></div>
					</div>
					
					<?php
					//only show deliver button if it's not delivered
						if(!$is_delivered){
					?>	
							<div class="simple-form span12">
										<div class="sf_label">&nbsp;</div>
										<div class="sf_input"><button class="btn create-btn btn-success" type="submit" ><i class="icon-ok icon-white"></i> <?=Kohana::lang('backend.deliver')?></button></div>
							</div>
					<?php
						}else{
					?>
					
						<div class="simple-form span12">
								<div class="sf_label"><?=Kohana::lang('backend.collected_on')?>: </div>
								<div class="sf_input"><input type="text"  name="collector_phone" <?php if($is_delivered){echo "value='". @$parcel->collected_on."' disabled";}?>></div>
						</div>

					<?php } ?>
					
				</form>
			</div>	
			
				
				
		</div>
	
		
		</div>
	
    </div> 

	</div>
	
	<script type="text/javascript">
	
	$(document).ready(function(){
		var is_delivered = '<?=$is_delivered?>';
		$('.sf_input').prop('disabled',true);
	
	})
	
	</script>