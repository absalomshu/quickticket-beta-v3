<?php defined('SYSPATH') or die('No direct script access');

//set default timezone, otherwise it's UTC which is an hour late
date_default_timezone_set('Africa/Douala');

//Almost all functions will take as parameter, the id of the agency we are dealing with
//in order not to interfere with another's stuff.

class Admin_Controller extends Template_Controller 
{
	public $template = 'template/admin_template';
	//public $uri_cont = 'http://api.wasamundi.com/v2/texto/';
	//public $uri_cont = 'http://quickticket.co/send_sms';
	//public $uri_cont = 'http://localhost:4001/quickticket/send_sms';
	protected $session;
	protected $agency_id;
	public $agency_code;
	protected $language;
	public $lang;
	public $total_pending_reminders;
	public $total_pending_online_purchases;
	public $total_pending_remote_tickets;
	public $total_pending_tickets;
	
	
	public function __construct()
	{	
		parent::__construct();
		
		$this->admin = Authlite::instance()->get_admin();
		//CALCULATE THE NUMBER OF PENDING REMINDERS
		//Pick from the reminder schedules, the date of the last uncompleted.
		$this->db = new Database();
		
		$this->session = Session::instance();
		//$this->current_page = url::current();
		
		//If the user hasn't changed language, use his default language	
		$this->lang = $this->session->get('lang');;
		//language settings have to be different for logged in and not logged in.
		if(empty($this->lang))
		{	
			$this->lang = array('en_US', 'English_United States');
		}
		
		Kohana::config_set('locale.language', $this->lang);
		
		//if there's an admin
		if (!empty($this->admin)) 
		{		
			$admin_default_lang = $this->admin->LanguageID;
			
			//if something has been done about the language, use the recently set language, else use the admin's default from the db
			if(!empty($this->lang))
			{
				Kohana::config_set('locale.language', $this->lang);
			}
			else
			{
				if($admin_default_lang == "en"){ $admin_default_lang = array('en_US', 'English_United States'); }
				elseif($admin_default_lang == "fr"){ $admin_default_lang = array('fr_FR', 'French_France'); }
				Kohana::config_set('locale.language', $admin_default_lang);
			}
			$this->agency_id = $this->admin->agency_id;
			$this->agency_town_id = Town_Model::get_agency_town_id($this->agency_id);
			$this->agency_town = Town_Model::get_agency_town_name($this->agency_id);
			
			$total_pending_reminders = $this->db->query("SELECT *  FROM  `reminder_schedules` WHERE DATEDIFF(`DueDate` , DATE( NOW( ) ) ) <=0 AND IsCompleted =  '0' AND AgencyID= $this->agency_id");
			$this->total_pending_reminders = $total_pending_reminders->count();
			
			$this->total_pending_online_purchases = count(Online_Purchase_Model::get_all_active($this->agency_id));
			$this->total_pending_remote_tickets = count(Ticket_Detail_Model::get_all_remote_incoming_pending($this->agency_id));
			$this->total_pending_tickets = ($this->total_pending_online_purchases + $this->total_pending_remote_tickets) ;
		
		}
	}  
	
	
	public function index()
	{	
		//ensure a logged in user can't land on the signup page
		Authlite::check_admin();
		//Decide whether admin is a branch manager, general manager or regular admin and redirect appropriately
		if($this->admin->admin_group_id == 3)
		{	
			url::redirect('control/main');
		}
		elseif($this->admin->admin_group_id == 4){
			url::redirect('manage/main');
		}
		else
		{
		//now if it's an admin, redirect depending on which menu he's allowed acces to
		//the conditions in the menu only check which LINKS he can see and access
			//if he's a schedule manager, send him directly home
			if($is_allowed_main_module = Admin_Module_Model::admin_is_allowed_main_module($this->admin->username,1))
			{
				url::redirect('admin/main');
			}elseif($is_allowed_main_module = Admin_Module_Model::admin_is_allowed_main_module($this->admin->username,4))
			{
				url::redirect('parcels/all/incoming');
			}elseif($is_allowed_main_module = Admin_Module_Model::admin_is_allowed_main_module($this->admin->username,5))
			{
				url::redirect('buses');
			}elseif($is_allowed_main_module = Admin_Module_Model::admin_is_allowed_main_module($this->admin->username,3))
			{	
				url::redirect('expenses');
			}else
			{
				$this->main();
			}
		}
		
	}
	
	public function login() 
	{	
		if (!empty($this->admin)) 
		{
			url::redirect('admin/main/'.$this->admin->agency_id);
		} 
		//$this->auto_render=false;
		$notice='';
		$this->template->title = Kohana::lang('backend.qt_login');
		
		if($_POST) 
		{	
			
			$post=new Validation($_POST);
			$post->add_rules("username", "required",'valid::standard_text');
			$post->add_rules("password", "required");
			if ($post->validate())
			{
				$username = $_POST['username'];
				$password = $_POST['password'];
				
				$admin = Authlite::instance()->admin_login($username, $password);
				
				if(!$admin)
				{	
					//$notice="Sorry, incorrect username and password combination!";
					$this->session->set('notice', array('message'=>Kohana::lang('backend.incorrect_credentials'),'type'=>'error'));
				}
				else
				{
					//Get once, all the modules to which the user has access and store in session
					$main_modules = Main_Module_Model::get_all();

					$is_allowed_home = Admin_Module_Model::admin_is_allowed_main_module($admin->username,1);
					$is_allowed_schedules = Admin_Module_Model::admin_is_allowed_main_module($admin->username,2);
					$is_allowed_expenses = Admin_Module_Model::admin_is_allowed_main_module($admin->username,3);
					$is_allowed_parcels = Admin_Module_Model::admin_is_allowed_main_module($admin->username,4);
					$is_allowed_buses = Admin_Module_Model::admin_is_allowed_main_module($admin->username,5);
					$is_allowed_reminders = Admin_Module_Model::admin_is_allowed_main_module($admin->username,6);
					$is_allowed_reports = Admin_Module_Model::admin_is_allowed_main_module($admin->username,7);
					$is_allowed_cash = Admin_Module_Model::admin_is_allowed_main_module($admin->username,8);
					$is_allowed_settings = Admin_Module_Model::admin_is_allowed_main_module($admin->username,9);
					
					$this->session->set('ModuleAccess', array(
						'1'=>$is_allowed_home,
						'2'=>$is_allowed_schedules,
						'3'=>$is_allowed_expenses,
						'4'=>$is_allowed_parcels,
						'5'=>$is_allowed_buses,
						'6'=>$is_allowed_reminders,
						'7'=>$is_allowed_reports,
						'8'=>$is_allowed_cash,
						'9'=>$is_allowed_settings
						));
						
						
					//this is already done in the construct, but for some reason, does not pick the admin for
					//the first time till you refresh. so it is done here again 
					$this->admin = Authlite::instance()->get_admin();
					//Get all his dashboard items with their values and store in an array in session which will be checked before displaying the dashboard
					$dashboard_items = Admin_Dashboard_Item_Model::get_all($this->admin->username);
					
					$items_for_session = array();
					foreach($dashboard_items as $item)
					{	//For the third dashboard item id, which is about restricting to a town, create a further array, so as to also hold the town id
						if($item->DashboardItemID == 3)
						{
							$items_for_session["$item->DashboardItemID"] = array('0'=>$item->IsAllowed,'1'=>$item->RestrictedTownID );	
						}else
						{
							$items_for_session["$item->DashboardItemID"] = $item->IsAllowed;
						}
					}
					$this->session->set('AdminDashboardItems',$items_for_session);
					
					//Redirect him to the first module he's allowed to use.
					if($is_allowed_home){url::redirect('admin/main');}
					elseif($is_allowed_schedules){url::redirect('admin/all_schedules/current');}
					elseif($is_allowed_expenses){url::redirect('expenses/all');}
					elseif($is_allowed_parcels){url::redirect('parcels/all/incoming');}
					elseif($is_allowed_buses){url::redirect('buses/all');}
					elseif($is_allowed_reminders){url::redirect('reminders/all');}
					elseif($is_allowed_reports){url::redirect('reports');}
					elseif($is_allowed_cash){url::redirect('cash');}
					elseif($is_allowed_settings){url::redirect('settings');}
					
					
					
					
				}
			}else 
			{
                $errors=$post->errors('errors');
                $notice="";
                foreach($errors as $error) 
				{
                    $notice .= $error;
                }
				$this->session->set('notice', array('message'=>$notice,'type'=>'error'));
            }
		}
		$view=new View('admin_login');
		$view->notice=$notice;
		$this->template->content=$view;
	}
	

	
	public function main() 
	{	
		Authlite::check_admin();
		Authlite::verify_referer();
		$permitted_levels = array(2);
		Authlite::check_access($permitted_levels);
				
		$agency_id = $this->agency_id;
		//Get agency town at once and use as "from" to display all schedules, to avoid repetitive db queries to get the town 'from'
		
		$this->template->title = Kohana::lang('backend.welcome_backend');
		$view=new View ('admin_main');
		
		$towns = get::all_towns();
		
		//If he is permitted, get all current schedules. Else get on current schedules he's permitted to see on the dashboard.
		if($_SESSION['AdminDashboardItems'][2])
		{
			$current_schedules=Schedule_Model::get_ten_agency_current_schedules($agency_id);
			$departed_schedules=Schedule_Model::get_ten_agency_departed_schedules($agency_id);
			//var_dump($departed_schedules);exit;
		}else{
			//This is an array, whose 0 is either 0 or 1, and whose 1 is the id of the town to which the user is restricted
			$restricted_town = $_SESSION['AdminDashboardItems'][3][1];
			$current_schedules=Schedule_Model::get_ten_agency_current_schedules_restricted($agency_id,$restricted_town);
			$departed_schedules=Schedule_Model::get_ten_agency_departed_schedules_restricted($agency_id, $restricted_town);
		}
		
		//$parcels = get::ten_agency_incoming_parcels($agency_id);
		$parent = get::_parent($agency_id);
		//used for parcels
		//$siblings = get::agency_siblings($agency_id,$parent);
		
		$parent_buses = get::all_buses($agency_id, $parent->id, 4);
		$date=date('d-m-Y');
		
		$view->current_schedules=$current_schedules;
		$view->departed_schedules=$departed_schedules;
		$view->towns=$towns;
		$view->restricted_town=@$restricted_town;
		$view->parent_buses=$parent_buses;
		
		$view->agency_id=$agency_id;
		$view->agency_town=$this->agency_town;
		$this->template->content=$view;
		
	}	
	
	public function generate_daily_summary() 
	{	
		Authlite::check_admin();
		Authlite::verify_referer();
		$date=date('d-m-Y');		
		$agency_id = $this->agency_id;
		
		$schedules_checked_out_today = get::all_agency_departed_schedules_by_date($agency_id,$date);
		$bus_rentals_today = Income_Model::get_income_from_rentals_today($agency_id);
		$expenses_today = get::agency_expenses_by_date($agency_id,$date);
		
		$parcel_income_today = Income_Model::get_outgoing_parcels_by_date($agency_id,$date);
		
				
		$total_income=0;
		$total_expenditure=0;
		
		//Total schedule income
		$total_schedules_income=0;
		foreach($schedules_checked_out_today as $schedule ){
			$total_schedules_income += $schedule->total_amount;
		}
		
		//Total bus rental income
		$total_bus_rental_income=0;
		foreach($bus_rentals_today as $rental ){
			$total_bus_rental_income += $rental->amount;
		}
		
		//Total bus expenditure
		$total_expenditure=0;
		foreach($expenses_today as $exp ){
			$total_expenditure += $exp->amount;
		}
		
		
		$total_income = $total_schedules_income + $total_bus_rental_income;
		$total_expenditure = 
		
		
		$towns = get::all_towns();
				
		$parent = get::_parent($agency_id);
		
		$parent_buses = get::all_buses($agency_id, $parent->id, 4);
		
		
		$view->current_schedules=$current_schedules;
		$view->departed_schedules=$departed_schedules;
		$view->towns=$towns;
		$view->restricted_town=@$restricted_town;
		$view->parent_buses=$parent_buses;
		
		$view->agency_id=$agency_id;
		$view->agency_town=$this->agency_town;
		$this->template->content=$view;
		
	}
	
	public function dashboard() 
	{	
		Authlite::check_admin();
		Authlite::verify_referer();
		
		$agency_id = $this->agency_id;
		//Get agency town at once and use as "from" to display all schedules, to avoid repetitive db queries to get the town 'from'
		
		$this->template->title = Kohana::lang('backend.dashboard');
		$view=new View ('dashboard');
		
		$towns = get::all_towns();
		
		//If he is permitted, get all current schedules. Else get on current schedules he's permitted to see on the dashboard.
		if($_SESSION['AdminDashboardItems'][2])
		{
			$current_schedules=Schedule_Model::get_ten_agency_current_schedules($agency_id);
			$departed_schedules=Schedule_Model::get_ten_agency_departed_schedules($agency_id);
			//var_dump($departed_schedules);exit;
		}else{
			//This is an array, whose 0 is either 0 or 1, and whose 1 is the id of the town to which the user is restricted
			$restricted_town = $_SESSION['AdminDashboardItems'][3][1];
			$current_schedules=Schedule_Model::get_ten_agency_current_schedules_restricted($agency_id,$restricted_town);
			$departed_schedules=Schedule_Model::get_ten_agency_departed_schedules_restricted($agency_id, $restricted_town);
		}
		
		//$parcels = get::ten_agency_incoming_parcels($agency_id);
		$parent = get::_parent($agency_id);
		//used for parcels
		//$siblings = get::agency_siblings($agency_id,$parent);
		
		$parent_buses = get::all_buses($agency_id, $parent->id, 4);
		$date=date('d-m-Y');
		
		$view->current_schedules=$current_schedules;
		$view->departed_schedules=$departed_schedules;
		$view->towns=$towns;
		$view->restricted_town=@$restricted_town;
		$view->parent_buses=$parent_buses;
		
		$view->agency_id=$agency_id;
		$view->agency_town=$this->agency_town;
		$this->template->content=$view;
		
	}
	
	public function tickets_pending() 
	{	
		Authlite::check_admin();
		Authlite::verify_referer();
		$permitted_levels = array(2);
		Authlite::check_access($permitted_levels);
		
		$agency_id = $this->agency_id;
		$this->template->title = Kohana::lang('backend.manage_tickets');
		$view=new View ('admin_tickets_pending');
		
		//This MUST be missed in particular and not pending coz for pending tickets created say in another branch, there's no schedule
		//hence the join (see ticket details model) will fail 
		
		
		
		//pagination
		$per_page = 15;		
		$missed_tickets = Ticket_Detail_Model::get_all_missed($agency_id);			
		$missed_tickets_total = count($missed_tickets);
		
		$this->pagination = new Pagination(array(
			// 'base_url'    => 'welcome/pagination_example/page/', // base_url will default to current uri
			'uri_segment'    => 'page', // pass a string as uri_segment to trigger former 'label' functionality
			'total_items'    => $missed_tickets_total, // use db count query here of course
			'items_per_page' => $per_page , // it may be handy to set defaults for stuff like this in config/pagination.php
			'style'          => 'punbb', // pick one from: classic (default), digg, extended, punbb, or add your own!
			//'query_string'   => "ORM::factory('schedule')->where('agency_id',$agency_id)->where('status','current')->find_all()"
		));
			
		$missed_tickets_per_page= $this->db->select('ticket_details.id,ticket_details.IsRemote,ticket_details.Price, FreeTicket, FreeTicketReason, ticket_details.ScheduleID, schedules.status AS schedule_status, MissedStatus,to,departure_time, departure_date, ClientName, ClientPhone, ClientEmail, ClientIDC, bus_number, checked_out_time, towns.name AS town_name_to, schedules.status')->from('ticket_details')->join('schedules','schedules.id=ticket_details.ScheduleID')->join('towns','towns.id=schedules.to')->where('AgencyID',$agency_id)->where('MissedStatus','1')->limit($per_page,$this->pagination->sql_offset)->get();
		
		$current_schedules=Schedule_Model::get_all_agency_current_schedules($agency_id);
		
			
		$view->missed_tickets=$missed_tickets_per_page;
		$view->current_schedules=$current_schedules;

		
		
		$view->agency_id=$agency_id;
		$this->template->content=$view;
	}
	
	public function online_purchases() 
	{	
		Authlite::check_admin();
		Authlite::verify_referer();
				
		$agency_id = $this->agency_id;
		$this->template->title = Kohana::lang('backend.online_purchases');
		$view=new View ('admin_online_purchases');
		
		$per_page = 15;		
		$online_purchases = Online_Purchase_Model::get_all_active($agency_id);		
		$online_purchases_total = count($online_purchases);
		
		$this->pagination = new Pagination(array(
			// 'base_url'    => 'welcome/pagination_example/page/', // base_url will default to current uri
			'uri_segment'    => 'page', // pass a string as uri_segment to trigger former 'label' functionality
			'total_items'    => $online_purchases_total, // use db count query here of course
			'items_per_page' => $per_page , // it may be handy to set defaults for stuff like this in config/pagination.php
			'style'          => 'punbb', // pick one from: classic (default), digg, extended, punbb, or add your own!
			//'query_string'   => "ORM::factory('schedule')->where('agency_id',$agency_id)->where('status','current')->find_all()"
		));

		$online_purchases = ORM::factory('online_purchase')->where('agency_id',$agency_id)->where('status','active')->limit($per_page,$this->pagination->sql_offset)->find_all();
		
		$current_schedules=Schedule_Model::get_all_agency_current_schedules($agency_id);
		$view->online_purchases=$online_purchases;
		$view->current_schedules=$current_schedules;
		$view->agency_id=$agency_id;
		$this->template->content=$view;
	}
	
	public function online_purchase_history() 
	{	
		Authlite::check_admin();
		Authlite::verify_referer();
				
		$agency_id = $this->agency_id;
		$this->template->title = Kohana::lang('backend.online_purchase_history');
		$view=new View ('admin_online_purchase_history');
		
		$per_page = 15;		
		$online_purchases = Online_Purchase_Model::get_all_nonactive($agency_id);		
		$online_purchases_total = count($online_purchases);
		
		$this->pagination = new Pagination(array(
			// 'base_url'    => 'welcome/pagination_example/page/', // base_url will default to current uri
			'uri_segment'    => 'page', // pass a string as uri_segment to trigger former 'label' functionality
			'total_items'    => $online_purchases_total, // use db count query here of course
			'items_per_page' => $per_page , // it may be handy to set defaults for stuff like this in config/pagination.php
			'style'          => 'punbb', // pick one from: classic (default), digg, extended, punbb, or add your own!
			//'query_string'   => "ORM::factory('schedule')->where('agency_id',$agency_id)->where('status','current')->find_all()"
		));

		$online_purchases = ORM::factory('online_purchase')->where('agency_id',$agency_id)->where('status!=','active')->limit($per_page,$this->pagination->sql_offset)->find_all();
		$current_schedules=Schedule_Model::get_all_agency_current_schedules($agency_id);
		
			
		$view->online_purchases=$online_purchases;
		$view->current_schedules=$current_schedules;

		
		$view->agency_id=$agency_id;
		$this->template->content=$view;
	}
	
	public function remote_tickets() 
	{	
		Authlite::check_admin();
		Authlite::verify_referer();
				
		$agency_id = $this->agency_id;
		$this->template->title = Kohana::lang('backend.remote_tickets');
		$view=new View ('admin_remote_tickets');
		
		//pagination
		$per_page = 15;		
		$remote_tickets = Ticket_Detail_Model::get_all_remote_active($agency_id);
		$remote_tickets_total = count($remote_tickets);
		
		$this->pagination = new Pagination(array(
			// 'base_url'    => 'welcome/pagination_example/page/', // base_url will default to current uri
			'uri_segment'    => 'page', // pass a string as uri_segment to trigger former 'label' functionality
			'total_items'    => $remote_tickets_total, // use db count query here of course
			'items_per_page' => $per_page , // it may be handy to set defaults for stuff like this in config/pagination.php
			'style'          => 'punbb', // pick one from: classic (default), digg, extended, punbb, or add your own!
			//'query_string'   => "ORM::factory('schedule')->where('agency_id',$agency_id)->where('status','current')->find_all()"
		));
			
		$remote_tickets_per_page= ORM::factory('ticket_detail')->where('AgencyID',$agency_id)->where('IsRemote','1')->limit($per_page,$this->pagination->sql_offset)->find_all();
		
			
		$current_schedules=Schedule_Model::get_all_agency_current_schedules($agency_id);
		$view->remote_tickets=$remote_tickets_per_page;
		$view->current_schedules=$current_schedules;

		
		$view->agency_id=$agency_id;
		$this->template->content=$view;
	}
	
	public function add_remote_ticket() 
	{	
		Authlite::check_admin();
		Authlite::verify_referer();
				
		$agency_id = $this->agency_id;
		$this->template->title = 'Remote tickets';
		$view=new View ('admin_add_remote_ticket');
		
		if($_POST)
		{
			//no_csrf::check();
			$post = new Validation($_POST);
			$post->add_rules('remote_agency_from','required');
			$post->add_rules('remote_agency_to','required');
			$post->add_rules('ticket_price','required','valid::digit');
			
			if ($post->validate())
			{	
				
				$client_name = $_POST['name'];
				$client_phone= $_POST['phone'];
				$client_idc = $_POST['idc'];
				$client_email = $_POST['email'];
				$ticket_price = $_POST['ticket_price'];
				$remote_agency_from = $_POST['remote_agency_from'];
				$remote_agency_to = $_POST['remote_agency_to'];
				
				//ensure origin and destination are not the same
				if ($remote_agency_from == $remote_agency_to)
					{
						$this->session->set('notice', array('message'=>Kohana::lang('backend.same_origin_destination'),'type'=>'error'));
						url::redirect('admin/add_remote_ticket/'.$this->admin->agency_id);
					}else{
						$this->session->set('notice', array('message'=>Kohana::lang('backend.remote_ticket_saved'),'type'=>'success'));
						$remote_ticket = Ticket_Detail_Model::add_ticket($agency_id, $schedule_id=0, $seat_number=0, $client_name, $client_phone, $client_idc, $client_email, $ticket_price, $missed_status=0, $free_ticket=0, $free_ticket_reason='', $this->admin->username, $is_remote=1, $remote_agency_from, $remote_agency_to, $remote_ticket_outgoing=1);
					}
			}
			else
			{
				$errors=$post->errors('errors');
				$notice="";
					foreach($errors as $error) 
					{
						$notice.=$error."<br />";
					}
					$this->session->set('notice', array('message'=>$notice,'type'=>'error'));
			}
		}
		
		//$remote_ticket = Ticket_Detail_Model::add_ticket($agency_id, $schedule_id, $seat_number, $client_name, $client_phone, $client_idc, $client_email, $price, $missed_status=0, $free_ticket, $free_ticket_reason, $created_by, $is_remote=0, $remote_agency_from=0, $remote_agency_to=0);
		//$online_purchases = Remote_Ticket_Detail_Model::get_all_active($agency_id);
			
		//$current_schedules=Schedule_Model::get_all_agency_current_schedules($agency_id);
		
		
		
		$parent = get::_parent($this->agency_id);
		
		$siblings = get::agency_siblings($agency_id,$parent);
		$all_children = get::all_children($parent);
		
		$view->all_children = $all_children;
		$view->siblings = $siblings;
		$this->template->content=$view;
	}
	
	public function logout() 
	{
		Authlite::instance()->logout(true);
		url::redirect('admin');
	}
	
	public function create_schedule()
	{	
		Authlite::check_admin();
		Authlite::verify_referer();
		$permitted_levels = array(2);
		Authlite::check_access($permitted_levels);
		$this->auto_render = TRUE;
				
		$schedule = ORM::FACTORY('schedule');
		if($_POST){
			//no_csrf::check(1);
			
			// by default, set unregistered bus to zero, so it's not undefined
			$unregistered_bus=false;
			
			$post = new Validation($_POST);
			$admins_town = get::admins_town($this->admin->agency_id);
			//$post->add_rules('from','required');
			$post->add_rules('to','required');
			
			$post->add_rules('departure_time','required');
			$post->add_rules('departure_date','required','valid::alpha_dash');
			$post->add_rules('ticket_price','required','valid::digit');
			
			//if it's an unregistered bus, we need the bus number and number of seats, else we know it
			if(isset($_POST['unregistered_bus_check']))
			{
				//create a variable which we easily check each time to know if it's an unregistered bus
				$unregistered_bus=true;
				$post->add_rules('unregistered_bus_number','required','valid::alpha_numeric');
				$post->add_rules('unregistered_bus_seats','required');
			}else
			
			{  	//else something must be chosen from the dropdown.
				$post->add_rules('bus_number_and_seats','required');
			}

			if ($post->validate())
			{		
					$from = $admins_town->id;
					$to = $_POST['to'];
				
				//if it's an unregistered bus, save the new values entered, else save the old
				//remove all spaces from bus numbers before saving so that during search, NW123AB is NOT different from NW 123 AB
				if($unregistered_bus)
				{
					$bus_number = strtoupper(str_replace(" ","", $_POST['unregistered_bus_number']));
					$bus_seats = $_POST['unregistered_bus_seats'];
				}else
				{
					//bus number and seats are passed as a single string separated by a semicolon, hence separate them here
					$bus_number_and_seats = explode(";", $_POST['bus_number_and_seats']);
					$bus_number = $bus_number_and_seats[0];
					$bus_seats = $bus_number_and_seats[1];
				}
															
					//convert time to 24hour for storage in sql time format, and for comparison in case of sorting
					$departure_time = date("H:i",strtotime($_POST['departure_time']));
					$departure_date = date("Y-m-d",strtotime($_POST['departure_date']));
					//ensure origin and destination are not the same
					if ($from == $to)
					{
						
						$this->session->set('notice', array('message'=>Kohana::lang('backend.same_origin_destination'),'type'=>'error'));
						url::redirect('admin/main/'.$this->admin->agency_id);
					}
					else
					{
						
						//IF IT'S UNREGISTERED, CHECK IF WHAT IS ENTERED HASN'T BEEN REGISTERED BEFORE, IF SO, MAKE THEM PICK FROM THE LIST (REDIRECT) 
						
						if($unregistered_bus)
						{
							$bus_exists = ORM::FACTORY('bus')->where('bus_number',$bus_number)->count_all();
							if($bus_exists){
								$this->session->set('notice', array('message'=>$bus_number . Kohana::lang('backend.exists_in_list'),'type'=>'error'));
								url::redirect('admin/main/'.$this->admin->agency_id);
							}
							
							//CREATE A NEW BUS if it doesn't exist,
							$new_bus = ORM::FACTORY('bus');
							$new_bus->agency_id = $this->agency_id;
							$new_bus->bus_number = $bus_number;
							$new_bus->bus_seats = $bus_seats;
							$new_bus->CreatedBy = strtoupper($this->admin->username);
							$new_bus->save();
							$this->session->set('notice', array('message'=>Kohana::lang('backend.new_bus').  $bus_number . Kohana::lang('backend.is_registered'),'type'=>'success'));
							
						}
						
						$schedule->agency_id = $this->admin->agency_id;
						//$schedule->agency_code = $agency_code;
						$schedule->bus_number = strtoupper($bus_number);
						$schedule->bus_seats = $bus_seats;
						$schedule->from = $from;
						$schedule->to = $to;
						$schedule->status = 'current';
						$schedule->departure_date = $departure_date;
						$schedule->departure_time = $departure_time;
						$schedule->ticket_price = $_POST['ticket_price'];
						$schedule->CreatedBy = strtoupper($this->admin->username);;	
						$schedule->save();	
						
						
						
						url::redirect('admin/complete_schedule/'.$schedule->id);	
					}			
				}
			else
			{
				$errors=$post->errors('errors');
				$notice="";
					foreach($errors as $error) 
					{
						$notice.=$error."<br />";
					}
					$this->session->set('notice', array('message'=>$notice,'type'=>'error'));
					url::redirect('admin/main/'.$this->admin->agency_id);
			}
		}
			
	}
	
	 function edit_schedule($schedule_id)
	 {		
		Authlite::check_admin();
		//Authlite::check_integrity($schedule_id);
		$permitted_levels = array(2);
		Authlite::check_access($permitted_levels);
		Authlite::verify_referer();
		
		$this->template->title=Kohana::lang('backend.edit_schedule');
		$schedule=get::schedule($schedule_id);
		$towns = get::all_towns();
		$parent = get::_parent($this->agency_id);
		$parent_buses = get::all_buses($this->agency_id, $parent->id, 4);
			
		$seats_and_occupants = json_decode($schedule->seat_occupants,true);
		$seats_taken = count($seats_and_occupants);
			
		if($_POST)
		{
			//no_csrf::check();
			$post = new Validation($_POST);
			$post->add_rules('to','required');
			//$post->add_rules('bus_number','required','valid::alpha_numeric');
			$post->add_rules('bus_number_and_seats','required');
			$post->add_rules('departure_time','required');
			$post->add_rules('departure_date','required','valid::alpha_dash');
			$post->add_rules('ticket_price','required','valid::digit');
			
			if ($post->validate())
			{	
				//bus number and seats are passed as a single string separated by a semicolon, hence separate them here
					$bus_number_and_seats = explode(";", $_POST['bus_number_and_seats']);
					$bus_number = $bus_number_and_seats[0];
					$bus_seats = $bus_number_and_seats[1];
			
				$from = get::admins_town($this->admin->agency_id)->id;
				$to = $_POST['to'];
				//$bus_number = strtoupper(str_replace(" ","", $_POST['bus_number']));
				//$bus_seats = $_POST['bus_seats'];
				$departure_time = date("H:i",strtotime($_POST['departure_time']));
				$departure_date = date("Y-m-d",strtotime($_POST['departure_date']));
				
				$schedule->agency_id = $this->admin->agency_id;
				//$schedule->agency_code = $agency_code;
				//make bus numbers uppercase
				
				
				
				$schedule->bus_number = $bus_number;
				$schedule->bus_seats = $bus_seats;
				$schedule->from = $from;
				$schedule->to = $to;
				$schedule->status = 'current';
				$schedule->departure_date = $departure_date;
				$schedule->departure_time = $departure_time;
				$schedule->ticket_price = $_POST['ticket_price'];
				$schedule->save();	
				url::redirect('admin/complete_schedule/'.$schedule->id);
			}
			else
			{
				$errors=$post->errors('errors');
				$notice="";
					foreach($errors as $error) 
					{
						$notice.=$error."<br />";
					}
					$this->session->set('notice', array('message'=>$notice,'type'=>'error'));
					url::redirect('admin/edit_schedule/'.$schedule_id);
			}
		}
					
		$view=new View('admin_edit_schedule');
		$view->parent_buses=$parent_buses;
		$view->seats_taken=$seats_taken;
		$view->schedule=$schedule;
		$view->towns=$towns;
		$this->template->content=$view;
	}
	
	
	function complete_schedule($schedule_id)
	{			
			Authlite::check_admin();
			$permitted_levels = array(2);
			Authlite::check_access($permitted_levels);
			//Authlite::check_integrity($schedule_id);
			Authlite::verify_referer();
			
			$date = date("Y-m-d");
			$this->template->title=Kohana::lang('backend.complete_schedule');
			
			//first check if the schedule is departed, and if so, send him to past_schedule
			//departed schedules cannot be modified, so if it's checked out and admin tries to go back, it shouldn't work
			$status = get::schedule_status($schedule_id);
			if ($status == 'departed')
			{
				$this->session->set('notice', array('message'=>Kohana::lang('backend.already_checkedout'),'type'=>'error'));
				url::redirect('admin/past_schedule/'.$schedule_id);
			}
			
			$towns = get::all_towns();
			//an array to hold seats and corresponding occupants
			$schedule=get::schedule($schedule_id);
			
			//get all expenses already registered for this schedule. i.e.bus & date
			//$expenses_for_schedule = get::expenses_for_bus_and_date($this->agency_id, $schedule->bus_number, $date);
			
			//rather use the schedule id for this purpose. It's more accurate
			$expenses_for_schedule = get::expenses_for_schedule($this->agency_id, $schedule_id);
			
			//this gets the entire object
			$client_reservations=get::client_reservations($schedule_id);
			
			//this gets only the json with the reserved seats details
			$client_reservation_seat_infos=json_decode($client_reservations->reserved_seats,true);
			
			//$reserve = ORM::factory('reservation')->where('schedule_id',$schedule_id)->find();
			//first check if the field in the db is not empty. If it is, create the array. If not, decode the array and populate			
			if (empty ($schedule->reserved_seats))
			{
				$seats_and_reservers = array();			
			}
			else
			{
				$seats_and_reservers = json_decode($schedule->reserved_seats,true); //true converts the object to an array
			}
			//first check if the field in the db is not empty. If it is, create the array. If not, decode the array and populate			
			if (empty ($schedule->seat_occupants))
			{
				$seats_and_occupants = array();			
			}
			else
			{
				$seats_and_occupants = json_decode($schedule->seat_occupants,true);
			}
			
			if($_POST)
			{		
			
				
				//if the submit is coming from the check out form
				if(isset($_POST['check_out_form']))
				{	
					//no_csrf::check(1);
					//variable for special drop and free tickets
					$special_drop=false;
					$free_ticket=false;
					$free_ticket_reason='';
					
					$post = new Validation($_POST);
					//if it;s a free ticket
					if(isset($_POST['free_ticket'])){$free_ticket=true; $free_ticket_reason=$_POST['free_ticket_reason'];}
					
					//if it is a special drop, the price is required
					if(isset($_POST['spec_drop_check']))
					{
						//create a variable which we easily check each time to know if it's a special drop
						$special_drop=true;
						$post->add_rules('special_drop_price','required','valid::digit');
						$post->add_rules('special_drop_town','required');
					}					
					$post->add_rules('client_name','required');
					$post->add_rules('seat_number','required');
					$post->add_rules('client_email','valid::email');
					$post->add_rules('client_phone','valid::digit');
					$post->add_rules('client_idc','valid::digit');
					
					
					if ($post->validate())
					{
						//Names should all have Title Case
						$client_name = ucwords(strtolower($_POST['client_name']));
						$client_phone = $_POST['client_phone'];
						$client_email = $_POST['client_email'];
						$seat_number = $_POST['seat_number'];
						$client_idc = $_POST['client_idc'];
						
						if($special_drop){
							$special_drop_price = $_POST['special_drop_price'];
							$special_drop_town = $_POST['special_drop_town'];
							}
						
						//THE POPULATION!
						//Seat number is key, client's array is value
						//first check if the seat is occupied, possibly in another tab
						if(isset($seats_and_occupants[$seat_number]))
						{
							$this->session->set('notice', array('message'=>Kohana::lang('backend.sorry_seat'). $seat_number . Kohana::lang('backend.given_out'),'type'=>'error'));
							url::redirect('admin/complete_schedule/'.$schedule_id);	
						}
						//int converts the boolean value of free ticket into an integer
						if($special_drop){
							$seats_and_occupants[$seat_number] = array('name'=>$client_name,'phone'=>$client_phone,'email'=>$client_email,'sd_price'=>$special_drop_price,'sd_town'=>$special_drop_town,'idc'=>$client_idc,'free_ticket'=>(int)$free_ticket);
						}else{
							$seats_and_occupants[$seat_number] = array('name'=>$client_name,'phone'=>$client_phone,'email'=>$client_email,'idc'=>$client_idc,'free_ticket'=>(int)$free_ticket);
						}
						//var_dump($seats_and_occupants);exit;
						//when it's through, json encode and save back to db
						$schedule->seat_occupants = json_encode($seats_and_occupants);
						$schedule->save();
						
						//the ticket price and destination might be variable depending whether it's a regula seat or special drop
						//this should be reflected on the ticket
						if($special_drop){
							$to = get::town($special_drop_town);
							$ticket_price = $special_drop_price;
						}else{
							$to = get::town($schedule->to); 
							$ticket_price = $schedule->ticket_price;
							}
						//If it's a free ticket, store ticket_price 0	
						if($free_ticket){$ticket_price=0;}
							
						
						$ticket_id = Ticket_Detail_Model::add_ticket($this->agency_id, $schedule_id, $seat_number, $client_name, $client_phone, $client_idc, $client_email, $ticket_price, $missed_status=0, (int)$free_ticket, $free_ticket_reason=$free_ticket_reason, $this->admin->username);
						
						//Also save in the tickets table
						//If it's a conversion from a missed ticket to a free ticket, it's no more a missed ticket. Delete from ticket_details table, as the creation 
						//of a new ticket will add a new entry.
						//Without reducing balance because the new ticket that is created is a free ticket, with zero francs so if you reduce his balance, zero
						//francs will be added afterwards which is erroneous
						if(isset($_POST['from_missed_ticket']))
						{	
							//If it's from pending and it's free, don't reduce balance
							$missed_ticket_id=$_POST['from_missed_ticket'];
							if(isset($_POST['free_ticket']))
							{
								Ticket_Detail_Model::delete_ticket_by_id_without_reducing_balance($missed_ticket_id);
							}else{
								Ticket_Detail_Model::delete_ticket_by_id($missed_ticket_id);
							}
						}
						
						//If it's a conversion from a purchase online, mark as
						if(isset($_POST['from_online_purchase']))
						{
							$purchase_id=$_POST['from_online_purchase'];
							Online_Purchase_Model::mark_closed($this->agency_id,$purchase_id,$this->admin->username);
							Admin_Model::subtract_balance($this->admin->username,$ticket_price);
							
							//Now, creating a new ticket by default adds the admin's balance. But an online purchase has already been paid for online. 
							//So reduce the price of the ticket from the admin's balance.
						}
						
						$this->session->set('notice', array('message'=>Kohana::lang('backend.seat')." <b>".$seat_number."</b> ".Kohana::lang('backend.seat_checked_out'),'type'=>'success'));
						$parent = strtoupper(get::_parent($this->agency_id)->name);
						$from = get::town($schedule->from);
						
							
						//use the parent id to check which ticket to use	
						$parent_id = get::_parent($this->admin->agency_id)->id;
					
						//check the parent agency and generate the ticket 
						//if it's type 2, use type 2. Else use default ticket
						//i
						$ticket_type_1 = settings::ticket_type_1();
						$ticket_type_2 = settings::ticket_type_2();
						if($ticket_type_2==1){
							
							$ticket = printer::print_ticket_type_2($parent, $schedule->departure_time, $schedule->departure_date, $client_name, $client_phone, $seat_number, $from, $to, $schedule->bus_number, $ticket_price, $schedule->id);	
						
						}else{
							
							$ticket = printer::print_ticket($parent, $schedule->departure_time, $schedule->departure_date, $client_name, $client_phone, $seat_number, $from, $to, $schedule->bus_number, $ticket_price, $schedule->id, $client_idc);	
						}
						//first check if the option to auto-generate tickets is set
						$auto_ticket = settings::auto_ticket();
						
						if($auto_ticket == 1)
						{ 	
							
							//open the ticket in a new window
							//Putting $ticket again in the ticket name(second parameter) causes each ticket to open in a new window
							echo "<script>
							
									win=window.open('$ticket','Print ticket $ticket','width=900,height=400');
									win.focus();
									
								  </script>
								  ";
							//DO NOT MODIFY THESE LINES. THEY DO THE REDIRECT ENSURING THAT PAGE REFRESH IS NOT POSSIBLE HERE, NOR THE BACK BUTTON  
							echo '<script>
									window.location ='.$schedule_id.'; 
							</script>';  
						} 	
					}
					else
					{
						$errors=$post->errors('errors');
						$notice="";
						foreach($errors as $error) 
						{
							$notice.=$error."<br />";
						}
						$this->session->set('post', $_POST);
						$this->session->set('notice', array('message'=>$notice,'type'=>'error'));
					}
				
				}
				
				//if the submit is coming from the reserve seat form
				if(isset($_POST['reserve_seat_form']))
				{
					//no_csrf::check(3);
					$post = new Validation($_POST);
					$post->add_rules('res_name','required');
					$post->add_rules('res_phone','valid::digit','length[9]');
					$post->add_rules('res_idc','valid::digit');
					$post->add_rules('res_seat','required');
					
					if ($post->validate())
					{	
						//print_r($reserve->seat_occupants);exit;
						$res_name = ucwords(strtolower($_POST['res_name']));
						$res_phone = $_POST['res_phone'];
						$res_email = $_POST['res_email'];
						$res_seat = $_POST['res_seat'];
						$res_idc = $_POST['res_idc'];
							
							
						//first check if the seat is occupied, possibly in another tab
						if(isset($seats_and_occupants[$res_seat]))
						{
								$this->session->set('notice', array('message'=> Kohana::lang('backend.sorry_seat'). $res_seat. Kohana::lang('backend.given_out'),'type'=>'error'));
								url::redirect('admin/complete_schedule/'.$schedule_id);	
						} 
							
						//on the schedule table, we enter the reserver's details in a multidimensional array
						//in the second level of the array, 1->reserver's name, 2->reserver's phone, 3->reserver's email
						$seats_and_reservers[$res_seat] = array('name'=>$res_name,'phone'=>$res_phone,'email'=>$res_email,'idc'=>$res_idc);
						$schedule->reserved_seats = json_encode($seats_and_reservers);
						$schedule->save();
						
						$this->session->set('notice', array('message'=>Kohana::lang('backend.seat_reserved'),'type'=>'success'));
						url::redirect('admin/complete_schedule/'.$schedule_id);	
						//need to modify this section		
					}
					else
					{
						$errors=$post->errors('errors');
						$notice="";
							foreach($errors as $error) 
							{
								$notice.=$error."<br />";
							}
							$this->session->set('post', $_POST);
							$this->session->set('notice', array('message'=>$notice,'type'=>'error'));
							//url::redirect('admin/complete_schedule/'.$schedule_id."#reservation-tab");	
					}
				}
		if (isset($_POST['res_to_occ']))
		{
			//no_csrf::check(2);
			//convert a reserved seat to occupied.

			$details = unserialize($_POST['arr']);
			
			//the seat number is the key of the current array
			$seat_number = $_POST['res_to_occ'];
			$client_name = $details[$seat_number]['name'];
			$client_phone = $details[$seat_number]['phone'];
			$client_email = $details[$seat_number]['email'];
			$client_idc = $details[$seat_number]['idc'];
			
			//Now for many reservations for the same seat, we have seat 2, 2.1, 2.2. But the actual seat in this case is seat 2. Hence
			$actual_seat_number=(int)$seat_number;
			
			//first check if the seat is already occupied
			if(isset($seats_and_occupants[$actual_seat_number]))
			{
				$this->session->set('notice', array('message'=>Kohana::lang('backend.sorry_seat') .$actual_seat_number. Kohana::lang('backend.given_out'),'type'=>'error'));
				url::redirect('admin/complete_schedule/'.$schedule_id);	
			}	
			//$seats_and_occupants = json_decode($schedule->seat_occupants,true);
			$seats_and_occupants[$actual_seat_number] = array('name'=>$client_name,'phone'=>$client_phone,'email'=>$client_email,'idc'=>$client_idc);
			//when it's through, json encode and save back to db				
			$schedule->seat_occupants = json_encode($seats_and_occupants);

			
				//if the request was from the frontend, remove it from the reservation_requests table	
				if (isset($_POST['request_from_frontend']))
				{
					//$client_reservations=json_decode($client_reservations);
					unset($client_reservation_seat_infos[$seat_number]);
					$client_reservations->reserved_seats = json_encode($client_reservation_seat_infos);
					$client_reservations->save();
					
					//then save the schedule
					$schedule->save();
					
					//Create ticket and add to the ticket_details table
					Ticket_Detail_Model::add_ticket($this->agency_id, $schedule_id, $actual_seat_number, $client_name, $client_phone, $client_idc, $client_email, $schedule->ticket_price, $missed_status=0, $free_ticket=0, $free_ticket_reason='', $this->admin->username);
					
					
				}else
				{
					//else remove that entry from the seats and reservers column
					unset($seats_and_reservers[$seat_number]);
					$schedule->reserved_seats = json_encode($seats_and_reservers);
					$schedule->save();
					
					//Create ticket and add to the ticket_details table
					Ticket_Detail_Model::add_ticket($this->agency_id, $schedule_id, $seat_number, $client_name, $client_phone, $client_idc, $client_email, $schedule->ticket_price, $missed_status=0, $free_ticket=0, $free_ticket_reason='', $this->admin->username);
						
				}
								
			$parent = strtoupper(get::_parent($this->agency_id)->name);				
			//send confirmation SMS	if he had a phone
			if(!empty($client_phone))
			{
				$msg = Kohana::lang('backend.hello').$client_name. ". ".Kohana::lang('backend.res_confirmed'). $parent. Kohana::lang('backend.is_confirmed')." Bus: ". $schedule->bus_number ." ".Kohana::lang('backend.time').": ".date("g:i A",strtotime($schedule->departure_time));
				$to = '237'.$client_phone;
				$result = sms::save($msg,$to,$this->agency_id);
			}
							
					$from = get::town($schedule->from);
					$to = get::town($schedule->to);
					$ticket_price = $schedule->ticket_price;
					
					$ticket = printer::print_ticket($parent, $schedule->departure_time, $schedule->departure_date, $client_name, $client_phone, $actual_seat_number, $from, $to, $schedule->bus_number, $ticket_price, $schedule->id, $client_idc);	
					//first check if the option to auto-generate tickets is set
					$auto_ticket = settings::auto_ticket();
					if($auto_ticket == 1)
					{ 	
						//open the ticket in a new window
						echo "<script>
								win=window.open('$ticket','Print ticket','width=900,height=400');
								win.focus();
								
							  </script>
							  ";
						//DO NOT MODIFY THESE LINES. THEY DO THE REDIRECT ENSURING THAT PAGE REFRESH IS NOT POSSIBLE HERE, NOR THE BACK BUTTON  
						echo '<script>
								window.location ='.$schedule_id.'; 
						</script>'; 
					} 	
			
			
			
			//$to = "23796362464";
			//$result = "SMS is disabled";
			
			$this->session->set('notice', array('message'=>Kohana::lang('backend.client_confirmed')."<br/>Notice: ".@$result,'type'=>'success'));
			
			//$this->session->set('notice', array('message'=>"The client has been confirmed. <br/>Notice: ".$result,'type'=>'success'));
		}
		
		
		
		if (isset($_POST['reject_reserve']))
		{
			$details = unserialize($_POST['arr']);
			//the seat number is the key of the current array
			$seat_number = $_POST['reject_reserve'];
			
			//if the request was from the frontend, remove it from the appropriate column in the reservation_requests table		
			if (isset($_POST['request_from_frontend']))
			{
				//$client_reservations=json_decode($client_reservations);
				unset($client_reservation_seat_infos[$seat_number]);
				$client_reservations->reserved_seats = json_encode($client_reservation_seat_infos);
				$client_reservations->save();
							
			}else
			{
				unset($seats_and_reservers[$seat_number]);
				$schedule->reserved_seats = json_encode($seats_and_reservers);
				$schedule->save();
				$this->session->set('notice', array('message'=>Kohana::lang('backend.deleted_res'),'type'=>'success'));
			}
		}
		
		if (isset($_POST['empty_seat']))
		{

			$details = unserialize($_POST['arr1']);
			//the seat number is the key of the current array
			//$seat_number = key($details);
			$seat_number = $_POST['empty_seat'];
				
			//$seats_and_occupants = json_decode($schedule->seat_occupants,true);
			//$seats_and_occupants[$seat_number] = array('name'=>$client_name,'phone'=>$client_phone,'email'=>$client_email);
			//when it's through, json encode and save back to db
						//$schedule->seat_occupants = json_encode($seats_and_occupants);
			//then remove that entry from the seats and reservers column
			unset($seats_and_occupants[$seat_number]);
			$schedule->seat_occupants = json_encode($seats_and_occupants);
			$schedule->save();
			
			//Delete from ticket_details table
			Ticket_Detail_Model::delete_ticket($this->agency_id, $schedule_id, $seat_number);
						
			
			$this->session->set('notice', array('message'=>Kohana::lang('backend.client_deleted').Kohana::lang('backend.seat').$seat_number." ".Kohana::lang('backend.now_empty'),'type'=>'success'));
			url::redirect('admin/complete_schedule/'.$schedule_id);
		}
		
		if (isset($_POST['add_to_favourites']))
		{

			$details = unserialize($_POST['arr1']);
			//the seat number is the key of the current array
			//$seat_number = key($details);
			$seat_number = $_POST['add_to_favourites'];
				
			//Get the ticket infos
			$ticket_details = Ticket_Detail_Model::get_ticket($this->agency_id, $schedule_id, $seat_number);
			
			//open the contacts file for reading and writing
			$file = "files/contacts/contacts.txt";
			$handle = fopen($file,"r+") or die ('Can\'t open file');
			
			//Read the file into a string not array as array cant be stored to file
			//$favourite_contacts = file("contacts.txt");
			$favourite_contacts = file_get_contents("files/contacts/contacts.txt");
			
			//If the name exists in the favourites already
			//If John Mayer exists, you'll not be able to add John. Hence the '"' mark the beginning and the end of the name so only a match of the EXACT
			//name and order will fail to add again
			if (strpos($favourite_contacts,'"'.$ticket_details->ClientName.'"') !== false) {
				$this->session->set('notice', array('message'=>Kohana::lang('backend.contact_exists'),'type'=>'error'));
			}
			else{			
				//Remove the "]" from the end of the string. Will be replaced
				$favourite_contacts = rtrim($favourite_contacts,']');
								
				//Add the new contact and end the string with ]
				$favourite_contacts .= '{label: "'.$ticket_details->ClientName.'", phone: "'.$ticket_details->ClientPhone.'", email:"'.$ticket_details->ClientEmail.'", idc:"'.$ticket_details->ClientIDC.'" },';
				$favourite_contacts .="\n]";
				//{label: "Shu Mabel", phone: "674777444", email:"", idc:"0" },]
			
				//close the file
				fwrite($handle,$favourite_contacts) or die("Couldn't write to file");
				fclose($handle);
				$this->session->set('notice', array('message'=>Kohana::lang('backend.added_to_favourites'),'type'=>'success'));
			}
			
			url::redirect('admin/complete_schedule/'.$schedule_id);
		}
		//possibly add or modify passenger information
		if (isset($_POST['edit_passenger']))
		{
			$seat_number = $_POST['seat_number'];
			$client_name = $_POST['client_name'];
			$client_phone = $_POST['client_phone'];
			$client_email = $_POST['client_email'];
			$client_idc = $_POST['client_idc'];
			
			
			$post = new Validation($_POST);
			$post->add_rules('client_name','required');
			$post->add_rules('client_phone','valid::digit','length[9]');
			$post->add_rules('client_idc','valid::digit');
					
			if ($post->validate()){
					
				
				
				//Update seat and occupants array.
				$seats_and_occupants = json_decode($schedule->seat_occupants,true);
				$seats_and_occupants[$seat_number] = array('name'=>$client_name,'phone'=>$client_phone,'email'=>$client_email,'idc'=>$client_idc);
				//when it's through, json encode and save back to db
							
				$schedule->seat_occupants = json_encode($seats_and_occupants);
				$schedule->save();
				
				//Update ticket_details table
				Ticket_Detail_Model::update_ticket($this->agency_id, $schedule_id, $seat_number, $client_name, $client_phone, $client_email, $client_idc);
							
				
				$this->session->set('notice', array('message'=>Kohana::lang('backend.passenger_edited'),'type'=>'success'));
				url::redirect('admin/complete_schedule/'.$schedule_id);
			}
			else{
				$errors=$post->errors('errors');
				$notice="";
					foreach($errors as $error){
						$notice.=$error."<br />";
					}
					$this->session->set('post', $_POST);
					$this->session->set('notice', array('message'=>$notice,'type'=>'error'));
					url::redirect("admin/edit_passenger_details/$schedule_id/$seat_number");	
			}
		
		}
		//REPLACED BY JAVASCRIPT
		/*
		if(isset($_POST['print_single_ticket']))
		{
			$details = unserialize($_POST['arr1']);
			//the seat number is the key of the current array
			$seat_number = $_POST['print_single_ticket'];

			//$ticket = printer::fetch_ticket($schedule->departure_date,$schedule->bus_number,$seat_number);
			
			
			$ticket_details = Ticket_Detail_Model::get_ticket($this->agency_id, $schedule_id, $seat_number);
			
			$new_ticket = printer::print_ticket($parent, $schedule->departure_time, $schedule->departure_date, $ticket_details->ClientName, $ticket_details->ClientPhone, $ticket_details->SeatNumber, $from, $to, $schedule->bus_number, $ticket_price);	
						
			//$new_ticket = printer::regenerate_ticket($this->agency_id,$schedule->id,$seat_number);
			
			
			//open the ticket in a new window
			echo "<script>
					win=window.open('$new_ticket','Print ticket','width=900,height=400');
					win.focus();
				  </script>";
		} */
		
		if(isset($_POST['mark_absent']))
		{
			$details = unserialize($_POST['arr1']);
			//the seat number is the key of the current array
			$seat_number = $_POST['mark_absent'];
			
			//Delete from the seats and occupants array
			$ticket_details = Ticket_Detail_Model::mark_absent($this->agency_id,$schedule->id,$seat_number);
			//To be more secure, use ticket_id rather than a mixture of schedule, agency, and seat as thesse can be easily duplicated
			//$ticket_details = Ticket_Detail_Model::mark_absent($this->agency_id,$ticket_details_id);
			unset($seats_and_occupants[$seat_number]);
			$schedule->seat_occupants = json_encode($seats_and_occupants);
			$schedule->save();
			$this->session->set('notice', array('message'=>Kohana::lang('backend.client_marked_absent'),'type'=>'success'));
			url::redirect('admin/complete_schedule/'.$schedule_id);
			
		}
		if(isset($_POST['mark_present']))
		{
			$details = unserialize($_POST['arr1']);
			//the seat number is the key of the current array
			$seat_number = $_POST['mark_present'];

			$ticket_details = Ticket_Detail_Model::mark_present($this->agency_id,$schedule->id,$seat_number);
			
		}
		
		

			
		}	
			$view=new View('admin_complete_schedule');
			
		//For each fixed expense just show that on the view. Will be calculated after checkout.			
			$view->fixed_percentage_expenses = Fixed_Expense_Detail_Model::get_all_fixed_percentage_expenses($this->agency_id);
			$view->all_fixed_amt_expenses = Fixed_Expense_Detail_Model::get_all_fixed_amounts($this->agency_id);
			
			$bus = get::bus($schedule->bus_number);
			$view->bus=$bus;
		
			
			$view->schedule=$schedule;
			$view->expenses_for_schedule=$expenses_for_schedule;
			$view->client_reservations=$client_reservation_seat_infos;
			$view->towns=$towns;
			$view->seats_taken = count($seats_and_occupants);
			$view->seats_left = ($schedule->bus_seats - count($seats_and_occupants) - 1);
			$view->seats_and_occupants = $seats_and_occupants;
			$view->seats_and_reservers = $seats_and_reservers;
			
			//check if the contacts file exists and create it if it doesn't with the initial empty '[]'
			if(!file_exists("files/contacts/contacts.txt")){
				
				$file = "files/contacts/contacts.txt";
				$handle = fopen($file,"w") or die ('Can\'t open file');
				fwrite($handle,'[]') or die("Couldn't write to file");
				fclose($handle);
			}
			$favourite_contacts = file_get_contents("files/contacts/contacts.txt");
			$view->favourite_contacts = @$favourite_contacts;
			$this->template->content=$view;
			
	}
	
	function complete_schedule_js($schedule_id)
	{				
			Authlite::check_admin();
			$permitted_levels = array(2);
			Authlite::check_access($permitted_levels);
			//Authlite::check_integrity($schedule_id);
			Authlite::verify_referer();
			
			$date = date("Y-m-d");
			$this->template->title=Kohana::lang('backend.complete_schedule');
			
			//first check if the schedule is departed, and if so, send him to past_schedule
			//departed schedules cannot be modified, so if it's checked out and admin tries to go back, it shouldn't work
			$status = get::schedule_status($schedule_id);
			
			if ($status == 'departed')
			{
				$this->session->set('notice', array('message'=>Kohana::lang('backend.already_checkedout'),'type'=>'error'));
				url::redirect('admin/past_schedule/'.$schedule_id);
			}
			
			
			$towns = get::all_towns();
			//an array to hold seats and corresponding occupants
			$schedule=get::schedule($schedule_id);
			
			//get all expenses already registered for this schedule. i.e.bus & date
			//$expenses_for_schedule = get::expenses_for_bus_and_date($this->agency_id, $schedule->bus_number, $date);
			
			//rather use the schedule id for this purpose. It's more accurate
			$expenses_for_schedule = get::expenses_for_schedule($this->agency_id, $schedule_id);
			
			//this gets the entire object
			$client_reservations=get::client_reservations($schedule_id);
			
			//this gets only the json with the reserved seats details
			$client_reservation_seat_infos=json_decode($client_reservations->reserved_seats,true);
			
			//$reserve = ORM::factory('reservation')->where('schedule_id',$schedule_id)->find();
			//first check if the field in the db is not empty. If it is, create the array. If not, decode the array and populate			
			if (empty ($schedule->reserved_seats))
			{
				$seats_and_reservers = array();			
			}
			else
			{
				$seats_and_reservers = json_decode($schedule->reserved_seats,true); //true converts the object to an array
			}
			//first check if the field in the db is not empty. If it is, create the array. If not, decode the array and populate			
			if (empty ($schedule->seat_occupants))
			{
				$seats_and_occupants = array();			
			}
			else
			{
				$seats_and_occupants = json_decode($schedule->seat_occupants,true);
			}
			
			if($_POST)
			{	
				//if the submit is coming from the check out form
				if(isset($_POST['check_out_form']))
				{	
					//variable for special drop and free tickets
					$special_drop=false;
					$free_ticket=false;
					$free_ticket_reason='';
					
					$post = new Validation($_POST);
					//if it;s a free ticket
					if(isset($_POST['free_ticket'])){$free_ticket=true; $free_ticket_reason=$_POST['free_ticket_reason'];}
					
					//if it is a special drop, the price is required
					if(isset($_POST['spec_drop_check']))
					{
						//create a variable which we easily check each time to know if it's a special drop
						$special_drop=true;
						$post->add_rules('special_drop_price','required','valid::digit');
						$post->add_rules('special_drop_town','required');
					}					
					$post->add_rules('client_name','required');
					$post->add_rules('seat_number','required');
					$post->add_rules('client_email','valid::email');
					$post->add_rules('client_phone','valid::digit');
					$post->add_rules('client_idc','valid::digit');
					
					
					if ($post->validate())
					{
						
						$client_name = $_POST['client_name'];
						$client_phone = $_POST['client_phone'];
						$client_email = $_POST['client_email'];
						$seat_number = $_POST['seat_number'];
						$client_idc = $_POST['client_idc'];
						
						if($special_drop){
							$special_drop_price = $_POST['special_drop_price'];
							$special_drop_town = $_POST['special_drop_town'];
							}
						
						//THE POPULATION!
						//Seat number is key, client's array is value
						//first check if the seat is occupied, possibly in another tab
						if(isset($seats_and_occupants[$seat_number]))
						{
							$this->session->set('notice', array('message'=>Kohana::lang('backend.sorry_seat'). $seat_number . Kohana::lang('backend.given_out'),'type'=>'error'));
							url::redirect('admin/complete_schedule/'.$schedule_id);	
						}
						//int converts the boolean value of free ticket into an integer
						if($special_drop){
							$seats_and_occupants[$seat_number] = array('name'=>$client_name,'phone'=>$client_phone,'email'=>$client_email,'sd_price'=>$special_drop_price,'sd_town'=>$special_drop_town,'idc'=>$client_idc,'free_ticket'=>(int)$free_ticket);
						}else{
							$seats_and_occupants[$seat_number] = array('name'=>$client_name,'phone'=>$client_phone,'email'=>$client_email,'idc'=>$client_idc,'free_ticket'=>(int)$free_ticket);
						}
						//var_dump($seats_and_occupants);exit;
						//when it's through, json encode and save back to db
						$schedule->seat_occupants = json_encode($seats_and_occupants);
						$schedule->save();
						
						//the ticket price and destination might be variable depending whether it's a regula seat or special drop
						//this should be reflected on the ticket
						if($special_drop){
							$to = get::town($special_drop_town);
							$ticket_price = $special_drop_price;
						}else{
							$to = get::town($schedule->to); 
							$ticket_price = $schedule->ticket_price;
							}
						//If it's a free ticket, store ticket_price 0	
						if($free_ticket){$ticket_price=0;}
							
						//Also save in the tickets table
						Ticket_Detail_Model::add_ticket($this->agency_id, $schedule_id, $seat_number, $client_name, $client_phone, $client_idc, $client_email, $ticket_price, $missed_status=0, (int)$free_ticket, $free_ticket_reason=$free_ticket_reason, $this->admin->username);
						
						//If it's a conversion from a missed ticket to a free ticket, it's no more a missed ticket. Delete from ticket_details table, as the creation 
						//of a new ticket will add a new entry.
						if(isset($_POST['from_missed_ticket']))
						{
							$ticket_id=$_POST['from_missed_ticket'];
							Ticket_Detail_Model::delete_ticket_by_id($ticket_id);;
						}
						
						$this->session->set('notice', array('message'=>Kohana::lang('backend.seat')." <b>".$seat_number."</b> ".Kohana::lang('backend.seat_checked_out'),'type'=>'success'));
						$parent = strtoupper(get::_parent($this->agency_id)->name);
						$from = get::town($schedule->from);
						
							
						//use the parent id to check which ticket to use	
						$parent_id = get::_parent($this->admin->agency_id)->id;
						
						//check the parent agency and generate the ticket 
						//if it's type 2, use type 2. Else use default ticket
						//i
						$ticket_type_1 = settings::ticket_type_1();
						$ticket_type_2 = settings::ticket_type_2();
						if($ticket_type_2==1){
							
							$ticket = printer::print_ticket_type_2($parent, $schedule->departure_time, $schedule->departure_date, $client_name, $client_phone, $seat_number, $from, $to, $schedule->bus_number, $ticket_price);	
						
						}else{
							
							$ticket = printer::print_ticket($parent, $schedule->departure_time, $schedule->departure_date, $client_name, $client_phone, $seat_number, $from, $to, $schedule->bus_number, $ticket_price);	
						}
						//first check if the option to auto-generate tickets is set
						$auto_ticket = settings::auto_ticket();
						
							
					}
					else
					{
						$errors=$post->errors('errors');
						$notice="";
							foreach($errors as $error) 
							{
								$notice.=$error."<br />";
							}
							$this->session->set('post', $_POST);
							$this->session->set('notice', array('message'=>$notice,'type'=>'error'));
					}
				
				}
				
		}	
			$view=new View('complete_schedule_bordereaux');
			$bordereaux_view=new View('complete_schedule_bordereaux');
			
		//For each fixed expense which is a percentage, just show that on the view. Will be calculated after checkout.			
			$view->fixed_percentage_expenses = Fixed_Expense_Detail_Model::get_all_fixed_percentage_expenses($this->agency_id);
			
			$view->schedule=$schedule;
			$view->expenses_for_schedule=$expenses_for_schedule;
			$view->client_reservations=$client_reservation_seat_infos;
			$view->towns=$towns;
			$view->seats_taken = count($seats_and_occupants);
			$view->seats_left = ($schedule->bus_seats - count($seats_and_occupants));
			$bordereaux_view->seats_and_occupants = $seats_and_occupants;
			$view->seats_and_occupants = $seats_and_occupants;
			$bordereaux_view->seats_and_reservers = $seats_and_reservers;
			$view->seats_and_reservers = $seats_and_reservers;
			$this->template->content=$view;
			//echo $bordereaux_view;exit;
			
	}
	
	
	
	public function checkout_schedule($schedule_id)
	{	
		Authlite::check_admin();
		Authlite::verify_referer();
		
		$this->template->title=Kohana::lang('backend.check_out_schedule');
		$schedule = get::schedule($schedule_id);
		
		//check if there are reserved seats on this bus. this MUST be declared empty or converted to 'occupied'
		// [] is left over when reserved seats are converted to occupied.
		if ($schedule->reserved_seats !=='[]' && $schedule->reserved_seats !=='')
		{
			$this->session->set('notice', array('message'=>Kohana::lang('backend.error_reserved'),'type'=>'error'));
			url::redirect('admin/complete_schedule/'.$schedule_id);
		}
		
		// now ensure an empty bus can't be checked out	
		elseif(empty($schedule->seat_occupants))
		{
			$this->session->set('notice', array('message'=>Kohana::lang('backend.err_empty_bus'),'type'=>'error'));
			url::redirect('admin/complete_schedule/'.$schedule_id);
		}
		
		// now ensure that schedule cannot be checked out when bus number is unknown
		elseif($schedule->bus_number =='UNKNOWN')
		{
			$this->session->set('notice', array('message'=>Kohana::lang('backend.select_valid_bus'),'type'=>'error'));
			url::redirect('admin/edit_schedule/'.$schedule_id);
		}
		
		else
		{		
			$seats_and_occupants = json_decode($schedule->seat_occupants,true);
			//number of seats and occupants
			//calculate the amount from special drops
			$special_drop_total = 0;
			//count the number of special drops
			$special_drop_count = 0;
			//count the number of free tickets
			$free_ticket_count = 0;
		
		
			foreach($seats_and_occupants as $s)
			{
				//get the special drop price, and if it's not defined, MOVE ON
				if(isset($s['sd_price']))
				{
					$special_drop_total += @$s['sd_price'];
					$special_drop_count++;
				}
				
				if(isset($s['free_ticket']))
				{
					if($s['free_ticket']=='1')
					{
						$free_ticket_count++;
					}
				}
			}

				
			//number of seats and occupants
			$no_so = count($seats_and_occupants);
			//regular seats are those paid for at normal price (as opposed to special drops and free tickets)				
			//speculated normal cost of seats taken by special drops and free tickets called amount lost
			
			
			$amount_lost = ($schedule->ticket_price) * ($special_drop_count + $free_ticket_count);
			//total is number of seats occupied times ticket price, minus speculated normal cost of special drops and free tickets(lost), plus actual amount got from the special drops		
			$schedule->total_amount = ($no_so * $schedule->ticket_price) - $amount_lost + $special_drop_total;
			
			
			$schedule->status = 'departed';
			//Set the driver as the current bus driver for that bus.
			$bus=get::bus($schedule->bus_number);
			$schedule->driven_by = $bus->driver;
			
			//Only the first time, is it being checked out. After that, these should not be modified
			if($schedule->update_count == 0){
				$schedule->CheckedOutBy = strtoupper($this->admin->username);
				//save the current time
				$schedule->checked_out_time = date("H:i:s");
				$schedule->checked_out_on = date("Y-m-d H:i:s");
			}
			
			$schedule->save();
			
			//If the schedule had once been checked out, this was already done. So don't recalculate
			if($schedule->update_count == 0){
			
				//Now, add any fixed expenses WITH FIXED AMOUNTS. Those with percentages will be calculated upon checkout.
				$all_fixed_amt_expenses = Fixed_Expense_Detail_Model::get_all_fixed_amounts($this->agency_id);
				foreach($all_fixed_amt_expenses as $expense)
				{
					Expense_Model::register_expense($this->agency_id, $schedule->bus_number, $expense->Value, $expense->Name, $schedule->departure_date, strtoupper($this->admin->username), '', $schedule->id);
				}
				
				//Now, we need to calculate the fixed percentage expenses that were based on the income from this schedule, and add in the expense table.
				//If this is done in the past schedule view, it will affect even schedules that were created before the fixed expense rule was created.

				$all_fixed_percentage_expenses = Fixed_Expense_Detail_Model::get_all_fixed_percentage_expenses($this->agency_id);
				foreach($all_fixed_percentage_expenses as $expense)
				{
					Expense_Model::register_expense($this->agency_id, $schedule->bus_number, (($expense->Value / 100) *$schedule->total_amount), $expense->Name, $schedule->departure_date, strtoupper($this->admin->username), '', $schedule->id);
				}
				
				//For each parcel put on this bus/schedule, mark the parcel as sent
				$parcels_on_schedule = Parcel_Model::get_parcels_on_schedule($schedule_id);
				foreach($parcels_on_schedule as $parcel)
				{
					Parcel_Model::mark_sent($parcel->id);
				}
			}
			$this->session->set('notice', array('message'=>Kohana::lang('backend.bus_departed'),'type'=>'success'));
			url::redirect('admin/past_schedule/'.$schedule_id);
		}	
	}
	
	public function modify_past_schedule($schedule_id)
	{	//If a schedule has been checked out, use this to make it current and make any modifications.
		Authlite::check_admin();
		Authlite::verify_referer();
		
		$notice = Schedule_Model::make_current($this->agency_id,$schedule_id, $this->admin->username);
			
		$this->session->set('notice', array('message'=>$notice,'type'=>'success'));
		url::redirect('admin/complete_schedule/'.$schedule_id);		
	}
	
	
	public function start_boarding_schedule_with_sms($schedule_id)
	{	
		Authlite::check_admin();
		//Authlite::check_integrity($schedule_id);
		Authlite::verify_referer();
		
		$this->template->title='Start boarding schedule';
		$schedule = get::schedule($schedule_id);
		
		$seats_and_occupants = json_decode($schedule->seat_occupants,true);
		$schedule->loading = 1;
		$schedule->save();
		
		$i = 0;
		//first ensure all seats are not empty
		if(!empty($seats_and_occupants))
		{
			foreach ($seats_and_occupants as $key=>$value)
			{	
				if(!empty($value['phone']))
				{
					//send notification SMS			
					$msg = "Hi ". $value['name'] .", your bus will take off in a few moments. Thanks for trusting ". get::_parent_name($this->agency_id).".";
					$to = '237'. $value['phone'];
					
					$result = sms::save($msg,$to,$this->agency_id);
					$i++;
				}
			}
		}
			$this->session->set('notice', array('message'=>"$i SMS sent",'type'=>'success'));
			url::redirect('admin/complete_schedule/'.$schedule_id);		
	}
	
	public function send_sms_after_schedule($schedule_id)
	{	
		Authlite::check_admin();
		//Authlite::check_integrity($schedule_id);
		Authlite::verify_referer();
		
		//$this->template->title='Start boarding schedule';
		$schedule = get::schedule($schedule_id);
		
		$seats_and_occupants = json_decode($schedule->seat_occupants,true);
		
		
		
		if($_POST)
		{
			
			$post = new Validation($_POST);
			$post->add_rules('sms_message','required');
			
			if ($post->validate())
			{	
				$msg=$_POST['sms_message'];
				$i = 0;
				//first ensure all seats are not empty
				if(!empty($seats_and_occupants))
				{	
					foreach ($seats_and_occupants as $key=>$value)
					{	
						if(!empty($value['phone']))
						{
							//send notification SMS			
							//$msg = "Hi ". $value['name'] .", your bus will take off in a few moments. Thanks for trusting ". get::_parent_name($this->agency_id).".";
							
							//var_dump($msg);exit;
							$to = '237'. $value['phone'];
							
							$result = sms::save($msg,$to,$this->agency_id);
							$i++;
						}
					}
				}
					$this->session->set('notice', array('message'=>"$i SMS saved.",'type'=>'success'));
					url::redirect('admin/past_schedule/'.$schedule_id);	
						
				
			}
			else
			{
				$errors=$post->errors('errors');
				$notice="";
					foreach($errors as $error) 
					{
						$notice.=$error."<br />";
					}
					$this->session->set('notice', array('message'=>$notice,'type'=>'error'));
			}
		}
		
		
		
			
	}
	
	public function start_boarding_schedule_without_sms($schedule_id)
	{	//var_dump($_SESSION);exit;
		Authlite::check_admin();
		//Authlite::check_integrity($schedule_id);
		Authlite::verify_referer();
		
		$this->template->title = Kohana::lang('backend.start_boarding');
		$schedule = get::schedule($schedule_id);
		
		$seats_and_occupants = json_decode($schedule->seat_occupants,true);
		$schedule->loading = 1;
		$schedule->save();
		
			$this->session->set('notice', array('message'=>"Started boarding. No SMS sent",'type'=>'success'));
			url::redirect('admin/complete_schedule/'.$schedule_id);		
	}	
	
	public function delay_schedule($schedule_id)
	{
		Authlite::check_admin();
		//Authlite::check_integrity($schedule_id);
		Authlite::verify_referer();
		
		$schedule = get::schedule($schedule_id);
		$schedule->loading = 2;
		$schedule->save();
		
		$this->template->title='Start boarding schedule';
		$schedule = get::schedule($schedule_id);
		
			$this->session->set('notice', array('message'=>Kohana::lang('backend.bus_delayed'),'type'=>'success'));
			url::redirect('admin/complete_schedule/'.$schedule_id);		
	}
	
	public function delete_pending_ticket($ticket_id)
	{
		Authlite::check_admin();
		Authlite::verify_referer();
		
		$notice = Ticket_Detail_Model::delete_ticket_by_id($ticket_id);
			
		//$this->template->title='Start boarding schedule';
		//$schedule = get::schedule($schedule_id);
		
			$this->session->set('notice', array('message'=>$notice,'type'=>'success'));
			url::redirect('admin/tickets_pending');		
	}
	
	public function delete_online_purchase($purchase_id)
	{
		Authlite::check_admin();
		Authlite::verify_referer();
		
		$notice = Online_Purchase_Model::safe_delete_online_purchase($this->agency_id,$purchase_id);
			
			$this->session->set('notice', array('message'=>$notice,'type'=>'success'));
			url::redirect('admin/online_purchases');		
	}
	
	public function update_contacts()
	{
		Authlite::check_admin();
		Authlite::verify_referer();
		
		//$today =(date("Y-m-d"));
		$file = "contacts.txt";
		$handle = fopen($file,"w") or die ('Can\'t open file');
		
		
		$contacts = $this->db->query('SELECT ClientName, ClientPhone, ClientEmail, ClientIDC, count(*) as number FROM `ticket_details` group by ClientName having number  >= 0');
		
		//Initialize the beginning of the data variable
		$data="[\n";
		foreach($contacts as $contact){
			//format the price and time before returning
			//The order and use of single and double quotes is important. Javascript doesn't take shit!
			$data .= '{label: "'.$contact->ClientName.'", phone: "'.$contact->ClientPhone.'", email:"'.$contact->ClientEmail.'", idc:"'.$contact->ClientIDC.'" },';
			$data .= "\n";
		}
		$data .=']';
		//End of the data variable
		
		//write the contacts to file
		fwrite($handle,$data) or die("Couldn't write to file");
		fclose($handle);
		
		$favourite_contacts = file_get_contents("files/contacts/contacts.txt");
		var_dump($favourite_contacts);exit;
				
	}
	
	/*
	public function checkout_seat($schedule_id)
	{
		Authlite::check_admin();
		Authlite::verify_referer();
		
		//no_csrf::check();
		$view = new View('admin_complete_schedule');
		$this->template->content=$view;

	} */
	
	
	
	public function past_schedule($schedule_id)
	{
		Authlite::check_admin();	
		Authlite::verify_referer();
		//Authlite::check_integrity($schedule_id);
		
		$this->template->title = Kohana::lang('backend.display_past');
		
		$schedule = get::schedule($schedule_id);
		$expenses_for_schedule = get::expenses_for_schedule($this->agency_id,$schedule_id);
		//Get the settings for this agency to know which branch the cash is being posted to
		$settings = Setting_Model::get($this->agency_id);
		
		$seats_and_reservers = json_decode($schedule->reserved_seats,true);
		$seats_and_occupants = json_decode($schedule->seat_occupants,true);
		$view = new View('admin_past_schedule');
		
		//number of seats and occupants
		//calculate the amount from special drops
		$special_drop_total = 0;
		//count the number of special drops
		$special_drop_count = 0;
		//count the number of free tickets
		$free_tickets_price = 0;
		$free_ticket_count = 0;
		
		foreach($seats_and_occupants as $s)
		{
			//get the special drop price, and if it's not defined, MOVE ON
			if(isset($s['sd_price']))
			{
				$special_drop_total += @$s['sd_price'];
				$special_drop_count++;
			}
			//if the value of free ticket is 1
			if(isset($s['free_ticket']) && $s['free_ticket']==1)
			{
			//if it's also a special drop, get the price to know how much is lost
				if(isset($s['sd_price'])){
					$free_tickets_price += @$s['sd_price'];}
				else{
					$free_tickets_price += $schedule->ticket_price;
				}
				$free_ticket_count++;
			}
		}

		//number of seats and occupants
		$no_so = count($seats_and_occupants);
		//for now we count only occupants as reservers will be converted to occupants upon payment

		//regular seats are those paid for at normal price (as opposed to special drops)
		$regular_seats = $no_so - $special_drop_count;
		
		
		$view->seats_and_occupants = $seats_and_occupants;
		$view->seats_and_reservers = $seats_and_reservers;
		$view->no_so = $no_so;
		$view->empty_seats = $schedule->bus_seats - $no_so;
		$view->ticket_price = $schedule->ticket_price;
		
		$parent = get::_parent($this->agency_id);
		$siblings = get::agency_siblings($this->agency_id,$parent);	
		
		
		//speculated normal cost of seats taken by special drops called amount lost
		$amount_lost = ($schedule->ticket_price) * $special_drop_count;
		//total is number of seats occupied times ticket price, minus speculated normal cost of special drops(lost), plus actual amount got from the special drops		
		$view->total = ($no_so * $schedule->ticket_price) - $amount_lost + $special_drop_total - $free_tickets_price;
		$view->siblings = $siblings;
		$view->schedule = $schedule;
		$view->free_ticket_count = $free_ticket_count;
		$view->special_drop_count = $special_drop_count;
		$view->special_drop_total = $special_drop_total;
		$view->regular_seats = $regular_seats;
		$view->expenses_for_schedule = $expenses_for_schedule;
		$view->settings = $settings;
		$this->template->content = $view;

	}
	
	/*
	public function control($agency_id, $user_group=3) 
	{	
		Authlite::check_admin();
		Authlite::verify_referer();
		$permitted_levels = array(3,4);
		Authlite::check_access($permitted_levels);
		Authlite::check_agency_integrity($agency_id);
		//print_r($not);exit;
		//print_r($this->admin->access_level);exit;
		$this->template->title = Kohana::lang('backend.welcome_backend');
		$view=new View ('admin_control');
		$towns = get::all_towns();
		$admin_town =get::admins_town($this->admin->agency_id);
		$parent_id = get::_parent($this->admin->agency_id);
		//$current_schedules = get::all_parent_current_schedules($parent_id);
		//$departed_schedules = get::all_parent_departed_schedules($parent_id);


	
		//now, if it's a branch manager, limit his query to his town
		//show all only for general manager
		if ($user_group == 3){
			$current_schedules=get::ten_agency_current_schedules($agency_id);
			$departed_schedules=get::ten_agency_departed_schedules($agency_id);
			}
		if ($user_group == 4){
			$current_schedules=get::all_parent_current_schedules($parent_id);
			$departed_schedules=get::all_parent_departed_schedules($parent_id);
			}
		$date=date('d-m-Y');
		
		$view->current_schedules=$current_schedules;
		$view->departed_schedules=$departed_schedules;
		
		$view->towns=$towns;
		$this->template->content=$view;
	} */
	
	
	/*
	public function schedule_by_date()
	{	
		Authlite::check_admin();
		Authlite::verify_referer();
		
		$agency_id = $this->agency_id;
		$parent_id = get::_parent($agency_id)->id;
		
		//$type: either parent or agency. Helps distinguish which the BM or GM sees.
		$type = $this->admin->admin_group_id;
		$this->template->title = Kohana::lang('backend.welcome_backend');
		if($_POST)
		{
			$post = new Validation($_POST);
			$post->add_rules('date','required');
			if ($post->validate())
			{	
				
			$date = $_POST['date'];
			//$schedules = ORM::FACTORY('schedule')->where('agency_id',$agency_id)->where('departure_date',$date)->find_all();
			
			//if it's a GM, type 2
			if ($type==4)
			{
				$_30seats = ORM::FACTORY('schedule')->join('agencies','agencies.id','schedules.agency_id')->where('parent_id',$parent_id)->where('departure_date',$date)->where('status','departed')->where('bus_seats',30)->count_all();
				$_55seats = ORM::FACTORY('schedule')->join('agencies','agencies.id','schedules.agency_id')->where('parent_id',$parent_id)->where('departure_date',$date)->where('status','departed')->where('bus_seats',55)->count_all();
				$_70seats = ORM::FACTORY('schedule')->join('agencies','agencies.id','schedules.agency_id')->where('parent_id',$parent_id)->where('departure_date',$date)->where('status','departed')->where('bus_seats',70)->count_all();
				
				$departed_schedules = get::all_parent_departed_schedules_by_date($parent_id,$date);				
				$current_schedules = get::all_parent_current_schedules_by_date($parent_id,$date); 
			}
			
			else{
			$_30seats = ORM::FACTORY('schedule')->where('agency_id',$agency_id)->where('departure_date',$date)->where('status','departed')->where('bus_seats',30)->count_all();
			$_55seats = ORM::FACTORY('schedule')->where('agency_id',$agency_id)->where('departure_date',$date)->where('status','departed')->where('bus_seats',55)->count_all();
			$_70seats = ORM::FACTORY('schedule')->where('agency_id',$agency_id)->where('departure_date',$date)->where('status','departed')->where('bus_seats',70)->count_all();
			
			$departed_schedules = get::all_agency_departed_schedules_by_date($agency_id,$date);				
			$current_schedules = get::all_agency_current_schedules_by_date($agency_id,$date); }
			
			$parcels=get::all_agency_parcels_by_date($agency_id,$date);

			//Calculate total amount expected from departed buses.
			$total = 0;
			foreach($departed_schedules as $ds){
				$seats_and_occupants = json_decode($ds->seat_occupants,true);
				$no_so = count($seats_and_occupants);
				$total += $no_so * $ds->ticket_price;
			}
			}
			else
			{
				$errors=$post->errors('errors');
				$notice="";
					foreach($errors as $error) 
					{
						$notice.=$error."<br />";
					}
					$this->session->set('notice', array('message'=>$notice,'type'=>'error'));
					url::redirect('admin/main/'.$this->admin->agency_id);
			}
	}
		$view = new View('admin_schedule_by_date');
		$view->departed_schedules = $departed_schedules;
		$view->current_schedules = $current_schedules;
		$view->parcels=$parcels;
		$view->_30seats = $_30seats;
		$view->_55seats = $_55seats;
		$view->_70seats = $_70seats;
		$view->total = $total;
		$view->date = $date;
			
		$this->template->content = $view;
		
	}	*/
	
	
	//type is either departed or current
	public function all_schedules($type, $sort="sort_by_date")
	{	
		Authlite::check_admin();
		$permitted_levels = array(2,3,4);
		Authlite::check_access($permitted_levels);
		Authlite::verify_referer();
		$this->template->title = Kohana::lang('backend.viewing_all_schedules');		
		$view = new View('admin_all_schedules');
		
		$agency_id = $this->agency_id;
		
		if($type=="incoming"){
			
			$incoming_schedules = Incoming_schedule_Model::get_all_today($agency_id);
			$view->incoming_schedules = $incoming_schedules;	
			//$view->schedules = '';	
			//$view->sched_type = '';	
		}else{
		
			//do the sorting
			if ($sort == "sort_by_time"){$orderby = "departure_time";}
			elseif ($sort == "sort_by_bus"){$orderby = "bus_number";}
			elseif ($sort == "sort_by_destination"){$orderby = "to";}
			elseif ($sort == "sort_by_origin"){$orderby = "from";}
			elseif ($sort == "sort_by_time"){$orderby = "departure_time";}
			elseif ($sort == "sort_by_date"){$orderby = "departure_date";}
			//default orderby, to avoid bug when none is specified
			else($orderby = "departure_date");
			
			//pagination
			$per_page = 20;
			if ($type == 'current')
			{
				$all= get::all_agency_current_schedules($agency_id);
			}
			else
			{
				$all= get::all_agency_departed_schedules($agency_id);
			}
			
			$total = count($all);
			$this->pagination = new Pagination(array(
				// 'base_url'    => 'welcome/pagination_example/page/', // base_url will default to current uri
				'uri_segment'    => 'page', // pass a string as uri_segment to trigger former 'label' functionality
				'total_items'    => $total, // use db count query here of course
				'items_per_page' => $per_page , // it may be handy to set defaults for stuff like this in config/pagination.php
				'style'          => 'punbb', // pick one from: classic (default), digg, extended, punbb, or add your own!
				//'query_string'   => "ORM::factory('schedule')->where('agency_id',$agency_id)->where('status','current')->find_all()"
			));
			
			if ($type == 'current')	{
				//$schedules = get::all_agency_current_schedules($agency_id);
				$schedules=$this->db->select('schedules.id,bus_number,to,departure_date,departure_time,towns.name AS town_name')->from('schedules')->join('towns','towns.id=schedules.to')->where('agency_id',$agency_id)->where('status','current')->where('Deleted','0')->limit($per_page,$this->pagination->sql_offset)->orderby($orderby,'desc')->get();			
			}else		{
				//$schedules = get::all_agency_departed_schedules($agency_id);
				//$schedules=ORM::factory('schedule')->where('agency_id',$agency_id)->where('status','departed')->where('Deleted','0')->limit($per_page,$this->pagination->sql_offset)->orderby($orderby,'desc')->find_all();			
				$schedules=$this->db->select('schedules.id,bus_number,to,departure_date,departure_time,checked_out_time,towns.name AS town_name')->from('schedules')->join('towns','towns.id=schedules.to')->where('agency_id',$agency_id)->where('status','departed')->where('Deleted','0')->limit($per_page,$this->pagination->sql_offset)->orderby($orderby,'desc')->get();			
			}
		$view->schedules = $schedules;	
		}
		
		
		$this->template->content = $view;
	
	}
	
	
	public function res_req()
	{	
		Authlite::check_admin();
		Authlite::verify_referer();
		
		$schedule_id = 12;		
		return $schedule_id;		
		$client_reservations=get::client_reservations($schedule_id);		
		$view = new View('admin_reservation_requests_div');	
		$view->client_reservations=json_decode($client_reservations->reserved_seats,true);
	}
	
	public function edit_passenger_details($schedule_id, $seat_number)
	{	//var_dump($schedule_id);
		//var_dump($seat_number);exit;
		Authlite::check_admin();
		Authlite::verify_referer();
		$this->template->title='Edit passenger details';
		
		
		$details = Ticket_Detail_Model::get_ticket($this->agency_id, $schedule_id, $seat_number);	
		
		$view = new View('admin_edit_passenger_details');	
		$view->details=$details;
		$view->schedule_id=$schedule_id;
		$view->seat_number=$seat_number;
		$this->template->content = $view;
		
	}
	

	public function safe_delete_schedule($schedule_id)
	{		
			Authlite::check_admin();
			Authlite::verify_referer();
			
			$this->template->title=Kohana::lang('backend.delete_admin');
			
			//First delete the schedule
			$notice = Schedule_Model::safe_delete_schedule($this->agency_id,$schedule_id);

			//Then delete the expenses related to that schedule.
			$related_expenses = get::expenses_for_schedule($this->agency_id,$schedule_id);
			foreach($related_expenses as $exp)
			{
				Expense_Model::safe_delete_expense($this->agency_id,$exp->id);
			}
			
			//Move every ticket to pending.
			//First get every ticket on this schedule.
			$tickets_for_schedule = Ticket_Detail_Model::get_all_for_schedule($this->agency_id, $schedule_id);
			
			//Mark them absent
			foreach($tickets_for_schedule as $ticket){
				$mark_absent = Ticket_Detail_Model::mark_absent($this->agency_id,$schedule_id,$ticket->SeatNumber);
			}
			$this->session->set('notice', array('message'=>$notice,'type'=>'success'));
			url::redirect('admin');
			
			$this->template->content='';
	}
	
	public function check_schedules()
	{	
		Authlite::check_admin();
		Authlite::verify_referer();
		$agency_id = $this->agency_id;
	
		$this->template->title = Kohana::lang('backend.check_schedules');		
		//$view = new View('admin_parcels');
		//$this->template->content = $view;
		
		
		if($_POST)
		{	
			$post = new Validation($_POST);
			//bus number is whichever comes from the form, either the existing or the one to be searched
			//if it's the search, then it shouldn't be empty
				
				$post->add_rules('schedule_id','required');
					
			
			if ($post->validate())
			{		
				$schedule_id = $_POST['schedule_id'];
				//first ensure the first 2 chars are SC and the next 3 chars must be the agency's id and total length is not less than 6 chars 
				if(strtoupper(substr($_POST['schedule_id'],0,2)) != 'SC'  OR substr($_POST['schedule_id'],2,3) != $this->agency_id OR strlen($_POST['schedule_id']) < 6)
				{
					$this->session->set('notice', array('message'=>'Invalid scheduleID','type'=>'error'));
					url::redirect('parcels/check_parcels');
				}
				//Start from position 5 as the first 2 chars are sc and the next 3 are the agency id
				$schedule_id = substr($_POST['schedule_id'],5);	
				//var_dump($schedule_id);exit;
				
			
			
				
				$schedule = Schedule_Model::get($schedule_id);
				if($schedule->status=='current')
				{
					$this->session->set('notice', array('message'=>Kohana::lang('backend.details_current_schedule')." SC$this->agency_id$schedule_id ",'type'=>'success'));
					url::redirect('admin/complete_schedule/'.$schedule->id);
				}elseif($schedule->status=='departed')
				{	
					$this->session->set('notice', array('message'=>Kohana::lang('backend.details_past_schedule')." SC$this->agency_id$schedule_id ",'type'=>'success'));
					url::redirect('admin/past_schedule/'.$schedule->id);
				}
				else{
					
					$this->session->set('notice', array('message'=>Kohana::lang('backend.schedule_current_or_unfound'),'type'=>'error'));
				}
			 
			}
			else
			{
				$errors=$post->errors('errors');
				$notice="";
				foreach($errors as $error) 
				{
					$notice.=$error."<br />";
				}
				$this->session->set('notice', array('message'=>$notice,'type'=>'error'));
				url::redirect('expenses');	
			}}
			$view = new View('admin_all_schedules');
			//$view->date = $date;
			//$view->sent_parcels = $sent_parcels;
			//$view->incoming_parcels = $incoming_parcels;
			//$view->count_sent = $count_sent;
			//$view->count_incoming = $count_incoming;
			$this->template->content = $view;
		
		}
	
	/*
	public function lang(){
		
		
		//$this->session->set('lang',array('en_US', 'French_France'));
		$lang = $this->session->get('lang');
	
		//first check if there's a language in session
		//if(isset($lang)){
			//now if it's english, make it french
			if($lang['0']=='en_US'){
				$this->session->set('lang',array('fr_FR', 'French_France'));
				Kohana::config_set('locale.language', array('fr_FR', 'French_France'));
			}else{
				$this->session->set('lang',array('en_US', 'English_United States'));
				Kohana::config_set('locale.language', array('en_US', 'English_United States'));
			}
		//}
		//go back to the page we came from
		echo "<script>history.go(-1)</script>";
	} */
	
	

	
	public function lang()
	{
			//var_dump($_SERVER['HTTP_REFERER']);exit;
			//if it's french, make it english and otherwise
			$view='';
			if($this->lang['0']=='fr_FR'){
				$this->lang = array('en_US', 'English_United States');
			}else{
				$this->lang = array('fr_FR', 'French_France');
			}
			
			$this->template->title="Change language";
		
			$this->session->set('lang',$this->lang);
			
			//go back to the page we came from
			url::redirect('admin/login');
			//echo "<script>history.go(-1)</script>";
	}
	
	public function test(){
		var_dump($_GET);exit;
	}
	
	
	
	/*
	public function add_admin()
	{			
		if (!isset($_SERVER['PHP_AUTH_USER'])) {
			header("WWW-Authenticate: Basic realm=\"Private Area\"");
			header("HTTP/1.0 401 Unauthorized");
			print "Sorry - you need valid credentials to be granted access!\n";
			exit;
		} else 
		{
			if (($_SERVER['PHP_AUTH_USER'] == 'admin') && ($_SERVER['PHP_AUTH_PW'] == 'Christ')) {
			print "Welcome to the private area!";
			$this->template->title = "Add a new administrator";
			$parents = get::all_parents();
			$towns = get::all_towns();
			$agencies = get::all_agencies();
			$groups = get::all_admingroups();
			//will use http header to secure this function so only trusted members can add an admin
			if($_POST) 
			{	
				$post=new Validation($_POST);
				$post->add_rules("username", "required");
				$post->add_rules("password", "required");
				if ($post->validate())
				{
				
					//need to ensure uniqueness of admin name
					$parent = $_POST['parent'];
					$town = $_POST['town'];
					$password = $_POST['password'];
					$username = $_POST['username'];
					$admin_group_id = $_POST['admin_group'];
					$user_exists = ORM::FACTORY('admin')->where('username',$username)->count_all();
					
						if ($user_exists)
						{
							$this->session->set('notice', array('message'=>Kohana::lang('backend.username_taken'),'type'=>'error'));
						}else
						{
							$notice = Admin_Model::add_admin($password, $username, $admin_group_id, $parent, $town);
						
							//Add him all main module and dashboard elements
							$main_modules = Main_Module_Model::get_all();
							$dashboard_items = Dashboard_Item_Model::get_all();

						
							foreach($main_modules as $module) 
						{	
							
							Admin_Module_Model::add_admin_modules($username, $module->id, 1);
						}
						//After adding the admin, add all dashboard items with value 1
						foreach($dashboard_items as $item) 
						{	//Set 1 by default
							$value =1;
							//For restrict user to town, set by default to 0
							if($item->id==3){$value=0;}
							Admin_Dashboard_Item_Model::add_dashboard_items($username, $item->id, $value);
						}
						
						
						
						
						
						
							$this->session->set('notice', array('message'=>$notice,'type'=>'success'));	
						}
				}	
				else 
				{
					$errors=$post->errors('errors');
					$notice="";
					foreach($errors as $error) 
					{
						$notice .= $error;
					}
					$this->session->set('notice', array('message'=>$notice,'type'=>'error'));
				}
		
			}
			$view = new View('add_admin');
			$view->parents = $parents;
			$view->agencies = $agencies;
			$view->towns = $towns;
			$view->groups = $groups;
			$this->template->content = $view;
		} else 
		{
			header("WWW-Authenticate: Basic realm=\"Private Area\"");
			header("HTTP/1.0 401 Unauthorized");
			print "Sorry - you need valid credentials to be granted access!\n";
			exit;
		}
		}	
	}  */
		
		}