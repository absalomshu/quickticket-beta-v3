<?php defined('SYSPATH') or die('No direct script access');

class Fixed_Expense_Detail_Model extends ORM{

	public function add_fixed_expense($agency_id, $expense_name, $expense_value, $expense_is_percent, $expense_is_amount,$admin_username)
	{	
		$expense = ORM::FACTORY('fixed_expense_detail');
		$expense->AgencyID = $agency_id;
		$expense->Name = $expense_name;
		$expense->Value = $expense_value;
		$expense->CreatedBy = strtoupper($admin_username);
		$expense->IsPercentage = $expense_is_percent;
		$expense->IsFixedAmount = $expense_is_amount;
		$expense->save();
	}
	
	public function edit_fixed_expense($agency_id, $expense_name, $expense_value, $expense_is_percent, $expense_is_amount, $expense_id)
	{	
		$expense = ORM::FACTORY('fixed_expense_detail')->where('AgencyID',$agency_id)->where('id',$expense_id)->find();
		$expense->AgencyID = $agency_id;
		$expense->Name = $expense_name;
		$expense->Value = $expense_value;
		$expense->IsPercentage = $expense_is_percent;
		$expense->IsFixedAmount = $expense_is_amount;
		$expense->save();
	}
	
	public function get_all($agency_id)
	{
		$fixed_expenses=ORM::factory('fixed_expense_detail')->where('AgencyID',$agency_id)->find_all();
		return $fixed_expenses;	
	}
	
	public function get_all_fixed_amounts($agency_id)
	{
		$fixed_expenses=ORM::factory('fixed_expense_detail')->where('AgencyID',$agency_id)->where('IsFixedAmount','1')->find_all();
		return $fixed_expenses;	
	}
	
	public function get_all_fixed_percentage_expenses($agency_id)
	{
		$fixed_expenses=ORM::factory('fixed_expense_detail')->where('AgencyID',$agency_id)->where('IsPercentage','1')->find_all();
		return $fixed_expenses;	
	}
	
	public function get_expense($agency_id,$expense_id)
	{

		$expense=ORM::factory('fixed_expense_detail')->where('AgencyID',$agency_id)->where('id',$expense_id)->find();
		return $expense;
		
	}
	public function delete_expense($agency_id,$expense_id)
	{
		
		$expense=ORM::factory('fixed_expense_detail')->where('id',$expense_id)->find();
		$expense->delete();
		$expense->save();
		
		$notice="Expense deleted";
		return $notice;
		
	}
	

}
