<?php defined('SYSPATH') or die('No direct script access'); ?> 
	

 
	<div class="row-fluid ">
	  	<div class="span12 hero-unit prof-unit" >
		
		<ul class="nav nav-tabs" id="myTab">
					
					<li class="active"><a href="#collection"><i class="icon-align-justify"></i> <?=Kohana::lang('backend.collection')?></a></li>
					<li><a href="<?=url::site('cash/other_income')?>"><i class="icon-align-justify"></i> <?=Kohana::lang('backend.other_income')?></a></li>
					<li><a href="<?=url::site('cash/cash_to_bank')?>"><i class="icon-align-justify"></i> <?=Kohana::lang('backend.cash_to_bank')?></a></li>
					<li><a href="<?=url::site('cash/balances')?>"><i class="icon-align-justify"></i> <?=Kohana::lang('backend.balances')?></a></li>
		</ul>
		
		<div class="tab-content">
			 
			
			<div class="tab-pane active" id="collection">
				<div class="span8 offset2">
				
					<legend><?=Kohana::lang('backend.admin_collections')?></legend>						
						<form action="<?=url::site('cash/collection_by_admin/')?>" method="POST" >
					
						
						<div class="">
							<div class="span4 text-right"><?=Kohana::lang('backend.admin')?><span class="red"> *</span>: </div>
							<div class="span8">
								<select name="admin" id="inputType">
								
									<?php 
										foreach ($admins_excluding_current as $admin){?>
										<option value="<?=$admin->username?>"><?=$admin->username?></option>
									<?php }; ?>
								
							</select>
							
							</div>
						</div>
					
						<div class="">
							<div class="span4 text-right"><?=Kohana::lang('backend.date');?>:</div>
							
							<div class="input-append date span4">
								<input type="text" name = 'date' class="span8 datepicker"  value="<?=date('d-m-Y')?>" id="dp1" >
								<span class="add-on"><i class="icon-calendar"></i></span>
							</div>	
							
							
							<div class="clear"></div>
						<div class="form-actions">
							<div class="span4"></div>
							<div class="span8">
								<button class="btn  btn-info" type="submit" ><i class="icon-search icon-white"></i> <?=Kohana::lang('backend.view');?></button><br/>
							</div>
						</div>
						</div>
					</form>	
				</div>	
					
			</div>
			
			
					
		</div>
		
			
		
			  
		</div>
		
		</div>
	  <div class="clear"></div>

<script type="text/javascript">
	$(document).ready(function(){
			$('#timepicker').timepicker({
				minuteStep:30,
				//defaultTime: '8:00 PM'
			});
			
			$('.input-append.date').datepicker({
				format: "dd-mm-yyyy",
				todayBtn: "linked",
				autoclose: true,
				todayHighlight: true
			});
			
			
				
			
	})
	</script>