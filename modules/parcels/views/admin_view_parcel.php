<?php defined('SYSPATH') or die('No direct script access'); ?> 

		<?php
		//set a variable depending on whether the parcel has been delivered or not
		//also check if the current admin is the sender. if so, disable the form , as he's not the recieving agency, hence shouldn't be allowed to fill
		//if it has, disable all input fields through javascript
		
		
		if($parcel->state == "delivered" || $parcel->from ==$this->admin->agency_id){
			$is_delivered = true;
		}else{ $is_delivered = false;}
		//var_dump($parcel->state);var_dump($parcel->from );var_dump($this->admin->agency_id);exit;
		?>
<div class="container">
<div class="">	  	
	<div class="row-fluid">	
		
		<div class="span12 hero-unit prof-unit">
		
		<ul class="nav nav-tabs">
					<li > <a href="<?=url::site('parcels/all/sent')?>"><i class="icon-arrow-left"></i> <?=Kohana::lang('backend.back_parcels')?></a></li>
		</ul>
		
		<div class="row-fluid" >	
		
			<div class="span6 hero-unit prof-unit print-area">
				
				
				<form action="<?=url::site('parcels/print_ticket')?>" method="POST">	
				<div class="heading"><?=Kohana::lang('backend.parcel_details')?>:
					<?php
					//if it's incoming, show the global parcel id. else compose it
					if(isset($parcel->global_parcel_id)){ echo "PC".$parcel->global_parcel_id;}
					else{ echo "PC".$this->agency_id.$parcel->id;}
					
					if(!isset($is_incoming)){
					?>
					<span class="view"><button type="submit" class="" name="print_parcel_ticket" value=" " title="Print parcel ticket"><i class="icon-print"></i></button>	
					<input class="span4" type="hidden" name="parcel_id" value="<?=$parcel->id?>" >
					
					<?php }?>
				</form>
				</div>
				
				<div class="span12">
								<div class="span4 text-right"><?=Kohana::lang('backend.status')?>: </div>
								<div class="span8 tiny-text"><?=get::agency_name($parcel->state);?></div>
				</div>
				<div class="span12">
								<div class="span4 text-right"><?=Kohana::lang('backend.sent_from')?>: </div>
								<div class="span8"><?=get::agency_name($parcel->from);?></div>
				</div>
				<div class=" span12">
								<div class="span4 text-right"><?=Kohana::lang('backend.sent_on')?>: </div>
								<div class="span8"><?=date("d-m-Y",strtotime($parcel->sent_date))?></div>
				</div>
				<div class="span12">
								<div class="span4 text-right"><?=Kohana::lang('backend.sent_to')?>: </div>
								<div class="span8"><?=get::agency_name($parcel->to);?></div>
				</div>
				<!--Only show this div for sent because incoming parcels don't have a type set yet, to avoid error.-->
				<?php if(!isset($is_incoming)){?>
				<div class="span12">
								<div class="span4 text-right">Type: </div>
								<div class="span8"><?=ucfirst($parcel->type);?></div>
				</div> 
				<?php } ?>
				<div class="span12">
								<div class="span4 text-right"><?=Kohana::lang('backend.receiver')?>: </div>
								<div class="span8"><?=$parcel->receiver_name;?></div>
				</div>
				
				<div class="span12">
								<div class="span4 text-right"><?=Kohana::lang('backend.receiver_phone')?>: </div>
								<div class="span8"><?=$parcel->receiver_phone;?></div>
				</div>
				<div class="span12">
								<div class="span4 text-right">Description: </div>
								<div class="span8"><?=$parcel->description;?></div>
				</div>
				<div class="span12">
								<div class="span4 text-right">Price: </div>
								<div class="span8"><?=number_format($parcel->price);?> frs</div>
				</div>
				
				<div class="clear rule"><hr/></div>
				<div class="span12">
								<div class="span4 text-right">Created by: </div>
								<div class="span8"><i><?=$parcel->CreatedBy;?></i></div>
				</div>
				
			
			</div>
		
		
			
			<div class="span6">
			
			<div class="heading"><?php if(!$is_delivered){ echo "Who is collecting this parcel?";}else{echo Kohana::lang('backend.coll_by');}?></div>
				<?php 
					$collected_by = json_decode($parcel->collected_by);
					//var_dump($collected_by);exit;
				?>
				<form action="<?=url::site('parcels/deliver/'.$parcel->id)?>" method="POST" >
								<div class="clear"></div>

					<div class="simple-form span12">
								<div class="sf_label"><?=Kohana::lang('backend.name')?>: </div>
								<div class="sf_input"><input type="text"  name="collector_name" <?php if(1){echo "value='". @$collected_by->name."' disabled";}?>></div>
					</div>
					<div class="simple-form span12">
								<div class="sf_label"><?=Kohana::lang('backend.idc')?>: </div>
								<input type="text"  name="collector_idc" <?php if(1){echo "value='". @$collected_by->idc. "' disabled";}?>>
					</div>				
					<div class="simple-form span12">
								<div class="sf_label"><?=Kohana::lang('backend.phone')?>: </div>
								<div class="sf_input"><input type="text"  name="collector_phone" <?php if(1){echo "value='".@$collected_by->phone. "' disabled";}?>></div>
					</div>
					
					<?php
					//only show deliver button if it's not delivered
						if(!$is_delivered){
					?>	
							
					<?php
						}else{
					?>
					
						<div class="simple-form span12">
								<div class="sf_label"><?=Kohana::lang('backend.collected_on')?>: </div>
								<div class="sf_input"><input type="text"  name="collector_phone" <?php if($is_delivered){echo "value='". @$parcel->collected_on."' disabled";}?>></div>
						</div>

					<?php } ?>
					
				</form>
			</div>	
			
				
				
		</div>
	
		
		</div>
	
    </div> 
</div></div>
	
	<script type="text/javascript">
	
	$(document).ready(function(){
		var is_delivered = '<?=$is_delivered?>';
		$('.sf_input').prop('disabled',true);
	
	})
	
	</script>