<?php defined('SYSPATH') or die('No direct script access'); ?>
	
		<div class="row-fluid  span12 hero-unit prof-unit reload" style="margin:5px 0 0px 0;">	
		
			
			
		
			<div class="no-print">
			
			<ul class="nav nav-tabs">
					<li > <a href="<?=url::site('reports')?>"><i class="icon-arrow-left"></i> <?=Kohana::lang('backend.back_reports')?></a></li>
			</ul>
			</div>
			<div class=" print-area" >
				<legend>
				<div class="print-head">
						<?php $admin=Authlite::instance()->get_admin();?>
						<?=strtoupper(get::_parent($admin->agency_id)->name);?><br/>
						<?=get::agency_name($admin->agency_id);?>
					</div>
				
					Report: Route history	<br/>
					Start date: <?=date("d-m-Y",strtotime($start_date))?> &nbsp;&nbsp;&nbsp;&nbsp; 
					End date: <?=date("d-m-Y",strtotime($end_date))?>&nbsp;&nbsp;&nbsp;&nbsp;
					Route: <?=get::town($town_from)." -> ".get::town($town_to);?><b></b><button class="view" onClick="window.print()"><i class="icon-print"></i> <?=Kohana::lang('backend.print')?></button>
				</legend>
				
				<?php
				if($search_results->count() !=0){?>
				<table class="table table-condensed table-striped ">
					<tr><th><?=Kohana::lang('backend.schedules')?></th></tr>
					<tr>
						<th class="span2">Date</th>
						<th>Bus number</th>
						<th class="text-right" >Expenses</th>
						<th class="text-right">Income</th>
					</tr>
					<?php 
					
					$overall_income =0;
					$overall_expenditure =0;
					//$total_schedule_expenses=0;
					foreach ($search_results as $schedule)
					{
						$total_schedule_expenses=0;
						$schedule_expenses = get::expenses_for_schedule($this->agency_id,$schedule->id);
						foreach($schedule_expenses as $expenses){
							$total_schedule_expenses+=$expenses->amount;
							$overall_expenditure+= $total_schedule_expenses;
						}
						?>
					<tr>		
						<td><?=date("d-m-Y",strtotime($schedule->departure_date))?></td>
						<td><?=$schedule->bus_number;?></td>
						<td class="span2 text-right"><?=number_format($total_schedule_expenses);?></td>
						<td class="span2 text-right"><?=number_format($schedule->total_amount);?></td>
					</tr>
					<?php $overall_income += $schedule->total_amount; }?>
					<tr>		
						<td></td>
						<th class="text-right">Total</th>
						<th class="span2 text-right"><?=number_format($overall_expenditure);?></th>
						<th class="span2 text-right"><?=number_format($overall_income);?></th>
					</tr>
				</table>	
				
				
			
				
				<!--NET SUMMARY TABLE-->
				<table class="table table-condensed table-striped ">
					<tr>
						<th class="span2">GRAND TOTAL</th>
						<td>&nbsp;</td>
						<th class="text-right" >EXPENSES</th>
						<th class="text-right">INCOME</th>
					</tr>
					
					<tr>		
						<td></td>
						<th></th>
						<th class="span2 text-right"><?=number_format($overall_expenditure)?></th>
						<th class="span2 text-right"><?=number_format($overall_income)?></th>
					</tr>
					<tr>		
						<td></td>
						<th></th>
						<th class="span2 text-right">NET</th>
						<th class="span2 text-right"><?=number_format($overall_income - $overall_expenditure)?> </th>
					</tr>
				</table>	
				
			
				<div>Printed by: <?=STRTOUPPER($this->admin->username);?></div> 
				<div>Printed on: <?=date('d-m-Y h:i:s A')?></div>
				<!--END OF NET SUMMARY-->
				<?php } else echo "No results found";?>	
			</div>

			
		
		
		
		
		
		
		</div>

