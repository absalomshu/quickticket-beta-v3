<?php defined('SYSPATH') or die('No direct script access'); ?> 
	

 
	<div class="row-fluid " style="margin:5px 0 0px 0px;">
	  	
		<div class="span12 hero-unit prof-unit" >
		
		<ul class="nav nav-tabs" id="myTab">
					<li><a href="#add-expense"><i class="icon-plus"></i> <?=Kohana::lang('backend.add_expense')?></a></li>
					<li class="active"><a href="#expenses"><i class="icon-list"></i> <?=Kohana::lang('backend.exp_list')?></a></li>
					<li ><a href="#check-expenses"><i class="icon-search"></i> <?=Kohana::lang('backend.check_exp')?></a></li>
		</ul>
		
		<div class="tab-content">
			<div class="tab-pane" id="add-expense">
				<form action="<?=url::site('expenses/register_expense')?>" method="POST" name="theform" class="span8 offset2">
				<legend><?=Kohana::lang('backend.register_expense')?></legend>
						<!--<div class="" >
							<div class="span4 text-right">Date: </div>
								<div class="input-append date span8">
									<input type="text" name = 'date_incurred' class="span5 datepicker"  value="<?=date('d-m-Y')?>" id="dp1" required>
									<span class="add-on"><i class="icon-calendar"></i></span>
								</div> 
						</div> -->
						
						<!--<div class="" >
							<div class="span4"><?=Kohana::lang('backend.bus_no')?>: </div>
							<div class="span8"><input type="text"  name="bus_number"></div>
						</div>-->
						<div class="">
							<div class="span4 text-right"><?=Kohana::lang('backend.bus_no')?>: </div>
							<div class="span8">
							<select name="bus_number" id="inputType">
									<option value=""> None</option>
									<?php foreach($all_buses as $bus){?>
										<option value="<?=$bus->bus_number?>"> <?=$bus->bus_number?></option>
										
									<?php }?>
							</select>
							</div>
						</div>
						
						<div class="">
							<div class="span4 text-right"><?=Kohana::lang('backend.authorised_by')?>: </div>
							<div class="span8"><input type="text"  name="authorised_by" ></div>
						</div>
						
						<div class="">
							<div class="span4 text-right"><?=Kohana::lang('backend.amount')?>: <span class="red"> *</span></div>
							<div class="span8 controls input-append"><input class="span5 IsAmount" type="text"  name="amount" required><span class="add-on"> FCFA</span></div>
						</div>
						
						<div class="">
							<div class="span4 text-right"><?=Kohana::lang('backend.purpose')?>: <span class="red"> *</span></div>
							<div class="span8"><textarea placeholder="<?=Kohana::lang('backend.spent_on')?>"  name="purpose" required ></textarea></div>
						</div>
						<div class="">
							<div class="span4 text-right"><?=Kohana::lang('backend.category')?>: </div>
							<div class="span8">
							<select name="category_id" id="inputType">
									<?php foreach($all_expense_categories as $category){?>
										<option value="<?=$category->id?>"> <?=$category->name?></option>
										
									<?php }?>
							</select>
							</div>
						</div>
								
						
					
						<div class="clear"></div>
						
						<div class="form-actions">
							<div class="span4"></div>
							<div class="span8"><button class="btn btn-info " type="submit"><?=Kohana::lang('backend.register')?></button></div>
						</div>
						
									
						
				</form>
					<!--end the first tab-->
			</div>  
			 
			<div class="tab-pane active" id="expenses">
				<table class="table table-striped table-hover table-condensed">
					<tr>
						<th>Date</th>
						
						<th><?=Kohana::lang('backend.purpose')?></th>
						<th><?=Kohana::lang('backend.category')?></th>
						<th class="text-right"><?=Kohana::lang('backend.amount')?></th>
						<th class="text-right"><?=Kohana::lang('backend.bus_no')?></th>
						<th >&nbsp;</th>
						<!--<th><?=Kohana::lang('backend.reg_at')?></th>-->
						<th><?=Kohana::lang('backend.authorised_by')?></th>
						<th><?=Kohana::lang('backend.created_by')?></th>
						<th>Actions</th>
					</tr>
					<?php foreach ($all_expenses as $expense):
					//var_dump($expense);exit;
					?>
						<tr>	
							<td><?=date("d-m-Y",strtotime($expense->date_incurred))?></td>
							
							
							
							<!--Trim if the description is too long. -->
								<td><a href="<?=url::site('expenses/details/'.$expense->id)?>"> 
									<?php if (strlen($expense->purpose) > 25) { echo substr($expense->purpose,0,35)."...";} else { echo $expense->purpose;}?>
								</a></td>
							<td><?=$expense->category_name?> </td>
							<td class="text-right"><?=number_format($expense->amount)?> </td>
							
							<td class="text-right"><?=$expense->bus_number?></td>
							<th >&nbsp;</th>
							<!--<td><?=get::agency_name($expense->agency_id)?></td>-->
							<td><?=$expense->authorised_by?></td>
							<td><?=$expense->CreatedBy?></td>
							
							<td >
								<?php
									//Admin can only delete what he posted
									//and can only do it on the same day
									
									
									if($expense->CreatedBy == strtoupper($this->admin->username) AND date("d-m-Y", strtotime($expense->CreatedOn)) == date("d-m-Y"))
									{
									
										?>
										<div class="btn-group ">
											<a class="btn dropdown-toggle btn-mini" data-toggle="dropdown" href="#" >
												-- <?=Kohana::lang('backend.select')?> --
												<span class="caret"></span>
											</a>
										  <ul class="dropdown-menu">
											<li><a href="<?=url::site('expenses/edit/'.$expense->id)?>" ><i class="icon-edit"></i> <?=Kohana::lang('backend.edit')?></a></li>
											<li><a href="<?=url::site('expenses/safe_delete_expense/'.$expense->id)?>" onClick="return confirm('<?=Kohana::lang('backend.confirm_delete')?>?');"><i class="icon-trash"></i> <?=Kohana::lang('backend.delete')?></a></li>
										  </ul>
										</div>
										
								<?php } ?>
							</td>
							
						</tr>
						<?php endforeach; ?>
				</table>	
			<div class="pagination"><?php echo @$this->pagination;?>	</div>	
			</div>
				
			<div class="tab-pane" id="check-expenses">
			
					
					<form action="<?=url::site('expenses/check_expenses')?>" method="POST" class="span8 offset2">
					<legend><?=Kohana::lang('backend.which_bus_exp')?>?</legend>					
					<div class="accordion" id="accordion2">
					
					  <div class="accordion-group">
						<div class="accordion-heading">
						  <a class="accordion-toggle" data-toggle="collapse" data-parent="#accordion2" href="#collapseOne">
							<?=Kohana::lang('backend.select_bus')?>:
						  </a>
						</div>
						<div id="collapseOne" class="accordion-body collapse in">
						  <div class="accordion-inner">
						  
							<div class="sf_input">
												<select name="existing_bus_number" id="inputType" class="span3" >
														<?php foreach ($all_buses as $bus):?>
														<option value="<?=$bus->bus_number?>" ><?=$bus->bus_number?></option>
														<?php endforeach;?>
													
												</select>
										<button type="submit" class="btn btn-success" name="existing_bus"><?=Kohana::lang('backend.search')?></button>
										
							</div>
						  
						  </div>
						</div>
					  </div>
					  <!--<div class="accordion-group">
						<div class="accordion-heading">
						  <a class="accordion-toggle" data-toggle="collapse" data-parent="#accordion2" href="#collapseTwo">
							<?=Kohana::lang('backend.enter_bus_search')?>:
						  </a>
						</div>
						<div id="collapseTwo" class="accordion-body collapse">
						  <div class="accordion-inner">
							
							<input type="text" class="span3 search-query" name="search_bus_number"/>
							<button type="submit" class="btn btn-success" name="search_bus"><?=Kohana::lang('backend.search')?></button>
							
							
							
							
						  </div>
						</div>
					  </div>-->
					  <div class="accordion-group">
						<div class="accordion-heading">
						  <a class="accordion-toggle" data-toggle="collapse" data-parent="#accordion2" href="#collapseThree">
							<?=Kohana::lang('backend.search_by_date')?>:
						  </a>
						</div>
						<div id="collapseThree" class="accordion-body collapse">
						  <div class="accordion-inner">
							
										
											
											<div class="input-append date left">
												<input type="text" name = 'date' class="span7 datepicker"  value="<?=date('d-m-Y')?>" id="dp1">
												<span class="add-on"><i class="icon-calendar"></i></span>
											</div><button class="btn  btn-info" type="submit" name="by_date"  ><i class="icon-search icon-white"></i> Check</button><br/>
									
						  </div>
						</div>
					  </div>
					</div>
					</form>	
					
					
			</div>
				
								
	  			
		</div>
		
			
		
			  
		</div>
		
		</div>
	  <div class="clear"></div>

<script type="text/javascript">
	$(document).ready(function(){
			$('#timepicker').timepicker({
				minuteStep:30,
				//defaultTime: '8:00 PM'
			});
			
		
			
			
				$(function () {
				$('#myTab a').click(function (e) {
					  e.preventDefault();
					  $(this).tab('show');  
			})
			  })
			  
		
			  
			
	})
	</script>
