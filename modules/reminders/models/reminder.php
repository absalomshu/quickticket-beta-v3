<?php defined('SYSPATH') or die('No direct script access');

class Reminder_Model extends ORM{

	public function add_reminder($agency_id, $bus_number, $name, $description, $days_to, $start_date, $due_date, $interval_value, $interval_type, $is_personal, $admin, $amount)
	{	
		$reminder = ORM::FACTORY('reminder');
		$reminder->AgencyID = $agency_id;
		$reminder->Name = $name;
		$reminder->BusNumber = $bus_number;
		$reminder->Amount = $amount;
		$reminder->Description = $description;
		$reminder->StartDate = $start_date;
		$reminder->DueDate = $due_date;
		$reminder->DaysTo = $days_to;
		$reminder->IsPersonal = $is_personal;
		$reminder->IntervalValue = $interval_value;
		$reminder->IntervalType = $interval_type;
		$reminder->CreatedBy = $admin;
		$reminder->save();
		
		//If there's no start date, it's a one time reminder
		if(empty($start_date))
		{	
			Reminder_Schedule_Model::add_schedule($agency_id, $reminder->id, $due_date);
		}else
		{		
			$year=substr($start_date,0,4);
			$month=substr($start_date,5,2);
			$day=substr($start_date,8,2);

			//If the interval is in months
			//Set reminder for a maximum of 24 entries
				if($interval_type == "M")
				{
					//Once interval value enters the loop, it'll be changing. So first keep the value in an original variable.
					$original_interval_value = $interval_value;
					for($i=0; $i<24; $i++)
					{	
						$due_date  = date("Y-m-d", mktime(0, 0, 0, $month+$interval_value , $day, $year));
						
						//add up the interval value. So if it adds 2 months the first time, interval value should be 4 the second time
						$interval_value = $original_interval_value + $interval_value;
						
						Reminder_Schedule_Model::add_schedule($agency_id, $reminder->id, $due_date);
					}
				}
			//If the interval is in days	
				elseif($interval_type == "D")
				{ 
					//Once interval value enters the loop, it'll be changing. So first keep the value in an original variable.
					$original_interval_value = $interval_value;
					for($i=0; $i<24; $i++)
					{	
						$due_date  = date("Y-m-d", mktime(0, 0, 0, $month , $day+$interval_value, $year));
						
						//add up the interval value. So if it adds 2 months the first time, interval value should be 4 the second time
						$interval_value = $original_interval_value + $interval_value;
						
						Reminder_Schedule_Model::add_schedule($agency_id, $reminder->id, $due_date);
						//var_dump($due_date);
					}
				}
		}
		return "Reminder added.";
	}
	
	public function edit_reminder($reminder_id, $agency_id, $bus_number, $name, $description, $days_to, $due_date, $is_personal)
	{	
		$reminder = ORM::FACTORY('reminder')->where('id',$reminder_id)->where('AgencyID',$agency_id)->find();
		$reminder->AgencyID = $agency_id;
		$reminder->Name = $name;
		$reminder->BusNumber = $bus_number;
		
		$reminder->Description = $description;
		$reminder->DueDate = $due_date;
		$reminder->DaysTo = $days_to;
		$reminder->IsPersonal = $is_personal;
		$reminder->save();
	}
	
	public function get_all($agency_id)
	{
		$reminders=ORM::factory('reminder')->where('AgencyID',$agency_id)->find_all();
		return $reminders;	
	}
	
	public function get_reminders_for_bus($agency_id, $bus_number)
	{
		$reminders=ORM::factory('reminder')->where('AgencyID',$agency_id)->where('BusNumber',$bus_number)->find_all();
		return $reminders;	
	}
	
	public function get_reminder($agency_id,$reminder_id)
	{
		$reminder=ORM::factory('reminder')->where('AgencyID',$agency_id)->where('id',$reminder_id)->find();
		return $reminder;	
	}
	

	public function delete_reminder($agency_id,$reminder_id)
	{
		
		$reminder=ORM::factory('reminder')->where('AgencyID',$agency_id)->where('id',$reminder_id)->find();
		$reminder->delete();
		$reminder->save();
		
		$notice="Reminder deleted";
		return $notice;
		
	}
	

}
