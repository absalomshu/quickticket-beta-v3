
				
						<?php 
						
						//If there are occupants or reservers, show the table
						if (!empty($seats_and_occupants) OR !empty($seats_and_reservers))
						{?>
							
						
	
							
								<div class="control-group" >
									<label class="control-label lab heading" for="inputType"  >
									BORDEREAU
									
									</label>
									
									
									<?php 
									//combine reserved and taken
									//COMBINING INEFFICIENT AS IT IS A 2D AND A 3D
									//$all_seats = $seats_and_reservers + $seats_and_occupants;
									//ksort($all_seats);
									//foreach($seats_and_reservers as $key=>$subkey){print_r($subkey['name']);echo"<br/>";}?>
										
								</div>
								<div class="rule"><hr/></div>
							
						
						<form action="<?=url::site('admin/complete_schedule/'.$schedule->id)?>" method="POST">
						<?php no_csrf::set(2); ?>
						<table class="table table-striped table-hover table-condensed">
										<tr>
												<th><?=Kohana::lang('backend.seat')?></th>
												<th>Name</th>
												<th>IDC No.</th>
												<th>Phone</th>
												<th><?=Kohana::lang('backend.state')?></th>
												<th>Actions</th>
										</tr>
										<?php 
										//sort the array in ascending order
										ksort($seats_and_occupants);
										foreach ($seats_and_occupants as $key=>$value)
										{ ?>
												<tr>
												
												<?php 
												//Go to ticket details table and check if missed or not.
												$ticket = ORM::FACTORY('ticket_detail')->where('AgencyID',$this->agency_id)->where('SeatNumber',$key)->where('ScheduleID',$schedule->id)->find();?>
												
													<td>
														<?=$key?>
															<?php if(isset($value['free_ticket']) && $value['free_ticket']==1){?> <span class="badge badge-warning" title="Free-ticket">F</span> <?php }?>
															<?php if($ticket->MissedStatus){?> <i class="icon-exclamation-sign" title="Absent"></i> <?php }?>
														</td>
													<td><?=$value['name']?></td>
													<td><?=$value['idc']?></td>
													<td><?=$value['phone']?></td>
													<td><?php if(isset($value['sd_price'])){echo "*SD-". get::town($value['sd_town']);}?></td>
													<?php
													$serial1 = serialize($seats_and_occupants);
													?>
													<input type="hidden" value="<?php echo htmlentities($serial1)?>" name="arr1"/>										
													<td>
														<!--<button type="submit" class="" name="print_single_ticket" value="<?=$key?>" title="Print this ticket"><i class="icon-print"></i></button>
														<button type="submit" class="trash" name="empty_seat" value="<?=$key?>" onClick="return confirm('<?=Kohana::lang('backend.confirm_emptyseat')?>?');" title="Empty this seat"><i class="icon-trash"></i></button>
														-->
														
														<div class="btn-group ">
															  <a class="btn dropdown-toggle btn-mini" data-toggle="dropdown" href="#" >
																		--Select--
															  <span class="caret"></span>
															  </a>
															  <ul class="dropdown-menu">
																<!--<li><button type="submit" class="btn btn-link" name="print_single_ticket" value="<?=$key?>" title="Print this ticket"><i class="icon-print"></i> Print ticket</button></li>
																<li><button type="button" class="btn btn-link" name="print_single_ticket" value="<?=$key?>" id="print_single_ticket"  title="Print this ticket"><i class="icon-print"></i> origingal Print ticket</button></li>-->
																
																<!--Print ticket via js so page doesn't reload-->
																<li><button type="button" class="btn btn-link" onclick="print_single_ticket_js(<?=$key?>,'<?=$schedule->bus_number?>','<?=$schedule->departure_date?>')"><i class="icon-print"></i> Print ticket</button></li>
																<input type="hidden" value="<?=$schedule->departure_date?>" id="single_ticket_departure_date">
																<input type="hidden" value="<?=$schedule->bus_number?>" id="single_ticket_bus">
																
																
																<?php 
																//if marked as abesent, show mark as present link. else do contrary
																if($ticket->MissedStatus){?>
																	<li><button type="submit" class="btn btn-link" name="mark_present" value="<?=$key?>" onClick="return confirm('<?=Kohana::lang('backend.mark_present')?>?');" title="Mark as present"><i class="icon-ok-sign"></i> Mark present</button></li>
																<?php } else{ ?>
																	<li><button type="submit" class="btn btn-link" name="mark_absent" value="<?=$key?>" onClick="return confirm('<?=Kohana::lang('backend.mark_absent')?>?');" title="Mark as absent"><i class="icon-exclamation-sign"></i> Mark absent</button></li>
																<?php } ?>
																<li><button type="submit" class="btn btn-link" name="empty_seat" value="<?=$key?>" onClick="return confirm('<?=Kohana::lang('backend.confirm_emptyseat')?>?');" title="Empty this seat"><i class="icon-trash"></i> Empty seat</button></li>	
															 </ul>
														</div>
													</td>
												</tr>
											
										<?php } ;?><?php 
										//sort the array in ascending order
										ksort($seats_and_reservers); 
										//serialize so it can be posted
										$serial = serialize($seats_and_reservers); ?>
													
										<input type="hidden" value="<?php echo htmlentities($serial)?>" name="arr"/>
										
										<?php
										foreach ($seats_and_reservers as $r_key=>$value)
										{ ?>
												<tr>
													<td><?=$r_key?></td>
													<td><?=$value['name']?></td>
													<td><?=$value['idc']?></td>
													<td><?=$value['phone']?></td>
													<td><?=Kohana::lang('backend.reserved')?> </td> 
													<td>
														<button type="submit" id="accept"name="res_to_occ" value="<?=$r_key?>" title="Confirm"></button> | 
														<button type="submit" id="reject" name="reject_reserve" value="<?=$r_key?>" onClick="return confirm('<?=Kohana::lang('backend.refuse_reserve')?>?');" title="Delete"></button>
													</td>
												</tr>
										<?php } ;?>
									</table>
								</form>
					 <?php }?>	
	