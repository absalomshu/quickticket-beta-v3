<?php defined('SYSPATH') or die('No direct script access'); ?>
	
    <div class="container">
		<div class="notice">
		<?php
		//determine what type of notice to display if at all
		$notice = $this->session->get_once('notice');
		if(!empty($notice))
		{ 
					if($notice['type'] == 'success'){?><div class="alert general success"><a class="close" data-dismiss="alert" href="#">&times;</a><?php echo $notice['message']; }?></div>
				<?	if($notice['type'] == 'error'){?><div class="alert general error"><a class="close" data-dismiss="alert" href="#">&times;</a><?php echo $notice['message']; }?></div><?
		} ?>
		</div>
		<div class="row-fluid marketing" style="margin:5px 0 0px 0;">	
			<div class="span5 hero-unit prof-unit" style="float:right;">
				<div class="heading"><?=Kohana::lang('backend.parcel_deliveries')?></div>
				<div class="rule"><hr/></div>
				<?php 
					//instantiate variable to total the prices in the loop below
					$parcel_total = 0;
					if (count($parcels)==0) 
					{
					?>
					<?=Kohana::lang('backend.no_parcel_deliveries')?>
					<?php
					}else
					{ 
						?>
						<ol>
						<?php 
						foreach ($parcels as $parcel):?>
						<li class="recent"><?=Kohana::lang('backend.for')?> <b><?=$parcel->receiver_name?></b> <?=Kohana::lang('backend.worth')?> <b><?=$parcel->price?>FCFA</b></li>
						<?php $parcel_total += $parcel->price;?>
						<?php endforeach; 
					}?>
						</ol>					
			</div>
			
			<?php if(count($current_schedules) != 0)
			{
				?>
				<div class="span7 hero-unit prof-unit reload" style="margin-left:0px;">
					<div class="heading"><?=Kohana::lang('backend.pending_for')?><?=date("d-m-Y",strtotime($date))?></div>
					<div class="rule"><hr/></div>
						<table class="table">
							<?php foreach ($current_schedules as $c_schedule):?>
							<tr><td><i class="icon-time"></i> <?=$c_schedule->bus_number?> : <b><?=get::town($c_schedule->from)?></b> to <b><?=get::town($c_schedule->to)?></b> : <b><?=date("g:i A", strtotime($c_schedule->departure_time))?></b><i> <span style="font-size:11px;">  (Loading now...) </span> </i></td></tr>
							<!--<input type="hidden"  class="schedule_id" value="<?=$c_schedule->id?>"></input>-->
							<?php endforeach; ?>
						</table>	
				</div>
			<?}?>
			<div class="span7 hero-unit prof-unit reload" style="margin-left:0px;">
				<div class="heading"><?=Kohana::lang('backend.checkedout_for')?> <?=date("d-m-Y",strtotime($date))?></div>
				<div class="rule"><hr/></div>
				
				<table class="table">
					<?php foreach ($departed_schedules as $d_schedule):?>
					<tr><td><i class="icon-ok-sign"></i> <a href="<?=url::site('control/past_schedule').'/'.$d_schedule->id?>"><?=$d_schedule->bus_number?></a> : <b><?=get::town($d_schedule->from)?></b> -> <b><?=get::town($d_schedule->to)?></b> : <b><?=date("g:i A", strtotime($d_schedule->departure_time))?></b><a class="view" href="<?=url::site('control/past_schedule').'/'.$d_schedule->id?>">View</a></td></tr>
					<?php endforeach; ?>
				</table>					
			</div>
			
			<div class="span7 hero-unit prof-unit reload" style="margin-left:0px;">
				<div class="heading"><?=Kohana::lang('backend.summary_for')?> <?=date("d-m-Y",strtotime($date))?></div>
				<div class="rule"><hr/></div>
				<i>NB: <?=Kohana::lang('backend.from_departed')?>!</i><br/>
				<table class="table table-bordered">
					<tr>
						<td><?=Kohana::lang('backend.number_of')?> 30 <?=Kohana::lang('backend.seater')?></td>
						<td><?=$_30seats?></td>
					</tr>
					<tr>
						<td><?=Kohana::lang('backend.number_of')?> 35 <?=Kohana::lang('backend.seater')?></td>
						<td><?=$_35seats?></td>
					</tr><tr>
						<td><?=Kohana::lang('backend.number_of')?> 39 <?=Kohana::lang('backend.seater')?></td>
						<td><?=$_39seats?></td>
					</tr>
					<tr>		
						<td><?=Kohana::lang('backend.number_of')?> 55 <?=Kohana::lang('backend.seater')?></td>
						<td><?=$_55seats?></td>
					</tr>
					<tr>		
						<td><?=Kohana::lang('backend.number_of')?> 70 <?=Kohana::lang('backend.seater')?></td>
						<td><?=$_70seats?></td>
					</tr>
					<tr>		
						<th><?=Kohana::lang('backend.total_ticket_sales')?></th>
						<th><?=$total?> FCFA</th>
					</tr><tr>		
						<th><?=Kohana::lang('backend.total_from_parcels')?></th>
						<th><?=$parcel_total?> FCFA</th>
					</tr>
					<tr>		
						<td><h3>GRAND TOTAL</h3></th>
						<td><h3><?=$total + $parcel_total?> FCFA</h3></td>
					</tr>
				</table>
			</div>
		</div>
    </div> 
