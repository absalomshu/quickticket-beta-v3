<?php defined('SYSPATH') or die('No direct script access'); ?> 


    <div class="container">		
	  <div class="notice">
	  <?php
		//determine what type of notice to display if at all
		$notice = $this->session->get_once('notice');
			if(!empty($notice)){ 
				if($notice['type'] == 'success'){?><div class="alert general success"><a class="close" data-dismiss="alert" href="#">&times;</a><?php echo $notice['message']; }?></div>
			<?	if($notice['type'] == 'error'){?><div class="alert general error"><a class="close" data-dismiss="alert" href="#">&times;</a><?php echo $notice['message']; }?></div>
		<?}?>
	  </div>
	<div class="row-fluid marketing" style="margin:5px 0 0px 0;">	
	<br/><br/><br/><br/><br/>
		<div class="span8 offset2" >
			<div class="span12 hero-unit prof-unit"  style="margin:5px 0 0px 0;">			
				<legend> Enter the email address you used to signup and we will send you password reset instructions </legend>
				<form method="POST" action=<?=url::site('home/forgot_password')?>>
					<input type="email" name="email">
					<button type="submit" class="btn btn-success">Send reset instructions</button>
				
				
				</form>
			</div>
		</div>
		

		
	
		
    </div> 
