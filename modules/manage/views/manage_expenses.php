<?php defined('SYSPATH') or die('No direct script access'); ?> 
	

 
	<div class="row-fluid " style="margin:5px 0 0px 0px;">
	  	
		<div class="span12 hero-unit prof-unit" >
		
		<ul class="nav nav-tabs" id="myTab">
					<!--<li><a href="#add-expense"><i class="icon-plus"></i> <?=Kohana::lang('backend.add_expense')?></a></li>-->
					<li class="active"><a href="#expenses"><i class="icon-list"></i> <?=Kohana::lang('backend.exp_list')?></a></li>
					<!--<li><a href="#check-expenses"><i class="icon-search"></i> <?=Kohana::lang('backend.check_exp')?></a></li>-->
		</ul>
		
		<div class="tab-content">
			
			 
			<div class="tab-pane active" id="expenses">
				<table class="table table-striped table-hover table-condensed">
					<tr>
						<th>Date</th>
						<th><?=Kohana::lang('backend.branch')?></th>
						<th><?=Kohana::lang('backend.purpose')?></th>
						<th><?=Kohana::lang('backend.amount')?></th>
						<th><?=Kohana::lang('backend.bus_no')?></th>
						<!--<th><?=Kohana::lang('backend.reg_at')?></th>-->
						<th><?=Kohana::lang('backend.authorised_by')?></th>
						<th><?=Kohana::lang('backend.admin')?></th>
					</tr>
					<?php foreach ($all_expenses as $expense):?>
						<tr>	
							<td><?=date("d-m-Y",strtotime($expense->date_incurred))?></td>
							<td><?=get::agency_name($expense->agency_id)?></td>
							
							
							<!--Trim if the description is too long. -->
								<td><a href="<?=url::site('manage/expense_details/'.$expense->id)?>"> 
									<?php if (strlen($expense->purpose) > 25) { echo substr($expense->purpose,0,35)."...";} else { echo $expense->purpose;}?>
								</a></td>
							<td class="text-right"><?=number_format($expense->amount)?> </td>
							
							<td><?=$expense->bus_number?></td>
							<!--<td><?=get::agency_name($expense->agency_id)?></td>-->
							<td><?=$expense->authorised_by?></td>
							<td><?=$expense->CreatedBy?></td>
						</tr>
						<?php endforeach; ?>
				</table>	
			<div class="pagination"><?php echo @$this->pagination;?>	</div>	
			</div>
				
			<div class="tab-pane" id="check-expenses">
			
					
					<form action="<?=url::site('expenses/check_expenses')?>" method="POST" class="span8 offset2">
					<legend><?=Kohana::lang('backend.which_bus_exp')?>?</legend>					
					<div class="accordion" id="accordion2">
					
					  <div class="accordion-group">
						<div class="accordion-heading">
						  <a class="accordion-toggle" data-toggle="collapse" data-parent="#accordion2" href="#collapseOne">
							<?=Kohana::lang('backend.select_bus')?>:
						  </a>
						</div>
						<div id="collapseOne" class="accordion-body collapse in">
						  <div class="accordion-inner">
						  
							<div class="sf_input">
												<select name="existing_bus_number" id="inputType" class="span3" >
														<?php //foreach ($all_buses as $bus):?>
														<option value="<?php //$bus->bus_number?>" ><?php //$bus->bus_number?></option>
														<?php //endforeach;?>
													
												</select>
										<button type="submit" class="btn btn-success" name="existing_bus"><?=Kohana::lang('backend.search')?></button>
										
							</div>
						  
						  </div>
						</div>
					  </div>
					  <!--<div class="accordion-group">
						<div class="accordion-heading">
						  <a class="accordion-toggle" data-toggle="collapse" data-parent="#accordion2" href="#collapseTwo">
							<?=Kohana::lang('backend.enter_bus_search')?>:
						  </a>
						</div>
						<div id="collapseTwo" class="accordion-body collapse">
						  <div class="accordion-inner">
							
							<input type="text" class="span3 search-query" name="search_bus_number"/>
							<button type="submit" class="btn btn-success" name="search_bus"><?=Kohana::lang('backend.search')?></button>
							
							
							
							
						  </div>
						</div>
					  </div>-->
					  <div class="accordion-group">
						<div class="accordion-heading">
						  <a class="accordion-toggle" data-toggle="collapse" data-parent="#accordion2" href="#collapseThree">
							<?=Kohana::lang('backend.search_by_date')?>:
						  </a>
						</div>
						<div id="collapseThree" class="accordion-body collapse">
						  <div class="accordion-inner">
							
										
											
											<div class="input-append date left">
												<input type="text" name = 'date' class="span7 datepicker"  value="<?=date('d-m-Y')?>" id="dp1">
												<span class="add-on"><i class="icon-calendar"></i></span>
											</div><button class="btn  btn-info" type="submit" name="by_date"  ><i class="icon-search icon-white"></i> Check</button><br/>
									
						  </div>
						</div>
					  </div>
					</div>
					</form>	
					
					
			</div>
				
								
	  			
		</div>
		
			
		
			  
		</div>
		
		</div>
	  <div class="clear"></div>

<script type="text/javascript">
	$(document).ready(function(){
			$('#timepicker').timepicker({
				minuteStep:30,
				//defaultTime: '8:00 PM'
			});
			
		
			
			
				$(function () {
				$('#myTab a').click(function (e) {
					  e.preventDefault();
					  $(this).tab('show');  
			})
			  })
			  
		
			  
			
	})
	</script>
