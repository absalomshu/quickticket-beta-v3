<?php defined('SYSPATH') or die('No direct script access'); ?> 
	

 
	<div class="row-fluid ">
	  	<div class="span12 hero-unit prof-unit" >
		
		<ul class="nav nav-tabs">
					<li > <a href="<?=url::site('cash')?>"><i class="icon-arrow-left"></i> <?=Kohana::lang('backend.back_cash')?></a></li>
		</ul>
		
		<div class="tab-content">
			 
			
			
			<div class="tab-pane active" id="cash-to-bank">
				<div class="span8 offset2">
				
					<legend><?=Kohana::lang('backend.add_cash_to_bank')?></legend>						
						<form action="<?=url::site('cash/add_cash_to_bank/')?>" method="POST" autocomplete="off">
						
						<div class="">
							<div class="span4 text-right"><?=Kohana::lang('backend.bank_name')?><span class="red"> *</span>: </div>
							<div class="span8">
								<input type="text" name="bank_name" required>
							</div>
						</div>
						<div class="">
							<div class="span4 text-right"><?=Kohana::lang('backend.account_number')?><span class="red"> *</span>: </div>
							<div class="span8">
								<input type="text" name="account_number" required>
							</div>
						</div><div class="">
							<div class="span4 text-right"><?=Kohana::lang('backend.amount')?><span class="red"> *</span>: </div>
							<div class="span8">
								<input type="text" name="amount" class="IsAmount" required>
							</div>
						</div>
					
						<div class="">
							<div class="span4 text-right"><?=Kohana::lang('backend.date');?>:</div>
							
							<div class="input-append date span4">
								<input type="text" name = 'date' class="span8 datepicker"  value="<?=date('d-m-Y')?>" id="dp1" >
								<span class="add-on"><i class="icon-calendar"></i></span>
							</div>	
							
							<!--<div class="span3">
								<button class="btn  btn-info" type="submit" name="by_period" ><i class="icon-search icon-white"></i> <?=Kohana::lang('backend.view');?></button><br/>
							</div>-->
						</div>
						<div class="clear"></div>
						<div class="form-actions">
							<div class="span4"></div>
							<div class="span8"><button class="btn  btn-info" type="submit" name="cash_to_bank" ><?=Kohana::lang('backend.save');?></button>
							<a class="btn " href="<?=url::site('cash')?>"><?=Kohana::lang('backend.cancel');?></a>
							</div>
						</div>
					</form>	
				</div>	
					
			</div>
					
		</div>
		
			
		
			  
		</div>
		
		</div>
	  <div class="clear"></div>

<script type="text/javascript">
	$(document).ready(function(){
			$('#timepicker').timepicker({
				minuteStep:30,
				//defaultTime: '8:00 PM'
			});
			
			$('.input-append.date').datepicker({
				format: "dd-mm-yyyy",
				todayBtn: "linked",
				autoclose: true,
				todayHighlight: true
			});
			
			
				$(function () {
				$('#myTab a').click(function (e) {
					  e.preventDefault();
					  $(this).tab('show');  
			})
			  })
			
	})
	</script>
