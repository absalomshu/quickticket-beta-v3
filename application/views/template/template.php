<!DOCTYPE html>
<html lang="en">

	<head>
	   <!-- <meta http-equiv="content-type" content="text/html; charset=iso-8859-1" /> -->
		<title><?=$title?> - <?=Kohana::lang('backend.online_booking')?>  | QuickTicket</title>
		<link rel="shortcut icon" href="<?=url::base()?>images/favicon.ico" type="image/x-icon" />
		<link rel="icon" href="<?=url::base()?>ico/favicon.ico">

		<meta name="viewport" content="width=device-width, initial-scale=1.0">
		<meta name="description" content="Travel made easy. Online reservation and account and agency management back-end.">
		<meta name="keywords" content="keywords go here">
		<meta name="author" content="@absalomshu">
		
		<link href="<?=url::base()?>css/bootstrap.css" rel="stylesheet">
		<link href="<?=url::base()?>css/style.css" rel="stylesheet">
		<link href="<?=url::base()?>css/datepicker.css" rel="stylesheet">
				  		

		<script src="<?=url::base()?>js/jquery.js"></script>

		

		
	</head>

	<body class="frontend">
	
		
		<?=render::template_header();?>
		
		<div class="" >
			<?=render::front_breadcrumb();?>
			<?=$content?>
		</div> 
		
		<div class="footer" >
			<?=render::frontend_footer();?>
		</div>
		
  </body>
</html>
