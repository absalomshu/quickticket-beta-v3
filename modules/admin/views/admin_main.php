<?php defined('SYSPATH') or die('No direct script access'); ?> 

		<?php
			//What the user entered in his previous submission attempt, in case it was a failure
			$post = $this->session->get_once('post');
			
			
			?>
	
		
	  
	 
	<div class="row-fluid">
	  <?php 
	  //first check if he is permitted to access each dashboard element
	  //var_dump($_SESSION['AdminDashboardItems']);exit;
	  if($_SESSION['AdminDashboardItems'][1]){?>
		<form action="<?=url::site('admin/create_schedule')?>" method="POST">
		 
     
	  	<div class="span12 hero-unit prof-unit" >
		<!--no csrf-->
		<?php no_csrf::set(1)?>
		<div class="heading"><?=Kohana::lang('backend.create_schedule')?><span style="float:right;font-size:12px;font-weight:normal;"></span></div>
			<div class="rule"><hr/></div>
			<div class="span2 spacious">
				<div class="control-group">
				<label class="control-label lab" for="inputType"><?=Kohana::lang('backend.from')?>:</label>
					
					<div class="controls ">
						<select name="from" id="inputType" disabled>
						 <?php //$admins_town = get::admins_town($this->admin->agency_id);?>
							<option value="" disabled><?=$agency_town?></option>
							
						</select>
					</div>
					
				</div>
			</div>
			<div class="span2 spacious">
				<div class="control-group">
				<label class="control-label lab" for="inputType"><?=Kohana::lang('backend.to')?>:</label>
					
					<div class="controls ">
						<select name="to" id="inputType">
							<?php foreach ($towns as $town):?>
							<option value="<?=$town->id?>"><?=$town->name?></option>
							<?php endforeach;?>
						
					</select>
					</div>
				</div>
			</div>
			<!--<div class="span2 spacious">
				<div class="control-group">
				<label class="control-label lab" for="inputType">Time:</label>
					<div class="controls ">
						<select name="departure_time" id="inputType">

							<option value="Day(8:00 AM)">Morning (8:00 AM)</option>
							<option value="Afternoon(1:00PM)">Afternoon (1:00 PM)</option>
							<option value="Night(8:00 PM)">Night (8:00 PM)</option>

						</select>
					</div>
				</div>
			</div>-->
			
			

			<div class="span2 spacious">
				<div class="control-group">
				<label class="control-label lab" for="inputType"><?=Kohana::lang('backend.time')?>:</label>
					<div class="controls ">
					
					<div class="input-append bootstrap-timepicker">
						<input name="departure_time" id="timepicker" type="text" class="span10"  value="<?=date("g:i A", strtotime("8:00 PM"));?>" />
						<span class="add-on"><i class="icon-time"></i></span>
					</div>
					</div>
				</div>
			</div>	
		
			
			<div class="span2 spacious">
				<div class="control-group">
				<label class="control-label lab" for="inputType">Date:</label>
					<div class="controls ">
					
					<div class="input-append date">
						<input type="text" name = 'departure_date' class="span7 datepicker"  value="<?=date('d-m-Y')?>" id="dp1">
						<span class="add-on"><i class="icon-calendar"></i></span>
					</div>
					</div>
				</div>
			</div>
			
			<div class="clear"></div>
			
			<div class="span2 spacious dropdown"  id="existing-bus-number">
				<div class="control-group">
				<label class="control-label lab" for="inputType"><?=Kohana::lang('backend.existing_buses')?>:</label>
					<div class="controls ">
						<select name="bus_number_and_seats" id="inputType">
					
						<ul class="dropdown-menu" role="menu" aria-labelledby="dropdownMenu">
						<optgroup label="Available">
						<?php foreach ($parent_buses as $bus):
							//First make sure the bus is not in another current schedule
							//If it's not, show it
							$bus_in_current_schedule = Schedule_Model::check_if_bus_in_current_schedule($this->agency_id,$bus->bus_number);
							if(!$bus_in_current_schedule){
							?>
							<li>
								 <a tabindex="-1" href="#">
									<option value="<?=$bus->bus_number?>;<?=$bus->bus_seats?>"><b><?=$bus->bus_number?></b> - <?=$bus->bus_seats?> <?=Kohana::lang('backend.seater')?> </option>
								</a>
							</li>
							
							<?php } endforeach;?>
						</optgroup>
						<optgroup label="Loading now...">
							<?php foreach ($parent_buses as $bus):
							//If it's in a current schedule, disable it
							$bus_in_current_schedule = Schedule_Model::check_if_bus_in_current_schedule($this->agency_id,$bus->bus_number);
							if($bus_in_current_schedule){
							?>
							<li>
								 <a tabindex="-1" href="#">
									<option disabled value="<?=$bus->bus_number?>;<?=$bus->bus_seats?>"><b><?=$bus->bus_number?></b> - <?=$bus->bus_seats?> <?=Kohana::lang('backend.seater')?> </option>
								</a>
							</li>
							
							<?php }endforeach;?>
						</optgroup>
						
						
						</ul>
						<!--putting a hidden input for each buses' seats was tricky as it has to be done within the loop which is within the 
							select, hence the value passed is the bus number AND the bus seats, separated by a semicolon. Handle in PHP-->
							
						</select>
						
					</div>
				</div>
			</div>
			
			
			<div class="span2 spacious" style="display:none" id="bus-number">
				<div class="control-group">
				<label class="control-label lab" for="inputType"><?=Kohana::lang('backend.bus_no')?>:</label>
					<div class="controls ">
						<input type="text" placeholder="e.g. NW999AB"  name="unregistered_bus_number" class="span12">
					</div>
				</div>
			</div>
			
			<div class="span2 spacious" style="display:none" id="bus-type" >
				<div class="control-group">
				<label class="control-label lab" for="inputType"><?=Kohana::lang('backend.bus_type')?>:</label>
					<div class="controls ">
						<select name="unregistered_bus_seats" id="inputType">

							<option value="30">30 <?=Kohana::lang('backend.seater')?></option>
							<option value="35">35 <?=Kohana::lang('backend.seater')?></option>
							<option value="39">39 <?=Kohana::lang('backend.seater')?></option>
							<option value="55">55 <?=Kohana::lang('backend.seater')?></option>
							<option value="70">70 <?=Kohana::lang('backend.seater')?></option>

						</select>
					</div>
				</div>
			</div>
			
			
			<div class="span2 spacious">
				<div class="control-group">
				<label class="control-label lab" for="inputType"><?=Kohana::lang('backend.ticket_price')?> :</label>
					<div class="controls input-append">
						<input type="text"  name="ticket_price" value="<?=@$post['ticket_price']?>" class="span9 IsAmount" required>
						<span class="add-on"> FCFA</span>
					</div>
				</div>
			</div>
			<div class="span2 ">	
			<div class="control-group">
				<label class="control-label lab" for="inputType"></label>
					<div class="controls large">
									<button class="btn create-btn btn-large btn-success" type="submit"><i class="icon-plus icon-white"></i> <?=Kohana::lang('backend.create')?></button><br/>

					</div>
				</div>
				
			</div>
			<!-- Option to create schedule from unregistered bus currently disabled.
			<div class="clear" style="height:20px;font-size:13px;margin: -20px 0 0 20px;"><i class="icon-bus"></i> <?=Kohana::lang('backend.unreg_bus')?>? <input type="checkbox" id="unregistered_bus" name="unregistered_bus_check"/><br/></div>
			-->  
		</div>
		</form>
	</div><?php } 
	
	?>
	
	<div class="row-fluid " id="current-scheds">	
		<?
		//If he's allowed to view all schedules
		?>
		<div class="span7 hero-unit prof-unit reload" >
			<div class="heading"><?=Kohana::lang('backend.current_schedules').": ";?><?php if(isset($restricted_town)){echo "To ". get::town($restricted_town)." only";}else {echo Kohana::lang('backend.from')." ".get::admins_town($this->admin->agency_id)->name;}?><a href="<?=url::site('admin/all_schedules/current')?>" class="view"><?=Kohana::lang('backend.see_all');?></a></div>
			<div class="rule"><hr/></div>
				<table class="table table-striped table-hover table-condensed">
					<tr><th><i class="icon-calendar "></i></th><th> Bus</th><th><?=Kohana::lang('backend.to')?></th> <th> <?=Kohana::lang('backend.departure')?></th><th class="text-right">Actions</th></tr>				
					<?php foreach ($current_schedules as $c_schedule):		?>
					<tr>
						<td><i><?php if(date("d-m-Y", strtotime($c_schedule->departure_date)) == date("d-m-Y")){echo "Today";}else{ ?><span <?php if(strtotime($c_schedule->departure_date) < strtotime(date("d-m-Y"))){?>class="red"<?php }?> ><?php echo date("d-m-Y", strtotime($c_schedule->departure_date));}?></span></i></td>
						<td> <a href="<?=url::site('admin/complete_schedule').'/'.$c_schedule->id?>"><?=$c_schedule->bus_number; if(Bus_Model::is_vip($this->agency_id,$c_schedule->bus_number)){?> <i class="icon-star" title="VIP bus"></i><?php } ?></a></td>
						<td><b><?=$c_schedule->town_name?></b> </td><td> <b><?=date("g:i A", strtotime($c_schedule->departure_time))?></b></td><td><span class="view"><a href="<?=url::site('admin/edit_schedule').'/'.$c_schedule->id?>"><?=Kohana::lang('backend.edit')?> </a>/<a href="#" onClick="checkout(<?=$c_schedule->id?>)"><?=Kohana::lang('backend.checkout')?> </a><!--<a href="<?php //url::site('admin/checkout_schedule/'.$c_schedule->id)?>" onClick="checkout(<?php //$c_schedule->id?>)">Check out</a>--></span></td>
					</tr>
					<!--<input type="hidden"  class="schedule_id" value="<?=$c_schedule->id?>"></input>-->
					<?php endforeach; ?>
				
				</table>
				
			
		</div>
		<?php
	  //first check if he is permitted to access each dashboard element
	  //var_dump($_SESSION['AdminDashboardItems']);exit;
	  if($_SESSION['AdminDashboardItems'][4]){?> 
		<div class="span5 hero-unit prof-unit">
				<div class="heading"><?=Kohana::lang('backend.recently_checkedout')?><a href="<?=url::site('admin/all_schedules/departed')?>" class="view"><?=Kohana::lang('backend.see_all')?></a></div>
				<div class="rule"><hr/></div>
				
					<table class="table table-condensed recent">
						<tr><th><i class="icon-calendar "></i></th><th> Bus</th><th><?=Kohana::lang('backend.to')?></th> <th> <?=Kohana::lang('backend.checked_out')?></th><th class="text-right">Actions</th></tr>	
						<?php foreach ($departed_schedules as $d_schedule):?>
						<tr><td><?php echo date("d-m-Y", strtotime($d_schedule->departure_date));?></td> <td><a href="<?=url::site('admin/past_schedule').'/'.$d_schedule->id?>"><?=$d_schedule->bus_number?></a></td> <td><b><?=$d_schedule->town_name?></b> </td><td> <b><?=date("g:i A", strtotime($d_schedule->checked_out_time))?></b></td><td><span class="view"><a href="<?=url::site('admin/past_schedule').'/'.$d_schedule->id?>"><?=Kohana::lang('backend.view')?></a></span></td></tr>
						<?php endforeach; ?>
					</table>				
				
				<!--<br/><a href="#" class="small-link">See all checked out...</a>-->
				
		</div>
		<?php }?>
		
	</div>
	<!--<div class="row-fluid marketing" style="margin:5px 0 0px 0;" id="parcels">	
		<div id="dashboard-demo">
    
      You need javascript to use the dashboard.
    </div>
	
	
		
		</div>-->
		
		
		

	
	<?php //$this->profiler = new Profiler();?>
	
<script type="text/javascript">
	$(document).ready(function(){
	
	$('.input-append.date').datepicker({
		format: "dd-mm-yyyy",
		todayBtn: "linked",
		autoclose: true,
		todayHighlight: true,
		startDate: '-0d'
	
	});
	
	$('#timepicker').timepicker({
		minuteStep:30
	});
		
	})
	
	$('#unregistered_bus').change(function(){
				if(this.checked){
					$('#existing-bus-number').hide(150);					
					$('#bus-number').show(150);					
					$('#bus-type').show(150);					
					}else{
					$('#existing-bus-number').show(150);	
					$('#bus-number').hide(150);					
					$('#bus-type').hide(150);
					}
			
			});
	
	function checkout(schedule_id)
	{	
		var domain = '<?=url::base()?>';
		var answer = confirm("<?=Kohana::lang('backend.confirm_checkout')?>");
		if (answer == true)
		{
			window.location.href=domain+'admin/checkout_schedule/'+schedule_id;
		}
		else{
			return;}
		
	};
</script>
