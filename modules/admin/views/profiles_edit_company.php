<?php defined('SYSPATH') or die('No direct script access'); ?> 
<!DOCTYPE html>

<?php //var_dump($logo);?>
		
	<div class="span6 hero-unit prof-unit offset3">
			<div class="notice">
				<?php
					//determine what type of notice to display if at all
					$notice = $this->session->get_once('notice');
						if(!empty($notice)){ 
							if($notice['type'] == 'success'){?><div class="alert general success"><a class="close" data-dismiss="alert" href="#">&times;</a><?php echo $notice['message']; ?></div><?php }?>
						<?	if($notice['type'] == 'error'){?><div class="alert general error"><a class="close" data-dismiss="alert" href="#">&times;</a><?php echo $notice['message'];?></div><?php }?>
				<?}?>
			</div>
				
			<h20>Edit details for <?=$company->name?></h20>
			<div class="rule"><hr/></div><br/>
				<form method="POST" action="<?=url::site('profiles/edit_company/'.$company->id)?>" enctype="multipart/form-data" class="form-horizontal">
				
				  <div >
					<label class="control-label" >Logo:</label>
					<div class="controls">
								<img src="<?php echo url::base(). $logo;?>" class="img-polaroid">
					</div>
				  </div>
				 
				  
				  <div >
					<label class="control-label" >&nbsp;</label>
					<div class="controls">
								<input type="file"  name="company_logo" />
					</div>
				  </div>
				  
				   <div >
					<label class="control-label" for="inputEmail">Name:</label>
					<div class="controls">
								<input type="text" value="<?=$company->name?>" disabled name="company_name"/>&nbsp;&nbsp;
					</div>
				  </div>
				   
				  <div >
					<label class="control-label" for="inputEmail">Contacts:</label>
					<div class="controls">
								<textarea type="text" name="contacts"/><?=@$company->contacts?></textarea>
					</div>
				  </div>
				  
				  <div >
					<label class="control-label" for="inputEmail">Additional ticket information:</label>
					<div class="controls">
								<textarea type="text"  name="ticket_info"/><?=@$company->ticket_info?></textarea>
					</div>
				  </div>
				  
				  
				  <div class="">
						<label class="control-label lab" for="inputType"></label>
							<div class="controls ">
								<button class="btn btn-info" type="submit" >Save</button>					
							</div>
						</div>	
				</form>			
				
			
			

		</div>

