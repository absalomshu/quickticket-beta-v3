<?php defined('SYSPATH') or die('No direct script access'); ?> 


	  <div class="row-fluid " style="margin:5px 0 0px 0px;">
	  
		<form action="<?=url::site('admin/edit_schedule/'.$schedule->id)?>" method="POST" name="theform">
		
	  	<div class="span12 hero-unit prof-unit" >
		<?php no_csrf::set(); ?>
		<div class="heading"><?=Kohana::lang('backend.edit_sched_for')?> <?=$schedule->bus_number?><span style="float:right;font-size:12px;font-weight:normal;"><!--<a href="#"><?=Kohana::lang('backend.see_all')?></a>--></span></div>
			<div class="rule"><hr/></div>
			<div class="span2 spacious">
				<div class="control-group">
				<label class="control-label lab" for="inputType"><?=Kohana::lang('backend.from')?>:</label>
					
					<!--<div class="controls ">
						<select name="from" id="inputType">
							<option value="<?php //$schedule->from;?>"><h2><?php //get::town($schedule->from);?></h2></option>
							<?php //foreach ($towns as $town):?>
							<option value="<?php //$town->id?>"><?php //$town->name?></option>
							<?php //endforeach;?>
						</select>
					</div>-->
					<div class="controls ">
						<select name="from" id="inputType" disabled>
						 <?php $admins_town = get::admins_town($this->admin->agency_id);?>
							<option value="<?=$admins_town->id?>" disabled><?=$admins_town->name?></option>
							
						</select>
					</div>
				</div>
			</div>
			<div class="span2 spacious">
				<div class="control-group">
				<label class="control-label lab" for="inputType" ><?=Kohana::lang('backend.to')?>:</label>
					
					<div class="controls ">
						<select name="to" id="inputType" readonly>
							<option value="<?=$schedule->to;?>"><h2><?=get::town($schedule->to);?></h2></option>
							<?php /*foreach ($towns as $town):?>
							<option value="<?=$town->id?>"><?=$town->name?></option>
							<?php endforeach;*/?>
						</select>
					</div>
				</div>
			</div>
			<div class="span2 spacious">
				<div class="control-group">
				<label class="control-label lab" for="inputType"><?=Kohana::lang('backend.time')?>:</label>
					<div class="controls ">
					
					<div class="input-append bootstrap-timepicker">
						<input name="departure_time" id="timepicker" type="text" class="input-small" value="<?=date("g:i A", strtotime($schedule->departure_time));?>">
						<span class="add-on"><i class="icon-time"></i></span>
					</div>
					</div>
				</div>
			</div>
			<div class="span2 spacious">
				<div class="control-group">
				<label class="control-label lab" for="inputType">Date:</label>
					<div class="controls ">
						<!--Convert date from Y-M-D in db -->
						<div class="input-append date">
							<input type="text" name = 'departure_date' class="span8 datepicker"  value="<?=date("d-m-Y",strtotime($schedule->departure_date))?>" id="dp1" />
							<span class="add-on"><i class="icon-th"></i></span>
						</div>
					</div>
				</div>
			</div>
			
			<div class="clear"></div>
			
			<div class="span2 spacious dropdown"  id="existing-bus-number">
				<div class="control-group">
				<label class="control-label lab" for="inputType"><?=Kohana::lang('backend.existing_buses')?>:</label>
					<div class="controls ">
						<select name="bus_number_and_seats" id="inputType" >
					
							<ul class="dropdown-menu" role="menu" aria-labelledby="dropdownMenu">
							<li>
								<option value="<?=$schedule->bus_number?>;<?=$schedule->bus_seats?>" readonly><b><?=$schedule->bus_number?></b> - <?=$schedule->bus_seats?> <?=Kohana::lang('backend.seater')?> </option>
							</li>
								<optgroup label="Available">
								<?php  foreach ($parent_buses as $bus):?>
									<?php //Show only buses whose number of seats is more than the number of seats already taken.
										if($seats_taken <= $bus->bus_seats){
											// make sure the bus is not in another current schedule
											//If it's not, show it
											$bus_in_current_schedule = Schedule_Model::check_if_bus_in_current_schedule($this->agency_id,$bus->bus_number);	
											if(!$bus_in_current_schedule){
											
									?>
										<li>
											<a tabindex="-1" href="#">
												<option value="<?=$bus->bus_number?>;<?=$bus->bus_seats?>" readonly><b><?=$bus->bus_number?></b> - <?=$bus->bus_seats?> <?=Kohana::lang('backend.seater')?> </option>
											</a>
										</li>
										<?php  }
										} endforeach; ?>
								</optgroup >
									<optgroup label="Loading now...">
										<?php foreach ($parent_buses as $bus):
										//If it's in a current schedule, disable it
										$bus_in_current_schedule = Schedule_Model::check_if_bus_in_current_schedule($this->agency_id,$bus->bus_number);
										if($bus_in_current_schedule){
										?>
										<li>
											 <a tabindex="-1" href="#">
												<option disabled value="<?=$bus->bus_number?>;<?=$bus->bus_seats?>"><b><?=$bus->bus_number?></b> - <?=$bus->bus_seats?> <?=Kohana::lang('backend.seater')?> </option>
											</a>
										</li>
										
										<?php }endforeach;?>
									</optgroup>
							</ul>
						<!--putting a hidden input for each buses' seats was tricky as it has to be done within the loop which is within the 
							select, hence the value passed is the bus number AND the bus seats, separated by a semicolon. Handle in PHP-->
							
						</select>
						
					</div>
				</div>
			</div>
			
			
			
			
			<div class="span2 spacious">
				<div class="control-group">
				<label class="control-label lab" for="inputType" ><?=Kohana::lang('backend.ticket_price')?>:</label>
					<div class="controls input-append">
						<input type="text" value="<?=$schedule->ticket_price?>"  name="ticket_price" class="span8" readonly>
						<span class="add-on"> FCFA</span>
					</div>
				</div>
			</div>
			<div class="span2 ">	
			<div class="control-group">
				<label class="control-label lab" for="inputType">&nbsp;</label>
					<div class="controls large">
									<button class="btn btn-info btn-medium" type="submit"  ><?=Kohana::lang('backend.save')?></button>
									<a href="<?=url::site('admin/complete_schedule/'.$schedule->id)?>"class="btn "  ><?=Kohana::lang('backend.cancel')?></a>
									<a href="<?=url::site('admin/safe_delete_schedule/'.$schedule->id)?>" class="btn" onClick="return confirm('<?=Kohana::lang('backend.delete_schedule')?>?');" title="Delete schedule">Delete</a>
					</div>
				</div>
				
			</div>
			
			  
		</div>
		</form>
		
	  </div>
	  <div style="height:19px;"></div>

<script type="text/javascript">
	$(document).ready(function(){
			$('#timepicker').timepicker({
				minuteStep:30,
				//defaultTime: '8:00 PM'
			});
			
			
			$('.input-append.date').datepicker({
				format: "dd-mm-yyyy",
				todayBtn: "linked",
				autoclose: true,
				todayHighlight: true
			});
	})
	</script>
