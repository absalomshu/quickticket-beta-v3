<div style="clear:both;height:50px;"></div>

		<div class="footer-content">
					<span class="loaddiv">
						<!-- DON'T DELETE THIS DIV. IT'S FOR THE ONLINE TRANSACTIONS-->	
					</span>
					<!--JS running in background should return messages here-->
					<div class="alert fade in" id="js_messages">
						<button type="button" class="close" data-dismiss="alert">×</button>
						Loading...
					</div>
					
		<span class="footer-left span5">
			<span class="heading"><?=Kohana::lang('backend.follow')?>:</span> 
			<a href="https://www.facebook.com/getquickticket" target="_blank"><img src="<?=url::base()?>images/social/facebook.png" width="48" height="48"/></a>
			<a href="https://twitter.com/getquickticket" target="_blank"><img src="<?=url::base()?>images/social/twitter.png"  width="48" height="48"/></a>
			<a href="#" ><img src="<?=url::base()?>images/social/google.png"  width="48" height="48"/></a>
		</span>
	
		       <span class="footer-right span7"><a href="<?=url::site('terms/conditions')?>"><?=Kohana::lang('backend.terms')?></a> | <a href="<?=url::site('terms/contact')?>">Contact</a>
					<br/>
					<?php 
					$admin=Authlite::instance()->get_admin(); 
					if (empty($admin)){?>
						<b><a href="<?=url::site('admin/lang')?>"><?=Kohana::lang('backend.change_lang')?></a></b> &nbsp;&nbsp;&nbsp;&nbsp; . &nbsp;&nbsp;&nbsp;&nbsp;  
					<?php } ?>
					Internet <span id="internet_available"><!--DON'T DELETE--></span>  &nbsp;&nbsp;&nbsp;&nbsp; . &nbsp;&nbsp;&nbsp;&nbsp;  
					QuickTicket V4.0.1 &copy; 2016  &nbsp;&nbsp;&nbsp;&nbsp; . &nbsp;&nbsp;&nbsp;&nbsp;   <?=Kohana::lang('backend.all_rights')?>
				</span>
		</div>
	
    <!-- Le javascript
    ================================================== -->
    <!-- Placed at the end of the document so the pages load faster -->
  
	<script src="<?=url::base()?>js/bjqs-1.3.min.js"></script>
	<script src="<?=url::base()?>js/bootstrap-datepicker.js"></script>
	<script src="<?=url::base()?>js/bootstrap-timepicker.min.js"></script>
    <script src="<?=url::base()?>js/bootstrap-transition.js"></script>
    <script src="<?=url::base()?>js/bootstrap-alert.js"></script>
    <script src="<?=url::base()?>js/bootstrap-modal.js"></script>
    <script src="<?=url::base()?>js/bootstrap-tooltip.js"></script>
    <script src="<?=url::base()?>js/bootstrap-popover.js"></script>
    <script src="<?=url::base()?>js/bootstrap-dropdown.js"></script>
    <script src="<?=url::base()?>js/bootstrap-scrollspy.js"></script>
    <script src="<?=url::base()?>js/bootstrap-tab.js"></script>
    <script src="<?=url::base()?>js/bootstrap-collapse.js"></script>
	
<script type="text/javascript">
	//js for the datepicker. done in the footer so it doesn't slow lower page load, and inline since it's not much
	$(document).ready(function(){
	$('.input-append.date').datepicker({
	format: "dd-mm-yyyy",
    todayBtn: "linked",
    autoclose: true,
    todayHighlight: true
	
});
	}) 
	
	//1
	//Check for any SMS to be sent and dispatch.
	//The update function checks for any unpushed sms in the offline db, and pushes online
	//Now the second parameter of the load below, is to ensure that the sms_update completes/succeeds before the sms_disptch is launched
	//If not, they might go at once
	//NB Set timeout is one-time based. Set interval is recurrent.
	window.onload = sms_update;
	function sms_update()
	{	
			$('#js_messages').load('<?=url::site('sms_monitor/update');?>',function sms_dispatch(){$('#js_messages').load('<?=url::site('sms_monitor/trigger_dispatch');?>');});	
	}
	
	//2
	//Download parcels after 10 secs, giving time for  functions above to run
	setTimeout(function()
	{
		$('#js_messages').load('<?=url::site('online/pull_parcels');?>');
	}, 10000);
	
	
	$(document).ready(function()
	{	
	
		//On page load, check internet
		check_internet_js();
		function check_internet_js(){
			$('#internet_available').load('<?=url::site('online/check_internet');?>',
			function(responseTxt, statusTxt, xhr){
				if(responseTxt == "1"){
					$("#internet_available").removeClass("red-dot");
					$("#internet_available").addClass("green-dot");
				}	
				if(responseTxt == "0"){
					$("#internet_available").removeClass("green-dot");
					$("#internet_available").addClass("red-dot");
				}	
		 });
			
			
		};
		
		
		
		
		//3 Get and push ALL transactions. First run after 25 secs, then every 25 secs.
		//This queues up, so it should first execute after 25s, then every 2 mins or so. 
		//2 mins because for now, it takes avg 1.2 mins for online/index to finish at Musango
		setTimeout(function()
		{
			$('#js_messages').load('<?=url::site('online/index');?>');
		}, 25000);
		
		setInterval(function()
		{
			$('#js_messages').load('<?=url::site('online/index');?>');
		}, 120000);
		
		//Check internet connectivity. First run at page load then called every 30 secs.
		setInterval(check_internet_js, 30000);	

		//4 Push cash to bank transactions
		setTimeout(function()
		{
			$('#js_messages').load('<?=url::site('online/push_cash_to_bank');?>');
		}, 35000);
		//5 Push and then pull remote_tickets	
		setTimeout(function()
		{
			$('#js_messages').load('<?=url::site('online/push_remote_tickets');?>');
		}, 45000);
		setTimeout(function()
		{
			$('#js_messages').load('<?=url::site('online/pull_remote_tickets');?>');
		}, 55000);
		
		//6 Pull online purchases
		setTimeout(function()
		{
			$('#js_messages').load('<?=url::site('online/pull_online_purchases');?>');
		}, 60000);
		
		//7 Pull online reservation requests
		setTimeout(function()
		{
			$('#js_messages').load('<?=url::site('online/pull_reservation_requests');?>');
		}, 70000);
		
		//8 Push income
		setTimeout(function()
		{
			$('#js_messages').load('<?=url::site('online/push_income');?>');
		}, 75000);
	
	})
	
	
	//This is a plugin to format numbers, to ensure that, all amounts, are amounts. No text, no commas or fullstops or special chars
	//Add IsAmount class to all amount inputs.
		//$('input.IsAmount').number(true);
		$('input.IsAmount').keyup(function(event) {

	  // skip for arrow keys
	  if(event.which >= 37 && event.which <= 40) return;

	  // format number
	  $(this).val(function(index, value) {
		return value
		  .replace(/\D/g, "")
		  .replace(/\B(?=(\d{3})+(?!\d))/g, "")
		;
	  });
	});
	
	//Popover for all schedules to give more details
	$('.IsSchedule').popover({
		'trigger':'hover'
	});
</script>

 