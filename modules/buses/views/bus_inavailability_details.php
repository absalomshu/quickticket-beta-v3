<?php defined('SYSPATH') or die('No direct script access'); ?>
	
		<div class="row-fluid  span12 hero-unit prof-unit reload" style="margin:5px 0 0px 0;">	
		
			
			
		
			<div class="no-print">
			
			<ul class="nav nav-tabs">
					<li > <a href="<?=url::site('buses/check_buses')?>"><i class="icon-arrow-left"></i> <?=Kohana::lang('backend.back_buses')?></a></li>
			<button class="view" onClick="window.print()"><i class="icon-print"></i> <?=Kohana::lang('backend.print')?></button>
	
			</ul>
			</div>
			<div class=" print-area" >
			
				<legend>
					<div class="print-head">
						<?php $admin=Authlite::instance()->get_admin();?>
						<?=strtoupper(get::_parent($admin->agency_id)->name);?><br/>
						<?=get::agency_name($admin->agency_id);?>
					</div>
					Report: Bus unavailable periods	<br/>
					Bus number: <?=$bus_number?><br/>
					Start date: <?=date("d-m-Y",strtotime($start_date))?> &nbsp;&nbsp;&nbsp;&nbsp; 
					End date: <?=date("d-m-Y",strtotime($end_date))?>&nbsp;&nbsp;&nbsp;&nbsp;
				</legend>
				
				<table class="table table-striped table-condensed">
					<tr>
						<th class="span2">From</th>
						<th class="span4"></th>
						<th class="span2">To</th>
						<th class="span4"></th>
						<th>Reason</th>
						<th>Done by</th>
					</tr>
					<?php 
					foreach ($availabilities as $availability)
					{?>
					<tr>
						<td ><?=date("d-m-Y",strtotime($availability->start_date))?></td>
						<td><?=date("H:i A",strtotime($availability->start_time))?></td>
						<td><?=date("d-m-Y",strtotime($availability->end_date))?></td>
						<td><?=date("H:i A",strtotime($availability->end_time))?></td>
						<td ><?=$availability->reason?></td>
						<td ><?=$availability->created_by?></td>
					</tr>
					<?php }?>
				
				</table>
				
					
			
		
			
				<div>Printed by: <?=STRTOUPPER($this->admin->username);?></div> 
				<div>Printed on: <?=date('d-m-Y h:i:s A')?></div>
				<!--END OF NET SUMMARY-->
				
			</div>

			
		
		
		
		
		
		
		</div>

