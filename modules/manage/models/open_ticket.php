<?php defined('SYSPATH') or die('No direct script access');

class Open_Ticket_Model extends ORM{

	public function add_open_ticket($agency_id,$open_ticket_name,$open_ticket_price,$open_ticket_town_from,$open_ticket_town_to)
	{	
		$open_ticket = ORM::FACTORY('open_ticket');
		$open_ticket->agency_id = $agency_id;
		$open_ticket->open_ticket_name = $open_ticket_name;
		$open_ticket->price = $open_ticket_price;
		$open_ticket->town_from = $open_ticket_town_from;
		$open_ticket->town_to = $open_ticket_town_to;
		
		$open_ticket->save();
		
		return $open_ticket->id;
	}
	
	
	public function get_all_for_agency($agency_id)
	{
		$open_tickets=ORM::factory('open_ticket')->where('agency_id',$agency_id)->where('deleted','0')->find_all();
		return $open_tickets;	
	}
	
	public function get_all_for_parent($parent_id)
	{
		$open_tickets=ORM::factory('open_ticket')->join('agencies','agencies.id = open_tickets.agency_id')->where('parent_id',$parent_id)->where('deleted','0')->find_all();
		return $open_tickets;	
	}
	
	public function get_all_for_route($town_from, $town_to)
	{
		$open_tickets=ORM::factory('open_ticket')->where('town_from',$town_from)->where('town_to',$town_to)->where('deleted','0')->find_all();
		return $open_tickets;	
	}
	
	public function delete_open_ticket($agency_id, $open_ticket_id)
	{
		$open_ticket=ORM::factory('open_ticket')->where('agency_id',$agency_id)->where('id',$open_ticket_id)->find();
		$open_ticket->deleted = '1';
		$open_ticket->save();
		
		$notice="Ticket deleted";
		return $notice;
		
	}
	

}
