<?php defined('SYSPATH') or die('No direct script access'); ?> 
	
	<div class="notice">
		<?php
			//determine what type of notice to display if at all
			$notice = $this->session->get_once('notice');
				if(!empty($notice)){ 
					if($notice['type'] == 'success'){?><div class="alert general success"><a class="close" data-dismiss="alert" href="#">&times;</a><?php echo $notice['message']; ?></div><?php }?>
				<?	if($notice['type'] == 'error'){?><div class="alert general error"><a class="close" data-dismiss="alert" href="#">&times;</a><?php echo $notice['message'];?></div><?php }?>
		<?}?>
	</div>
 
	<div class="row-fluid " >
	  
						
	  	<div class="span12 hero-unit prof-unit" >
				<ul class="nav nav-tabs">
					<li > <a href="<?=url::site('settings')?>"><i class="icon-arrow-left"></i> <?=Kohana::lang('backend.back_buses')?></a></li>
				</ul>
		
				<form action="<?=url::site('settings/edit_bus/')?>" method="POST" enctype="multipart/form-data" class="span8 offset2">
				<legend><?=Kohana::lang('backend.add_bus_requirement')?> </legend>
					
						
						
						
						<div class="">
							<div class="span4"><?=Kohana::lang('backend.serial_no')?>: </div>
							<div class="span8"><input type="text"  name="serial_number" ></div>
						</div>
						
						<div class="">
							<div class="span4"><?=Kohana::lang('backend.bus_name')?>: </div>
							<div class="span8"><input type="text"  name="bus_name"  ></div>
						</div>
					
						
						<div class="">
							<div class="span4"><?=Kohana::lang('backend.drivers_name')?>: </div>
							<div class="span8"><input type="text"  name="driver" ></div>
						</div>
						
						<div class="">
							<div class="span4"><?=Kohana::lang('backend.motorboys_name')?>: </div>
							<div class="span8"><input type="text"  name="motorboy"  ></div>
						</div>
						
						<div class="">
							<div class="span4"><?=Kohana::lang('backend.other_information')?>: </div>
							<div class="span8"><textarea placeholder="<?=Kohana::lang('backend.desc_parcel')?>"  name="other_infos" style="height:100px;"></textarea></div>
						</div>
						
						<div class="">
							<div class="span4"><?=Kohana::lang('backend.vip_bus')?>: </div>
							<div class="span8">
								<input type="hidden" name="vip" value="0" />
								<label class="checkbox"><input type="checkbox" name="vip" value="1" <?php if(1){echo 'checked = "checked"';} ?>/></label>
								
							</div>
						</div>
						
						
							 
							  
					  <div>
						<label class="span4" >&nbsp;</label>
						<div class="span8">
									<input type="file"  name="bus_photo" />
						</div>
					  </div>
									
					
						<div class="clear"></div>
						<div class="clear"><br/><br/></div>
						
						<div class="">
							<div class="span4"> </div>
							<div class="form-actions">
								<button class="btn btn-info" type="submit"> <?=Kohana::lang('backend.save')?></button>
								<a href="<?=url::site('buses/all')?>" class="btn" type="submit"> <?=Kohana::lang('backend.cancel')?></a>
								<!--<a href="<?php //url::site('buses/delete_bus/'.$bus->bus_number)?>"  onClick="return confirm('Are you sure you want to delete this bus?');" type="submit"> <?=Kohana::lang('backend.delete')?></a>
							--></div>
						</div>
						
									
				
						
				</form>
			 
			
				
			  					
			
		
			  
		</div>
		
		</div>
	  <div class="clear"></div>

<script type="text/javascript">
	$(document).ready(function(){
			$('#timepicker').timepicker({
				minuteStep:30,
				//defaultTime: '8:00 PM'
			});
			
			
			$('.input-append.date').datepicker({
				format: "dd-mm-yyyy",
				todayBtn: "linked",
				autoclose: true,
				todayHighlight: true
			});
			
			
				$(function () {
				$('#myTab a').click(function (e) {
					  e.preventDefault();
					  $(this).tab('show');  
			})
			  })
			  
		
			  
			
	})
	</script>
