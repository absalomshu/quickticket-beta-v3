<?php defined('SYSPATH') or die('No direct script access'); ?> 


	<?php
		//determine what type of notice to display if at all
		$notice = $this->session->get_once('notice');
			if(!empty($notice)){ 
				if($notice['type'] == 'success'){?><div class="alert general success"><a class="close" data-dismiss="alert" href="#">&times;</a><?php echo $notice['message']; }?></div>
			<?	if($notice['type'] == 'error'){?><div class="alert general error"><a class="close" data-dismiss="alert" href="#">&times;</a><?php echo $notice['message']; }?></div>
		<?}?>
    <div class="container">
	
	<div class="row-fluid">	
		
		<div class="span12 hero-unit prof-unit">
		
		<ul class="nav nav-tabs">
					<li > <a href="<?=url::site('expenses')?>"><i class="icon-arrow-left"></i> <?=Kohana::lang('backend.back_expenses')?></a></li>
		</ul>
		
		
		<div class="row-fluid">	
		
			<div class="span6 hero-unit prof-unit white-back">
				
				<div class="heading"><?=Kohana::lang('backend.details_expense')?>: #1222211</div>
				
				<div class="simple-form span12">
								<div class="sf_label">Date: </div>
								<div class="sf_text"><?=$expense->date_incurred;?></div>
				</div>
				<div class="simple-form span12">
								<div class="sf_label"><?=Kohana::lang('backend.amount')?>: </div>
								<div class="sf_text"><?=$expense->amount;?></div>
				</div>
				<div class="simple-form span12">
								<div class="sf_label"><?=Kohana::lang('backend.purpose')?>: </div>
								<div class="sf_text"><?=$expense->purpose;?></div>
				</div>
				<div class="simple-form span12">
								<div class="sf_label"><?=Kohana::lang('backend.authorised_by')?>: </div>
								<div class="sf_text"><?=$expense->authorised_by;?></div>
				</div>
				
			
			</div>
		
		
			
			<div class="span6">
			
			<div class="heading">f</div>
				
				
			</div>	
			
				
				
		</div>
	
		
		</div>
	
    </div> 

	</div>
	
	