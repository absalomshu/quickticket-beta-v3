<?php defined('SYSPATH') or die('No direct script access');

//Bus number is the primary key, as no two buses can have the same number
//ORM seems to require that an id column be available for a table to be updated, hence id is used for bus_number

class Buses_Controller extends Admin_Controller 
{
		
	protected $logo_directory = 'images/ticket/agency-logos/';
	//public $template = 'template/admin_template';
	
	
	public function __construct()
	{	
		parent::__construct();
		//$this->admin = Authlite::instance()->get_admin();
		$this->session = Session::instance();
		//if (!empty($this->admin)) $this->key = $this->admin->agency_id;
		
	}  
	
	
	public function index()
	{	
		//ensure a logged in user can't land on the signup page
		if (empty($this->admin)) 
		{
			url::redirect('admin/login');
		}else
		{
			$this->all();
		}
	}
	
	public function all()
	{
		Authlite::check_admin();
		Authlite::verify_referer();
		
		//If he's not a bus manager, by no means should he access buses page.
		if(!($this->admin->AllowBusManagement))
		{
			$this->logout();
		}
		
		$agency_id = $this->agency_id;
		$this->template->title = Kohana::lang('backend.all_buses');;
		$parent_id = get::_parent($this->agency_id)->id;		
		
		//pagination
		$per_page = 15;		
		$all_buses = get::all_buses($agency_id, $parent_id, 4);				
		$all_buses_total = count($all_buses);
		
		$this->pagination = new Pagination(array(
			// 'base_url'    => 'welcome/pagination_example/page/', // base_url will default to current uri
			'uri_segment'    => 'page', // pass a string as uri_segment to trigger former 'label' functionality
			'total_items'    => $all_buses_total, // use db count query here of course
			'items_per_page' => $per_page , // it may be handy to set defaults for stuff like this in config/pagination.php
			'style'          => 'punbb', // pick one from: classic (default), digg, extended, punbb, or add your own!
			//'query_string'   => "ORM::factory('schedule')->where('agency_id',$agency_id)->where('status','current')->find_all()"
		));
			
		$all_buses_per_page=ORM::factory('bus')->where('agency_id',$agency_id)->where('deleted','0')->orderby('bus_number','DESC')->limit($per_page,$this->pagination->sql_offset)->find_all();
		$view = new View('buses/all_buses');
		$view->all_buses = $all_buses_per_page;
		$this->template->content = $view;
	}
	
	
	function add_bus()
	{	
		Authlite::check_admin();
		Authlite::verify_referer();

		$this->template->title=Kohana::lang('backend.edit_company');
		$photo_directory = 'images/buses/';
		
		//$db = new Database();
		$bus = ORM::FACTORY('bus');	
		if($_POST){
			$post = new Validation($_POST);
			$post->add_rules('bus_number','required','valid::alpha_numeric');
			$post->add_rules('serial_number','valid::alpha_numeric');
			
			if ($post->validate())
			{
				//remove white spaces and check is number exists
				$bus_seats = $_POST['bus_seats'];
				$serial_number = $_POST['serial_number'];
				$bus_number = str_replace(" ","" ,strtoupper($_POST['bus_number']));
				$driver = $_POST['driver'];
				$bus_name = $_POST['bus_name'];
				$motorboy = $_POST['motorboy'];
				$other_infos = $_POST['other_infos'];
				$vip = $_POST['vip'];
				//intialize so if there's no photo, the model doesn't return an error
				$bus_photo = "";
				
				$bus_exists = ORM::FACTORY('bus')->where('bus_number',$bus_number)->where('deleted','0')->count_all();
				//ensure the bus number is unique
				if ($bus_exists)
				{
					$this->session->set('notice', array('message'=>Kohana::lang('backend.bus_exists'),'type'=>'error'));
				}else
				{	
					//if a picture is posted and it's not nameless
					if($_FILES['bus_photo']['error'] == 0 AND strlen($_FILES['bus_photo']['name'] )>0)
					{
						$files = new Validation($_FILES);
						$files->add_rules('bus_photo', 'upload::valid', 'upload::required', 'upload::type[gif,jpg,png]', 'upload::size[5M]');
						if ($files->validate())
						{	
							$photo = $_FILES['bus_photo'];
							
							// Temporary file name - the name of the company without whitespaces
							$filename = upload::save($photo, strtolower(str_replace(" ",'', $bus_number)));
							
							// Resize, sharpen, and save the image
							Image::factory($filename)
								->resize(100, 100, Image::WIDTH)
								->save($photo_directory.basename($filename).'.jpg');
								//$bus->photo = $photo_directory.basename($filename).'.jpg';
								$bus_photo = $photo_directory.basename($filename).'.jpg';
							// Remove the temporary file
							unlink($filename);
							
						}
						
					}
				
				
					Bus_Model::add_bus($this->agency_id, $bus_number, $serial_number, $bus_name, $bus_seats, $driver, $motorboy, $vip, $other_infos, $bus_photo);
					$this->session->set('notice', array('message'=>Kohana::lang('backend.bus_registered'),'type'=>'success'));
				}
			}
				else
				{
					$errors=$post->errors('errors');
					$notice="";
						foreach($errors as $error) 
						{
							$notice.=$error."<br />";
						}
						$this->session->set('notice', array('message'=>$notice,'type'=>'error'));
				}
			
				url::redirect('buses/all');
			}
		
		$view=new View('buses/edit_bus');
		$view->bus=$bus;
		//$view->logo = $logo;
		$this->template->content=$view;
			
	}

	
	function edit_bus($bus_number)
	 {
			Authlite::check_admin();
			Authlite::verify_referer();
			//Authlite::check_integrity($schedule_id);
			$this->template->title=Kohana::lang('backend.edit_company');
			$bus = get::bus($bus_number);	
			$photo_directory = 'images/buses/';
			$current_photo=get::bus_photo($bus_number);

		if($_POST)
		{
			$post = new Validation($_POST);
			$post->add_rules('serial_number','valid::alpha_numeric');
			
			if ($post->validate())
			{	
				$bus_seats = $_POST['bus_seats'];
				$serial_number = $_POST['serial_number'];
				$driver = $_POST['driver'];
				$vip = $_POST['vip'];
				$other_infos = $_POST['other_infos'];
				$motorboy = $_POST['motorboy'];
				$bus_name = $_POST['bus_name'];
				
				//iniialize
				$bus_photo='';
				
				//if a picture is posted and it's not nameless
				if($_FILES['bus_photo']['error'] == 0 AND strlen($_FILES['bus_photo']['name'] )>0)
				{
					$files = new Validation($_FILES);
					$files->add_rules('bus_photo', 'upload::valid', 'upload::required', 'upload::type[gif,jpg,png]', 'upload::size[5M]');
					if ($files->validate())
					{	
						$photo = $_FILES['bus_photo'];
						
						// Temporary file name - the name of the company without whitespaces
						$filename = upload::save($photo, strtolower(str_replace(" ",'', $bus_number)));
						
						// Resize, sharpen, and save the image
						Image::factory($filename)
							->resize(100, 100, Image::WIDTH)
							->save($photo_directory.basename($filename).'.jpg');
							$bus_photo = $photo_directory.basename($filename).'.jpg';
						// Remove the temporary file
						unlink($filename);
						
					}
						
				}
					
					Bus_Model::edit_bus($this->agency_id, $bus_number, $serial_number, $bus_name, $bus_seats, $driver, $motorboy, $vip, $other_infos, $bus_photo);
					
					
					$this->session->set('notice', array('message'=>Kohana::lang('backend.bus_modified'),'type'=>'success'));
			}
			else
			{
				$errors=$post->errors('errors');
				$notice="";
					foreach($errors as $error) 
					{
						$notice.=$error."<br />";
					}
					$this->session->set('notice', array('message'=>$notice,'type'=>'error'));
			}
				
			url::redirect('buses/all');
		}
		
			
		$view=new View('buses/edit_bus');
		$view->bus=$bus;
		$view->current_photo=$current_photo;
		//$view->logo = $logo;
		$this->template->content=$view;		
	}
	
	
	public function delete_bus($bus_number)
	{	
		//DO NOT USE DELETED. SEE QT MANUAL
		Authlite::check_admin();
		Authlite::verify_referer();
		
		$this->template->title='';
		$bus = get::bus($bus_number);
		//$bus->deleted = 1;
		$bus->delete();
		//$bus->save();
		
		$this->session->set('notice', array('message'=>Kohana::lang('backend.bus'). "<b>$bus_number</b>". Kohana::lang('backend.deleted'),'type'=>'success'));
		url::redirect('buses/all');
	}
	
			
}