<?php  defined('SYSPATH') or die('No direct script access');
//Language
$lang=array(
// Home page

	'contact_us_page'=>'<p>

		<p><b>N\'h�sitez pas � nous contacter avec des questions, au sujet de donn�es, de partenariat, de propositions et d\'autres.</b><br/>
		</p>
		
		<p><b>Bureau</b><br/>
		Premier �tage, Ngeke Plaza<br/>
		Clerks Quarters, Buea.<br/>
		CIDR Suite 001, Chambre 2. <br/>
		B.P. 58 Buea.<br/>
		Region du Sud Ouest.<br/>
		Republique du Cameroon. 
		</p>
		
		<p><b>Laboratoire des ing�nieurs </b><br/>
		Razel Quarters,<br/>
		Sandpit, Buea.<br/>
		Region du Sud Ouest.<br/>
		Republique du Cameroon. 
		</p>
		
		<p><b>Telephone</b><br/>
		Fixe: 00237 333-227-27 <br/>
		Mobile: 237 949-943-04, 237 755-981-06<br/>
		</p>

		<p><b>Vous pouvez nous �crire directement �</b><br/>
		info@wasamundi.com, support@wasamundi.com
		</p>		
		</p><br />	',
	
	'our_mission_page'=>'<p>
		<b>Notre Mission</b><br/> 
		Notre mission ultime est d\'aider les gens, d�couvrir et se connecter avec les entreprises locales dans leurs communaut�s.
		
		<p><b>Vous devriez savoir</b></br> 
		Chez Wasamundi, vous pouvez trouver des entreprises, des produits, des services et des personnes au Cameroun.
		Vous pouvez parler des entreprises que vous avez visit�s, y ajouter des photos et les partager sur les r�seaux sociaux.		You can add places, add photos for, talk about and share these on social media.
		Les propri�taires d\'entreprise peuvent contr�ler leurs entreprises.
		Nous servons actuellement les entreprises locales dans plus de 5 r�gions du Cameroun:. Bu�a, Limb�, Yaound�, Bamenda, Douala entre autres.	
		Grace � notre amour pour l\'innovation et l\'exp�rimentation, nous avons pu mettre au point plusieurs produits de premi�re classe et des services pour notre communaut� et le monde.		</p>
		
		<p><b>Nos produits et applications</b><br/>
			<a href="http://wasahostel.com" target="_blank">wasaHOSTEL</a>, une plate-forme en ligne qui simplifie la recherche de logements pour �tudiants.<br/>
			<a href="http://wasatexto.com" target="_blank">wasaTEXTO</a>, lie des personnes � d\'autre personnes et les entreprises aux consommateurs par le biais de messages texte instantan�s mises � jour � travers le monde.<br/>
			<a href="http://wasa.me" target="_blank">wasaME</a>, est un raccourcisseur d\'url qui fournit aux entreprises sur wasamundi, un ID unique de Web simplifi�e.
			Wasamundi construit aussi des sites pour d\'autres entreprises.
		</p>

		
		Wasamundi.com est un produit de Wasamundi, form� en F�vrier 2011.		
		</p><br />	',
		
	'terms_page'=>'
	
			<p>

		<b>Conditions d\'utilisation </b> <p> <br/>
		S\'IL VOUS PLA�T LISEZ ATTENTIVEMENT CES CONDITIONS D\'UTILISATION AVANT D\'UTILISER LE SITE WEB ET SERVICES OFFERTS PAR wasamundi.com. ("WasaMundi"). LE PR�SENT CONTRAT D�FINIT LES CONDITIONS JURIDIQUES ET CONDITIONS D\'UTILISATION DU SITE WEB HTTP :/ / WWW.WASAMUNDI.COM (LE �SITE�) ET TOUS LES SERVICES FOURNIS PAR WasaMundi SUR LE SITE.
		En utilisant le site, de quelque mani�re, y compris mais non limit� � la visite ou la navigation sur le site, vous (�l\'utilisateur� ou �vous�) acceptez d\'�tre li� par cette Accord, y compris les termes et conditions suppl�mentaires et les politiques mentionn�s dans ce document et / ou accessibles par hyperlien. Le pr�sent accord s\'applique � tous les utilisateurs du Site, y compris, sans limitation les utilisateurs qui sont des personnes normales, les utilisateurs, les contributeurs de contenu, de l\'information et d\'autres mat�riaux ou services sur le site. Si vous avez des questions, s\'il vous pla�t  r�f�rez-vous � la section "contactez-nous" du site.
		WasaMundi est un lieu qui permet � des utilisateurs, des entit�s qui se conforment aux politiques WasaMundi � Parcourir, Chat avec les entit�s existantes messages, et les gens dans leur ville ou des villes respectives, o� WasaMundi a index�. WasaMundi vise � pr�senter une nouvelle fa�on de se d�placer autour de votre ville. WasaMundi ne peut pas garantir la v�ritable identit�, l\'�ge et la nationalit� d\'un utilisateur. WasaMundi vous encourage � communiquer directement avec les entit�s � travers les outils disponibles sur le site.
		</p>

		<b>Admissibilit� d\'utilisateur <p>  </b> <br/>
		�ge:
		Les services WasaMundi sont disponibles aux utilisateurs qui ont atteint l\'�ge  d\'utiliser de l\'internet comme sp�cifi� par le pays o� l\'utilisateur se trouve.  Vous d�clarez et garantissez que vous �tes l�galement admissible et que toutes les informations d\'inscription que vous soumettez sont exactes et v�ridiques. WasaMundi peut, � sa seule discr�tion, refuser d\'offrir l\'acc�s pour l\'utilisation du site � toute personne ou entit� et modifier ses crit�res d\'admissibilit� � tout moment. Cette disposition est nul l� o� interdit par la loi et le droit d\'acc�s au site est r�voqu� dans ces juridictions.
		<br/> Conformit�:
		Vous acceptez de vous conformer � toutes les lois locales concernant la conduite en ligne et le contenu acceptable. Vous �tes responsable de toutes les taxes applicables. En outre, vous devez respecter les politiques WasaMundi, comme stipul� dans l\'accord ainsi que toutes les r�gles d\'exploitation, des politiques et des proc�dures qui peuvent �tre publi�s de temps � autre sur le site par WasaMundi, dont chacun est incorpor� ici par r�f�rence et chacun des qui peuvent �tre mis � jour par WasaMundi de temps � autre sans pr�avis:
		<br/> Politique de confidentialit�:
		En outre, certains services offerts par le site peuvent �tre soumis � des termes et conditions �dict�es par WasaMundi de temps en temps; votre utilisation de ces services est soumise aux termes et conditions suppl�mentaires, qui sont incorpor�s dans le pr�sent contrat par r�f�rence.
		<br/> Mot de passe:
		Gardez votre mot de passe s�curis�. Vous �tes enti�rement responsable de toute activit�, de responsabilit� et de dommages r�sultant de votre incapacit� � maintenir la confidentialit� de votre mot de passe. Vous vous engagez � informer imm�diatement WasaMundi de toute utilisation non autoris�e de votre mot de passe ou toute autre violation de la s�curit�. Vous acceptez �galement que WasaMundi ne peut pas et ne sera pas responsable de toute perte ou dommage survenant en cas de manquement de garder votre mot de passe s�curis�.
		<br/> Informations sur le compte:
		Vous acceptez de ne pas fournir votre nom d\'utilisateur et mot de passe en combinaison pour toute partie autre que WasaMundi sans  l\'autorisation expresse �crite de WasaMundi. Entit�s, les utilisateurs ont le droit de conserver leurs informations de compte mises � jour et exacts en tout temps, y compris une adresse �lectronique valide.
		<br/> Droit de refuser un service:
		Les services d\'enregistrement d\'utilisateur ne sont pas disponibles pour des utilisateurs temporairement ou ind�finiment suspendus. WasaMundi r�serve le droit d\'interdire les utilisateurs qui ont �t� signal�s comme des nuisances ou des spammeurs. WasaMundi r�serve le droit de refuser le service � quiconque, pour quelque raison, � tout moment.
		</p>

		<b>Contr�le d\'information  </b><p>  <br/>
		WasaMundi contr�le le contenu fourni par les utilisateurs qui est mis � disposition sur WasaMundi. WasaMundi r�serve le droit, apr�s enqu�te, de supprimer tout ce qui est offensant pour le public, y compris les examens battant, d�clar�es anti-spam. Tous les commentaires qui sont marqu�s jusqu\'� 24 heures avant que l\'action appropri�e soit prise. S\'il vous pla�t faites attention, en surfant WasaMundi. Alors que nous essayons de notre mieux pour �viter les erreurs, nous ne pouvons pas �tre tenus responsables pour toute information obsol�te ou erron�e sur ce site. WasaMundi va faire de son mieux de v�rifier les entit�s avant de les confirmer formel.
		</p>

		<p> <b> confidentialit� </b> <br/>
		Sous r�serve des dispositions WasaMundi WasaMundi politique de confidentialit� de ne pas vendre ou divulguer vos renseignements personnels (tels que d�finis dans la Politique de protection des renseignements personnels) � des tiers sans votre consentement explicite. WasaMundi magasins et contenus processus sur des ordinateurs situ�s aux �tats-Unis qui sont prot�g�s par la s�curit� physique ainsi que technologique.
		</p>

		<p> <b> Aucune garantie </b> <br/>
		WasaMundi ne garantit pas l\'acc�s continu au site, et le fonctionnement du site peut �tre affect� par de nombreux facteurs qui �chappent au contr�le de WasaMundi.
		</p>

		<p> <b> Divisibilit� </b> <br/>
		Si une disposition du pr�sent contrat est jug�e non ex�cutoire, la disposition sera modifi�e afin de refl�ter l\'intention des parties. Toutes les autres dispositions du pr�sent accord resteront en vigueur et de plein effet. WasaMundi r�serve le droit de modifier ou de r�silier le service WasaMundi pour une raison quelconque, sans pr�avis, � tout moment. WasaMundi r�serve le droit de modifier les pr�sentes conditions d\'utilisation du site ou les politiques d\'autres � tout moment, donc veuillez examiner les politiques fr�quemment. Si WasaMundi fait un changement important WasaMundi vous en informerons ici, par courriel, au moyen d\'un avis sur notre page d\'accueil, ou d\'autres lieux Wasamundi juge appropri�. Ce qui constitue un �changement important� sera d�termin� � l\'unique discr�tion de WasaMundi , de bonne foi et de bon sens et de jugement raisonnable.
		</p>

		Choix <p> de droit </b> <br/>
		Le pr�sent accord est interpr�t� avec et par les lois de la R�publique du Cameroun.
		</p>

		<p> <b> Avis </b> <br/>
		Sauf stipulation expresse contraire, tout avis doit �tre donn� par courrier �lectronique � l\'adresse e-mail que vous fournissez � WasaMundi (soit pendant le processus d\'inscription ou lorsque vous changez d\'adresse e-mail ou d\'autres circonstances en demandes de service). L\'avis sera r�put� 24 heures apr�s son envoi, sauf si l\'exp�diteur est inform� que l\'adresse e-mail n\'est pas valide.
		</p>

		<p> <b> Divers </b> <br/>
		S\'il ya un litige concernant ou impliquant le Site et / ou du Service, ces modalit�s de service de WasaMundi, vous acceptez que tout litige sera r�gi par les lois de la R�publique du Cameroun, sans �gard � son conflit de dispositions l�gales
		Les conditions d\'utilisation constituent l\'accord complet entre vous et WasaMundi concernant l\'utilisation du site, et remplacent toute entente ant�rieure entre vous et WasaMundi sur un tel sujet. Les parties reconnaissent que personne ne se fie sur toute repr�sentation faite, mais ne figurent pas express�ment dans les pr�sentes Conditions d\'utilisation.
		L\'�chec de WasaMundi � exercer ou � appliquer tout droit ou disposition des pr�sentes Conditions d\'utilisation ne constitue pas une renonciation � ce droit ou � cette disposition. L\'�chec de l\'une des parties � exercer en aucune fa�on un droit pr�vu aux pr�sentes ne peut �tre interpr�t� comme une renonciation de tout autre droit en vertu des pr�sentes.
		Si une disposition des Conditions d\'utilisation est jug�e inapplicable ou invalide, cette disposition sera limit�e ou �limin�e dans la mesure minimale n�cessaire pour que les conditions d\'utilisation demeureront en vigueur et de plein effet et force ex�cutoire.
		Les conditions d\'utilisation ne sont pas cessibles, transmissibles ou sous-licenciable par vous, sauf avec le consentement �crit pr�alable de WasaMundi, mais peuvent �tre c�d�es ou transf�r�es par WasaMundi � une tierce partie sans restriction et sans pr�avis. Toute tentative de cession d\'�tre faite en violation des Conditions d\'utilisation est nul.
		Titre d\'une clause des pr�sentes conditions d\'utilisation sont fournis � titre indicatif et n\'ont aucun effet l�gal ou contractuel.
		</p>
			
		</p>	
		<br />
	',
	
	'privacy_page'=>'
	
		<p>

		<p> Ce que cette politique de confidentialit� traite </ b> <br/>
		Cette politique de confidentialit� traite l\'utilisation du site web http://www.wasamundi.com et les services fournis par wasaMundi.com. ("WasaMundi�), y compris la fa�on dont WasaMundi traite les renseignements personnels que recueilles et re�oits WasaMundi. Conform�ment aux modalit�s d\'utilisation de WasaMundi, vous devez �tre admissible par pays (ou �l�galement admissible�) pour utiliser ce site ou avoir l\'autorisation d\'un tuteur l�gal .. Cette politique ne s\'applique pas aux pratiques des tierces parties que WasaMundi ne poss�de ni ne contr�le, ou des personnes que WasaMundi n\'emploie pas ou ne g�rent pas.
		Entr�e en vigueur: 10/10/2010.
		Collection des don�es et comment il est utilis�
		</ P>

		<p>Informations g�n�ral: </ b> <br/>
		WasaMundi ne vendra ni ne divulguera votre nom ou votre adresse email � des tiers sans votre consentement explicite, sauf comme indiqu� dans cette politique de confidentialit�. Votre nom d\'utilisateur, ou pseudonyme est affich� tout au long de WasaMundi (et donc � la disposition du public) et est reli� � l\'ensemble de votre activit� WasaMundi. D\'autres personnes peuvent voir vos commentaires, notations et commentaires associ�s. WasaMundi recueille des renseignements personnels lorsque vous vous inscrivez pour utiliser les services WasaMundi ou de publier du contenu sur WasaMundi. Lors de l\'inscription vous est demand� de fournir votre adresse courriel et certaines autres informations facultatives. Si vous souhaitez obtenir un compte v�rifi�, WasaMundi vous demandera de fournir votre adresse entit�. WasaMundi re�oit et enregistre automatiquement des informations sur nos serveurs � partir de votre navigateur, y compris votre adresse IP, les cookies et les pages que vous demandez. Ces donn�es ne sont utilis�es que dans l\'ensemble.
		Annonces li�es au service:
		WasaMundi vous enverra un email de bienvenue / confirmation lorsque vous vous inscrivez � des services de WasaMundi. La r�ception de cette e-mail est n�cessaire pour utiliser les services de WasaMundi. WasaMundi vous enverras des annonces li�es au service de rares occasions o� il est n�cessaire de le faire. WasaMundi vous enverra �galement des entit�s qui ne sont pas encore index�s sur les notifications par courrier �lectronique venue lorsqu\'un utilisateur tente de laisser un message � l\'entit� sur le site. Les administratifs de WasaMundi messages peuvent vous envoyer p�riodiquement des messages administratifs. WasaMundi ne vendra ou louer vos informations personnelles � des tiers � des fins de marketing sans votre consentement explicite. Vos informations peuvent �tre combin�es avec des informations WasaMundi recueille aupr�s d\'autres entreprises et utilis�s pour am�liorer et personnaliser les services, contenus et publicit�s. Si vous ne souhaitez pas recevoir de messages administratifs ou de participer � ces programmes, il suffit de refuser de se d�sabonner.
		</ P>

		<p>Choix  / Opt-out </ b> <br/>
		Si vous ne souhaitez plus recevoir de messages d\'administration, vous pouvez choisir de vous d�sinscrire. S\'il vous pla�t notez que si vous ne souhaitez pas recevoir des messages administratifs de nous, ces avis seront toujours r�gissent votre utilisation du site Web WasaMundi et de service, et vous �tes responsable de l\'examen de ces avis pour des changements.
		</ P>

		<p> <b> Chat </ b> <br/>
		Si vous utilisez le service de chat sur ce site, vous devez �tre conscient que toute information personnelle identifiable que vous soumettez peut �tre lue, recueillie ou utilis�e par d\'autres personnes, ou qui pourraient �tre utilis�es pour vous envoyer des messages non sollicit�s. WasaMundi n\'est pas responsable pour des informations personnellement identifiables que vous choisissez de soumettre ici.
		Modification et suppression des informations de compte
		Vous pouvez modifier vos informations de compte Wasamundi (� l\'exception de votre nom d\'utilisateur ou e-mail), � tout moment. Wasamundi peut vous envoyer certaines communications relatives au service Wasamundi comme annonces li�es au service et des messages administratifs qui sont consid�r�s comme faisant partie de votre compte Wasamundi. Si vos renseignements personnels changent personnellement identifiables vous pouvez corriger ou mettre � jour en effectuant le changement sur votre profil Wasamundi.
		</p>

		<p> <b> S�curit� </ b> <br/>
		La s�curit� de vos renseignements personnels est importante pour nous. Vos informations de compte WasaMundi sont prot�g�s par un mot de passe pour votre vie priv�e et votre s�curit�. Vous avez besoin de travailler pour vous prot�ger contre l\'acc�s non autoris� � votre mot de passe et � votre ordinateur en vous connectant au loin une fois que vous avez fini d\'utiliser un ordinateur partag�. WasaMundi suit les normes reconnues dans l\'industrie pour prot�ger les renseignements personnels qui nous sont soumis, � la fois lors de la transmission et une fois WasaMundi qu\'il re�oit. Aucune m�thode de transmission sur l\'internet, ou m�thode de stockage �lectronique n\'est s�r � 100%. Par cons�quent, si WasaMundi s\'efforce d\'utiliser des moyens commercialement acceptables pour prot�ger vos renseignements personnels, WasaMundi ne peut pas garantir la s�curit� absolue. Si vous avez des questions concernant la s�curit� sur le site WasaMundi, vous pouvez envoyer un courriel � support@wasamundi.com.
		</p>

		<p><b> Modifications � cette politique de confidentialit� </ b> <br/>
		Si la politique de confidentialit� change, WasaMundi affichera ces modifications afin que vous soyez au courant de ces changements. WasaMundi r�serve le droit de modifier cette d�claration de confidentialit� � tout moment, donc s\'il vous pla�t consulter-le r�guli�rement. Si WasaMundi apporte des changements importants � cette politique, vous serez notifi� ici, par courriel, ou d\'autres lieux que WasaMundi juge appropri�.
		</p>

		Questions <p> <b> </ b> <br/>
		Si vous avez des questions ou suggestions, vous pouvez contacter l\'�quipe de soutien WasaMundi via notre aide ou contactez-nous ou �crivez support@wasamundi.com.
		</p>

	',
	
);
