<?php defined('SYSPATH') or die('No direct script access');

class Town_Model extends ORM{

	public function get_all(){
		$towns=ORM::factory('town')->orderby('name','asc')->find_all();
		return $towns;
	}
	
	public function get_all_excluding_town($town_id_to_exclude){
		$towns=ORM::factory('town')->where('id !=',$town_id_to_exclude)->orderby('name','asc')->find_all();
		return $towns;
	}
	
	
	
	public function get_town($id){
		$db=new Database();
		$town=$this->db->select('name')->from('towns')->where('id',$id)->limit(1)->get();
		return $town->current()->name;
	}
	
	public function get_agency_town_name($agency_id)
	{	$agency_town_name = $this->db->select('towns.name')->from('towns')->join('agencies','agencies.town_id = towns.id')->where('agencies.id',$agency_id)->get();
		return $agency_town_name->current()->name;
	}
	
	public function get_agency_town_id($agency_id)
	{	$agency_town_name = $this->db->select('towns.id')->from('towns')->join('agencies','agencies.town_id = towns.id')->where('agencies.id',$agency_id)->get();
		return $agency_town_name->current()->id;
		
	}


}
