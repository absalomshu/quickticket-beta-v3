<?php defined('SYSPATH') or die('No direct script access'); ?> 
	
	<div class="notice">
		<?php
			//determine what type of notice to display if at all
			$notice = $this->session->get_once('notice');
				if(!empty($notice)){ 
					if($notice['type'] == 'success'){?><div class="alert general success"><a class="close" data-dismiss="alert" href="#">&times;</a><?php echo $notice['message']; ?></div><?php }?>
				<?	if($notice['type'] == 'error'){?><div class="alert general error"><a class="close" data-dismiss="alert" href="#">&times;</a><?php echo $notice['message'];?></div><?php }?>
		<?}?>
	</div>
 
	<div class="row-fluid " >
	  
						
	  	<div class="span12 hero-unit prof-unit" >
				<ul class="nav nav-tabs">
					<li > <a href="<?=url::site('buses/all')?>"><i class="icon-arrow-left"></i> <?=Kohana::lang('backend.back_buses')?></a></li>
				</ul>
				<legend><?=Kohana::lang('backend.maintenance')?>: <?= $bus->bus_number?></legend>
				<form action="<?=url::site('buses/maintain/'.$bus->bus_number)?>" method="POST" class="span7">
				
				<legend><?=Kohana::lang('backend.documents')?></legend>
				
						<table class="table no-border" >
						<tr>
							<th>&nbsp;</th>
							<th>Available</th>
							<th>Expiry date</th>
							<th></th>
						</tr>
						<?php foreach($bus_requirements as $requirement)
						{ 
						//Check if the status has been registered in the status table. If not, register it.
							$should_be_checked = 0;
							$requirement_status = Bus_Requirements_Status_Model::get_requirement_status($this->agency_id, $requirement->id, $bus->bus_number);
							//var_dump($requirement_status->ExpiryDate);exit;
							//Running a direct $requirement_status->count_all() gives erroneous result. Hence the complete ORM.
							$requirement_status_exists = ORM::factory('bus_requirements_status')->where('BusNumber',$bus->bus_number)->where('BusRequirementID',$requirement->id)->count_all();
							
							//if it exists, check its value. if it's 1, the checkbox should be checked
							if($requirement_status_exists)
							{	//If exists, just verify whether the box should be checked or not
								if($requirement_status->IsDone == '1'){$should_be_checked = 1;}
							}else
							{	
								Bus_Requirements_Status_Model::add_status($this->agency_id, $bus->bus_number, $requirement->id, $bus->bus_number, 0);	
							} 
						?>
						
						<tr style="border-color:white;">
							<td class="span3 text-right"><?=$requirement->Name?>: </td>
							<td class="span1">
								<!--So if unchecked, the value is 0-->
								<input type="hidden" name="all_requirements[<?=$requirement->id?>]" value="0" />
								<!--<label class="checkbox"><input type="checkbox" name="all_requirements[<?=$requirement->id?>]" value="1" <?php if($should_be_checked){echo 'checked = "checked"';} ?>/></label>
								-->
								<select name="all_requirements[<?=$requirement->id?>]">
										<!--No is put before yes so the default is no-->
											<option value="0">No</option>
											<option value="1"  <?php if($should_be_checked){echo 'selected';}?> >Yes</option>
											
											
								</select>
							
							
							</td>
							<td class="span4">
								<div class="input-append date span6">
									<input type="text" name = 'expiry_dates[<?=$requirement->id?>]' class=" datepicker"  value="<?php if (($requirement_status->ExpiryDate)){echo date('d-m-Y',strtotime($requirement_status->ExpiryDate));}?>" id="dp1" >
									<span class="add-on"><i class="icon-calendar"></i></span>
								</div>	
							</td>
							
							<td class="span4"><?php 
								//If the date is set, and the doc is expiring, indicate
								if ($requirement_status->ExpiryDate){
									$time_left = time() - strtotime($requirement_status->ExpiryDate);
									//If it's less than one week left
									if($time_left < 604800){
										$days_left = ceil(-1*($time_left/86400));
										
										if($days_left < 0){ ?>
										<span class="label label-important">EXPIRED</span>
										<?php }
										elseif($days_left >= 1  AND $days_left <= 7){ ?>
											<span class="label label-warning"> Expires in <?=$days_left ?> days</span>
										<?php }
										//else{
										//	echo "Expires in ".$days_left." days ";
										//}
									}else{ //Expired more than 1 week ago ?>
										<span class="label label-important">EXPIRED</span>
									<?php }
								}
								
							
							?></td>
						</tr>
						<?php } ?>
						</table>
						
									
					
						<div class="clear"></div>
						<div class="clear"><br/><br/></div>
						
						<div class="">
							<div class="span4"> </div>
							<div class="form-actions">
								<button class="btn btn-info" type="submit"> <?=Kohana::lang('backend.save')?></button>
								<a href="<?=url::site('buses/all')?>" class="btn" type="submit"> <?=Kohana::lang('backend.cancel')?></a>
							</div>
						</div>
						
									
				
						
				</form>
				<form action="<?=url::site('settings/add_bus_requirement')?>" method="POST" class="span4" >	
				<legend><?=Kohana::lang('backend.reminders')?></legend>				
					   <div class="">
							
							<table class="table table-striped table-hover">
								<!--<tr>
									<th>Task</th>						
									<th><?=Kohana::lang('backend.actions')?></th>						
								</tr>-->
								<?php foreach ($reminders_for_bus as $reminder):?>
									<tr>	
										<td><i class="icon-file"></i>
										<a href="<?=url::site('reminders/view_reminder/'.$reminder->id)?>"> <?=$reminder->Name?></a></td>
										<!--<td><a href="<?=url::site('reminders/delete_reminder/'.$reminder->id)?>" onClick="return confirm('<?=Kohana::lang('backend.confirm_delete')?>?');" class="btn"><i class="icon-remove">Delete</i></a></td>
										-->
										<td>
											<div class="btn-group ">
											  <a class="btn dropdown-toggle btn-mini" data-toggle="dropdown" href="#" >
												-- <?=Kohana::lang('backend.select')?> --
												<span class="caret"></span>
											  </a>
											  <ul class="dropdown-menu">
												<li><a href="<?=url::site('reminders/view_reminder/'.$reminder->id)?>"> View details</a></li>
												
												<li><a href="<?=url::site('reminders/delete_reminder/'.$reminder->id)?>" onClick="return confirm('<?=Kohana::lang('backend.confirm_delete')?>?');">Delete reminder</a></li>

											  </ul>
										</div>
										</td>
									
									
									
									
									
									
									
									</tr>
								<?php endforeach; ?>
							</table>
						
						</div>
				</form>
			<div  class="span7">
				
				<legend><?=Kohana::lang('backend.availability')?></legend>				
					<?php if ($bus->is_available){ ?>
								<form action="<?=url::site('buses/make_unavailable/'.$bus->bus_number)?>" method="GET" >					
									<div class="">				
										<div class="span4 text-right">Status:</div>				
										<div class="span8 "><b>Available</b></div>				
									</div>
									<div class="">				
										<div class="span4 text-right heading">Make unavailable</div>				
										<div class="span8 ">&nbsp;</div>				
									</div>		
									<div class="">
										<div class="span4 text-right"><?=Kohana::lang('backend.reason')?>: </div>
										<div class="span8"><textarea placeholder=""  name="reason" style=""></textarea></div>
									</div>			
											
											
								<div class="clear"></div>			
								<div class="">
									<div class="form-actions">
																
										<div class="span4"></div>
										<div class="span8">
											<button class="btn btn-info" type="submit"> <?=Kohana::lang('backend.make_unavailable')?></button>
											<!--<a href="<?=url::site('buses/make_unavailable/'.$bus->bus_number)?>"> <?=Kohana::lang('backend.make_unavailable')?></a>
									-->
											<a href="<?=url::site('buses/all')?>" class="btn" type="submit"> <?=Kohana::lang('backend.cancel')?></a>
										</div>
									</div>
								</div>			
											
								</form>			
											<?php 
											
											
											
											
											}else{?>
											<div class="">				
												<div class="span4 text-right">Status:</div>				
												<div class="span8 "><b>Unavailable</b></div>				
											</div>
											<div class="">				
												<div class="span4 text-right">Reason:</div>				
												<div class="span8 "><?=Bus_inavailability_detail_Model::get_current($this->agency_id,$bus->bus_number)->reason?></div>				
											</div>
														
								<div class="clear"></div>			
								<div class="">
									<div class="span4"></div>
									<div class="form-actions">
											<a href="<?=url::site('buses/make_available/'.$bus->bus_number)?>" class="btn btn-info"> <?=Kohana::lang('backend.make_available')?></a>
											<a href="<?=url::site('buses/all')?>" class="btn" type="submit"> <?=Kohana::lang('backend.cancel')?></a>
										
									</div>
								</div>	
											
											
											
											
											
												<?php }?>
					
					
					
						
								
				
				
				
				
			</div>
				
			  					
			
		
			  
		</div>
		
		</div>
	  <div class="clear"></div>

<script type="text/javascript">
	$(document).ready(function(){
			$('#timepicker').timepicker({
				minuteStep:30,
				//defaultTime: '8:00 PM'
			});
			
			
			$('.input-append.date').datepicker({
				format: "dd-mm-yyyy",
				todayBtn: "linked",
				autoclose: true,
				todayHighlight: true
			});
			
			
				$(function () {
				$('#myTab a').click(function (e) {
					  e.preventDefault();
					  $(this).tab('show');  
			})
			  })
			  
		
			  
			
	})
	</script>
