<?php defined('SYSPATH') or die('No direct script access'); ?> 


	<!--
			For each input element, we check if it's a manager
			i.e. if it's the 'control' function. 
			If so, disable the input fields so they can't change anything. For now.		
		-->
	<?php //$manager = ((substr($_SERVER['PATH_INFO'],7,7))=='control') ? 1:0;
			//print_r($manager);exit;
	?>
    <div class="container">
		
	  
	  <div class="row-fluid marketing" style="margin:5px 0 0px 0px;">
	  
	  	<div class="span12 hero-unit prof-unit" >
		<div class="heading"><?=Kohana::lang('backend.schedule_for')?> <?=$schedule->bus_number?>
			<span style="float:right;font-size:12px;font-weight:normal;">
				<?php if ($schedule->loading == 0){ ?>
					<button class="btn plain"  onClick="startloading(<?=$schedule->id?>)" type="submit" id="start-loading" style="color:red;" disabled>
					<i class="icon-bullhorn"></i><?=Kohana::lang('backend.start_loading')?>
					</button>
				<?php  ;} else {?>
					<button class="btn disabled"   type="submit" id="start-loading" style="height:30px; width:110px;font-size:12px;float:right;font-style:italic;color:green;" >
						<?=Kohana::lang('backend.loading')?>...
					</button>
				<?php } ?>
				<!--<a href="<?=url::site('admin/edit_schedule').'/'.$schedule->id?>"> Edit </a> &nbsp;   |  &nbsp;  -->
			</span>
		</div>
			<div class="rule"><hr/></div>
			<div class="span2 spacious">
				<div class="control-group">
				<label class="control-label lab" for="inputType"><?=Kohana::lang('backend.from')?>:</label>
					<div class="controls ">
						<h3><?=get::town($schedule->from);?></h3>
					</div>
				</div>
			</div>
			<div class="span2 spacious">
				<div class="control-group">
				<label class="control-label lab" for="inputType"><?=Kohana::lang('backend.to')?>:</label>
				<div class="controls ">	
				<h3><?=get::town($schedule->to);?></h3>
				</div>
					</div>
				</div>
			<div class="span2 spacious">
				<label class="control-label lab" for="inputType"><?=Kohana::lang('backend.time')?>:</label>
					<div class="controls ">
					<h3><?=$schedule->departure_time;?></h3>
					</div>
			</div>
			<div class="span2 spacious">
				<label class="control-label lab" for="inputType">Date:</label>
				<div class="controls ">
									<h3><?=$schedule->departure_date;?></h3>
				</div>
			</div>
			
			<div class="span2 spacious" >
				<div class="control-group">
				<label class="control-label lab" for="inputType"><?=Kohana::lang('backend.bus_type')?>:</label>
						<div class="controls ">	
							<h3><?=$schedule->bus_seats?> <?=Kohana::lang('backend.seater')?></h3>
						</div>	

				</div>
			</div>
			
			<div class="span2 spacious">
				<label class="control-label lab" for="inputType"><?=Kohana::lang('backend.bus_no')?>:</label>
					<div class="controls ">
						<h3><?=$schedule->bus_number?></h3>
					</div>
			</div>
			<div class="span2 spacious">
				<label class="control-label lab" for="inputType"><?=Kohana::lang('backend.ticket_price')?> (FCFA):</label>
					<div class="controls ">	
						<h3><?=$schedule->ticket_price?>FCFA</h3>
					</div>
			</div>
			<div class="span2 spacious">
				<!--<label class="control-label lab" for="inputType">Done?</label>-->
					<div class="controls ">	
						<!--<a href="#" onClick="checkout(<?php // echo $schedule->id?>)">Check out</a>-->
						<!--<a href="<?=url::site('admin/checkout_schedule/'.$schedule->id)?>" ><i class="icon-road"></i> Check out</a>-->
					</div>
			</div>
			
		
			
			  
		</div>
	<?php
		//determine what type of notice to display if at all
		$notice = $this->session->get_once('notice');
			if(!empty($notice)){ 
				if($notice['type'] == 'success'){?><div class="alert general success"><a class="close" data-dismiss="alert" href="#">&times;</a><?php echo $notice['message']; }?></div>
			<?	if($notice['type'] == 'error'){?><div class="alert general error"><a class="close" data-dismiss="alert" href="#">&times;</a><?php echo $notice['message']; }?></div>
		<?}?>	
	<div class="row-fluid marketing" style="margin:5px 0 0px 0;">	
		
		
	
		
		<div class="span8 hero-unit prof-unit" style="margin-left:0px;" >
		<form action="<?=url::site('admin/complete_schedule/'.$schedule->id)?>" method="POST">
		<?php no_csrf::set(1); ?>
			<div class="heading"><?=Kohana::lang('backend.checkout_seat')?></div>
			<div class="rule"><hr/></div>
				
				<div class="span3 spacious">
					<div class="control-group">
					<label class="control-label lab" for="inputType"><?=Kohana::lang('backend.client_name')?>:</label>
						<div class="controls ">
							<input type="text"  name="client_name" disabled>
						</div>
					</div>
				</div>
				<div class="span3 spacious">
					<div class="control-group">
					<label class="control-label lab" for="inputType"><?=Kohana::lang('backend.client_phone')?>:</label>
						<div class="controls ">
							<input type="text"  name="client_phone" disabled>
						</div>
					</div>
				</div>
				<div class="span3 spacious">
					<div class="control-group">
					<label class="control-label lab" for="inputType"><?=Kohana::lang('backend.client_email')?>:</label>
						<div class="controls ">
							<input type="text"  name="client_email" disabled>
						</div>
					</div>
				</div>
			<div class="span3 spacious" >
				<div class="control-group">
				<label class="control-label lab" for="inputType"><?=Kohana::lang('backend.seats_av')?>:</label>
					<div class="short-dropdown">
						<select name="seat_number" id="inputType" disabled>
						<?php 
						//array to hold all occupied seats for highlighting  on bus plan
						echo "<script> var jsar = new Array();</script>";
						$number=$schedule->bus_seats;
						
						//start from 2 since seat 1 is the driver
						$i=2;
						for($i; $i<= $number; $i++){
						//make sure the seat is not already occupied
						if(!isset($seats_and_occupants[$i])  AND !isset($seats_and_reservers[$i]) )
						{
						?>
							<option value="<?=$i?>"><?=$i?></option>
						<?php 
						}if(isset($seats_and_occupants[$i])) 
						{
							echo "<script> jsar[".$i."] = ".$i.";</script>";
						}
						//$i++;
						}
						?>
	
						</select>
					</div>
				</div>
			</div>
			<div class="span3" >
				<div class="control-group">
				<label class="control-label lab" for="inputType">&nbsp;</label>
					<div class="short-dropdown">
						<!--<button class="btn btn-info" type="submit" name="check_out_form" style="height:30px; width:130px;font-size:20px;margin-top:-5px;" ><i class="icon-check"></i>Check out</button><br/>
						-->	
					</div>
				</div>
			</div>
			
			
			
			<?php 
			//print_r($seats_and_occupants);exit;
			if (!empty($seats_and_occupants) OR !empty($seats_and_reservers))
			{?>
			<div class="span12"><div class="rule"><hr/></div></div>
				<div class="span8" >
					<div class="control-group">
						<label class="control-label lab" for="inputType"><?=Kohana::lang('backend.taken_seats')?></label>
						<?php 
						//combine reserved and taken
						//COMBINING INEFFICIENT AS IT IS A 2D AND A 3D
						//$all_seats = $seats_and_reservers + $seats_and_occupants;
						//ksort($all_seats);
						//foreach($seats_and_reservers as $key=>$subkey){print_r($subkey['name']);echo"<br/>";}?>
							
					</div>
				</div>
			<?php }?>	
			</form>
			
		<form action="<?=url::site('admin/complete_schedule/'.$schedule->id)?>" method="POST">
			<?php no_csrf::set(2); ?>
			<table class="table table-bordered">
							<tr>
									<th><?=Kohana::lang('backend.seat_no')?></th>
									<th><?=Kohana::lang('backend.occupant')?></th>
									<th><?=Kohana::lang('backend.phone')?></th>
									<th><?=Kohana::lang('backend.state')?></th>
							</tr>
							<?php 
							//sort the array in ascending order
							ksort($seats_and_occupants);
							foreach ($seats_and_occupants as $key=>$value)
							{ ?>
									<tr>
										<td><?=$key?></td>
										<td><?=$value['name']?></td>
										<td><?=$value['phone']?></td>
										<td></td>
									</tr>
								
								
								
							<?php } ;?><?php 
							//sort the array in ascending order
							ksort($seats_and_reservers);
							foreach ($seats_and_reservers as $key=>$value)
							{ ?>
									<tr>
										<td><?=$key?></td>
										<td><?=$value['name']?></td>
										<td><?=$value['phone']?></td>
										<?php $serial = serialize($seats_and_reservers);
										//var_dump($serial);exit;
											?>
											<input type="hidden" value="<?php echo htmlentities($serial)?>" name="arr"/>
											<td colspan="2"><?=Kohana::lang('backend.reserved')?> &nbsp;&nbsp;&nbsp;
											<input type="submit" id="accept"name="res_to_occ" value="" title="Confirm" disabled /> | 
											<input type="submit" id="reject" name="reject_reserve" value="" title="Delete" disabled />
											</td>
									</tr>
								
								
								
							<?php } ;?>
						</table>
					</form>
		</div>
		<div class="span4 hero-unit prof-unit"  >
				<div class="heading"><?=Kohana::lang('backend.bus_plan')?></div>
				<div class="rule"><hr/></div>
				<?php $number=$schedule->bus_seats;
		//check number of seats to know which bus plan to display
		if ($number == 70){include 'application/views/bus_plans/70_seater.php';}
		elseif ($number == 55){include 'application/views/bus_plans/55_seater.php';}
		elseif ($number == 30){include 'application/views/bus_plans/30_seater.php';}
				?>
			
				
		</div>
	
		<div class="span8 hero-unit prof-unit" style="margin-left:0px; " >
		<form action="<?=url::site('admin/complete_schedule/'.$schedule->id)?>" method="POST">
		<?php no_csrf::set(3); ?>
			<div class="heading"<?=Kohana::lang('backend.reserve_seat')?><span style="float:right;font-size:12px;font-weight:normal;"></span></div>
			<div class="rule"><hr/></div>
				
				<div class="span3 spacious">
					<div class="control-group">
					<label class="control-label lab" for="inputType"><?=Kohana::lang('backend.name')?>:</label>
						<div class="controls ">
							<input type="text"  name="res_name" disabled>
						</div>
					</div>
				</div>
				<div class="span3 spacious">
					<div class="control-group">
					<label class="control-label lab" for="inputType"><?=Kohana::lang('backend.phone')?>:</label>
						<div class="controls ">
							<input type="text"  name="res_phone" disabled>
						</div>
					</div>
				</div>
				<div class="span3 spacious">
					<div class="control-group">
					<label class="control-label lab" for="inputType"><?=Kohana::lang('backend.email')?>:</label>
						<div class="controls ">
							<input type="text"  name="res_email" disabled>
						</div>
					</div>
				</div>
				
				<?php //print_r($seats_and_occupants);exit;?>
				<div class="span3 spacious" >
				<div class="control-group">
				<label class="control-label lab" for="inputType"><?=Kohana::lang('backend.seats_av')?>:</label>
					<div class="short-dropdown">
						<select name="res_seat" id="inputType" disabled >
						<?php $number=$schedule->bus_seats;
						$i=2;
						for($i; $i<= $number; $i){
						//make sure the seat is not already occupied and not reserved either.
						
						if(!isset($seats_and_occupants[$i])  AND !isset($seats_and_reservers[$i]) )
						{
						?>
							<option value="<?=$i?>"><?=$i?></option>
						<?php 
						}
						$i++;
						}
						?>
	
						</select>
					</div>
				</div>
			</div>
				<div class="span3 spacious">
					<div class="control-group">
					<label class="control-label lab" for="inputType">&nbsp;</label>
							<!--<button type="submit" name="reserve_seat_form" class="btn btn-info">Reserve</button>-->
					</div>
				</div>
					

		</form>		
				
		</div>
			<div class="span4 hero-unit prof-unit">
				<div class="heading"><?=Kohana::lang('backend.res_requests')?></div>
				<div class="rule"><hr/></div>
				<?php if (!empty ($client_reservations)){
				
					foreach($client_reservations as $seat => $details){
					
				?>
					<div class="client-res"><?=$details['name']?> | <?=$details['phone']?> | <?=Kohana::lang('backend.seat')?> <?=$seat?>  </div>
					<!--<button type="submit">Reserve</button><button type="submit">Checkout</button><button type="submit">Reject</button>-->
					<div class="rule"><hr/></div>
				<?php	
					}
					}else{ echo "No client reservation request for the moment.";}
				?>
				
			
				
		</div>	
		</div>
	  </div>
	  <div style="height:19px;"></div>
	  

	  <script type="text/javascript"> 
		$(document).ready(function(){
			$('#start-loading').click(function(){
				
			});
		});
		
		function startloading(schedule_id){
			var domain = 'http://localhost/quickticket/';
			//alert(schedule_id);
			$.ajax ({
				type: 'POST',
				url: domain+'admin/start_loading/'+schedule_id,
				data: 'schedule_id='+schedule_id,
				success: function()
				{
				$('#start-loading').css({"color":"green","font-style":"italic"}).addClass('disabled').html('Loading...');
				//$("#return").html(' '+html+'');
				
				}
			
			});		
		}



	</script>
	<script type="text/javascript">
	$(document).ready(function(){
		$('.datepicker').datepicker();
	})
	function checkout(schedule_id)
	{	
		var domain = 'http://localhost/quickticket/';
		var answer = confirm('Are you sure you want to check this bus out?');
		if (answer == true)
		{
			//for now, reload the page. eventually, reload just the two divs involved
			//NB even though reload comes before action, action is executed b4 page finishes reloading
			//location.reload();
			
			
			$.ajax({
			type: 'POST',
			url: domain+'admin/checkout_schedule/'+schedule_id,
			data: 'schedule_id='+schedule_id,
			cache: false,
			sucess: function()
				{
				
				//$("#return").html(' '+html+'');
				//window.location = "http://localhost/quickticket/admin/past_schedule/"+schedule_id
				}
			});
		
		}
		else{
			return false;}
		
	};
	$(document).ready(function(){
	
	});




</script>
<script type="text/javascript">
	$(document).ready(function(){
		var number_seats = "<?php echo $number; ?>";
		for (i=0; i <= number_seats; i++){
		if( (i!=1) && typeof jsar[i] != 'undefined'  ){
			$("td.plan:eq("+(i-1)+")").addClass('taken-seat');
			}
			} 		
	})
</script>
    </div> 

