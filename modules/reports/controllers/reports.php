<?php defined('SYSPATH') or die('No direct script access');

//set default timezone, otherwise it's UTC which is an hour late
date_default_timezone_set('Africa/Douala');

//Almost all functions will take as parameter, the id of the agency we are dealing with
//in order not to interfere with another's stuff.

class Reports_Controller extends Admin_Controller {


	
	public function index() 
	{	Authlite::check_admin();
		//var_dump($_SESSION);EXIT;
		$permitted_levels = array(2);
		Authlite::check_access($permitted_levels);
		$user_group = $this->admin->admin_group_id;
		$agency_id = $this->agency_id;
		
		//Authlite::check_agency_integrity($agency_id);
		Authlite::verify_referer();
		
	
		$this->template->title = Kohana::lang('backend.welcome_backend');
		$view=new View ('reports_main');
		
		$admins=get::agency_admins($this->agency_id);
		$admins_excluding_current = Admin_Model::get_admins_excluding_current($this->agency_id, $this->admin->username);
		$parent_id = get::_parent($this->agency_id)->id;		
		
		
		
		//pagination
		$per_page = 15;		
		$all_buses = get::all_buses($agency_id, $parent_id, 4);	


		$towns = get::all_towns();
		$view->admins=$admins;
		$view->towns=$towns;
		$view->admins_excluding_current=$admins_excluding_current;
		$view->all_buses=$all_buses;
		$this->template->content=$view;
		
	}
	
	
	public function route()
	{	
		Authlite::check_admin();
		Authlite::verify_referer();
		$this->template->title = Kohana::lang('backend.passenger_search');
		$towns = get::all_towns();	//$view = '';
		
		if($_POST){
		
			if(isset($_POST['by_period']))
			{
				$post = new Validation($_POST);
				$post->add_rules('start_date','required');
				$post->add_rules('end_date','required');
				
				if ($post->validate())
				{	
					$town_from=$_POST['town_from'];
					$town_to=$_POST['town_to'];
					$start_date=date("Y-m-d",strtotime($_POST['start_date']));
					$end_date=date("Y-m-d",strtotime($_POST['end_date']));
					
					$search_results=Schedule_Model::get_agency_schedules_by_route_by_period($this->agency_id, $town_from, $town_to,$start_date,$end_date);
				}
				else
				{
					$errors=$post->errors('errors');
					$notice="";
						foreach($errors as $error) 
						{
							$notice.=$error."<br />";
						}
						$this->session->set('notice', array('message'=>$notice,'type'=>'error'));
						url::redirect('reports');
				}
				
				
				$view = new View('route_search_results');
				$view->search_results = $search_results;
				$view->start_date = $start_date;
				$view->end_date = $end_date;
				$view->town_from = $town_from;
				$view->town_to = $town_to;
				$view->towns=$towns;
			}
			if(isset($_POST['compare_routes']))
			{	
				$post = new Validation($_POST);
				$post->add_rules('start_date','required');
				$post->add_rules('end_date','required');
				if ($post->validate())
				{	
					//auto set the agency's town to be the town from
					$town_from_name = $this->agency_town;
					$town_from = $this->agency_town_id;
					$start_date = date("Y-m-d",strtotime($_POST['start_date']));
					$end_date = date("Y-m-d",strtotime($_POST['end_date']));
					
					if(strtotime($start_date) > strtotime($end_date)){
						$this->session->set('notice', array('message'=>'Start date must be earlier than end date','type'=>'error'));
						url::redirect('reports');
					}
					
					$destinations_and_income = array();
					$destinations_and_expenses = array();
					$destinations_and_net = array();
					
					//for each town, get all schedules going there within that period
					foreach($towns as $town){
						$period_schedules = Schedule_Model::get_agency_schedules_by_route_by_period($this->agency_id, $town_from, $town->id,$start_date,$end_date);
						
						//for each schedule, get schedule_income, other_income, and expenses
						$schedule_income_total=0;
						$other_income_total=0;
						$expenses_total=0;
						foreach ($period_schedules as $schedule){
							//$schedule_income_total = Schedule_Model::get_departed_schedule_income_by_period($agency_id,$start_date,$end_date){
							$schedule_income_total += $schedule->total_amount;
							$other_income_total = Income_Model::get_agency_income_total_by_schedule($this->agency_id,$schedule->id);
							$expenses_total = get::get_expenses_total_by_schedule($this->agency_id,$schedule->id);
							
						}
						
							$total_income = $schedule_income_total+$other_income_total;
							$net = $schedule_income_total+$other_income_total-$expenses_total;
						//$all_destinations_array[$town->name]=array('income'=>$total_income, 'expenses'=>$expenses_total, 'net'=>$net);
						
						$destinations_and_income[$town->name] = $total_income;
						$destinations_and_expenses[$town->name] = $expenses_total;
						$destinations_and_net[$town->name] = $net;
					}
					//var_dump($all_destinations_array);exit;
				
					
				}
				else
				{
					$errors=$post->errors('errors');
					$notice="";
						foreach($errors as $error) 
						{
							$notice.=$error."<br />";
						}
						$this->session->set('notice', array('message'=>$notice,'type'=>'error'));
						url::redirect('admin/main/'.$this->admin->agency_id);
				}
				
				$view = new View('compare_routes');
				
				
				//sort from highest to lowest values
				arsort($destinations_and_income);arsort($destinations_and_expenses);arsort($destinations_and_net);
				
				$view->destinations_and_income=$destinations_and_income ;
				$view->destinations_and_expenses=$destinations_and_expenses ;
				$view->destinations_and_net=$destinations_and_net ;
				$view->start_date = $start_date;
				$view->end_date = $end_date;
				$view->town_from_name = $town_from_name;
				
			}
			$this->template->content = $view;
		}
		
	}
		
	
	//$type: either parent or agency. Helps distinguish which the BM or GM sees.
	public function schedule_by_date()
	{	
		Authlite::check_admin();
		Authlite::verify_referer();
		
		$agency_id = $this->agency_id;
		$parent_id = get::_parent($agency_id)->id;
		$type = $this->admin->admin_group_id;
		
		//Authlite::check_agency_integrity($agency_id);
		$this->template->title = Kohana::lang('backend.welcome_backend');
		
		if($_POST)
		{
			$post = new Validation($_POST);
			$post->add_rules('date','required');
			if ($post->validate())
			{	
				
			$date = date("Y-m-d",strtotime($_POST['date']));
			//$schedules = ORM::FACTORY('schedule')->where('agency_id',$agency_id)->where('departure_date',$date)->find_all();
			
			/*if it's a GM, type 2
			if ($type==4){
			$_30seats = ORM::FACTORY('schedule')->join('agencies','agencies.id','schedules.agency_id')->where('parent_id',$parent_id)->where('departure_date',$date)->where('status','departed')->where('bus_seats',30)->count_all();
			$_35seats = ORM::FACTORY('schedule')->join('agencies','agencies.id','schedules.agency_id')->where('parent_id',$parent_id)->where('departure_date',$date)->where('status','departed')->where('bus_seats',35)->count_all();
			$_39seats = ORM::FACTORY('schedule')->join('agencies','agencies.id','schedules.agency_id')->where('parent_id',$parent_id)->where('departure_date',$date)->where('status','departed')->where('bus_seats',39)->count_all();
			$_55seats = ORM::FACTORY('schedule')->join('agencies','agencies.id','schedules.agency_id')->where('parent_id',$parent_id)->where('departure_date',$date)->where('status','departed')->where('bus_seats',55)->count_all();
			$_70seats = ORM::FACTORY('schedule')->join('agencies','agencies.id','schedules.agency_id')->where('parent_id',$parent_id)->where('departure_date',$date)->where('status','departed')->where('bus_seats',70)->count_all();
			
			//print_r("HAPPY");exit;
			$departed_schedules = get::all_parent_departed_schedules_by_date($parent_id,$date);				
			$current_schedules = get::all_parent_current_schedules_by_date($parent_id,$date); 
			$parcels=get::all_parent_parcels_by_date($parent_id,$date);
			}*/
			
			//else{
			$_30seats = ORM::FACTORY('schedule')->where('agency_id',$agency_id)->where('departure_date',$date)->where('status','departed')->where('bus_seats',30)->count_all();
			$_35seats = ORM::FACTORY('schedule')->where('agency_id',$agency_id)->where('departure_date',$date)->where('status','departed')->where('bus_seats',35)->count_all();
			$_39seats = ORM::FACTORY('schedule')->where('agency_id',$agency_id)->where('departure_date',$date)->where('status','departed')->where('bus_seats',39)->count_all();
			$_55seats = ORM::FACTORY('schedule')->where('agency_id',$agency_id)->where('departure_date',$date)->where('status','departed')->where('bus_seats',55)->count_all();
			$_70seats = ORM::FACTORY('schedule')->where('agency_id',$agency_id)->where('departure_date',$date)->where('status','departed')->where('bus_seats',70)->count_all();
						
			$departed_schedules = get::all_agency_departed_schedules_by_date($agency_id,$date);				
			$current_schedules = get::all_agency_current_schedules_by_date($agency_id,$date);
			$parcels=get::all_agency_parcels_by_date($agency_id,$date);
			
			//Need to calculate the total expenses for that day
			$agency_expenses_by_date = get::agency_expenses_by_date($agency_id,$date);
			$total_expenditure = 0;
			foreach($agency_expenses_by_date as $exp )
			{
				$total_expenditure += $exp->amount;
			}
			
			//}

			//Calculate total amount expected from departed buses.
			$total = 0;
			foreach($departed_schedules as $ds){
				$total += $ds->total_amount;
			}
			}
			else
			{
				$errors=$post->errors('errors');
				$notice="";
					foreach($errors as $error) 
					{
						$notice.=$error."<br />";
					}
					$this->session->set('notice', array('message'=>$notice,'type'=>'error'));
					url::redirect('admin/main/'.$this->admin->agency_id);
			}
		}
		$view = new View('control_schedule_by_date');
		$view->departed_schedules = $departed_schedules;
		$view->current_schedules = $current_schedules;
		$view->parcels=$parcels;
		$view->total_expenditure=$total_expenditure;
		$view->_30seats = $_30seats;
		$view->_35seats = $_35seats;
		$view->_39seats = $_39seats;
		$view->_55seats = $_55seats;
		$view->_70seats = $_70seats;
		$view->total = $total;
		$view->date = $date;
			
		$this->template->content = $view;
		
	}		
	
	//$type: either parent or agency. Helps distinguish which the BM or GM sees.
	public function activity_by_period(){	
		
		Authlite::check_admin();
		Authlite::verify_referer();
		
		$agency_id = $this->agency_id;
		$parent_id = get::_parent($agency_id)->id;
		$type = $this->admin->admin_group_id;

		//Authlite::check_agency_integrity($agency_id);
		$this->template->title = Kohana::lang('backend.welcome_backend');
		
		if($_POST)
		{
			$post = new Validation($_POST);
			$post->add_rules('start_date','required');
			$post->add_rules('end_date','required');
			$post->add_rules('output_format','required');
			if ($post->validate())
			{	
				
			$start_date = date("Y-m-d",strtotime($_POST['start_date']));
			$end_date = date("Y-m-d",strtotime($_POST['end_date']));
			$output_format = $_POST['output_format'];
			
			if(strtotime($start_date) > strtotime($end_date)){
				$this->session->set('notice', array('message'=>'Start date must be earlier than end date','type'=>'error'));
				url::redirect('reports');
			}
			
				$period_parcels=get::agency_parcels_by_period($agency_id,$start_date,$end_date);
				$period_expenses=Expense_Model::agency_expenses_by_period($agency_id,$start_date,$end_date);
				$period_schedules=Schedule_Model::agency_schedules_by_period($agency_id,$start_date,$end_date);
				$period_income=Income_Model::get_agency_income_by_period($this->agency_id,$start_date,$end_date);
				
				//Remote tickets don't feature under schedules, hence they are not included in this report. They need to be.
				$remote_outgoing_tickets=Ticket_Detail_Model::get_agency_outgoing_remote_tickets_by_period_all_admins($agency_id,$start_date,$end_date);

				
				//calculate the total expenses for that period
				$total_expenditure = 0;
				foreach($period_expenses as $exp )
				{
					$total_expenditure += $exp->amount;
				}
			
				//Calculate total amount expected from departed buses for the period. DEPARTED BUSES ONLY
				$total = 0;
				foreach($period_schedules as $ds)
				{
					$total += $ds->total_amount;
				}
				
				//instantiate variable to total the prices in the loop below
				$parcel_total = 0;
				foreach ($period_parcels as $parcel)
				{
				$parcel_total += $parcel->price;
				}
				
				if($output_format =='grouped'){
					$view = new View('activity_by_period');
				}
				if($output_format == 'percentage'){
					$view = new View('activity_by_period_pie_chart');
				}if($output_format == 'compare'){
					$view = new View('activity_by_period_line_chart');
				}if($output_format == 'daily_total'){
					$view = new View('activity_by_period_daily_total');
				}
			}
			else
			{
				$errors=$post->errors('errors');
				$notice="";
					foreach($errors as $error) 
					{
						$notice.=$error."<br />";
					}
					$this->session->set('notice', array('message'=>$notice,'type'=>'error'));
					url::redirect('admin/main/'.$this->admin->agency_id);
			}
		}
	
		//build chart data
		$current_time = time();
		$chart_filename = "overall_activity_chart". $current_time.".txt";
		$daily_totals = get::build_line_chart_data($agency_id,$start_date,$end_date,$chart_filename);
		
		//$view->parcels=$period_parcels;
		$view->remote_outgoing_tickets=$remote_outgoing_tickets;
		$view->total_expenditure=$total_expenditure;
		$view->period_parcels = $period_parcels;
		$view->parcel_total = $parcel_total;
		$view->period_expenses = $period_expenses;
		$view->period_schedules = $period_schedules;
		$view->period_income = $period_income;
		$view->total = $total;
		$view->start_date = $start_date;
		$view->end_date = $end_date;
		$view->chart_filename = $chart_filename;
		$view->daily_totals = $daily_totals;
			
		$this->template->content = $view;
	
	}
	
	/*
	public function total_income_by_period($agency_id,$start_date,$end_$date){	
		
				$agency_id = $this->agency_id;
				$start_date = date("Y-m-d",strtotime($start_date));
				$end_date = date("Y-m-d",strtotime($end_date));
				
				$period_parcels=get::agency_parcels_by_period($agency_id,$start_date,$end_date);
				$period_schedules=get::agency_schedules_by_period($agency_id,$start_date,$end_date);
				$period_other_income=Income_Model::get_agency_income_by_period($this->agency_id,$start_date,$end_date);
				//Remote tickets don't feature under schedules, hence they are not included in this report. They need to be.
				$remote_outgoing_tickets=Ticket_Detail_Model::get_agency_outgoing_remote_tickets_by_period_all_admins($agency_id,$start_date,$end_date);
			
				$period_expenses=get::agency_expenses_by_period($agency_id,$start_date,$end_date);	
				
				
				//Income from parcels
				$parcel_total = 0;
				foreach ($period_parcels as $parcel){
				$parcel_total += $parcel->price;
				}
				
				//Income from schedules. DEPARTED BUSES ONLY
				$schedule_income = 0;
				foreach($period_schedules as $ds){
					$schedule_income_total += $ds->total_amount;
				}
				
				//Other income
				$other_income_total = 0;
				foreach($period_other_income as $income){
					$other_income_total += $income->amount;
				}
				
				//Remote outgoing tickets
				$remote_outgoing_ticket_income_total =0;
				foreach ($remote_outgoing_tickets as $ticket){
					$remote_outgoing_ticket_income_total += $ticket->Price;
				}
				//Expenses
				$expenses_total = 0;
				foreach($period_expenses as $exp )
				{
					$expenses_total += $exp->amount;
				}
			
			$overall_income = $schedule_income_total + $parcel_total + $other_income_total + $remote_outgoing_ticket_income_total;
			return $overall_income;
	
	} */
	
	
	
	public function activity_by_period_by_admin()
	{	
		Authlite::check_admin();
		Authlite::verify_referer();
		
		$agency_id = $this->agency_id;
		$parent_id = get::_parent($agency_id)->id;
		$type = $this->admin->admin_group_id;
		$admins=get::agency_admins($this->agency_id);

		$this->template->title = Kohana::lang('backend.welcome_backend');
		
		if($_POST)
		{
			
			if(isset($_POST['by_period']))
			{
				$post = new Validation($_POST);
				$post->add_rules('start_date','required');
				$post->add_rules('end_date','required');
				$post->add_rules('admin','required');
				if ($post->validate())
				{	
					
					$start_date = date("Y-m-d",strtotime($_POST['start_date']));
					$end_date = date("Y-m-d",strtotime($_POST['end_date']));
					
					if(strtotime($start_date) > strtotime($end_date)){
						$this->session->set('notice', array('message'=>'Start date must be earlier than end date','type'=>'error'));
						url::redirect('reports');
					}
					$admin=$_POST['admin'];
				
					$period_parcels=Parcel_Model::get_agency_parcels_by_period_by_admin($agency_id,$start_date,$end_date,$admin);
					$period_expenses=Expense_Model::get_agency_expenses_by_period_by_admin($agency_id,$start_date,$end_date,$admin);
					$period_schedules=Ticket_Detail_Model::get_agency_tickets_by_period_by_admin($agency_id,$start_date,$end_date,$admin);
					$period_income=Income_Model::get_agency_income_by_period_by_admin($this->agency_id,$start_date,$end_date,$admin);
									
					//calculate the total expenses for that period
					
					$total_expenditure = Expense_Model::get_agency_expenses_total_by_period_by_admin($agency_id,$start_date,$end_date,$admin);
					/*$total_expenditure = 0;
					foreach($period_expenses as $exp )
					{
						$total_expenditure += $exp->amount;
					} */
				
					//Calculate total amount expected from departed buses for the period. DEPARTED BUSES ONLY
					$ticket_total = Ticket_Detail_Model::get_agency_tickets_total_by_period_by_admin($agency_id,$start_date,$end_date,$admin);
					
				}
				else
				{
					$errors=$post->errors('errors');
					$notice="";
						foreach($errors as $error) 
						{
							$notice.=$error."<br />";
						}
						$this->session->set('notice', array('message'=>$notice,'type'=>'error'));
						url::redirect('admin/main/'.$this->admin->agency_id);
				}
				
				$view = new View('activity_by_period_by_admin');
				$view->parcels=$period_parcels;
				$view->total_expenditure=$total_expenditure;
				$view->period_parcels = $period_parcels;
				$view->period_expenses = $period_expenses;
				
				$view->period_schedules = $period_schedules;
				$view->period_income = $period_income;
				$view->total = $ticket_total;
				$view->start_date = $start_date;
				$view->end_date = $end_date;
				$view->admin = $admin;
			}
			if(isset($_POST['compare_admins']))
			{	
				$post = new Validation($_POST);
				$post->add_rules('start_date','required');
				$post->add_rules('end_date','required');
				if ($post->validate())
				{	
					//auto set the agency's town to be the town from
					$town_from_name = $this->agency_town;
					$town_from = $this->agency_town_id;
					$start_date = date("Y-m-d",strtotime($_POST['start_date']));
					$end_date = date("Y-m-d",strtotime($_POST['end_date']));
					
					if(strtotime($start_date) > strtotime($end_date)){
						$this->session->set('notice', array('message'=>'Start date must be earlier than end date','type'=>'error'));
						url::redirect('reports');
					}
					
					$admins_and_income = array();
					$admins_and_expenses = array();
					$admins_and_net = array();
					
					//for each admin, get all schedules going there within that period
					foreach($admins as $admin){
							
						$period_parcels=Parcel_Model::get_agency_parcels_by_period_by_admin($agency_id,$start_date,$end_date,$admin->username);
						$period_schedules=Ticket_Detail_Model::get_agency_tickets_by_period_by_admin($agency_id,$start_date,$end_date,$admin->username);
						$period_income=Income_Model::get_agency_income_by_period_by_admin($this->agency_id,$start_date,$end_date,$admin->username);
						$period_expenses=Expense_Model::get_agency_expenses_by_period_by_admin($agency_id,$start_date,$end_date,$admin->username);
						//var_dump($period_parcels);exit;
										
						$parcel_total = Parcel_Model::get_agency_parcel_total_by_period_by_admin($agency_id,$start_date,$end_date,$admin->username);
						$ticket_total = Ticket_Detail_Model::get_agency_tickets_total_by_period_by_admin($agency_id,$start_date,$end_date,$admin->username);
						$other_income_total = Income_Model::get_agency_income_total_by_period_by_admin($agency_id,$start_date,$end_date,$admin->username);
						$expenses_total = Expense_Model::get_agency_expenses_total_by_period_by_admin($agency_id,$start_date,$end_date,$admin->username);
							
						$income_total = $parcel_total + $ticket_total + $other_income_total;
						$net = $income_total - $expenses_total;
							
						$admins_and_income[$admin->username] = $income_total;
						$admins_and_expenses[$admin->username] = $expenses_total;
						$admins_and_net[$admin->username] = $net;
					}
					
				}
				else
				{
					$errors=$post->errors('errors');
					$notice="";
						foreach($errors as $error) 
						{
							$notice.=$error."<br />";
						}
						$this->session->set('notice', array('message'=>$notice,'type'=>'error'));
						url::redirect('admin/main/'.$this->admin->agency_id);
				}
				
				$view = new View('compare_admins');
				
				
				//sort from highest to lowest values
				arsort($admins_and_income);arsort($admins_and_expenses);arsort($admins_and_net);
				
				$view->admins_and_income=$admins_and_income ;
				$view->admins_and_expenses=$admins_and_expenses ;
				$view->admins_and_net=$admins_and_net ;
				$view->start_date = $start_date;
				$view->end_date = $end_date;
				
			}
		}
		
		$this->template->content = $view;
	
	}
	
		
	
	
	
	public function passenger_activity($passenger_name,$passenger_idc)
	{	
		Authlite::check_admin();
		Authlite::verify_referer();
		$this->template->title = Kohana::lang('backend.reports_for_bus');
		
		$search_results=Ticket_Detail_Model::passenger_activity($this->agency_id,$passenger_name,$passenger_idc);				
		
		$view = new View('passenger_activity');
		$view->search_results = $search_results;
		$view->passenger_name = $passenger_name;
		$view->passenger_idc = $passenger_idc;
		$this->template->content = $view;
	}	
	
	public function passenger_search()
	{	
		Authlite::check_admin();
		Authlite::verify_referer();
		$this->template->title = Kohana::lang('backend.passenger_search');
		
		if($_POST)
		{
			if(isset($_POST['by_name']))
			{
				$post = new Validation($_POST);
				$post->add_rules('passenger_name','required');
				if ($post->validate())
				{	
					$passenger_name=$_POST['passenger_name'];
					$search_results=Ticket_Detail_Model::search_passenger_by_name($this->agency_id,$passenger_name);
				}
				else
				{
					$errors=$post->errors('errors');
					$notice="";
						foreach($errors as $error) 
						{
							$notice.=$error."<br />";
						}
						$this->session->set('notice', array('message'=>$notice,'type'=>'error'));
						url::redirect('reports');
				}
				$view = new View('passenger_search_results');
				$view->search_results = $search_results;
				$view->passenger_name = $passenger_name;
			}
		}
		$this->template->content = $view;
	}	
	
	public function download_passenger_list()
	{	
		Authlite::check_admin();
		Authlite::verify_referer();
		$this->template->title = Kohana::lang('backend.passenger_search');
		
		//Delete all files in folder
		//array_map('unlink', glob("C://wamp/www/quickticket/downloads/*.csv")); 
		
		//The file name is used to retrieve the file
		$current_time = time();
		$filepath = "C://wamp/www/quickticket/downloads/report-".$current_time.".csv";
		Ticket_Detail_Model::get_passenger_list($this->agency_id,$filepath);
		
		//Kohana download::force() helper returns empty file
		//Without auto_render, returns error page.
		$this->auto_render = false;
		  $file = $filepath;
		  $filename = 'report-'.$current_time.".csv";
		  header('Content-type: application/csv');
		  header('Content-Disposition: inline; filename="' . $filename . '"');
		  header('Content-Transfer-Encoding: binary');
		  header('Accept-Ranges: bytes');
		  @readfile($file);
		  //unlink($filename);
		
		
	}	
	
		

		
		
		}