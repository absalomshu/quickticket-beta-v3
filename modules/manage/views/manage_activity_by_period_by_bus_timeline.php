<?php defined('SYSPATH') or die('No direct script access'); ?>
	
    <div class="container">
		
		
		<div class="row-fluid  span12 hero-unit prof-unit reload" style="margin:5px 0 0px 0;">	
		
			
			
			<div class="span12" >
			<div class="no-print">
			
			<ul class="nav nav-tabs">
					<li > <a href="<?=url::site('manage/reports')?>"><i class="icon-arrow-left"></i> <?=Kohana::lang('backend.back_reports')?></a></li>
				</ul>
			</div>
			<div class="span8 offset2 print-area" >
			
				
				<legend><?=Kohana::lang('backend.summary_from')?> <?=date("d-m-Y",strtotime($start_date))." to ".date("d-m-Y",strtotime($end_date))." for "?><b><?=$bus_number?></b><button class="view" onClick="window.print()"><i class="icon-print"></i> <?=Kohana::lang('backend.print')?></button></legend>
				<table class="table table-bordered table-condensed">
					<tr><th><?=Kohana::lang('backend.timeline')?></th></tr>
					<tr>
						<td class="span2">Date</td>
						<td>Description</td>
						<td class="text-right">Debit</td>
						<td class="text-right">Credit</td>
					</tr>
					<?php 
					foreach ($all_activity as $key=>$value)
					{?>
					<tr>
						<td class="span2"><?=date("d-m-Y",$value['date'])?></td>
						<td><?=$value['description']?></td>
						<td class="text-right"><?php if($value['type']=='debit'){echo number_format($value['amount']);}?></td>
						<td class="text-right"><?php if($value['type']=='credit'){echo number_format($value['amount']);}?></td>
					</tr>
					<?php }?>
					<tr>		
						<td class="span2"></td>
						<th ><?=Kohana::lang('backend.net_income')?></th>
						<td class="span2"></td>
						<th class="span2 text-right"><?=number_format($total + $income_total - $total_expenditure)?> FCFA</th>
					</tr>
				</table>
				
					
			
				<table class="table table-bordered table-condensed">					
					<tr>		
						<td><h3><?=Kohana::lang('backend.net_income')?></h3></th>
						<td class="text-right"><h3><?=number_format($total + $income_total - $total_expenditure)?> FCFA</h3></td>
					</tr>
				</table>
			</div>
		</div>
			
		
		
		
		
		
		
		</div>
    </div> 
