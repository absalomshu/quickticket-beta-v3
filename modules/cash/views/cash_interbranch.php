<?php defined('SYSPATH') or die('No direct script access'); ?> 
	

 
	<div class="row-fluid ">
	  	<div class="span12 hero-unit prof-unit" >
		
		<ul class="nav nav-tabs" id="myTab">
					
					<li ><a href="<?=url::site('cash/collections')?>"><i class="icon-align-justify"></i> <?=Kohana::lang('backend.collection')?></a></li>
					<li class="active"><a href="#cash-interbranch"><i class="icon-align-justify"></i> <?=Kohana::lang('backend.cash_interbranch')?></a></li>
					<li><a href="<?=url::site('cash/cash_to_bank')?>"><i class="icon-align-justify"></i> <?=Kohana::lang('backend.cash_to_bank')?></a></li>
		</ul>
		
		<div class="tab-content">
			 
			
			
			
			<div class="tab-pane active" id="cash-interbranch">
				<div class="span8 offset2">
				
					<legend><?=Kohana::lang('backend.cash_interbranch')?><div class='view'><a href="<?=url::site('cash/send_cash_interbranch')?>">+ <?=Kohana::lang('backend.send')?></a> | <a href="<?=url::site('cash/receive_cash_interbranch')?>">+ <?=Kohana::lang('backend.receive')?></a></div></legend>						
						<table class="table table-striped table-hover table-condensed">
					<tr>
						
						<th><?=Kohana::lang('backend.date')?></th>
						<th><?=Kohana::lang('backend.branch')?></th>
						<th><?=Kohana::lang('backend.amount')?></th>
						<th><?=Kohana::lang('backend.description')?></th>
						<th><?=Kohana::lang('backend.done_by')?></th>
												
					</tr>
					<?php foreach ($cib as $trx):?>
						<tr>	
							<td><?=date("d-m-Y",strtotime($trx->date_incurred))?></td>
							<td><?=get::town($trx->agency_to_id)?></td>
							<td><?=number_format($trx->amount)?></td>
							
							<td><?=$trx->description?></td>
							<td><?=$trx->created_by?></td>
							
						</tr>
					<?php endforeach; ?>
						</table>	
				
						<?php echo $this->pagination;?>		
				</div>	
					
			</div>
			
			
					
		</div>
		
			
		
			  
		</div>
		
		</div>
	  <div class="clear"></div>

<script type="text/javascript">
	$(document).ready(function(){
			$('#timepicker').timepicker({
				minuteStep:30,
				//defaultTime: '8:00 PM'
			});
			
			$('.input-append.date').datepicker({
				format: "dd-mm-yyyy",
				todayBtn: "linked",
				autoclose: true,
				todayHighlight: true
			});
			
			
			
	})
	</script>