<?php defined('SYSPATH') or die('No direct script access'); ?>
	
    <div class="container">
		<div class="notice">
		<?php
		//determine what type of notice to display if at all
		$notice = $this->session->get_once('notice');
		if(!empty($notice))
		{ 
					if($notice['type'] == 'success'){?><div class="alert general success"><a class="close" data-dismiss="alert" href="#">&times;</a><?php echo $notice['message']; }?></div>
				<?	if($notice['type'] == 'error'){?><div class="alert general error"><a class="close" data-dismiss="alert" href="#">&times;</a><?php echo $notice['message']; }?></div><?
		} ?>
		</div>
		
		<div class="row-fluid marketing" style="margin:5px 0 0px 0;">	
		
			
			
			<div class="span7" style="float:left;">
			
			<?php 
			//instantiate variable to total the prices in the loop below
			$parcel_total = 0;
			foreach ($parcels as $parcel)
			{
			$parcel_total += $parcel->price;
			}
				
			
			if(count($current_schedules) != 0)
			{
				?>
				<div class="span12 hero-unit prof-unit reload" style="margin-left:0px;">
					<div class="heading"><?=Kohana::lang('backend.pending_for')?><?=date("d-m-Y",strtotime($date))?></div>
					<div class="rule"><hr/></div>
						<table class="table">
							<?php foreach ($current_schedules as $c_schedule):?>
							<tr><td><i class="icon-time"></i> <?=$c_schedule->bus_number?> : <b><?=get::town($c_schedule->from)?></b> to <b><?=get::town($c_schedule->to)?></b> : <b><?=date("g:i A", strtotime($c_schedule->departure_time))?></b><i> <span style="font-size:11px;">  (Loading now...) </span> </i></td></tr>
							<!--<input type="hidden"  class="schedule_id" value="<?=$c_schedule->id?>"></input>-->
							<?php endforeach; ?>
						</table>	
				</div>
			<?}?>
			<div class="span12 hero-unit prof-unit reload" style="margin-left:0px;">
				<div class="heading"><?=Kohana::lang('backend.checkedout_for')?> <?=date("d-m-Y",strtotime($date))?></div>
				<div class="rule"><hr/></div>
				
				<table class="table">
					<?php foreach ($departed_schedules as $d_schedule):?>
					<tr><td><i class="icon-ok-sign"></i> <a href="<?=url::site('control/past_schedule').'/'.$d_schedule->id?>"><?=$d_schedule->bus_number?></a> : <b><?=get::town($d_schedule->from)?></b> -> <b><?=get::town($d_schedule->to)?></b> : <b><?=date("g:i A", strtotime($d_schedule->departure_time))?></b><a class="view" href="<?=url::site('control/past_schedule').'/'.$d_schedule->id?>">View</a></td></tr>
					<?php endforeach; ?>
				</table>					
			</div>
			
			<div class="span12 hero-unit prof-unit reload" style="margin-left:0px;">
				<div class="heading"><?=Kohana::lang('backend.summary_for')?> <?=date("d-m-Y",strtotime($date))?></div>
				<div class="rule"><hr/></div>
				<i>NB: <?=Kohana::lang('backend.from_departed')?>!</i><br/>
				<table class="table table-bordered">
					<tr>
						<td><?=Kohana::lang('backend.number_of')?> 30 <?=Kohana::lang('backend.seater')?></td>
						<td><?=$_30seats?></td>
					</tr>
					<tr>
						<td><?=Kohana::lang('backend.number_of')?> 35 <?=Kohana::lang('backend.seater')?></td>
						<td><?=$_35seats?></td>
					</tr><tr>
						<td><?=Kohana::lang('backend.number_of')?> 39 <?=Kohana::lang('backend.seater')?></td>
						<td><?=$_39seats?></td>
					</tr>
					<tr>		
						<td><?=Kohana::lang('backend.number_of')?> 55 <?=Kohana::lang('backend.seater')?></td>
						<td><?=$_55seats?></td>
					</tr>
					<tr>		
						<td><?=Kohana::lang('backend.number_of')?> 70 <?=Kohana::lang('backend.seater')?></td>
						<td><?=$_70seats?></td>
					</tr>
					<tr>		
						<th><?=Kohana::lang('backend.total_ticket_sales')?></th>
						<th><?=number_format($total)?> FCFA</th>
					</tr>
					<tr>		
						<th><?=Kohana::lang('backend.total_from_parcels')?></th>
						<th><?=number_format($parcel_total)?> FCFA</th>
					</tr>
					<tr>		
						<th><?=Kohana::lang('backend.total_expenditure')?></th>
						<th><?=number_format($total_expenditure)?> FCFA</th>
					</tr>
					<tr>		
						<td><h3>GRAND TOTAL</h3></th>
						<td><h3><?=number_format($total + $parcel_total - $total_expenditure)?> FCFA</h3></td>
					</tr>
				</table>
			</div>
			</div>
			
			<div class="span5" style="float:left;">
				<div class="span12 hero-unit prof-unit" >
					<div class="heading"><?=Kohana::lang('backend.parcel_deliveries')?><span class="view"><button type='button' class='btn btn-large btn-primary btn-warning disabled' disabled='disabled'><?=count($parcels)?></button></span></div>
						
				</div>
		
				
				<div class="span12 hero-unit prof-unit" style="float:right;">
					<div class="heading"><?=Kohana::lang('backend.departed_buses')?><span class="view"><button type='button' class='btn btn-large btn-primary btn-success disabled' disabled='disabled'><?=$_30seats+$_35seats+$_39seats+$_55seats+$_70seats?></button></span></div>			
				</div>
			</div>
		
		
		
		
		
		</div>
    </div> 
