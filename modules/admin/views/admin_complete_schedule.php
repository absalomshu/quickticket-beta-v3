<?php defined('SYSPATH') or die('No direct script access'); ?> 


	<!--
			For each input element, we check if it's a manager
			i.e. if it's the 'control' function. 
			If so, disable the input fields so they can't change anything. For now.		
		-->
	<?php //$manager = ((substr($_SERVER['PATH_INFO'],7,7))=='control') ? 1:0;
			//print_r($manager);exit;
	?>
 
	  <script>
		  function preventBack(){window.history.forward();}
		  setTimeout("preventBack()", 0);
		  window.onunload=function(){null};
		</script>

 
		<!-- Modal for start boarding-->
		<div id="confirm-boarding" class="modal hide fade" tabindex="-1" role="dialog" aria-labelledby="myModalLabel" aria-hidden="true">
		  <div class="modal-header">
			<button type="button" class="close" data-dismiss="modal" aria-hidden="true">×</button>
			<h3 id="myModalLabel"><?=Kohana::lang('backend.start_boarding')?> <?=$schedule->bus_number?></h3>
		  </div>
		  <div class="modal-body">
			<p><?=Kohana::lang('backend.send_sms_passengers')?>?</p>
		  </div>
		  <div class="modal-footer">
			<a href="<?=url::site('admin/start_boarding_schedule_with_sms/'.$schedule->id)?>"class="btn btn-primary"><?=Kohana::lang('backend.yes')?></a>
			<a href="<?=url::site('admin/start_boarding_schedule_without_sms/'.$schedule->id)?>"class="btn"><?=Kohana::lang('backend.no')?></a>
			<a class="btn" data-dismiss="modal" aria-hidden="true"><?=Kohana::lang('backend.cancel')?></a>
		  </div>
		</div>

	  <div class="row-fluid marketing" style="margin:5px 0 0px 0px;">
	  
	  	<div class="span12 hero-unit prof-unit" >
		<div class="heading"><?=Kohana::lang('backend.sched_for')?> <?=$schedule->bus_number?><?php if(get::vip_bus($schedule->bus_number)){?> <i class="icon-star" title="VIP Bus"></i> <?php }?> | <?=Kohana::lang('backend.schedule_id')?>: <?="SC".$this->agency_id.$schedule->id?> | <span class="badge badge-warning">Tickets sold: <?=$seats_taken?></span>
			<?php
			//only show for schedules that have not been re-modified
			if($schedule->update_count == 0){?>
			<div class="view">
			
										
					<a href="<?=url::site('admin/edit_schedule').'/'.$schedule->id?>"> <?=Kohana::lang('backend.edit')?> </a> &nbsp;   |  &nbsp;  	

						<div class="btn-group ">
						  <a class="btn dropdown-toggle btn-mini" data-toggle="dropdown" href="#" >
							<?php if($schedule->loading == 0){echo Kohana::lang('backend.loading');}?>
							<?php if($schedule->loading == 1){echo Kohana::lang('backend.boarding');}?>
							<?php if($schedule->loading == 2){echo Kohana::lang('backend.delayed');}?>
							<span class="caret"></span>
						  </a>
						  <ul class="dropdown-menu">
						  <?php if($schedule->loading != 1){?>
							<li><a href="#confirm-boarding" data-toggle="modal"><i class="icon-share-alt"></i> <?=Kohana::lang('backend.boarding')?></a></li>
								
							
						  <?php } if($schedule->loading != 2){?>		
							<li><a href="<?=url::site('admin/delay_schedule').'/'.$schedule->id?>"><i class="icon-warning-sign"></i> <?=Kohana::lang('backend.delayed')?></a></li>
							<?php }?>
						 </ul>
						</div>
			
							
							
			</div>
			<? } ?>
		
		</div>
			<div class="rule"><hr/></div>
			<div class="span2 spacious">
				<div class="control-group">
				<label class="control-label lab" for="inputType"><?=Kohana::lang('backend.from')?>:</label>
					<div class="controls ">
						<h3><?=get::town($schedule->from);?></h3>
					</div>
				</div>
			</div>
			<div class="span2 spacious">
				<div class="control-group">
				<label class="control-label lab" for="inputType"><?=Kohana::lang('backend.to')?>:</label>
				<div class="controls ">	
				<h3><?=get::town($schedule->to);?></h3>
				</div>
					</div>
				</div>
			<div class="span2 spacious">
				<label class="control-label lab" for="inputType"><?=Kohana::lang('backend.time')?>:</label>
					<div class="controls ">
					<h3><?=date("g:i A", strtotime($schedule->departure_time));?></h3>
					</div>
			</div>
			<div class="span2 spacious">
				<label class="control-label lab" for="inputType">Date:</label>
				<div class="controls ">
									<h3><?=date("d-m-Y",strtotime($schedule->departure_date));?></h3>
				</div>
			</div>
			
			<div class="span2 spacious" >
				<div class="control-group">
				<label class="control-label lab" for="inputType"><?=Kohana::lang('backend.bus_type')?>:</label>
						<div class="controls ">	
							<h3><?=$schedule->bus_seats?> <?=Kohana::lang('backend.seater')?></h3>
						</div>	

				</div>
			</div>
			
			<div class="span2 spacious">
				<label class="control-label lab" for="inputType"><?=Kohana::lang('backend.bus_no')?>:</label>
					<div class="controls ">
						<h3 <?php if ($schedule->bus_number=="UNKNOWN"){?> style="color:red;" <?php }?>><?=$schedule->bus_number?></h3>
					
					</div>
			</div>
			<div class="span2 spacious">
				<label class="control-label lab" for="inputType"><?=Kohana::lang('backend.ticket_price')?> (FCFA):</label>
					<div class="controls ">	
						<h3><?=number_format($schedule->ticket_price)?> FCFA</h3>
					</div>
			</div>
			<div class="span2 spacious">
				<label class="control-label lab" for="inputType"><?=Kohana::lang('backend.done')?>?</label>
					<div class="controls ">	
						<?php 
						//Show "check out" if the schedule has never been re-modified, and "save changes" if it has
						if($schedule->update_count == 0){?>
						<a class="btn btn-large btn-success" href="<?=url::site('admin/checkout_schedule/'.$schedule->id)?>" onClick="return confirm('<?=Kohana::lang('backend.confirm_checkout')?>?');"><i class="icon-road"></i> <?=Kohana::lang('backend.checkout')?></a>
						<?php }else{?>
						<a class="btn btn-large btn-success" href="<?=url::site('admin/checkout_schedule/'.$schedule->id)?>" onClick="return confirm('<?=Kohana::lang('backend.save_changes')?>?');"> <?=Kohana::lang('backend.save_changes')?></a>
						<?php } ?>
						
						<!--<a href="<?php //url::site('admin/checkout_schedule/'.$schedule->id)?>" ><i class="icon-road"></i> Check out</a>-->
					</div>
			</div>
			
		
			
			  
		</div>
	 </div>	
		

	<?php
			//What the user entered in his previous submission attempt, in case it was a failure
			$post = $this->session->get_once('post');
	 ?>
		
				
	<div class="row-fluid marketing" style="margin:5px 0 0px 0;">	
					
		<div class="span8 " style="margin-left:0px;" >
				
			<div class="span12 hero-unit prof-unit" >
					<form action="<?=url::site('admin/complete_schedule/'.$schedule->id)?>" method="POST" id="complete_schedule_form" autocomplete="off">
						<?php no_csrf::set(1); ?>
					
							<ul class="nav nav-tabs" id="myTab">
							  <li class="active"><a href="#seat-tab"><?=Kohana::lang('backend.seats')?></a></li>
							  <li><a href="#reservation-tab">Reservations</a></li>
							  <?php 
							  //count and display the number of reservations
							  $res_count = count($client_reservations);
							  ?>
							  <!-- autoloaded by jquery, but still done here so it appears when page is loaded manually, then jquery takes over-->
							   <li><a href="#request-tab"><?=Kohana::lang('backend.requests')?> 
								<span class="res_count">
									<?php if($res_count != 0){ ?>
										<span class="notification"><?=$res_count?></span>
									<?php }?>
								</span></a></li>

							</ul>
							 
							<div class="tab-content">
							  <div class="tab-pane active" id="seat-tab">
								<div class="heading"><?=Kohana::lang('backend.checkout_seat')?>
									<div class="view">
										<input type="checkbox" name="free_ticket" id="ft"/> <?=Kohana::lang('backend.free_ticket')?> 
										<input type="text" class="input-medium"  name="free_ticket_reason" placeholder="<?=Kohana::lang('backend.reason')?>" id="ft-reason" style="display:none" />
									</div>
								</div>
								
								<div class="rule"><hr/></div>
											
								<div class="span3 spacious">
									<div class="control-group">
									<label class="control-label lab" for="inputType"  ><?=Kohana::lang('backend.client_name')?><span class="red"> *</span> :</label>
										<div class="controls ">
											<!--Putting required in the following input causes the reservations from clients not to be able to be accepted or rejected-->
											<input type="text" class="span12" id="client_name" name="client_name" value="<?=@$post['client_name']?>"  required/>
										</div>
									</div>
								</div>
								<div class="span3 spacious">
									<div class="control-group">
									<label class="control-label lab" for="inputType"><?=Kohana::lang('backend.client_id')?>:</label>
										<div class="controls ">
											<input type="text" class="span12"   name="client_idc" value="<?=@$post['client_idc']?>" id="client_idc" >
										</div>
									</div>
								</div>
								<div class="span3 spacious">
									<div class="control-group">
									<label class="control-label lab" for="inputType"><?=Kohana::lang('backend.phone')?>:</label>
										<div class="controls input-append">
											<input type="text" class="span12"  name="client_phone" value="<?=@$post['client_phone']?>" id="client_phone" >
										</div>
									</div>
								</div>
								<div class="clear"></div>
								<div class="span3 spacious">
									<div class="control-group">
									<label class="control-label lab" for="inputType"><?=Kohana::lang('backend.email')?>:</label>
										<div class="controls ">
											<input  type="email" class="span12" name="client_email" value="<?=@$post['client_email']?>" email id="client_email" >
										</div>
									</div>
								</div>
								
								
								<div class="span3 spacious">
									<div class="control-group">
										<label class="control-label lab" for="inputType"><?=Kohana::lang('backend.spec_drop')?>  &nbsp;    <input type="checkbox" id="special_drop" name="spec_drop_check"/>   </label>
											
										<div class="controls"  id="special-drop-town" style="display:none;">
											<select name="special_drop_town" id="inputType" >
												<?php foreach ($towns as $town):?>
												<option value="<?=$town->id?>"><?=$town->name?></option>
												<?php endforeach;?>
											
										</select>
										</div>											
									</div>
								</div>
								<div class="span3 spacious " id="special-drop-price" style="display:none;">
									<div class="control-group">
										<label class="control-label lab" for="inputType"><?=Kohana::lang('backend.drop_price')?>:</label>
											<div>
												<input type="text"  class="span12 IsAmount" name="special_drop_price" placeholder="" value="<?=@$post['special_drop_price']?>"/>
											</div>
									</div>
								</div>
							
								<div class="clear"></div>
								
								<div class="span3 spacious" >
									<div class="control-group">
										<label class="control-label lab" for="inputType"><?=Kohana::lang('backend.seat')?>:</label>
										<div class="short-dropdown">
											<select name="seat_number" id="inputType">
											<?php 
											//array to hold all occupied seats for highlighting  on bus plan
											echo "<script> var jsar = new Array();</script>";
											$number=$schedule->bus_seats;
											
											//start from 2 since seat 1 is the driver
											$i=2;
											for($i; $i<= $number; $i++){
											//make sure the seat is not already occupied
											if(!isset($seats_and_occupants[$i])  AND !isset($seats_and_reservers[$i]) )
											{
											?>
												<option value="<?=$i?>"><?=$i?></option>
											<?php 
											}if(isset($seats_and_occupants[$i])) 
											{
												echo "<script> jsar[".$i."] = ".$i.";</script>";
											}
											//$i++;
											}
											?>
		
											</select>
										</div>
										
										<div >
											<button class="btn btn-info" type="submit" name="check_out_form"><i class="icon-check"></i> <?=Kohana::lang('backend.save')?></button><br/>
											<!--<button type="button" class="btn btn-info" name="check_out_form" onclick="complete_schedule_js()"><i class="icon-check"></i> <?=Kohana::lang('backend.done')?></button><br/>-->
										</div>
																
									</div>
								
								</div>
							
						
						<div class="clear"></div>
						
						</form>
						
					 </div>
			  
			  <div class="tab-pane" id="reservation-tab">

				<form action="<?=url::site('admin/complete_schedule/'.$schedule->id)?>" method="POST" autocomplete="off" >
					<?php no_csrf::set(3); ?>
					
						<div class="heading"><?=Kohana::lang('backend.reserve_seat')?></div>
						
							
							<div class="span3 spacious">
								<div class="control-group">
								<label class="control-label lab" for="inputType"><?=Kohana::lang('backend.name')?><span class="red"> *</span>:</label>
									<div class="controls ">
										<input type="text" class="span12" name="res_name" value="<?=@$post['res_name']?>" required id="res_name"/>
									</div>
								</div>
							</div>
							<div class="span3 spacious">
								<div class="control-group">
								<label class="control-label lab" for="inputType"><?=Kohana::lang('backend.phone')?>:</label>
									<div class="controls ">
										<input type="text" class="span12" name="res_phone" value="<?=@$post['res_phone']?>" id="res_phone">
									</div>
								</div>
							</div>
							<div class="span3 spacious">
								<div class="control-group">
								<label class="control-label lab" for="inputType"><?=Kohana::lang('backend.email')?>:</label>
									<div class="controls ">
										<input type="text" class="span12" name="res_email" value="<?=@$post['res_email']?>" id="res_email">
									</div>
								</div>
							</div>
							<div class="span3 spacious">
									<div class="control-group">
									<label class="control-label lab" for="inputType"><?=Kohana::lang('backend.id_card')?>:</label>
										<div class="controls ">
											<input type="text" class="span12"  name="res_idc" value="<?=@$post['res_idc']?>" id="res_idc">
										</div>
									</div>
								</div>
							
							<?php //print_r($seats_and_occupants);exit;?>
							<div class="span3 spacious" >
							<div class="control-group">
							<label class="control-label lab" for="inputType"><?=Kohana::lang('backend.seats_av')?>:</label>
								<div class="short-dropdown">
									<select name="res_seat" id="inputType">
									<?php $number=$schedule->bus_seats;
									$i=2;
									for($i; $i<= $number; $i){
									//make sure the seat is not already occupied and not reserved either.
									
									if(!isset($seats_and_occupants[$i])  AND !isset($seats_and_reservers[$i]) )
									{
									?>
										<option value="<?=$i?>"><?=$i?></option>
									<?php 
									}
									$i++;
									}
									?>
				
									</select>
								</div>
							</div>
						</div>
							<div class="span3 spacious">
								<div class="control-group">
								<label class="control-label lab" for="inputType">&nbsp;</label>
										<button type="submit" name="reserve_seat_form" class="btn btn-info"><?=Kohana::lang('backend.reserve')?></button>
								</div>
							</div>
								
					</form>	
				
			  </div>
				
			<div class="tab-pane" id="request-tab">
					
					<div class="heading"><?=Kohana::lang('backend.res_requests')?></div>
					 <!-- autoloaded by jquery, but still done here so it appears when page is loaded manually, then jquery takes over-->					
						<form action="<?=url::site('admin/complete_schedule/'.$schedule->id)?>" method='POST'>
						<!--The form above MUST come before the div below. Coz when jQuery reloads the data into the div, it does not load
						the form element. And for that reason, no submit is possible. Hence the <form> is loaded before, and left there permanently
						Only the inner contents will get reloaded/updated
						-->
						<div id="res_requests">
						<?php
						$client_reservations=get::client_reservations($schedule->id);
						$client_reservations=json_decode($client_reservations->reserved_seats,true);
						if (!empty ($client_reservations)){ ?>
								
							<table class='table table-bordered'>
								<tr>
										<th><?=Kohana::lang('backend.seat_no')?></th>
										<th><?=Kohana::lang('backend.occupant')?></th>
										<th><?=Kohana::lang('backend.phone')?></th>
										<th>Actions</th>
								</tr> 
													
							<?php						
								foreach($client_reservations as $seat => $details){
								$serial = serialize($client_reservations);
														?>
																
										<tr>
											<!--Cast to interger, so seat 2.1 shows as seat 2-->
											<td><?=(int)$seat?></td>
											<td><?=$details['name']?></td>
											<td><?=$details['phone']?></td>
											
											<td><input type='hidden' value='<?php echo htmlentities($serial)?>' name='arr'/>
												<!--simple hidden field to notify that this is from the reservation request form (client)-->
												<input type='hidden' value='1' name='request_from_frontend'/>
												<button type='submit' id='accept'name='res_to_occ' value="<?=$seat?>" title='Confirm' onClick="return confirm('<?=Kohana::lang('backend.confirm_reservation')?>?');"></button> | 
												<button type='submit' id='reject' name='reject_reserve' value="<?=$seat?>" title='Delete' onClick="return confirm('<?=Kohana::lang('backend.reject_reservation')?>?');"></button>
											</td>
										</tr>	
									<?php					
					
								} ?>
							</table>
									
									<?php
									}
					else{ echo Kohana::lang('backend.no_reservation');} ?>
					
							
						</div></form>

			</div>			
			</div>
					
			</div>
			
			<div class="span12 hero-unit prof-unit" style="margin-left:0px;">
	<div class="notice" style="font-size:14px;line-height:14px;">
		<?php
			//determine what type of notice to display if at all
			$notice = $this->session->get_once('notice');
				if(!empty($notice)){ 
					if($notice['type'] == 'success'){?><div class="alert general success"><a class="close" data-dismiss="alert" href="#">&times;</a><?php echo $notice['message']; ?></div><?php }?>
				<?	if($notice['type'] == 'error'){?><div class="alert general error"><a class="close" data-dismiss="alert" href="#">&times;</a><?php echo $notice['message'];?></div><?php }?>
		<?}?>
	</div>
				
						<?php 
						
						//If there are occupants or reservers, show the table
						if (!empty($seats_and_occupants) OR !empty($seats_and_reservers))
						{?>
							
						
	
									<div class="heading" >BORDEREAU
										<?php if($bus->driver){?><div class="view">Driver: <b><?=$bus->driver?></b></div><?php }?>
									</div>
									
									
									<?php 
									//combine reserved and taken
									//COMBINING INEFFICIENT AS IT IS A 2D AND A 3D
									//$all_seats = $seats_and_reservers + $seats_and_occupants;
									//ksort($all_seats);
									//foreach($seats_and_reservers as $key=>$subkey){print_r($subkey['name']);echo"<br/>";}?>
										
								
								
								<div class="rule"><hr/></div>
							
						
						<form action="<?=url::site('admin/complete_schedule/'.$schedule->id)?>" method="POST">
						<?php no_csrf::set(2); ?>
						<table class="table table-striped table-hover table-extra-condensed">
										<tr>
												<th><?=Kohana::lang('backend.seat')?></th>
												<th>Name</th>
												<th><?=Kohana::lang('backend.idc')?></th>
												<th>Phone</th>
												<th><?=Kohana::lang('backend.state')?></th>
												<th>Actions</th>
										</tr>
										<?php 
										//sort the array in ascending order
										ksort($seats_and_occupants);
										foreach ($seats_and_occupants as $key=>$value)
										{ ?>
												<tr>
												
												<?php 
												//Go to ticket details table and check if missed or not.
												//to pick the right ticket, you must get the one where the missed status is 0. All others might be people who were attributed this seat, then marked absent
												$ticket = ORM::FACTORY('ticket_detail')->where('AgencyID',$this->agency_id)->where('SeatNumber',$key)->where('ScheduleID',$schedule->id)->where('MissedStatus','0')->find();?>
												
													<td>
														<?=$key?>
															<?php if(isset($value['free_ticket']) && $value['free_ticket']==1){?> <span class="badge badge-warning" title="<?=Kohana::lang('backend.free_ticket')?>: <?=$ticket->FreeTicketReason?>"><?=Kohana::lang('backend.f_for_free')?></span> <?php }?>
															<?php if($ticket->MissedStatus){?> <i class="icon-exclamation-sign" title="Absent"></i> <?php }?>
														</td>
													<td><?=$value['name']?></td>
													<td><?=$value['idc']?></td>
													<td><?=$value['phone']?></td>
													<td><?php if(isset($value['sd_price'])){echo "*SD-". get::town($value['sd_town']);}?></td>
													<?php
													$serial1 = serialize($seats_and_occupants);
													?>
													<input type="hidden" value="<?php echo htmlentities($serial1)?>" name="arr1"/>										
													<td>
														<!--<button type="submit" class="" name="print_single_ticket" value="<?=$key?>" title="Print this ticket"><i class="icon-print"></i></button>
														<button type="submit" class="trash" name="empty_seat" value="<?=$key?>" onClick="return confirm('<?=Kohana::lang('backend.confirm_emptyseat')?>?');" title="Empty this seat"><i class="icon-trash"></i></button>
														-->
														
														<div class="btn-group ">
															  <a class="btn dropdown-toggle btn-mini" data-toggle="dropdown" href="#" >
																		--<?=Kohana::lang('backend.select')?>--
															  <span class="caret"></span>
															  </a>
															  <ul class="dropdown-menu">
																<!--<li><button type="submit" class="btn btn-link" name="print_single_ticket" value="<?=$key?>" title="Print this ticket"><i class="icon-print"></i> Print ticket</button></li>
																<li><button type="button" class="btn btn-link" name="print_single_ticket" value="<?=$key?>" id="print_single_ticket"  title="Print this ticket"><i class="icon-print"></i> origingal Print ticket</button></li>-->
																
																<!--Print ticket via js so page doesn't reload-->
																<li><button type="button" class="btn btn-link" onclick="print_single_ticket_js(<?=$key?>,'<?=$schedule->id?>','<?=$schedule->departure_date?>')"><i class="icon-print"></i> <?=Kohana::lang('backend.print_ticket')?></button></li>
																<input type="hidden" value="<?=$schedule->departure_date?>" id="single_ticket_departure_date">
																<input type="hidden" value="<?=$schedule->bus_number?>" id="single_ticket_bus">
																
																
																<?php 
																//if marked as abesent, show mark as present link. else do contrary
																if($ticket->MissedStatus){?>
																	<li><button type="submit" class="btn btn-link" name="mark_present" value="<?=$key?>" onClick="return confirm('<?=Kohana::lang('backend.mark_present')?>?');" title="Mark as present"><i class="icon-ok-sign"></i> <?=Kohana::lang('backend.mark_present')?></button></li>
																<?php } else{ ?>
																	<li><button type="submit" class="btn btn-link" name="mark_absent" value="<?=$key?>" onClick="return confirm('<?=Kohana::lang('backend.mark_absent')?>?');" title="Mark as absent"><i class="icon-exclamation-sign"></i> <?=Kohana::lang('backend.mark_absent')?></button></li>
																<?php } ?>
																<?php if($schedule->update_count == 0){?>
																
																
																<li><a href='<?=url::site('admin/edit_passenger_details/'.$schedule->id."/".$key)?>' class="btn-link" title="Edit passenger information"><i class="icon-edit"></i> <?=Kohana::lang('backend.edit')?></a></li>	
																<li><button type="submit" class="btn btn-link" name="add_to_favourites" value="<?=$key?>" onClick="return confirm('<?=Kohana::lang('backend.add_to_favourites')?>');" title="Add client to favourites"><i class="icon-star"></i> <?=Kohana::lang('backend.favourite')?></button></li>

																<li><button type="submit" class="btn btn-link" name="empty_seat" value="<?=$key?>" onClick="return confirm('<?=Kohana::lang('backend.confirm_emptyseat')?>?');" title="Empty this seat"><i class="icon-trash"></i> <?=Kohana::lang('backend.empty_seat')?></button></li>	
																<?php } ?>
															</ul>
														</div>
													</td>
												</tr>
											
										<?php } ;?><?php 
										//sort the array in ascending order
										ksort($seats_and_reservers); 
										//serialize so it can be posted
										$serial = serialize($seats_and_reservers); ?>
													
										<input type="hidden" value="<?php echo htmlentities($serial)?>" name="arr"/>
										
										<?php
										foreach ($seats_and_reservers as $r_key=>$value)
										{ ?>
												<tr>
													<td><?=$r_key?></td>
													<td><?=$value['name']?></td>
													<td><?=$value['idc']?></td>
													<td><?=$value['phone']?></td>
													<td><?=Kohana::lang('backend.reserved')?> </td> 
													<td>
														<button type="submit" id="accept"name="res_to_occ" value="<?=$r_key?>" title="<?=Kohana::lang('backend.confirm')?>"></button> | 
														<button type="submit" id="reject" name="reject_reserve" value="<?=$r_key?>" onClick="return confirm('<?=Kohana::lang('backend.refuse_reserve')?>?');" title="Delete"></button>
													</td>
												</tr>
										<?php } ;?>
									</table>
								</form>
					 <?php }?>	
			
			</div>
			<div class="span12 hero-unit prof-unit" style="margin-left:0px;" >
				<?=Kohana::lang('backend.created_by')?>: <i><?=$schedule->CreatedBy?></i> &nbsp;
			
		
			</div>
			
		</div>
					
				
					
			<script>
			  $(function () {
				$('#myTab a').click(function (e) {
			  e.preventDefault();
			  $(this).tab('show');
			})
			  })
			  
			</script>
					
		<div class="span4" >				
		<div class="span12 hero-unit prof-unit"   >				
				<div class="heading"><?=Kohana::lang('backend.bus_plan')?> <div class="view" style="float:right;"> &nbsp; Available: <b><?=$seats_left?></b></div> &nbsp; <div class="view" style="float:right;"> Occupied: <b><?=$seats_taken?></b></div></div>
				<div class="rule"><hr/></div>
				<?php $number=$schedule->bus_seats;
				//check number of seats to know which bus plan to display
				if ($number == 70){include 'application/views/bus_plans/70_seater.php';}
				elseif ($number == 55){include 'application/views/bus_plans/55_seater.php';}
				elseif ($number == 48){include 'application/views/bus_plans/48_seater.php';}
				elseif ($number == 39){include 'application/views/bus_plans/39_seater.php';}
				elseif ($number == 35){include 'application/views/bus_plans/35_seater.php';}
				elseif ($number == 30){include 'application/views/bus_plans/30_seater.php';}
				else{echo "Unavailable";}
				?>							
		</div>			
		<div class="span12 hero-unit prof-unit" style="margin-left:0px;">
			<form action="<?=url::site('expenses/quick_add_expense')?>" method="POST">
				<div class="heading"><?=Kohana::lang('backend.expenses_sched')?></div>
				<div class="rule"><hr/></div>
				<table class="table table-striped table-condensed">
					<!--Start by showing those fixed  expenses. Just showing. They are only deducted after checkout
						Only show these when the schedule has not been re-made current from checked out.
						Else it will show the expense first, as a fixed expense, then show it again as an expense already on this schedule.
						Though harmless, it's confusing.
					-->
					<?php
					if($schedule->update_count == 0){
					
					if(count($fixed_percentage_expenses)!=0)
						foreach ($fixed_percentage_expenses as $exp){
					?>
					<tr>
						<td class="span8"><small><? if (strlen($exp->Name) > 25) { echo substr($exp->Name,0,23)."...";} else { echo $exp->Name;}?></small></td>
						<td class="span4 text-right"><small><?=number_format($exp->Value)." %";?></small></td>
					</tr>
					<?php } ?>
					<?php
					if(count($all_fixed_amt_expenses)!=0)
						foreach ($all_fixed_amt_expenses as $exp){
					?>
					<tr>
						<td class="span8"><small><? if (strlen($exp->Name) > 25) { echo substr($exp->Name,0,23)."...";} else { echo $exp->Name;}?></small></td>
						<td class="span4 text-right"><small><?=number_format($exp->Value);?></small></td>
					</tr>
					<?php } 
					}
					?>
					
					<?php 
						foreach ($expenses_for_schedule as $exp){
						
					?>
					<tr>
						<td class="span8"><small><? if (strlen($exp->purpose) > 25) { echo substr($exp->purpose,0,23)."...";} else { echo $exp->purpose;}?></small></td>
						<td class="span4 text-right"><small><?=number_format($exp->amount);?></small></td>
					</tr>
					
					<?php } ?>
				</table> 
				
				<div class="rule"><hr/></div>
					<input class="span6" type="text" name="exp_purpose" placeholder="<?=Kohana::lang('backend.purpose')?>" required>
					<input class="span3 IsAmount" type="text" name="exp_amount" placeholder="<?=Kohana::lang('backend.amount')?>" required>
					<!--A hidden field for the bus number and schedule id to redirect back to -->
					<input class="span4" type="hidden" name="bus_number" value="<?=$schedule->bus_number?>" >
					<input class="span4" type="hidden" name="sched_id" value="<?=$schedule->id?>" >
					<button type="submit" class="btn btn-info" title="Quick-add expense"><?=Kohana::lang('backend.add')?></button>
			</form>
			</div>
		</div>
						
					<span class="loaddiv">
						<!-- DON'T DELETE THIS DIV. IT'S FOR THE ONLINE TRANSACTIONS-->	
					</span>
					
					</div>
	  
<?php //$this->profiler = new Profiler();?>
	
	  
	<script type="text/javascript"> 
		$(document).ready(function(){
			//focus on entering a new ticket name, and scroll slightly down for this
			$( "#client_name" ).focus();
			$('html, body').animate({
			   scrollTop: $('#complete_schedule_form').offset().top 
			 }, 'fast');
			
			
			//show special drop town and price if checked, if not hide it
			$('#special_drop').change(function(){
				if(this.checked){
					$('#special-drop-price').show(150);					
					$('#special-drop-town').show(150);
					$('#ft-reason').hide(150);
					
					//if he selects special drop, disactivate free ticket
					$('#ft').prop('checked', false);
					
					}else{
					$('#special-drop-price').hide(150);	
					$('#special-drop-town').hide(150);
					}
			
			});
			
			$('#ft').change(function(){
				if(this.checked){
					//if he selects free-ticket, disactivate special drop, and hide its fields
					//Show special drop reason
					$('#ft-reason').show(150);	
					$('#special_drop').prop('checked', false);
					$('#special-drop-price').hide(150);	
					$('#special-drop-town').hide(150);
				}else
				{
					$('#ft-reason').hide(150);
				}
			
			});
			
		});
		
	</script>
	<script type="text/javascript">
	$(document).ready(function(){
		$('.datepicker').datepicker();
		
			setInterval(function(){
			//$('#res_requests').empty();
			$('#res_requests').load('<?=url::site('reserve/reservation_details/'.$schedule->id);?>');
			$('.res_count').load('<?=url::site('reserve/reservation_count/'.$schedule->id);?>');
			//$('.loaddiv').load('<?=url::site('online/index');?>');
			}, 60000);
		
	})
	
	
	
	
	//Print ticket via js to avoid unecessary page reloads.
	function print_single_ticket_js(seat_number, schedule_id, departure_date)
	{
		$.post("<?=url::site('tickets/print_single');?>",
		{	
			seat_number: seat_number,
			departure_date: departure_date,
			schedule_id: schedule_id
		},
		function(data, status){
			win=window.open(data,'Print ticket','width=900,height=400');
			win.focus();
		});		
	};
	
	/*Populate bus via js to avoid unecessary page reloads.
	function complete_schedule_js(seat_number, bus_number, departure_date)
	{	
		var data_in = $('#complete_schedule_form').serialize();
		alert(data_in);
		$.post("<?=url::site('admin/complete_schedule/38');?>",
		{	
			data_in: data_in
			
		},
		function(data, status){
			alert(data);
		});		
	}; */
	
	
	// Display bus plan
	$(document).ready(function()
	{
		var number_seats = "<?php echo $number; ?>";
		for (i=0; i <= number_seats; i++){
		if( (i!=1) && typeof jsar[i] != 'undefined'  ){
			$("td.plan:eq("+(i-1)+")").addClass('taken-seat');
			}
			} 		
	})
</script>
  
  
  
  
  <script>
//3 way confirmation popup to send sms on boarding or not  
/*
$('#boarding').click(function(){
				
	var domain = '<?=url::base()?>';		
	var schedule_id = '<?=$schedule->id;?>';	
    $("#confirm-boarding").dialog({
      resizable: true,
      modal: true,
      buttons: {
        "Yes": function() {
          window.location.href=domain+'admin/start_boarding_schedule_with_sms/'+schedule_id;
        },
		"No": function() {
          window.location.href=domain+'admin/start_boarding_schedule_without_sms/'+schedule_id;
        },
        Cancel: function() {
          $( this ).dialog( "close" );
        }
      }
    });
 }); */
 </script>
 
<!--INCLUDE ADDITIONAL FILES FOR THE AUTOFILL-->
	<!--<link rel="stylesheet" href="//code.jquery.com/ui/1.11.4/themes/smoothness/jquery-ui.css">-->
	<link rel="stylesheet" href="<?=url::base('')?>css/jquery-ui-1.11.4.css">
	<script src="<?=url::base('')?>js/jquery-ui-1.11.4.js"></script>
	<!--<script src="//code.jquery.com/jquery-1.10.2.js"></script>-->

	<!--<link rel="stylesheet" href="/resources/demos/style.css"> -->
<script> 
$(function() {

	
	
	var favourite_contacts = <?=$favourite_contacts?>;
	//Autofill for the main form
	function autofill( item ) {
		
		//don't fill with zeroes
		if(item.phone != 0){$("#client_phone").val( item.phone );}
		if(item.idc != 0){$("#client_idc").val( item.idc ); }
		$("#client_email").val( item.email );
    }
    $( "#client_name" ).autocomplete({
      source: favourite_contacts,
	  minLength: 2,
	  select: function( event, ui ) {
      	   autofill( ui.item ? ui.item : " "); 
     // source: 'contacts.txt'
	  }
    });
	
	//Autofill for the reservations form
	function autofill_res( item ) {
		
		//don't fill with zeroes
		if(item.phone != 0){$("#res_phone").val( item.phone );}
		if(item.idc != 0){$("#res_idc").val( item.idc ); }
		$("#res_email").val( item.email );
    }
    $( "#res_name" ).autocomplete({
      source: favourite_contacts,
	  minLength: 2,
	  select: function( event, ui ) {
      	   autofill_res( ui.item ? ui.item : " "); 
     // source: 'contacts.txt'
	  }
    });
 
 
});
  </script> 
  

