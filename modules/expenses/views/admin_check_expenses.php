<?php defined('SYSPATH') or die('No direct script access'); ?> 


	<?php
		//determine what type of notice to display if at all
		$notice = $this->session->get_once('notice');
			if(!empty($notice)){ 
				if($notice['type'] == 'success'){?><div class="alert general success"><a class="close" data-dismiss="alert" href="#">&times;</a><?php echo $notice['message']; }?></div>
			<?	if($notice['type'] == 'error'){?><div class="alert general error"><a class="close" data-dismiss="alert" href="#">&times;</a><?php echo $notice['message']; }?></div>
		<?}?>
    <div class="container">
	
	  	
	<div class="row-fluid">	
		
		<div class="span12 hero-unit prof-unit">
		
		<ul class="nav nav-tabs">
					<li > <a href="<?=url::site('expenses/all')?>"><i class="icon-arrow-left"></i> <?=Kohana::lang('backend.back_expenses')?></a></li>
		</ul>
		
		<div class="row-fluid">	
		
				
				
				<?php 
						//if it was a search, and if the result wasn't zero
						if((isset($bus_expenses)) AND ($count_expenses != 0)){
							$total_expenses = 0;
						?>
							<pre><?=Kohana::lang('backend.search_results')?>: <?=Kohana::lang('backend.exp_for')?> <b><?=$bus_number?></b></pre>
							
							<div class="clear"></div>
							<table class="table  table-striped table-hover table-condensed">
										<tr>
											<th>Date</th>
											<th><?=Kohana::lang('backend.amount')?></th>
											<th><?=Kohana::lang('backend.purpose')?></th>
											<th><?=Kohana::lang('backend.bus_no')?></th>
											<th><?=Kohana::lang('backend.registered_by')?></th>
											<th><?=Kohana::lang('backend.authorised_by')?></th>
										</tr>
										<?php foreach ($bus_expenses as $expense):?>
											<tr>	
												<td><?=date("d-m-Y",strtotime($expense->date_incurred))?></td>
												
												<td class="text-right"><?=number_format($expense->amount)?></td>
												
												<!--Trim if the description is too long. -->
													<td><a href="<?=url::site('expenses/details/'.$expense->id)?>"> 
														<?php if (strlen($expense->purpose) > 25) { echo substr($expense->purpose,0,35)."...";} else { echo $expense->purpose;}?>
													</a></td>
												
												
												<td><?=$expense->bus_number?></td>
												<td><?=$expense->CreatedBy?></td>
												<td><?=$expense->authorised_by?></td>
												<?php 
													//sum the expenses
													$total_expenses += $expense->amount; 
												?>
											</tr>
										<?php endforeach; ?>
											<tr>
												<th><?=Kohana::lang('backend.total')?></th>
												<th><?=number_format($total_expenses)?> FCFA </th>
												
											</tr>
							</table>
								
									<?php }
									
						//if it was a search, and if the result wasn't zero
						if(isset($agency_expenses_by_date) AND ($count_expenses != 0)){
							$total_expenses = 0;
						?>
							<pre><?=Kohana::lang('backend.search_results')?>: <?=Kohana::lang('backend.exp_on')?> <b><?=date("d-m-Y",strtotime($date))?></b></pre>
							
							<div class="clear"></div>
							<table class="table  table-striped table-hover">
										<tr>
											<th>Date</th>
											<th><?=Kohana::lang('backend.amount')?></th>
											<th><?=Kohana::lang('backend.purpose')?></th>
											<th><?=Kohana::lang('backend.bus_no')?></th>
											<th><?=Kohana::lang('backend.registered_by')?></th>
											<th><?=Kohana::lang('backend.authorised_by')?></th>
										</tr>
										<?php foreach ($agency_expenses_by_date as $expense):?>
											<tr>	
												<td><?=date("d-m-Y",strtotime($expense->date_incurred))?></td>
												
												<td><?=number_format($expense->amount)?></td>
												
												<!--Trim if the description is too long. -->
													<td><a href="<?=url::site('expenses/details/'.$expense->id)?>"> 
														<?php if (strlen($expense->purpose) > 25) { echo substr($expense->purpose,0,35)."...";} else { echo $expense->purpose;}?>
													</a></td>
												
												
												<td><?=$expense->bus_number?></td>
												<td><?=$expense->CreatedBy?></td>
												<td><?=$expense->authorised_by?></td>
												<?php 
													//sum the expenses
													$total_expenses += $expense->amount; 
												?>
											</tr>
										<?php endforeach; ?>
											<tr>
												<th><?=Kohana::lang('backend.total')?></th>
												<th><?=number_format($total_expenses)?> FCFA </th>
												
											</tr>
							</table>
								
									<?php }?>	
				
			
			
		
		
			
			
				
				
		</div>
	
		
		</div>
	
    </div> 

	</div>
	
	<script type="text/javascript">

	
	</script>