<?php defined('SYSPATH') or die('No direct script access');

//Almost all functions will take as parameter, the id of the agency we are dealing with
//in order not to interfere with another's stuff.

class Cash_Controller extends Admin_Controller {
			
	
	public function __construct()
	{	
		parent::__construct();
		$this->session = Session::instance();
	}  
	
	
	public function cash_to_bank()
	{	
		Authlite::check_admin();
		Authlite::verify_referer();
		
		$this->template->title = Kohana::lang('backend.cash');
		//$admins_excluding_current = Admin_Model::get_admins_excluding_current($this->agency_id, $this->admin->username);
		//$view->admins_excluding_current=$admins_excluding_current;
		
			$ctb = Cash_To_Bank_Model::get_all($this->agency_id);
			//$cib = Cash_Interbranch_Model::get_all($this->agency_id);
			$per_page = 15;
					
			$all_ctb_total = count($ctb);
			
			$this->pagination = new Pagination(array(
				// 'base_url'    => 'welcome/pagination_example/page/', // base_url will default to current uri
				'uri_segment'    => 'page', // pass a string as uri_segment to trigger former 'label' functionality
				'total_items'    => $all_ctb_total, // use db count query here of course
				'items_per_page' => $per_page , // it may be handy to set defaults for stuff like this in config/pagination.php
				'style'          => 'punbb', // pick one from: classic (default), digg, extended, punbb, or add your own!
				//'query_string'   => "ORM::factory('schedule')->where('agency_id',$agency_id)->where('status','current')->find_all()"
			));
				
			$ctb=ORM::factory('cash_to_bank')->where('agency_id',$this->agency_id)->where('deleted','0')->orderby('created_on','DESC')->limit($per_page,$this->pagination->sql_offset)->find_all();	
			$view->collections=$ctb;
		
		$view = new View('cash_to_bank');
		$view->ctb = $ctb;
		$this->template->content = $view;
	}
	
	
	public function collections()
	{	
		Authlite::check_admin();
		Authlite::verify_referer();
		
		$this->template->title = Kohana::lang('backend.cash');
		$admins_excluding_current = Admin_Model::get_admins_excluding_current($this->agency_id, $this->admin->username);
		$all_active_admins = Admin_Model::get_all_active_admins($this->agency_id);
		
		
		$view = new View('cash_collections');
		$view->admins_excluding_current = $admins_excluding_current;
		$this->template->content = $view;
	}	
	
	public function balances()
	{	
		Authlite::check_admin();
		Authlite::verify_referer();
		
		$this->template->title = Kohana::lang('backend.cash');
		$all_active_admins = Admin_Model::get_all_active_admins($this->agency_id);
		//Branch balance is the sum of all admin balances
		$branch_balance = 0;
		foreach($all_active_admins as $admin){
			$branch_balance += $admin->available_balance;
		}
		
		
		$view = new View('cash_balances');
		$view->all_active_admins = $all_active_admins;
		$view->branch_balance = $branch_balance;
		$this->template->content = $view;
	}
	
	public function interbranch()
	{	
		Authlite::check_admin();
		Authlite::verify_referer();
		
		$this->template->title = Kohana::lang('backend.cash_interbranch');
		//$admins_excluding_current = Admin_Model::get_admins_excluding_current($this->agency_id, $this->admin->username);
		//$view->admins_excluding_current=$admins_excluding_current;
		
		//$ctb = Cash_To_Bank_Model::get_all($this->agency_id);
		$cib = Cash_Interbranch_Model::get_all($this->agency_id);
		$per_page = 15;
				
		//$all_ctb_total = count($ctb);
		$all_cib_total = count($cib);
		
		$this->pagination = new Pagination(array(
			// 'base_url'    => 'welcome/pagination_example/page/', // base_url will default to current uri
			'uri_segment'    => 'page', // pass a string as uri_segment to trigger former 'label' functionality
			'total_items'    => $all_cib_total, // use db count query here of course
			'items_per_page' => $per_page , // it may be handy to set defaults for stuff like this in config/pagination.php
			'style'          => 'punbb', // pick one from: classic (default), digg, extended, punbb, or add your own!
			//'query_string'   => "ORM::factory('schedule')->where('agency_id',$agency_id)->where('status','current')->find_all()"
		));
			
		$cib=ORM::factory('cash_interbranch')->where('agency_id',$this->agency_id)->where('deleted','0')->orderby('created_on','DESC')->limit($per_page,$this->pagination->sql_offset)->find_all();	
		
		$view = new View('cash_interbranch');
		$view->cib = $cib;
		$this->template->content = $view;
	}
	
	public function collection_by_admin()
	{	
		Authlite::check_admin();
		Authlite::verify_referer();
		
		$agency_id = $this->agency_id;
		$parent_id = get::_parent($agency_id)->id;
		$type = $this->admin->admin_group_id;
		
		$this->template->title = Kohana::lang('backend.welcome_backend');
		$view = new View('collection_by_admin');
		if($_POST)
		{
			$post = new Validation($_POST);
			$post->add_rules('date','required');
			$post->add_rules('admin','required');
			if ($post->validate())
			{	
				
				$start_date = date("Y-m-d",strtotime($_POST['date']));
				$end_date = date("Y-m-d",strtotime($_POST['date']));
				$admin_name=$_POST['admin'];
			
				$period_parcels=Parcel_Model::get_agency_parcels_by_period($agency_id,$start_date,$end_date,$admin_name);
				$period_expenses=Expense_Model::get_agency_expenses_by_period_by_admin($agency_id,$start_date,$end_date,$admin_name);
				$period_schedules=Schedule_Model::get_agency_schedules_by_period_by_admin($agency_id,$start_date,$end_date,$admin_name);
				$period_schedules=Ticket_Detail_Model::get_agency_tickets_by_period_by_admin($agency_id,$start_date,$end_date,$admin_name);
				$period_income=Income_Model::get_agency_income_by_period_by_admin($this->agency_id,$start_date,$end_date,$admin_name);
								
				//calculate the total expenses for that period
				$total_expenditure = 0;
				foreach($period_expenses as $exp )
				{
					$total_expenditure += $exp->amount;
				}
			
				//Calculate total amount expected from departed buses for the period. DEPARTED BUSES ONLY
				$total = 0;
				foreach($period_schedules as $ds)
				{
					$total += $ds->Price;
				}
					
			$collections = Collection_Model::get_collections_from_admin($admin_name);
			$per_page = 15;
					
			$all_collections_total = count($collections);
			
			$this->pagination = new Pagination(array(
				// 'base_url'    => 'welcome/pagination_example/page/', // base_url will default to current uri
				'uri_segment'    => 'page', // pass a string as uri_segment to trigger former 'label' functionality
				'total_items'    => $all_collections_total, // use db count query here of course
				'items_per_page' => $per_page , // it may be handy to set defaults for stuff like this in config/pagination.php
				'style'          => 'punbb', // pick one from: classic (default), digg, extended, punbb, or add your own!
				//'query_string'   => "ORM::factory('schedule')->where('agency_id',$agency_id)->where('status','current')->find_all()"
			));
				
			$collections=ORM::factory('collection')->where('agency_id',$this->agency_id)->where('deleted','0')->where('admin_id',$admin_name)->orderby('CreatedOn','DESC')->limit($per_page,$this->pagination->sql_offset)->find_all();	
			$view->collections=$collections;	
			
				}
				else
				{
					$errors=$post->errors('errors');
					$notice="";
						foreach($errors as $error) 
						{
							$notice.=$error."<br />";
						}
						$this->session->set('notice', array('message'=>$notice,'type'=>'error'));
						url::redirect('admin/main/'.$this->admin->agency_id);
				}
				
				$admin = Admin_Model::get_by_username($admin_name);
				
				
				$view->parcels=$period_parcels;
				$view->total_expenditure=$total_expenditure;
				$view->period_parcels = $period_parcels;
				$view->period_expenses = $period_expenses;
				$view->period_schedules = $period_schedules;
				$view->period_income = $period_income;
				$view->total = $total;
				$view->start_date = $start_date;
				$view->end_date = $end_date;
				$view->admin = $admin;
		}
		
		$this->template->content = $view;
	
	}
	
	public function add_cash_to_bank()
	{	
		Authlite::check_admin();
		//Authlite::verify_referer();
		
		$agency_id = $this->agency_id;
		$parent_id = get::_parent($agency_id)->id;
		$type = $this->admin->admin_group_id;
		
		$this->template->title = Kohana::lang('backend.welcome_backend');
		
		
		if($_POST)
		{
			$post = new Validation($_POST);
			$post->add_rules('date','required');
			$post->add_rules('bank_name','required');
			$post->add_rules('account_number','required');
			$post->add_rules('amount','required');
			if ($post->validate())
			{	
				
				$date = date("Y-m-d",strtotime($_POST['date']));
				$current_date = date('Y-m-d H:i:s');
				$bank_name=$_POST['bank_name'];
				$account_number=$_POST['account_number'];
				$amount=$_POST['amount'];
			
				if($this->admin->available_balance >= $amount)
				{	//make sure it's a positive amount
					if($amount>0)
					{
						Cash_To_Bank_Model::add($this->agency_id, $bank_name, $account_number, $amount, $date, $this->admin->username, $current_date );
						$this->session->set('notice', array('message'=>Kohana::lang('backend.saved'),'type'=>'success'));	
						url::redirect('cash');
					}else
					{
						$this->session->set('notice', array('message'=>Kohana::lang('backend.invalid_amount') ,'type'=>'error'));
						url::redirect('cash/add_cash_to_bank');
					}	
				}else
				{
					$this->session->set('notice', array('message'=>Kohana::lang('backend.collection_amount_mismatch') ,'type'=>'error'));
					url::redirect('cash/add_cash_to_bank');
				}	
				}
				else
				{
					$errors=$post->errors('errors');
					$notice="";
						foreach($errors as $error) 
						{
							$notice.=$error."<br />";
						}
						$this->session->set('notice', array('message'=>$notice,'type'=>'error'));
						url::redirect('cash');
				}
				
			
		}
		$view = new View('add_cash_to_bank');
		$this->template->content = $view;
	}
	
	public function send_cash_interbranch()
	{	
		Authlite::check_admin();
		//Authlite::verify_referer();
		
		$agency_id = $this->agency_id;
		$parent_id = get::_parent($agency_id)->id;
		$siblings = get::agency_siblings($agency_id,$parent_id);	
		//$type = $this->admin->admin_group_id;
		
		$this->template->title = Kohana::lang('backend.welcome_backend');
		
		
		if($_POST)
		{
			$post = new Validation($_POST);
			$post->add_rules('date','required');
			$post->add_rules('receiver_agency_id','required');
			$post->add_rules('description','required');
			$post->add_rules('amount','required');
			if ($post->validate())
			{	
				$date = date("Y-m-d",strtotime($_POST['date']));
				$current_date = date('Y-m-d H:i:s');
				$receiver_agency_id=$_POST['receiver_agency_id'];
				$description=$_POST['description'];
				//$account_number=$_POST['account_number'];
				$amount=$_POST['amount'];
			
				if($this->admin->available_balance >= $amount)
				{	//make sure it's a positive amount
					if($amount>0)
					{
						Cash_Interbranch_Model::add($this->agency_id, $receiver_agency_id, $amount, $description, $date, $this->admin->username, $current_date );
						$this->session->set('notice', array('message'=>Kohana::lang('backend.saved'),'type'=>'success'));	
						url::redirect('cash/cash_interbranch');
					}else
					{
						$this->session->set('notice', array('message'=>Kohana::lang('backend.invalid_amount') ,'type'=>'error'));
						url::redirect('cash/add_cash_to_bank');
					}	
				}else
				{
					$this->session->set('notice', array('message'=>Kohana::lang('backend.collection_amount_mismatch') ,'type'=>'error'));
					url::redirect('cash/add_cash_interbranch');
				}	
				}
				else
				{
					$errors=$post->errors('errors');
					$notice="";
						foreach($errors as $error) 
						{
							$notice.=$error."<br />";
						}
						$this->session->set('notice', array('message'=>$notice,'type'=>'error'));
						url::redirect('cash');
				}
				
			
		}
		$view = new View('send_cash_interbranch');
		$view->siblings=$siblings;
		$this->template->content = $view;
	}
	
	public function receive_cash_interbranch()
	{	
		Authlite::check_admin();
		//Authlite::verify_referer();
		
		$agency_id = $this->agency_id;
		$parent_id = get::_parent($agency_id)->id;
		$siblings = get::agency_siblings($agency_id,$parent_id);	
		//$type = $this->admin->admin_group_id;
		
		$this->template->title = Kohana::lang('backend.welcome_backend');
		
		
		if($_POST)
		{
			$post = new Validation($_POST);
			$post->add_rules('date','required');
			$post->add_rules('branch_to_id','required');
			$post->add_rules('description','required');
			$post->add_rules('amount','required');
			if ($post->validate())
			{	
				$date = date("Y-m-d",strtotime($_POST['date']));
				$current_date = date('Y-m-d H:i:s');
				$branch_to_id=$_POST['branch_to_id'];
				$description=$_POST['description'];
				//$account_number=$_POST['account_number'];
				$amount=$_POST['amount'];
			
				if($this->admin->available_balance >= $amount)
				{	//make sure it's a positive amount
					if($amount>0)
					{
						Cash_Interbranch_Model::add($this->agency_id, $branch_to_id, $amount, $description, $date, $this->admin->username, $current_date );
						$this->session->set('notice', array('message'=>Kohana::lang('backend.saved'),'type'=>'success'));	
						url::redirect('cash/cash_interbranch');
					}else
					{
						$this->session->set('notice', array('message'=>Kohana::lang('backend.invalid_amount') ,'type'=>'error'));
						url::redirect('cash/add_cash_to_bank');
					}	
				}else
				{
					$this->session->set('notice', array('message'=>Kohana::lang('backend.collection_amount_mismatch') ,'type'=>'error'));
					url::redirect('cash/add_cash_interbranch');
				}	
				}
				else
				{
					$errors=$post->errors('errors');
					$notice="";
						foreach($errors as $error) 
						{
							$notice.=$error."<br />";
						}
						$this->session->set('notice', array('message'=>$notice,'type'=>'error'));
						url::redirect('cash');
				}
				
			
		}
		$view = new View('receive_cash_interbranch');
		$view->siblings=$siblings;
		$this->template->content = $view;
	}
	
	public function register_collection()
	{	
		Authlite::check_admin();
		Authlite::verify_referer();
		
		if($_POST)
		{	
			$post = new Validation($_POST);
			$post->add_rules('admin_username','required');
			$post->add_rules('admin_available_balance','required');
			$post->add_rules('amount','required','valid::numeric');
						
			if ($post->validate())
			{	
				$amount = $_POST['amount'];
				$admin_concerned_username = $_POST['admin_username'];
				$admin_available_balance = $_POST['admin_available_balance'];
				$CreatedBy = strtoupper($this->admin->username);
				
				//ensure that the amount collected cannot be more than the amount available
				if($admin_available_balance >= $amount)
				{
					Collection_Model::register_collection($this->agency_id, $admin_concerned_username, $admin_available_balance, $amount, $CreatedBy);
					
					$this->session->set('notice', array('message'=>Kohana::lang('backend.collection_saved') ,'type'=>'success'));
					url::redirect('cash');	
					
				}else
				{
					$this->session->set('notice', array('message'=>Kohana::lang('backend.collection_amount_mismatch') ,'type'=>'error'));
					url::redirect('cash');
				}			
			}
			else
			{
				$errors=$post->errors('errors');
				$notice="";
					foreach($errors as $error) 
					{
						$notice.=$error."<br />";
					}
					$this->session->set('notice', array('message'=>$notice,'type'=>'error'));
					url::redirect('cash');	
			}
		}
	}
	
	public function other_income()
	{
		Authlite::check_admin();
		Authlite::verify_referer();
		
		$agency_id = $this->agency_id;
		$this->template->title = Kohana::lang('backend.all_buses');;
		$parent_id = get::_parent($this->agency_id)->id;		
		
		//pagination
		$per_page = 15;		
		$other_income = ORM::factory('income')->where('agency_id',$agency_id)->where('purpose','bus_rental')->orderby('date_incurred','DESC')->find_all();				
		$other_income_total = count($other_income);
		
		$this->pagination = new Pagination(array(
			// 'base_url'    => 'welcome/pagination_example/page/', // base_url will default to current uri
			'uri_segment'    => 'page', // pass a string as uri_segment to trigger former 'label' functionality
			'total_items'    => $other_income_total, // use db count query here of course
			'items_per_page' => $per_page , // it may be handy to set defaults for stuff like this in config/pagination.php
			'style'          => 'punbb', // pick one from: classic (default), digg, extended, punbb, or add your own!
			//'query_string'   => "ORM::factory('schedule')->where('agency_id',$agency_id)->where('status','current')->find_all()"
		));
			
		$other_income_per_page=ORM::factory('income')->where('agency_id',$agency_id)->where('purpose','other')->orderby('start_date','DESC')->limit($per_page,$this->pagination->sql_offset)->find_all();
		
		$view = new View('cash_other_income');
		$view->other_income_per_page = $other_income_per_page;
		$this->template->content = $view;
	}
	
	public function add_other_income()
	{	
		Authlite::check_admin();
		Authlite::verify_referer();
		
		$agency_id = $this->agency_id;
		//$parent_id = get::_parent($agency_id)->id;
		//$admin_group_id = $this->admin->admin_group_id;
		$this->template->title=Kohana::lang('backend.add_income');
		$all_buses = Bus_Model::get_all($this->agency_id);	
	
		if($_POST)
		{	
			
			$post = new Validation($_POST);
			$post->add_rules('date_incurred','required');
			$post->add_rules('amount','required','valid::numeric');
						
			if ($post->validate())
			{	
				$bus_number = strtoupper(str_replace(" ","",$_POST['bus_number']));
				$amount = $_POST['amount'];
				$description = htmlentities($_POST['description'],ENT_QUOTES);
				$date_incurred = date("Y-m-d",strtotime($_POST['date_incurred']));

				$CreatedBy = strtoupper($this->admin->username);			
				
				//Add income
				Income_Model::add_other_income($this->agency_id, $bus_number, $amount, $description, $date_incurred, strtoupper($this->admin->username));
				
				//Make bus
				
				$this->session->set('notice', array('message'=>Kohana::lang('backend.saved') ,'type'=>'success'));
				url::redirect('cash/other_income');	
							
			}
			else
			{
				$errors=$post->errors('errors');
				$notice="";
					foreach($errors as $error) 
					{
						$notice.=$error."<br />";
					}
					$this->session->set('notice', array('message'=>$notice,'type'=>'error'));
					url::redirect('cash/other_income');	
			}
		}
		
		$view = new View('cash_add_other_income');
		$view->all_buses = $all_buses;
		$this->template->content = $view;
	
	}
}