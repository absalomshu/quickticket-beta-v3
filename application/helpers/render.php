<?php defined('SYSPATH') or die('No direct script access');

/**
 * Description:  renders content (views), js and CSS
 * wasaMUNDI.com
 *
 * @package		 wasamundi
 * @subpackage 	 Helper
 * @author 		 Wasamundi Team
 * @copyright    (c) 2012 Wasamundi
 * @license 	 Wasamundi
 */
 
class render_Core
{
	/**
	 * Renders header
	 * 
	 * @param  string  town
	 * @return  null
	 */
	public function template_header()
	{
		View::factory('template/header')->render(TRUE);
	}
	

	/**
	 * Renders main Footer
	 * 
	 * @return  null
	 */
	public function template_footer()
	{
		View::factory('template/footer')->render(TRUE);
	}
	
	public function frontend_footer()
	{
		View::factory('template/frontend_footer')->render(TRUE);
	}
	
	public function notices()
	{
		View::factory('template/notices')->render(TRUE);
	}
	
	/**
	 * Renders sub template header banner
	 * 
	 * @return  null
	 */
	public function admin_header()
	{
		View::factory('template/admin_header')->render(TRUE);
	}
	

	/**
	 * Renders breadcrumb
	 * 
	 * @return  null
	 */
	public function breadcrumb()
	{
		View::factory('template/breadcrumb')->render(TRUE);
	}
	public function front_breadcrumb()
	{
		View::factory('template/front_breadcrumb')->render(TRUE);
	}
	public function menu()
	{
		View::factory('template/menu')->render(TRUE);
	}
	public function sub_menu()
	{
		View::factory('template/sub_menu')->render(TRUE);
	}
	
	
	
	
	
	

}
