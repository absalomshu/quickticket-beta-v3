<?php defined('SYSPATH') or die('No direct script access'); ?> 
	

 <div class="container">
	<div class="row-fluid " style="margin:5px 0 0px 0px;">
	  	
		<div class="span12 hero-unit prof-unit" >
		<legend>Payment details</legend>
	<?php 
		//This is done coz the query returns a mysql result even for one result.
	
		foreach($purchase as $p){$purchase = $p;}?>
		<div class="span7">
			<div class="">
				<div class="span1"><?=Kohana::lang('backend.name')?>: </div>
				<div class="span5"><input type="text"  name="authorised_by" class="span11" value="<?=$purchase->client_name?>" disabled></div>
				<div class="span1"><?=Kohana::lang('backend.phone')?>: </div>
				<div class="span4"><input type="text"  name="authorised_by" class="input-medium" value="<?=$purchase->client_phone?>" disabled></div>
				<div class="clear"></div>
			</div>
			<div class="">
				<div class="span1"><?=Kohana::lang('backend.email')?>: </div>
				<div class="span5"><input type="text"  name="authorised_by" class="span11" value="<?=$purchase->client_email?>" disabled ></div>
				<div class="span1"><?=Kohana::lang('backend.idc')?>: </div>
				<div class="span4"><input type="text"  name="authorised_by" class="input-medium" value="<?=$purchase->client_idc?>" disabled></div>
			</div>
		<div class="clear"></div>
		<br/>
		
		<b style="margin-left:0px;">Means of payment</b>
		<ul class="nav nav-tabs" id="myTab">
					<li class="active"><a href="#credit-card"><i class="icon-barcode"></i> <?=Kohana::lang('backend.credit_card')?></a></li>
					<li ><a href="#bank-account"><i class="icon-home"></i> <?=Kohana::lang('backend.bank_account')?></a></li>
		</ul>
		
		<div class="tab-content">
			<div class="tab-pane active" id="credit-card">
				<form action="<?=url::site('payment/complete_payment')?>" method="POST" name="theform" class="span10 offset1">
				
				<?php 
					if($is_open_ticket){$amount_payable= $purchase->ot_ticket_price;}else{$amount_payable=$purchase->ticket_price;}
					
					//Conversion to USD
					/*$get = file_get_contents("https://www.google.com/finance/converter?a=$amount_payable&from=XOF&to=USD");
					$get = explode("<span class=bld>",$get);
					$get = explode("</span>",$get[1]);  
					$amount_payable_converted = preg_replace("/[^0-9\.]/", null, $get[0]);
					$amount_payable_converted = (Float)$amount_payable_converted;
					//Float so it removes trailing zeroes which otherwise interrupt the payment */
			
			
		
			
					
				?>
				<input type="hidden" name = 'amount_payable'  value="<?=$amount_payable?>">
				<input type="hidden" name = 'purchase_id'  value="<?php if($is_open_ticket){echo $purchase->id;}else{echo $purchase->purchase_id;} ?>">
						<div class="form-actions">
							<div class="span4"><b>Amount payable</b></div>
							<div class="span8"><h4><?=number_format($amount_payable)?> FCFA ($<?=@$amount_payable_converted?> USD)</h4></div>
						</div>
						<!--<div class="" >
							<div class="span4">Date: </div>
								<div class="input-append date span8">
									<input type="text" name = 'date_incurred' class="span5 datepicker"  value="<?=date('d-m-Y')?>" id="dp1" required>
									<span class="add-on"><i class="icon-th"></i></span>
								</div>
						</div>
						
						
						<div class="">
							<div class="span4">Name on card: </div>
							<div class="span8"><input type="text"  name="authorised_by" ></div>
						</div>
						<div class="">
							<div class="span4">Card number: </div>
							<div class="span8"><input type="text" class="span3" name="authorised_by" maxlength="4" ><input type="text" class="span3" name="authorised_by" ><input type="text" class="span3" name="authorised_by" ><input type="text" class="span3" name="authorised_by" ></div>
						</div>
						<div class="">
							<div class="span4">Expiry date: </div>
							<div class="span8">
								<select name="bus_number" id="inputType" class="span5">
										<option value=""> Month</option>
										<option value="1"> 01</option>
										<option value="2"> 02</option>
										<option value="3"> 03</option>
										<option value="4"> 04</option>
										<option value="5"> 05</option>
										<option value="6"> 06</option>
										<option value="7"> 07</option>
										<option value="8"> 08</option>
										<option value="9"> 09</option>
										<option value="10"> 10</option>
										<option value="11"> 11</option>
										<option value="12"> 12</option>
										
								</select>
								<select name="bus_number" id="inputType" class="span5">
										<option value=""> Year</option>
										<option value="2015"> 2015</option>
										<option value="2016"> 2016</option>
										<option value="2017"> 2017</option>
										<option value="2018"> 2018</option>
										<option value="2019"> 2019</option>
										<option value="2020"> 2020</option>
										<option value="2021"> 2021</option>
										<option value="2022"> 2022</option>
										<option value="2023"> 2023</option>
										<option value="2024"> 2024</option>
										<option value="2025"> 2025</option>
										
								</select>
							</div>
						</div>
						
						
						<div class="">
							<div class="span4">CVV: </div>
							<div class="span8"><input type="text"  name="authorised_by" placeholder="xxxx" ></div>
						</div>
								
						-->
					
						<div class="clear"></div>
						
						<div class="form-acstions">
							<div class="span4"><button class="btn btn-info " disabled>Buy with MTN Airtime</button></div>
							<div class="span8"><button class="btn btn-info " type="submit">Confirm payment(DEMO)</button></div>
							
							
						</div><div class="form-acstions">
							<div class="span4"></div>
							<div class="span8"></div>
							
							
						</div>
						
				</form>
				<div class="clear"></div>
						<div class="form-actions">
							<div class="span4"></div>
							<div class="span8">
								<!--Required by PayPal-->
								<form action="https://www.paypal.com/cgi-bin/webscr" method="post">
								<!--Comes from a Buy Now button-->
								<input type="hidden" name="cmd" value="_xclick">
								<input type="hidden" name="business" value="payment@quickticket.co">
								<input type="hidden" name="item_name" value="Bus travel ticket">
								<!--Do not prompt buyers to include a note with their payments.-->
								<input type="hidden" name="no_note" value="0">
								<input type="hidden" name="page_style" value="primary">
								<!--The background color for the checkout page below the header-->
								<input type="hidden" name="cpp_payflow_color" value="#3299bb">
								<input type="hidden" name="cpp_logo_image" value="http://dev.quickticket.co/images/logos/logo-paypal.png" >
								
								<input type="hidden" name="amount" value="<?php //echo $amount_payable_converted; ?>">
								<input type="hidden" name="currency_code" value="USD">
								<input type="image" src="https://www.paypalobjects.com/en_US/i/btn/btn_buynowCC_LG.gif" border="0" name="submit" alt="PayPal - The safer, easier way to pay online!" disabled>
								<img alt="" border="0" src="https://www.paypalobjects.com/en_US/i/scr/pixel.gif" width="1" height="1">
								</form>
							</div>
						</div>
				<!--Paypal button-->
						<!--<form action="https://www.paypal.com/cgi-bin/webscr" method="post" target="_top">
						<input type="hidden" name="cmd" value="_s-xclick">
						<input type="hidden" name="hosted_button_id" value="VAW7W3MJD2BW2">
						<input type="image" src="https://www.paypalobjects.com/en_US/i/btn/btn_buynowCC_LG.gif" border="0" name="submit" alt="PayPal - The safer, easier way to pay online!">
						<img alt="" border="0" src="https://www.paypalobjects.com/en_US/i/scr/pixel.gif" width="1" height="1">
						<input type="hidden" name="amount" value="">
						</form>-->
						
						
						

				
					<!--end the first tab-->
			</div> 
		
			
			<div class="tab-pane" id="bank-account">
				<form action="<?=url::site('expenses/register_expense')?>" method="POST" name="theform" class="span8 offset1">
						
						
						<div class="form-actions">
							<div class="span1"></div>
							<div class="span8"><button class="btn btn-success " type="submit" disabled><?=Kohana::lang('backend.login_with_unics')?></button></div>
						</div>
						
									
						
				</form>
					<!--end the first tab-->
			</div>  
			 
						
	  			
		</div>
		
		</div>	
		<div class="span4">
			<legend>Other information</legend>
			<div class="">
				<div class=""><?=Kohana::lang('backend.agency')?>: <?=get::agency_name($purchase->agency_id)?></div>
				<div class=""><?=Kohana::lang('backend.from')?>: <?php if($is_open_ticket){echo get::town($purchase->town_from);}else{echo get::town($purchase->from);}?></div>
				<div class=""><?=Kohana::lang('backend.to')?>: <?php if($is_open_ticket){echo get::town($purchase->town_to);}else{echo get::town($purchase->to);}?></div>
				<div class="">Date: <?php if($is_open_ticket){echo " Any";}else{echo $purchase->departure_date;}?> </div>
				<div class=""><?=Kohana::lang('backend.time')?>: <? if($is_open_ticket){echo " Any";}else{echo $purchase->departure_time;}?> </div>
				<div class=""><?=Kohana::lang('backend.price')?>: <?php if($is_open_ticket){echo number_format($purchase->ot_ticket_price);}else{echo number_format($purchase->ticket_price);}?> FCFA </div>
			
			</div>
		</div>
			  
		
		
		</div>
	</div>

<script type="text/javascript">
	$(document).ready(function(){
		
			
			
				$(function () {
				$('#myTab a').click(function (e) {
					  e.preventDefault();
					  $(this).tab('show');  
			})
			  })
			  
		
			  
			
	})
	</script>
