<?php defined('SYSPATH') or die('No direct script access'); ?> 

	<?php
		//determine what type of notice to display if at all
		$notice = $this->session->get_once('notice');
			if(!empty($notice)){ 
				if($notice['type'] == 'success'){?><div class="alert general success"><a class="close" data-dismiss="alert" href="#">&times;</a><?php echo $notice['message']; }?></div>
			<?	if($notice['type'] == 'error'){?><div class="alert general error"><a class="close" data-dismiss="alert" href="#">&times;</a><?php echo $notice['message']; }?></div>
		<?}?>
	  
	  <div class="container">
	  <div class="row-fluid " style="margin:5px 0 0px 0px;">
	  
		
		<section class="download" id="components">

	  	<div class="span12 hero-unit prof-unit" >
		
			<?php
		//determine what type of notice to display if at all
		$notice = $this->session->get_once('notice');
			if(!empty($notice)){ 
				if($notice['type'] == 'success'){?><div class="alert general success"><a class="close" data-dismiss="alert" href="#">&times;</a><?php echo $notice['message']; }?></div>
			<?	if($notice['type'] == 'error'){?><div class="alert general error"><a class="close" data-dismiss="alert" href="#">&times;</a><?php echo $notice['message']; }?></div>
		<?}?>
		
		<legend><?=Kohana::lang('backend.settings')?></legend>
             
			
		
			
			<div class="tabbable tabs-left span12">
				<ul class="nav nav-tabs" id="myTab">
							  <li class="active"><a href="#general-tab">General</a></li>
							  <li><a href="#infos-tab">Company information</a></li>
				</ul>
							 
				<div class="tab-content">
					<div class="tab-pane active" id="general-tab">
						<form action="<?=url::site('settings')?>" method="POST" >		
					   <div class="span5 offset1">
								<!--hidden inputs here is a technique to ensure that unchecked fields pass a 0 to the POST-->
								<input type="hidden" name="auto_ticket" value="0" />
								<label class="checkbox"><input type="checkbox" id="auto_ticket" name="auto_ticket" value="1" <?php if($settings->auto_ticket == 1){echo 'checked = "checked"';} ?>/><?=Kohana::lang('backend.auto_generate_ticket')?></label>
								<input type="hidden" name="another" value="0" />
								<label class="checkbox"><input type="checkbox" id="another" name="another" value="1" <?php  if($settings->another == '1'){echo 'checked = "checked"';} ?>/>Another</label>
								<input type="hidden" name="third" value="0" />
								<label class="checkbox"><input type="checkbox" id="third" name="third" value="1" <?php if($settings->third == 1){echo 'checked = "checked"';} ?>/>Third</label>
							<!--<input type="submit" value="Save" name="general">-->
							<button class="btn btn-info" type="submit" name="general">Save</button>
						</div>
						</form>
					</div>
				  <div class="tab-pane" id="infos-tab">

						<div class="span10 ">
						<form class="form-horizontal">  </form>
							
							<form class="form-horizontal" method="POST" action="<?=url::site('settings')?>" enctype="multipart/form-data">
							<div >
								<label class="control-label" for="inputEmail">Name:</label>
								<div class="controls">
											<input type="text" value="<?=$agency->name?>" disabled name="agency_name"/>&nbsp;&nbsp;
								</div>
							  </div>
							  <div >
								<label class="control-label" >Logo:</label>
								<div class="controls">
											<img src="<?php echo url::base(). $logo;?>" class="img-polaroid">
								</div>
							  </div>
							 
							  
							  <div >
								<label class="control-label" >&nbsp;</label>
								<div class="controls">
											<input type="file"  name="agency_logo" />
								</div>
							  </div>
							  
							   <br/>
							   
							  <div >
								<label class="control-label" for="inputEmail">Contacts:</label>
								<div class="controls">
											<textarea type="text" name="contacts" style="width:300px;height:100px;"/><?=@$agency->contacts?></textarea>
								</div>
							  </div>
							  <br/>
							  <div >
								<label class="control-label" for="inputEmail">Additional ticket information:</label>
								<div class="controls">
											<textarea type="text"  name="ticket_info"  style="width:300px;height:100px;" /><?=@$agency->TicketInfo?></textarea>
								</div>
							  </div>
							  
							  
							  <div class="">
									<label class="control-label lab" for="inputType"></label>
										<div class="controls ">
											<button class="btn btn-info" type="submit" name="company_info">Save</button>					
										</div>
									</div>	
							</form>			
						</div>
						
								
								
								
							  </div>
			
					</div>
					</div>
					
								
			<script>
			  $(function () {
				$('#myTab a').click(function (e) {
			  e.preventDefault();
			  $(this).tab('show');
			})
			  })
			  
			</script>
		</div>
		</section>
		
	  </div>
	  </div>
	  <div style="height:19px;"></div>
