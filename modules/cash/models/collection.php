<?php defined('SYSPATH') or die('No direct script access');

class Collection_Model extends ORM{

	public function register_collection($agency_id, $admin_username, $admin_available_balance, $amount, $CreatedBy)
	{
		$collection = ORM::FACTORY('collection');
		$collection->agency_id = $agency_id;
		$collection->admin_id = $admin_username;
		$collection->amount = $amount;
		$collection->previous_balance = $admin_available_balance;
		$collection->CreatedBy = $CreatedBy;
		$collection->save();
		
		//Update the admin's availaible balance
		Admin_Model::subtract_balance($admin_username, $amount);
		//Update the collector's available balance
		Admin_Model::add_balance($CreatedBy, $amount);	
	}
	
	
	public function get_collections_from_admin($admin_username)
	{
		$db = new Database();
		$collections_for_admin=ORM::factory('collection')->where('deleted','0')->where('admin_id',$admin_username)->find_all();
		return $collections_for_admin;
	}
	
	public function get_agency_expenses_by_period_by_bus($agency_id,$start_date,$end_date,$bus_number)
	{
		$db = new Database();
		$expenses = $db->query("SELECT * FROM expenses WHERE agency_id= ".$agency_id." AND bus_number= '".$bus_number."' AND deleted = '0' AND date_incurred BETWEEN CAST('$start_date' AS DATE) AND CAST('$end_date' AS DATE) ORDER BY date_incurred ASC");
		return $expenses;
	}
	

}
