<?php defined('SYSPATH') or die('No direct script access'); ?> 
	

 
	<div class="row-fluid ">
	  	<div class="span12 hero-unit prof-unit" >
		
		<ul class="nav nav-tabs" id="myTab">
					
					<li ><a href="#collection"><i class="icon-align-justify"></i> <?=Kohana::lang('backend.collection')?></a></li>
					<li><a href="<?=url::site('cash/other_income')?>"><i class="icon-align-justify"></i> <?=Kohana::lang('backend.other_income')?></a></li>
					<li><a href="<?=url::site('cash/cash_to_bank')?>"><i class="icon-align-justify"></i> <?=Kohana::lang('backend.cash_to_bank')?></a></li>
					<li class="active"><a href="<?=url::site('cash/balances')?>"><i class="icon-align-justify"></i> <?=Kohana::lang('backend.balances')?></a></li>
		</ul>
		
		<div class="tab-content">
			 
			
			<div class="tab-pane active" id="collection">
				<div class="span8 offset2">
				
					<legend><?=Kohana::lang('backend.balances')?></legend>						
						
						
						
									<?php 
										foreach ($all_active_admins as $admin){?>
										<div class="">
											<div class="span4 text-right"><?=$admin->username?>: </div>
											<div class="span8">
												<?=number_format($admin->available_balance)?>
											</div>
										</div>
									<?php }; ?>
						
						
						<div class="">
							<div class="span4 text-right"><h3>Branch balance:</h3></div>
							<div class="span8">
								<h3><?=number_format($branch_balance)?></h3>
							</div>
						</div>
					
						
				</div>	
					
			</div>
			
			
					
		</div>
		
			
		
			  
		</div>
		
		</div>
	  <div class="clear"></div>

