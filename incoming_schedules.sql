-- phpMyAdmin SQL Dump
-- version 4.0.10.14
-- http://www.phpmyadmin.net
--
-- Host: localhost:3306
-- Generation Time: Jan 07, 2017 at 10:34 AM
-- Server version: 5.6.28-76.1-log
-- PHP Version: 5.6.20

SET SQL_MODE = "NO_AUTO_VALUE_ON_ZERO";
SET time_zone = "+00:00";


/*!40101 SET @OLD_CHARACTER_SET_CLIENT=@@CHARACTER_SET_CLIENT */;
/*!40101 SET @OLD_CHARACTER_SET_RESULTS=@@CHARACTER_SET_RESULTS */;
/*!40101 SET @OLD_COLLATION_CONNECTION=@@COLLATION_CONNECTION */;
/*!40101 SET NAMES utf8 */;

--
-- Database: `quicktic_dev_db`
--

-- --------------------------------------------------------

--
-- Table structure for table `incoming_schedules`
--

CREATE TABLE IF NOT EXISTS `incoming_schedules` (
  `id` int(12) NOT NULL,
  `agency_id` int(12) NOT NULL,
  `from_agency_id` int(12) NOT NULL,
  `agency_code` varchar(255) NOT NULL,
  `bus_number` varchar(255) NOT NULL,
  `bus_seats` int(255) NOT NULL COMMENT '?? seater',
  `from` int(11) NOT NULL,
  `to` int(11) NOT NULL,
  `ticket_price` int(12) NOT NULL,
  `seat_occupants` mediumtext NOT NULL,
  `reserved_seats` mediumtext NOT NULL,
  `status` enum('current','departed') NOT NULL DEFAULT 'current',
  `loading` enum('0','1','2') NOT NULL DEFAULT '0' COMMENT '1-Boarding 2-Delayed',
  `departure_date` date NOT NULL,
  `checked_out_on` datetime NOT NULL,
  `departure_time` time NOT NULL,
  `checked_out_time` time NOT NULL COMMENT 'Time bus was checked out',
  `total_amount` int(10) NOT NULL COMMENT 'Total amount got from this schedule',
  `CreatedBy` varchar(255) NOT NULL,
  `CreatedOn` datetime NOT NULL,
  `CheckedOutBy` varchar(255) NOT NULL,
  `deleted` enum('0','1') NOT NULL DEFAULT '0',
  PRIMARY KEY (`id`),
  KEY `agency_id` (`agency_id`),
  KEY `to` (`to`),
  KEY `from` (`from`)
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

