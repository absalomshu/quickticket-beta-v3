<?php defined('SYSPATH') or die('No direct script access'); ?> 
	

 
	<div class="row-fluid ">
	  	<div class="span12 hero-unit prof-unit" >
		
		<ul class="nav nav-tabs" id="myTab">
					<!--<li class="active"><a href="#all-reports"> <?=Kohana::lang('backend.all')?></a></li>
					<li><a href="#admin-reports"><i class="icon-user"></i> <?=Kohana::lang('backend.admins')?></a></li>-->
					<li class="active"><a href="#all-reports"> <?=Kohana::lang('backend.all')?></a></li>
					<li ><a href="#bus-reports"><i class="icon-bus"></i> <?=Kohana::lang('backend.buses')?></a></li>
					
					<!--<li><a href="#collection"><i class="icon-align-justify"></i> <?=Kohana::lang('backend.collection')?></a></li>-->
		</ul>
		
		<div class="tab-content">
			<div class="tab-pane active" id="all-reports" >
				<div class="span8 offset2">
					<form action="<?=url::site('manage/reports_activity_by_period')?>" method="POST" >
						
						<legend><?=Kohana::lang('backend.all_activity')?></legend>
							<div>
								<div class="span4 text-right"><?=Kohana::lang('backend.branch')?>: <span class="red"> *</span> </div>
								<div class="span8 input-append">
									<select name="agency_id" id="inputType">
										<?php foreach ($all_children as $child):?>
										<option value="<?=$child->id?>"><?=$child->name?></option>
										<?php endforeach;?>
									</select>				</div>
							</div>							
							<div>
							<div class="span4 text-right"><?=Kohana::lang('backend.from');?><span class="red"> *</span>:</div>
							<div class="span8">
								<div class="input-append date span4">
									<input type="text" name = 'start_date' class="span8 datepicker"  value="<?=date('d-m-Y')?>" id="dp1" >
									<span class="add-on"><i class="icon-calendar"></i></span>
								</div>	
							</div>
							</div>
							
							<div>
								<div class="span4 text-right"><?=Kohana::lang('backend.to');?><span class="red"> *</span>:</div>
								<div class="span8">
									<div class="input-append date span6">
										<input type="text" name = 'end_date' class="span8 datepicker"  value="<?=date('d-m-Y')?>" id="dp1" >
										<span class="add-on"><i class="icon-calendar"></i></span>
									</div>	
								</div>
							</div>
							
							
							<div class="clear"></div>
							<div class="form-actions">
								<div class="span4"></div>
								<div class="span8">
									<button class="btn  btn-info" type="submit" ><i class="icon-search icon-white"></i> <?=Kohana::lang('backend.view');?></button><br/>
							
								</div>
							</div>
					</form>	
				</div>	
					
			</div> 
			
			<div class="tab-pane" id="admin-reports">
				<!--<div class="span8 offset2">
				
					<legend><?=Kohana::lang('backend.admin_activity')?><!--<a href="#" class="view">See all activity</a></legend>						
						<form action="<?=url::site('reports/activity_by_period_by_admin/')?>" method="POST" >
						<div class="">
							<div class="span4"><?=Kohana::lang('backend.admin')?><span class="red"> *</span>: </div>
							<div class="span8">
								<select name="admin" id="inputType">
								
									<?php 
										//foreach ($admins as $admin){?>
										<option value="<?php //$admin->username?>"><? //$admin->username?></option>
									<?php //}; ?>
								
							</select>
							
							</div>
						</div>
					
						
							<div class="span4"><?=Kohana::lang('backend.from');?>:</div>
							<div class="span4"><?=Kohana::lang('backend.to');?>:</div>
							<div class="input-append date span4">
								<input type="text" name = 'start_date' class="span8 datepicker"  value="<?=date('d-m-Y')?>" id="dp1" >
								<span class="add-on"><i class="icon-calendar"></i></span>
							</div>	
							
							<div class="input-append date span4">
								<input type="text" name = 'end_date' class="span8 datepicker"  value="<?=date('d-m-Y')?>" id="dp1" >
								<span class="add-on"><i class="icon-calendar"></i></span>
							</div>	
							<div class="span3">
								<button class="btn  btn-info" type="submit" name="by_period" ><i class="icon-search icon-white"></i> <?=Kohana::lang('backend.view');?></button><br/>
							</div>
					</form>	
				</div>	-->
					
			</div>
			
			
			<div class="tab-pane" id="bus-reports">
				<div class="span8 offset2">
				
					<legend><?=Kohana::lang('backend.bus_activity')?><!--<a href="#" class="view">See all activity</a>--></legend>						
						<form action="<?=url::site('manage/activity_by_period_by_bus/')?>" method="POST" >
						<div class="">
							<div class="span4"><?=Kohana::lang('backend.bus_no')?><span class="red"> *</span>: </div>
							<div class="span8">
								<select name="bus_number" id="inputType">
								
									<?php 
										foreach ($all_buses as $bus){?>
										<option value="<?=$bus->bus_number?>"><?=$bus->bus_number?></option>
									<?php }; ?>
								
							</select>
							
							</div>
						</div>
					
						<!--
							<div class="span12"><?=Kohana::lang('backend.on');?>:</div>
							<div class="input-append date span4">
								<input type="text" name = 'date' class="span8 datepicker"  value="<?=date('d-m-Y')?>" id="dp1" >
								<span class="add-on"><i class="icon-calendar"></i></span>
							</div>				
							<button class="btn  btn-info" type="submit" name="by_date"  ><i class="icon-search icon-white"></i> <?=Kohana::lang('backend.view');?></button><br/>
							
							<div class="clear"></div>
								<legend></legend>
						-->
						
						<div class="">
							<div class="span4"><?=Kohana::lang('backend.from')?><span class="red"> *</span>: </div>
							<div class="span8">
								<div class="input-append date span6">
								<input type="text" name = 'start_date' class="span8 datepicker"  value="<?=date('d-m-Y')?>" id="dp1" >
								<span class="add-on"><i class="icon-calendar"></i></span>
								</div>	
							</div>
						</div>
						<div class="">
							<div class="span4"><?=Kohana::lang('backend.to')?><span class="red"> *</span>: </div>
							<div class="span8">
								<div class="input-append date span6">
								<input type="text" name = 'end_date' class="span8 datepicker"  value="<?=date('d-m-Y')?>" id="dp1" >
								<span class="add-on"><i class="icon-calendar"></i></span>
								</div>	
							</div>
						</div>
						
						
							<!--
							<div class="span4"><?=Kohana::lang('backend.from');?>:</div>
							<div class="span4"><?=Kohana::lang('backend.to');?>:</div>
							<div class="input-append date span4">
								<input type="text" name = 'start_date' class="span8 datepicker"  value="<?=date('d-m-Y')?>" id="dp1" >
								<span class="add-on"><i class="icon-calendar"></i></span>
							</div>	
							
							<div class="input-append date span4">
								<input type="text" name = 'end_date' class="span8 datepicker"  value="<?=date('d-m-Y')?>" id="dp1" >
								<span class="add-on"><i class="icon-calendar"></i></span>
							</div>	
							<div class="span3">
								<button class="btn  btn-info" type="submit" name="by_period" ><i class="icon-search icon-white"></i> <?=Kohana::lang('backend.view');?></button><br/>
							</div> -->
							
							
							
						<div class="">
							<div class="span4"><?=Kohana::lang('backend.output_format')?><span class="red"> *</span>: </div>
							<div class="span8">
								<!--<label class="checkbox inline">
								<input type="radio" name="display_options" value="grouped" checked="checked"> Grouped
								</label>-->
								<label class="checkbox inline">
								<input type="radio" name="display_options" value="timeline" checked='checked'> Timeline
								</label>
							
							</div>
						</div>	
						<div class="clear"></div>
						<div class="form-actions">
							<div class="span4"></div>
							<div class="span8"><button class="btn  btn-info" type="submit" name="by_period" ><i class="icon-search icon-white"></i> <?=Kohana::lang('backend.view');?></button>
							</div>
						</div>
					</form>	
				</div>	
					
			</div>  
			
			<!--<div class="tab-pane" id="collection">
				<div class="span8 offset2">
				
					<legend><?=Kohana::lang('backend.admin_collections')?></legend>						
						<form action="<?=url::site('reports/collection_by_admin/')?>" method="POST" >
						<div class="">
							<div class="span4"><?=Kohana::lang('backend.admin')?><span class="red"> *</span>: </div>
							<div class="span8">
								<select name="admin" id="inputType">
								
									<?php 
										//foreach ($admins_excluding_current as $admin){?>
										<option value="<?php //$admin->username?>"><?php //$admin->username?></option>
									<?php //}; ?>
								
							</select>
							
							</div>
						</div>
					
						<div class="">
							<div class="span4"><?=Kohana::lang('backend.date');?>:</div>
							
							<div class="input-append date span4">
								<input type="text" name = 'date' class="span8 datepicker"  value="<?=date('d-m-Y')?>" id="dp1" >
								<span class="add-on"><i class="icon-calendar"></i></span>
							</div>	
							
							<div class="span3">
								<button class="btn  btn-info" type="submit" name="by_period" ><i class="icon-search icon-white"></i> <?=Kohana::lang('backend.view');?></button><br/>
							</div>
						</div>
					</form>	
				</div>	
					
			</div>-->
					
		</div>
		
			
		
			  
		</div>
		
		</div>
	  <div class="clear"></div>

	  
<link rel="stylesheet" href="//code.jquery.com/ui/1.11.4/themes/smoothness/jquery-ui.css">
	<script src="//code.jquery.com/jquery-1.10.2.js"></script>
	<script src="//code.jquery.com/ui/1.11.4/jquery-ui.js"></script>
	<link rel="stylesheet" href="/resources/demos/style.css">	  
	  
	  
<script type="text/javascript">
	$(document).ready(function(){
			$('#timepicker').timepicker({
				minuteStep:30,
				//defaultTime: '8:00 PM'
			});
			
			$('.input-append.date').datepicker({
				format: "dd-mm-yyyy",
				todayBtn: "linked",
				autoclose: true,
				todayHighlight: true
			});
			
			
				$(function () {
				$('#myTab a').click(function (e) {
					  e.preventDefault();
					  $(this).tab('show');  
			})
			  })
			
	})
	
	
	
	

	
	</script>
