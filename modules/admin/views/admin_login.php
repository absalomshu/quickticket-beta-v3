<?php defined('SYSPATH') or die('No direct script access'); ?> 
<!DOCTYPE html>
	

		<?php
		//determine what type of notice to display if at all
		$notice = $this->session->get_once('notice');
			if(!empty($notice)){ 
				if($notice['type'] == 'success'){?><div class="alert general success"><a class="close" data-dismiss="alert" href="#">&times;</a><?php echo $notice['message']; }?></div>
			<?	if($notice['type'] == 'error'){?><div class="alert short error"><a class="close" data-dismiss="alert" href="#">&times;</a><?php echo $notice['message']; }?></div>
		<?}?>
		<br/><br/>
		<br/><br/>


		<div class="" >
		<div class="span5 hero-unit prof-unit auto-center"><br/>
			<h20>&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;<?=Kohana::lang('backend.welcome_admin')?></h20>
				<form method="POST" autocomplete="off" action="<?=url::site('admin/login')?>" class="form-inline" >
					<!--This is a specific hack to prevent browser from remembering login and password, as autocomplete is often by-passed-->
					<input style="display:none">
					<input type="password" style="display:none">
					<input type="text" name="username" value="" style="display: none" /> 
					
					<div class="span4 spacious">
						<div class="control-group">
						<label class="control-label lab" for="inputType"></label>
							<div class="controls large">
										
										<input type="text" placeholder="<?=Kohana::lang('backend.username')?>" style="width:300px;height:30px;" name="username" id="username" required/>
							</div>
						</div>
					</div>	
					<br/>
			
					<div class="span4 spacious">
						<div class="control-group">
						<label class="control-label lab" for="inputType"></label>
							<div class="controls large">
							<input type="password" autocomplete="off" class="input-small" placeholder="<?=Kohana::lang('backend.password')?>" style="width:300px;height:30px;" name="password" required >&nbsp;&nbsp;
							</div>
						</div>
					</div>
					<div class="span4 spacious">
						<div class="control-group">
						<label class="control-label lab" for="inputType"></label>
							<div class="controls large">
							<button class="btn btn-info" type="submit" style="height:35px; width:120px;font-size:20px;"><?=Kohana::lang('backend.login')?></button>					
							</div>
						</div>
					</div>
						
				</form> 
			<div class="clear"></div>
		</div>
	</div>
				<br/>
	
		
