<?php defined('SYSPATH') or die('No direct script access'); ?>
	
		
		
		<div class="row-fluid  span12 hero-unit prof-unit reload" style="margin:5px 0 0px 0;">	

			<div class="no-print">
			
			<ul class="nav nav-tabs">
					<li > <a href="<?=url::site('reports')?>"><i class="icon-arrow-left"></i> <?=Kohana::lang('backend.back_reports')?></a></li>
					<button class="view" onClick="window.print()"><i class="icon-print"></i> <?=Kohana::lang('backend.print')?></button>
				</ul>
			</div>
			<div class=" print-area" >
			
				
				<legend>
				<div class="print-head">
						<?php $admin=Authlite::instance()->get_admin();?>
						<?=strtoupper(get::_parent($admin->agency_id)->name);?><br/>
						<?=get::agency_name($admin->agency_id);?>
					</div>
					Report: Income & Expenses [Chart]	<br/>
					Start date: <?=date("d-m-Y",strtotime($start_date))?> &nbsp;&nbsp;&nbsp;&nbsp; 
					End date: <?=date("d-m-Y",strtotime($end_date))?>&nbsp;&nbsp;&nbsp;&nbsp;
				</legend>
			
					
					<?php
					//Now take account of remote tickets
					foreach ($remote_outgoing_tickets as $ticket)
					{
					?>
					
					<?php 
					//add to total from schedules
					$total += $ticket->Price;
					}?>
					
			
				<div class="span6" style="margin: 0px" >
					<div class="heading">Income</div>
					<div class="rule"><hr/></div>
					
					<div id="pie"></div>
				</div>
					
				<div class="span6" >
					<div class="heading">Expenses</div>
					<div class="rule"><hr/></div>
					<div id="pie2"></div>
				</div>
					
					
					
					<?php 
					foreach ($period_parcels as $parcel) 
					{
					if($parcel->type == 'mail')
						{
						
						} 
					}
					foreach ($period_parcels as $parcel){
					
					}
					
					//Separate other income into bus rental and others
					$other_income_total = 0;
					$bus_rental_total = 0;
					foreach ($period_income as $income)
					{	
						if($income->purpose=='bus_rental'){
							$bus_rental_total += $income->amount; 
						}else{
							$other_income_total += $income->amount; 
						}
					}
					$income_total = $other_income_total + $bus_rental_total;
					
					$income_chart_content =  "{label: 'Schedules [".number_format($total)."] ', value: ".$total.", caption: 'someting'},"; 
					$income_chart_content .= "{label: 'Parcels [".number_format($parcel_total)."]', value: ".$parcel_total."},"; 
					$income_chart_content .= "{label: 'Bus rental [".number_format($bus_rental_total)."]', value: ".$bus_rental_total."},"; 
					$income_chart_content .= "{label: 'Others [".number_format($other_income_total)."]', value: ".$other_income_total."},"; 
					
					//create an array to hold the various expense categories totals
					$all_expense_categories = Expense_category_Model::get_all($this->agency_id);
					$all_expense_categories_array = array();
					
					//Initialize an array with all active expense categories and set them to zero;
					//Later for each category, just update total amount.
					
					foreach($all_expense_categories as $category){
						$all_expense_categories_array[$category->name]=0;
					}
						
					
					foreach ($period_expenses as $exp)
					{
						$all_expense_categories_array[$exp->name] += $exp->amount;
					}
					
					$expenditure_chart_content='';
					foreach($all_expense_categories_array as $key=>$total_category_expenditure){
						$expenditure_chart_content .= "{label: '".$key." [".number_format($total_category_expenditure)."]', value: ".$total_category_expenditure."},"; 
					}
					?>
					
	
				
				<!--NET SUMMARY TABLE-->
				<table class="table table-condensed table-striped ">
					<tr>
						<th class="span2">GRAND TOTAL</th>
						<td>&nbsp;</td>
						<th class="text-right" >EXPENSES</th>
						<th class="text-right">INCOME</th>
					</tr>
					
					<tr>		
						<td></td>
						<th></th>
						<th class="span2 text-right"><?=number_format($total_expenditure)?></th>
						<th class="span2 text-right"><?=number_format($total + $parcel_total + $income_total )?></th>
					</tr>
					<tr>		
						<td></td>
						<th></th>
						<th class="span2 text-right">NET</th>
						<th class="span2 text-right"><?=number_format($total + $parcel_total + $income_total - $total_expenditure)?> </th>
					</tr>
				</table>	
				
			
				<div>Printed by: <?=STRTOUPPER($this->admin->username);?></div> 
				<div>Printed on: <?=date('d-m-Y h:i:s A')?></div>
				<!--END OF NET SUMMARY-->
				
				
				
			</div>

			
		
		
		
		
		
		
		</div>
		
		
<!--PIE CHARTS-->
<script src="<?=url::base()?>js/d3.min.js"></script>
<script src="<?=url::base()?>js/d3pie.js"></script>

<script>
	var pie = new d3pie("pie", {
		
		size: {
        "pieOuterRadius": "100%",
		"canvasHeight":360,
		"canvasWidth":560
	
    },tooltips: {
    enabled: true,
    type: "placeholder"
  },
		data: {
			content: [
				<?=$income_chart_content;?>
			]
		}
	});
	
	var pie2 = new d3pie("pie2", {
		size: {
        "pieOuterRadius": "100%",
		"canvasHeight":360,
		"canvasWidth":560
    },
		data: {
			content: [
				<?=$expenditure_chart_content;?>
				//{ label: "JavaScript", value: 50 },
				//{ label: "Ruby", value: 30 },
				//{ label: "Java (157618)", value: 20},
			]
		}
	});

</script>

<!--END OF PIE CHARTS -->


