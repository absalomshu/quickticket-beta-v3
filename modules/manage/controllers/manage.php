<?php defined('SYSPATH') or die('No direct script access');

//set default timezone, otherwise it's UTC which is an hour late
date_default_timezone_set('Africa/Douala');

//Almost all functions will take as parameter, the id of the agency we are dealing with
//in order not to interfere with another's stuff.

class Manage_Controller extends Template_Controller {
	
	public $template = 'template/admin_template';
	private $current_branch;
	private $agency_id;
	private $parent_id;
	
	public function __construct()
	{	
		parent::__construct();
		$this->admin = Authlite::instance()->get_admin();
		$this->db = new Database();
		$this->session = Session::instance();
		
		//If there's an admin
		if (!empty($this->admin)) 
		{
			$this->agency_id = $this->admin->agency_id;
			$this->parent_id = get::_parent($this->agency_id)->id;
			//The manage controller handles all branches, and single branches. Hence the current_branch variable.
			//0 for all branches, and branch_id for a single branch.
			if(isset($_SESSION['current_branch']))
			{
				$this->current_branch = $_SESSION['current_branch'];
			}else
			{
				$this->current_branch =0;
			}
		}
	}
	
	public function index()
	{	
		
		Authlite::check_admin();
		//$this->session = Session::instance();
		//ensure a logged in user can't land on the signup page
		if (empty($this->admin)) 
		{	
			url::redirect('manage/login');
		}
		else
		{	
				url::redirect('manage/main');
		
		} 
	}
	
	public function login() 
	{	
		if (!empty($this->admin)) 
		{
				url::redirect('manage/main/'.$this->admin->agency_id);
		}
		//$this->auto_render=false;
		$notice='';
		$this->template->title="login";
		
		if($_POST) 
		{	
			
			$post=new Validation($_POST);
			$post->add_rules("username", "required",'valid::standard_text');
			$post->add_rules("password", "required");
			if ($post->validate())
			{
				$username = $_POST['username'];
				$password = $_POST['password'];
				
				
				$admin = Authlite::instance()->admin_login($username, $password);
				
				if(!$admin){	
					//$notice="Sorry, incorrect username and password combination!";
					$this->session->set('notice', array('message'=>Kohana::lang('backend.incorrect_credentials'),'type'=>'error'));

				}
				else{
				
					//this is already done in the construct, but for some reason, does not pick the admin for
					//the first time till you refresh. so it is done here again 
					$this->admin = Authlite::instance()->get_admin();
					$admin_agency_id = $this->admin->agency_id;
					//url::redirect('admin/main/'.$admin_agency_id);
					url::redirect('manage');
			}}else 
			{
                $errors=$post->errors('errors');
                $notice="";
                foreach($errors as $error) 
				{
                    $notice .= $error;
                }
				$this->session->set('notice', array('message'=>$notice,'type'=>'error'));
            }
		}
		$view=new View('admin_login');
		$view->notice=$notice;
		$this->template->content=$view;
	}
	
	
	public function logout() 
	{
		Authlite::instance()->logout(true);
		url::redirect('admin');
	}
	
	
	public function past_schedule($schedule_id)
	{
		Authlite::check_admin();	
		Authlite::verify_referer();
		//Authlite::check_integrity($schedule_id);
		
		$this->template->title = Kohana::lang('backend.past_sched_display');
		
		$schedule = get::schedule($schedule_id);
		$expenses_for_schedule = manage::get_expenses_for_schedule($schedule_id);
		
		$seats_and_reservers = json_decode($schedule->reserved_seats,true);
		$seats_and_occupants = json_decode($schedule->seat_occupants,true);
		$view = new View('manage_past_schedule');
		
		//number of seats and occupants
		//calculate the amount from special drops
		$special_drop_total = 0;
		//count the number of special drops
		$special_drop_count = 0;
		//count the number of free tickets
		$free_tickets_price = 0;
		$free_ticket_count = 0;
		
		foreach($seats_and_occupants as $s)
		{
			//get the special drop price, and if it's not defined, MOVE ON
			if(isset($s['sd_price']))
			{
				$special_drop_total += @$s['sd_price'];
				$special_drop_count++;}
			//if the value of free ticket is 1
			if(isset($s['free_ticket']) && $s['free_ticket']==1){
			//if it's also a special drop, get the price to know how much is lost
				if(isset($s['sd_price'])){
					$free_tickets_price += @$s['sd_price'];}
				else{
					$free_tickets_price += $schedule->ticket_price;
				}
				$free_ticket_count++;}
		}

		//number of seats and occupants
		$no_so = count($seats_and_occupants);
		//for now we count only occupants as reservers will be converted to occupants upon payment

		//regular seats are those paid for at normal price (as opposed to special drops)
		$regular_seats = $no_so - $special_drop_count;
		
		
		$view->seats_and_occupants = $seats_and_occupants;
		$view->seats_and_reservers = $seats_and_reservers;
		$view->no_so = $no_so;
		$view->empty_seats = $schedule->bus_seats - $no_so;
		$view->ticket_price = $schedule->ticket_price;
		
		//speculated normal cost of seats taken by special drops called amount lost
		$amount_lost = ($schedule->ticket_price) * $special_drop_count;
		//total is number of seats occupied times ticket price, minus speculated normal cost of special drops(lost), plus actual amount got from the special drops		
		$view->total = ($no_so * $schedule->ticket_price) - $amount_lost + $special_drop_total - $free_tickets_price;
		$view->schedule = $schedule;
		$view->free_ticket_count = $free_ticket_count;
		$view->special_drop_count = $special_drop_count;
		$view->special_drop_total = $special_drop_total;
		$view->expenses_for_schedule = $expenses_for_schedule;
		$view->regular_seats = $regular_seats;
		$this->template->content = $view;

	}
	
	public function main($current_branch=0) 
	{	
		Authlite::check_admin();	
		//$permitted_levels = array(4);
		//Authlite::check_access($permitted_levels);
		//Authlite::verify_referer();
		
		$user_group = $this->admin->admin_group_id;
		$agency_id = $this->agency_id;
		Authlite::check_agency_integrity($agency_id);
		
		$parent_id = get::_parent($this->admin->agency_id)->id;
		$this->template->title = Kohana::lang('backend.welcome_backend');
		$view=new View ('manage_main');
		
		$towns = get::all_towns();
		$admin_town =get::admins_town($this->admin->agency_id);
		$all_children = get::all_children($parent_id);
		//$current_schedules = get::all_parent_current_schedules($parent_id);
		//$departed_schedules = get::all_parent_departed_schedules($parent_id);

			//enable the GM to surf various agencies by setting a session variable that determines which particular branch to process
			//if 0, then he's viewing the whole company.
			if($current_branch == 0){
				//$current_schedules=get::all_parent_current_schedules($parent_id);
				$current_schedules=manage::get_ten_parent_current_schedules($parent_id);
				//$departed_schedules=get::all_parent_departed_schedules($parent_id);
				$departed_schedules=manage::get_ten_parent_departed_schedules($parent_id);
			}else
			{	//die('not parent');
				$current_schedules=get::ten_agency_current_schedules($current_branch);
				$departed_schedules=get::ten_agency_departed_schedules($current_branch);
			}
			
			
		$date=date('d-m-Y');
		
		//var_dump($departed_schedules);	exit;
		
		$view->current_schedules=$current_schedules;
		$view->departed_schedules=$departed_schedules;
		
		//set current branch in session as it's needed in the submenu, which is not part of the view.
		$this->session->set('current_branch', $current_branch);
		$view->current_branch=$current_branch;
		//Needs to be global here, else these variables will not be available in the sub-menu

		//set in session for future pages
		$this->session->set('children', $all_children);
		$view->towns=$towns;
		$this->template->content=$view;
		
	}
	
		
	
	//$type: either parent or agency. Helps distinguish which the BM or GM sees.
	public function schedule_by_date()
	{	
		Authlite::check_admin();
		Authlite::verify_referer();
		
		$current_branch = $_SESSION['current_branch'];
		$agency_id = $this->agency_id;
		$parent_id = get::_parent($agency_id)->id;
		$type = $this->admin->admin_group_id;
				
		//Authlite::check_agency_integrity($agency_id);
		$this->template->title = Kohana::lang('backend.welcome_backend');
		if($_POST)
		{
			$post = new Validation($_POST);
			$post->add_rules('date','required');
			if ($post->validate())
			{	
				
			$date = date("Y-m-d",strtotime($_POST['date']));
			
			
			//if it's "all branches"
			if ($current_branch==0){
			$_30seats = ORM::FACTORY('schedule')->join('agencies','agencies.id','schedules.agency_id')->where('parent_id',$parent_id)->where('departure_date',$date)->where('status','departed')->where('bus_seats',30)->count_all();
			$_35seats = ORM::FACTORY('schedule')->join('agencies','agencies.id','schedules.agency_id')->where('parent_id',$parent_id)->where('departure_date',$date)->where('status','departed')->where('bus_seats',35)->count_all();
			$_39seats = ORM::FACTORY('schedule')->join('agencies','agencies.id','schedules.agency_id')->where('parent_id',$parent_id)->where('departure_date',$date)->where('status','departed')->where('bus_seats',39)->count_all();
			$_55seats = ORM::FACTORY('schedule')->join('agencies','agencies.id','schedules.agency_id')->where('parent_id',$parent_id)->where('departure_date',$date)->where('status','departed')->where('bus_seats',55)->count_all();
			$_70seats = ORM::FACTORY('schedule')->join('agencies','agencies.id','schedules.agency_id')->where('parent_id',$parent_id)->where('departure_date',$date)->where('status','departed')->where('bus_seats',70)->count_all();
			
			//print_r("HAPPY");exit;
			$departed_schedules = get::all_parent_departed_schedules_by_date($parent_id,$date);				
			$current_schedules = get::all_parent_current_schedules_by_date($parent_id,$date); 
			$parcels=get::all_parent_parcels_by_date($parent_id,$date);
			
			//Need to calculate the total expenses for that day
			$parent_expenses_by_date = get::parent_expenses_by_date($parent_id,$date);
			$total_expenditure = 0;
			foreach($parent_expenses_by_date as $exp )
			{
				$total_expenditure += $exp->amount;
			}
			}
			
			else{
			//else the agency_id is that of the current_branch the gm is on
			//die('here');
			$agency_id = $current_branch;
			$_30seats = ORM::FACTORY('schedule')->where('agency_id',$agency_id)->where('departure_date',$date)->where('status','departed')->where('bus_seats',30)->count_all();
			$_35seats = ORM::FACTORY('schedule')->where('agency_id',$agency_id)->where('departure_date',$date)->where('status','departed')->where('bus_seats',35)->count_all();
			$_39seats = ORM::FACTORY('schedule')->where('agency_id',$agency_id)->where('departure_date',$date)->where('status','departed')->where('bus_seats',39)->count_all();
			$_55seats = ORM::FACTORY('schedule')->where('agency_id',$agency_id)->where('departure_date',$date)->where('status','departed')->where('bus_seats',55)->count_all();
			$_70seats = ORM::FACTORY('schedule')->where('agency_id',$agency_id)->where('departure_date',$date)->where('status','departed')->where('bus_seats',70)->count_all();
						
			$departed_schedules = get::all_agency_departed_schedules_by_date($agency_id,$date);				
			$current_schedules = get::all_agency_current_schedules_by_date($agency_id,$date);
			$parcels=get::all_agency_parcels_by_date($agency_id,$date);
			
			//Need to calculate the total expenses for that day
			$agency_expenses_by_date = get::agency_expenses_by_date($agency_id,$date);
			$total_expenditure = 0;
			foreach($agency_expenses_by_date as $exp )
			{
				$total_expenditure += $exp->amount;
			}
			}
			
			//Calculate total amount expected from departed buses.
			$total = 0;
			foreach($departed_schedules as $ds){
				$total += $ds->total_amount;
			}
			}
			else
			{
				$errors=$post->errors('errors');
				$notice="";
					foreach($errors as $error) 
					{
						$notice.=$error."<br />";
					}
					$this->session->set('notice', array('message'=>$notice,'type'=>'error'));
					url::redirect('admin/main/'.$this->admin->agency_id);
			}
	}
	$view = new View('manage_schedule_by_date');
	$view->departed_schedules = $departed_schedules;
	$view->current_schedules = $current_schedules;
	$view->parcels=$parcels;
	$view->total_expenditure=$total_expenditure;
	$view->_30seats = $_30seats;
	$view->_35seats = $_35seats;
	$view->_39seats = $_39seats;
	$view->_55seats = $_55seats;
	$view->_70seats = $_70seats;
	$view->total = $total;
	$view->date = $date;
		//var_dump($children);exit;
	$this->template->content = $view;
	
	}	
	
	//$type: either parent or agency. Helps distinguish which the BM or GM sees.
	public function activity_by_period()
	{	
		Authlite::check_admin();
		
		$agency_id = $this->agency_id;
		$parent_id = get::_parent($agency_id)->id;
		$current_branch = $_SESSION['current_branch'];
		//$type = $this->admin->admin_group_id;
		Authlite::verify_referer();
		//Authlite::check_agency_integrity($agency_id);
		$this->template->title = Kohana::lang('backend.welcome_backend');
		
		if($_POST)
		{
			$post = new Validation($_POST);
			$post->add_rules('start_date','required');
			$post->add_rules('end_date','required');
			if ($post->validate())
			{	
				
			$start_date = date("Y-m-d",strtotime($_POST['start_date']));
			$end_date = date("Y-m-d",strtotime($_POST['end_date']));
			
			//$schedules = ORM::FACTORY('schedule')->where('agency_id',$agency_id)->where('departure_date',$date)->find_all();
			
			
			/*
				$_30seats = ORM::FACTORY('schedule')->join('agencies','agencies.id','schedules.agency_id')->where('parent_id',$parent_id)->where('departure_date',$date)->where('status','departed')->where('bus_seats',30)->count_all();
				$_35seats = ORM::FACTORY('schedule')->join('agencies','agencies.id','schedules.agency_id')->where('parent_id',$parent_id)->where('departure_date',$date)->where('status','departed')->where('bus_seats',35)->count_all();
				$_39seats = ORM::FACTORY('schedule')->join('agencies','agencies.id','schedules.agency_id')->where('parent_id',$parent_id)->where('departure_date',$date)->where('status','departed')->where('bus_seats',39)->count_all();
				$_55seats = ORM::FACTORY('schedule')->join('agencies','agencies.id','schedules.agency_id')->where('parent_id',$parent_id)->where('departure_date',$date)->where('status','departed')->where('bus_seats',55)->count_all();
				$_70seats = ORM::FACTORY('schedule')->join('agencies','agencies.id','schedules.agency_id')->where('parent_id',$parent_id)->where('departure_date',$date)->where('status','departed')->where('bus_seats',70)->count_all();
				
				//print_r("HAPPY");exit;
				$departed_schedules = get::all_parent_departed_schedules_by_date($parent_id,$date);				
				$current_schedules = get::all_parent_current_schedules_by_date($parent_id,$date); 			
			*/
			//if it's "all branches"
			if ($current_branch==0)
			{
				$period_parcels=get::parent_parcels_by_period($parent_id,$start_date,$end_date);
				$period_expenses=get::parent_expenses_by_period($parent_id,$start_date,$end_date);
				$period_schedules=get::parent_schedules_by_period($parent_id,$start_date,$end_date);
				$remote_outgoing_tickets=Ticket_Detail_Model::get_parent_outgoing_remote_tickets_by_period_all_admins($parent_id,$start_date,$end_date);
			
			}else{
				$period_parcels=get::agency_parcels_by_period($current_branch,$start_date,$end_date);
				$period_expenses=get::agency_expenses_by_period($current_branch,$start_date,$end_date);
				$period_schedules=get::agency_schedules_by_period($current_branch,$start_date,$end_date);
				//Add remote tickets to this report
				$remote_outgoing_tickets=Ticket_Detail_Model::get_agency_outgoing_remote_tickets_by_period_all_admins($current_branch,$start_date,$end_date);
			
			}				
				//calculate the total expenses for that period
				$total_expenditure = 0;
				foreach($period_expenses as $exp )
				{
					$total_expenditure += $exp->amount;
				}
			
				//Calculate total amount expected from departed buses for the period. DEPARTED BUSES ONLY
				$total = 0;
				foreach($period_schedules as $ds)
				{
					$total += $ds->total_amount;
				}
			}
			else
			{
				$errors=$post->errors('errors');
				$notice="";
					foreach($errors as $error) 
					{
						$notice.=$error."<br />";
					}
					$this->session->set('notice', array('message'=>$notice,'type'=>'error'));
					url::redirect('admin/main/'.$this->admin->agency_id);
			}
	}
	$view = new View('manage_activity_by_period');
	//$view->departed_schedules = $departed_schedules;
	$view->remote_outgoing_tickets=$remote_outgoing_tickets;
	$view->parcels=$period_parcels;
	$view->total_expenditure=$total_expenditure;
	$view->period_parcels = $period_parcels;
	$view->period_expenses = $period_expenses;
	$view->period_schedules = $period_schedules;
	$view->total = $total;
	$view->start_date = $start_date;
	$view->end_date = $end_date;
		
	$this->template->content = $view;
	
	}
	
	
	//type is either departed or current
	public function all_schedules($type)
	{	
		Authlite::check_admin();
		Authlite::verify_referer();
		$agency_id = $this->agency_id;
		
		//pagination
		$per_page = 15;
		if ($type == 'current'){
			$all= get::all_agency_current_schedules($agency_id);}else{
			$all= get::all_agency_departed_schedules($agency_id);}
		$total = count($all);
		$this->pagination = new Pagination(array(
			// 'base_url'    => 'welcome/pagination_example/page/', // base_url will default to current uri
			'uri_segment'    => 'page', // pass a string as uri_segment to trigger former 'label' functionality
			'total_items'    => $total, // use db count query here of course
			'items_per_page' => $per_page , // it may be handy to set defaults for stuff like this in config/pagination.php
			'style'          => 'punbb', // pick one from: classic (default), digg, extended, punbb, or add your own!
			//'query_string'   => "ORM::factory('schedule')->where('agency_id',$agency_id)->where('status','current')->find_all()"
));
		if ($type == 'current'){
			//$schedules = get::all_agency_current_schedules($agency_id);
			$schedules=ORM::factory('schedule')->where('agency_id',$agency_id)->where('status','current')->where('deleted','0')->limit($per_page,$this->pagination->sql_offset)->find_all();			
		}else{
			//$schedules = get::all_agency_departed_schedules($agency_id);
			$schedules=ORM::factory('schedule')->where('agency_id',$agency_id)->where('status','departed')->limit($per_page,$this->pagination->sql_offset)->find_all();			
		}
		
		$this->template->title = 'Viewing all schedules';		
		$view = new View('manage_all_schedules');
		$view->schedules = $schedules;
		$this->template->content = $view;
	
	}
	
	public function add_open_ticket()
	{
		Authlite::check_admin();
		Authlite::verify_referer();
		$this->template->title='Add fixed expenses';
		
		if($_POST)
		{
			
				$post = new Validation($_POST);
				$post->add_rules('open_ticket_name','required');
				$post->add_rules('open_ticket_price','required');
				//$post->add_rules('open_ticket_town_from','required');
				$post->add_rules('open_ticket_town_to','required');
				
				if ($post->validate())
				{	
					$open_ticket_name=$_POST['open_ticket_name'];
					$open_ticket_price=$_POST['open_ticket_price'];
					$agency_from=$_POST['agency_from'];
					$open_ticket_town_from=get::agency_town($agency_from);
					$open_ticket_town_to=$_POST['open_ticket_town_to'];
					
					if ($open_ticket_town_from == $open_ticket_town_to)
					{
						$this->session->set('notice', array('message'=>Kohana::lang('same_origin_destination'),'type'=>'error'));
						url::redirect('manage/online_settings');
					}else
					{
					$notice = Open_Ticket_Model::add_open_ticket($agency_from,$open_ticket_name,$open_ticket_price,$open_ticket_town_from,$open_ticket_town_to);		
					
						$this->session->set('notice', array('message'=>Kohana::lang('backend.saved'),'type'=>'notice'));
						url::redirect('manage/online_settings');
					}
				}
				else
				{
					$errors=$post->errors('errors');
					$notice="";
						foreach($errors as $error) 
						{
							$notice.=$error."<br />";
						}
						$this->session->set('notice', array('message'=>$notice,'type'=>'error'));
				}
				url::redirect('manage/online_settings');
			
			
			
		}
		//$view=new View('manage/online_settings');
		//$this->template->content=$view;
	}
	
	public function delete_open_ticket($open_ticket_id)
	{		
		Authlite::check_admin();
		Authlite::verify_referer();
		
		//$this->template->title="Delete open expense";
		
		$notice = Open_Ticket_Model::delete_open_ticket($this->agency_id,$open_ticket_id);		
		
	
		$this->session->set('notice', array('message'=>$notice,'type'=>'success'));
		url::redirect('manage/online_settings');
			
	}
	
	public function online_settings()
	{	
		Authlite::check_admin();
		//Authlite::verify_referer();
		$this->template->title='Online settings';
		
		$parent = get::_parent($this->agency_id);
		
		$open_tickets = Open_ticket_Model::get_all_for_parent($parent);
		//url::site('manage/online_settings');
			
		$view = new View('manage_online_settings');
		$view->open_tickets=$open_tickets;
		$view->towns =  Town_Model::get_all();
		
		
		$parent_agencies = get::all_children($parent);	
		$view->parent_agencies = $parent_agencies;
		
		$this->template->content = $view;
	}
	
	public function cash()
	{	
		//force the sub-menu to show all branches
		$_SESSION['current_branch'] = 0;
		Authlite::check_admin();
		Authlite::verify_referer();
		
		$this->template->title = Kohana::lang('backend.cash');
		$parent_id=get::_parent($this->agency_id)->id;
		
		if($this->current_branch == '0')
		{
			//Get all ctb for the parent of the person signed in
			$ctb = manage::get_cash_to_bank_for_parent($parent_id);	
		}else
		{
			$ctb = manage::get_cash_to_bank_for_agency($this->current_branch);
		}
		
		$per_page = 15;
		$all_collections_total = count($ctb);
		$this->pagination = new Pagination(array(
			// 'base_url'    => 'welcome/pagination_example/page/', // base_url will default to current uri
			'uri_segment'    => 'page', // pass a string as uri_segment to trigger former 'label' functionality
			'total_items'    => $all_collections_total, // use db count query here of course
			'items_per_page' => $per_page , // it may be handy to set defaults for stuff like this in config/pagination.php
			'style'          => 'punbb', // pick one from: classic (default), digg, extended, punbb, or add your own!
			//'query_string'   => "ORM::factory('schedule')->where('agency_id',$agency_id)->where('status','current')->find_all()"
		));
			
		if($this->current_branch == '0')
		{
			//Get all ctb for the parent of the person signed in
			$ctb = ORM::factory('cash_to_bank')->join('agencies','agencies.id','cash_to_banks.agency_id')->where('agencies.parent_id',$parent_id)->where('deleted','0')->orderby('created_on','DESC')->find_all();	
		}else
		{
			$ctb=ORM::factory('cash_to_bank')->where('agency_id',$this->current_branch)->where('deleted','0')->orderby('date_incurred','DESC')->find_all();
		}		$view->collections=$ctb;
		
		
		
		$view = new View('manage_cash');
		$view->ctb = $ctb;
		$this->template->content = $view;
	}
		
		
	public function expenses()
	{
		Authlite::check_admin();
		//Authlite::verify_referer();
		$agency_id = $this->agency_id;
		$parent_id = get::_parent($agency_id)->id;
		//$admin_group_id = $this->admin->admin_group_id;
		$this->template->title=Kohana::lang('backend.expenses');
		
		//If it all branches
		if($this->current_branch == '0')
		{
			//Get all expenses for the parent of the person signed in
			$all_expenses = manage::get_all_expenses_for_parent($parent_id);	
		}else
		{
			$all_expenses = manage::get_all_expenses_for_agency($this->current_branch);	
		}
		
		$all_expenses_total = count($all_expenses);
		
		//pagination
		$per_page = 15;
		$all_expenses_total = count($all_expenses);
		
		$this->pagination = new Pagination(array(
			// 'base_url'    => 'welcome/pagination_example/page/', // base_url will default to current uri
			'uri_segment'    => 'page', // pass a string as uri_segment to trigger former 'label' functionality
			'total_items'    => $all_expenses_total, // use db count query here of course
			'items_per_page' => $per_page , // it may be handy to set defaults for stuff like this in config/pagination.php
			'style'          => 'punbb', // pick one from: classic (default), digg, extended, punbb, or add your own!
			//'query_string'   => "ORM::factory('schedule')->where('agency_id',$agency_id)->where('status','current')->find_all()"
		));
			
		if($this->current_branch == '0')
		{
			//Get all expenses for the parent of the person signed in
			$all_expenses=ORM::factory('expense')->join('agencies','agencies.id','expenses.agency_id')->where('agencies.parent_id',$parent_id)->where('deleted','0')->orderby('date_incurred','DESC')->find_all();
		}else
		{
			$all_expenses=ORM::factory('expense')->where('agency_id',$this->current_branch)->where('deleted','0')->orderby('date_incurred','DESC')->find_all();
		}
		
		
		$view = new View('manage_expenses');
		//$view->all_buses = $all_buses;
		$view->all_expenses = $all_expenses;
		$this->template->content = $view;
			
	}
	
	public function expense_details($expense_id)
	{	
		Authlite::check_admin();
		Authlite::verify_referer();
		$this->template->title = '';
		$expense = get::expense($expense_id);
		
		
		$view = new View('manage_expense_details');
		$view->expense = $expense;
		$this->template->content = $view;
	
	}
		
	public function online_purchases() 
	{	
		Authlite::check_admin();
		//Authlite::verify_referer();
				
		$agency_id = $this->agency_id;
		$this->template->title = 'Purchases from online';
		$view=new View ('manage_online_purchases');
		
		//If it all branches
		if($this->current_branch == '0')
		{
			//Get all expenses for the parent of the person signed in
			$online_purchases = manage::get_all_expenses_for_parent($this->parent_id);	
		}else
		{
			$online_purchases = manage::get_active_purchases_for_agency($agency_id);	
		}
		
		$per_page = 15;		
			
		$online_purchases_total = count($online_purchases);
		
		$this->pagination = new Pagination(array(
			// 'base_url'    => 'welcome/pagination_example/page/', // base_url will default to current uri
			'uri_segment'    => 'page', // pass a string as uri_segment to trigger former 'label' functionality
			'total_items'    => $online_purchases_total, // use db count query here of course
			'items_per_page' => $per_page , // it may be handy to set defaults for stuff like this in config/pagination.php
			'style'          => 'punbb', // pick one from: classic (default), digg, extended, punbb, or add your own!
			//'query_string'   => "ORM::factory('schedule')->where('agency_id',$agency_id)->where('status','current')->find_all()"
		));

		if($this->current_branch == '0')
		{
			//Get all expenses for the parent of the person signed in
			$online_purchases = ORM::factory('purchase')->join('agencies','agencies.id','purchases.agency_id')->where('agencies.parent_id',$this->parent_id)->where('agency_id',$agency_id)->where('status','active')->limit($per_page,$this->pagination->sql_offset)->find_all();
		}else
		{
			$online_purchases = ORM::factory('purchase')->where('agency_id',$this->current_branch)->where('status','active')->limit($per_page,$this->pagination->sql_offset)->find_all();
		}
		
		
		//$current_schedules=Schedule_Model::get_all_agency_current_schedules($agency_id);
		$view->online_purchases=$online_purchases;
		//$view->current_schedules=$current_schedules;
		$view->agency_id=$agency_id;
		$this->template->content=$view;
	}
	
	public function activity_by_period_by_bus()
	{	
		Authlite::check_admin();
		Authlite::verify_referer();
		$this->template->title = Kohana::lang('backend.reports_for_bus');
		
		if($_POST)
		{
			
			if(isset($_POST['by_period']))
			{
				$post = new Validation($_POST);
				$post->add_rules('start_date','required');
				$post->add_rules('end_date','required');
				$post->add_rules('bus_number','required');
				if ($post->validate())
				{	
					
					$start_date = date("Y-m-d",strtotime($_POST['start_date']));
					$end_date = date("Y-m-d",strtotime($_POST['end_date']));
					$bus_number=$_POST['bus_number'];
					$display_option=$_POST['display_options'];
					
				
					
					//$period_expenses=Expense_Model::get_agency_expenses_by_period_by_bus($this->agency_id,$start_date,$end_date,$bus_number);
					$period_expenses=manage::get_parent_expenses_by_period_by_bus($this->parent_id,$start_date,$end_date,$bus_number);
					
					//$period_schedules=Schedule_Model::get_agency_schedules_by_period_by_bus($this->agency_id,$start_date,$end_date,$bus_number);
					$period_schedules=manage::get_parent_schedules_by_period_by_bus($this->parent_id,$start_date,$end_date,$bus_number);
					
					$period_income=manage::get_parent_income_by_period_by_bus($this->parent_id,$start_date,$end_date,$bus_number);
					//$period_schedules=Ticket_Detail_Model::get_agency_tickets_by_period_by_admin($agency_id,$start_date,$end_date,$admin);
					
					//To display timeline of events by date/chronological transaction history, create array, put ALL el'ts inside, then sort by date
					$all_activity = array();
					foreach($period_expenses as $exp)
					{
						$all_activity[] = array('date'=>strtotime($exp->date_incurred), 'description'=>$exp->purpose, 'amount'=>$exp->amount, 'type'=>'debit', 'admin'=>$exp->CreatedBy);
					}
					foreach($period_schedules as $schedule)
					{
						$all_activity[] = array('date'=>strtotime($schedule->checked_out_on), 'description'=>'Schedule check out', 'amount'=>$schedule->total_amount, 'type'=>'credit', 'admin'=>$schedule->CheckedOutBy);
					}
					foreach($period_income as $income)
					{
						$all_activity[] = array('date'=>strtotime($income->date_incurred), 'description'=>$income->purpose, 'amount'=>$income->amount, 'type'=>'credit', 'admin'=>$income->CreatedBy);
					}
					
					//Compare function to sort the array by datetime
					function cmp($a, $b)
					{
						if ($a['date'] == $b['date']) {
							return 0;
						}
						return ($a['date'] < $b['date']) ? -1 : 1;
					}
					usort($all_activity, "cmp");
					

					
					
					
					//calculate the total expenses for that period
					$total_expenditure = 0;
					foreach($period_expenses as $exp )
					{
						$total_expenditure += $exp->amount;
					}
				
					//Calculate total amount expected from departed buses for the period. DEPARTED BUSES ONLY
					$total = 0;
					foreach($period_schedules as $ds)
					{
						$total += $ds->total_amount;
					}
					
					$income_total = 0;
					foreach($period_income as $income)
					{
						$income_total += $income->amount;
					}
				}
				else
				{
					$errors=$post->errors('errors');
					$notice="";
						foreach($errors as $error) 
						{
							$notice.=$error."<br />";
						}
						$this->session->set('notice', array('message'=>$notice,'type'=>'error'));
						url::redirect('admin/main/'.$this->admin->agency_id);
				}
				
				if($display_option=='grouped')
				{
					$view = new View('activity_by_period_by_bus');
				}else{
					$view = new View('manage_activity_by_period_by_bus_timeline');
				}
				//$view->parcels=$period_parcels;
				$view->total_expenditure=$total_expenditure;
				$view->period_income = $period_income;
				$view->period_expenses = $period_expenses;
				$view->period_schedules = $period_schedules;
				$view->total = $total;
				$view->income_total = $income_total;
				$view->start_date = $start_date;
				$view->end_date = $end_date;
				$view->all_activity = $all_activity;
				$view->bus_number = $bus_number;
			}
		}
		
		$this->template->content = $view;
	
	}

	public function reports() 
	{	
		//force the sub-menu to show all branches
		$_SESSION['current_branch'] = 0;
		$agency_id = $this->agency_id;
		$parent = get::_parent($this->agency_id);
		
		//$siblings = get::agency_siblings($agency_id,$parent);
		$all_children = get::all_children($parent);
		
		//Authlite::check_agency_integrity($agency_id);
		Authlite::verify_referer();
		
	
		$this->template->title = Kohana::lang('backend.welcome_backend');
		$view=new View ('manage_reports_main');
		
		//$admins=get::agency_admins($this->agency_id);
		//$admins_excluding_current = Admin_Model::get_admins_excluding_current($this->agency_id, $this->admin->username);
		$parent_id = get::_parent($this->agency_id)->id;		
		
		//pagination
		$per_page = 15;	
		//get all buses for this parent
		$all_buses = manage::get_all_buses_for_parent($parent_id);

		
		//$view->admins=$admins;
		//$view->admins_excluding_current=$admins_excluding_current;
		$view->all_buses=$all_buses;
		$view->all_children = $all_children;
		$this->template->content=$view;
		
	}

		
	public function reports_activity_by_period()
	{	
		Authlite::check_admin();
		Authlite::verify_referer();
		
		//$agency_id = $this->agency_id;
		//$type = $this->admin->admin_group_id;
		//Authlite::check_agency_integrity($agency_id);
		$this->template->title = Kohana::lang('backend.welcome_backend');
		
		if($_POST)
		{
			$post = new Validation($_POST);
			$post->add_rules('agency_id','required');
			$post->add_rules('start_date','required');
			$post->add_rules('end_date','required');
			if ($post->validate())
			{	
				
			$agency_id = $_POST['agency_id'];
			$parent_id = get::_parent($agency_id)->id;
			
			$start_date = date("Y-m-d",strtotime($_POST['start_date']));
			$end_date = date("Y-m-d",strtotime($_POST['end_date']));
		
			
				$period_parcels=get::agency_parcels_by_period($agency_id,$start_date,$end_date);
				$period_expenses=get::agency_expenses_by_period($agency_id,$start_date,$end_date);
				$period_schedules=get::agency_schedules_by_period($agency_id,$start_date,$end_date);
				$period_income=Income_Model::get_agency_income_by_period($agency_id,$start_date,$end_date);
				//$all_activity=get::agency_activity_by_period($agency_id,$start_date,$end_date);
				
				//Remote tickets don't feature under schedules, hence they are not included in this report. They need to be.
				$remote_outgoing_tickets=Ticket_Detail_Model::get_agency_outgoing_remote_tickets_by_period_all_admins($agency_id,$start_date,$end_date);

				
				//calculate the total expenses for that period
				$total_expenditure = 0;
				foreach($period_expenses as $exp )
				{
					$total_expenditure += $exp->amount;
				}
			
				//Calculate total amount expected from departed buses for the period. DEPARTED BUSES ONLY
				$total = 0;
				foreach($period_schedules as $ds)
				{
					$total += $ds->total_amount;
				}
			}
			else
			{
				$errors=$post->errors('errors');
				$notice="";
					foreach($errors as $error) 
					{
						$notice.=$error."<br />";
					}
					$this->session->set('notice', array('message'=>$notice,'type'=>'error'));
					url::redirect('admin/main/'.$this->admin->agency_id);
			}
	}
	$view = new View('manage_reports_activity_by_period');
	
	$view->parcels=$period_parcels;
	$view->remote_outgoing_tickets=$remote_outgoing_tickets;
	$view->total_expenditure=$total_expenditure;
	$view->period_parcels = $period_parcels;
	$view->period_expenses = $period_expenses;
	$view->period_schedules = $period_schedules;
	$view->period_income = $period_income;
	$view->total = $total;
	$view->start_date = $start_date;
	$view->end_date = $end_date;
	$view->agency_id = $agency_id;
	
		
	$this->template->content = $view;
	
	}
	
	
	
	
		
	}