<?php defined('SYSPATH') or die('No direct script access'); ?> 


	<?php
		//determine what type of notice to display if at all
		$notice = $this->session->get_once('notice');
			if(!empty($notice)){ 
				if($notice['type'] == 'success'){?><div class="alert general success"><a class="close" data-dismiss="alert" href="#">&times;</a><?php echo $notice['message']; }?></div>
			<?	if($notice['type'] == 'error'){?><div class="alert general error"><a class="close" data-dismiss="alert" href="#">&times;</a><?php echo $notice['message']; }?></div>
		<?}?>
    <div class="container">
	  	
	<div class="row-fluid">	
		
		<div class="span12  hero-unit prof-unit">
		
		<ul class="nav nav-tabs">
					<li > <a href="<?=url::site('parcels/check_parcels')?>"><i class="icon-arrow-left"></i> <?=Kohana::lang('backend.back_parcels')?></a></li>
		</ul>
		
		<div class="row-fluid span10 offset1">	
		<legend><?=Kohana::lang('backend.all_parcels_on_this_schedule')?> <button class="view" onClick="window.print()"><i class="icon-print"></i> <?=Kohana::lang('backend.print')?></button></legend>
				
				
				<?php 
						//if it was a search, and if the result wasn't zero
						if($count_outgoing != 0){
							$total_amount = 0;
						?>
							<pre>Outgoing</pre>
							
							<div class="clear"></div>
							<table class="table  table-striped table-hover table-condensed">
										<tr>
										<th>Date</th>
										<th><?=Kohana::lang('backend.To')?></th>
										<th><?=Kohana::lang('backend.receiver')?></th>
										<th><?=Kohana::lang('backend.receiver_phone')?></th>
										<th>Description</th>
										<th><?=Kohana::lang('backend.status')?></th>
										<th><?=Kohana::lang('backend.amount')?></th>
										<th>ID</th>
									</tr>
									<?php foreach ($outgoing_parcels as $parcel):?>
									<tr>	
										<td><?=date("d-m-Y",strtotime($parcel->sent_date))?></td>
										
										<td><?php echo get::agency_town_name($parcel->to);?></td>
										
										<td><?=$parcel->receiver_name?></td>
										<td><?=$parcel->receiver_phone?></td>
										<!--Trim if the description is too long. -->
										<td><a href="<?=url::site('parcels/view/'.$parcel->id)?>"><?php if (strlen($parcel->description) > 25) { echo substr($parcel->description,0,35)."...";} else { echo $parcel->description;}?></a></td>
									
										<td >
											<?php		
											//Firstly, if it's from here, then it's sent.
											if($parcel->from == $this->admin->agency_id){?>
											<span class="">Sent</span>
											<?php }
											?>
										</td>
										<td>
											<?=$parcel->price?>
										</td><td>
											<?=$this->agency_id.$parcel->id?>
										</td>
									</tr>
									<?php 
												//sum the prices
												$total_amount += $parcel->price; 
									?>
									
									<?php endforeach; ?>
									<tr>
												<th><?=Kohana::lang('backend.total')?></th>
												<th><?=number_format($total_amount)?> FCFA </th>
												
									</tr>
							</table>
							<?php }else{echo "No parcel available.<br/>";} ?>
							
						
									
									
						
		</div>
	
		
		</div>
	
    </div> 

	</div>
	
	<script type="text/javascript">

	
	</script>