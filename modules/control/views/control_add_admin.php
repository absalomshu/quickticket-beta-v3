<?php defined('SYSPATH') or die('No direct script access'); ?> 
<!DOCTYPE html>

<?php //var_dump($logo);?>
		
	<div class="span6 hero-unit prof-unit offset3 ">
			
				
			<h20><?=Kohana::lang('backend.add_admin')?><a href="<?=url::site('settings#users-tab')?>" class="view"><?=Kohana::lang('backend.back')?></a></h20>
			<div class="rule"><hr/></div><br/>
				<form method="POST" action="<?=url::site('settings/add_admin')?>" enctype="multipart/form-data" class="form-horizontal">
				
				  
				  <div >
					<label class="control-label " for="inputEmail"><?=Kohana::lang('backend.username')?>:</label>
					<div class="controls">
								<input type="text"   name="username"/>&nbsp;&nbsp;
					</div>
				  </div>
				  <div >
					<label class="control-label " for="inputEmail"><?=Kohana::lang('backend.password')?>:</label>
					<div class="controls">
								<input type="password"   name="password"/>&nbsp;&nbsp;
					</div>
				  </div>
			
				    <div class="span4 offset1  download">
				  <?php
						$main_modules = Main_Module_Model::get_all();
					
						foreach($main_modules as $module)
						{ 
							$is_allowed_main_module = Admin_Module_Model::admin_is_allowed_main_module($admin->username,$module->id);
						?>
							<input type="hidden" name="all_modules[<?=$module->id?>]" value="0" />
							<label class="checkbox"><input type="checkbox"  name="all_modules[<?=$module->id?>]" value="1"/><?=Kohana::lang('backend.'.$module->Name)?></label>
						<?php 
						}
				  ?>
				  
					
					</div>			
				  <div class="clear"></div>
				  <div class="form-actions">
								<button class="btn btn-info" type="submit" ><?=Kohana::lang('backend.save')?></button>					
								<a class="btn" href="<?=url::site('settings')?>" ><?=Kohana::lang('backend.cancel')?></a>					
				</div>	
				</form>			
				
			
			

		</div>

