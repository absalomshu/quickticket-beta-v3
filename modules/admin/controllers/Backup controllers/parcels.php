<?php defined('SYSPATH') or die('No direct script access');

class Parcels_Controller extends Admin_Controller 
{
	public function __construct()
	{	
		parent::__construct();
		$this->admin = Authlite::instance()->get_admin();
		$this->session = Session::instance();
		//if (!empty($this->admin)) $this->key = $this->admin->agency_id;
		
	}  

	
	public function index()
	{	
		//ensure a logged in user can't land on the signup page
		if (empty($this->admin)) 
		{
			url::redirect('admin/login');
		}
	}
	
	
	public function register_parcel()
	{	
		Authlite::check_admin();
		Authlite::verify_referer();
		//Authlite::check_agency_integrity($agency_id);
		$this->template->title = '';
		$agency_id = $this->agency_id;
		$agency_name = get::agency_name($agency_id);
		$parcel = ORM::factory('parcel');
		if($_POST)
		{	
			//var_dump($_POST);EXIT;
			//no_csrf::check();
			$post = new Validation($_POST);
			$post->add_rules('receiver_name','required');
			$post->add_rules('agency_to','required');
			$post->add_rules('receiver_phone','valid::digit','length[9]');
			$post->add_rules('parcel_description','required');
			$post->add_rules('price','required','valid::alpha_numeric');
			
			if ($post->validate())
			{	
				
				$receiver_name = $_POST['receiver_name'];
				$receiver_phone = $_POST['receiver_phone'];
				$description = htmlentities($_POST['parcel_description'],ENT_QUOTES);
				$to = $_POST['agency_to'];
				$price = $_POST['price'];
				
				
					$parcel->receiver_phone = $receiver_phone;
					$parcel->description = $description;
					$parcel->receiver_name = $receiver_name;
					$parcel->agency_id = $agency_id;
					$parcel->from = $agency_id;
					$parcel->to = $to;
					$parcel->sent_date = date('Y-m-d');
					$parcel->price = $price;
					$parcel->CreatedBy = strtoupper($this->admin->username);
					$parcel->save();	
					
					//send confirmation SMS			
					$msg = Kohana::lang('backend.hello').$receiver_name.". ".Kohana::lang('backend.parcel_deposited').$agency_name.". " .Kohana::lang('backend.inform_you');
					$to = "237".$receiver_phone;
					//$to = "23796362464";
					
					//send_sms::index();
					//this doesn't keep track of SMS dispatcching. Does direct sending by calling WAsamundi API. No records. Works perfectly though.
					//$result = sms::send($msg,$to,$this->agency_id);
					
					//this keeps records.
					
					//if a phone number is set, save to db for SMS to be sent
					if(!empty($receiver_phone))
					{	
						$result = sms::save($msg,$to,$this->agency_id);
					}
					
					$this->session->set('notice', array('message'=>Kohana::lang('backend.parcel_registered')." (<a href='". url::site('parcels/all/sent'). "'>". Kohana::lang('backend.all_parcels')."</a>). <br/> "  .@$result ,'type'=>'success'));
					url::redirect('parcels/all/incoming');	
							
			}
			else
			{
				$errors=$post->errors('errors');
				$notice="";
					foreach($errors as $error) 
					{
						$notice.=$error."<br />";
					}
					$this->session->set('notice', array('message'=>$notice,'type'=>'error'));
					//set the user's post in session so he doesn't re-enter stuff if some is wrong
					$this->session->set('post', $_POST);
					url::redirect('parcels/register_parcel');
			}
		}
		$view = new View('admin_parcels');
		$parent = get::_parent($agency_id);
		$siblings = get::agency_siblings($agency_id,$parent);	
		$view->siblings = $siblings;
		$this->template->content=$view;
	
	}
	
	
	//type is either sent or incoming
	public function all($type="sent") 
	{	
		Authlite::check_admin();
		Authlite::verify_referer();
		
		$agency_id = $this->agency_id;
		
		//pagination
		$per_page = 15;
		
		$sent= get::all_agency_sent_parcels($agency_id);
		
		$incoming= get::all_agency_incoming_parcels($agency_id);
				
		$sent_total = count($sent);
		$incoming_total = count($incoming);
		
		$this->sent_pagination = new Pagination(array(
			// 'base_url'    => 'welcome/pagination_example/page/', // base_url will default to current uri
			'uri_segment'    => 'page', // pass a string as uri_segment to trigger former 'label' functionality
			'total_items'    => $sent_total, // use db count query here of course
			'items_per_page' => $per_page , // it may be handy to set defaults for stuff like this in config/pagination.php
			'style'          => 'punbb', // pick one from: classic (default), digg, extended, punbb, or add your own!
			//'query_string'   => "ORM::factory('schedule')->where('agency_id',$agency_id)->where('status','current')->find_all()"
		));$this->incoming_pagination = new Pagination(array(
			// 'base_url'    => 'welcome/pagination_example/page/', // base_url will default to current uri
			'uri_segment'    => 'page', // pass a string as uri_segment to trigger former 'label' functionality
			'total_items'    => $incoming_total, // use db count query here of course
			'items_per_page' => $per_page , // it may be handy to set defaults for stuff like this in config/pagination.php
			'style'          => 'punbb', // pick one from: classic (default), digg, extended, punbb, or add your own!
			//'query_string'   => "ORM::factory('schedule')->where('agency_id',$agency_id)->where('status','current')->find_all()"
		));
		
		
		$this->template->title = Kohana::lang('backend.welcome_backend');
		//If not specified, shoe incoming parcels
		
			//$parcels = get::all_agency_sent_parcels($agency_id);
			$sent_parcels=ORM::factory('parcel')->where('from',$agency_id)->orderby('sent_date','DESC')->limit($per_page,$this->sent_pagination->sql_offset)->find_all();
		
			//$parcels = get::all_agency_incoming_parcels($agency_id);
			$incoming_parcels=ORM::factory('incoming_parcel')->where('to',$agency_id)->orderby('sent_date','DESC')->limit($per_page,$this->incoming_pagination->sql_offset)->find_all();
		
		
		$view = new View('admin_parcels');
		$view->sent_parcels = $sent_parcels;
		$view->incoming_parcels = $incoming_parcels;
		$this->template->content = $view;

	}
	
	public function receive($parcel_id)
	{	
		Authlite::check_admin();
		Authlite::verify_referer();
		$agency_name = get::agency_name($this->agency_id);
		$this->template->title = '';
		#$parcel = get::parcel($parcel_id);
		$parcel = get::incoming_parcel($parcel_id);
		$parcel->state = 'received';
		
		//send confirmation SMS			
		$msg = Kohana::lang('backend.hello').$parcel->receiver_name.". ".Kohana::lang('backend.parcel_available').$agency_name.". ".Kohana::lang('backend.collect_it');
		$to = "237".$parcel->receiver_phone;
		//$to = "23796362464";
		
		//Send tries to send automatically, and will fail unretrievable in absence of connection
		//$result = sms::send($msg,$to,$this->agency_id);
		
		//save saves to offline db, pushes to online db, then tries to send.
		//first check it's not a parcel without phone number
		if(!empty($parcel->receiver_phone))
		{
		$result = sms::save($msg,$to,$this->agency_id);
		}
		
		$this->session->set('notice', array('message'=>Kohana::lang('backend.parcel_received')."<br/> Notice: "  .@$result ,'type'=>'success'));			
		//save here coz when done above, the SMS isn't sent due to the redirect immediately after
		$parcel->save();
		url::redirect('parcels/all/incoming/'.$this->admin->agency_id);
	}	
	
	
	public function deliver($parcel_id)
	{	
		Authlite::check_admin();
		Authlite::verify_referer();
		$this->template->title = '';
		#$parcel = get::parcel($parcel_id);
		$parcel = get::incoming_parcel($parcel_id);
	
		if($_POST)
		{
			$post = new Validation($_POST);
			if ($post->validate())
			{	
				$collector_name = $_POST['collector_name'];
				$collector_idc = $_POST['collector_idc'];
				$collector_phone = $_POST['collector_phone'];
				
				$collector_info = array('name'=>$collector_name, 'idc'=>$collector_idc, 'phone'=>$collector_phone);
				
				//json encode and save
				$parcel->collected_by = json_encode($collector_info);
				$parcel->collected_on = date('Y-m-d H:i:s');
				$parcel->state = 'delivered';
				$parcel->save();
					
					$this->session->set('notice', array('message'=>Kohana::lang('backend.marked_delivered'),'type'=>'success'));
					url::redirect('parcels/all/incoming/'.$this->admin->agency_id);

					//if required, send email to notify sender that parcel has been delivered.
					//Hence the parcel needs some variable set at the sending agency if that's the case
			}else
			{
				$errors=$post->errors('errors');
				$notice="";
				foreach($errors as $error) 
				{
					$notice.=$error."<br />";
				}
				$this->session->set('notice', array('message'=>$notice,'type'=>'error'));
			}
		}
		
		$view = new View('admin_deliver_parcel');
		$view->parcel = $parcel;
		$this->template->content = $view;
	}
	
	
	public function view($parcel_id)
	{	
		Authlite::check_admin();
		Authlite::verify_referer();
		
		$this->template->title = '';
		
		$parcel = get::parcel($parcel_id);
	
		if($_POST)
		{
			$post = new Validation($_POST);
			if ($post->validate())
			{	
				$collector_name = $_POST['collector_name'];
				$collector_idc = $_POST['collector_idc'];
				$collector_phone = $_POST['collector_phone'];
				
				$collector_info = array('name'=>$collector_name, 'idc'=>$collector_idc, 'phone'=>$collector_phone);
				
				//json encode and save
				$parcel->collected_by = json_encode($collector_info);
				$parcel->collected_on = date('Y-m-d H:i:s');
				$parcel->state = 'delivered';
				$parcel->save();
				
				$this->session->set('notice', array('message'=>Kohana::lang('backend.marked_delivered'),'type'=>'success'));
				url::redirect('parcels/all/incoming/'.$this->admin->agency_id);

				//if required, send email to notify sender that parcel has been delivered.
				//Hence the parcel needs some variable set at the sending agency if that's the case
			}else
			{
				$errors=$post->errors('errors');
				$notice="";
				foreach($errors as $error) 
				{
					$notice.=$error."<br />";
				}
				$this->session->set('notice', array('message'=>$notice,'type'=>'error'));
			}
		}
		
		$view = new View('admin_view_parcel');
		$view->parcel = $parcel;
		$this->template->content = $view;
	}
	
	
	public function view_incoming($parcel_id)
	{	
		Authlite::check_admin();
		Authlite::verify_referer();
		$this->template->title = '';
		
		$parcel = get::incoming_parcel($parcel_id);
	
		if($_POST)
		{
			$post = new Validation($_POST);
			if ($post->validate())
			{	
				$collector_name = $_POST['collector_name'];
				$collector_idc = $_POST['collector_idc'];
				$collector_phone = $_POST['collector_phone'];
				
				$collector_info = array('name'=>$collector_name, 'idc'=>$collector_idc, 'phone'=>$collector_phone);
				
				//json encode and save
				$parcel->collected_by = json_encode($collector_info);
				$parcel->collected_on = date('Y-m-d H:i:s');
				$parcel->state = 'delivered';
				$parcel->save();
					
					$this->session->set('notice', array('message'=>Kohana::lang('backend.marked_delivered'),'type'=>'success'));
					url::redirect('parcels/all/incoming/'.$this->admin->agency_id);

					//if required, send email to notify sender that parcel has been delivered.
					//Hence the parcel needs some variable set at the sending agency if that's the case
			}else
			{
				$errors=$post->errors('errors');
				$notice="";
				foreach($errors as $error) 
				{
					$notice.=$error."<br />";
				}
				$this->session->set('notice', array('message'=>$notice,'type'=>'error'));
			}
		}
		
		$view = new View('admin_view_parcel');
		$view->parcel = $parcel;
		//Set this variable to the view for incoming parcels, so that the print option is not displayed.
		$view->is_incoming = 1;
		$this->template->content = $view;
	}
	
	public function print_ticket()
	{	
		Authlite::check_admin();
		Authlite::verify_referer();
		$this->template->title = '';
		$agency_id=$this->agency_id;
		
	
		if($_POST)
		{
			$post = new Validation($_POST);
			if ($post->validate())
			{	
				$parcel_id = $_POST['parcel_id'];
				$parcel = get::parcel($parcel_id);
				$from = get::agency_town_name($parcel->from);
				$to = get::agency_town_name($parcel->to);
				$name = $parcel->receiver_name;
				//$parent = get::_parent_name($agency_id);
				//var_dump($parent);
				$price = $parcel->price;
				
				$ticket = printer::print_parcel_ticket($agency_id,  $parcel_id, $from, $to, $price, $name);	
				//var_dump($ticket);exit;
				
				//open the ticket in a new window
				echo "<script>
						win=window.open('$ticket','Print ticket','width=900,height=400');
						win.focus();
						
					  </script>
					  ";
				//DO NOT MODIFY THESE LINES. THEY DO THE REDIRECT ENSURING THAT PAGE REFRESH IS NOT POSSIBLE HERE, NOR THE BACK BUTTON  
				echo '<script>
						window.location = view/'.$parcel_id.'; 
				</script>'; 
					
				
			}else
			{
				$errors=$post->errors('errors');
				$notice="";
				foreach($errors as $error) 
				{
					$notice.=$error."<br />";
				}
				$this->session->set('notice', array('message'=>$notice,'type'=>'error'));
			}
		}
		
		$view = new View('admin_view_parcel');
		$view->parcel = $parcel;
		$this->template->content = $view;
	}
		
}
		