<?php defined('SYSPATH') or die('No direct script access');

class Reminder_Schedule_Model extends ORM{

	public function add_schedule($agency_id, $reminder_id, $due_date)
	{	
		$reminder = ORM::FACTORY('reminder_schedule');
		$reminder->AgencyID = $agency_id;
		$reminder->ReminderID = $reminder_id;
		$reminder->DueDate = $due_date;
		$reminder->save();
		//die('here');
		return "Reminder added.";
	}
	

	
	public function get_all($agency_id)
	{
		$reminders=ORM::factory('reminder')->where('AgencyID',$agency_id)->find_all();
		return $reminders;	
	}
	
	public function get_schedules_for_reminder($agency_id, $reminder_id)
	{
		$reminders=ORM::factory('reminder_schedule')->where('AgencyID',$agency_id)->where('ReminderID',$reminder_id)->find_all();
		return $reminders;	
	}
	
	public function get_reminder_schedule($agency_id,$reminder_schedule_id)
	{
		$reminder=ORM::factory('reminder_schedule')->where('AgencyID',$agency_id)->where('id',$reminder_schedule_id)->find();
		return $reminder;	
	}
		

	public function complete_reminder_schedule($agency_id,$reminder_schedule_id)
	{
		
		$requirement=ORM::factory('reminder_schedule')->where('AgencyID',$agency_id)->where('id',$reminder_schedule_id)->find();
		$requirement->IsCompleted='1';
		$requirement->ModifiedBy=$this->admin->username;
		
		$requirement->save();
		
		$notice="Reminder completed";
		return $notice;
		
	}
	

}
