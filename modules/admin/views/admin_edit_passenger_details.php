<?php defined('SYSPATH') or die('No direct script access'); ?> 


				
	<div class="row-fluid marketing" style="margin:5px 0 0px 0;">	
					
		<div class="span8 " style="margin-left:0px;" >
				
			<div class="span12 hero-unit prof-unit" >
					<form action="<?=url::site('admin/complete_schedule/'.$schedule_id)?>" method="POST">
						
					
							
							 
							<div class="tab-content">
							  <div class="tab-pane active" id="seat-tab">
								<div class="heading"><?=Kohana::lang('backend.edit_passenger')?>
									
								</div>
								
								<div class="rule"><hr/></div>
											
								<div class="span3 spacious">
									<div class="control-group">
									<label class="control-label lab" for="inputType"  ><?=Kohana::lang('backend.client_name')?><span class="red"> *</span> :</label>
										<div class="controls ">
											<input type="text" class="span12" name="client_name" value="<?=$details->ClientName?>"  required/>
										</div>
									</div>
								</div>
								<div class="span3 spacious">
									<div class="control-group">
									<label class="control-label lab" for="inputType"><?=Kohana::lang('backend.client_id')?>:</label>
										<div class="controls ">
											<!--Check if value is not zero in the db to avoid zeroes being re-saved-->
											<input type="text" class="span12"   name="client_idc" value="<?php if($details->ClientIDC != '0'){echo $details->ClientIDC;}?>" >
										</div>
									</div>
								</div>
								<div class="span3 spacious">
									<div class="control-group">
									<label class="control-label lab" for="inputType"><?=Kohana::lang('backend.phone')?>:</label>
										<div class="controls input-append">
											<input type="text" class="span12"  name="client_phone" value="<?php if($details->ClientPhone != '0'){echo $details->ClientPhone;}?>" >
										</div>
									</div>
								</div>
								<div class="clear"></div>
								<div class="span3 spacious">
									<div class="control-group">
									<label class="control-label lab" for="inputType"><?=Kohana::lang('backend.email')?>:</label>
										<div class="controls ">
											<input  type="email" class="span12" name="client_email" value="<?=$details->ClientEmail?>" email>
										</div>
									</div>
								</div>
								
								
							
								<div class="clear"></div>
								
								<div class="span3 spacious" >
									<div class="control-group">
										<label class="control-label lab" for="inputType"><?=Kohana::lang('backend.seat')?>:</label>
										<div class="short-dropdown">
											<select name="seat_number" id="inputType" readonly>
											
												<option value="<?=$seat_number?>"><?=$seat_number?></option>
											
											</select>
										</div>
										
										<div >
											<button class="btn btn-info" type="submit" name="edit_passenger"><i class="icon-check"></i> <?=Kohana::lang('backend.save')?></button><br/>
										</div>
																
									</div>
								
								</div>
								<div class="span3 spacious" >
									<div class="control-group">
										<label class="control-label lab" for="inputType">&nbsp;</label>
										
										<div >
											<a class="btn" href="<?=url::site('admin/complete_schedule/'.$schedule_id)?>" name="edit_passenger"><?=Kohana::lang('backend.cancel')?></a><br/>
										</div>
																
									</div>
								
								</div>
							
						
						<div class="clear"></div>
						
						</form>
						
					 </div>
			  
				
			</div>
					
			</div>

			
			
		</div>
					
				
					
			<script>
			  $(function () {
				$('#myTab a').click(function (e) {
			  e.preventDefault();
			  $(this).tab('show');
			})
			  })
			  
			</script>
					
			
					<span class="loaddiv">
						<!-- DON'T DELETE THIS DIV. IT'S FOR THE ONLINE TRANSACTIONS-->	
					</span>
					
					</div>
	  
<?php //$this->profiler = new Profiler();?>
	
	  
	<script type="text/javascript"> 

	</script>
	<script type="text/javascript">
	
	
</script>
  
  
  
  
