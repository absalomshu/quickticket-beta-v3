<?php defined('SYSPATH') or die('No direct script access');

class Tickets_Controller extends Admin_Controller
{
	public function print_single()
	{	
		//flag that indicates wether the template view sould be rendered or not
		//Without auto_render, for some reason, it shows 404 no permission
		
		$this->auto_render = false;
		$seat_number = $_POST['seat_number'];
		$departure_date = $_POST['departure_date'];
		$schedule_id = $_POST['schedule_id'];

		//Recollect information as ticket will be regenerated
		$schedule=get::schedule($schedule_id);
		$parent = strtoupper(get::_parent($this->agency_id)->name);
		$from = get::town($schedule->from);
		$to = get::town($schedule->to);
		
		//Get the updated information for the ticket
		$ticket_details = Ticket_Detail_Model::get_ticket($this->agency_id, $schedule_id, $seat_number);
		
		//Regenerate the ticket	
		$ticket = printer::print_ticket($parent, $schedule->departure_time, $schedule->departure_date, $ticket_details->ClientName, $ticket_details->ClientPhone, $ticket_details->SeatNumber, $from, $to, $schedule->bus_number, $schedule->ticket_price, $schedule->id, $ticket_details->ClientIDC);	
		
		//Now, fetch the ticket.
		$ticket = printer::fetch_ticket($departure_date,$schedule_id,$seat_number);	
	
		echo $ticket;
	}
	
	public function test()
	{	
		
		echo 'test';
		
	}	
}
