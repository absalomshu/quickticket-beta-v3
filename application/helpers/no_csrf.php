<?php defined('SYSPATH') or die('No direct script access');

/**
 * Description:  
 
 *
 * @package		 QuickTicket
 * @subpackage 	 Helper
 * @author 		 Absalom Shu
 * @copyright    (c) 2013
 * @license 	 QuickTicket
 */
 
class no_csrf_Core
{	
	//parameters in both set and get are to ensure setting multiple tokens per page doesn't clash
	public function set($i=0){
	
	// Generate a randomized token
	$token = md5(microtime(TRUE) . rand(0, 100000));

	// Save the token to session
	//session_start();
	$_SESSION['token'.$i] = $token;
	// Add an expiration date to prevent fixation.
	//$_SESSION['token-expires'] = time() + 1800;
	
	//Generate the hidden form field
	?>
	<input type="hidden" name="csrf_token" value="<?=$token?>">
	<?php 
	
	}
	
	public function check($i=0){
		
		// Get the form token.
		$supplied_token = $_POST['csrf_token'];

		// Ensure that a token has been previously set.
		//session_start();
		//if (!isset($_SESSION['token'], $_SESSION['token-expires']))
		if (!isset($_SESSION['token'.$i]))
		{
			die ('token is required!');
		}

		$token = $_SESSION['token'.$i];
		//$expires = $_SESSION['token-expires'];

		// Clear the tokens, they are single use.
		$_SESSION['token'.$i] = NULL;
		$_SESSION['token-expires'] = 0;

		if ( /*$expires < time() ||*/ $token != $supplied_token)
		{
			die ('Invalid or expired token! Just might be a security breach');
		}
		//else
		//{
		//	print_r ('Token good!');exit;
		//}
	
	
	}
	
}
	
