<?php defined('SYSPATH') or die('No direct script access'); 
	$current_branch = $this->admin->agency_id;
?> 

    <div class="container">
		<div class="print-area">
			
		</div>
	
	  <!--defines which area of the doc is printed-->
	  <div class="print-area">
	  <div class="row-fluid marketing" >
	  		
	
	  <? $admin=Authlite::instance()->get_admin(); ?>
	  	<div class="span12 hero-unit prof-unit" >
		<div class="heading"><?php 
						//echo get::_parent($this->admin->agency_id)->name." ".get::admins_town($this->admin->agency_id)->name;
						echo get::agency_name($current_branch);
					?> || Schedule for <?=$schedule->bus_number?></div>
			<div class="rule"><hr/></div>
			<div class="span2 spacious">
				<div class="control-group">
				<label class="control-label lab" for="inputType"><?=Kohana::lang('backend.from')?>:</label>
					
					
							<h3><?=get::town($schedule->from);?></h3>
				</div>
			</div>
			<div class="span2 spacious">
				<div class="control-group">
				<label class="control-label lab" for="inputType"><?=Kohana::lang('backend.to')?>:</label>
					
				<h3><?=get::town($schedule->to);?></h3>
					</div>
				</div>
			<div class="span2 spacious">
				<label class="control-label lab" for="inputType"><?=Kohana::lang('backend.time')?>:</label>
									<h3><?=date("g:i A", strtotime($schedule->departure_time));?></h3>

			</div>
			<div class="span2 spacious">
				<div class="control-group">
				<label class="control-label lab" for="inputType">Date:</label>
					
						<h3><?=date("d-m-Y",strtotime($schedule->departure_date));?></h3>
					
				</div>
			</div>
			
			<div class="span2 spacious" >
				<div class="control-group">
				<label class="control-label lab" for="inputType"><?=Kohana::lang('backend.bus_type')?>:</label>
							
							<h3><?=$schedule->bus_seats?> Seater</h3>
							

				</div>
			</div>
			
			<div class="span2 spacious">
				<label class="control-label lab" for="inputType"><?=Kohana::lang('backend.bus_no')?>:</label>
						<h3><?=$schedule->bus_number?></h3>
				
			</div>
			<div class="span2 spacious">
				<label class="control-label lab" for="inputType"><?=Kohana::lang('backend.ticket_price')?>:</label>
						<h3><?=$schedule->ticket_price?>FCFA</h3>
			</div>
			
			<div class="span2 spacious">
				<label class="control-label lab" for="inputType"><?=Kohana::lang('backend.checkedout_at')?>:</label>
						<h3><?=date("g:i A", strtotime($schedule->checked_out_time));?></h3>
			</div>
			
		
			
			  
		</div>
	<div class="notice">	
	<?php
		//determine what type of notice to display if at all
		$notice = $this->session->get_once('notice');
			if(!empty($notice)){ 
				if($notice['type'] == 'success'){?><div class="alert general success"><a class="close" data-dismiss="alert" href="#">&times;</a><?php echo $notice['message']; }?></div>
			<?	if($notice['type'] == 'error'){?><div class="alert general error"><a class="close" data-dismiss="alert" href="#">&times;</a><?php echo $notice['message']; }?></div>
		<?}?>	
	</div>
	<div class="row-fluid marketing" style="margin:5px 0 0px 0;">	
		<div class="span5 hero-unit prof-unit" style="float:right;" >
				<div class="heading"><?=Kohana::lang('backend.statistics')?></div>
				<div class="rule"><hr/></div>
				
				<table class="table table-bordered">
					<tr>
						<td><?=Kohana::lang('backend.total_filled_seats')?></td>
						<td> <?=$no_so?></td>
					</tr>
					<tr>
						<td><?=Kohana::lang('backend.regular_seats')?></td>
						<td><?=$regular_seats?></td>
					</tr>
					<tr>
						<td><?=Kohana::lang('backend.spec_drops')?></td>
						<td><?=$special_drop_count?></td>
					</tr>
					<tr>
						<td><?=Kohana::lang('backend.empty_seats')?></td>
						<td><?=$empty_seats?></td>
					</tr>
					<tr>
						<td><?=Kohana::lang('backend.amt_spec_drops')?></td>
						<td><?=$special_drop_total?></td>
					</tr>
					<tr>
						<td><?=Kohana::lang('backend.free_tickets')?></td>
						<td><?=$free_ticket_count?></td>
					</tr>
					<tr>
						<td><?=Kohana::lang('backend.price_per_ticket')?></td>
						<td><?=$ticket_price?> FCFA</td>
					</tr>	
					<tr>
						<th>GRAND TOTAL </th>
						<th><?=$total?> FCFA</th>
					</tr>	
				</table>
				
		</div>
		<div class="span7 hero-unit prof-unit" style="margin-left:0px;" >
			<div class="heading">SLIP / BORDEREAU <button class="view" onClick="window.print()"><i class="icon-print"></i> <?=Kohana::lang('backend.print')?></button></div>
			<div class="rule"><hr/></div>
				<form action="<?=url::site('admin/complete_schedule/'.$schedule->id)?>" method="POST">
				
		
			
			
			
			
			<?php 
			//print_r($seats_and_occupants);exit;
			if (!empty($seats_and_occupants) OR !empty($seats_and_reservers))
			{?>
				<div>
						<?php 
						//combine reserved and taken
						//COMBINING INEFFICIENT AS IT IS A 2D AND A 3D
						//$all_seats = $seats_and_reservers + $seats_and_occupants;
						//ksort($all_seats);
						//foreach($seats_and_reservers as $key=>$subkey){print_r($subkey['name']);echo"<br/>";}?>
						<table class="table table-bordered">
							<tr>
									<th><?=Kohana::lang('backend.seat')?></th>
									<th><?=Kohana::lang('backend.name')?></th>
									<th>IDC No.</th>
									<th>Phone</th>
									<th>*<?=Kohana::lang('backend.spec_drops')?></th>
									<!--<th>State</th>-->
							</tr>
							<?php 
						
							//sort the array in ascending order
							ksort($seats_and_occupants);
							foreach ($seats_and_occupants as $key=>$value)
							{ ?>
									<tr>
										<td><?=$key?><?php if(isset($value['free_ticket']) && $value['free_ticket']==1){?> <span class="badge badge-warning" title="Free-ticket">F</span> <?php }?></td>
										<td><?=$value['name']?></td>
										<td><?=$value['idc']?></td>
										<td><?=$value['phone']?></td>
										<!--supress error if it's not a special drop, and hence has no special drop town. else, display it-->
										<td><?=@get::town($value['sd_town'])?></td>
										<!--<td></td>-->
									</tr>
								
								
								
							<?php } ;?>
						
						</table>	
				</div>
			<?php }?>	
			</form>
		</div>
		
		
		
		</div></div>
	  </div>
	  <div style="height:19px;"></div>
    </div> 

