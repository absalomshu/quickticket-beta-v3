<?php defined('SYSPATH') or die('No direct script access');

class Bus_Requirements_Status_Model extends ORM{

	public function add_status($agency_id, $bus_number, $bus_requirement_id, $is_done='0')
	{	
		$requirement_status = ORM::FACTORY('bus_requirements_status');
		$requirement_status->AgencyID = $agency_id;
		$requirement_status->BusNumber = $bus_number;
		$requirement_status->BusRequirementID = $bus_requirement_id;
		$requirement_status->IsDone = $is_done;
	
		$requirement_status->save();
	}
	
	public function update_status($agency_id, $bus_number, $bus_requirement_id, $is_done, $expiry_date)
	{	
		$requirement_status = ORM::FACTORY('bus_requirements_status')->where('BusRequirementID',$bus_requirement_id)->where('BusNumber',$bus_number)->find();
		$requirement_status->AgencyID = $agency_id;
		$requirement_status->IsDone = $is_done;
		$requirement_status->ExpiryDate = $expiry_date;
		$requirement_status->save();
	}
	
	public function get_all($agency_id)	{
		$requirement_status=ORM::factory('bus_requirements_status')->where('AgencyID',$agency_id)->find_all();
		return $requirement_status;	
	}
	
	public function get_all_for_bus($agency_id, $bus_number)	{
		$requirement_status=ORM::factory('bus_requirements_status')->where('AgencyID',$agency_id)->where('BusNumber',$bus_number)->find_all();
		return $requirement_status;	
	}
	
	public function count_all_undone_for_bus($agency_id, $bus_number)	{
		$requirement_status=ORM::factory('bus_requirements_status')->where('AgencyID',$agency_id)->where('IsDone','0')->where('BusNumber',$bus_number)->count_all();
		return $requirement_status;	
	}
	
	public function get_all_undone_for_bus($agency_id, $bus_number)	{
		$requirement_status=ORM::factory('bus_requirements_status')->where('AgencyID',$agency_id)->where('IsDone','0')->where('BusNumber',$bus_number)->find_all();
		return $requirement_status;	
	}
	
	public function get_requirement_status($agency_id, $requirement_id, $bus_number)
	{	
		$requirement_status=ORM::factory('bus_requirements_status')->where('BusNumber',$bus_number)->where('BusRequirementID',$requirement_id)->find();
		return $requirement_status;
		
	}
	
	//check whether that status has been set by counting
	public function check_existence($agency_id,$requirement_id, $bus_number)
	{
		$requirement_status_exists=ORM::factory('bus_requirement')->where('AgencyID',$agency_id)->where('id',$requirement_id)->where('BusNumber',$requirement_id)->count_all();
		return $requirement_status_exists;
	}
	
	//get expired documents/requirements. Check that IsDone is 1 i.e. the document is available
	public function get_all_expired_requirements($agency_id)
	{	
		$today = date('Y-m-d');
		//$db = new Database();
		//$expired_requirements = $db->query("SELECT * FROM bus_requirements_statuses WHERE AgencyID= ".$agency_id." AND IsDone ='1' AND ExpiryDate <= CURDATE() ORDER BY ExpiryDate ASC");
		$expired_requirements =ORM::factory('bus_requirements_status')->where('AgencyID',$agency_id)->where('IsDone','1')->where('ExpiryDate <=',$today)->find_all();
		return $expired_requirements;
	}
	
	public function count_all_expired_requirements($agency_id)
	{	
		$today = date('Y-m-d');
		$number_of_expired_requirements=ORM::factory('bus_requirements_status')->where('AgencyID',$agency_id)->where('IsDone','1')->where('ExpiryDate <=',$today)->count_all();
		return $number_of_expired_requirements;
	}
	
	//takes a date and returns the number of days left if it's less than one week or has expired.
	//if expired more than one week ago, just return -1
	public function get_number_of_days_left($date){
		//make sure the date is set if not, return null
		if($date){
			$time_left = time() - strtotime($date);
			//If it's less than one week left
			if($time_left < 604800){
				$days_left = ceil(-1*($time_left/86400));
				
				//if($days_left < 0){ EXPIRED }
				//elseif($days_left >= 1  AND $days_left <= 14){ //Expires in $days_left days}
				
				//send as an integer. default is float
				return (int)$days_left;
				
			}else{ //Expired more than 1 week ago 
				return -1;	
			}
		}else{
			return NULL;
		}
	}
	
	//In one week
	public function count_agency_expiring_requirements($agency_id)
	{	
		$all_requirements = self::get_all($agency_id);
		$count=0;
		foreach($all_requirements as $requirement){
			$days_left = self::get_number_of_days_left($requirement->ExpiryDate);
			if($days_left >= 1 and $days_left <= 7){
				$count++;
			}
		}
		return $count;
	}
	
	//In one week
	public function count_bus_expiring_requirements($agency_id,$bus_number)
	{	
		$all_requirements = self::get_all_for_bus($agency_id,$bus_number);
		$count=0;
		foreach($all_requirements as $requirement){
			$days_left = self::get_number_of_days_left($requirement->ExpiryDate);
			if($days_left >= 1 and $days_left <= 7){
				$count++;
			}
		}
		return $count;
	}
	public function count_bus_expired_requirements($agency_id,$bus_number)
	{	
		$all_requirements = self::get_all_for_bus($agency_id,$bus_number);
		$count=0;
		foreach($all_requirements as $requirement){
			$days_left = self::get_number_of_days_left($requirement->ExpiryDate);
			
			//null in DB will return null, which is less than zero. So ensure null values are not counted
			if( $days_left <=0 AND !empty($days_left)){
				//var_dump($days_left);
				$count++;
			}
		}
		return $count;
	}

	
	public function delete_requirement($agency_id,$requirement_id)
	{
		
		$requirement=ORM::factory('bus_requirement')->where('id',$requirement_id)->find();
		$requirement->delete();
		$requirement->save();
		
		$notice="Requirment deleted";
		return $notice;
		
	}
	

}
