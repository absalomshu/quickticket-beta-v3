<?php defined('SYSPATH') or die('No direct script access'); ?> 

	<div class="notice">
		<?php
			//What the user entered in his previous submission attempt, in case it was a failure
			$post = $this->session->get_once('post');
			//determine what type of notice to display if at all
			$notice = $this->session->get_once('notice');
				if(!empty($notice)){ 
					if($notice['type'] == 'success'){?><div class="alert general success"><a class="close" data-dismiss="alert" href="#">&times;</a><?php echo $notice['message']; ?></div><?php }?>
				<?	if($notice['type'] == 'error'){?><div class="alert general error"><a class="close" data-dismiss="alert" href="#">&times;</a><?php echo $notice['message'];?></div><?php }?>
		<?}?>
	</div>
	
		
	  
	 
	<div class="row-fluid">
	  
		<form action="<?=url::site('admin/create_schedule')?>" method="POST">
		 
     
	  	<div class="span12 hero-unit prof-unit" >
		<!--no csrf-->
		<?php no_csrf::set(1)?>
		<div class="heading"><?=Kohana::lang('backend.create_schedule')?><span style="float:right;font-size:12px;font-weight:normal;"></span></div>
			<div class="rule"><hr/></div>
			<div class="span2 spacious">
				<div class="control-group">
				<label class="control-label lab" for="inputType"><?=Kohana::lang('backend.from')?>:</label>
					
					<!--<div class="controls ">
						<select name="from" id="inputType" disabled>
							<?php //foreach ($towns as $town):?>
							<option value="<?php //$town->id?>" disabled><?php //$town->name?></option>
							<?php //endforeach;?>
						</select>
					
					</div>-->
					<div class="controls ">
						<select name="from" id="inputType" disabled>
						 <?php $admins_town = get::admins_town($this->admin->agency_id);?>
							<option value="<?=$admins_town->id?>" disabled><?=$admins_town->name?></option>
							
						</select>
					</div>
					
				</div>
			</div>
			<div class="span2 spacious">
				<div class="control-group">
				<label class="control-label lab" for="inputType"><?=Kohana::lang('backend.to')?>:</label>
					
					<div class="controls ">
						<select name="to" id="inputType">
							<?php foreach ($towns as $town):?>
							<option value="<?=$town->id?>"><?=$town->name?></option>
							<?php endforeach;?>
						
					</select>
					</div>
				</div>
			</div>
			<!--<div class="span2 spacious">
				<div class="control-group">
				<label class="control-label lab" for="inputType">Time:</label>
					<div class="controls ">
						<select name="departure_time" id="inputType">

							<option value="Day(8:00 AM)">Morning (8:00 AM)</option>
							<option value="Afternoon(1:00PM)">Afternoon (1:00 PM)</option>
							<option value="Night(8:00 PM)">Night (8:00 PM)</option>

						</select>
					</div>
				</div>
			</div>-->
			
			

			<div class="span2 spacious">
				<div class="control-group">
				<label class="control-label lab" for="inputType"><?=Kohana::lang('backend.time')?>:</label>
					<div class="controls ">
					
					<div class="input-append bootstrap-timepicker">
						<input name="departure_time" id="timepicker" type="text" class="span10"  value="<?=date("g:i A", strtotime("8:00 PM"));?>" />
						<span class="add-on"><i class="icon-time"></i></span>
					</div>
					</div>
				</div>
			</div>	
		
			
			<div class="span2 spacious">
				<div class="control-group">
				<label class="control-label lab" for="inputType">Date:</label>
					<div class="controls ">
					
					<div class="input-append date">
						<input type="text" name = 'departure_date' class="span7 datepicker"  value="<?=date('d-m-Y')?>" id="dp1">
						<span class="add-on"><i class="icon-th"></i></span>
					</div>
					</div>
				</div>
			</div>
			
			<div class="clear"></div>
			
			<div class="span2 spacious dropdown"  id="existing-bus-number">
				<div class="control-group">
				<label class="control-label lab" for="inputType"><?=Kohana::lang('backend.existing_buses')?>:</label>
					<div class="controls ">
						<select name="bus_number_and_seats" id="inputType">
					
						<ul class="dropdown-menu" role="menu" aria-labelledby="dropdownMenu">
							<?php foreach ($parent_buses as $bus):?>
							
							
							<li>
								 <a tabindex="-1" href="#">
									<option value="<?=$bus->bus_number?>;<?=$bus->bus_seats?>"><b><?=$bus->bus_number?></b> - <?=$bus->bus_seats?> <?=Kohana::lang('backend.seater')?> </option>
								</a>
							</li>
						<?php endforeach;?>
						</ul>
						<!--putting a hidden input for each buses' seats was tricky as it has to be done within the loop which is within the 
							select, hence the value passed is the bus number AND the bus seats, separated by a semicolon. Handle in PHP-->
							
							
							

						</select>
						
					</div>
				</div>
			</div>
			
			
			<div class="span2 spacious" style="display:none" id="bus-number">
				<div class="control-group">
				<label class="control-label lab" for="inputType"><?=Kohana::lang('backend.bus_no')?>:</label>
					<div class="controls ">
						<input type="text" placeholder="e.g. NW 999 AB"  name="unregistered_bus_number" class="span12">
					</div>
				</div>
			</div>
			
			<div class="span2 spacious" style="display:none" id="bus-type" >
				<div class="control-group">
				<label class="control-label lab" for="inputType"><?=Kohana::lang('backend.bus_type')?>:</label>
					<div class="controls ">
						<select name="unregistered_bus_seats" id="inputType">

							<option value="30">30 <?=Kohana::lang('backend.seater')?></option>
							<option value="35">35 <?=Kohana::lang('backend.seater')?></option>
							<option value="39">39 <?=Kohana::lang('backend.seater')?></option>
							<option value="55">55 <?=Kohana::lang('backend.seater')?></option>
							<option value="70">70 <?=Kohana::lang('backend.seater')?></option>

						</select>
					</div>
				</div>
			</div>
			
			
			<div class="span2 spacious">
				<div class="control-group">
				<label class="control-label lab" for="inputType"><?=Kohana::lang('backend.ticket_price')?> :</label>
					<div class="controls input-append">
						<input type="text" placeholder="e.g. 4500"  name="ticket_price" value="<?=@$post['ticket_price']?>" class="span9">
						<span class="add-on"> FCFA</span>
					</div>
				</div>
			</div>
			<div class="span2 ">	
			<div class="control-group">
				<label class="control-label lab" for="inputType"></label>
					<div class="controls large">
									<button class="btn create-btn btn-large btn-success" type="submit"><i class="icon-plus icon-white"></i> <?=Kohana::lang('backend.create')?></button><br/>

					</div>
				</div>
				
			</div>
			
			<div class="clear" style="height:20px;font-size:13px;margin: -15px 0 0 20px;"><i class="icon-bus"></i> <?=Kohana::lang('backend.unreg_bus')?>? <input type="checkbox" id="unregistered_bus" name="unregistered_bus_check"/><br/></div>
			  
		</div>
		</form></div>
	<div class="row-fluid " style="margin:5px 0 0px 0;" id="current-scheds">	
	
		<div class="span7 hero-unit prof-unit reload" >
			<div class="heading"><?=Kohana::lang('backend.current_scheds_from')." ".get::admins_town($this->admin->agency_id)->name?><a href="<?=url::site('admin/all_schedules/current')?>" class="view"><?=Kohana::lang('backend.see_all')?></a></div>
			<div class="rule"><hr/></div>
				<table class="table table-striped table-hover">
				
					<?php foreach ($current_schedules as $c_schedule):?>
					<tr><td><i class="icon-time "></i> <a href="<?=url::site('admin/complete_schedule').'/'.$c_schedule->id?>"><?=$c_schedule->bus_number?></a> : <?=Kohana::lang('backend.to')?>  <b><?=get::town($c_schedule->to)?></b> : <b><?=date("g:i A", strtotime($c_schedule->departure_time))?></b><span style="float:right;font-size:12px;font-weight:normal;"><a href="<?=url::site('admin/edit_schedule').'/'.$c_schedule->id?>"><?=Kohana::lang('backend.edit')?> </a>/<a href="#" onClick="checkout(<?=$c_schedule->id?>)"><?=Kohana::lang('backend.checkout')?> </a><!--<a href="<?php //url::site('admin/checkout_schedule/'.$c_schedule->id)?>" onClick="checkout(<?php //$c_schedule->id?>)">Check out</a>--></span></td></tr>
					<!--<input type="hidden"  class="schedule_id" value="<?=$c_schedule->id?>"></input>-->
					<?php endforeach; ?>
				
				</table>
				
			
</div>
	
		<div class="span5 hero-unit prof-unit" style="float:right;">
				<div class="heading"><?=Kohana::lang('backend.recently_checkedout')?></div>
				<div class="rule"><hr/></div>
				
					<table class="table ">
						<?php foreach ($departed_schedules as $d_schedule):?>
						<tr><td class="recent"><i class="icon-ok-sign "></i> <a href="<?=url::site('admin/past_schedule').'/'.$d_schedule->id?>"><?=$d_schedule->bus_number?></a> : <?=get::town($d_schedule->from)?> -> <b><?=get::town($d_schedule->to)?></b> at <b><?=date("g:i A", strtotime($d_schedule->checked_out_time))?></b><span style="float:right;font-size:12px;font-weight:normal;"><a href="<?=url::site('admin/past_schedule').'/'.$d_schedule->id?>">View</a></span></td></tr>
						<?php endforeach; ?>
					</table>				
				
				<!--<br/><a href="#" class="small-link">See all checked out...</a>-->
				
		</div>
		
			</div>
	<!--<div class="row-fluid marketing" style="margin:5px 0 0px 0;" id="parcels">	
		<div class="span7 hero-unit prof-unit" style="margin-left:0px;" >
			<div class="heading"><?=Kohana::lang('backend.incoming_parcels')?><a href="<?=url::site('parcels/all/incoming/'.$agency_id)?>" class="view"><?=Kohana::lang('backend.see_parcels')?></a></div>
				<div class="rule"><hr/></div>
				
					<ul>
						<?php foreach ($parcels as $parcel):?>
						<li class="recent"><?=Kohana::lang('backend.for')?><a href=""> <?=$parcel->receiver_name?></a>, <?=Kohana::lang('backend.sent_on')?> <b><?=date("d-m-Y",strtotime($parcel->sent_date))?></b> </li>
						<?php endforeach; ?>
					</ul>	
		</div>
		<div class="span5 hero-unit prof-unit" style="float:right;">
			<form action="<?=url::site('parcels/register_parcel/'.$this->admin->agency_id)?>" method="POST">
			<?php no_csrf::set(2)?>
			<div class="heading"><?=Kohana::lang('backend.reg_parcels')?>: <?=Kohana::lang('backend.rec_details')?></div>
				<div class="rule"><hr/></div>
				
				
			<div class="simple-form span12">
				<div class="sf_label"><?=Kohana::lang('backend.to')?>: </div>
				<div class="sf_text">
						<select name="agency_to" id="inputType" class="span10" >
							<?php foreach ($siblings as $sib):?>
							<option value="<?=$sib->id?>"><?=$sib->name?></option>
							<?php endforeach;?>
						
					</select>
				</div>
			</div>
			
				<div class="simple-form span12">
								<div class="sf_label"><?=Kohana::lang('backend.price')?>: </div>
								<div class="sf_text"><input type="text" placeholder="Price"  name="price"></div>
				</div>
				<div class="simple-form span12">
								<div class="sf_label"><?=Kohana::lang('backend.name')?>: </div>
								<div class="sf_text"><input type="text" placeholder="Receiver's name"  name="receiver_name"></div>
				</div>
				
				<div class="simple-form span12">
							<div class="sf_label"><?=Kohana::lang('backend.phone')?>: </div>
							<div class="sf_text"><input type="text" placeholder="Receiver's phone number"  name="receiver_phone"></div>
				</div>
				<div class="simple-form span12">
								<div class="sf_label">Description: </div>
								<div class="sf_text"><textarea placeholder="Describe the parcel"  name="parcel_description" ></textarea></div>
				</div>
				<div class="simple-form span12">
								<div class="sf_label">&nbsp;</div>
								<div class="sf_text"><button type="submit" class="btn btn-info"><?=Kohana::lang('backend.register')?></button></div>
				</div>
				
		</form>	
								
				
			
				
		</div>
		</div>-->
	  </div>
	  <div style="height:19px;"></div>
    </div> 
	
	<?php //$this->profiler = new Profiler();?>
	
<script type="text/javascript">
	$(document).ready(function(){
	
	$('#timepicker').timepicker({
		minuteStep:30
	});
		
		$('.input-append.date').datepicker({
			format: "dd-mm-yyyy",
			todayBtn: "linked",
			autoclose: true,
			todayHighlight: true
		});
		
		
	})
	
	
	$('#unregistered_bus').change(function(){
				if(this.checked){
					$('#existing-bus-number').hide(150);					
					$('#bus-number').show(150);					
					$('#bus-type').show(150);					
					}else{
					$('#existing-bus-number').show(150);	
					$('#bus-number').hide(150);					
					$('#bus-type').hide(150);
					}
			
			});
	
	
	
	
	
	
	function checkout(schedule_id)
	{	
		var domain = '<?=url::base()?>';
		var answer = confirm("<?=Kohana::lang('backend.confirm_checkout')?>");
		if (answer == true)
		{
			window.location.href=domain+'admin/checkout_schedule/'+schedule_id;
		}
		else{
			return;}
		
	};
</script>
