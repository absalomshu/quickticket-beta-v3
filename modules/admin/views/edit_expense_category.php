<?php defined('SYSPATH') or die('No direct script access'); ?> 
	
 
	<div class="row-fluid " >
	  
						
	  	<div class="span12 hero-unit prof-unit" >
				<ul class="nav nav-tabs">
					<li > <a href="<?=url::site('buses/all')?>"><i class="icon-arrow-left"></i> <?=Kohana::lang('backend.back_buses')?></a></li>
				</ul>
		
				<form action="<?=url::site('settings/edit_expense_category/'.$expense_category->id);?>" method="POST" class="span8 offset2">
				<legend><?=Kohana::lang('backend.edit_expense_category')?> </legend>
					
						
						<div class="">
							<div class="span4 text-right"><?=Kohana::lang('backend.category_name')?><span class="red"> *</span>: </div>
							<div class="span8"><input type="text"  name="category_name" placeholder="e.g. fuel, miscelleanous, road money" value="<?=$expense_category->name?>"  /></div>
						</div>
						
						
												
						<div class="">
							<div class="span4 text-right"><?=Kohana::lang('backend.description')?>: </div>
							<div class="span8"><textarea placeholder=""  name="description" style="height:100px;"><?=$expense_category->description?></textarea></div>
						</div>
						
						
							
					
						<div class="clear"></div>
						<div class="clear"><br/><br/></div>
						
						<div class="">
							
							<div class="form-actions">
								<div class="span4"> </div>
								<div class="span8"> 
									<button class="btn btn-info" type="submit"> <?=Kohana::lang('backend.save')?></button>
									<a href="<?=url::site('settings/expenses')?>" class="btn" type="submit"> <?=Kohana::lang('backend.cancel')?></a>
									<a href="<?=url::site('settings/delete_expense_category/'.$expense_category->id)?>" onClick="return confirm('<?=Kohana::lang('backend.confirm_delete')?>?');" >Delete</a>
								</div>
							</div>
						</div>
						
									
				
						
				</form>
			 
			
				
			  					
			
		
			  
		</div>
		
		</div>
	  <div class="clear"></div>

<script type="text/javascript">
	$(document).ready(function(){
			$('#timepicker').timepicker({
				minuteStep:30,
				//defaultTime: '8:00 PM'
			});
			
			
			$('.input-append.date').datepicker({
				format: "dd-mm-yyyy",
				todayBtn: "linked",
				autoclose: true,
				todayHighlight: true
			});
			
			
				$(function () {
				$('#myTab a').click(function (e) {
					  e.preventDefault();
					  $(this).tab('show');  
			})
			  })
			  
		
			  
			
	})
	</script>
