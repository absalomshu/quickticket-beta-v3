<?php defined('SYSPATH') or die('No direct script access'); ?> 
<!DOCTYPE html>


		
		<div class="notice">
			<?php
				//determine what type of notice to display if at all
				$notice = $this->session->get_once('notice');
					if(!empty($notice)){ 
						if($notice['type'] == 'success'){?><div class="alert general success"><a class="close" data-dismiss="alert" href="#">&times;</a><?php echo $notice['message']; ?></div><?php }?>
					<?	if($notice['type'] == 'error'){?><div class="alert general error"><a class="close" data-dismiss="alert" href="#">&times;</a><?php echo $notice['message'];?></div><?php }?>
			<?}?>
		</div>
			
	  
	  <div class="row-fluid"> 	
	  
	  <div class="span9 hero-unit prof-unit offset1"  >
	  
		<form action="<?=url::site('settings/view_fixed_expense')?>" method="POST" >		
					   <div class="span8 offset1">
					
							<table class="table">
								<tr>
									<th><?=Kohana::lang('backend.schedules')?></th>						
								</tr>
								
								 <tr>
									 <td>
									 <input type="radio" name="fixed_expense"></input> <input type="text" class="span2"></input> % of total income from each schedule
									 </td>
								 </tr>
								 <tr>
									<td>
									<input type="radio" name="fixed_expense"> <input type="text" class="span2"> FCFA from each schedule
									</td>
								</tr> 
								 
							</table>
							<div class="form-actions">
											<button type="button" class="btn btn-info" id="edit"><?=Kohana::lang('backend.edit')?></button>					
											<button class="btn btn-success" type="submit" name="company_info" id="save"><?=Kohana::lang('backend.save')?></button>					
											<button type="button" class="btn"  id="cancel"><?=Kohana::lang('backend.cancel')?></button>					
											
											<a href="<?=url::site('settings/add_fixed_expense')?>" class="btn"  id="cancel"><?=Kohana::lang('backend.cancel')?></a>					
							</div>
						</div>
						</form>
					
			

		</div>


		<script>
			//Disable the inputs and save. When he clicks edit, enable.
			$("#advanced-tab input").prop("disabled", true);
			$("#advanced-tab #save").prop("disabled", true);
			$("#advanced-tab  #cancel").prop("disabled", true);
			
			$('#advanced-tab #edit').click(function(){ 
				$("#advanced-tab #save").prop("disabled", false);
				$("#advanced-tab input").prop("disabled", false);
				$("#advanced-tab #edit").prop("disabled", true);
				$("#advanced-tab  #cancel").prop("disabled", false);
			})
			
			$('#cancel').click(function(){ 
				$("#advanced-tab #edit").prop("disabled", false);
				$("#advanced-tab #save").prop("disabled", true);
				$("#advanced-tab input").prop("disabled", true);
				$("#advanced-tab #cancel").prop("disabled", true);
			})
			
		</script>
			  