<?php defined('SYSPATH') or die('No direct script access'); ?> 


	<?php
		//determine what type of notice to display if at all
		$notice = $this->session->get_once('notice');
			if(!empty($notice)){ 
				if($notice['type'] == 'success'){?><div class="alert general success"><a class="close" data-dismiss="alert" href="#">&times;</a><?php echo $notice['message']; }?></div>
			<?	if($notice['type'] == 'error'){?><div class="alert general error"><a class="close" data-dismiss="alert" href="#">&times;</a><?php echo $notice['message']; }?></div>
		<?}?>
    <div class="container">
		<!--<div class="row-fluid marketing mid_content" >
		<div class="span1"></div>
			<div class="span11 " >
				<span class="v-line"><img src="<?=url::base()?>images/psd/line.png"></span>
			</div>
			
		</div>-->
	  <div class="row-fluid marketing" style="margin:5px 0 0px 0px;">
	  
		
	<div class="row-fluid marketing" style="margin:5px 0 0px 0;">	
		<div class="span5 hero-unit prof-unit" style="float:right;">
				<div class="heading">Recently checked out<a href="<?=url::site('admin/all_schedules/departed')?>" class="view">See all </a></div>
				<div class="rule"><hr/></div>
				
					<ul>
						<?php foreach ($departed_schedules as $d_schedule):?>
						<li class="recent"><a href="<?=url::site('admin/past_schedule').'/'.$d_schedule->id?>"><?=$d_schedule->bus_number?></a> : <b><?=get::town($d_schedule->from)?></b> -> <b><?=get::town($d_schedule->to)?></b> at <b><?=$d_schedule->checked_out_time?></b><span style="float:right;font-size:12px;font-weight:normal;"><a href="<?=url::site('admin/past_schedule').'/'.$d_schedule->id?>">View</a></span></li>
						<?php endforeach; ?>
					</ul>					
				
				
				
		</div>
		<div class="span7 hero-unit prof-unit reload" style="margin-left:0px;">
			<div class="heading"><?=get::_parent($this->admin->agency_id)->name." ".get::admins_town($this->admin->agency_id)->name?>'s current schedules <a href="<?=url::site('admin/all_schedules/current')?>" class="view">See all </a></div>
			<div class="rule"><hr/></div>
				
				<ul>
					<?php foreach ($current_schedules as $c_schedule):?>
					<!--<li><a href="<?=url::site('admin/complete_schedule').'/'.$c_schedule->id?>"><?=$c_schedule->bus_number?></a> from <b><?=get::town($c_schedule->from)?></b> to <b><?=get::town($c_schedule->to)?></b> at <b>8:00PM</b><i> <span style="font-size:11px;">  (Loading now...) </span> </i><span style="float:right;font-size:12px;font-weight:normal;"><a href="<?=url::site('admin/complete_schedule').'/'.$c_schedule->id?>">View</a></span></li>-->
					<li><?=$c_schedule->bus_number?> : <b><?=get::town($c_schedule->from)?></b> -> <b><?=get::town($c_schedule->to)?></b> : <b><?=$c_schedule->departure_time?></b><i> <span style="font-size:11px;">  (Loading now...) </span> </i><span style="float:right;font-size:12px;font-weight:normal;"></span></li>
					<!--<input type="hidden"  class="schedule_id" value="<?=$c_schedule->id?>"></input>-->
					<?php endforeach; ?>
				</ul>
				
			

		</div>
	
		<div class="span7 hero-unit prof-unit" style="margin-left:0px;" >
		<form action="<?=url::site('admin/schedule_by_date/')?>" method="POST">
				<div class="heading">Branches' daily activity by date<span style="float:right;font-size:12px;font-weight:normal;"><a href="#">See all activity</a></span></div>
				<div class="rule"><hr/></div>
				
				
					
					
					<input type="text" name = 'date' class="span2 datepicker"  value="<?=date('d-m-Y')?>" id="dp1" style="width:120px;height:40px;font-size:20px;">
									<button class="btn  btn-info" type="submit"  style="height:40px; width:140px;font-size:20px;" ><i class="icon-search icon-white"></i> Check</button><br/>
		</form>		
		</div>
		</div>
	  </div>
	  <div style="height:19px;"></div>
    </div> 
<script type="text/javascript">
	$(document).ready(function(){
		$('.datepicker').datepicker();
	})
	function checkout(schedule_id)
	{	
		var domain = 'http://localhost/quickticket/';
		var answer = confirm('Are you sure you want to check this bus out?');
		if (answer == true)
		{
			//for now, reload the page. eventually, reload just the two divs involved
			//NB even though reload comes before action, action is executed b4 page finishes reloading
			location.reload();
			
			
			
			$.ajax({
			type: 'POST',
			url: domain+'admin/checkout_schedule/'+schedule_id,
			data: 'schedule_id='+schedule_id,
			cache: false,
			sucess: function()
				{
				//$("#return").html(' '+html+'');
				
				}
			});
		//window.location.reload();	
		}
		else{
			return false;}
		
	};
	$(document).ready(function(){
	
	});




</script>
