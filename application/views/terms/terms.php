<?php defined('SYSPATH') or die('No direct script access'); ?> 


    <div class="container">
		
	  
	  
		
	  	
	<div class="row-fluid marketing" style="margin:5px 0 0px 0;">	
	
		<div class="span8 hero-unit prof-unit" >
			<div class="heading">Terms<span style="float:right;font-size:12px;font-weight:normal;"></span></div>

Please read the following terms and conditions carefully before using this QuickTicket Software. Your use, distribution or installation of this copy of QuickTicket indicates your acceptance of this License.

QuickTicket Software here means Software, image files, all accompanying files, data and materials received with your order of QuickTicket.

If you do not agree to any of the terms of this License, then do not install, distribute or use the QuickTicket Software. If you have purchased a single copy from Viasso Ltd. or an authorized distributor, reseller or any retail channel, you may return it unused, within thirty (30) days after purchase, for a refund of your payment less any incidental charges. The 30-day warrantee is applicable only to products bought within Cameroon. Products downloaded to or shipped out of Cameroon are strictly non-refundable. 

Warrantee covers defects in the software, which prevents successfully installing the software in the buyer's PC. Warrantee does not cover fitness of purpose, not meeting of expectations or needs in the mind of the buyer. 

This QuickTicket Software is for personal use only and may be installed and used by on only one computer. Its component parts may not be separated for use on more than one computer. QuickTicket Software may be accessed through a network only after obtaining a site license. All components accompanying the software are copyrighted by Viasso Ltd. and may not be taken apart, modified, used or published with other software or means except with the QuickTicket Software and may not be 
distributed or copied in any manner.

This QuickTicket Software, all accompanying files, data and materials, are distributed "AS IS" and with no warranties of any kind, whether express or implied. The user must assume all risk of using the program. This disclaimer of warranty constitutes an essential part of the agreement. 

Any liability of Viasso Ltd. will be limited exclusively to refund of purchase price. In addition, in no event shall Viasso Ltd., or its principals, shareholders, officers, employees, affiliates, contractors, subsidiaries, or parent organizations, be liable for any incidental, consequential, punitive or any other damages whatsoever relating to the use of QuickTicket Software.

In addition, in no event does Viasso Ltd. authorize you to use this QuickTicket Software in applications or systems where QuickTicket Software's failure to perform can reasonably be expected to result in a physical injury, or in loss of life. Any such use by you is entirely at your own risk, and you agree to hold Viasso Ltd. harmless from any claims or losses relating to such unauthorized use.

This Agreement constitutes the entire statement of the Agreement between the parties on the subject matter, and merges and supersedes all other or prior understandings, purchase orders, agreements and arrangements. This Agreement shall be governed by the laws of Cameroon. 

Viasso Ltd. the owner of the copyright of this QuickTicket Software, all of its derivatives, titles and accompanying materials are the exclusive property of Viasso Ltd. All rights of any kind, which are not expressly granted in this License, are entirely and exclusively reserved to and by Viasso Ltd. You may not rent, lease, transfer, modify, translate, reverse engineer, de-compile, disassemble or create derivative works based on this QuickTicket Software. You may not make access to QuickTicket Software available to others in connection with a service bureau, application service provider, or similar business, or use this QuickTicket Software in a business to provide file compression, decompression, or conversion services to others. There are no third party beneficiaries of any promises, obligations or representations made by Viasso Ltd. herein.

You may not disclose to other persons the data or techniques relating to this QuickTicket Software, that you know or should know that it is a trade secret of Viasso Ltd. in any manner that will cause damage to Viasso Ltd.

This QuickTicket Software and all services provided may be used for lawful purposes only. Transmission, storage, or presentation of any information, data or material in violation of any law is strictly prohibited. This includes, but is not limited to: copyrighted material, material we judge to be threatening or obscene, or material protected by trade secret and other statute. You agree to indemnify 
and hold Viasso Ltd. harmless from any claims resulting from the use of this QuickTicket Software, which may damage any other party.





				</div>
				

		
	
		</div>
	  <div style="height:19px;"></div>
    </div> 

