<?php defined('SYSPATH') or die('No direct script access'); ?> 


    <div class="container">		
	  <div class="notice">
	  <?php
		//determine what type of notice to display if at all
		$notice = $this->session->get_once('notice');
			if(!empty($notice)){ 
				if($notice['type'] == 'success'){?><div class="alert general success"><a class="close" data-dismiss="alert" href="#">&times;</a><?php echo $notice['message']; }?></div>
			<?	if($notice['type'] == 'error'){?><div class="alert general error"><a class="close" data-dismiss="alert" href="#">&times;</a><?php echo $notice['message']; }?></div>
		<?}?>
	  </div>
	<div class="row-fluid marketing" style="margin:5px 0 0px 0;">	
	<br/><br/><br/><br/><br/>
		<div class="span8 offset2" >
			<div class="span12 hero-unit prof-unit"  style="margin:5px 0 0px 0;">			
				<legend> Enter and confirm your new password </legend>
				<form method="POST" action=<?=url::site('home/reset_password')?>>
					<?php 
					//get user's email from the uri
					$email=qt_email::decrypt_string($this->uri->segment(3));?>
					<input type="password" placeholder="New password" class="span4" name="password1" required>
					<input type="password" placeholder="Confirm password" class="span4" name="password2" required>
					<input type="hidden" placeholder="Confirm password" value="<?=$email?>" name="email" required>

					<button type="submit" class="btn btn-success">Reset password</button>
				
				
				</form>
			</div>
		</div>
		

		
	
		
    </div> 
