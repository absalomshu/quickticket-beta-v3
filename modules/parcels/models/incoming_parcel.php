<?php defined('SYSPATH') or die('No direct script access');

class Incoming_parcel_Model extends ORM{

	public function get_incoming_parcels_by_date($agency_id,$date)
	{
		$parcels = ORM::FACTORY('incoming_parcel')->where('to',$agency_id)->where('sent_date',$date)->find_all();	
		return $parcels;
		
	}
	public function search_client_by_name($agency_id, $client_name)
	{
		$db = new Database();
		//get only parcels coming to this particular agency
		//Using to and not `to` will give a bug
		$tickets = $db->query("SELECT * FROM incoming_parcels WHERE `to` = ".$agency_id." AND receiver_name LIKE '%".$client_name."%' ORDER BY sent_date DESC");
		return $tickets;
	}
	
	public function count_total_incoming_today($agency_id)
	{	
		$today = date('Y-m-d');
		$count=ORM::factory('incoming_parcel')->where('agency_id',$agency_id)->where('state','registered')->where('sent_date',$today)->count_all();
		
		return $count;	
	}
	

}
