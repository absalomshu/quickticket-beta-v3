<?php defined('SYSPATH') or die('No direct script access'); ?> 


	  <div class="row-fluid " >
	  	<div class="span12 hero-unit prof-unit" >
		
		<ul class="nav nav-tabs">
					<li <?php if ($this->uri->segment(2)=='general'){?> class="active"<?php } ?>><a href="<?=url::site('settings/general')?>"><i class="iscon-plus"></i> <?=Kohana::lang('backend.general')?></a></li>
					<li <?php if ($this->uri->segment(2)=='administrators'){?> class="active"<?php } ?>><a href="<?=url::site('settings/administrators')?>"><i class="icson-list"></i> <?=Kohana::lang('backend.admins')?></a></li>
					<li <?php if ($this->uri->segment(2)=='company'){?> class="active"<?php } ?>><a href="<?=url::site('settings/company')?>"><i class="icosn-share-alt"></i> <?=Kohana::lang('backend.company')?></a></li>
					<li <?php if ($this->uri->segment(2)=='expenses'){?> class="active"<?php } ?>><a href="<?=url::site('settings/expenses')?>"><i class="iscon-search"></i> <?=Kohana::lang('backend.expenses')?></a></li>
					<li <?php if ($this->uri->segment(2)=='advanced'){?> class="active"<?php } ?>><a href="<?=url::site('settings/advanced')?>"><i class="iscon-search"></i> <?=Kohana::lang('backend.advanced')?></a></li>
					<li <?php if ($this->uri->segment(2)=='cash'){?> class="active"<?php } ?>><a href="<?=url::site('settings/cash')?>"><i class="icson-search"></i> <?=Kohana::lang('backend.cash')?></a></li>
		</ul>
			
				<?php if ($this->uri->segment(2)=='general'){ ?>
					<div class="tab-pane span8 offset2" id="general-tab">
					<legend><?=Kohana::lang('backend.general')?></legend>
						<form action="<?=url::site('settings/general')?>" method="POST" >		
					   <div class="">
								<br/>
								<!--hidden inputs here is a technique to ensure that unchecked fields pass a 0 to the POST-->
								<table class="table table-striped table-hover">
									<tr>
										<td><input type="hidden" name="auto_ticket" value="0" /></td>
										<td><label class="checkbox"><input type="checkbox" id="auto_ticket" name="auto_ticket" value="1" <?php if($settings->auto_ticket == 1){echo 'checked = "checked"';} ?>/><?=Kohana::lang('backend.auto_generate_ticket')?></label></td>
									</tr>
									<tr>
										<td><input type="hidden" name="ticket_type_1" value="0" /></td>
										<td><label class="checkbox"><input type="checkbox" id="ticket_type_1" name="ticket_type_1" value="1" <?php  if($settings->ticket_type_1 == '1'){echo 'checked = "checked"';} ?>/><?=Kohana::lang('backend.ticket_type_1')?></label></td>
									</tr>
									<tr>
										<td><input type="hidden" name="ticket_type_2" value="0" />
										<td><label class="checkbox"><input type="checkbox" id="ticket_type_2" name="ticket_type_2" value="1" <?php if($settings->ticket_type_2 == 1){echo 'checked = "checked"';} ?>/><?=Kohana::lang('backend.ticket_type_2')?></label></td>
									</tr>
									<!--<input type="submit" value="Save" name="general">-->
									<tr>
										<td></td>
										<td></td>
										
									</tr>
									
									
									
								</table>
								
								<div class="clear"></div>
									<div class="form-actions">
										<div class="span4"></div>
										<div class="span8">
									<td><button class="btn btn-info" type="submit" name="general"><?=Kohana::lang('backend.save')?></button></td>
										</div>
									</div>
						</div>
						</form>
					
						
					</div><?php } ?>
					<?php if ($this->uri->segment(2)=='administrators'){ ?>
				  <div class="tab-pane span8 offset2" id="users-tab">
				  <legend><?=Kohana::lang('backend.administrators')?></legend>
						<form action="<?=url::site('settings')?>" method="POST" >		
					   <div class="">
					
							<table class="table table-striped table-hover">
								<tr>
									<th class="span11"><?=Kohana::lang('backend.admins')?></th>						
									<th class="span1">Actions</th>						
								</tr>
								<?php foreach ($admins as $admin):?>
									<tr>	
										<td>
											<i class="icon-user"></i> <?=$admin->username?>
											<!--<i class="icon-user"></i> <a href="<?=url::site('settings/edit_admin/'.$admin->id)?>"><?=$admin->username?></a>
											<a href="<?=url::site('settings/delete_admin/'.$admin->id)?>" onClick="return confirm('<?=Kohana::lang('backend.confirm_delete_user')?>?');" class="view"><i class="icon-remove"></i></a>-->
										</td>
										
										
									<td>	
										<div class="btn-group ">
										  <a class="btn dropdown-toggle btn-mini" data-toggle="dropdown" href="#" >
											-- <?=Kohana::lang('backend.select')?> --
											<span class="caret"></span>
										  </a>
										  <ul class="dropdown-menu pull-right">
											<li><a href="<?=url::site('settings/edit_admin/'.$admin->id)?>"><i class="icon-edit"></i> Edit</a></li>
											<li><a href="<?=url::site('settings/delete_admin/'.$admin->id)?>" onClick="return confirm('<?=Kohana::lang('backend.confirm_delete_user')?>?');" ><i class="icon-remove"></i> Delete</a>
											</li>
										  </ul>
										</div>
									</td>
										
										
										
										
										
										
										
										
									</tr>
								<?php endforeach; ?>
							</table>
							<a class="btn" href="<?=url::site('settings/add_admin')?>" ><?=Kohana::lang('backend.add_admin')?></a>
						</div>
						</form>
					</div>	<?php } ?>
					
					
					<?php if ($this->uri->segment(2)=='company'){ ?>
				  <div class="tab-pane span8 offset2" id="infos-tab">
					<legend><?=Kohana::lang('backend.company')?></legend>
						<div class="span10 ">
						<form class="form-horizontal">  </form>
							
							<form class="form-horizontal" method="POST" action="<?=url::site('settings/company')?>" enctype="multipart/form-data">
							<div >
								<label class="control-label" for="inputEmail"><?=Kohana::lang('backend.name')?>:</label>
								<div class="controls">
											<input type="text" value="<?=$agency->name?>" disabled name="agency_name"/>&nbsp;&nbsp;
								</div>
							  </div>
							  
							  <div>
								<label class="control-label" >Logo:</label>
								<div class="controls">
											<img src="<?php echo url::base(). $logo;?>" class="img-polaroid">
								</div>
							  </div>
							  <div>
								<label class="control-label" >&nbsp;</label>
								<div class="controls">
											<input type="file"  name="agency_logo" />
								</div>
							  </div>
							  
							  <div>
								<label class="control-label" ><?=Kohana::lang('backend.background_image')?>:</label>
								<div class="controls">
											<img src="<?php echo url::base(). $ticket_background;?>" class="img-polaroid">
								</div>
							  </div>
							  <div>
								<label class="control-label" >&nbsp;</label>
								<div class="controls">
											<input type="file"  name="new_ticket_photo" />
								</div>
							  </div>
							  
							  
							  
							   <br/>
							   
							  <div>
								<label class="control-label" for="inputEmail">Contacts:</label>
								<div class="controls">
											<textarea type="text" name="contacts" style="width:300px;height:100px;"/><?=@$agency->contacts?></textarea>
								</div>
							  </div>
							  <br/>
							  <div >
								<label class="control-label" for="inputEmail"><?=Kohana::lang('backend.additional_infos')?></label>
								<div class="controls">
											<textarea type="text"  name="ticket_info"  style="width:300px;height:100px;" /><?=@$agency->TicketInfo?></textarea>
								</div>
							  </div>
							  
							

							<div class="clear"></div>
							<div class="form-actions">
								<div class="span8">
									<button class="btn btn-info" type="submit" name="company_info"><?=Kohana::lang('backend.save')?></button>					
							  <a href="<?=url::site('settings/general')?>" class="btn" type="submit"> <?=Kohana::lang('backend.cancel')?></a>
						
								</div>
							</div>							  
							</form>			
						</div>	
					</div><?php } ?>
					
					
					<?php if ($this->uri->segment(2)=='expenses'){ ?>
					<div class="tab-pane span8 offset2" id="expenses-tab">
					<legend><?=Kohana::lang('backend.expense_categories')?></legend>
						
						<form class="form-horizontal">  </form>
							
							<form class="form-horizontal" method="POST" action="<?=url::site('settings')?>">
								 <div class="">
					
									<table class="table table-striped table-hover">
										<tr>
											<th class="span11"><?=Kohana::lang('backend.expense_categories')?></th>						
											<th class="span1">Actions</th>						
										</tr>
										<?php foreach ($expense_categories as $category):?>
											<tr>	
												<td>
													<?=$category->name?>
													<!--<a href="<?=url::site('settings/edit_expense_category/'.$category->id)?>"><?=$category->name?></a>
													<a href="<?=url::site('settings/delete_expense_category/'.$category->id)?>" onClick="return confirm('<?=Kohana::lang('backend.confirm_delete')?>?');" class="view"><i class="icon-remove"></i></a>
													-->
												</td>
											
											<td>	
												<div class="btn-group ">
												  <a class="btn dropdown-toggle btn-mini" data-toggle="dropdown" href="#" >
													-- <?=Kohana::lang('backend.select')?> --
													<span class="caret"></span>
												  </a>
												  <ul class="dropdown-menu pull-right">
													<li><a href="<?=url::site('settings/edit_expense_category/'.$category->id)?>"><i class="icon-edit"></i> Edit</a></li>
													<li><a href="<?=url::site('settings/delete_expense_category/'.$category->id)?>" onClick="return confirm('<?=Kohana::lang('backend.confirm_delete')?>?');"><i class="icon-remove"></i> Delete </a>
													</li>
												  </ul>
												</div>
											</td>
											
											</tr>
										<?php endforeach; ?>
									</table>
									<a class="btn" href="<?=url::site('settings/add_expense_category')?>" ><?=Kohana::lang('backend.add')?></a>
								</div>						
							</form>			
					</div>
					<?php } ?>
					
					
					<?php if ($this->uri->segment(2)=='advanced'){ ?>
					<div class="tab-pane span8 offset2" id="advanced-tab">
						 <legend><?=Kohana::lang('backend.advanced')?></legend>
						<form action="<?=url::site('settings/view_fixed_expense')?>" method="POST" >		
					   <div class="span5 ">
					
							<table class="table table-striped table-hover">
								<tr>
									<th><?=Kohana::lang('backend.fixed_expenses')?></th>						
								</tr>
								<?php foreach ($fixed_expenses as $expense):?>
									<tr>	
										<td><i class="icon-wrench"></i> <a href="<?=url::site('settings/view_fixed_expense/'.$expense->id)?>"><?=$expense->Name?></a><a href="<?=url::site('settings/delete_fixed_expense/'.$expense->id)?>" onClick="return confirm('<?=Kohana::lang('backend.confirm_delete_fixed_expense')?>?');" class="view"><i class="icon-remove"></i></a></td>
									</tr>
								<?php endforeach; ?>
							</table>
							<a class="btn btn-info" href="<?=url::site('settings/add_fixed_expense')?>" ><?=Kohana::lang('backend.add')?></a>
						</div>
						</form>
						
					<form action="<?=url::site('settings/add_bus_requirement')?>" method="POST" >		
					   <div class="span5 offset1">
							
							<table class="table table-striped table-hover">
								<tr>
									<th><?=Kohana::lang('backend.bus_requirements')?></th>						
								</tr>
								<?php foreach ($bus_requirements as $requirement):?>
									<tr>	
										<td><i class="icon-file"></i> <?=$requirement->Name?><a href="<?=url::site('settings/delete_bus_requirement/'.$requirement->id)?>" onClick="return confirm('<?=Kohana::lang('backend.confirm_delete')?>?');" class="view"><i class="icon-remove"></i></a></td>
									</tr>
								<?php endforeach; ?>
							</table>
							
							<div class="rule"><hr/></div>
							<input class="span7" type="text" name="req_name" placeholder="<?=Kohana::lang('backend.name')?>" required>
							<button type="submit" class="btn btn-info" title="Quick-add bus requirement"><?=Kohana::lang('backend.add')?></button>
							
							<!--<a class="btn" href="<?=url::site('settings/add_bus_requirement')?>" ><?=Kohana::lang('backend.add')?></a>-->
						</div>
						</form>
					
						
						
					</div><?php } ?>
					
					
					<?php if ($this->uri->segment(2)=='cash'){ ?>
					<div class="tab-pane span8 offset2" id="cash-tab">
						 <legend><?=Kohana::lang('backend.cash')?></legend>
						<form action="<?=url::site('settings/auto_disburse_cash')?>" method="POST" >		
					   <div class="span5 ">
					
							<table class="table  table-hover">
								<tr>
									<th><?=Kohana::lang('backend.auto_disburse_cash')?></th>						
								</tr><tr>
									<th class="tiny-text"><?=Kohana::lang('backend.auto_disburse_cash_explanation')?></th>						
								</tr>
								<tr>
									<td>
									<div class="span4 text-right">Choose branch: </div>
										<div class="span8">
										<select name="auto_disburse_cash_to_agency_id" id="inputType">
											<!--Zero,is for none, meaning Auto-disburse cash is not active for that branch.-->
											<option value="0">None</option>
											<?php foreach ($siblings as $sib):?>
											<option value="<?=$sib->id?>" <?php if($sib->id == $settings->auto_disburse_cash_to_agency_id){echo "selected='selected'";}?>><?=$sib->name?></option>
											<?php endforeach;?>
										</select>
										</div>
									</td>	
								</tr>
							</table>
							<button type="submit" class="btn btn-info"><?=Kohana::lang('backend.save')?></button>
						</div>
						</form>
						
					</div><?php } ?>
			
		</div>
		
	  </div>
	
	 <div class="clear"></div>
	 
	 
	  	<script>
			  $(function () {
				$('#myTab a').click(function (e) {
			  e.preventDefault();
			  $(this).tab('show');
			})
			  })
			  
			$('#ticket_type_1').change(function(){
				if(this.checked){
					//if he selects ticket_type_1, disactivate ticket_type_2
					$('#ticket_type_2').prop('checked', false);
					}
			});$('#ticket_type_2').change(function(){
				if(this.checked){
					//if he selects ticket_type_2, deactivate ticket_type_1
					$('#ticket_type_1').prop('checked', false);
					}
			});
			
		
			</script>
<script type="text/javascript">
				$(function () 
				{
					$('#myTab a').click(function (e) {
					e.preventDefault();
					$(this).tab('show');  
					})
				}) 
</script>