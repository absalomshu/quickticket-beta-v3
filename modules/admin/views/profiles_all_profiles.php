<?php defined('SYSPATH') or die('No direct script access'); ?> 
<!DOCTYPE html>


		
		<div class="span5 hero-unit prof-unit offset3">
		<div class="notice">
			<?php
				//determine what type of notice to display if at all
				$notice = $this->session->get_once('notice');
					if(!empty($notice)){ 
						if($notice['type'] == 'success'){?><div class="alert general success"><a class="close" data-dismiss="alert" href="#">&times;</a><?php echo $notice['message']; ?></div><?php }?>
					<?	if($notice['type'] == 'error'){?><div class="alert general error"><a class="close" data-dismiss="alert" href="#">&times;</a><?php echo $notice['message'];?></div><?php }?>
			<?}?>
		</div>
			
			<h20>All Company profiles</h20>
			<div class="rule"><hr/></div><br/>
				<table class="table table-striped table-hover">
				
					<?php foreach ($company_profiles as $profile):?>
					<tr>
						<td><i class="icon-bookmark "></i><a href="<?=url::site('profiles/edit_company').'/'.$profile->id?>"> <?=$profile->name?></a> 
							<!--<a href="" class="btn btn-info" type="submit" name="check_out_form"><i class="icon-check"></i> Edit</button><br/>-->
						</td>
					</tr>
					
					<?php endforeach; ?>
				
				</table>
			

		</div>

