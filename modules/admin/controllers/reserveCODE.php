<?php defined('SYSPATH') or die('No direct script access');




class Reserve_Controller{
		
		public function reservation_details($PHPprotectV39){	
		
		$PHPprotectV42=get::client_reservations($PHPprotectV39);
		$PHPprotectV42=json_decode($PHPprotectV42->reserved_seats,true);
		if (!empty ($PHPprotectV42)){
				
?>
				<form action="<?=url::site('admin/complete_schedule/'.$PHPprotectV39)
?>" method='POST'>
				<table class='table table-bordered'>
										<tr>
												<th><?=Kohana::lang('backend.seat_no')
?></th>
												<th><?=Kohana::lang('backend.occupant')
?></th>
												<th><?=Kohana::lang('backend.phone')
?></th>
												<th>Actions</th>
										</tr> 
										
				<?php						
					foreach($PHPprotectV42 as $PHPprotectV162 => $PHPprotectV65){
					$PHPprotectV163 = serialize($PHPprotectV42);
											
?>
													
							<tr>
								<td><?=$PHPprotectV162
?></td>
								<td><?=$PHPprotectV65['name']
?></td>
								<td><?=$PHPprotectV65['phone']
?></td>
								
								<td><input type='hidden' value='<?php echo htmlentities($PHPprotectV163)
?>' name='arr'/>
									<!--simple hidden field to notify that this is from the reservation request form (client)-->
									<input type='hidden' value='1' name='request_from_frontend'/>
									<button type='submit' id='accept'name='res_to_occ' value="<?=$PHPprotectV162
?>" title='Confirm'></button> | 
									<button type='submit' id='reject' name='reject_reserve' value="<?=$PHPprotectV162
?>" title='Delete'></button>
								</td>
							</tr>	
						<?php					
		
					}
					}
					else{ echo Kohana::lang('backend.no_reservation');}
		}
		
		/* live update of the the number of reservation requests		*/
		public function reservation_count($PHPprotectV39){
			$PHPprotectV42=get::client_reservations($PHPprotectV39);
			$PHPprotectV164 = count(json_decode($PHPprotectV42->reserved_seats,true));
			if($PHPprotectV164 != 0){ echo "<span class='notification'>".$PHPprotectV164."</span>";}
		}
		
		}
