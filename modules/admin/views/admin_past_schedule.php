<?php defined('SYSPATH') or die('No direct script access'); 
	$current_branch = $this->admin->agency_id;
?> 

    <div class="container">
	<!-- Modal for sms after checkout-->
		<div id="send-sms" class="modal hide fade" tabindex="-1" role="dialog" aria-labelledby="myModalLabel" aria-hidden="true">
		  <div class="modal-header">
			<button type="button" class="close" data-dismiss="modal" aria-hidden="true">×</button>
			<h3 id="myModalLabel"><?=Kohana::lang('backend.send_sms_to_passengers')?> <?=$schedule->bus_number?></h3>
		  </div>
		  <form action="<?=url::site('admin/send_sms_after_schedule/'.$schedule->id)?>" method="POST" >
		  <div class="modal-body">
		   
			<div class="">
				<div class=" ">Message: <span class="red"> *</span></div><br>
				<center><textarea maxlength="150" style="height:100px;width:450px;"  name="sms_message"   required >Musango VIP Bus Service wishes to thank you for travelling with us. Have a wonderful day and hope to see you again.  </textarea></center>
			</div>
			
		  </div>
		  <div class="modal-footer">
			<button type="submit" class="btn btn-info" ><?=Kohana::lang('backend.send')?></button>
			<a class="btn" data-dismiss="modal" aria-hidden="true"><?=Kohana::lang('backend.cancel')?></a>
		  </div>
		  </form>
		</div>
	
	
	<!-- Modal for printing-->
		<div id="print-confirmation" class="modal hide fade" tabindex="-1" role="dialog" aria-labelledby="myModalLabel" aria-hidden="true">
		  <div class="modal-header">
			<button type="button" class="close" data-dismiss="modal" aria-hidden="true">×</button>
			<h3 id="myModalLabel"><?=Kohana::lang('backend.print_statistics')?> </h3>
		  </div>
		  <div class="modal-body">
			<p><?=Kohana::lang('backend.print_with_statistics')?></p>
		  </div>
		  <div class="modal-footer">
			<a href="#" id="print-with-stats" class="btn btn-primary"><?=Kohana::lang('backend.yes')?></a>
			<a href="#" id="print-without-stats" class="btn"><?=Kohana::lang('backend.no')?></a>
			<a class="btn" data-dismiss="modal" aria-hidden="true"><?=Kohana::lang('backend.cancel')?></a>
		  </div>
		</div>
	
	
	  <!--defines which area of the doc is printed-->
	  <div class="print-area">
	  <div class="row-fluid marketing" >
	  		
	
	  <? $admin=Authlite::instance()->get_admin(); ?>
	  	<div class="span12 hero-unit prof-unit" >
		<div class="heading"><?php 
						//echo get::_parent($this->admin->agency_id)->name." ".get::admins_town($this->admin->agency_id)->name;
						echo get::agency_name($current_branch);
					?> || <?=Kohana::lang('backend.schedule_for')?> <?=$schedule->bus_number?><?php if(get::vip_bus($schedule->bus_number)){?> <i class="icon-star" title="VIP Bus"></i> | <?=Kohana::lang('backend.schedule_id')?>: <?="SC".$this->agency_id.$schedule->id?>  <?php }?></div>
			<div class="rule"><hr/></div>
			<div class="span2 spacious">
				<div class="control-group">
				<label class="control-label lab" for="inputType"><?=Kohana::lang('backend.from')?>:</label>
					
					
							<h3><?=get::town($schedule->from);?></h3>
				</div>
			</div>
			<div class="span2 spacious">
				<div class="control-group">
				<label class="control-label lab" for="inputType"><?=Kohana::lang('backend.to')?>:</label>
					
				<h3><?=get::town($schedule->to);?></h3>
					</div>
				</div>
			<div class="span2 spacious">
				<label class="control-label lab" for="inputType"><?=Kohana::lang('backend.time')?>:</label>
									<h3><?=date("g:i A", strtotime($schedule->departure_time));?></h3>

			</div>
			<div class="span2 spacious">
				<div class="control-group">
				<label class="control-label lab" for="inputType">Date:</label>
					
						<h3><?=date("d-m-Y",strtotime($schedule->departure_date));?></h3>
					
				</div>
			</div>
			
			<div class="span2 spacious" >
				<div class="control-group">
				<label class="control-label lab" for="inputType"><?=Kohana::lang('backend.bus_type')?>:</label>
							
							<h3><?=$schedule->bus_seats?> Seater</h3>
							

				</div>
			</div>
			
			<div class="span2 spacious">
				<label class="control-label lab" for="inputType"><?=Kohana::lang('backend.bus_no')?>:</label>
						<h3><?=$schedule->bus_number?></h3>
				
			</div>
			<div class="span2 spacious">
				<label class="control-label lab" for="inputType"><?=Kohana::lang('backend.ticket_price')?>:</label>
						<h3><?=number_format($schedule->ticket_price)?> FCFA</h3>
			</div>
			
			<div class="span2 spacious">
				<label class="control-label lab" for="inputType"><?=Kohana::lang('backend.checkedout_at')?>:</label>
						<h4><?=date("d-m-Y g:i A ", strtotime($schedule->checked_out_on));?></h4>
			</div>
			
		
			
			  
		</div>

	<div class="row-fluid marketing" style="margin:5px 0 0px 0;">	
	<div class="span7 " style="margin-left:0px;" >
		<div class="span12 hero-unit prof-unit">
			<div class="heading">
				SLIP / BORDEREAU 
				<!--<button class="view" onClick="window.print()"><i class="icon-print"></i> <?=Kohana::lang('backend.print')?></button>-->
				<!--<span class="view"><?php if(date("d-m-Y", strtotime($schedule->checked_out_on)) == date("d-m-Y")){?><a href="<?=url::site('admin/modify_past_schedule/'.$schedule->id)?>"><?=Kohana::lang('backend.modify')?></a>&nbsp;&nbsp;&nbsp;<? }?></span>-->
				<div class="view">
					<div class="btn-group ">
					  <a class="btn dropdown-toggle btn-mini" data-toggle="dropdown" href="#" >
						-- Actions --
						<span class="caret"></span>
					  </a>
					  <ul class="dropdown-menu">
						
						<li><a href="#print-confirmation" data-toggle="modal"><i class="icon-print"></i> &nbsp;<?=Kohana::lang('backend.print')?></a></li>
						<li><?php if(date("d-m-Y", strtotime($schedule->checked_out_on)) == date("d-m-Y")){?><a href="<?=url::site('admin/modify_past_schedule/'.$schedule->id)?>"><i class="icon-edit"></i>&nbsp;<?=Kohana::lang('backend.modify')?></a><? }?></li>
						<li><a href="#send-sms" data-toggle="modal"><i class="icon-envelope"></i>&nbsp;<?=Kohana::lang('backend.send_sms')?></a></li>
					 </ul>
					</div>	
				</div>
			</div>
			<div class="rule"><hr/></div>
				<form action="<?=url::site('admin/complete_schedule/'.$schedule->id)?>" method="POST">
				
		
			
			
			
			
			<?php 
			//print_r($seats_and_occupants);exit;
			if (!empty($seats_and_occupants) OR !empty($seats_and_reservers))
			{?>
				<div>
						<?php 
						//combine reserved and taken
						//COMBINING INEFFICIENT AS IT IS A 2D AND A 3D
						//$all_seats = $seats_and_reservers + $seats_and_occupants;
						//ksort($all_seats);
						//foreach($seats_and_reservers as $key=>$subkey){print_r($subkey['name']);echo"<br/>";}?>
						<table class="table table-bordered table-condensed">
							
							<tr>
									<th><?=Kohana::lang('backend.seat')?></th>
									<th><?=Kohana::lang('backend.name')?></th>
									<th>IDC No.</th>
									<th>Phone</th>
									<th>*<?=Kohana::lang('backend.spec_drops')?></th>
									<!--<th>State</th>-->
							</tr>
							<tr><td>Driver</td><td colspan='4'><b><?=$schedule->driven_by?></b></td></tr>
							<?php 
						
							//sort the array in ascending order
							ksort($seats_and_occupants);
							foreach ($seats_and_occupants as $key=>$value)
							{ ?>
									<tr>
									<?php
									//Go to ticket details table and check if missed or not.
												$ticket = ORM::FACTORY('ticket_detail')->where('AgencyID',$this->agency_id)->where('SeatNumber',$key)->where('ScheduleID',$schedule->id)->find();?>
												
										<td>
											<?=$key?>
												<?php if(isset($value['free_ticket']) && $value['free_ticket']==1){?> <span class="badge badge-warning" title="Free-ticket">F</span> <?php }?>
												

										</td>

										<td><?=$value['name']?></td>
										<td><?=$value['idc']?></td>
										<td><?=$value['phone']?></td>
										<!--supress error if it's not a special drop, and hence has no special drop town. else, display it-->
										<td><?=@get::town($value['sd_town'])?></td>
										<!--<td></td>-->
									</tr>
								
								
								
							<?php } ;?>
						
						</table>	
				</div>
			<?php }?>	
			</form>
			</div>
			<div class="span12 hero-unit prof-unit" style="margin-left:0px;" >
				<?=Kohana::lang('backend.created_by')?>: <i><?=$schedule->CreatedBy?></i> &nbsp;
				<?=Kohana::lang('backend.checked_out_by')?>: <i><?=$schedule->CheckedOutBy?></i> &nbsp;
				<?=Kohana::lang('backend.modified_by')?>: <i><?=$schedule->modified_by?></i> &nbsp;
		
			</div>
		</div>
		
		
		<div class="span5" id="statistics" >
			<div class="span12  hero-unit prof-unit">
				<div class="heading"><?=Kohana::lang('backend.statistics')?></div>
				<div class="rule"><hr/></div>
				
				<table class="table table-bordered table-condensed">
					<tr>
						<td><?=Kohana::lang('backend.total_filled_seats')?></td>
						<td> <?=$no_so?></td>
					</tr>
					<tr>
						<td><?=Kohana::lang('backend.regular_seats')?></td>
						<td><?=$regular_seats?></td>
					</tr>
					<tr>
						<td><?=Kohana::lang('backend.spec_drops')?></td>
						<td><?=$special_drop_count?></td>
					</tr>
					<tr>
						<td><?=Kohana::lang('backend.empty_seats')?></td>
						<td><?=$empty_seats - 1; //-1 for the driver?></td>
					</tr>
					<tr>
						<td><?=Kohana::lang('backend.amt_spec_drops')?></td>
						<td><?=number_format($special_drop_total)?></td>
					</tr>
					<tr>
						<td><?=Kohana::lang('backend.free_tickets')?></td>
						<td><?=$free_ticket_count?></td>
					</tr>
					<tr>
						<td><?=Kohana::lang('backend.price_per_ticket')?></td>
						<td><?=number_format($ticket_price)?> FCFA</td>
					</tr>	
					<tr>
						<th> <?=Kohana::lang('backend.total_income')?> </th>
						<th><?=number_format($total)?> FCFA</th>
					</tr>	
				</table>
				
				<div class="heading"><?=Kohana::lang('backend.expenses_sched')?></div>
				<table class="table table-bordered table-condensed">
				
				
				
				<?php 
				$total_expenses_for_schedule = 0;
				foreach($expenses_for_schedule as $exp){?>
					<tr>
						<td class="span8"><?=$exp->purpose?></td>
						<td class="span4"><?=number_format($exp->amount)?> FCFA</td>
					</tr>
				<?php 
				$total_expenses_for_schedule += $exp->amount;
				} ?>	
					<tr>
						<th class="span8"><?=Kohana::lang('backend.total_expenses')?></th>
						<th class="span4"><?=number_format($total_expenses_for_schedule)?> FCFA</th>
					</tr>
				</table>
				
				<table class="table table-bordered table-condensed">
					<tr>
						<th class="span8"><?=Kohana::lang('backend.net_income')?></th>
						<?php $net_income = $total - $total_expenses_for_schedule;?>
						<th class="span4"><?=number_format($net_income)?> FCFA</th>
					</tr>
				</table>
				
				<form action="<?=url::site('expenses/auto_disburse_cash/'.$schedule->id)?>" method="POST">
					<input name='net_income' type="hidden" value='<?=$net_income?>'>
					<input name='cash_interbranch_agency_to' type="hidden" value='<?=$settings->auto_disburse_cash_to_agency_id?>'>
					
					<!--Auto-disburse can only be done once, which is if it's not been done before, hence the net income disbursed is still 0
						Also ensure the net income is not negative to avoid complications
					-->
					<?php if($schedule->net_income_disbursed == 0){
						if($net_income > 0 AND $settings->auto_disburse_cash_to_agency_id !='0'){ ?>
						<button class='btn btn-link'  type='submit' onClick="return confirm('<?=Kohana::lang('backend.confirm_auto_disburse').number_format($net_income).Kohana::lang('backend.confirm_auto_disburse_middle').get::agency_name($settings->auto_disburse_cash_to_agency_id ).Kohana::lang('backend.confirm_auto_disburse_ending');?>?');"><div class="view">Disburse net income</div></button>
					<?php } ?>
					<?php }else{?>
					<div class="tiny-text"><?=number_format($schedule->net_income_disbursed)?> FCFA disbursed</div>
					<?php }?>
				</form>
				</div>
				<div class="span12"  >
					<br><br><br><br><br>
					<?=Kohana::lang('backend.verified_by')?>: &nbsp;&nbsp;&nbsp; _________________________________
				
				</div>
		</div>
		
		
		</div>
		</div>
	  </div>
	  <div style="height:19px;"></div>
    </div> 
	
<script>

	$("#print-without-stats" ).click(function() {
		$("#statistics" ).addClass( "no-print" );
		$('#print-confirmation').modal('hide');
		window.print();
		
	});	
	$("#print-with-stats" ).click(function() {
		$("#statistics" ).removeClass( "no-print" );
		$('#print-confirmation').modal('hide');
		window.print();
		
	});	

</script>
