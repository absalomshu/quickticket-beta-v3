<?php defined('SYSPATH') or die('No direct script access');

class Income_Model extends ORM{

	public function register_bus_rental($agency_id, $bus_number, $rented_to, $amount, $description, $start_date, $end_date, $admin_username)
	{
		$income = ORM::FACTORY('income');
		$income->agency_id = $agency_id;
		$income->bus_number = $bus_number;
		$income->rented_to = $rented_to;
		$income->amount = $amount;
		$income->purpose = 'bus_rental';
		$income->description = $description;
		$income->date_incurred = date("Y-m-d");
		$income->start_date = $start_date;
		$income->end_date = $end_date;
		$income->CreatedBy = $admin_username;
		$income->save();
		
		
		//Update the admin's available balance
		$admin = ORM::FACTORY('admin')->where('username',$admin_username)->find();
		$admin->available_balance = $admin->available_balance + $amount;
		$admin->save();
	}
	
	public function add_other_income($agency_id, $bus_number,  $amount, $description, $date_incurred, $admin_username)
	{
		$income = ORM::FACTORY('income');
		$income->agency_id = $agency_id;
		$income->bus_number = $bus_number;
		//$income->rented_to = $rented_to;
		$income->amount = $amount;
		$income->purpose = 'other';
		$income->description = $description;
		$income->date_incurred = date("Y-m-d");
		
		$income->CreatedBy = $admin_username;
		$income->save();
		
		
		//Update the admin's available balance
		$admin = ORM::FACTORY('admin')->where('username',$admin_username)->find();
		$admin->available_balance = $admin->available_balance + $amount;
		$admin->save();
	}
	
	
	
	public function get_agency_income_by_period_by_bus($agency_id,$start_date,$end_date,$bus_number)
	{
		$db = new Database();
		$income = $db->query("SELECT * FROM incomes WHERE agency_id= ".$agency_id." AND bus_number = '".$bus_number."' AND deleted = '0' AND date_incurred BETWEEN CAST('$start_date' AS DATE) AND CAST('$end_date' AS DATE) ORDER BY date_incurred ASC");
		return $income;
	}
	
	public function get_bus_rentals_today($agency_id)
	{
		//$count=ORM::factory('bus')->where('agency_id',$agency_id)->count_all();
		$today = date('Y-m-d');
		
		$db = new Database();
		$income = $db->query("SELECT * FROM incomes WHERE agency_id= ".$agency_id." AND deleted = '0' AND date_incurred ='".$today."'");
		return $income;
		
		//return $count;	
	}
	public function get_bus_rentals_by_period($agency_id,$start_date,$end_date,$bus_number)
	{
		
		$db = new Database();
		$income = $db->query("SELECT * FROM incomes WHERE agency_id= ".$agency_id." AND deleted = '0' AND date_incurred BETWEEN CAST('$start_date' AS DATE) AND CAST('$end_date' AS DATE) ORDER BY date_incurred ASC");
		return $income;
		
		//return $count;	
	}
	
	public function get_income_from_rentals_today($agency_id)
	{	
		$today = date('Y-m-d');
		$rentals=ORM::factory('income')->where('agency_id',$agency_id)->where('purpose','bus_rental')->where('deleted','0')->where('date_incurred',$today)->find_all();
		return $rentals;	
	}
	
	public function count_bus_rentals_today($agency_id)
	{	
		$today = date('Y-m-d');
		$count=ORM::factory('income')->where('agency_id',$agency_id)->where('deleted','0')->where('date_incurred',$today)->count_all();
		return $count;	
	}	
	
	
	public function get_agency_income_by_period_by_admin($agency_id,$start_date,$end_date,$admin)	{
		$db = new Database();
		$income = $db->query("SELECT * FROM incomes WHERE agency_id= ".$agency_id." AND CreatedBy = '".$admin."' AND deleted = '0' AND date_incurred BETWEEN CAST('$start_date' AS DATE) AND CAST('$end_date' AS DATE) ORDER BY date_incurred ASC");
		return $income;
	}
	
	public function get_agency_income_total_by_period_by_admin($agency_id,$start_date,$end_date,$admin)	{
		$incomes=Income_Model::get_agency_income_by_period_by_admin($agency_id,$start_date,$end_date,$admin);
		//Income from schedule
		$income_total = 0;
		foreach($incomes as $income){	
				$income_total += $income->amount;
		}
		return $income_total;
	}
	
	public function get_agency_income_by_period($agency_id,$start_date,$end_date){
		$db = new Database();
		$income = $db->query("SELECT * FROM incomes WHERE agency_id= ".$agency_id." AND deleted = '0' AND date_incurred BETWEEN CAST('$start_date' AS DATE) AND CAST('$end_date' AS DATE) ORDER BY date_incurred ASC");
		return $income;
	}
	
	public function get_agency_income_by_schedule($agency_id,$schedule_id)	{
		$db = new Database();
		$income = $db->query("SELECT * FROM incomes WHERE agency_id= ".$agency_id." AND deleted = '0' AND schedule_id =".$schedule_id." ORDER BY date_incurred ASC");
		return $income;
	}
	
	public function get_agency_income_total_by_schedule($agency_id,$schedule_id){
		
		$schedule_income=Income_Model::get_agency_income_by_schedule($agency_id,$schedule_id);
		//Income from schedule
		$schedule_income_total = 0;
		foreach($schedule_income as $income)
		{
			$schedule_income_total += $income->amount;
		}
		
		return $schedule_income_total;
	}
	
	
	
	public function get_other_income_excluding_rentals_by_date($agency_id,$date){
		//$schedules=ORM::factory('income')->where('agency_id',$agency_id)->where('deleted','0')->where('date_incurred',$date)->find_all();
		$other_income=ORM::factory('income')->where('agency_id',$agency_id)->where('purpose !=','bus_rental')->where('deleted','0')->where('date_incurred',$date)->find_all();
		return $other_income;
	}
	public function get_other_income_by_date($agency_id,$date){
		//$schedules=ORM::factory('income')->where('agency_id',$agency_id)->where('deleted','0')->where('date_incurred',$date)->find_all();
		$other_income=ORM::factory('income')->where('agency_id',$agency_id)->where('deleted','0')->where('date_incurred',$date)->find_all();
		return $other_income;
	}
	
	public function get_other_income_excluding_rentals_by_date_total($agency_id,$date){
		
		//Only calculate from departed schedules
		$date = date("Y-m-d",strtotime($date));
		$other_income=Income_Model::get_other_income_excluding_rentals_by_date($agency_id,$date);
		//var_dump($period_schedules);exit;
		//Income from schedules
		$total_other_income = 0;
		foreach($other_income as $income){
			$total_other_income += $income->amount;
		}
		
		return $total_other_income;
	}
	
	public function get_all_unpushed($agency_id)
	{
		$income = new Database();
		$income=ORM::factory('income')->where('agency_id',$agency_id)->where('deleted','0')->where('online','0')->find_all();
		return $income;
	}
	

}
