<?php defined('SYSPATH') or die('No direct script access'); ?>
	
		
		
		<div class="row-fluid  span12 hero-unit prof-unit reload" style="margin:5px 0 0px 0;">	

			<div class="no-print">
			<?php 
			
			?>
			<ul class="nav nav-tabs">
					<li > <a href="<?=url::site('reports')?>"><i class="icon-arrow-left"></i> <?=Kohana::lang('backend.back_reports')?></a></li>
					<button class="view" onClick="window.print()"><i class="icon-print"></i> <?=Kohana::lang('backend.print')?></button>
				</ul>
			</div>
			<div class=" print-area" >
			
				
				<legend>
					<div class="print-head">
						<?php $admin=Authlite::instance()->get_admin();?>
						<?=strtoupper(get::_parent($admin->agency_id)->name);?><br/>
						<?=get::agency_name($admin->agency_id);?>
					</div>
					Report: Income & Expenses [Chart]	<br/>
					Start date: <?=date("d-m-Y",strtotime($start_date))?> &nbsp;&nbsp;&nbsp;&nbsp; 
					End date: <?=date("d-m-Y",strtotime($end_date))?>&nbsp;&nbsp;&nbsp;&nbsp;
				</legend>
					<?php
					//Now take account of remote tickets
					foreach ($remote_outgoing_tickets as $ticket)
					{
						//add to total from schedules
						$total += $ticket->Price;
					}?>
					
				<div class="span8" >
						<svg width="960" height="500"></svg>
						<div class="span4 offset4">
							<span class="badge badge-info">&nbsp;</span> Income &nbsp;&nbsp;
							<span class="badge badge-warning">&nbsp;</span> Expenditure &nbsp;&nbsp;
							<span class="badge badge-success">&nbsp;</span> Net
						</div>	
				</div><br><br>
				
				
				<div class="span12" style="margin: 5px 0 0 0;" >
				<div class="heading">
					Daily total
				</div>
				
					
					<?php 
					$income_total = 0;
					foreach ($period_income as $income){
					$income_total += $income->amount; }
					?>
				<table class="table table-condensed table-striped ">	
				<tr>
						<th class="span2">Date</th>
						<th class="text-right" ></th>
						<th class="text-right" >Expenses</th>
						<th class="text-right">Income</th>
						<th class="text-right">Net</th>
					</tr>
				<?php foreach($daily_totals as $day => $totals){ ?>
					<tr>		
						<td><?=date('d-m-Y',strtotime($day))?></td>
						<th class="span8 text-right">&nbsp;</th>
						<th class="span2 text-right"><?=number_format($totals['total_expenses'])?></th>
						<th class="span2 text-right"><?=number_format($totals['total_income'] )?></th>
						<th class="span2 text-right"><?=number_format($totals['net'] )?></th>
					</tr>
					
				
				<?php } ?>
				</table>
				
				<div class="clear" style="height:10px;"><br><br></div>
				<!--NET SUMMARY TABLE-->
				<table class="table table-condensed table-striped ">
					<tr>
						<th class="span2">GRAND TOTAL</th>
						<td>&nbsp;</td>
						<th class="text-right" >EXPENSES</th>
						<th class="text-right">INCOME</th>
					</tr>
					<tr>		
						<td></td>
						<th></th>
						<th class="span2 text-right"><?=number_format($total_expenditure)?></th>
						<th class="span2 text-right"><?=number_format($total + $parcel_total + $income_total )?></th>
					</tr>
					<tr>		
						<td></td>
						<th></th>
						<th class="span2 text-right">NET</th>
						<th class="span2 text-right"><?=number_format($total + $parcel_total + $income_total - $total_expenditure)?> </th>
					</tr>
				</table>	
				
			
				<div>Printed by: <?=STRTOUPPER($this->admin->username);?></div> 
				<div>Printed on: <?=date('d-m-Y h:i:s A')?></div>
				<!--END OF NET SUMMARY-->
				</div>
			
				
			</div>
		</div>
		
<?php //Include the code for the line chart
include ("js/line_chart.js")?>