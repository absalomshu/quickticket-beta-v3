<?php defined('SYSPATH') or die('No direct script access'); ?> 

	
	  
	  <div class="container">
	  <div class="row-fluid " style="margin:5px 0 0px 0px;">
	  
		
		<section class="download" id="components">

	  	<div class="span12 hero-unit prof-unit" >
		
			<?php
		//determine what type of notice to display if at all
		
		$notice = $this->session->get_once('notice');
			if(!empty($notice)){ 
				if($notice['type'] == 'success'){?><div class="alert general success"><a class="close" data-dismiss="alert" href="#">&times;</a><?php echo $notice['message']; }?></div>
			<?	if($notice['type'] == 'error'){?><div class="alert general error"><a class="close" data-dismiss="alert" href="#">&times;</a><?php echo $notice['message']; }?></div>
		<?}?>
		
		<legend><?=Kohana::lang('backend.settings')?></legend>
             
			
		
			
			<div class="tabbable tabs-left span12">
				<ul class="nav nav-tabs" id="myTab">
							  
							  <li><a href="#online-tab"><?=Kohana::lang('backend.online')?></a></li>
							  
				</ul>
							 
				<div class="tab-content">
					
					
				
						
					
					<div class="tab-pane active" id="online-tab">
						
						<form action="<?=url::site('manage/add_open_ticket')?>" method="POST" >		
					   <div class="span8 offset1">
								<!--hidden inputs here is a technique to ensure that unchecked fields pass a 0 to the POST-->
								<legend>Fixed schedules</legend>
								<table class="table table-striped table-condensed table-hover">
									
									<tr>
										<th>Service/ticket name</th>
										<th>From</th>
										<th>To</th>
										<th>Price</th>
										<th>Actions</th>
									</tr>
									<?php foreach($open_tickets as $open_ticket){?>
									<tr>
										<td><?=$open_ticket->open_ticket_name?></td>
										<td><?=get::town($open_ticket->town_from)?></td>
										<td><?=get::town($open_ticket->town_to)?></td>
										
										<td><?=number_format($open_ticket->price)?></td>
										<td><a href="<?=url::site('manage/delete_open_ticket/'.$open_ticket->id)?>"  onClick="return confirm('<?=Kohana::lang('backend.confirm_delete')?>?');"><i class="icon-trash"></i> </a></td>
									</tr>
									<?php }?>
								</table>
								
								<legend>Add fixed schedule</legend>
								
								
								<div>
									<div class="span4 text-right">From <span class="red"> *</span>: </div>
										<div class="span8">
										<select name="agency_from" id="inputType">
													<?php foreach ($parent_agencies as $sib):?>
													<option value="<?=$sib->id?>"><?=$sib->name?></option>
													<?php endforeach;?>
										</select></div>
								</div>
								<!--<div class="">
									<div class="span4"><?=Kohana::lang('backend.from')?><span class="red"> *</span>: </div>
									<div class="span8">
										<select name="open_ticket_town_to" id="inputType" class="span4">
										
										<?php foreach ($towns as $town):?>
										<option value="<?=$town->id?>"><?=$town->name?></option>
										<?php endforeach;?>
									
										</select>
									</div>
								</div> -->
								<div class="">
									<div class="span4 text-right"><?=Kohana::lang('backend.to')?><span class="red"> *</span>: </div>
									<div class="span8">
										<select name="open_ticket_town_to" id="inputType" class="span4">
										
										<?php foreach ($towns as $town):?>
										<option value="<?=$town->id?>"><?=$town->name?></option>
										<?php endforeach;?>
									
									</select>
									</div>
								</div>
								
								<div>
									<div class="span4 text-right">Service/ticket name: <span class="red"> *</span></div>
									<div class="span8"><input class="span6" type="text" name="open_ticket_name" placeholder="<?=Kohana::lang('backend.ticket_name')?>" required></div>
								</div>
								
								<div>
									<div class="span4 text-right">Price: <span class="red"> *</span></div>
									<div class="span8"><input class="span6" type="text" name="open_ticket_price" placeholder="<?=Kohana::lang('backend.price')?>" required></div>
								</div>
								
								<div>
									<div class="span4"></div>
									<div class="span8"><button type="submit" class="btn btn-info" title="Quick-add bus requirement"><?=Kohana::lang('backend.add')?></button></div>
								&nbsp;
								</div>
								
								
							
						</div>
						</form>
						
						
				
					
						
						
					</div>	
					</div>
					</div>
					
								
			<script>
			  $(function () {
				$('#myTab a').click(function (e) {
			  e.preventDefault();
			  $(this).tab('show');
			})
			  })
			  
			$('#ticket_type_1').change(function(){
				if(this.checked){
					//if he selects ticket_type_1, disactivate ticket_type_2
					$('#ticket_type_2').prop('checked', false);
					}
			});$('#ticket_type_2').change(function(){
				if(this.checked){
					//if he selects ticket_type_2, deactivate ticket_type_1
					$('#ticket_type_1').prop('checked', false);
					}
			});
			
		
			</script>
		</div>
		</section>
		
	  </div>
	  </div>
	  <div style="height:19px;"></div>
