<?php defined('SYSPATH') or die('No direct script access');

class User_Model extends ORM
{

	public function add_user($name, $email, $password)
	{
		$user=ORM::factory('user');
		$user->names=$name;
		$user->email=$email;
		$user->password=Authlite::instance()->hash($password);
		$user->save();
	} 
	
	public function delete_user($id)
	{
		$delete=ORM::factory('user')->where('id',$id)->find();
		$delete->delete();
		$delete->save();
	}
	
	public function edit_password($email, $new_password)
	{
		$user=ORM::factory('user')->where('email',$email)->find();
		$user->password=Authlite::instance()->hash($new_password);
		$user->save();
	}
	
	public function confirm_email($email_address)
	{
		$confirm=ORM::factory('user')->where('email',$email_address)->find();
		$confirm->is_email_confirmed='1';
		$confirmed=$confirm->save();
		
		return $confirmed;
	}
}
