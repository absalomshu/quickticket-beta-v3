<?php defined('SYSPATH') or die('No direct script access'); ?> 
	
	<div class="heading">Today: <?=date("d-m-Y")?></div>
	<div class="rule"><hr/></div><br>
	 
	<div class="row-fluid " style="margin:5px 0 0px 0px;">
	  	
		
		
		
		
		<?php if($_SESSION['ModuleAccess']['2']){ ?>
			<div class="span3 hero-unit prof-unit" >
				<table class="table table-condensed">
					<div class="heading">Schedules</div>
					<tr><td>Current </td><td class="text-right">  <?=$current_schedules_count?></td></tr>
					<tr><td>Checked out today </td><td class="text-right">  <?=$checked_out_today_count?></td></tr>
					<tr><td>Free tickets</td><td class="text-right"> <?=$free_tickets_issued_today_count?></td></tr>
					<tr><td>Total tickets issued</td><td class="text-right">  <?=$tickets_total_issued_today_count?></td></tr>
				</table>  
			</div>
		<? } ?>
		
		<?php if($_SESSION['ModuleAccess']['4']){ ?>
		<div class="span3 hero-unit prof-unit" >
			<table class="table table-condensed">
				<div class="heading">Parcels</div>
				
				<tr><td>Unsent</td> <td class="text-right"><?=$parcels_unsent_today_count?></td></tr>
				<tr><td>Sent </td><td class="text-right">  <?=$parcels_sent_today_count?></td></tr>
				<tr><td>Total registered</td><td class="text-right"> <?=$parcels_total_outgoing_today_count?></td></tr>
				<tr><td>Total incoming</td><td class="text-right">  <?=$parcels_total_incoming_today_count?></td></tr>
			</table>
		</div>
		<? } ?>
		
		<?php if($_SESSION['ModuleAccess']['5']){ ?>
		<div class="span3 hero-unit prof-unit" >
		<table class="table table-condensed">
			<div class="heading">Income</div>
			<tr><td>Bus rentals</td><td class="text-right"> <?=number_format($income_from_bus_rentals_today)?></td></tr>
			<tr><td>Parcel income</td><td class="text-right"> <?=number_format($income_from_parcels_registered_today)?></td></tr>
			<tr><td>Schedule income</td><td class="text-right"><?=number_format($total_schedule_income_today)?></td></tr>
			<tr><td>Other income</td><td class="text-right"><?=number_format($other_income_total)?></td></tr>
			<tr><th>Total</th><th class="text-right"> <?=number_format($total_income_today)?></th></tr>
		</table> 
		</div>
		<? } ?>
		
		<?php if($_SESSION['ModuleAccess']['3']){ ?>
		<div class="span3 hero-unit prof-unit" >
		<table class="table table-condensed">
			<div class="heading">Expenses</div>
			
			<?php 
			$expenses_total = 0;
			foreach($expense_category_totals as $cat_name=>$cat_total){ ?>
				<tr><td><?=$cat_name?></td><td class="text-right"> <?=number_format($cat_total)?></td></tr>
			<?php  }?>
				<tr><th>Total</th><th class="text-right"> <?=number_format($expenses_total)?></th></tr>
		</table>	  
		</div>
		<? } ?>
		
		<div class="clear"></div>
		
		<?php if($_SESSION['ModuleAccess']['7']){ ?>
			<div class="span6 hero-unit prof-unit"  style="margin:5px 0 0px 0px;">
			<div class="heading">Past week</div>
			<div class="rule"><hr></div>
				<svg width="600" height="300"></svg>
							<div class="span6 offset4">
								<span class="badge badge-info">&nbsp;</span> Income &nbsp;&nbsp;
								<span class="badge badge-warning">&nbsp;</span> Expenditure &nbsp;&nbsp;
								<span class="badge badge-success">&nbsp;</span> Net
							</div>	
			</div>
		<? } ?>
		
		<?php if($_SESSION['ModuleAccess']['7']){ ?>
			<div class="span3 hero-unit prof-unit" >
			<table class="table table-condensed">
				<div class="heading">Summary</div>
				<tr><td>Total income</td><td class="text-right"> <?=number_format($income_from_bus_rentals_today)?></td></tr>
				<tr><td>Total expenses</td><td class="text-right"><?=number_format($expenses_total)?></td></tr>
				<tr><th>Net</th><th class="text-right"><?=number_format($income_from_parcels_registered_today)?></th></tr>
			</table>
			</div>
		<? } ?>
		
		<?php if($_SESSION['ModuleAccess']['5']){ ?>
			<div class="span3 hero-unit prof-unit" >
			<table class="table table-condensed">
				<div class="heading">Bus documents</div>
				<tr><td>Expired</td><td class="text-right"><?=number_format($no_of_expired_documents)?></td></tr>
				<tr><td>Expiring</td><td class="text-right"> <?=number_format($no_of_expiring_documents)?></td></tr>
				
			</table>
			</div>
		<? } ?>
		
		<?php if($_SESSION['ModuleAccess']['5']){ ?>
		<div class="span3 hero-unit prof-unit" >
			<table class="table table-condensed">
				<div class="heading">Buses</div>
				<tr><td>Available </td><td class="text-right"> <?=$available_buses_count?></td></tr>
				<tr><td>Unavailable </td><td class="text-right"> <?=$unavailable_buses_count?></td></tr>
				<tr><td>Total </td><td class="text-right"> <?=$total_buses_count?></td></tr>
				<tr><td>Rentals today </td><td class="text-right"> <?=$rentals_today_count?></td></tr>
			</table>
			
			<table class="table table-condensed table-striped">
			<tr>
				<th>Seater</th>
				<th>Number</th>
			</tr>
			<?php foreach($seats_and_number_of_buses as $seater=>$number_of_buses){?>	
				<tr>
					<td><?=$seater?></td>
					<td><?=$number_of_buses?></td>
				</tr>
			<? } ?>
			</table>
			  
		</div>
		<? } ?>
	</div>
	 <div class="clear"></div>
<!--
<script type="text/javascript">
	$(document).ready(function(){
			$('#timepicker').timepicker({
				minuteStep:30,
				//defaultTime: '8:00 PM'
			});
			
				$(function () {
				$('#myTab a').click(function (e) {
					  e.preventDefault();
					  $(this).tab('show');  
			})
			  })
	
	})
	</script>-->
<?php //Include the code for the line chart
include ("js/line_chart.js")?>