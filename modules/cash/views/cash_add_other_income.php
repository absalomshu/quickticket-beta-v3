<?php defined('SYSPATH') or die('No direct script access'); ?> 

 
	
	<div class="row-fluid">	
		
		<div class="span12 hero-unit prof-unit">
		
		<ul class="nav nav-tabs">
					<li > <a href="<?=url::site('cash/other_income')?>"><i class="icon-arrow-left"></i> <?=Kohana::lang('backend.back_other_income')?></a></li>
		</ul>
		
		
		<div class="row-fluid">	
		
			<form action="<?=url::site('cash/add_other_income/')?>" method="POST" name="theform" class="span8 offset2">
				<legend><?=Kohana::lang('backend.add_other_income')?></legend>
						
						
						<div class="">
							<div class="span4 text-right"><?=Kohana::lang('backend.bus_no')?>: </div>
							<div class="span8">
							<select name="bus_number" id="inputType">
									<option value=""> None</option>
									<?php foreach($all_buses as $bus){?>
										<option value="<?=$bus->bus_number?>"> <?=$bus->bus_number?></option>
										
									<?php }?>
							</select>
							</div>
						</div>
						
					
						
						<div class="" >
							<div class="span4 text-right"><?=Kohana::lang('backend.date')?>: </div>
								<div class="input-append date span8">
									<input type="text" name = 'date_incurred' class="span5 datepicker"  value="<?=date('d-m-Y')?>" id="dp1" required>
									<span class="add-on"><i class="icon-calendar"></i></span>
								</div>
						</div>
						
						
						
						
						<div class="">
							<div class="span4 text-right"><?=Kohana::lang('backend.amount')?>: <span class="red"> *</span></div>
							<div class="span8 controls input-append"><input class="span5 IsAmount" type="text"  name="amount" required><span class="add-on"> FCFA</span></div>
						</div>
						
						<div class="">
											<div class="span4 text-right"><?=Kohana::lang('backend.description')?>: <span class="red"> *</span> </div>
											<div class="span8"><textarea placeholder="<?=Kohana::lang('backend.description')?>"  name="description"></textarea></div>
						</div>
								
						
					
						<div class="clear"></div>
						
						<div class="form-actions">
							<div class="span4"></div>
							<div class="span8"><button class="btn btn-info " type="submit"><?=Kohana::lang('backend.register')?></button></div>
						</div>
						
									
						
				</form>
		
		
			
			<div class="span6">
			
				
				
			</div>	
			
				
				
		</div>
	
		
		</div>
	
    </div> 

	
	