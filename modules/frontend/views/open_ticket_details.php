<?php defined('SYSPATH') or die('No direct script access'); ?> 


  <div class="container">
    
	  <div class="row-fluid marketing" style="margin:5px 0 0px 0px;">

	  
		<div class="span12 hero-unit prof-unit" >
			<table class="table schedules-table">	
						<tr>
							<th>Travel agency</th>
							<th>From</th>
							<th>To</th>
							<th>Date</th>
							<th>Departures</th>
							<th>Price</th>
							<th></th>
						</tr>
					
							<tr>
								<td><?=get::agency_and_town($purchase->agency_id)?></td>
								<td><?=get::town($purchase->town_from);?></td>
								<td><?=get::town($purchase->town_to);?></td>
								<td><?=date("d-m-Y");?></td>
								<td>N/A</td>
								<td><?=number_format($purchase->ot_ticket_price)?> frs</td>
								
							</tr>
						
			</table>
		</div>
		
	<?php
		//determine what type of notice to display if at all
		$notice = $this->session->get_once('notice');
			if(!empty($notice)){ 
				if($notice['type'] == 'success'){?><div class="alert general success"><a class="close" data-dismiss="alert" href="#">&times;</a><?php echo $notice['message']; }?></div>
			<?	if($notice['type'] == 'error'){?><div class="alert general error"><a class="close" data-dismiss="alert" href="#">&times;</a><?php echo $notice['message']; }?></div>
		<?}?>	
	

	<div class="row-fluid marketing" style="margin:5px 0 0px 0;">	
		<div class="span4 hero-unit prof-unit" style="float:right;" >
				<div class="heading">Tips for open tickets</div>
				<div class="rule"><hr/></div>
				<ul>
					<li>Arrive at travel agency</li>
					<li>Present your ID card</li>
				</ul>

		</div>
		
		
		<div class="span8 hero-unit prof-unit" style="margin-left:0px;" >
		<form action="<?=url::site('home/open_ticket_details/'.$purchase->id)?>" method="POST">
			<div class="heading">Passenger information<span class="small-right-link"></span></div>
			<div class="rule"><hr/></div>
				
				<div class="span5 spacious">
					<div class="control-group">
					<label class="control-label lab" for="inputType">Names:</label>
						<div class="controls ">
							<input type="text"  name="client_name" placeholder="Passenger names as on ID card" required>
						</div>
					</div>
				</div>
				<div class="span5 spacious">
					<div class="control-group">
					<label class="control-label lab" for="inputType">Phone:</label>
						<div class="controls input-append ">
							<span class="add-on"> +237</span><input class="span11" type="text"  name="client_phone" placeholder="For confirmation SMS">
						</div>
					</div>
				</div>
				<div class="span5 spacious">
					<div class="control-group">
					<label class="control-label lab" for="inputType">Email:</label>
						<div class="controls ">
							<input type="email"  name="client_email" placeholder="Your email address">
						</div>
					</div>
				</div>
				<div class="span5 spacious">
									<div class="control-group">
									<label class="control-label lab" >ID card no:</label>
										<div class="controls ">
											<input type="text"  name="client_idc" placeholder="Your ID card number" required>
										</div>
									</div>
				</div>
				
				
			
				
		
				<div class="span5 spacious">
					<div class="control-group">
						<label class="control-label lab" for="inputType">&nbsp;</label>					
						<label class="control-label lab" for="inputType">
							
							<input type="submit" class="btn btn-info" value="Reserve" name="reserve_open_ticket" />
							<!--<input type="submit" class="btn btn-warning" value="Buy ticket" name="purchase_open_ticket" />-->
							<!--<input type="submit" class="btn btn-info" value="Cancel" />-->
						</label>
					</div>
				</div>
		
		
		</form>		
		
		
		<!--<div class="span10 nota-bene">
						* You will receive a confirmation message if this seat is granted you.<br/>
						* All seats reserved must be confirmed by payment before 8:00 PM
		</div>-->
		</div>
		
		</div>
	  </div>  </div>
   

