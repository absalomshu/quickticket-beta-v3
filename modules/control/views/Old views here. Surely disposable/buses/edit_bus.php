<?php defined('SYSPATH') or die('No direct script access'); ?> 
	
	<div class="notice">
		<?php
			//determine what type of notice to display if at all
			$notice = $this->session->get_once('notice');
				if(!empty($notice)){ 
					if($notice['type'] == 'success'){?><div class="alert general success"><a class="close" data-dismiss="alert" href="#">&times;</a><?php echo $notice['message']; ?></div><?php }?>
				<?	if($notice['type'] == 'error'){?><div class="alert general error"><a class="close" data-dismiss="alert" href="#">&times;</a><?php echo $notice['message'];?></div><?php }?>
		<?}?>
	</div>
 
	<div class="row-fluid " style="margin:5px 0 0px 0px;">
	  
		
		
	  	<div class="span12 hero-unit prof-unit" >
		
				<div class="heading"><?=Kohana::lang('backend.edit_bus')?> <?= $bus->bus_number?></div>
				<div class="rule"><hr/></div>
		
				<form action="<?=url::site('buses/edit_bus/'.$bus->bus_number)?>" method="POST">
					<div class="span6" >
						
						<div class="simple-form span12" style="margin-left:10px;">
							<div class="sf_label"><?=Kohana::lang('backend.bus_no')?>: </div>
							<div class="sf_input"><input type="text"  name="bus_number" value="<?=$bus->bus_number?>" disabled /></div>
						</div>
						
						<div class="simple-form span12">
							<div class="sf_label"><?=Kohana::lang('backend.bus_type')?>: </div>
								<div class="sf_input">
									<select name="bus_seats" id="inputType" class="span6">

										<option value="<?=$bus->bus_seats?>"><?=$bus->bus_seats?> <?=Kohana::lang('backend.seater')?></option>
										<option value="30">30 <?=Kohana::lang('backend.seater')?></option>
										<option value="35">35 <?=Kohana::lang('backend.seater')?></option>
										<option value="39">39 <?=Kohana::lang('backend.seater')?></option>
										<option value="55">55 <?=Kohana::lang('backend.seater')?></option>
										<option value="70">70 <?=Kohana::lang('backend.seater')?></option>

									</select>
								</div>
							
						</div>
						
						<div class="simple-form span12">
							<div class="sf_label"><?=Kohana::lang('backend.drivers_name')?>: </div>
							<div class="sf_input"><input type="text"  name="driver" value="<?=$bus->driver?>"></div>
						</div>
						
					
						<!--
						<div class="simple-form span12">
											<div class="sf_label">Purpose: </div>
											<div class="sf_text"><textarea placeholder="What was this spent on?"  name="purpose" class="span11" required ></textarea></div>
						</div>-->
								
						
					
						<div class="clear"></div>
						<div class="clear"><br/><br/></div>
						
						<div class="simple-form span12">
							<div class="sf_label">&nbsp; </div>
							<div class="sf_input">
								<button class="btn btn-info" type="submit"> <?=Kohana::lang('backend.save')?></button>
								<a href="<?=url::site('buses/all_buses')?>" class="btn" type="submit"> <?=Kohana::lang('backend.cancel')?></a>
								<a href="<?=url::site('buses/delete_bus/'.$bus->bus_number)?>"  onClick="return confirm('Are you sure you want to delete this bus?');" type="submit"> <?=Kohana::lang('backend.delete')?></a>
							</div>
						</div>
						
									
					</div>
						
				</form>
			 
			
				
			  					
			
		
			  
		</div>
		
		</div>
	  <div class="clear"></div>

<script type="text/javascript">
	$(document).ready(function(){
			$('#timepicker').timepicker({
				minuteStep:30,
				//defaultTime: '8:00 PM'
			});
			
			
			$('.input-append.date').datepicker({
				format: "dd-mm-yyyy",
				todayBtn: "linked",
				autoclose: true,
				todayHighlight: true
			});
			
			
				$(function () {
				$('#myTab a').click(function (e) {
					  e.preventDefault();
					  $(this).tab('show');  
			})
			  })
			  
		
			  
			
	})
	</script>
