<?php defined('SYSPATH') or die('No direct script access');

/**
 * Description:  
 
 *
 * @package		 quickticket
 * @subpackage 	 Helper
 * @author 		 Absalom Shu
 * @copyright    (c) 2013
 * @license 	 quickticket
 */
 
class Get_Core
{	
	
	public function all_towns(){
		$PHPprotectV19=ORM::factory('town')->orderby('name','asc')->find_all();
		return $PHPprotectV19;
	}
	
	public function agency_name($PHPprotectV3){
		$PHPprotectV146=ORM::factory('agency')->where('id',$PHPprotectV3)->find();
		return $PHPprotectV146->name;
	}
	
	public function agency_town_name($PHPprotectV3)
	{
		$PHPprotectV146 = ORM::factory('agency')->where('id',$PHPprotectV3)->find();
		$PHPprotectV147 =$PHPprotectV146->town_id;
		$PHPprotectV98 = ORM::factory('town')->where('id',$PHPprotectV147)->find();
		return $PHPprotectV98->name;
	}
	
	
	public function agency($PHPprotectV3){
		$PHPprotectV146=ORM::factory('agency')->where('id',$PHPprotectV3)->find();
		return $PHPprotectV146;
	}

	public function group_role($PHPprotectV99){
		$PHPprotectV18 = ORM::factory('admin_group')->find($PHPprotectV99)->role;
		return $PHPprotectV18;
	}
	
	
	public function all_admingroups(){
		$PHPprotectV97=ORM::factory('admin_group')->where(array('id !='=>1))->find_all();
		return $PHPprotectV97;
	}
	
	public function all_agencies(){
		$PHPprotectV96=ORM::factory('agency')->find_all();
		return $PHPprotectV96;
	}
	
	public function all_parents(){
		$PHPprotectV95=ORM::factory('parent')->find_all();
		return $PHPprotectV95;
	}
	
	public function all_agency_schedules($PHPprotectV3){
		$PHPprotectV83=ORM::factory('schedule')->where('agency_id',$PHPprotectV3)->where('deleted','0')->find_all();
		return $PHPprotectV83;
	}
	public function all_parent_schedules($PHPprotectV57){
			
		
		$PHPprotectV106 = new Database();
		$PHPprotectV148 =$PHPprotectV106->from('schedules')->join('agencies','agencies.id','schedules.agency_id')->where('agencies.parent_id',$PHPprotectV57)->where('deleted','0')->getwhere();
		return $PHPprotectV148;
	}
	
	public function all_parent_current_schedules($PHPprotectV57){
			
		
		$PHPprotectV106 = new Database();
		$PHPprotectV148 =$PHPprotectV106->from('schedules')->join('agencies','agencies.id','schedules.agency_id')->where('status','current')->where('deleted','0')->where('agencies.parent_id',$PHPprotectV57)->get();
		
		
		return $PHPprotectV148;
	}
	
	public function all_parent_departed_schedules($PHPprotectV57){
			
		
		$PHPprotectV106 = new Database();
		$PHPprotectV148 =$PHPprotectV106->select('schedules.id AS schedID','schedules.bus_number','schedules.from','schedules.to','schedules.id','schedules.checked_out_time')->from('schedules')->join('agencies','agencies.id','schedules.agency_id')->where('status','departed')->where('agencies.parent_id',$PHPprotectV57)->get();
		return $PHPprotectV148;
	}
	
	public function all_agency_current_schedules($PHPprotectV3){
		$PHPprotectV83=ORM::factory('schedule')->where('agency_id',$PHPprotectV3)->where('status','current')->where('deleted','0')->orderby('departure_date','desc')->find_all();
		return $PHPprotectV83;
	}
	public function ten_agency_current_schedules($PHPprotectV3){
		$PHPprotectV83 = ORM::factory('schedule')->where('agency_id',$PHPprotectV3)->where('status','current')->where('deleted','0')->orderby('id','desc')->limit(10)->find_all();
		return $PHPprotectV83;
	}
	
	public function all_agency_departed_schedules($PHPprotectV3){
		$PHPprotectV83=ORM::factory('schedule')->where('agency_id',$PHPprotectV3)->where('status','departed')->orderby('departure_date','desc')->find_all();
		return $PHPprotectV83;
	}
	public function ten_agency_departed_schedules($PHPprotectV3){
		$PHPprotectV83=ORM::factory('schedule')->where('agency_id',$PHPprotectV3)->where('status','departed')->orderby('id','desc')->limit(10)->find_all();
		return $PHPprotectV83;
	}
	
	public function all_agency_schedules_by_date($PHPprotectV3,$PHPprotectV26){
		$PHPprotectV83=ORM::factory('schedule')->where('agency_id',$PHPprotectV3)->where('deleted','0')->where('departure_date',$PHPprotectV26)->find_all();
		return $PHPprotectV83;
	}
	public function all_parent_schedules_by_date($PHPprotectV57,$PHPprotectV26){
		$PHPprotectV83=ORM::factory('schedule')->join('agencies','agencies.id','schedules.agency_id')->where('deleted','0')->where('departure_date',$PHPprotectV26)->where('parent_id',$PHPprotectV57)->find_all();
		return $PHPprotectV83;
	}
	public function all_parent_current_schedules_by_date($PHPprotectV57,$PHPprotectV26){
		$PHPprotectV83=ORM::factory('schedule')->join('agencies','agencies.id','schedules.agency_id')->where('parent_id',$PHPprotectV57)->where('deleted','0')->where('departure_date',$PHPprotectV26)->where('status','current')->find_all();
		return $PHPprotectV83;
	}public function all_parent_departed_schedules_by_date($PHPprotectV57,$PHPprotectV26){
		$PHPprotectV83=ORM::factory('schedule')->join('agencies','agencies.id','schedules.agency_id')->where('agencies.parent_id',$PHPprotectV57)->where('departure_date',$PHPprotectV26)->where('status','departed')->find_all();
		return $PHPprotectV83;
	}
	public function all_agency_departed_schedules_by_date($PHPprotectV3,$PHPprotectV26){
		$PHPprotectV83=ORM::factory('schedule')->where('agency_id',$PHPprotectV3)->where('status','departed')->where('departure_date',$PHPprotectV26)->orderby('id','desc')->find_all();
		return $PHPprotectV83;
	}
	public function all_agency_current_schedules_by_date($PHPprotectV3,$PHPprotectV26){
		$PHPprotectV83=ORM::factory('schedule')->where('agency_id',$PHPprotectV3)->where('status','current')->where('deleted','0')->where('departure_date',$PHPprotectV26)->orderby('id','desc')->find_all();
		return $PHPprotectV83;
	}
	
	public function schedule($PHPprotectV149){
		$PHPprotectV27=ORM::factory('schedule')->where('id',$PHPprotectV149)->where('deleted','0')->find();
		return $PHPprotectV27;
	}
	
	public function town($PHPprotectV149){
		$PHPprotectV98=ORM::factory('town')->where('id',$PHPprotectV149)->find();
		return $PHPprotectV98->name;
	}
	
	
	public function _parent($PHPprotectV3){
		$PHPprotectV146 = ORM::factory('agency')->where('id',$PHPprotectV3)->find();
		$PHPprotectV57 = $PHPprotectV146->parent_id;
		$PHPprotectV23 = ORM::factory('parent')->where('id',$PHPprotectV57)->find();
		return $PHPprotectV23;
	}
	
	public function _parent_name($PHPprotectV3){
		$PHPprotectV146 = ORM::factory('agency')->where('id',$PHPprotectV3)->find();
		$PHPprotectV57 = $PHPprotectV146->parent_id;
		$PHPprotectV23 = ORM::factory('parent')->where('id',$PHPprotectV57)->find();
		return $PHPprotectV23->name;
	}
	
	public function admins_town($PHPprotectV13){
	
		$PHPprotectV150 = ORM::factory('agency')->where('id',$PHPprotectV13)->find();
		$PHPprotectV151 = $PHPprotectV150->town_id;
		
		$PHPprotectV81 = ORM::factory('town')->where('id',$PHPprotectV151)->find();
		return $PHPprotectV81;
	
	}
	public function agency_and_town($PHPprotectV3){
	
		$PHPprotectV146 = ORM::factory('agency')->where('id',$PHPprotectV3)->find();
		$PHPprotectV57 = $PHPprotectV146->parent_id;
		$PHPprotectV23 = ORM::factory('parent')->where('id',$PHPprotectV57)->find();
		
		$PHPprotectV147 = $PHPprotectV146->town_id;
		$PHPprotectV98 = ORM::factory('town')->where('id',$PHPprotectV147)->find();
		
		return $PHPprotectV23->name ." ". $PHPprotectV98->name;
	}
	public function client_reservations($PHPprotectV149){
		$PHPprotectV152 = ORM::factory('reservation_request')->where('schedule_id',$PHPprotectV149)->find();
		return $PHPprotectV152;
	}
	public function all_agency_parcels($PHPprotectV3){
		$PHPprotectV22=ORM::factory('parcel')->where('agency_id',$PHPprotectV3)->orderby('sent_date','DESC')->find_all();
		return $PHPprotectV22;
	}
	
	/*Gets from the same db. This works for test purposes only
	public function all_agency_incoming_parcels($PHPprotectV3)
	{
		$PHPprotectV22=ORM::factory('parcel')->where('to',$PHPprotectV3)->orderby('sent_date','DESC')->find_all();
		return $PHPprotectV22;
	} 
	public function ten_agency_incoming_parcels($PHPprotectV3)
	{
		$PHPprotectV22=ORM::factory('parcel')->where('to',$PHPprotectV3)->orderby('sent_date','DESC')->limit(10)->find_all();
		return $PHPprotectV22;
	}
	*/
	
	public function all_agency_incoming_parcels($PHPprotectV3)
	{
		$PHPprotectV22=ORM::factory('incoming_parcel')->where('to',$PHPprotectV3)->orderby('sent_date','DESC')->find_all();
		return $PHPprotectV22;
	}
	
	public function ten_agency_incoming_parcels($PHPprotectV3)
	{
		$PHPprotectV22=ORM::factory('incoming_parcel')->where('to',$PHPprotectV3)->orderby('sent_date','DESC')->limit(10)->find_all();
		return $PHPprotectV22;
	}
	
	public function all_agency_sent_parcels($PHPprotectV3){
		$PHPprotectV22=ORM::factory('parcel')->where('from',$PHPprotectV3)->orderby('sent_date','DESC')->find_all();
		return $PHPprotectV22;
	}
	
	public function all_agency_parcels_by_date($PHPprotectV3,$PHPprotectV26){
		$PHPprotectV22=ORM::factory('parcel')->where('from',$PHPprotectV3)->where('sent_date',$PHPprotectV26)->find_all();
		return $PHPprotectV22;
	}
	
	
	public function agency_parcels_by_period($PHPprotectV3,$PHPprotectV126,$PHPprotectV127)
	{
		$PHPprotectV106 = new Database();
		$PHPprotectV22 = $PHPprotectV106->query("SELECT * FROM parcels WHERE agency_id= ".$PHPprotectV3." AND sent_date BETWEEN CAST('$PHPprotectV126' AS DATE) AND CAST('$PHPprotectV127' AS DATE) ORDER BY sent_date ASC");
		return $PHPprotectV22;
	}
	
	public function parent_parcels_by_period($PHPprotectV57,$PHPprotectV126,$PHPprotectV127)
	{
		$PHPprotectV106 = new Database();
		$PHPprotectV22 = $PHPprotectV106->query("SELECT * FROM parcels JOIN agencies ON agencies.id = parcels.agency_id WHERE parent_id= ".$PHPprotectV57." AND sent_date BETWEEN CAST('$PHPprotectV126' AS DATE) AND CAST('$PHPprotectV127' AS DATE) ORDER BY sent_date ASC");
		return $PHPprotectV22;
	}
	
	public function agency_expenses_by_period($PHPprotectV3,$PHPprotectV126,$PHPprotectV127)
	{
		$PHPprotectV106 = new Database();
		$PHPprotectV145 = $PHPprotectV106->query("SELECT * FROM expenses WHERE agency_id= ".$PHPprotectV3." AND deleted = '0' AND date_incurred BETWEEN CAST('$PHPprotectV126' AS DATE) AND CAST('$PHPprotectV127' AS DATE) ORDER BY date_incurred ASC");
		return $PHPprotectV145;
	}
	public function parent_expenses_by_period($PHPprotectV57,$PHPprotectV126,$PHPprotectV127)
	{
		$PHPprotectV106 = new Database();
		$PHPprotectV145 = $PHPprotectV106->query("SELECT * FROM expenses JOIN agencies ON agencies.id = expenses.agency_id WHERE parent_id= ".$PHPprotectV57." AND deleted = '0' AND date_incurred BETWEEN CAST('$PHPprotectV126' AS DATE) AND CAST('$PHPprotectV127' AS DATE) ORDER BY date_incurred ASC");
		return $PHPprotectV145;
	}
	
	
	public function agency_schedules_by_period($PHPprotectV3,$PHPprotectV126,$PHPprotectV127)
	{
		$PHPprotectV106 = new Database();
		$PHPprotectV83 = $PHPprotectV106->query("SELECT * FROM schedules WHERE agency_id= ".$PHPprotectV3." AND deleted = '0' AND departure_date BETWEEN CAST('$PHPprotectV126' AS DATE) AND CAST('$PHPprotectV127' AS DATE) AND status = 'departed' ORDER BY departure_date ASC");
		return $PHPprotectV83;
	}
	
	public function parent_schedules_by_period($PHPprotectV57,$PHPprotectV126,$PHPprotectV127)
	{
		$PHPprotectV106 = new Database();
		$PHPprotectV83 = $PHPprotectV106->query("SELECT * FROM schedules JOIN agencies ON agencies.id = schedules.agency_id WHERE parent_id= ".$PHPprotectV57." AND deleted = '0' AND departure_date BETWEEN CAST('$PHPprotectV126' AS DATE) AND CAST('$PHPprotectV127' AS DATE) ORDER BY departure_date ASC");
		return $PHPprotectV83;
	}
	
	
	public function agency_activity_by_period($PHPprotectV3,$PHPprotectV126,$PHPprotectV127){
		$PHPprotectV106 = new Database();
		$PHPprotectV153 = $PHPprotectV106->query("SELECT * FROM parcels WHERE agency_id= ".$PHPprotectV3." AND sent_date BETWEEN CAST('$PHPprotectV126' AS DATE) AND CAST('$PHPprotectV127' AS DATE)
							   UNION SELECT * FROM expenses WHERE agency_id= ".$PHPprotectV3." AND date_incurred BETWEEN CAST('$PHPprotectV126' AS DATE) AND CAST('$PHPprotectV127' AS DATE)
							   UNION SELECT * FROM schedules WHERE agency_id= ".$PHPprotectV3." AND departure_date BETWEEN CAST('$PHPprotectV126' AS DATE) AND CAST('$PHPprotectV127' AS DATE) AND status = 'departed' ORDER BY departure_date ASC
					");
		return $PHPprotectV153;
	}
	
	public function all_parent_parcels_by_date($PHPprotectV57,$PHPprotectV26){
		
		$PHPprotectV22=ORM::factory('parcel')->join('agencies','agencies.id','parcels.agency_id')->where('parent_id',$PHPprotectV57)->where('sent_date',$PHPprotectV26)->find_all();
		return $PHPprotectV22;
	}
	
	
	public function agency_siblings($PHPprotectV3,$PHPprotectV23){
		
		$PHPprotectV22=ORM::factory('agency')->where('id !=',$PHPprotectV3)->where('parent_id',$PHPprotectV23)->find_all();
		return $PHPprotectV22;
	}
	public function all_children($PHPprotectV57){
		$PHPprotectV154=ORM::factory('agency')->where('parent_id',$PHPprotectV57)->find_all();
		return $PHPprotectV154;
	}
	
	
	public function parcel($PHPprotectV155)
	{
		$PHPprotectV156 = ORM::factory('parcel')->find($PHPprotectV155);
		return $PHPprotectV156;
	}
	
	
	public function incoming_parcel($PHPprotectV155)
	{
		$PHPprotectV156 = ORM::factory('incoming_parcel')->find($PHPprotectV155);
		return $PHPprotectV156;
	} 

	public function schedule_status($PHPprotectV39){
		
		$PHPprotectV27 = ORM::factory('schedule')->where('id',$PHPprotectV39)->find();
		return $PHPprotectV27->status;
	}
		
	public function expense($PHPprotectV142){
		$PHPprotectV156 = ORM::factory('expense')->where('deleted','0')->find($PHPprotectV142);
		return $PHPprotectV156;
	}
	
	public function expenses_for_bus($PHPprotectV32){
	
		$PHPprotectV144 = ORM::factory('expense')->where('bus_number',$PHPprotectV32)->where('deleted','0')->find_all();		
		return $PHPprotectV144;
	}
	
	
	public function expenses_for_bus_and_date($PHPprotectV3,$PHPprotectV32,$PHPprotectV26)
	{
	
		$PHPprotectV157 = ORM::factory('expense')->where('agency_id',$PHPprotectV3)->where('bus_number',$PHPprotectV32)->where('deleted','0')->where('date_incurred',$PHPprotectV26)->find_all();		
		return $PHPprotectV157;
	}
	public function expenses_for_schedule($PHPprotectV3,$PHPprotectV39)
	{
	
		$PHPprotectV157 = ORM::factory('expense')->where('agency_id',$PHPprotectV3)->where('schedule_id',$PHPprotectV39)->find_all();		
		return $PHPprotectV157;
	}
	
	public function agency_expenses_by_date($PHPprotectV3,$PHPprotectV26){
		$PHPprotectV145 = ORM::FACTORY('expense')->where('agency_id',$PHPprotectV3)->where('date_incurred',$PHPprotectV26)->where('deleted','0')->find_all();		
		return $PHPprotectV145;
	}
	
	public function parent_expenses_by_date($PHPprotectV57,$PHPprotectV26)
	{
		$PHPprotectV145 = ORM::FACTORY('expense')->join('agencies','agencies.id','expenses.agency_id')->where('parent_id',$PHPprotectV57)->where('deleted','0')->where('date_incurred',$PHPprotectV26)->find_all();		
		return $PHPprotectV145;
	}
	
	public function company($PHPprotectV149){
		$PHPprotectV158 = ORM::factory('parent')->where('id',$PHPprotectV149)->find();
		return $PHPprotectV158;
	}
	
	public function company_logo($PHPprotectV159){
		
		$PHPprotectV158 = ORM::factory('parent')->where('id',$PHPprotectV159)->find();
		
		$PHPprotectV118 = "images/ticket/agency-logos/" .$PHPprotectV158->name .".jpg";
		$PHPprotectV118 = strtolower(str_replace(" ",'', $PHPprotectV118));
		
		
		if(file_exists($PHPprotectV118)){
			return $PHPprotectV118;
		}else{ 
			return "images/ticket/agency-logos/sample_logo.jpg";
		}
	}
	public function agency_logo($PHPprotectV3)
	{
		
		$PHPprotectV146 = ORM::factory('agency')->where('id',$PHPprotectV3)->find();
		
		$PHPprotectV118 = "images/ticket/agency-logos/" .$PHPprotectV146->name .".jpg";
		$PHPprotectV118 = strtolower(str_replace(" ",'', $PHPprotectV118));
		
		
		if(file_exists($PHPprotectV118)){
			return $PHPprotectV118;
		}else{ 
			return "images/ticket/agency-logos/sample_logo.jpg";
		}
	}
	
	public function bus_photo($PHPprotectV32)
	{
		
		$PHPprotectV146 = ORM::factory('bus')->where('bus_number',$PHPprotectV32)->find();
		
		$PHPprotectV116 = "images/buses/" .$PHPprotectV32 .".jpg";
		
		
		if(file_exists($PHPprotectV116)){
			return $PHPprotectV116;
		}else{ 
			return "images/ticket/agency-logos/sample_logo.jpg";
		}
	}
	
	
	public function agency_ticket_background($PHPprotectV3)
	{
		
		$PHPprotectV146 = ORM::factory('agency')->where('id',$PHPprotectV3)->find();
		
		$PHPprotectV118 = "images/ticket/" ."ticket.".$PHPprotectV146->name .".jpg";
		$PHPprotectV118 = strtolower(str_replace(" ",'', $PHPprotectV118));
		
		
		if(file_exists($PHPprotectV118)){
			return $PHPprotectV118;
		}else{ 
			return "images/ticket/agency-logos/sample_logo.jpg";
		}
	}
	
	
	public function all_company_buses($PHPprotectV159){
		$PHPprotectV160=ORM::factory('bus')->where('agency_id',$PHPprotectV57)->where('deleted','0')->find_all();
		return $PHPprotectV160;
	}
	

	
	
	
	
	
	
	
	public function all_buses($PHPprotectV3, $PHPprotectV57, $PHPprotectV99=3)
	{	
		/*$PHPprotectV106=new Database();
		if($PHPprotectV99 ==4){
			
			$PHPprotectV102 = $PHPprotectV106->from('buses')->join('agencies','agencies.id','buses.agency_id')->where('agencies.parent_id',$PHPprotectV57)->where('buses.deleted',0)->get();
			var_dump($PHPprotectV102);exit;
		}else{  */
			
			$PHPprotectV102 = ORM::factory('bus')->where('agency_id',$PHPprotectV3)->where('deleted','0')->find_all();
			
		
		return $PHPprotectV102;
	}
	
	
	public function all_expenses($PHPprotectV3, $PHPprotectV57, $PHPprotectV99=3)
	{
		if($PHPprotectV99 ==4){
			$PHPprotectV145=ORM::factory('expense')->join('agencies','agencies.id','expenses.agency_id')->where('parent_id',$PHPprotectV57)->where('deleted','0')->orderby('date_incurred','DESC')->find_all();
		}else{
			$PHPprotectV145=ORM::factory('expense')->where('agency_id',$PHPprotectV3)->where('Deleted','0')->orderby('date_incurred','DESC')->find_all();
		}
		return $PHPprotectV145;
	}
	
	public function bus($PHPprotectV32){
		$PHPprotectV107=ORM::factory('bus')->where('bus_number',$PHPprotectV32)->find();
		return $PHPprotectV107;
	}
	
	public function vip_bus($PHPprotectV32){
		$PHPprotectV107=ORM::factory('bus')->where('bus_number',$PHPprotectV32)->find();
		return $PHPprotectV107->vip;
	}
	
	public function agency_admins($PHPprotectV3){
		$PHPprotectV161=ORM::factory('admin')->where('agency_id',$PHPprotectV3)->where('deleted','0')->find_all();
		return $PHPprotectV161;
	}
	
	public function unpushed_transactions(){
		
		
		$PHPprotectV162 = ORM::factory('transaction')->where('online','0')->orderby('change_time','ASC')->find_all();	
		
		return $PHPprotectV162;
	}
	
	public function unpushed_transactions_parcels(){
		
		$PHPprotectV163 = ORM::factory('transactions_parcel')->where('online','0')->orderby('change_time','DESC')->find_all();	
		return $PHPprotectV163;
	}
	public function admin($PHPprotectV164){
		$PHPprotectV12 = ORM::factory('admin')->where('id',$PHPprotectV164)->find();	
		return $PHPprotectV12;
	}
}
