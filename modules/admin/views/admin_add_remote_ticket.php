<?php defined('SYSPATH') or die('No direct script access'); ?> 


	<?php
		//determine what type of notice to display if at all
		$notice = $this->session->get_once('notice');
			if(!empty($notice)){ 
				if($notice['type'] == 'success'){?><div class="alert general success"><a class="close" data-dismiss="alert" href="#">&times;</a><?php echo $notice['message']; }?></div>
			<?	if($notice['type'] == 'error'){?><div class="alert general error"><a class="close" data-dismiss="alert" href="#">&times;</a><?php echo $notice['message']; }?></div>
		<?}?>
    <div class="container">
	  	
	<div class="row-fluid">	
		
		<div class="span12  hero-unit prof-unit">
		
		<ul class="nav nav-tabs">
					<li > <a href="<?=url::site('admin/remote_tickets')?>"><i class="icon-arrow-left"></i> <?=Kohana::lang('backend.back_remote_tickets')?></a></li>
		</ul>
		<form action="<?=url::site('admin/add_remote_ticket/')?>" method="POST">
		<div class="row-fluid span8 offset2 print-area">	
		<legend>Add remote ticket</legend>
							
			<div>
				<div class="span4 text-right"><?=Kohana::lang('backend.from')?>: <span class="red"> *</span> </div>
				<div class="span8 input-append">
					<select name="remote_agency_from" id="inputType">
						<?php foreach ($siblings as $sib):?>
						<option value="<?=$sib->id?>"><?=$sib->name?></option>
						<?php endforeach;?>
					</select>				</div>
			</div>	
			<div>
				<div class="span4 text-right"><?=Kohana::lang('backend.to')?>: <span class="red"> *</span> </div>
				<div class="span8 input-append">
					<select name="remote_agency_to" id="inputType">
						<?php foreach ($all_children as $child):?>
						<option value="<?=$child->id?>"><?=$child->name?></option>
						<?php endforeach;?>
					</select>				</div>
			</div>	
			<div>
				<div class="span4 text-right"><?=Kohana::lang('backend.name')?>: <span class="red"> *</span> </div>
				<div class="span8 input-append">
					<input type="text"  name="name" value="<?=@$post['name']?>" required placeholder="<?=Kohana::lang('backend.passenger_name')?>"/>	
				</div>
			</div>
			<div>
				<div class="span4 text-right"><?=Kohana::lang('backend.phone')?>:  </div>
				<div class="span8 input-append">
					<input type="text"  name="phone" value="<?=@$post['phone']?>"  />	
				</div>
			</div>
			<div>
				<div class="span4 text-right"><?=Kohana::lang('backend.email')?>: <span class="red"> </span> </div>
				<div class="span8 input-append">
					<input type="text"  name="email" value="<?=@$post['email']?>">
				</div>
			</div>		
			<div>
				<div class="span4 text-right"><?=Kohana::lang('backend.id_card')?>: <span class="red"> </span> </div>
				<div class="span8 input-append">
					<input type="text"   name="idc" value="<?=@$post['idc']?>">
				</div>
			</div>	
			<div>
				<div class="span4 text-right"><?=Kohana::lang('backend.price')?>: <span class="red">* </span> </div>
				<div class="span8 input-append">
					<input type="text"   name="ticket_price" value="<?=@$post['ticket_price']?>" required>
				</div>
			</div>			
			
						
						
			<div class="clear"></div>
			<div class="form-actions">
				<div class="span4"></div>
				<div class="span8">
					<button type="submit" class="btn btn-info"><?=Kohana::lang('backend.save')?></button>
					<a href="<?=url::site('admin/tickets_pending')?>" class="btn" type="submit"> <?=Kohana::lang('backend.cancel')?></a>
				</div>
			</div>		
						
		</div>
		</form>
		
		</div>
	
    </div> 

	</div>
	
	<script type="text/javascript">

	
	</script>