<?php defined('SYSPATH') or die('No direct script access');

class Main_Module_Model extends ORM{

	public function get_all()
	{

		$main_modules=ORM::factory('main_module')->find_all();
	
		return $main_modules;
		
	}
	
	public function get_agency_schedules_by_period_by_admin($agency_id,$start_date,$end_date, $admin)
	{
		$db = new Database();
		$schedules = $db->query("SELECT * FROM schedules WHERE agency_id= ".$agency_id." AND CreatedBy= '".$admin."'  AND deleted = '0' AND departure_date BETWEEN CAST('$start_date' AS DATE) AND CAST('$end_date' AS DATE) AND status = 'departed' ORDER BY departure_date ASC");
		return $schedules;
	}


}
