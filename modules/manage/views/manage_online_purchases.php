<?php defined('SYSPATH') or die('No direct script access'); ?> 


	  
	<div class="row-fluid marketing" style="margin:5px 0 0px 0;">	
		<div class="span12 hero-unit prof-unit reload" style="margin-left:0px;">
	

				<legend><?=Kohana::lang('backend.purchases_online')?><div class='view'></div></legend>
				
				<table class="table table-striped table-hover table-condensed">
					<tr>
						<th><?=Kohana::lang('backend.purchase_date')?> </th>
						<th><?=Kohana::lang('backend.name')?> </th>
						<th><?=Kohana::lang('backend.phone')?> </th>
						<th><?=Kohana::lang('backend.idc')?> </th>
						<th><?=Kohana::lang('backend.schedule_requested')?> </th>
						<th><?=Kohana::lang('backend.scheduleID')?> </th>
						<th><?=Kohana::lang('backend.amount_paid')?> </th>
						
					</tr>
					<?php foreach ($online_purchases as $purchase){ 
					?>
					<tr>
						<td><?=date("d-m-Y",strtotime($purchase->CreatedOn))?></td>						
						<td><?=$purchase->client_name?></td>
						<td><?=$purchase->client_phone?></td>
						<td><b><?=$purchase->client_idc?></b></td>
						<?php 
						//Only if a particular schedule was requested i.e. NOT open ticket
						if($purchase->schedule_id){
						//Since it's the global schedule id, the local id excludes the 1st 3 chars which are the agency id 
						
							$schedule=get::schedule($purchase->schedule_id); ?>
						
						<td><?=$schedule->bus_number?> : <?=get::town($schedule->from)?> --> <b><?=get::town($schedule->to)?></b> : <b><?=date("g:i A", strtotime($schedule->departure_time))?></td>
						<td><?="SC".$purchase->schedule_id?></td>
						<?php }else{ ?>
						
						<td><?=get::town($purchase->town_from)?> --> <?=get::town($purchase->town_to)?> : <b>Anytime</b></td>
						<td></td>
						<?php }?>
						<td><?=number_format($purchase->amount_paid)?></td>
						
						
						
						
					</tr>
					<?php 
					
					} ?>
				</table>
				
			
					<?php echo $this->pagination;?>
<?php //$this->profiler = new Profiler();?>
		</div>
	
		
		</div>

