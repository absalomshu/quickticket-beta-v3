<?php defined('SYSPATH') or die('No direct script access');

class Purchase_Model extends ORM{

	public function add_purchase($agency_id, $schedule_id, $client_name, $client_phone, $client_idc, $client_email, $amount_paid, $town_from, $town_to, $ticket_name, $ticket_price, $is_open_ticket, $created_by, $created_on)
	{	
		$purchase = ORM::FACTORY('purchase');
		$purchase->agency_id = $agency_id;
		$purchase->schedule_id = $schedule_id;
		$purchase->client_name = $client_name;
		$purchase->client_phone = $client_phone;
		$purchase->client_idc = $client_idc;
		$purchase->client_email = $client_email;
		$purchase->amount_paid = $amount_paid;
		$purchase->town_from = $town_from;
		$purchase->town_to = $town_to;
		$purchase->ot_ticket_name = $ticket_name;
		$purchase->ot_ticket_price = $ticket_price;
		$purchase->is_open_ticket = $is_open_ticket;
		$purchase->status = 'initiated';
		$purchase->CreatedBy = $created_by;
		$purchase->CreatedOn = $created_on;
		
		$purchase->save();
		
		return $purchase->id;
	}
	
	public function edit_purchase($purchase_id, $agency_id, $schedule_id, $client_name, $client_phone, $client_idc, $client_email, $amount_paid, $town_from, $town_to, $ticket_name, $ticket_price, $modified_by, $modified_on, $status = 'initiated')
	{	
		$purchase = ORM::FACTORY('purchase')->where('id',$purchase_id)->find();
		$purchase->agency_id = $agency_id;
		$purchase->schedule_id = $schedule_id;
		$purchase->client_name = $client_name;
		$purchase->client_phone = $client_phone;
		$purchase->client_idc = $client_idc;
		$purchase->client_email = $client_email;
		$purchase->amount_paid = $amount_paid;
		$purchase->town_from = $town_from;
		$purchase->town_to = $town_to;
		$purchase->ot_ticket_name = $ticket_name;
		$purchase->ot_ticket_price = $ticket_price;
		$purchase->status = $status;
		$purchase->ModifiedBy = $modified_by;
		$purchase->ModifiedOn = $modified_on;
		
		$purchase->save();
		
		return 'Succesfully updated';
	}
	

	
	public function get($purchase_id)
	{
		$purchase=ORM::factory('purchase')->where('id',$purchase_id)->find();
		return $purchase;	
	}
	
	public function mark_active($purchase_id,$amount_payable)	
	{
		$purchase=ORM::factory('purchase')->where('id',$purchase_id)->find();
		$purchase->status='active';
		$purchase->amount_paid=$amount_payable;
		$purchase->save();
		
	}
	
	public function get_full_details($purchase_id)
	{
		$purchase = $this->db->select('purchases.id as purchase_id, schedules.id as schedule_id,schedules.agency_id,client_name,client_phone,client_email,client_idc,bus_seats,from,to,ticket_price,departure_date,departure_time')->from('purchases')->join('schedules','schedules.id=purchases.schedule_id')->where('purchases.id',$purchase_id)->get();		
		/*$purchase = $this->db->query("SELECT `schedules`.`id`, `client_name`, `client_phone`, `client_email`, `client_idc`, `bus_seats`, `from`, `to`, `ticket_price`, `departure_date`, `departure_time`
						FROM `purchases`
						JOIN `schedules` ON (schedules.id=purchases.schedule_id)
						WHERE `purchases`.`id` = '11'"); */
		//$purchase = ORM::factory('purchase')->join('schedules','schedules.id','purchases.schedule_id')->where('purchases.id',$purchase_id)->find();
		
		return $purchase;	
	}
	//This is different as open tickets don't have a schedule_id, which is what is used to join the 2 tables above.
	public function get_full_ot_details($purchase_id)
	{
		//$purchase = $this->db->select('schedules.id,schedules.agency_id,client_name,client_phone,client_email,client_idc,bus_seats,from,to,ticket_price,departure_date,departure_time')->from('purchases')->join('schedules','schedules.id=purchases.schedule_id')->where('purchases.id',$purchase_id)->get();		
		$purchase = ORM::factory('purchase')->where('purchases.id',$purchase_id)->find();
		
		return $purchase;	
	}
	
	public function get_all($agency_id)
	{
		$purchases=ORM::factory('purchase')->where('agency_id',$agency_id)->find_all();
		return $purchases;	
	}

	public function is_open_ticket($purchase_id)
	{
		$purchase=ORM::factory('purchase')->where('id',$purchase_id)->find();
		
		return $purchase->is_open_ticket;
		
	}
	
	

}
