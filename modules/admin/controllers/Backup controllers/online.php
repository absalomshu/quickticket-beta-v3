<?php defined('SYSPATH') or die('No direct script access');

/**
 * Description:  
 
 *
 * @package		 quickticket
 * @subpackage 	 Helper
 * @author 		 Absalom Shu
 * @copyright    (c) 2013
 * @license 	 quickticket
 */
//prevent the script from crashing after the default 30 seconds max execution time
ini_set("max_execution_time", "600000"); 
 
class Online_Controller extends Admin_Controller
{	
	//Requires an internet connection NECESSARILY!
	private $onlinedb;
	private $offlinedb;
	
	public function __construct()
	{		
		$this->admin = Authlite::instance()->get_admin();
		$this->session = Session::instance();
	
		if (!empty($this->admin)) 
		{
			$this->agency_id = $this->admin->agency_id;
		}
	}
	
	public function index()
	{	
		//establish connection to online database
		$this->onlinedb = new Database('online_db');
		$this->offlinedb = new Database('offline_db');
		
		//get unpushed schedules and parcels
		$unpushed_transactions=get::unpushed_transactions();
		//$unpushed_transactions_parcels=get::unpushed_transactions_parcels();
		//$this->push_transactions doesn't work here as it instead gives Transactions_controller::push_transactions.
		$push_result = $this->push_transactions($unpushed_transactions);
		echo $push_result;	}
	
	
	//takes the transaction resource
	public function push_transactions($unpushed_transactions)
	{	
		//instantiate, if not the if statement toward the end of this foreach sometimes gives an error
		$expense = 0; $bus = 0; $parcel = 0; $schedule = 0;
		foreach($unpushed_transactions as $trx)
		{
			/*
			*
			*FOR SOME UNKNOWN REASON,THE WRONG AGENCY ID IS POSTED WHEN UPDATING PARCELS. HENCE THE 'FROM' IS USED
			
			DO NOT USE $this->anything IN THIS CONTROLLOER. VALUES GET MIXED UP. USED THE VALUE FROM trx
			*/
			
			//ORM removed here as it might be troublesome to pick another db and table with it.
			
			//var_dump($trx);echo"<br/><br/><br/><br/>";continue;
			//check the table it's coming from to know the table to post it to, as all transactions come to one table
			if($trx->table=='schedules'){
				$entry_exists = $this->onlinedb->count_records ('schedules',array('id'=>$trx->agency_id.$trx->change_id,'agency_id'=>$this->admin->agency_id)); 
				
				//if entry exists online, update else insert
				if($entry_exists){
				$schedule = $this->onlinedb->update('schedules',array(
							
							'agency_id' => $trx->agency_id,
							'bus_number' => $trx->bus_number,
							'bus_seats' => $trx->bus_seats,
							'from' => $trx->from,
							'to' => $trx->to,
							'ticket_price' => $trx->ticket_price,
							'seat_occupants' => $trx->seat_occupants,
							'reserved_seats' => $trx->reserved_seats,
							'status' => $trx->status,
							'loading' => $trx->loading,
							'departure_date' => $trx->departure_date,
							'departure_time' => $trx->departure_time,
							'checked_out_time' => $trx->checked_out_time,
							'total_amount' => $trx->total_amount,			
							'CreatedBy' => $trx->CreatedBy,			
							'CreatedOn' => $trx->CreatedOn,			
							'CheckedOutBy' => $trx->CheckedOutBy,			
							'deleted' => $trx->deleted			
							), array('id'=>$trx->agency_id.$trx->change_id,'agency_id'=>$this->admin->agency_id));
				}else{
					$schedule = $this->onlinedb->insert('schedules',array(
							
							'id' => $trx->agency_id.$trx->change_id,
							'agency_id' => $trx->agency_id,
							'bus_number' => $trx->bus_number,
							'bus_seats' => $trx->bus_seats,
							'from' => $trx->from,
							'to' => $trx->to,
							'ticket_price' => $trx->ticket_price,
							'seat_occupants' => $trx->seat_occupants,
							'reserved_seats' => $trx->reserved_seats,
							'status' => $trx->status,
							'loading' => $trx->loading,
							'departure_date' => $trx->departure_date,
							'departure_time' => $trx->departure_time,
							'checked_out_time' => $trx->checked_out_time,
							'total_amount' => $trx->total_amount,
							'CreatedBy' => $trx->CreatedBy,			
							'CreatedOn' => $trx->CreatedOn,			
							'CheckedOutBy' => $trx->CheckedOutBy,
							'deleted' => $trx->deleted	
							));
				
				}
							
			}elseif($trx->table=='buses'){
				//if that agency already created that bus, just update. Else create the entry where(array('id' => 5, 'title' => 'Demo'))
				$entry_exists = $this->onlinedb->count_records ('buses',array('bus_number'=>$trx->bus_number,'agency_id'=>$this->admin->agency_id)); 
				if($entry_exists){
				//var_dump($entry_exists);
					//update the column where the bus number(which is the id in this case) and the ageny id coincide.
					$bus = $this->onlinedb->update('buses',array(
							'agency_id' => $trx->agency_id,
							'bus_number' => $trx->bus_number,
							'bus_seats' => $trx->bus_seats,
							'driver' => $trx->driver,
							'deleted' => $trx->deleted	
					), array('bus_number'=>$trx->bus_number,'agency_id'=>$this->admin->agency_id));
					
					//var_dump(count($bus));exit;
				}else{				
				$bus = $this->onlinedb->insert('buses',array(
							
							'id' => $trx->agency_id.$trx->change_id,
							'agency_id' => $trx->agency_id,
							'bus_number' => $trx->bus_number,
							'bus_seats' => $trx->bus_seats,
							'driver' => $trx->driver,
							'deleted' => $trx->deleted		
							));}
							
			}elseif($trx->table=='expenses'){
				$entry_exists = $this->onlinedb->count_records ('expenses',array('id'=>$trx->agency_id.$trx->change_id,'agency_id'=>$this->admin->agency_id)); 
				if($entry_exists){
				$expense = $this->onlinedb->update('expenses',array(
							
							'agency_id' => $trx->agency_id,
							'bus_number' => $trx->bus_number,
							'schedule_id' => $trx->agency_id.$trx->schedule_id,
							'amount' => $trx->amount,
							'purpose' => $trx->purpose,
							'date_incurred' => $trx->date_incurred,
							'authorised_by' => $trx->authorised_by,
							'deleted' => $trx->deleted	
							), array('id'=>$trx->agency_id.$trx->change_id,'agency_id'=>$this->admin->agency_id));
							
			}else{
					$expense = $this->onlinedb->insert('expenses',array(
							'id' => $trx->agency_id.$trx->change_id,
							'agency_id' => $trx->agency_id,
							'bus_number' => $trx->bus_number,
							'schedule_id' => $trx->agency_id.$trx->schedule_id,
							'amount' => $trx->amount,
							'purpose' => $trx->purpose,
							'date_incurred' => $trx->date_incurred,
							'authorised_by' => $trx->authorised_by,
							'deleted' => $trx->deleted	
							));}

			}elseif($trx->table=='parcels'){
				$entry_exists = $this->onlinedb->count_records ('parcels',array('id'=>$trx->agency_id.$trx->change_id,'agency_id'=>$this->admin->agency_id)); 
				if($entry_exists){
				$parcel = $this->onlinedb->update('parcels',array(
							
							'agency_id' => $trx->from,
							'receiver_name' => $trx->receiver_name,
							'receiver_phone' => $trx->receiver_phone,
							'collected_by' => $trx->collected_by,
							'collected_on' => $trx->collected_on,
							'description' => $trx->description,
							'from' => $trx->from,
							'to' => $trx->to,
							'price' => $trx->price,
							'sent_date' => $trx->sent_date,
							'state' => $trx->parcel_state,
							'CreatedBy' => $trx->CreatedBy,			
							'CreatedOn' => $trx->CreatedOn
							), array('id'=>$trx->agency_id.$trx->change_id,'agency_id'=>$this->admin->agency_id));
			}else{
				
				$parcel = $this->onlinedb->insert('parcels',array(
							'id' => $trx->agency_id.$trx->change_id,
							'agency_id' => $trx->from,
							'receiver_name' => $trx->receiver_name,
							'receiver_phone' => $trx->receiver_phone,
							'collected_by' => $trx->collected_by,
							'collected_on' => $trx->collected_on,
							'description' => $trx->description,
							'from' => $trx->from,
							'to' => $trx->to,
							'price' => $trx->price,
							'sent_date' => $trx->sent_date,
							'state' => $trx->parcel_state,			
							'CreatedBy' => $trx->CreatedBy,			
							'CreatedOn' => $trx->CreatedOn			
							));
			}
			}			
						
						//change the online state to 1, only if query was successfully executed
						//This IF is in an attempt to prevent uploading next row if theres a problem with the current one.
						//Else later, the earlier transaction will overwrite the later one, hence cancelling changes.
						if($expense or $bus or $schedule or $parcel)
						{	
							//Pick out the transaction from the transactions table and ENSURE that 'online' is set to 1 once done,
							//so next time, it's not uploaded again
							//IDentification is by time and agencyid
							$updatetrx = $this->offlinedb->update('transactions',array('online'=>'1'),array('change_time' => $trx->change_time, 'agency_id' => $trx->agency_id));
						}
					/*	else
						{	//halt execution in case of error. Problem is NOTHING else will be pushed online.
							//SHOULD LOOK FOR A WAY TO SKIP AND CONTINUE.
							return 'Error. Some transaction blocking. Error:'.$trx->table. ':'.$trx->change_id;
						} */
		} return "Everything pushed online";	
	}
	
	public function pull_parcels()
	{
		$onlinedb = new Database('online_db');
		$offlinedb = new Database('offline_db');
		
		//Download parcels that are destined for this branch
		$unpulled_parcels = $onlinedb->from('parcels')->where('to' , $this->agency_id)->where('downloaded','0')->getwhere();
		
		$i=0;
		foreach($unpulled_parcels as $parcel)
		{	//the composite parcel id is a combination of the originating agency id and the parcel id
			$pull = $offlinedb->insert('incoming_parcels',array(
										
										'agency_id' => $parcel->agency_id,
										'global_parcel_id' => $parcel->id,
										'receiver_name' => $parcel->receiver_name,
										'receiver_phone' => $parcel->receiver_phone,
										'description' => $parcel->description,
										'from' => $parcel->from,
										'to' => $parcel->to,
										'price' => $parcel->price,
										'sent_date' => $parcel->sent_date,
										'state' => $parcel->state,
										'CreatedBy' => $parcel->CreatedBy,
										'CreatedOn' => $parcel->CreatedOn
										));
			$updatesms = $onlinedb->update('parcels',array('downloaded'=>'1'),array('id' => $parcel->id, 'agency_id' => $parcel->agency_id));
			if($updatesms){$i++;}
		}echo "$i parcels downloaded. Parcels up to date";
	}
	
	
	
	public function push_transactions_parcels($unpushed_transactions_parcels)
	{	
		foreach($unpushed_transactions_parcels as $trx){
						
						//var_dump($trx);exit;
						//ORM removed here as it might be troublesome to pick another db and table with it.
						$parcel = $this->onlinedb->insert('parcels',array(
										
										'agency_id' => $this->admin->agency_id,
										'receiver_name' => $trx->receiver_name,
										'receiver_phone' => $trx->receiver_phone,
										'collected_by' => $trx->collected_by,
										'collected_on' => $trx->collected_on,
										'description' => $trx->description,
										'from' => $trx->from,
										'to' => $trx->to,
										'price' => $trx->price,
										'sent_date' => $trx->sent_date,
										'state' => $trx->state
												
										));
						//change the online state to 1
						
						if($parcel)
						{	
							//Pick out the transaction from the transactions table and ENSURE that 'online' is set to 1 once done,
							//so next time, it's not uploaded again
						
							//IDentification is by time and agencyid
							$updatetrx = $this->offlinedb->update('transactions_parcels',array('online'=>'1'),array('change_time' => $trx->change_time, 'agency_id' => $trx->agency_id));
						}
			}
				
	}
	
}
