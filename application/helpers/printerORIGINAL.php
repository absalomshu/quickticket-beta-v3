<?php defined('SYSPATH') or die('No direct script access');

/**
 * Description:  
 
 *
 * @package		 quickticket
 * @subpackage 	 Helper
 * @author 		 Absalom Shu
 * @copyright    (c) 2013
 * @license 	 quickticket
 */
 
class Printer_Core extends Admin_Controller
{
	//Builds up a pdf ticket and saves to server, returning the name of the file generated
	public function print_ticket($parent, $departure_time, $departure_date, $client_name, $client_phone, $seat_number, $from, $to, $bus_number, $ticket_price, $schedule_id)
	{	
		$agency = get::agency($this->agency_id);
		$ticket_info = $agency->TicketInfo;
		$contact = $agency->contacts;
		$logo = $agency->Logo;
		$ticket_background = $agency->ticket_background;
		//parent and from give you the agency name which is the logo name
		if(!file_exists($logo)){$logo = 'sample_logo.jpg';}else{$logo = strtolower($parent)."-".strtolower($from).".jpg";}
		if(!file_exists($ticket_background)){$ticket_background = 'images/ticket/bus.png';}

		
		$pdf = new Fpdf();
		$page_size = array('150','85');
		
		$pdf->SetAutoPageBreak(false);
		
		$pdf->AddPage('l',$page_size);
		//$pdf->SetFont('Times','',12);
		//$pdf->SetFont('Arial','B',16);
		$pdf->Image('images/ticket/logo.png',10,2,12);
		

		
		// Title
		$pdf->SetY(5);$pdf->Cell(11);
		$pdf->SetFont('Arial','B',10);
		$pdf->Cell(30,9,'QuickTicket',0,0,'L');
		
		// Move to the right
		$pdf->SetY(5);$pdf->Cell(90);
		$pdf->SetFont('Arial','B',14);
		$pdf->Cell(30,10,$parent,0,0,'R');
		$pdf->Image("images/ticket/agency-logos/$logo",130,2,12);
		// Line break
		$pdf->SetY(15);
		// The underlying line	
		$pdf->Cell(2,0,'________________________________________________',0,1);
		
		//$pdf->Image('images/ticket/bus.png',30,17,85);
		$pdf->Image($ticket_background,30,17,85);

		// Line break
		$pdf->SetY(20);$pdf->Cell(5);
		$pdf->Cell(25,8,'Time: ' . date("g:i A",strtotime($departure_time)),0,0,'L');
		$pdf->Cell(50);
		$pdf->Cell(0,8,'Date: '. date("d-m-Y",strtotime($departure_date)),0,1,'L');
		
		
		// Line break
		$pdf->SetY(27);$pdf->Cell(5);
		$pdf->Cell(25,8,'Name: ' . $client_name,0,0,'L');
		
		// Line break
		$pdf->SetY(34);$pdf->Cell(5);
		$pdf->Cell(25,8,'Phone: '. $client_phone ,0,0,'L');
		$pdf->Cell(50);
		$pdf->Cell(0,8,'Seat: '.$seat_number,0,1,'L');
		
		// Line break
		$pdf->SetY(41);
		$pdf->Cell(5);
		$pdf->Cell(25,8,'From: ' . $from,0,0,'L');
		$pdf->Cell(50);
		$pdf->Cell(0,8,'To: ' . $to,0,1,'L');
		
		// Line break
		$pdf->SetY(48);
		$pdf->Cell(5);
		$pdf->Cell(25,8,'Vehicle: '. $bus_number,0,0,'L');
		$pdf->Cell(50);
		$pdf->Cell(0,8,'Price: ' . $ticket_price.' FCFA',0,1,'L');
		
		
		$pdf->SetFont('Arial','','8');
		$pdf->SetY(55);
		$pdf->Cell(25,8,"$contact",0,0,'c');

		
		$pdf->SetFont('Arial','','7');
		$pdf->SetY(60);
		$pdf->Cell(25,8,"$ticket_info",0,0,'L');
		$pdf->SetY(63);
		$pdf->Cell(130,8,' We are not responsible for your unpaid and undeclared luggage.',0,0,'C');
		
		//FOOTER
		// Position at 1.5 cm from bottom
		$pdf->SetY(65);
		// Arial italic 8
		$pdf->SetFont('Arial','I',8);
		$pdf->Cell(0,20,'www.quickticket.co',0,0,'C');
		
		//save the file as "date busnumber S- seatnumber"
		//NB THE DATE OF THE SCHEDULE NOT CURRENT DATE
		//$ticket_name = date('Ymd').str_replace(' ','',$bus_number)."-S".$seat_number;
		//$ticket_name = date("Ymd",strtotime($departure_date)).str_replace(' ','',$bus_number)."-S".$seat_number;
		$ticket_name = date("Ymd",strtotime($departure_date)).".A$this->agency_id.".$schedule_id."-S".$seat_number;
		
		//output as a file saved on the server
		$here = $pdf->Output("tickets/$ticket_name.pdf","F");
		$ticket = url::base()."tickets/$ticket_name.pdf";
		
		return $ticket;
								
	}	
	
	//Ticket for Original Jean Jeannot
	public function print_ticket_type_2($parent, $departure_time, $departure_date, $client_name, $client_phone, $seat_number, $from, $to, $bus_number, $ticket_price, $schedule_id)
	{
		$agency = get::agency($this->agency_id);
		$ticket_info = $agency->TicketInfo;
		$contact = $agency->contacts;
		$logo = $agency->Logo;
		$ticket_background = $agency->ticket_background;
		//parent and from give you the agency name which is the logo name
		if(!file_exists($logo)){$logo = 'sample_logo.jpg';}else{$logo = strtolower($parent)."-".strtolower($from).".jpg";}
		if(!file_exists($ticket_background)){$ticket_background = 'images/ticket/bus.png';}
		
		$pdf = new Fpdf();		
		$page_size = array('120','130');
		$pdf->SetAutoPageBreak(false);
		$pdf->AddPage('P',$page_size);
		
		
		// Move to the right
		$pdf->SetY(5);$pdf->Cell(7);
		$pdf->SetFont('Arial','B',17);
		$pdf->Cell(0,10,$parent,0,0,'L');$pdf->Cell(20);
		$pdf->Image("images/ticket/agency-logos/$logo",97,1,15);
		// Line break
		$pdf->SetY(15);
		// The underlying line	
		$pdf->Cell(2,0,'__________________________________',0,1);
		
		$pdf->Image('images/ticket/bus.png',25,27,85);

		$pdf->SetFont('Arial','',16);
		$pdf->SetY(20);$pdf->Cell(8);
		$pdf->Cell(25,8,'Time:',0,0,'L');
		
		$pdf->SetFont('Arial','B',20);
		$pdf->SetY(20);$pdf->Cell(40);
		$pdf->Cell(25,8,date("g:i A",strtotime($departure_time)),0,0,'L');
		
	
		$pdf->SetFont('Arial','',16);
		$pdf->SetY(29);$pdf->Cell(8);
		$pdf->Cell(25,8,'Name:',0,0,'L');
		
		$pdf->SetFont('Arial','B',20);
		$pdf->SetY(29);$pdf->Cell(40);
		$pdf->Cell(25,8,$client_name,0,0,'L');
		
		$pdf->SetFont('Arial','',16);
		$pdf->SetY(38);$pdf->Cell(8);
		$pdf->Cell(25,8,'Phone:' ,0,0,'L');
		
		$pdf->SetFont('Arial','B',20);
		$pdf->SetY(38);$pdf->Cell(40);
		$pdf->Cell(25,8,$client_phone,0,0,'L');
		
		$pdf->SetFont('Arial','',16);
		$pdf->SetY(47);$pdf->Cell(8);
		$pdf->Cell(25,8,'Date:' ,0,0,'L');
		
		$pdf->SetFont('Arial','B',20);
		$pdf->SetY(47);$pdf->Cell(40);
		$pdf->Cell(25,8,date("d-m-Y",strtotime($departure_date)),0,0,'L');
		
		
		$pdf->SetFont('Arial','',16);		
		$pdf->SetY(56);$pdf->Cell(8);
		$pdf->Cell(0,8,'Seat:',0,1,'L');
		
		$pdf->SetFont('Arial','B',20);		
		$pdf->SetY(56);$pdf->Cell(40);
		$pdf->Cell(0,8,$seat_number,0,1,'L');
		
		$pdf->SetFont('Arial','',16);		
		$pdf->SetY(65);	$pdf->Cell(8);
		$pdf->Cell(25,8,'Origin:',0,0,'L');
		
		$pdf->SetFont('Arial','B',20);		
		$pdf->SetY(65);	$pdf->Cell(40);
		$pdf->Cell(25,8,$from,0,0,'L');
		
		$pdf->SetFont('Arial','',16);				
		$pdf->SetY(74);$pdf->Cell(8);
		$pdf->Cell(0,8,'Destination:' ,0,1,'L');
		
		$pdf->SetFont('Arial','B',20);				
		$pdf->SetY(74);$pdf->Cell(40);
		$pdf->Cell(0,8,$to,0,1,'L');
		
		$pdf->SetFont('Arial','',16);				
		$pdf->SetY(83);$pdf->Cell(8);
		$pdf->Cell(0,8,'Price:' ,0,1,'L');
		
		$pdf->SetFont('Arial','B',20);				
		$pdf->SetY(83);$pdf->Cell(40);
		$pdf->Cell(0,8,$ticket_price." FCFA",0,1,'L');
		
		$pdf->SetFont('Arial','','12');
		$pdf->SetY(89);$pdf->Cell(7);
		$pdf->Cell(2,0,'_________________________________________',0,1);
				
		$pdf->SetFont('Arial','','13');
		$pdf->SetY(91);$pdf->Cell(7);
		$pdf->Cell(25,8,'Tel: Buea: 33322867, Bfssam: 77484763',0,0,'L');
		
		$pdf->SetY(95);$pdf->Cell(7);
		$pdf->Cell(25,8,'Bda: 99061408, Lbe: 75543623, Yde: 75543623',0,0,'L');

		
		$pdf->SetFont('Arial','','12');
		$pdf->SetY(100);$pdf->Cell(7);
		$pdf->Cell(5,8,'If you miss your schedule, inform management within',0,0,'L');
		$pdf->SetY(104);$pdf->Cell(7);
		$pdf->Cell(25,8,'6 hours after which this ticket is valid for 30 days. We ',0,0,'L');
		$pdf->SetY(108);$pdf->Cell(7);
		$pdf->Cell(25,8,' are not responsible for unpaid/undeclared luggage.',0,0,'L');
		
		$pdf->Image('images/ticket/logo.png',60,114,12);
	
	
		$pdf->SetFont('Arial','','12');
		$pdf->SetY(114);$pdf->Cell(5);
		$pdf->Cell(25,8,'via',0,0,'L');
	
	
		$pdf->SetFont('Arial','I','12');
		$pdf->SetY(114);$pdf->Cell(11);
		$pdf->Cell(25,8,'QuickTicket | Travel made easy.',0,0,'L');
		
		$pdf->SetFont('Arial','I','11');
		$pdf->SetY(114);$pdf->Cell(82);
		$pdf->Cell(25,8,'www.quickticket.co',0,0,'R');

		//save the file as "date busnumber S- seatnumber"
		//$ticket_name = date('Ymd').str_replace(' ','',$bus_number)."-S".$seat_number;
		$ticket_name = date("Ymd",strtotime($departure_date)).".A$this->agency_id.".$schedule_id."-S".$seat_number;
		
		//output as a file saved on the server
		$here = $pdf->Output("tickets/$ticket_name.pdf","F");
		$ticket = url::base()."tickets/$ticket_name.pdf";
		
		return $ticket;
								
	}
	
	
	//Use the date of the ticket, the bus number and seat number to retrieve the ticket ALREADY CREATED and STORED somewhere.
	//Return the ticket's name and location
	//We don't use the date of today, but the date when the ticket was created
	public function fetch_ticket($date, $schedule_id, $seat_number )
	{
		//var_dump($seat_number);
		//$ticket_name = date("Ymd",strtotime($date)).str_replace(' ','',$bus_number)."-S".$seat_number;
		$ticket_name = date("Ymd",strtotime($date)).".A".$this->agency_id.".".$schedule_id."-S".$seat_number;
		$ticket = url::base()."tickets/$ticket_name.pdf";
		
		return $ticket;
	
	
	
	}
	
	
	public function print_parcel_ticket($agency_id, $parcel_id, $from, $to, $price, $name)
	{	
		$agency = get::agency($this->agency_id);
		$parent = strtoupper(get::_parent($agency_id)->name);
		$ticket_info = $agency->TicketInfo;
		$contact = $agency->contacts;
		$logo = $agency->Logo;
		$ticket_background = $agency->ticket_background;
		//parent and from give you the agency name which is the logo name
		if(!file_exists($logo)){$logo = 'sample_logo.jpg';}else{$logo = strtolower($parent)."-".strtolower($from).".jpg";}
		//if(!file_exists($ticket_background)){$ticket_background = 'images/ticket/bus.png';}

		
		$pdf = new Fpdf();
		$page_size = array('150','85');
		
		$pdf->SetAutoPageBreak(false);
		
		$pdf->AddPage('l',$page_size);
		//$pdf->SetFont('Times','',12);
		//$pdf->SetFont('Arial','B',16);
		$pdf->Image('images/ticket/logo.png',10,2,12);
		

		
		// Title
		$pdf->SetY(5);$pdf->Cell(11);
		$pdf->SetFont('Arial','B',10);
		$pdf->Cell(30,9,'QuickTicket',0,0,'L');
		
		// Move to the right
		$pdf->SetY(5);$pdf->Cell(90);
		$pdf->SetFont('Arial','B',14);
		$pdf->Cell(30,10,$parent,0,0,'R');
		$pdf->Image("images/ticket/agency-logos/$logo",130,2,12);
		// Line break
		$pdf->SetY(15);
		// The underlying line	
		$pdf->Cell(2,0,'________________________________________________',0,1);
		
		//$pdf->Image('images/ticket/bus.png',30,17,85);
		//$pdf->Image($ticket_background,30,17,85);

		// Line break
		$pdf->SetY(20);$pdf->Cell(5);
		//$pdf->Cell(25,8,'Parcel ID: '.$agency_id.$parcel_id,0,0,'L');
		//$pdf->Cell(50);
		$pdf->Cell(25,8,' ',0,0,'L');
		$pdf->Cell(50);
		$pdf->Cell(0,8,'Date: ',0,1,'L');
		
		
		// Line break
		$pdf->SetY(27);$pdf->Cell(5);
		$pdf->Cell(25,8,'Name: '.$name ,0,0,'L');
		
		
		
		// Line break
		$pdf->SetY(41);
		$pdf->Cell(5);
		$pdf->Cell(25,8,'From: ' . $from,0,0,'L');
		$pdf->Cell(50);
		$pdf->Cell(0,8,'To: ' . $to,0,1,'L');
		
		// Line break
		$pdf->SetY(48);
		$pdf->Cell(5);
		$pdf->Cell(25,8,'Parcel ID: '.$agency_id.$parcel_id,0,0,'L');
		$pdf->Cell(50);
		$pdf->Cell(0,8,'Price: ' . $price.' FCFA',0,1,'L');
		
		
		$pdf->SetFont('Arial','','8');
		$pdf->SetY(55);
		$pdf->Cell(25,8,"asdfadsf",0,0,'c');

		/*
		$pdf->SetFont('Arial','','7');
		$pdf->SetY(60);
		$pdf->Cell(25,8,"asdfasdfasdf",0,0,'L');
		$pdf->SetY(63);
		$pdf->Cell(130,8,' We are not responsible for your unpaid and undeclared luggage.',0,0,'C');
		*/
		
		//FOOTER
		// Position at 1.5 cm from bottom
		$pdf->SetY(65);
		// Arial italic 8
		$pdf->SetFont('Arial','I',8);
		$pdf->Cell(0,20,'www.quickticket.co',0,0,'C');
		
		//save the file as "agency_id parcel_id "
		//NB THE DATE OF THE SCHEDULE NOT CURRENT DATE
		//$ticket_name = date('Ymd').str_replace(' ','',$bus_number)."-S".$seat_number;
		$ticket_name = $agency_id.$parcel_id;
		
		//output as a file saved on the server
		$here = $pdf->Output("tickets/parcels/$ticket_name.pdf","F");
		$ticket = url::base()."tickets/parcels/$ticket_name.pdf";
		
		return $ticket;
								
	}
	
}
