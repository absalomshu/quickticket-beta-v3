<?php defined('SYSPATH') or die('No direct script access');

class Admin_Model extends ORM{

public function add_admin($password, $username, $admin_group_id, $parent_id, $town_id){
		
		//get parent names, remove white spaces and combine to form codename
		$parent = ORM::factory('parent')->where('id',$parent_id)->find();
		$parent_name = $parent->name;
		$town_name = get::town($town_id);
		
		//check if an agency exists with the same name and same town
		$agency_exists = ORM::factory('agency')->where('parent_id',$parent_id)->where('town_id',$town_id)->count_all();
		//first save the new agency being added if it's not registered
		if(!$agency_exists)
		{	
			$name = $parent_name." - ".$town_name;
			$agency = ORM::factory('agency');
			$agency->name = $name;
			$agency->parent_id = $parent_id;
			$agency->town_id = $town_id;
			$agency->save();
			//hold the agency id generated for future use
			$agency_id = $agency->id;
			$notice = "New agency registered and new admin created";
		}else
		{
			//if the agency already exists, simply pick its id and attribute to this new admin
			$agency_id = ORM::factory('agency')->where('parent_id',$parent_id)->where('town_id',$town_id)->find()->id;
			$notice = "New admin created";
			
		}
		//now save in the admin table
		$admin=ORM::factory('admin');
		$admin->admin_group_id=$admin_group_id;
		$admin->username=$username;
		$admin->password = Authlite::instance()->hash($password);
		$admin->agency_id = $agency_id;
		$admin->save();
		return $notice;
		} 
		
	//this is the add admin to be used by BMs
		//differs from above as they don't choose group, and they DO choose aditional privileges
	public function safe_add_admin($username, $password, $agency_id, $created_by)
	{
		$admin=ORM::factory('admin');
		$admin->username=$username;
		$admin->password = Authlite::instance()->hash($password);
		$admin->admin_group_id=2;
		$admin->agency_id = $agency_id;
		$admin->CreatedBy = $created_by;
		
		$admin->save();
		
		$notice = "Admin created.";
		return $notice;		
	}
	
	public function safe_delete_admin($id,$deleted_by)
	{

		$delete=ORM::factory('admin')->where('id',$id)->find();
		$delete->Deleted = 1;
		$delete->DeletedBy = strtoupper($deleted_by);
		$delete->save();
		
		$notice="Admin deleted";
		return $notice;
		
	}
	public function change_password($admin_id,$new_password)
	{
		$admin=ORM::factory('admin')->where('id',$admin_id)->find();
		$admin->password = Authlite::instance()->hash($new_password);
		$admin->save();
		
		$notice = "Password modified.";
		return $notice;		
	} 
		
	public function delete_admin($id){

		$delete=ORM::factory('admin')->where('id',$id)->find();
		$delete->delete();
		$delete->save();}	
		
	public function get_by_username($admin_name){

		$admin=ORM::factory('admin')->where('username',$admin_name)->find();
		return $admin;
		
		}
	public function subtract_balance($admin_username,$amount)
	{
		$admin=ORM::factory('admin')->where('username',$admin_username)->find();
		$admin->available_balance = $admin->available_balance - $amount;
		$admin->save();
		return $admin;
	}
	
	public function add_balance($admin_username,$amount)
	{
		$admin=ORM::factory('admin')->where('username',$admin_username)->find();
		$admin->available_balance = $admin->available_balance + $amount;
		$admin->save();
		return $admin;
	}

	
	
	public function get_allowed_main_module_items($admin_name)
	{
		$allowed_main_modules=ORM::factory('admin_module')->where('AdminID',$admin_name)->find_all();
		return $allowed_main_modules;	
	}
	
	
	
	public function get_admins_excluding_current($agency_id, $admin_name){
		$admins=ORM::factory('admin')->where('agency_id',$agency_id)->where('deleted','0')->where('username !=',$admin_name)->find_all();
		return $admins;
	}
	
	public function get_all_active_admins($agency_id){
		$admins=ORM::factory('admin')->where('agency_id',$agency_id)->where('deleted','0')->find_all();
		return $admins;
	}

}
