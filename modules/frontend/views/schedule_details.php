<?php defined('SYSPATH') or die('No direct script access'); ?> 


  <div class="container">
    
	  <div class="row-fluid marketing" style="margin:5px 0 0px 0px;">

	  
		<div class="span12 hero-unit prof-unit" >
			<table class="table schedules-table">	
						<tr>
							<th>Travel agency</th>
							<th>From</th>
							<th>To</th>
							<th>Date</th>
							<th>Departure</th>
							<th>Bus type</th>
							<th>Bus number</th>
							<th>Price</th>
							<th></th>
						</tr>
					
							<tr>
								<td><?=get::agency_and_town($schedule->agency_id)?></td>
								<td><?=get::town($schedule->from);?></td>
								<td><?=get::town($schedule->to);?></td>
								<td><?=date("d-m-Y",strtotime($schedule->departure_date));?></td>
								<td><?=date("g:i A", strtotime($schedule->departure_time));?></td>
								<td><?=$schedule->bus_seats?> Seater</td>
								<td><?=$schedule->bus_number?></td>
								<td><?=number_format($schedule->ticket_price)?> frs</td>
								
							</tr>
						
			</table>
		</div>
	<?php
		//determine what type of notice to display if at all
		$notice = $this->session->get_once('notice');
			if(!empty($notice)){ 
				if($notice['type'] == 'success'){?><div class="alert general success"><a class="close" data-dismiss="alert" href="#">&times;</a><?php echo $notice['message']; }?></div>
			<?	if($notice['type'] == 'error'){?><div class="alert general error"><a class="close" data-dismiss="alert" href="#">&times;</a><?php echo $notice['message']; }?></div>
		<?}?>	
	<?php $number=$schedule->bus_seats; ?>

	<div class="row-fluid marketing" style="margin:5px 0 0px 0;">	
		
		
		
		<div class="span8" style="margin-left:0px;" >
		<div class="span12 hero-unit prof-unit" style="margin-left:0px;" >
			<form action="<?=url::site('home/schedule_details/'.$schedule->id)?>" method="POST">
				<div class="heading">Passenger information<span class="small-right-link"></span></div>
				<div class="rule"><hr/></div>
					
					<div class="span5 spacious">
						<div class="control-group">
						<label class="control-label lab" for="inputType">Names:</label>
							<div class="controls ">
								<input type="text"  name="res_name" placeholder="Passenger names as on ID card">
							</div>
						</div>
					</div>
					<div class="span5 spacious">
						<div class="control-group">
						<label class="control-label lab" for="inputType">Phone:</label>
							<div class="controls input-append ">
								<span class="add-on"> +237</span><input class="span11" type="text"  name="res_phone" placeholder="For confirmation SMS">
							</div>
						</div>
					</div>
					<div class="span5 spacious">
						<div class="control-group">
						<label class="control-label lab" for="inputType">Email:</label>
							<div class="controls ">
								<input type="text"  name="res_email" placeholder="Your email address">
							</div>
						</div>
					</div>
					<div class="span5 spacious">
										<div class="control-group">
										<label class="control-label lab" >ID card no:</label>
											<div class="controls ">
												<input type="text"  name="res_idc" placeholder="Your ID card number">
											</div>
										</div>
					</div>
					<input type="hidden"  name="agency_id" value="<?=$schedule->agency_id?>">
					
					<?php //print_r($seats_and_occupants);exit;?>
				<div class="span5 spacious" >
					<div class="control-group">
					<label class="control-label lab" for="inputType">Preferred seat :</label>
						<div class="short-dropdown">
							<select name="res_seat" id="inputType">
							<?php 
							//array to hold all occupied seats for highlighting  on bus plan
							echo "<script> var jsar = new Array();</script>";
							$i=2;
							for($i; $i<= $number; $i++){
							//make sure the seat is not already occupied and not reserved either.
							
							if(!isset($seats_and_occupants[$i])  AND !isset($seats_and_reservers[$i]) )
							{
							?>
								<option value="<?=$i?>"><?=$i?></option>
							<?php 
							}
							if(isset($seats_and_occupants[$i])) 
							{
								echo "<script> jsar[".$i."] = ".$i.";</script>";
							}
							}
							?>
		
							</select>
						</div>
					</div>
				</div>
			
			
					<div class="span5 spacious">
						<div class="control-group">
					<label class="control-label lab" for="inputType">&nbsp;</label>					
						<label class="control-label lab" for="inputType">
							
							<input type="submit" class="btn btn-info" value="Reserve" name="reserve_seat" />
							<!--<input type="submit" class="btn btn-success" value="Buy ticket" name="purchase_ticket" />-->
							<!--<input type="submit" class="btn btn-info" value="Cancel" />-->
						</label>
							</div>
					</div>
			
			
			</form>		
			
			
			
		</div>
		<div class="span12 reservation-guidelines">
			<br>
			* You will receive a confirmation message if this seat is granted you.<br/>
			* All seats reserved must be confirmed by payment before 8:00 PM</div>
		
		</div>
		
		<div class="span4 hero-unit prof-unit" >
					<div class="heading">Bus plan | <?=$number?> Seater</div>
					<div class="rule"><hr/></div>
					
				<?php 
						//check number of seats to know which bus plan to display
						if ($number == 70){include 'bus_plans/70_seater.php';}
						elseif ($number == 55){include 'bus_plans/55_seater.php';}
						elseif ($number == 35){include 'bus_plans/35_seater.php';}
						elseif ($number == 48){include 'bus_plans/48_seater.php';}
						elseif ($number == 30){include 'bus_plans/30_seater.php';}
				?>				

						Seats in <span style="color:red;">red</span> are occupied.		
						</div>
		
		
	  </div>  
	  </div>
	  
	  
   
<script type="text/javascript">
	$(document).ready(function(){
		var number_seats = "<?php echo $number; ?>";
		for (i=0; i <= number_seats; i++){
		if( (i!=1) && typeof jsar[i] != 'undefined'  ){
			$("td.plan:eq("+(i-1)+")").addClass('taken-seat');
			}
			} 		
	})
</script>
