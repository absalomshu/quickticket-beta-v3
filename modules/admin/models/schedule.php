<?php defined('SYSPATH') or die('No direct script access');

class Schedule_Model extends ORM{

	public function get($id)
	{
		$schedule=ORM::factory('schedule')->where('id',$id)->where('deleted','0')->find();
		//var_dump($schedule);exit;
		return $schedule;
	}
	
	
	public function safe_delete_schedule($agency_id, $schedule_id)
	{

		$schedule=ORM::factory('schedule')->where('agency_id',$agency_id)->where('id',$schedule_id)->find();
		$schedule->deleted = 1;
		$schedule->save();
		
		$notice="Schedule deleted";
		return $notice;
		
	}
	public function make_current($agency_id, $schedule_id, $admin_username)
	{
		$schedule=ORM::factory('schedule')->where('agency_id',$agency_id)->where('id',$schedule_id)->find();
		$schedule->status = 'current';
		$schedule->modified_by = strtoupper($admin_username);
		//Increment the update count
		$schedule->update_count += 1;
		$schedule->save();
		
		$notice="Schedule can now be modified. Save changes when done.";
		return $notice;
		
	}
	/*
	public function get_agency_departed_schedules_by_period($agency_id,$start_date,$end_date)
	{
		$db = new Database();
		$schedules = $db->query("SELECT * FROM schedules WHERE agency_id= ".$agency_id."  AND deleted = '0' AND departure_date BETWEEN CAST('$start_date' AS DATE) AND CAST('$end_date' AS DATE) AND status = 'departed' ORDER BY departure_date ASC");
		return $schedules;
	}	*/
	
	public function get_agency_schedules_by_route_by_period($agency_id, $town_from, $town_to,$start_date,$end_date)
	{
		$db = new Database();
		$schedules = $db->query("SELECT * FROM schedules WHERE `agency_id` = '".$agency_id."' AND `from` = '".$town_from."' AND `to` = '".$town_to."'  AND deleted = '0' AND departure_date BETWEEN CAST('$start_date' AS DATE) AND CAST('$end_date' AS DATE) AND status = 'departed' ORDER BY departure_date DESC");
		return $schedules;
	}
	
	//Gets only departed schedules
	public function get_agency_schedules_by_period_by_admin($agency_id,$start_date,$end_date, $admin)
	{
		$db = new Database();
		$schedules = $db->query("SELECT * FROM schedules WHERE agency_id= ".$agency_id." AND CreatedBy= '".$admin."'  AND deleted = '0' AND departure_date BETWEEN CAST('$start_date' AS DATE) AND CAST('$end_date' AS DATE) AND status = 'departed' ORDER BY departure_date ASC");
		return $schedules;
	}
	
	//Gets only departed schedules
	public function get_agency_schedules_by_period_by_bus($agency_id,$start_date,$end_date, $bus_number)
	{
		$db = new Database();
		$schedules = $db->query("SELECT * FROM schedules WHERE agency_id= ".$agency_id." AND bus_number= '".$bus_number."'  AND deleted = '0' AND departure_date BETWEEN CAST('$start_date' AS DATE) AND CAST('$end_date' AS DATE) AND status = 'departed' ORDER BY departure_date ASC");
		return $schedules;
	}
	
	public function count_current_schedules($agency_id)	{	
		$today = date('Y-m-d');
		$count = ORM::factory('schedule')->where('agency_id',$agency_id)->where('status','current')->where('deleted','0')->count_all();
		return $count;	
	}
	
	public function count_checked_out_today($agency_id){	
		$today = date('Y-m-d');
		$count=ORM::factory('schedule')->where('agency_id',$agency_id)->where('status','departed')->where('deleted','0')->where('checked_out_on',$today)->count_all();

		return $count;	
	}
	
	public function get_all_agency_current_schedules($agency_id){
		//$schedules=ORM::factory('schedule')->where('agency_id',$agency_id)->where('status','current')->where('deleted','0')->orderby('departure_date','desc')->find_all();
		$schedules = $this->db->select('schedules.id,schedules.ticket_price,schedules.bus_number,to,departure_time,departure_date,bus_seats,towns.name AS town_name_to,seat_occupants,reserved_seats')->from('schedules')->join('towns','towns.id=schedules.to')->where('agency_id',$agency_id)->where('status','current')->where('deleted','0')->orderby('departure_date','desc')->get();		
		return $schedules;
	}
	
	
	public function get_ten_agency_current_schedules($agency_id)
	{
	
	//$db = new Database();
		$schedules = $this->db->select('schedules.id,schedules.bus_number,to,departure_time, departure_date, towns.name AS town_name')->from('schedules')->join('towns','towns.id=schedules.to')->join('buses','buses.bus_number=schedules.bus_number')->where('schedules.agency_id',$agency_id)->where('status','current')->where('schedules.deleted','0')->orderby('schedules.departure_date','desc')->orderby('schedules.departure_time','desc')->limit(10)->get();
		//$schedules=$db->query("SELECT * FROM schedules WHERE agency_id= ".$agency_id." AND status= 'current'  AND deleted = '0' ORDER BY id DESC");
		
		return $schedules;
	}
	
	public function get_ten_agency_departed_schedules($agency_id){
		$schedules = $this->db->select('schedules.id,schedules.bus_number,to,checked_out_time,departure_date,towns.name AS town_name')->from('schedules')->join('towns','towns.id=schedules.to')->where('agency_id',$agency_id)->where('status','departed')->where('deleted','0')->orderby('schedules.id','desc')->limit(10)->get();
		//$schedules=ORM::factory('schedule')->where('agency_id',$agency_id)->where('status','departed')->orderby('id','desc')->limit(10)->find_all();
		return $schedules;
	}
	public function get_ten_agency_departed_schedules_restricted($agency_id,$town_id){
		//$schedules=ORM::factory('schedule')->where('agency_id',$agency_id)->where('status','departed')->where('to',$town_id)->orderby('id','desc')->limit(10)->find_all();
		$schedules = $this->db->select('schedules.id,schedules.bus_number,to,departure_time, departure_date, checked_out_time,towns.name AS town_name')->from('schedules')->join('towns','towns.id=schedules.to')->where('agency_id',$agency_id)->where('status','departed')->where('deleted','0')->where('to',$town_id)->orderby('schedules.id','desc')->limit(10)->get();

		return $schedules;
	}
	
	public function get_ten_agency_current_schedules_restricted($agency_id, $town_id){
	
		//$schedules = ORM::factory('schedule')->where('agency_id',$agency_id)->where('status','current')->where('deleted','0')->where('to',$town_id)->orderby('id','desc')->limit(10)->find_all();
		$schedules = $this->db->select('schedules.id,schedules.bus_number,to,departure_time,departure_date,checked_out_time,towns.name AS town_name')->from('schedules')->join('towns','towns.id=schedules.to')->join('buses','buses.bus_number=schedules.bus_number')->where('schedules.agency_id',$agency_id)->where('status','current')->where('schedules.deleted','0')->where('to',$town_id)->orderby('schedules.departure_date','desc')->orderby('schedules.departure_time','desc')->limit(10)->get();

		return $schedules;
	}
	
	public function check_if_bus_in_current_schedule($agency_id, $bus_number)
	{
		$in_current_schedule=ORM::factory('schedule')->where('agency_id',$agency_id)->where('bus_number',$bus_number)->where('status','current')->where('deleted','0')->count_all();
		
		return $in_current_schedule	;
	}
	
	/*public function all_agency_schedules_by_date($agency_id,$date){
		$schedules=ORM::factory('schedule')->where('agency_id',$agency_id)->where('deleted','0')->find_all();
		return $schedules;
	}*/
	
	public function all_departed_schedules_by_date($agency_id,$date){
		$schedules=ORM::factory('schedule')->where('agency_id',$agency_id)->where('deleted','0')->where('status','departed')->where('checked_out_on',$date)->find_all();
		return $schedules;
	}
	
	public function get_departed_schedule_income_by_date($agency_id,$date){
		
		//Only calculate from departed schedules
		$date = date("Y-m-d",strtotime($date));
		$period_schedules=Schedule_Model::all_departed_schedules_by_date($agency_id,$date);
		//var_dump($period_schedules);exit;
		//Income from schedules
		$schedule_income_total = 0;
		foreach($period_schedules as $ds)
		{
			$schedule_income_total += $ds->total_amount;
		}
		
		return $schedule_income_total;
	}
	
	//Ticket sales is only calculated from departed buses
	public function agency_schedules_by_period($agency_id,$start_date,$end_date)
	{
		$db = new Database();
		$schedules = $db->query("SELECT * FROM schedules WHERE agency_id= ".$agency_id." AND deleted = '0' AND departure_date BETWEEN CAST('$start_date' AS DATE) AND CAST('$end_date' AS DATE) AND status = 'departed' ORDER BY departure_date ASC");
		return $schedules;
	}
	
	public function get_departed_schedule_income_by_period($agency_id,$start_date,$end_date){
		
		//Only calculate from departed schedules
		//$date = date("Y-m-d",strtotime($date));
		$period_schedules=Schedule_Model::agency_schedules_by_period($agency_id,$start_date,$end_date);
		//var_dump($period_schedules);exit;
		//Income from schedules
		$schedule_income_total = 0;
		foreach($period_schedules as $ds)
		{
			$schedule_income_total += $ds->total_amount;
		}
		
		return $schedule_income_total;
	}

}
