<?php defined('SYSPATH') or die('No direct script access'); ?> 
<!DOCTYPE html>


		
		<div class="span5 hero-unit prof-unit offset3">
		<div class="notice">
			<?php
				//determine what type of notice to display if at all
				$notice = $this->session->get_once('notice');
					if(!empty($notice)){ 
						if($notice['type'] == 'success'){?><div class="alert general success"><a class="close" data-dismiss="alert" href="#">&times;</a><?php echo $notice['message']; ?></div><?php }?>
					<?	if($notice['type'] == 'error'){?><div class="alert general error"><a class="close" data-dismiss="alert" href="#">&times;</a><?php echo $notice['message'];?></div><?php }?>
			<?}?>
		</div>
			
			<h20>Please enter details for admin to be added</h20>
			<div class="rule"><hr/></div><br/>
				<form method="POST" action="<?=url::site('admin/add_admin')?>" class="form-inline">
					
							
							<div class="s-by-s">
								<select name="parent" id="inputType" style="width:auto;">
									<?php foreach ($parents as $parent):?>
									<option value="<?=$parent->id?>" ><?=$parent->name?></option>
									<?php endforeach;?>
								
							</select>
							</div>
						
						
							
							<div class="s-by-s ">
								<select name="town" id="inputType" style="width:auto;">
									<?php foreach ($towns as $town):?>
									<option value="<?=$town->id?>" ><?=$town->name?></option>
									<?php endforeach;?>
								
							</select>
							</div>
						
						<div class="">
						<label class="control-label lab" for="inputType"></label>
							
							<div class="controls ">
								<select name="admin_group" id="inputType">
								
									<?php //remove group id 1 which is a user, and not useful
									foreach ($groups as $group){?>
										<option value="<?=$group->id?>"><?=$group->role?></option>
									<?php };?>
								
							</select>
							</div>
						</div>
						<!--<div class="control-group">
						<label class="control-label " for="inputType"></label>
							
							<div class="controls ">
								<select name="town" id="inputType">
									<?php //foreach ($towns as $town):?>
									<option value="<?php //$town->id?>"><?php //$town->name?></option>
									<?php //endforeach;?>
								
							</select>
							</div>
						</div>-->
					
					
					
						<div class="">
						<label class="control-label lab" for="inputType"></label>
							<div class="controls ">
							<input type="text" class="input-small" placeholder="Username" style="width:300px;height:30px;" name="username">
												
							</div>
						</div>
					
						<div class="">
						<label class="control-label lab" for="inputType"></label>
							<div class="controls ">
							<input type="password" class="input-small" placeholder="Password" style="width:300px;height:30px;" name="password">&nbsp;&nbsp;
												
							</div>
						</div>
	
						<div class="">
						<label class="control-label lab" for="inputType"></label>
							<div class="controls ">
							<button class="btn btn-info" type="submit" style="height:35px; width:120px;font-size:20px;"><?=Kohana::lang('backend.add')?></button>					
							</div>
						</div>
						
				</form> 
			

		</div>

