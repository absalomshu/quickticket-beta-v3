<?php defined('SYSPATH') or die('No direct script access'); ?> 
	

 
	<div class="row-fluid ">
	  	<div class="span12 hero-unit prof-unit" >
		
		<ul class="nav nav-tabs" id="myTab">
					
					<li ><a href="<?=url::site('cash/collections')?>"><i class="icon-align-justify"></i> <?=Kohana::lang('backend.collection')?></a></li>
					<li><a href="<?=url::site('cash/other_income')?>"><i class="icon-align-justify"></i> <?=Kohana::lang('backend.other_income')?></a></li>
					<li class="active"><a href="#"><i class="icon-align-justify"></i> <?=Kohana::lang('backend.cash_to_bank')?></a></li>
					<li><a href="<?=url::site('cash/balances')?>"><i class="icon-align-justify"></i> <?=Kohana::lang('backend.balances')?></a></li>
		</ul>
		
		<div class="tab-content">
			 
			
			
			
		
			
			<div class="tab-pane active" id="cash-to-bank">
				<div class="">
				
					<legend><?=Kohana::lang('backend.cash_to_bank')?><div class='view'><a href="<?=url::site('cash/add_cash_to_bank')?>">+ <?=Kohana::lang('backend.add_cash_to_bank')?></a></div></legend>						
						<table class="table table-striped table-hover table-condensed">
					<tr>
						
						<th><?=Kohana::lang('backend.date')?></th>
						<th><?=Kohana::lang('backend.bank_name')?></th>
						<th><?=Kohana::lang('backend.account_number')?></th>
						<th><?=Kohana::lang('backend.amount')?></th>
						<th><?=Kohana::lang('backend.done_by')?></th>
												
					</tr>
					<?php foreach ($ctb as $trx):?>
						<tr>	
							<td><?=date("d-m-Y",strtotime($trx->date_incurred))?></td>
							<td><?=$trx->bank_name?></td>
							<td><?=$trx->account_number?></td>
							<td><?=number_format($trx->amount)?></td>
							<td><?=$trx->created_by?></td>
							
						</tr>
						<?php endforeach; ?>
				</table>	
		
					<?php echo $this->pagination;?>			
						
						
						
						
						
				</div>	
					
			</div>
			
		
					
		</div>
		
			
		
			  
		</div>
		
		</div>
	  <div class="clear"></div>

<script type="text/javascript">
	$(document).ready(function(){
			
			$('.input-append.date').datepicker({
				format: "dd-mm-yyyy",
				todayBtn: "linked",
				autoclose: true,
				todayHighlight: true
			});
			
			
				
			
	})
	</script>