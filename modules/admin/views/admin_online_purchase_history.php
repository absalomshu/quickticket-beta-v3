<?php defined('SYSPATH') or die('No direct script access'); ?> 


	  
	<div class="row-fluid marketing" style="margin:5px 0 0px 0;">	
		<div class="span12 hero-unit prof-unit reload" style="margin-left:0px;">
	<ul class="nav nav-tabs">
					<li > <a href="<?=url::site('admin/online_purchases')?>"><i class="icon-arrow-left"></i> <?=Kohana::lang('backend.back_online_purchases')?></a></li>
		</ul>
				<legend><?=Kohana::lang('backend.purchases_online')?></legend>
				
				<table class="table table-striped table-hover table-condensed">
					<tr>
						<th><?=Kohana::lang('backend.purchase_date')?> </th>
						<th><?=Kohana::lang('backend.name')?> </th>
						<th><?=Kohana::lang('backend.phone')?> </th>
						<th><?=Kohana::lang('backend.idc')?> </th>
						<th><?=Kohana::lang('backend.schedule_requested')?> </th>
						<th><?=Kohana::lang('backend.schedule_id')?> </th>
						<th><?=Kohana::lang('backend.amount_paid')?> </th>
						<th><?=Kohana::lang('backend.status')?> </th>
						<th><?=Kohana::lang('backend.modified_by')?> </th>
						
					</tr>
					<?php foreach ($online_purchases as $purchase){ 
					?>
					<tr>
						<td><?=date("d-m-Y",strtotime($purchase->CreatedOn))?></td>						
						<td><?=$purchase->client_name?></td>
						<td><?=$purchase->client_phone?></td>
						<td><b><?=$purchase->client_idc?></b></td>
						<?php 
						//Only if a particular schedule was requested i.e. NOT open ticket
						if($purchase->schedule_id){
						//Since it's the global schedule id, the local id excludes the 1st 3 chars which are the agency id 
						
							$schedule=Schedule_Model::get(substr($purchase->schedule_id,3)); ?>
						
						<td><?=$schedule->bus_number?> : <?=get::town($schedule->from)?> --> <b><?=get::town($schedule->to)?></b> : <b><?=date("g:i A", strtotime($schedule->departure_time))?></td>
						<td><?="SC".$purchase->schedule_id?></td>
						<?php }else{ ?>
						
						<td><?=get::town($purchase->town_from)?> --> <?=get::town($purchase->town_to)?> : <b>Anytime</b></td>
						<td>N/A</td>
						<?php }?>
						<td><?=number_format($purchase->amount_paid)?>	</td>
						<td><?=$purchase->status?>	</td>
						<td><?=$purchase->ModifiedBy?>	</td>
						
						
					</tr>
					<?php 
					
					} ?>
				</table>
				
			
					<?php echo $this->pagination;?>
<?php //$this->profiler = new Profiler();?>
		</div>
	
		
		</div>

