<?php defined('SYSPATH') or die('No direct script access');

/**
 * Description:  
 
 *
 * @package		 quickticket
 * @subpackage 	 Helper
 * @author 		 Absalom Shu
 * @copyright    (c) 2013
 * @license 	 quickticket
 */
//prevent the script from crashing after the default 30 seconds max execution time
ini_set("max_execution_time", "600000"); 
 
class Online_Controller extends Admin_Controller
{	
	//Requires an internet connection NECESSARILY!
	private $onlinedb;
	private $offlinedb;
	
	public function __construct()
	{		
		$this->admin = Authlite::instance()->get_admin();
		$this->session = Session::instance();
	
		if (!empty($this->admin)) 
		{
			$this->agency_id = $this->admin->agency_id;
		}
	}
	
	public function index()
	{	
		//establish connection to online database
		$this->onlinedb = new Database('online_db');
		$this->offlinedb = new Database('offline_db');
		
		//get unpushed schedules and parcels
		$unpushed_transactions=get::unpushed_transactions();
		//$unpushed_transactions_parcels=get::unpushed_transactions_parcels();
		//$this->push_transactions doesn't work here as it instead gives Transactions_controller::push_transactions.
		$push_result = $this->push_transactions($unpushed_transactions);
		echo $push_result;	
	}
	
	
	//takes the transaction resource
	public function push_transactions($unpushed_transactions)
	{	
		//instantiate, if not the if statement toward the end of this foreach sometimes gives an error
		$expense = 0; $bus = 0; $parcel = 0; $schedule = 0;
		foreach($unpushed_transactions as $trx)
		{
			/*
			*
			*FOR SOME UNKNOWN REASON,THE WRONG AGENCY ID IS POSTED WHEN UPDATING PARCELS. HENCE THE 'FROM' IS USED
			
			DO NOT USE $this->anything IN THIS CONTROLLOER. VALUES GET MIXED UP. USED THE VALUE FROM trx
			*/
			
			//ORM removed here as it might be troublesome to pick another db and table with it.
			
			//var_dump($trx);echo"<br/><br/><br/><br/>";continue;
			//check the table it's coming from to know the table to post it to, as all transactions come to one table
			if($trx->table=='schedules'){
				$entry_exists = $this->onlinedb->count_records ('schedules',array('id'=>$trx->agency_id.$trx->change_id,'agency_id'=>$this->admin->agency_id)); 
				
				//if entry exists online, update else insert
				if($entry_exists){
				$schedule = $this->onlinedb->update('schedules',array(
							
							'agency_id' => $trx->agency_id,
							'bus_number' => $trx->bus_number,
							'bus_seats' => $trx->bus_seats,
							'from' => $trx->from,
							'to' => $trx->to,
							'ticket_price' => $trx->ticket_price,
							'seat_occupants' => $trx->seat_occupants,
							'reserved_seats' => $trx->reserved_seats,
							'status' => $trx->status,
							'loading' => $trx->loading,
							'departure_date' => $trx->departure_date,
							'departure_time' => $trx->departure_time,
							'checked_out_on' => $trx->checked_out_on,
							'checked_out_time' => $trx->checked_out_time,
							'total_amount' => $trx->total_amount,			
							'CreatedBy' => $trx->CreatedBy,			
							'CreatedOn' => $trx->CreatedOn,			
							'CheckedOutBy' => $trx->CheckedOutBy,			
							'deleted' => $trx->deleted			
							), array('id'=>$trx->agency_id.$trx->change_id,'agency_id'=>$this->admin->agency_id));
				}else{
					$schedule = $this->onlinedb->insert('schedules',array(
							
							'id' => $trx->agency_id.$trx->change_id,
							'agency_id' => $trx->agency_id,
							'bus_number' => $trx->bus_number,
							'bus_seats' => $trx->bus_seats,
							'from' => $trx->from,
							'to' => $trx->to,
							'ticket_price' => $trx->ticket_price,
							'seat_occupants' => $trx->seat_occupants,
							'reserved_seats' => $trx->reserved_seats,
							'status' => $trx->status,
							'loading' => $trx->loading,
							'departure_date' => $trx->departure_date,
							'departure_time' => $trx->departure_time,
							'checked_out_on' => $trx->checked_out_on,
							'checked_out_time' => $trx->checked_out_time,
							'total_amount' => $trx->total_amount,
							'CreatedBy' => $trx->CreatedBy,			
							'CreatedOn' => $trx->CreatedOn,			
							'CheckedOutBy' => $trx->CheckedOutBy,
							'deleted' => $trx->deleted	
							));
				
				}
							
			}elseif($trx->table=='buses'){
				//if that agency already created that bus, just update. Else create the entry where(array('id' => 5, 'title' => 'Demo'))
				//$entry_exists = $this->onlinedb->count_records ('buses',array('bus_number'=>$trx->bus_number,'agency_id'=>$this->admin->agency_id)); 
				//bus numbers appear to be unique. so 1 agency should modify 1 bus, as long as the number is same.
				$entry_exists = $this->onlinedb->count_records ('buses',array('bus_number'=>$trx->bus_number)); 
				if($entry_exists){
				//var_dump($entry_exists);
					//update the column where the bus number(which is the id in this case) and the ageny id coincide.
					$bus = $this->onlinedb->update('buses',array(
							'agency_id' => $trx->agency_id,
							'bus_number' => $trx->bus_number,
							'bus_seats' => $trx->bus_seats,
							'vip' => $trx->vip,
							'driver' => $trx->driver,
							'deleted' => $trx->deleted	
					//), array('bus_number'=>$trx->bus_number,'agency_id'=>$this->admin->agency_id));
					), array('bus_number'=>$trx->bus_number));
					
					//var_dump(count($bus));exit;
				}else{				
				$bus = $this->onlinedb->insert('buses',array(
							
							'id' => $trx->agency_id.$trx->change_id,
							'agency_id' => $trx->agency_id,
							'bus_number' => $trx->bus_number,
							'bus_seats' => $trx->bus_seats,
							'vip' => $trx->vip,
							'driver' => $trx->driver,
							'deleted' => $trx->deleted		
							));}
							
			}elseif($trx->table=='expenses'){
				$entry_exists = $this->onlinedb->count_records ('expenses',array('id'=>$trx->agency_id.$trx->change_id,'agency_id'=>$this->admin->agency_id)); 
				if($entry_exists){
				$expense = $this->onlinedb->update('expenses',array(
							
							'agency_id' => $trx->agency_id,
							'bus_number' => $trx->bus_number,
							'schedule_id' => $trx->agency_id.$trx->schedule_id,
							'amount' => $trx->amount,
							'purpose' => $trx->purpose,
							'date_incurred' => $trx->date_incurred,
							'authorised_by' => $trx->authorised_by,
							'CreatedBy' => $trx->CreatedBy,	
							'CreatedOn' => $trx->CreatedOn,	
							'deleted' => $trx->deleted	
							), array('id'=>$trx->agency_id.$trx->change_id,'agency_id'=>$this->admin->agency_id));
							
			}else{
					$expense = $this->onlinedb->insert('expenses',array(
							'id' => $trx->agency_id.$trx->change_id,
							'agency_id' => $trx->agency_id,
							'bus_number' => $trx->bus_number,
							'schedule_id' => $trx->agency_id.$trx->schedule_id,
							'amount' => $trx->amount,
							'purpose' => $trx->purpose,
							'date_incurred' => $trx->date_incurred,
							'authorised_by' => $trx->authorised_by,
							'CreatedBy' => $trx->CreatedBy,	
							'CreatedOn' => $trx->CreatedOn,
							'deleted' => $trx->deleted	
							));}

			}elseif($trx->table=='parcels'){
				$entry_exists = $this->onlinedb->count_records ('parcels',array('id'=>$trx->agency_id.$trx->change_id,'agency_id'=>$this->admin->agency_id)); 
				if($entry_exists){
				$parcel = $this->onlinedb->update('parcels',array(
							
							'agency_id' => $trx->from,
							'schedule_id' => $trx->agency_id.$trx->schedule_id,
							'bus_number' => $trx->bus_number,
							'receiver_name' => $trx->receiver_name,
							'receiver_phone' => $trx->receiver_phone,
							'collected_by' => $trx->collected_by,
							'collected_on' => $trx->collected_on,
							'description' => $trx->description,
							'from' => $trx->from,
							'to' => $trx->to,
							'price' => $trx->price,
							'sent_date' => $trx->sent_date,
							'state' => $trx->parcel_state,
							'CreatedBy' => $trx->CreatedBy,			
							'CreatedOn' => $trx->CreatedOn
							), array('id'=>$trx->agency_id.$trx->change_id,'agency_id'=>$this->admin->agency_id));
			}else
			{
				//For parcels, only send online if its state is 'sent'
				//When it's not sent, Online will still be set to 1, but that is ok so that the online module does not try to put the parcel online forever
				if($trx->parcel_state=='sent')
				{
					$parcel = $this->onlinedb->insert('parcels',array(
					'id' => $trx->agency_id.$trx->change_id,
					'agency_id' => $trx->from,
					'schedule_id' => $trx->agency_id.$trx->schedule_id,
					'bus_number' => $trx->bus_number,
					'receiver_name' => $trx->receiver_name,
					'receiver_phone' => $trx->receiver_phone,
					'collected_by' => $trx->collected_by,
					'collected_on' => $trx->collected_on,
					'description' => $trx->description,
					'from' => $trx->from,
					'to' => $trx->to,
					'price' => $trx->price,
					'sent_date' => $trx->sent_date,
					'state' => $trx->parcel_state,			
					'CreatedBy' => $trx->CreatedBy,			
					'CreatedOn' => $trx->CreatedOn			
					));
				}
			}
			}			
						
						//change the online state to 1, only if query was successfully executed
						//This IF is in an attempt to prevent uploading next row if theres a problem with the current one.
						//Else later, the earlier transaction will overwrite the later one, hence cancelling changes.
						if($expense or $bus or $schedule or $parcel)
						{	
							//Pick out the transaction from the transactions table and ENSURE that 'online' is set to 1 once done,
							//so next time, it's not uploaded again
							//IDentification is by time and agencyid
							$updatetrx = $this->offlinedb->update('transactions',array('online'=>'1'),array('change_time' => $trx->change_time, 'agency_id' => $trx->agency_id));
						}
					/*	else
						{	//halt execution in case of error. Problem is NOTHING else will be pushed online.
							//SHOULD LOOK FOR A WAY TO SKIP AND CONTINUE.
							return 'Error. Some transaction blocking. Error:'.$trx->table. ':'.$trx->change_id;
						} */
		} return Kohana::lang('backend.everything_pushed_online');	
	}
	
	public function pull_parcels()
	{
		$onlinedb = new Database('online_db');
		$offlinedb = new Database('offline_db');
		
		//Download parcels that are destined for this branch
		$unpulled_parcels = $onlinedb->from('parcels')->where('to' , $this->agency_id)->where('downloaded','0')->getwhere();
		
		$i=0;
		foreach($unpulled_parcels as $parcel)
		{	//the composite parcel id is a combination of the originating agency id and the parcel id
			$pull = $offlinedb->insert('incoming_parcels',array(
										
										'agency_id' => $parcel->agency_id,
										'global_parcel_id' => $parcel->id,
										'schedule_id' => $parcel->schedule_id,
										'bus_number' => $parcel->bus_number,
										'receiver_name' => $parcel->receiver_name,
										'receiver_phone' => $parcel->receiver_phone,
										'description' => $parcel->description,
										'from' => $parcel->from,
										'to' => $parcel->to,
										'price' => $parcel->price,
										'sent_date' => $parcel->sent_date,
										'state' => $parcel->state,
										'CreatedBy' => $parcel->CreatedBy,
										'CreatedOn' => $parcel->CreatedOn
										));
			$updatesms = $onlinedb->update('parcels',array('downloaded'=>'1'),array('id' => $parcel->id, 'agency_id' => $parcel->agency_id));
			if($updatesms){$i++;}
		}echo "$i ". Kohana::lang('backend.parcels_downloaded');
	}
	
	public function pull_schedules()
	{
		$onlinedb = new Database('online_db');
		$offlinedb = new Database('offline_db');
		
		$today = date("Y-m-d");
		$parent = get::_parent($this->agency_id);
		$siblings = get::agency_siblings($this->agency_id,$parent);
		
		$agency_id=$this->agency_id;
		
		$this->db = new Database();
		$agency_town_id = Town_Model::get_agency_town_id($agency_id);
		
		//Download schedules that are destined for this branch. Get all, overwrite existing. Ensure it's from a sibling
		//this gets all schedules going to that town.
		$unpulled_schedules = $onlinedb->from('schedules')->where('to',$agency_town_id)->where('departure_date',$today)->getwhere();
		
		//now get only schedules created by a sibling
		$sibling_list = array();
		foreach($siblings as $sibling){
			$sibling_list[] = $sibling->id; 
		}
		$i=0;
		
		
		foreach($unpulled_schedules as $schedule)
		{	//make sure it's from a sibling
				if(in_array($schedule->agency_id,$sibling_list)){
				
				//check if it exists
				$entry_exists = $offlinedb->count_records ('incoming_schedules',array('id'=>$schedule->id,'from_agency_id'=>$schedule->agency_id)); 
				if($entry_exists){
					$pull = $offlinedb->update('incoming_schedules',array(
						'id' => $schedule->id,
						'agency_id' => $this->agency_id,
						'from_agency_id' => $schedule->agency_id,
						'bus_number' => $schedule->bus_number,
						'bus_seats' => $schedule->bus_seats,
						'from' => $schedule->from,
						'to' => $schedule->to,
						'ticket_price' => $schedule->ticket_price,
						'seat_occupants' => $schedule->seat_occupants,
						'reserved_seats' => $schedule->reserved_seats,
						'status' => $schedule->status,
						'loading' => $schedule->loading,
						'departure_date' => $schedule->departure_date,
						'departure_time' => $schedule->departure_time,
						'checked_out_on' => $schedule->checked_out_on,
						'checked_out_time' => $schedule->checked_out_time,
						'total_amount' => $schedule->total_amount,
						'CreatedBy' => $schedule->CreatedBy,			
						'CreatedOn' => $schedule->CreatedOn,			
						'CheckedOutBy' => $schedule->CheckedOutBy,
						'deleted' => $schedule->deleted				
							), array('id'=>$schedule->id,'from_agency_id'=>$schedule->agency_id));
				}else{
					$pull = $offlinedb->insert('incoming_schedules',array(
						'id' => $schedule->id,
						'agency_id' => $this->agency_id,
						'from_agency_id' => $schedule->agency_id,
						'bus_number' => $schedule->bus_number,
						'bus_seats' => $schedule->bus_seats,
						'from' => $schedule->from,
						'to' => $schedule->to,
						'ticket_price' => $schedule->ticket_price,
						'seat_occupants' => $schedule->seat_occupants,
						'reserved_seats' => $schedule->reserved_seats,
						'status' => $schedule->status,
						'loading' => $schedule->loading,
						'departure_date' => $schedule->departure_date,
						'departure_time' => $schedule->departure_time,
						'checked_out_on' => $schedule->checked_out_on,
						'checked_out_time' => $schedule->checked_out_time,
						'total_amount' => $schedule->total_amount,
						'CreatedBy' => $schedule->CreatedBy,			
						'CreatedOn' => $schedule->CreatedOn,			
						'CheckedOutBy' => $schedule->CheckedOutBy,
						'deleted' => $schedule->deleted	
					));
				}
			$i++;
			}//$updatesms = $onlinedb->update('parcels',array('downloaded'=>'1'),array('id' => $parcel->id, 'agency_id' => $parcel->agency_id));
			//if($updatesms){$i++;}
		}echo "$i ". Kohana::lang('backend.incoming_schedules_downloaded');
	}
	
	public function pull_online_purchases()
	{
		$onlinedb = new Database('online_db');
		$offlinedb = new Database('offline_db');
		
		//Download purchases that are destined for this branch. Only those that are active, and download only once
		$unpulled_purchases = $onlinedb->from('purchases')->where('agency_id', $this->agency_id)->where('status','active')->where('downloaded','0')->getwhere();
		
		$i=0;
		foreach($unpulled_purchases as $purchase)
		{	//the composite parcel id is a combination of the originating agency id and the parcel id
			$pull = $offlinedb->insert('online_purchases',array(
										
				'agency_id' => $purchase->agency_id,
				'schedule_id' => $purchase->schedule_id,
				'is_open_ticket' => $purchase->is_open_ticket,
				'town_to' => $purchase->town_to,
				'town_from' => $purchase->town_from,
				'ot_ticket_name' => $purchase->ot_ticket_name,
				'ot_ticket_price' => $purchase->ot_ticket_price,
				'client_name' => $purchase->client_name,
				'client_phone' => $purchase->client_phone,
				'client_email' => $purchase->client_email,
				'client_idc' => $purchase->client_idc,
				'amount_paid' => $purchase->amount_paid,
				'status' => $purchase->status,
				'CreatedBy' => $purchase->CreatedBy,
				'CreatedOn' => $purchase->CreatedOn
										));
			$updatesms = $onlinedb->update('purchases',array('downloaded'=>'1'),array('id' => $purchase->id, 'agency_id' => $purchase->agency_id));
			if($updatesms){$i++;}
		}echo "$i ". Kohana::lang('backend.purchases_downloaded');
	}
	
	
	
	public function push_remote_tickets()
	{	
		$onlinedb = new Database('online_db');
		$offlinedb = new Database('offline_db');
		
		$unpushed_remote_tickets = Ticket_Detail_Model::get_all_unpushed_outgoing_remote_tickets($this->agency_id);
	
		foreach($unpushed_remote_tickets as $trx){
						
						//var_dump($trx);exit;
						//ORM removed here as it might be troublesome to pick another db and table with it.
						$remote_ticket_pushed_online = $onlinedb->insert('ticket_details',array(
										
										'id' => $this->agency_id.$trx->id,
										'AgencyID' => $this->agency_id,
										'ScheduleID' => $trx->ScheduleID,
										'ClientName' => $trx->ClientName,
										'SeatNumber' => $trx->SeatNumber,
										'Price' => $trx->Price,
										'MissedStatus' => $trx->MissedStatus,
										'FreeTicket' => $trx->FreeTicket,
										'FreeTicketReason' => $trx->FreeTicketReason,
										'IsRemote' => $trx->IsRemote,
										'RemoteTicketOutgoing' => '1',
										'RemoteTicketAgencyFrom' => $trx->RemoteTicketAgencyFrom,
										'RemoteTicketAgencyTo' => $trx->RemoteTicketAgencyTo,
										'CreatedOn' => $trx->CreatedOn,
										'CreatedBy' => $trx->CreatedBy	
										));
						
						//change the online state to 1
						if($remote_ticket_pushed_online)
						{	
							//Pick out the transaction from the transactions table and ENSURE that 'online' is set to 1 once done,
							//so next time, it's not uploaded again
						
							//IDentification is by ticket id and agencyid
							$updatetrx = $offlinedb->update('ticket_details',array('Online'=>'1'),array('id' => $trx->id, 'AgencyID' => $trx->AgencyID));
						}
			}
				echo Kohana::lang('backend.remote_tickets_online');	
	}
	
	public function pull_remote_tickets()
	{
		$onlinedb = new Database('online_db');
		$offlinedb = new Database('offline_db');
		
		//Get tickets online where the RemoteTicketAgencyFrom is this agency.
		$unpulled_remote_tickets = $onlinedb->from('ticket_details')->where('RemoteTicketAgencyFrom', $this->agency_id)->where('IsRemote','1')->where('Downloaded','0')->getwhere();
		
		
		$i=0;
		foreach($unpulled_remote_tickets as $ticket)
		{	
		$agency_created_at = get::agency_name($ticket->AgencyID);
		//the composite parcel id is a combination of the originating agency id and the parcel id
			$pull = $offlinedb->insert('ticket_details',array(
								
				//since the current agency is the agency from for the remote ticket				
				'AgencyID' => $ticket->RemoteTicketAgencyFrom,
				'ScheduleID' => $ticket->ScheduleID,
				'ClientName' => $ticket->ClientName,
				'SeatNumber' => $ticket->SeatNumber,
				'Price' => $ticket->Price,
				'IsPending' => '1',
				'MissedStatus' => $ticket->MissedStatus,
				'FreeTicket' => $ticket->FreeTicket,
				'FreeTicketReason' => "Paid for at ".$agency_created_at,
				'IsRemote' => $ticket->IsRemote,
				'RemoteTicketIncoming' => '1',
				'RemoteTicketAgencyFrom' => $ticket->RemoteTicketAgencyFrom,
				'RemoteTicketAgencyTo' => $ticket->RemoteTicketAgencyTo,
				//'RemoteTicketCreatedAt' => $ticket->AgencyID,
				'CreatedOn' => $ticket->CreatedOn,
				'CreatedBy' => $ticket->CreatedBy	
					));
					
			if($pull)
			{		
				$update = $onlinedb->update('ticket_details',array('Downloaded'=>'1'),array('id' => $ticket->id, 'AgencyID' => $ticket->AgencyID));
			}
			if($update){$i++;}
		}echo "$i ".Kohana::lang('backend.remote_tickets_downloaded');
	}
	
	public function push_cash_to_bank()
	{	
		$onlinedb = new Database('online_db');
		$offlinedb = new Database('offline_db');
		
		$unpushed_cash_to_bank = Cash_To_Bank_Model::get_all_unpushed($this->agency_id);
	
		foreach($unpushed_cash_to_bank as $trx){
						
						//var_dump($trx);exit;
						//ORM removed here as it might be troublesome to pick another db and table with it.
						$cash_to_bank_pushed_online = $onlinedb->insert('cash_to_banks',array(
										
								'id' => $this->agency_id.$trx->id,
								'agency_id' => $this->agency_id,
								'bank_name' => $trx->bank_name,
								'account_number' => $trx->account_number,
								'amount' => $trx->amount,
								'purpose' => $trx->purpose,
								'date_incurred' => $trx->date_incurred,
								'created_by' => $trx->created_by,
								'created_on' => $trx->created_on,
								'deleted' => $trx->deleted
								
								));
						
						//change the online state to 1
						if($cash_to_bank_pushed_online)
						{	
							//Pick out the cash_to_bank from the transactions table and ENSURE that 'online' is set to 1 once done,
							//so next time, it's not uploaded again
						
							//IDentification is by ticket id and agencyid
							$updatetrx = $offlinedb->update('cash_to_banks',array('online'=>'1'),array('id' => $trx->id, 'agency_id' => $trx->agency_id));
						}
			}
				echo Kohana::lang('backend.cash_to_bank_online');	
	}
	
	public function push_income()
	{	
		$onlinedb = new Database('online_db');
		$offlinedb = new Database('offline_db');
		
		$unpushed_income = Income_Model::get_all_unpushed($this->agency_id);
	
		foreach($unpushed_income as $trx){
						
						
						//ORM removed here as it might be troublesome to pick another db and table with it.
						$income_online = $onlinedb->insert('incomes',array(
										
								'id' => $this->agency_id.$trx->id,
								'agency_id' => $this->agency_id,
								'bus_number' => $trx->bus_number,
								'rented_to' => $trx->rented_to,
								'schedule_id' => $trx->schedule_id,
								'amount' => $trx->amount,
								'purpose' => $trx->purpose,
								'description' => $trx->description,
								'date_incurred' => $trx->date_incurred,
								'start_date' => $trx->start_date,
								'end_date' => $trx->end_date,
								'authorised_by' => $trx->authorised_by,
								'CreatedBy' => $trx->CreatedBy,
								'CreatedOn' => $trx->CreatedOn,
								'deleted' => $trx->deleted
								
								));
						
						//change the online state to 1
						if($income_online)
						{	
							//Pick out the cash_to_bank from the transactions table and ENSURE that 'online' is set to 1 once done,
							//so next time, it's not uploaded again
						
							//IDentification is by ticket id and agencyid
							$updatetrx = $offlinedb->update('incomes',array('online'=>'1'),array('id' => $trx->id, 'agency_id' => $trx->agency_id));
						}
			}
				echo Kohana::lang('backend.income_online');	
	}
	
	
	/*
		Avoid testing reservations with the offline app. Reservations maintain the ID from online, so using both the online and offline will
		cause duplicate reservation IDs which will then overwrite.
	*/
	public function pull_reservation_requests()
	{
		
		$onlinedb = new Database('online_db');
		$offlinedb = new Database('offline_db');
		
		//Get tickets online where the RemoteTicketAgencyFrom is this agency.
		//$unpulled_ticket_reservations = $onlinedb->from('reservation_requests')->where('reservesd_seats', "! ")->getwhere();
		$unpulled_reservation_requests = $onlinedb->query("SELECT * FROM reservation_requests WHERE agency_id=".$this->agency_id." AND reserved_seats !='' ");

		
		
		$i=0;
		foreach($unpulled_reservation_requests as $request)
		{	

			$entry_exists = $offlinedb->query("SELECT 1 FROM reservation_requests WHERE agency_id=".$request->agency_id." AND id=".$request->id);
			$entry_exists=count($entry_exists);
			
			//if reservation was made for that schedule, update it. If not, insert it
			if($entry_exists){
				//if some seats are already avaialable in the seats and reservers tab, decode it and add them, rather than overwrite
				$offline_reservation_requests = ORM::FACTORY('reservation_request')->where('id',$request->id)->find();
				//var_dump($offline_reservation_requests->reserved_seats);exit;
				//decode and convert offline reservations pending and new online to an array
				$offline_reservation_requests = json_decode($offline_reservation_requests->reserved_seats,true);
				$online_reservation_requests = json_decode($request->reserved_seats,true);
				$new_offline_reservation_requests = array_merge($offline_reservation_requests,$online_reservation_requests);
				
				
				$new_offline_reservation_requests = json_encode($new_offline_reservation_requests);
				//var_dump($new_offline_reservation_requests);
				//var_dump($request->reserved_seats_permanent);
				$pull = $offlinedb->update('reservation_requests',array(
				
				'reserved_seats' => $new_offline_reservation_requests,
				'reserved_seats_permanent' => $request->reserved_seats_permanent
					
					), array('id'=>$request->id));
			}else{
			
				//schedule id offline is from the 4th character of online schedule id upwards
				$offline_schedule_id = substr($request->schedule_id,3);
				$pull = $offlinedb->insert('reservation_requests',array(
					
				//offline maintains the id of the online reservation			
				'id' => $request->id,
				'schedule_id' => $offline_schedule_id,
				'agency_id' => $request->agency_id,
				'reserved_seats' => $request->reserved_seats,
				'reserved_seats_permanent' => $request->reserved_seats_permanent
					
					));
			
			}
			
			if($pull)
			{	//when it's pulled, delete the reservation requests columnn online so it's not pulled twice. 	
				$update = $onlinedb->update('reservation_requests',array('reserved_seats'=>''),array('id' => $request->id));
				
				$i++;
			}
			
		}echo "$i ".Kohana::lang('backend.reservations_downloaded');
	}
	
	public function check_internet(){
		
		 $connected = @fsockopen("dev.quickticket.cm", 80); 
                                  //website, port  (try 80 or 443)
		if ($connected){
			//$this->internet_available = true; 
			fclose($connected);
			echo "1";
		}else{
			//$this->internet_available = false; 
			echo "0";
		}
		//var_dump($is_conn);
		
	}
	
}
