<?php defined('SYSPATH') or die('No direct script access'); ?> 


	<?php
		//determine what type of notice to display if at all
		$notice = $this->session->get_once('notice');
			if(!empty($notice)){ 
				if($notice['type'] == 'success'){?><div class="alert general success"><a class="close" data-dismiss="alert" href="#">&times;</a><?php echo $notice['message']; }?></div>
			<?	if($notice['type'] == 'error'){?><div class="alert general error"><a class="close" data-dismiss="alert" href="#">&times;</a><?php echo $notice['message']; }?></div>
		<?}?>
    <div class="container">
	  	
	<div class="row-fluid">	
		
		<div class="span12  hero-unit prof-unit">
		
		<ul class="nav nav-tabs">
					<li > <a href="<?=url::site('parcels/all/sent')?>"><i class="icon-arrow-left"></i> <?=Kohana::lang('backend.back_parcels')?></a></li>
		</ul>
		
		<div class="row-fluid span8 offset2 print-area">	
				
				
				<div class="tab-pane" id="add-parcel">
								<div class="span8 offset2">
							<form action="<?=url::site('parcels/edit/'.$parcel->id)?>" method="POST" class="theform">
							<?php //no_csrf::set(1)?>
							
					<legend><?=Kohana::lang('backend.reg_parcels')?></legend>
						<?php
							//if he can manage mails and parcesl at the same time, activate the radio buttons so he choses
							//else disactivate both and show him the one he's authorised to manage
							if($is_allowed_manage_mails AND $is_allowed_manage_baggage){
						?>	
						<div class="">
							<div class="span4 text-right">Type:</div>
							<div class="span2"><input type="radio" name="parcel_type" value="mail" id="mail_radio" <?php if($parcel->type =='mail'){echo 'checked = "checked"';}?> /> <?=Kohana::lang('backend.mail')?></div>
							<div class="span3"><input type="radio" name="parcel_type" value="baggage" id="baggage_radio" <?php if($parcel->type =='baggage'){echo 'checked = "checked"';}?> /> <?=Kohana::lang('backend.baggage')?> </div>
						</div>
						<?php }else{ ?>
						<div class="">
							<div class="span4 text-right">Type:</div>
							<div class="span3"><input type="radio" name="parcel_type" value="mail" id="mail_radio" <?php if($is_allowed_manage_mails){echo 'checked = "checked"';} ?> disabled/> <?=Kohana::lang('backend.mail')?></div>
							<div class="span3"><input type="radio" name="parcel_type" value="baggage" id="baggage_radio" <?php if($is_allowed_manage_baggage){echo 'checked = "checked"';} ?> disabled/> <?=Kohana::lang('backend.baggage')?> </div>
						</div>
						<?php }?>
						<div>
							<div class="span4 text-right">To: </div>
								<div class="span8">
								<select name="agency_to" id="inputType">
											<?php foreach ($siblings as $sib):?>
											<option value="<?=$sib->id?>" <?php if($sib->id==$parcel->to){echo "selected";}?>><?=$sib->name?></option>
											<?php endforeach;?>
								</select>
								</div>
						</div>
						
						<div >
							<div class="span4 text-right"><?=Kohana::lang('backend.sender')?>: </div>
							<div class="span8"><input type="text" placeholder="<?=Kohana::lang('backend.sender_name')?>"  name="sender_name" value="<?=$parcel->sender_name?>"></div>
						</div>
						
						<div >
							<div class="span4 text-right"><?=Kohana::lang('backend.receiver')?>: <span class="red"> *</span></div>
							<div class="span8"><input type="text" placeholder="<?=Kohana::lang('backend.rec_name')?>"  name="receiver_name" value="<?=$parcel->receiver_name?>" required></div>
						</div>
						<div >
							<div class="span4 text-right"><?=Kohana::lang('backend.phone')?>: </div>
							<div class="span8"><input type="text" placeholder="<?=Kohana::lang('backend.rec_phone')?>"  name="receiver_phone" value="<? /*If not, 0 will come for empty phone numbers*/ if(!empty($parcel->receiver_phone)){echo $parcel->receiver_phone;}?>"></div>
						</div>
						<div>
							<div class="span4 text-right">Description: <span class="red"> *</span></div>
							<div class="span8"><textarea placeholder="<?=Kohana::lang('backend.desc_parcel')?>"  name="parcel_description" style="height:50px;" required><?=$parcel->description?></textarea></div>
						</div>
						<div>
							<div class="span4 text-right"><?=Kohana::lang('backend.price')?>: <span class="red"> *</span> </div>
							<div class="span8 input-append"><input type="text" class="IsAmount span5" placeholder="<?=Kohana::lang('backend.price')?>"  name="price" value="<?=$parcel->price?>" required><span class="add-on"> FCFA</span></div>
						</div>
						<div class="clear"></div>
						<div class="form-actions">
							<div class="span4"></div>
							<div class="span8">
								<button type="submit" class="btn btn-info"><?=Kohana::lang('backend.save')?></button>
								<a href="<?=url::site('parcels/all/sent')?>" class="btn" type="submit"> <?=Kohana::lang('backend.cancel')?></a>
							</div>
						</div>
						
							
							</form>		
						</div>

						<!--end the first tab -->
				</div>
						
		</div>
	
		
		</div>
	
    </div> 

	</div>
	
	<script type="text/javascript">

	
	</script>