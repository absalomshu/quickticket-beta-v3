<?php defined('SYSPATH') or die('No direct script access');

class Online_Purchase_Model extends ORM{

	public function add_ticket($agency_id, $schedule_id, $seat_number, $client_name, $price, $missed_status=0, $free_ticket, $free_ticket_reason, $created_by)
	{	
		$ticket = ORM::FACTORY('ticket_detail');
		$ticket->AgencyID = $agency_id;
		$ticket->ScheduleID = $schedule_id;
		$ticket->SeatNumber = $seat_number;
		$ticket->ClientName = $client_name;
		$ticket->Price = $price;
		$ticket->MissedStatus = $missed_status;
		$ticket->FreeTicket = $free_ticket;
		$ticket->FreeTicketReason = $free_ticket_reason;
		$ticket->CreatedBy = $created_by;
	
		$ticket->save();
		return $ticket->id;
	}
	
	public function get_all($agency_id)
	{
		$purchases=ORM::factory('online_purchase')->where('agency_id',$agency_id)->find_all();
		return $purchases;	
	}
	
	public function get_all_active($agency_id)
	{
		$purchases=ORM::factory('online_purchase')->where('agency_id',$agency_id)->where('status','active')->find_all();
		return $purchases;	
	}
	
	public function get_all_nonactive($agency_id)
	{
		$purchases=ORM::factory('online_purchase')->where('agency_id',$agency_id)->where('status!=','active')->find_all();
		return $purchases;	
	}
	
	
	public function mark_closed($agency_id,$purchase_id,$admin_id)
	{
		$purchase=ORM::factory('online_purchase')->where('agency_id',$agency_id)->where('id',$purchase_id)->find();
		$purchase->status = 'closed';
		$purchase->ModifiedBy = strtoupper($admin_id);
		$purchase->save();
		
		$notice="Purchase confimed and marked closed";
		return $notice;	
	}
	
	public function safe_delete_online_purchase($agency_id, $purchase_id)
	{
		$purchase=ORM::factory('online_purchase')->where('agency_id',$agency_id)->where('id',$purchase_id)->find();
		$purchase->status = 'cancelled';
		$purchase->save();
	
		$notice="Purchase has been deleted";
		return $notice;	
	}
	


	

}
