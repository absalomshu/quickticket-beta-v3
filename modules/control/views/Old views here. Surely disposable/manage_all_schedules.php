<?php defined('SYSPATH') or die('No direct script access'); ?> 

	<?php
		//determine what type of notice to display if at all
		$notice = $this->session->get_once('notice');
			if(!empty($notice)){ 
				if($notice['type'] == 'success'){?><div class="alert general success"><a class="close" data-dismiss="alert" href="#">&times;</a><?php echo $notice['message']; }?></div>
			<?	if($notice['type'] == 'error'){?><div class="alert general error"><a class="close" data-dismiss="alert" href="#">&times;</a><?php echo $notice['message']; }?></div>
		<?}?>
 
	  
	  <div class="row-fluid marketing" style="margin:5px 0 0px 0px;">
	  
	<div class="row-fluid marketing" style="margin:5px 0 0px 0;">	
		<div class="span12 hero-unit prof-unit reload" style="margin-left:0px;">
	

				<ul class="nav nav-tabs">
				<?php if ($this->uri->segment(3)=='current') {?>
					<li class="active"> <a href=""> <i class="icon-time "></i> <?=Kohana::lang('backend.current')?> </a></li>
					<li><a href="<?=url::site('control/all_schedules/departed')?>"> <i class="icon-ok-sign "></i> <?=Kohana::lang('backend.departed')?> </a></li>
				<?php	}else { ?>
					<li><a href="<?=url::site('control/all_schedules/current')?>"> <i class="icon-time "></i> <?=Kohana::lang('backend.current')?> </a></li>
					<li class="active"><a href="#"> <i class="icon-ok-sign "></i> <?=Kohana::lang('backend.departed')?> </a></li>
					<?php } ?>
				</ul>
				
				<table class="table table-bordered">
					<tr>
						<th>Date</th>
						<th><?=Kohana::lang('backend.bus_no')?></th>
						<th><?=Kohana::lang('backend.from')?></th>
						<th><?=Kohana::lang('backend.to')?></th>
						<th><?=Kohana::lang('backend.time')?></th>
					</tr>
					<?php foreach ($schedules as $schedule):?>
					<tr>
						<td><?=$schedule->departure_date?></td>
						
						<td>
							<!--make a link only if it's a simple admin lest a manager should go to a current schedule-->
								<a href="<?php 
									if ($this->uri->segment(3)=='current') { 
										echo url::site('control/complete_schedule').'/'.$schedule->id;
									}
									else{echo url::site('control/past_schedule').'/'.$schedule->id;}?>">
									
							<?=$schedule->bus_number?></a></td> 
						
						<td><?=get::town($schedule->from)?></td>
						<td><?=get::town($schedule->to)?></td>
						<td><?=$schedule->checked_out_time?></td>
					</tr>
					<!--<input type="hidden"  class="schedule_id" value="<?=$schedule->id?>"></input>-->
					<?php endforeach; ?>
				</table>
					<?php echo $this->pagination;?>
<?php $this->profiler = new Profiler();?>
		</div>
	
		
		</div>
	  </div>
	  <div style="height:19px;"></div>

