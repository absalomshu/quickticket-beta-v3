<?php defined('SYSPATH') or die('No direct script access');

//For each update to the database, copy the php code converted from sql by phpmyadmin and store in 

//NB:/ UPON EVERY NEW RELEASE, FLUSH, TURN OFF THE SUPERCACHE, OPEN THE UPDATE FILE DIRECTLY IN THE BROWSER. SEEMS TO HAVE THE EFFECT OF REFRESHING THE FILE,
// POSITIVELY PREVENTING THE CACHING EFFECT 

class Update_Controller{
	
	public $host = "http://dev.quickticket.co/updates/";
	public $notice = ""; 
	
	 
	public function index()
	{
		$this->update_filelist();
	}

	public function update_filelist()
	{

		//copy entire contents of files and replace local content as the download option passes control to the browser which we don't want
		//works for files and images
		
		//name of the file holding the list of files to be downloaded
		//in the versions folder, the one for today has filename, the date of today
		$OnlineFileWithList = $this->host.'versions/'.date('Ymd').'.txt';
		
		//write to the local file, the list. if local_file doesn't exist, create it.
		$OfflineFileWithList = "update_versions_client/".date('Ymd').".txt";

		//var_dump($OnlineFileWithList);exit;
		
		$url  = $OnlineFileWithList;
		$path = $OfflineFileWithList;
	 
		$ch = curl_init($url);
		
		//store in a php variable so it's not output directly into the browser
		curl_setopt($ch, CURLOPT_RETURNTRANSFER, true);
	 
		$data = curl_exec($ch);
		
		var_dump($data);exit;
	 
		curl_close($ch);
	 
		//put the content into the offline file and ensure proper reporting
		//@ is to suppress the custom php error, and return the error defined in the else below
		if (file_put_contents($path, $data)){
			$this->notice .= "File list successfully updated <br/>";
		}else{
			$this->notice .= "Problem updating file list. Click <a href=".url::site('update').">here</a> to try again. If the probem persists, contact the administrator";
			echo $this->notice;exit;
		}
		
		//now, update the respective files
		$this->curl_update_files();

}
	
	public function curl_update_files()
	{	
			
		$OfflineFileWithList = "update_versions_client/".date('Ymd').".txt";
		
		//get the list of files concerned as an array
		//it's EXTREMELY IMPORTANT, in fact IMPECCABLE to remove the newline after each line, else it's attached to the filename and returns an error
		$update_list = str_replace("\n","",file($OfflineFileWithList));
		
		//for each file in the list, pick it out and overwrite the local copy
		foreach($update_list as $array_key => $file){
				
				
				$url  = $this->host.$file;
				$path = $file;
			 
				$ch = curl_init($url);
				curl_setopt($ch, CURLOPT_RETURNTRANSFER, true);
			 
				$data = curl_exec($ch);
			 
				curl_close($ch);
				
				//put into the offline file, the online file.
				
				if (@file_put_contents($path, $data)){
					$this->notice .= "$file successfully updated<br/>";
				}else{
					$this->notice .= "Problem updating $file. Click <a href=".url::site('update').">here</a> to restart the update. If the problem persists, contact the administrator";
					echo $this->notice;exit;
				}
		}
			echo "<b>Update report</b><br/>" . $this->notice;
		}
		

	
	public function update_files()
	{	
			
	$local_file = "update_versions/".date('Ymd').".txt";
	
	//get the list of files concerned as an array
	$update_list = file($local_file);
	
	
	foreach($update_list as $array_key => $file){
	
			//for each file
			//go to file location ONline, open the file, copy the content		
			//(specifically from the package in the updates directory, as the main package would be unacessible from outside access)
			$content = file_get_contents($this->host.$file);
			
			//var_dump($file);
			
			
			//go to file location OFFline, open the file, overwrite the content
			$handle = fopen($file,"w") or die ('Can\'t open file');
			
			//DO THE OVERWRITE
			fwrite($handle,$content) or die("Couldn't write to file");
			fclose($handle);
			
			$report .= "$file written";
	}
	
			var_dump($report);
	}
	
	
	
		
		
		}