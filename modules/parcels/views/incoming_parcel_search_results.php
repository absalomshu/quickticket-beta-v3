<?php defined('SYSPATH') or die('No direct script access'); ?>
	
    <div class="container">
		
		
		<div class="row-fluid  span12 hero-unit prof-unit reload" style="margin:5px 0 0px 0;">	
			<div class="span12" >
			<div class="no-print">
			<ul class="nav nav-tabs">
					<li > <a href="<?=url::site('parcels/check_parcels')?>"><i class="icon-arrow-left"></i> <?=Kohana::lang('backend.back_parcels')?></a></li>
				</ul>
			</div>
			
			<div class="span8 offset2 print-area" >
				<legend><?=Kohana::lang('backend.search_results') . "  for "?><i><?="\"".$passenger_name."\""?></i><button class="view" onClick="window.print()"><i class="icon-print"></i> <?=Kohana::lang('backend.print')?></button></legend>
				<?php if(count($incoming_search_results)!=0){?>
				<table class="table table-bordered table-condensed">
					<tr><th>Incoming parcels</th></tr>
					<tr>
						<th>Sent date</th>
						<th>Receiver</th>
						<th>Receiver phone</th>
						<th>Parcel ID</th>
					
					</tr>
					<?php 
					foreach ($incoming_search_results as $result)
					{
						?>
					<tr>
						<td ><?=date("d-m-Y",strtotime($result->CreatedOn))?></td>
						<td ><b><?=$result->receiver_name?></b></td>
						<td ><?=$result->receiver_phone?></td>
						<td ><a href="<?=url::site('parcels/view_incoming/'.$result->id);?>"><?="PC".$result->global_parcel_id?></a></td>
						
					</tr>
					<?php }?>
					
				</table>
				<?php } else echo "No incoming parcel found.";?>
				<br>
				<br>
				
			</div>
			
		</div>
			
		
		
		
		
		
		
		</div>
    </div> 
