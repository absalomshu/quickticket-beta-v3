<?php defined('SYSPATH') or die('No direct script access'); ?> 


	<?php
		//determine what type of notice to display if at all
		$notice = $this->session->get_once('notice');
			if(!empty($notice)){ 
				if($notice['type'] == 'success'){?><div class="alert general success"><a class="close" data-dismiss="alert" href="#">&times;</a><?php echo $notice['message']; }?></div>
			<?	if($notice['type'] == 'error'){?><div class="alert general error"><a class="close" data-dismiss="alert" href="#">&times;</a><?php echo $notice['message']; }?></div>
		<?}?>
    <div class="container">
	  
	  <div class="row-fluid marketing" style="margin:5px 0 0px 0px;"> 
			<div class="span7 hero-unit prof-unit reload" style="margin-left:0px;">
				<div class="heading">
				<div class="heading">
				<!-- If it's a General Manager, viewing the genral account don't show a town. Just show the parent name-->
				<?php 
					if($current_branch == 0){
						echo get::_parent($this->admin->agency_id)->name;
					}else{
						//echo get::_parent($this->admin->agency_id)->name." ".get::admins_town($this->admin->agency_id)->name;
						echo get::agency_name($current_branch);
					}
				?> | <?=Kohana::lang('backend.current_schedules')?>
			</div>
				</div>
				<div class="rule"><hr/></div>
					<table class="table">
					<?php 
				
					$no_current_schedules = count($current_schedules);
					
						if($no_current_schedules != 0){
					foreach ($current_schedules as $c_schedule):?>
					<tr><td><?=$c_schedule->bus_number?> : <b><?=get::town($c_schedule->from)?></b> -> <b><?=get::town($c_schedule->to)?></b> : <b><?=date("g:i A", strtotime($c_schedule->departure_time))?></b><i> <span style="font-size:11px;">  (<?=Kohana::lang('backend.loading')?>...) </span> </i><span style="float:right;font-size:12px;font-weight:normal;"></span></td></tr>
					<?php endforeach; }else{
						echo "No current schedules";
					} ?>
				</table>
			</div>
			<div class="span5 hero-unit prof-unit">
				<div class="heading"><?=Kohana::lang('backend.recently_checkedout')?><!--<a href="<?=url::site('control/all_schedules/departed')?>" class="view">See all </a>--></div>
				<div class="rule"><hr/></div>
				
					<table class="table table-striped">
						<?php 
						$no_departed_schedules = count($departed_schedules);
						
						
						
						if($no_departed_schedules != 0){
						foreach ($departed_schedules as $d_schedule):
									//var_dump($d_schedule);		

						?>
						
									
							<?php 
							//when the join is done for the GM, there's a conflict between the schedule->id and agency_id, and the 
							//agency-> id overwrites the other. hence $d_schedule->id in that case would rather be the agency id.
							//so the join uses schedID as an alias for schedule->id in that case.
							
							//HAS BEEN TROUBLESOME SO I USE JUST THE FIRST LINE FOR A WHILE TO SEE OUTCOME
							
							//if ($user_group==3){
								$necessaryID = $d_schedule->id;
						//	}else{
							//	$necessaryID = $d_schedule->schedID;
							//}
							?>
							
						<tr><td class="recent"><a href="<?=url::site('control/past_schedule').'/'.$necessaryID?>"><?=$d_schedule->bus_number?></a> : <b><?=get::town($d_schedule->from)?></b> -> <b><?=get::town($d_schedule->to)?></b> <?=Kohana::lang('backend.at')?> <b><?=date("g:i A", strtotime($d_schedule->checked_out_time))?></b><span style="float:right;font-size:12px;font-weight:normal;"><a href="<?=url::site('control/past_schedule').'/'.$necessaryID?>">View</a></span></td></tr>
						<?php endforeach; }else{
							echo "No departed schedules";
						} ?>
					</table>					
				
		</div>	
		<div class="span7 hero-unit prof-unit" style="margin-left:0px;" >
		<form action="<?=url::site('control/schedule_by_date/')?>" method="POST">
				<div class="heading"><?=Kohana::lang('backend.branch_activity_date')?><!--<a href="#" class="view">See all activity</a>--></div>
				<div class="rule"><hr/></div>
				
				
					
					<div class="input-append date left">
						<input type="text" name = 'date' class="span7 datepicker"  value="<?=date('d-m-Y')?>" id="dp1" style="font-size:18px;">
						<span class="add-on"><i class="icon-th"></i></span>
					</div>				
					&nbsp;	&nbsp;	&nbsp;	&nbsp;	&nbsp;	&nbsp;	&nbsp;			<button class="btn  btn-info" type="submit"  style="height:40px; width:140px;font-size:20px;" ><i class="icon-search icon-white"></i> Check</button><br/>
					
		</form>		
		</div>
		<div class="span5 hero-unit prof-unit" >
				
				
		</div>
		
	
		
		
	  </div>
	  <div style="height:19px;"></div>
    </div> 
	<?php //$this->profiler = new Profiler();?>

<script type="text/javascript">
		$(document).ready(function(){
		$('.input-append.date').datepicker({
	format: "dd-mm-yyyy",
    todayBtn: "linked",
    autoclose: true,
    todayHighlight: true
});
	})
	
</script>
