<?php defined('SYSPATH') or die('No direct script access'); ?>
	
    <div class="container">
		<div class="notice">
		<?php
		//determine what type of notice to display if at all
		$notice = $this->session->get_once('notice');
		if(!empty($notice))
		{ 
					if($notice['type'] == 'success'){?><div class="alert general success"><a class="close" data-dismiss="alert" href="#">&times;</a><?php echo $notice['message']; }?></div>
				<?	if($notice['type'] == 'error'){?><div class="alert general error"><a class="close" data-dismiss="alert" href="#">&times;</a><?php echo $notice['message']; }?></div><?
		} ?>
		</div>
		
		<div class="row-fluid marketing" style="margin:5px 0 0px 0;">	
		
			
			
			<div class="span12" style="float:left;">
			
			<?php 
			//instantiate variable to total the prices in the loop below
				$parcel_total = 0;
				foreach ($parcels as $parcel)
				{
				$parcel_total += $parcel->price;
				}
			?>
			
			
			<div class="span12 hero-unit prof-unit reload" style="margin-left:0px;">
				<div class="heading"><?=Kohana::lang('backend.summary_from')?> <?=date("d-m-Y",strtotime($start_date)) ." " .Kohana::lang('backend.to')." " . date("d-m-Y",strtotime($end_date))?></div>
				<div class="rule"><hr/></div>
				<table class="table table-bordered">
					<tr><th><?=Kohana::lang('backend.schedules')?></th></tr>
					<?php 
					foreach ($period_schedules as $sched)
					{?>
					<tr>
						<td class="span2"><?=date("d-m-Y",strtotime($sched->departure_date))?></td>
						<td><?=$sched->bus_number." (".get::town($sched->from). "-". get::town($sched->to).")"?></td>
						<td></td>
						<td class="text-right"><?=number_format($sched->total_amount)?></td>
					</tr>
					<?php }?>
					<tr>		
						<td class="span2"></td>
						<th ><?=Kohana::lang('backend.total_ticket_sales')?></th>
						<td class="span2"></td>
						<th class="span2 text-right"><?=number_format($total)?> FCFA</th>
					</tr>
				</table>
				
				<table class="table table-bordered">
					<tr><th><?=Kohana::lang('backend.expenses')?></th></tr>
					<?php
					foreach ($period_expenses as $exp)
					{?>
					<tr>
						<td class="span2"><?=date("d-m-Y",strtotime($exp->date_incurred))?></td>
						<td><?=$exp->purpose?></td>
						<td class="text-right" ><?=number_format($exp->amount)?></td>
						<td></td>
					</tr>
					<?php }?>
					<tr>		
						<td></td>
						<th><?=Kohana::lang('backend.total_expenditure')?></th>
						<th class="span2 text-right"><?=number_format($total_expenditure)?> FCFA</th>
						<td class="span2"></td>
					</tr>
				</table>	
					
				<table class="table table-bordered">	
					<tr><th><?=Kohana::lang('backend.parcels')?></th></tr>
					<?php 
					foreach ($period_parcels as $parcel)
					{?>
					<tr>
						<td class="span2"><?=date("d-m-Y",strtotime($parcel->sent_date))?></td>
						<td><?=$parcel->description?></td>
						<td></td>
						<td class="text-right"><?=number_format($parcel->price)?></td>
					</tr>
					<?php }?>
					
					
					<tr>		
						<td class="span2"></td>
						<th><?=Kohana::lang('backend.total_from_parcels')?></th>
						<td class="span2"></td>
						<th class="span2 text-right"><?=number_format($parcel_total)?> FCFA</th>
					</tr>
				</table>
				<table class="table table-bordered">					
					<tr>		
						<td><h3><?=Kohana::lang('backend.net_income')?></h3></th>
						<td class="text-right"><h3><?=number_format($total + $parcel_total - $total_expenditure)?> FCFA</h3></td>
					</tr>
				</table>
			</div>
			</div>
			
		
		
		
		
		
		
		</div>
    </div> 
