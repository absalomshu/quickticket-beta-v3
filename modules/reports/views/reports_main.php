<?php defined('SYSPATH') or die('No direct script access'); ?> 

	<div class="row-fluid ">
	  	<div class="span12 hero-unit prof-unit" >
		
		<ul class="nav nav-tabs" id="myTab">
					<li class="active"><a href="#all-reports"> <i class="icon-money"></i> <?=Kohana::lang('backend.income_and_expenses')?></a></li>
					<li><a href="#admin-reports"><i class="icon-user"></i> <?=Kohana::lang('backend.admins')?></a></li>
				
					<!--<li><a href="#parcels"><i class="icon-gift"></i> <?=Kohana::lang('backend.parcels')?></a></li>-->
					<!--<li><a href="#collection"><i class="icon-align-justify"></i> <?=Kohana::lang('backend.collection')?></a></li>-->
					<li><a href="#route-reports"><i class="icon-road"></i> <?=Kohana::lang('backend.routes')?></a></li>
						<li><a href="#passenger-reports"><i class="icon-briefcase"></i> <?=Kohana::lang('backend.passengers')?></a></li>
		</ul>
		
		<div class="tab-content">
			<div class="tab-pane active " id="all-reports" >
				<div class="span8 offset2">
					<form action="<?=url::site('reports/activity_by_period')?>" method="POST" >
						
						<legend><?=Kohana::lang('backend.income_and_expenses_report')?></legend>	
							<div>
							<div class="span4 text-right"><?=Kohana::lang('backend.from');?><span class="red"> *</span>:</div>
							<div class="span8">
								<div class="input-append date span6">
									<input type="text" name = 'start_date' class="span8 datepicker"  value="<?=date('d-m-Y')?>" id="dp1" >
									<span class="add-on"><i class="icon-calendar"></i></span>
								</div>	
							</div>
							</div>
							
							<div>
								<div class="span4 text-right"><?=Kohana::lang('backend.to');?><span class="red"> *</span>:</div>
								<div class="span8">
									<div class="input-append date span6">
										<input type="text" name = 'end_date' class="span8 datepicker"  value="<?=date('d-m-Y')?>" id="dp1" >
										<span class="add-on"><i class="icon-calendar"></i></span>
									</div>	
								</div>
							</div>
							
							<div class="">
								<div class="span4 text-right"><?=Kohana::lang('backend.output_format')?><span class="red"> *</span>: </div>
								<div class="span8">
									<select name="output_format" id="inputType">
											<option value="grouped">Grouped</option>
											<option value="daily_total">Daily total</option>
											<option value="percentage">Percentage - Pie chart</option>
											<option value="compare">Compare - Line chart</option>
											
								</select>
								
								</div>
							</div>
							
							
							<div class="clear"></div>
							<div class="form-actions">
								<div class="span4"></div>
								<div class="span8">
									<button class="btn  btn-info" type="submit" ><i class="icon-search icon-white"></i> <?=Kohana::lang('backend.view');?></button><br/>
							
								</div>
							</div>
					</form>	
				</div>	
					
			</div>  
			
			<div class="tab-pane" id="admin-reports">
				<div class="span8 offset2">
				
					<legend><?=Kohana::lang('backend.admin_activity')?><!--<a href="#" class="view">See all activity</a>--></legend>						
					<form action="<?=url::site('reports/activity_by_period_by_admin/')?>" method="POST" >
						<div class="">
							<div class="span4 text-right"><?=Kohana::lang('backend.admin')?><span class="red"> *</span>: </div>
							<div class="span8">
								<select name="admin" id="inputType">
								
									<?php 
										foreach ($admins as $admin){?>
										<option value="<?=$admin->username?>"><?=$admin->username?></option>
									<?php }; ?>
								
							</select>
							
							</div>
						</div>
					
						<!--
							<div class="span12"><?=Kohana::lang('backend.on');?>:</div>
							<div class="input-append date span4">
								<input type="text" name = 'date' class="span8 datepicker"  value="<?=date('d-m-Y')?>" id="dp1" >
								<span class="add-on"><i class="icon-calendar"></i></span>
							</div>				
							<button class="btn  btn-info" type="submit" name="by_date"  ><i class="icon-search icon-white"></i> <?=Kohana::lang('backend.view');?></button><br/>
							
							<div class="clear"></div>
								<legend></legend>
						-->
							<div>
								<div class="span4 text-right"><?=Kohana::lang('backend.from');?><span class="red"> *</span>:</div>
								<div class="span8">
									<div class="input-append date span6">
										<input type="text" name = 'start_date' class="span8 datepicker"  value="<?=date('d-m-Y')?>" id="dp1" >
										<span class="add-on"><i class="icon-calendar"></i></span>
									</div>	
								</div>
							</div>
							
							<div>
								<div class="span4 text-right"><?=Kohana::lang('backend.to');?><span class="red"> *</span>:</div>
								<div class="span8">
									<div class="input-append date span6">
										<input type="text" name = 'end_date' class="span8 datepicker"  value="<?=date('d-m-Y')?>" id="dp1" >
										<span class="add-on"><i class="icon-calendar"></i></span>
									</div>	
								</div>
							</div>
						
							<div class="clear"></div>
							<div class="form-actions">
								<div class="span4"></div>
								<div class="span8">
									<button class="btn  btn-info" type="submit" name="by_period" ><i class="icon-search icon-white"></i> <?=Kohana::lang('backend.view');?></button><br/>
								
								</div>
							</div>
							
					</form>	
					
					
					<legend>Ranking (all administrators, by income & expenses)</legend>						
						<form action="<?=url::site('reports/activity_by_period_by_admin/')?>" method="POST" >
						
						<div class="">
							<div class="span4 text-right"><?=Kohana::lang('backend.from')?><span class="red"> *</span>: </div>
							<div class="span8">
								<div class="input-append date span6">
								<input type="text" name = 'start_date' class="span8 datepicker"  value="<?=date('d-m-Y')?>" id="dp1" >
								<span class="add-on"><i class="icon-calendar"></i></span>
								</div>	
							</div>
						</div>
						<div class="">
							<div class="span4 text-right"><?=Kohana::lang('backend.to')?><span class="red"> *</span>: </div>
							<div class="span8">
								<div class="input-append date span6">
								<input type="text" name = 'end_date' class="span8 datepicker"  value="<?=date('d-m-Y')?>" id="dp1" >
								<span class="add-on"><i class="icon-calendar"></i></span>
								</div>	
							</div>
						</div>
						
						<div class="clear"></div>
						<div class="form-actions">
							<div class="span4"></div>
							<div class="span8"><button class="btn  btn-info" type="submit" name="compare_admins" ><i class="icon-search icon-white"></i> <?=Kohana::lang('backend.view');?></button>
							</div>
						</div>
					</form>	
				</div>	
					
			</div>	
			
			<div class="tab-pane" id="route-reports">
				<div class="span8 offset2">
				
					<legend><?=Kohana::lang('backend.route_activity')?><!--<a href="#" class="view">See all activity</a>--></legend>						
						<form action="<?=url::site('reports/route/')?>" method="POST" >
						<div class="">
							<div class="span4 text-right"><?=Kohana::lang('backend.town_from')?><span class="red"> *</span>: </div>
							<div class="span8">
								<select name="town_from" id="inputType">
										<!--<option value="<?php //$town->id?>"><?php //$town->name?></option>-->
										<option value="<?=$this->agency_town_id?>" selected><?=$this->agency_town?></option>
									
								</select>
							
							</div>
						</div>
						<div class="">
							<div class="span4 text-right"><?=Kohana::lang('backend.town_to')?><span class="red"> *</span>: </div>
							<div class="span8">
								<select name="town_to" id="inputType">
										<?php foreach ($towns as $town):?>
										<option value="<?=$town->id?>"><?=$town->name?></option>
										<?php endforeach;?>
									
								</select>
							
							</div>
						</div>
					
						
							<div>
								<div class="span4 text-right"><?=Kohana::lang('backend.from');?><span class="red"> *</span>:</div>
								<div class="span8">
									<div class="input-append date span6">
										<input type="text" name = 'start_date' class="span8 datepicker"  value="<?=date('d-m-Y')?>" id="dp1" >
										<span class="add-on"><i class="icon-calendar"></i></span>
									</div>	
								</div>
							</div>
							
							<div>
								<div class="span4 text-right"><?=Kohana::lang('backend.to');?><span class="red"> *</span>:</div>
								<div class="span8">
									<div class="input-append date span6">
										<input type="text" name = 'end_date' class="span8 datepicker"  value="<?=date('d-m-Y')?>" id="dp1" >
										<span class="add-on"><i class="icon-calendar"></i></span>
									</div>	
								</div>
							</div>
						
							<div class="clear"></div>
							<div class="form-actions">
								<div class="span4"></div>
								<div class="span8">
									<button class="btn  btn-info" type="submit" name="by_period" ><i class="icon-search icon-white"></i> <?=Kohana::lang('backend.view');?></button><br/>
								
								</div>
							</div>
							
					</form>	
					
					
					
					<legend>Ranking (all routes, by income & expenses)</legend>						
					<form action="<?=url::site('reports/route/')?>" method="POST" >
						
						<div class="">
							<div class="span4 text-right"><?=Kohana::lang('backend.from')?><span class="red"> *</span>: </div>
							<div class="span8">
								<div class="input-append date span6">
								<input type="text" name = 'start_date' class="span8 datepicker"  value="<?=date('d-m-Y')?>" id="dp1" >
								<span class="add-on"><i class="icon-calendar"></i></span>
								</div>	
							</div>
						</div>
						<div class="">
							<div class="span4 text-right"><?=Kohana::lang('backend.to')?><span class="red"> *</span>: </div>
							<div class="span8">
								<div class="input-append date span6">
								<input type="text" name = 'end_date' class="span8 datepicker"  value="<?=date('d-m-Y')?>" id="dp1" >
								<span class="add-on"><i class="icon-calendar"></i></span>
								</div>	
							</div>
						</div>
						
						<div class="clear"></div>
						<div class="form-actions">
							<div class="span4"></div>
							<div class="span8"><button class="btn  btn-info" type="submit" name="compare_routes" ><i class="icon-search icon-white"></i> <?=Kohana::lang('backend.view');?></button>
							</div>
						</div>
					</form>	
					
					
				</div>	
					
			</div>
			
			
			<div class="tab-pane" id="passenger-reports">
				<div class="span8 offset2">
				
					<legend><?=Kohana::lang('backend.passenger_activity')?></legend>		
						
						<form action="<?=url::site('reports/passenger_search/')?>" method="POST" >
						<div class="">
							<div class="span4 text-right"><?=Kohana::lang('backend.search_name')?><span class="red"> *</span>: </div>
							<div class="span8">
								<input type="text" name="passenger_name" required />
							
							</div>
						</div>
					
						
						<div class="clear"></div>
						<div class="form-actions">
							<div class="span4"></div>
							<div class="span8">
								<button class="btn  btn-info" type="submit" name="by_name" ><i class="icon-search icon-white"></i> <?=Kohana::lang('backend.search');?></button>
								<a href="<?=url::site('reports/download_passenger_list')?>" class="btn "><i class="icon-download-alt"></i> Save full passenger list</a>
							</div>
						</div>
					</form>	
				</div>	
					
			</div>  
			
			
					
		</div>
		
			
		
			  
		</div>
		
	</div>
	<div class="clear"></div>

<script type="text/javascript">
	$(document).ready(function(){
			$('#timepicker').timepicker({
				minuteStep:30,
				//defaultTime: '8:00 PM'
			});
			
			$('.input-append.date').datepicker({
				format: "dd-mm-yyyy",
				todayBtn: "linked",
				autoclose: true,
				todayHighlight: true
			});
			
			
				$(function () {
				$('#myTab a').click(function (e) {
					  e.preventDefault();
					  $(this).tab('show');  
			})
			  })
			
	})
	</script>
