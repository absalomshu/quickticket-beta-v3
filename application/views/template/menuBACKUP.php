<?php defined('SYSPATH') or die('No direct script access'); 

	if ($this->uri->segment(2) != 'login' AND $this->uri->segment(2) != 'add_admin'){
	$admin=Authlite::instance()->get_admin(); 
	$isallowed = Admin_Model::admin_is_allowed_main_module($admin->username,3);
	var_dump($isallowed);exit;
	$agency_id = $admin->agency_id;
	?>
		<div class="row-fluid">
			<div class="navbar">
			  <div class="navbar-inner">
				<ul class="nav bold">
				  <li  <?php if($this->uri->segment(2) == 'main') {?> class="active"<?php }?>><?php if($admin->AllowScheduleManagement){?><a href="<?=url::site('admin')?>"><i class="icon-home"></i> <?=Kohana::lang('backend.home')?> </a> <?php }?></li> 
					<!--If it's an admin allowed to manage expenses, or generally a BM-->
				 <li  <?php if($admin->AllowExpenseManagement == 1 OR $admin->admin_group_id >= 3) { if($this->uri->segment(1) == 'expenses') {?> class="active"<?php }?>><a href="<?=url::site('expenses/all')?>"><i class="icon-wrench"></i> <?=Kohana::lang('backend.expenses');}?></a></li>
		
		<!--Other menu items only available for admin not BM OR GM 
			Now, also check whether that PARTICULAR USER has the right to use that PARTICULAR MODULE
		-->
		
				  <?php if ($admin->admin_group_id == 2) {?>
				  <li  <?php if($admin->AllowScheduleManagement){ if($this->uri->segment(2) == 'all_schedules') {?> class="active"<?php }?>><a href="<?=url::site('admin/all_schedules/current')?>"> <i class="icon-list"></i> <?=Kohana::lang('backend.schedules');}?></a></li>
				  <li  <?php if($admin->AllowParcelManagement)  { if($this->uri->segment(1) == 'parcels') {?> class="active"<?php }?>><a href="<?=url::site('parcels/all/incoming')?>"><i class="icon-gift"></i> <?=Kohana::lang('backend.parcels');}?></a></li>
				  <li  <?php if($admin->AllowBusManagement) { if ($this->uri->segment(1) == 'buses') {?> class="active"<?php }?>><a href="<?=url::site('buses/all')?>"><i class="icon-bus"></i> <?=Kohana::lang('backend.buses');}?></a></li>
				  <?php }
				  //Sho		<!-- SMS management for BM--> 
				  if ($admin->admin_group_id == 3) { ?>
					<li  <?php if ($this->uri->segment(1) == 'sms') {?> class="active"<?php }?>><a href="<?=url::site('sms')?>"><i class="icon-envelope"></i> <?=Kohana::lang('backend.sms');?></a></li>
				  <?php
				  }
				  //if it's a parcel only account, show the parcels
				  if ($admin->admin_group_id == 1) { ?>
				  <li  <?php if($this->uri->segment(1) == 'parcels') {?> class="active"<?php }?>><a href="<?=url::site('parcels/all/incoming')?>"><i class="icon-gift"></i> <?=Kohana::lang('backend.parcels')?></a></li>
				  <?php }?>
				  				  
				<li  <?php if($this->uri->segment(1) == 'reminders') {?> class="active"<?php }?>><a href="<?=url::site('reminders/all')?>"><i class="icon-check"></i> <?=Kohana::lang('backend.reminders')?></a></li>
				<li  <?php if($this->uri->segment(1) == 'reports') {?> class="active"<?php }?>><a href="<?=url::site('reports')?>"><i class="icon-info-sign"></i> <?=Kohana::lang('backend.reports')?></a></li>
				</ul>
				<!-- Only show settings to BM-->
				<?php if ($admin->admin_group_id == 3) {?>
		
				<span style="font-weight:normal;font-size:12px;float:right;margin:10px; "><a  href="<?=url::site('settings')?>"><i class="icon-cog"></i> <?=Kohana::lang('backend.settings')?></a></span>
				<?php } ?>
			  </div>
			</div>
					
		</div>
		<?php }?>