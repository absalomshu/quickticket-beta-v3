<?php defined('SYSPATH') or die('No direct script access');

//set default timezone, otherwise it's UTC which is an hour late
date_default_timezone_set('Africa/Douala');

class Mobile_Controller extends Template_Controller {

	private $user;
	
	public function __construct()
	{	
		parent::__construct();
		//$this->admin = Authlite::instance()->get_admin();
		$this->session = Session::instance();
		//Save the page he's on for each function. That way, he can easily continue where he left off if he's signed out, or clicks a link that 
		//requires sign in
		//$this->session->set('last_visited_uri',$this->uri->string());
		$this->db = new Database();
		$this->user=User_Authlite::instance()->get_user();
	}
	
	public function index() 
	{	
		$this->template->title = 'Home';
		
		$towns = get::all_towns();
		$view=new View ('home');
		$view->agencies = get::all_agencies();
		
		$view->towns = $towns;
		$this->template->content=$view;
	}
	
	/*
	public function login() 
	{	
		
		if (!empty($this->user)) 
		{
				url::redirect('home');
		}
		$notice='';
		$this->template->title="login";
		
		if($_POST) 
		{	
			
			$post=new Validation($_POST);
			$post->add_rules("email", "required",'valid::standard_text');
			$post->add_rules("password", "required");
			if ($post->validate())
			{
				$email = $_POST['email'];
				$password = $_POST['password'];
				
				
				$user = User_Authlite::instance()->login($email, $password);
								
				if(!$user){	
					//$notice="Sorry, incorrect username and password combination!";
					$this->session->set('notice', array('message'=>'Sorry, incorrect email and password combination','type'=>'error'));

				}
				else{
				
					//this is already done in the construct, but for some reason, does not pick the admin for
					//the first time till you refresh. so it is done here again
					
					$this->user = User_Authlite::instance()->get_user();

					//$admin_agency_id = $this->admin->agency_id;
					//url::redirect($this->session->get('last_visited_uri'));
					url::redirect('home');
			}}else 
			{
                $errors=$post->errors('errors');
                $notice="";
                foreach($errors as $error) 
				{
                    $notice .= $error;
                }
				$this->session->set('notice', array('message'=>$notice,'type'=>'error'));
            }
		}
		$view=new View('login');
		$view->notice=$notice;
		$view->agencies = get::all_agencies();
		$view->towns = get::all_towns();
		$this->template->content=$view;
	} */
	
	public function logout() 
	{
		User_Authlite::instance()->logout(true);
		url::redirect('home');
	}
	
	public function confirm_email() 
	{
	
		$email_decrypted=qt_email::decrypt_string($this->uri->segment(3));
		$confirmed = User_Model::confirm_email($email_decrypted);
		
		if($confirmed)
		{
			$this->session->set('notice', array('message'=>'Thanks for confirming your email address. You can now login and continue.','type'=>'success'));
		}else
		{
				$this->session->set('notice', array('message'=>'Sorry, something went wrong. Please try again','type'=>'error'));
		}
		url::redirect('home');
		
	}
	
	public function forgot_password() 
	{	
		$this->template->title = 'Forgot password';
		if($_POST) 
		{	
			$post=new Validation($_POST);
			$post->add_rules("email", "required", "valid::email");
		
			if ($post->validate())
			{
				
				$email = $_POST['email'];
				$email_exists = ORM::FACTORY('user')->where('email',$email)->count_all();
				
					if ($email_exists)
					{
						$encrypted_email=qt_email::encrypt_string($email);
						$from	 ='info@quickticket.co';
						$to      = $email;  
						$subject = 'Reset your password | QuickTicket';
						$message = '
											Hello,<br/><br/>
											A password reset request was made using your email address.<br/><br/>
											If this was you, click <a href="http://dev.quickticket.co/home/reset_password/'.$encrypted_email.'">here to change your password.</a><br/>
											If not, simply ignore this mail.<br/><br/>		
											PS/ This link is valid for one hour.<br/><br/>		
											';
								
						$mail_result = qt_email::send($to, $from, $subject, $message);
						$this->session->set('notice', array('message'=>'Password reset instructions have been sent to your email box.','type'=>'success'));
						url::redirect('home');
					
					}else
					{
						$this->session->set('notice', array('message'=>'Sorry, no such email has been registered.','type'=>'error'));
				
					}

			}	
			else 
			{
                $errors=$post->errors('errors');
                $notice="";
                foreach($errors as $error) 
				{
                    $notice .= $error;
                }
				$this->session->set('notice', array('message'=>$notice,'type'=>'error'));
            }
		}
		
		$view=new View ('forgot_password');
		$this->template->content=$view;
	}
	
	public function reset_password() 
	{	
		$this->template->title = 'Reset password';
			
		
		if($_POST) 
		{	
			$post=new Validation($_POST);
			$post->add_rules("password1", "required");
			$post->add_rules("password2", "required");
			$post->add_rules("email", "required");
		
			if ($post->validate())
			{ 
				
				$email = $_POST['email'];
				$password1 = $_POST['password1'];
				$password2 = $_POST['password2'];
				//If the link expires, the decryption will not evaluate to an email.
				//Inform user that link is wrong
				if (!filter_var($email, FILTER_VALIDATE_EMAIL)) 
				{
					$this->session->set('notice', array('message'=>'Link has expired. Kindly restart.','type'=>'error'));
					url::redirect('home/forgot_password');
				}
				
				
					if($password1 === $password2)
						{
							$notice = User_Model::edit_password($email, $password1);
													
							$this->session->set('notice', array('message'=>'Your password has been successfully reset. Log in with your email and new password.','type'=>'success'));
							url::redirect('home');
						}else
						{
							$this->session->set('notice', array('message'=>'Your password doesn\'t match. Please try again','type'=>'error'));
							url::redirect('home');
						}

			}	
			else 
			{
                $errors=$post->errors('errors');
                $notice="";
                foreach($errors as $error) 
				{
                    $notice .= $error;
                }
				$this->session->set('notice', array('message'=>$notice,'type'=>'error'));
            }
		}
		
		$view=new View ('reset_password');
		$this->template->content=$view;
	}
	
	public function signup()
	{
		
		$this->template->title = "Signup";
		//will use http header to secure this function so only trusted members can add an admin
		if($_POST) 
		{	
			$post=new Validation($_POST);
			$post->add_rules("name", "required");
			$post->add_rules("email", "required", "valid::email");
			$post->add_rules("password1", "required");
			$post->add_rules("password2", "required");
			if ($post->validate())
			{
				
				//need to ensure uniqueness of admin name
				$name = $_POST['name'];
				$email = $_POST['email'];
				$password1 = $_POST['password1'];
				$password2 = $_POST['password2'];
				//$username = $_POST['username'];
				//$admin_group_id = $_POST['admin_group'];
				$email_exists = ORM::FACTORY('user')->where('email',$email)->count_all();
				
					if ($email_exists)
					{
						$this->session->set('notice', array('message'=>'This email is taken. Try another.','type'=>'error'));
					}else
					{
						if($password1 === $password2)
						{
							$notice = User_Model::add_user($name, $email, $password1);
							//encrypt user's email and send in a link to his mail.
							$encrypted_email=qt_email::encrypt_string($email);
							
							//send email
							$from	 ='info@quickticket.co';
							$to      = $email;  // Address can also be array('to@example.com', 'Name')
							$subject = 'Welcome to QuickTicket | Confirm your email address';
							$message = '
											Hello <strong>'.$name.'</strong>,<br/><br/>
											Thank you for signing up for QuickTicket,<br/><br/>
											Kindly click <a href="http://dev.quickticket.co/home/confirm_email/'.$encrypted_email.'">here to confirm your email address.</a><br/><br/>
													
											';
								
								
								$mail_result = qt_email::send($to, $from, $subject, $message);
								//var_dump($mail_result);exit;
							
							$this->session->set('notice', array('message'=>'Welcome '.$name.', please confirm your email address by clicking the link that has been sent to your email box.','type'=>'success'));
						}else
						{
							$this->session->set('notice', array('message'=>'Your password doesn\'t match. Please try again','type'=>'error'));
							url::redirect('home');
						}
						//then log him in at once
						//$user = User_Authlite::instance()->login($email, $password1);
					}
			}	
			else 
			{
                $errors=$post->errors('errors');
                $notice="";
                foreach($errors as $error) 
				{
                    $notice .= $error;
                }
				$this->session->set('notice', array('message'=>$notice,'type'=>'error'));
            }
		
		}
		$view = new View('home');
		//$view->parents = $parents;
		//$view->agencies = $agencies;
		$view->agencies = get::all_agencies();
		$view->towns = get::all_towns();
		//$view->groups = $groups;
		$this->template->content = $view;
	
	}
		
	function search_schedule()
	{
		
		if($_GET)
		{
			$post = new Validation($_GET);

			if ($post->validate())
			{ 	
				$from = $_GET['from'];
				$to = $_GET['to'];
				
				
				$this->template->title=get::town($from)." to ".get::town($to);
				
				$departure_time = $_GET['departure_time'];
				$departure_date = date("Y-m-d",strtotime($_GET['departure_date']));
				
				
				//ensure origin and destination are not the same
				if ($from == $to)
				{
					$this->session->set('notice', array('message'=>'The origin and destination cannot be the same','type'=>'error'));
					echo "origin and destination cannot be same";
				}
				else
				{	
					if ($departure_time=='any'){
						//if user selects any time, search only by origin and destination and ensure departed buses don't come up
						//$results = ORM::factory('schedule')->where('from',$from)->where('to',$to)->where('departure_date',$departure_date)->where('status','current')->find_all();
						$results = mysql_query("
						SELECT `parents`.`name` as 'parent_name', `schedules`.`agency_id`, `schedules`.`id` as 'schedule_id',`schedules`.`bus_seats`, `schedules`.`departure_time`, `schedules`.`ticket_price`, `buses`.`vip` FROM `schedules`
						JOIN `agencies` ON `agencies`.`id` = `schedules`.`agency_id`
						JOIN `parents` ON `parents`.`id` = `agencies`.`parent_id`
						JOIN `buses` ON `buses`.`bus_number` = `schedules`.`bus_number`
						WHERE `from` = $from
						AND `to` = $to
						AND `schedules`.`deleted` = '0'
						AND `departure_date` = '".$departure_date."'
						AND `status` = 'current'
						
						ORDER BY `schedules`.`id` ASC");
						
					}else{
						$results = ORM::factory('schedule')->where('from',$from)->where('to',$to)->where('departure_time',$departure_time)->where('departure_date',$departure_date)->where('status','current')->find_all();
					}
					
				}			
			}
			else
			{
				$errors=$post->errors('errors');
				$notice="";
					foreach($errors as $error) 
					{
						$notice.=$error."<br />";
					}
					$this->session->set('notice', array('message'=>$notice,'type'=>'error'));
					//url::redirect('admin/main/'.$this->admin->agency_id);
			}
		
			
		$rows = array();
		while($row = mysql_fetch_assoc($results)){
				
			//format the price and time before returning
			$row['ticket_price']=number_format($row['ticket_price']);
			$row['departure_time']=date("g:iA", strtotime($row['departure_time']));
			$rows[] = $row;
		}
		
		
		print json_encode($rows);exit;
		
		$open_tickets = Open_Ticket_Model::get_all_for_route($from,$to);
		var_dump($open_tickets);
		
		}
		
		//$open_ticket_agencies = Agency_Model::get_open_ticket_agencies_from_and_to_town($from,$to);
		
		
		
		
		
		//var_dump($open_tickets);exit;
		$towns = get::all_towns();
		$view->agencies = get::all_agencies();
		
		$view->towns = $towns;
		$view->results = $results;
		$this->template->content = $view;
		
	}
	
	/*
	function add_purchase()
	{	
		//For a start, even un-logged in users should be able to purchase
		//User_Authlite::check_login();
		var_dump('in the purchase');exit;
		if($_POST)
		{			
					$post = new Validation($_POST);
					$post->add_rules('name','required');
					$post->add_rules('phone','required');
					$post->add_rules('idc','required');
					
					if ($post->validate())
					{	
						
						//print_r($reserve->seat_occupants);exit;
						$client_name = $_POST['name'];
						$client_phone = $_POST['phone'];
						$client_email = $_POST['email'];
						$client_idc = $_POST['idc'];
						$agency_id = $_POST['agency_id'];
						$schedule_id = $_POST['schedule_id'];
						$town_from = $_POST['town_from'];
						$town_to = $_POST['town_to'];
						$ticket_name = '';
						$created_on = date('Y-m-d');
						$amount_paid=0;

						Purchase_Model::add_purchase($agency_id, $schedule_id, $client_name, $client_phone, $amount_paid, $res_email, $created_on);
						Purchase_Model::add_purchase($agency_id, $schedule_id, $client_name, $client_phone, $client_idc, $client_email, $amount_paid, $town_from, $town_to, $ticket_name, $ticket_price, $is_open_ticket, $created_by, $created_on);
						
						print_r($_POST);exit;	
									
					}
					else
					{
						echo "Data error. Try again";						
					}
		}
		
	}
	
	
	function schedule_details($schedule_id)
	{	
		$this->session->set('last_visited_uri',$this->uri->string());
		$this->template->title = "Particular schedule";
		$schedule = ORM::factory('schedule')->where('id',$schedule_id)->find();
		
		$seats_and_occupants = json_decode($schedule->seat_occupants,true);
		$seats_and_reservers = json_decode($schedule->reserved_seats,true); //true converts the object to an array

		if($_POST)
		{	
			$post = new Validation($_POST);
			$post->add_rules('res_name','required');
			$post->add_rules('res_phone','required');
			$post->add_rules('res_seat','required');
			
			if ($post->validate())
			{	
				$client_name = $_POST['res_name'];
				$client_phone = $_POST['res_phone'];
				$client_email = $_POST['res_email'];
				$client_seat = $_POST['res_seat'];
				$client_idc = $_POST['res_idc'];
				
				
				if(isset($_POST['reserve_seat']))
				{	
					$client_reservations = get::client_reservations($schedule_id);
					//first check if the reservation_request in the db is not empty. If it is, create the array. If not, decode as array and populate			
					if (empty ($client_reservations->reserved_seats))
					{
						$reservation_request = array();			
					}
					else
					{
						$reservation_request = json_decode($client_reservations->reserved_seats,true); //true converts the object to an array
					}
					//on the reservation_request table, we enter the reserver's details in a multidimensional array
					//in the second level of the array, 1->reserver's name, 2->reserver's phone, 3->reserver's email
					
					//we need to get information about this schedule to know if the seat is occupied or not.
					//This is to prevent the error when a client reserves a seat, and hence it's not yet allocated to him, so the seat shows at
					//the frontend. then it's confirmed at the backend. 
					//they should not be able to reserve if the seat was given out, but they didn't refresh hence still see it as available
					$schedule=get::schedule($schedule_id);
					if (empty ($schedule->seat_occupants))
						{
							$seats_and_occupants = array();			
						}
						else
						{
							$seats_and_occupants = json_decode($schedule->seat_occupants,true);
						}
					if(isset($seats_and_occupants[$client_seat])){
						//echo"<script> alert('Sorry, seat $client_seat has been taken. Please select another');</script>";
						$this->session->set('notice', array('message'=>"Sorry, seat $client_seat has been taken. Please select another",'type'=>'error'));
						url::redirect('home/schedule_details/'.$schedule_id);
					
					}
					
					$reservation_request[$client_seat] = array('name'=>$client_name,'phone'=>$client_phone,'email'=>$client_email,'idc'=>$client_idc);
					//print_R($reservation_request);exit;
					$client_reservations->reserved_seats = json_encode($reservation_request);
					$client_reservations->schedule_id = $schedule_id;
					$client_reservations->save();
					
					$this->session->set('notice', array('message'=>"Your request has been made. An admin will verify and you will be notified",'type'=>'success'));
					url::redirect('home/schedule_details/'.$schedule_id);	
				}
				if(isset($_POST['purchase_ticket']))
				{	
					$created_on = date('Y-m-d');
					$purchase_id=Purchase_Model::add_purchase($schedule->agency_id, $schedule_id, $client_name, $client_phone, $client_idc, $client_email, $amount_paid=$schedule->ticket_price,$schedule->from, $schedule->to, $ticket_name='', $ticket_price='', $is_open_ticket='0', STRTOUPPER(@$this->user->username), $created_on);
					$this->session->set('notice', array('message'=>"Purchase confirmed",'type'=>'success'));
					
					url::redirect('payment/details/'.$purchase_id);
				}
			
			}else
			{
				$errors=$post->errors('errors');
				$notice="";
					foreach($errors as $error) 
					{
						$notice.=$error."<br />";
					}
					$this->session->set('notice', array('message'=>$notice,'type'=>'error'));
					url::redirect('home/schedule_details/'.$schedule_id);	
			}
		}
		$view = new View('schedule_details');
		$view->schedule = $schedule;
		$view->seats_and_occupants = $seats_and_occupants;
		$view->seats_and_reservers = $seats_and_reservers;
		$this->template->content = $view;
	} */
	


	
	function reserve_seat()
	{
		if($_POST)
		{	
			$schedule_id=$_POST['schedule_id'];
			$client_reservations = get::client_reservations($schedule_id);
			$client_reservation_seat_infos=json_decode($client_reservations->reserved_seats_permanent,true);
			$schedule=get::schedule($schedule_id);
							
			$client_name = $_POST['name'];
			$client_phone = $_POST['phone'];
			$client_email = $_POST['email'];
			$client_idc = $_POST['idc'];
			$agency_id = $_POST['agency_id'];
			
			//For now, from mobile, give a random seat number between 1 and 20
			$client_seat = rand(1,20);
				
				
			if (empty ($schedule->seat_occupants)){
				$seats_and_occupants = array();			
			}
			else{
				$seats_and_occupants = json_decode($schedule->seat_occupants,true);
			}
			//If seat is taken search another
			if(isset($seats_and_occupants[$client_seat])){
				$client_seat = rand(1,20); //randomly select another seat
			}
			
					
			//If the seat has been previously reserved, don't overwrite. Seat 2 is reserved, save as 2.1, 2.2 and so on
			if(isset($client_reservation_seat_infos[$client_seat])){
				for($i=1;$i<100;$i++){
					if(!isset($client_reservation_seat_infos[$client_seat.".$i"])){ //e.g. if 2.1 is not taken
						$client_seat = $client_seat.".$i";
						break;
					}
				}
			}
					
				//first check if the reservation_request in the db is not empty. If it is, create the array. If not, decode as array and populate	
				//we rather check reserved seats permanent, as it's not changed when seats are downloaded from reserved_seats, hence deleted
				//if not, seat 2 is reserved, then downloaded and deleted, then seat 2 will be reserved again (instead of 2.1) and the new download
				//will overwrite the previous reserver of seat 2 in the offline db
				
			if (empty ($client_reservations->reserved_seats_permanent))//no reservation has been made for this schedule. create new array
			{ 	//Create new seat in reserved_seats
				$reservation_request = array();	
				$reservation_request[$client_seat] = array('name'=>$client_name,'phone'=>$client_phone,'email'=>$client_email,'idc'=>$client_idc);
				$client_reservations->reserved_seats = json_encode($reservation_request);
				//Create same seat to reserved_seats_permanent
				$client_reservations->reserved_seats_permanent = json_encode($reservation_request);
			}
			elseif (empty ($client_reservations->reserved_seats)) //reservations were made but have been downloaded. create new array
			{	//Create new seat in reserved_seats
				$reservation_request = array();	
				$reservation_request[$client_seat] = array('name'=>$client_name,'phone'=>$client_phone,'email'=>$client_email,'idc'=>$client_idc);
				$client_reservations->reserved_seats = json_encode($reservation_request);	
				//Add new seat to the permanent reservations and save
				$client_reservation_seat_infos[$client_seat] = array('name'=>$client_name,'phone'=>$client_phone,'email'=>$client_email,'idc'=>$client_idc);
				$client_reservations->reserved_seats_permanent = json_encode($client_reservation_seat_infos);
			}
			else //reservations made but not downloaded. get the array
			{	
				$reservation_request = json_decode($client_reservations->reserved_seats,true); //true converts the object to an array
				//Add new seat to the reserved_seats
				$reservation_request[$client_seat] = array('name'=>$client_name,'phone'=>$client_phone,'email'=>$client_email,'idc'=>$client_idc);
				$client_reservations->reserved_seats = json_encode($reservation_request);
				//Add new seat to the permanent reservations and save
				$client_reservation_seat_infos[$client_seat] = array('name'=>$client_name,'phone'=>$client_phone,'email'=>$client_email,'idc'=>$client_idc);
				$client_reservations->reserved_seats_permanent = json_encode($client_reservation_seat_infos);
			}
				
				//the permanent column remains unchanged online while seats from the normal column and downloaded (and deleted so they are not downloaded twice)
				//then also deleted once they are 'used' offline.
				
				//Specify those from mobile app for future use.
				if (empty ($client_reservations->from_mobile_app))//no mobile reservation has been made for this schedule. create new array
				{ 	//Create new seat in reserved_seats
					$mobile_reservations = array();
					$mobile_reservations[$client_seat] = array('name'=>$client_name,'phone'=>$client_phone,'email'=>$client_email,'idc'=>$client_idc);
				}else{
					$mobile_reservations = json_decode($client_reservations->from_mobile_app,true); //true converts the object to an array
					//Add new seat to the reserved_seats
					$mobile_reservations[$client_seat] = array('name'=>$client_name,'phone'=>$client_phone,'email'=>$client_email,'idc'=>$client_idc);
				}
				
				$client_reservations->from_mobile_app = json_encode($mobile_reservations);
				$client_reservations->agency_id = $agency_id;
				$client_reservations->schedule_id = $schedule_id;
				$client_reservations->save();
				
				print_r("Your request has been made. An admin will verify and you will be notified");exit;
				
		
		
		
		}
	$this->template->title="";
	
	}
	
}
	
