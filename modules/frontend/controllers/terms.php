<?php defined('SYSPATH') or die('No direct script access');

class Terms_Controller extends Admin_NotLogged_Template_Controller {

		public function __construct()
	{	
		parent::__construct();
		//$this->admin = Authlite::instance()->get_admin();
		$this->session = Session::instance();
		
		
	}
	
	public function privacy() {
		
		$this->template->title = "Privacy";
		
		$view=new View ('terms/privacy');
		
		$this->template->content=$view;
	}	
	public function conditions() {
		
		$this->template->title = "Terms and conditions";
		
		$view=new View ('terms/terms');
		
		$this->template->content=$view;
	}
	public function about() {
		
		$this->template->title = "About";
		
		$view=new View ('terms/about');
		
		$this->template->content=$view;
	}
	public function contact() {
		
		$this->template->title = "About";
		
		$view=new View ('terms/contact');
		
		$this->template->content=$view;
	}

		

		

	}

	
