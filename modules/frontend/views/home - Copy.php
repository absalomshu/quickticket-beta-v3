<?php defined('SYSPATH') or die('No direct script access'); ?> 
	
	<div class="banner no-margin">
		<div class="container">
					
				<div class="banner ">
					<div  style="color:white;">
						<center>
						<h1>Bus Ticket Booking </h1>
						<span class="banner-subheading">Travel schedules from your favourite travel agencies in one place.
						</center>
					</div> 	
				</div>
				
				<div class="banner-form">
					<form action="<?=url::site('home/search_schedule')?>" method="GET" id="search-schedule" >
					<div class="span3 " >
						<div class="control-group">
						<label class="control-label" for="inputType" style="color:white;">From:</label>
							<div class="controls ">
								<select name="from" id="town_from" class="input-large">
									<?php foreach ($towns as $town):
										//for now, only Yaounde and buea
										if($town->id==3 OR $town->id==1){
											
									?>
									<option value="<?=$town->id?>" <?php if($town->id==3){echo "selected='selected'";}?>>
										<?=$town->name?>
									</option>
										<?=$town_from = $town->name?>
									<?php 
										}
									endforeach;?>
								</select>
							</div>
						</div>
					</div>
					<div class="span3 ">
						<div class="control-group">
						<label class="control-label lab" for="inputType" style="color:white;">To:</label>
							<div class="controls ">
								<select name="to" id="town_to" class="input-large">
									<?php foreach ($towns as $town):
										//for now, only Yaounde and buea
										if($town->id==3 OR $town->id==1){
									?>
									<option value="<?=$town->id?>" <?php if($town->id==4){echo "selected='selected'";}?> ><?=$town->name?></option>
									<?=$town_to = $town->name?>
										<?php } endforeach;?>
								</select>
							</div>
						</div>
					</div>
					<input type="hidden" name="departure_time" value="any">
					<!--<div class="span2 home">
						<div class="control-group">
						<label class="control-label lab" for="inputType">Time:</label>
							<div class="controls ">
								<select name="departure_time" id="inputType" readonly>
									<option value="any">Anytime</option>
									<!--<option value="Day(8:00 AM)">Day (8:00 AM)</option>
									<option value="Afternoon(1:00PM)">Afternoon(1:00PM)</option>
									<option value="Night(8:00 PM)">Night (8:00 PM)</option>
									

								</select>
							</div>
						</div>
					</div> -->
					<div class="span3 date" >
						<div class="control-group">
						<label class="control-label lab" for="inputType" style="color:white;">Date:</label>
							<div class="input-append date left">
								<input type="text" name = 'departure_date' class=" datepicker span3"  value="<?=date('d-m-Y')?>" id="dp1" >
								<span class="add-on" style='padding: 14px 5px;'><i class="icon-th"></i></span>
							</div>
						</div>
					</div>
					<input type="hidden" name="route" id="route" value="">
			
					<div class=" span3 ">	
						<label class="" >&nbsp;</label>
						<button class="btn btn-warning btn-large banner-button" type="submit"   id="submit-search" > Find buses</button><br/>
					</div>
				</form>
				</div>
				
					
				
				
		</div>	
		
		
		<br/><br/><br/><br/><br/><br/><br/><br/><br/>
		
		<!--
		<style>
		/* Basic jQuery Slider essential styles */
			/* Put here for the sake of faster page load */
			

			ul.bjqs{position:relative; list-style:none;padding:0;margin:0;overflow:hidden; display:none;}
			li.bjqs-slide{position:absolute; display:none;}
			ul.bjqs-controls{list-style:none;margin:0;padding:0;z-index:9999;}
			ul.bjqs-controls.v-centered li a{position:absolute;}
			ul.bjqs-controls.v-centered li.bjqs-next a{right:0;}
			ul.bjqs-controls.v-centered li.bjqs-prev a{left:0;}
			ol.bjqs-markers{list-style: none; padding: 0; margin: 0; width:100%;}
			ol.bjqs-markers.h-centered{text-align: center;}
			ol.bjqs-markers li{display:inline;}
			ol.bjqs-markers li a{display:inline-block;}
			p.bjqs-caption{display:block;width:96%;margin:0;padding:2%;position:absolute;bottom:0;}
	
		</style>
		
		<!--<div id="banner-fades">
		<ul class="bjqs">
		<li><img src="<?=url::base()?>images/5.png" ></li>
		</ul>
		</div>-->
				  

	</div>




<div class="container">
		<?php
		//determine what type of notice to display if at all
		$notice = $this->session->get_once('notice');
			if(!empty($notice)){ 
				if($notice['type'] == 'success'){?><div class="alert general success"><a class="close" data-dismiss="alert" href="#">&times;</a><?php echo $notice['message']; }?></div>
			<?	if($notice['type'] == 'error'){?><div class="alert general error"><a class="close" data-dismiss="alert" href="#">&times;</a><?php echo $notice['message']; }?></div>
		<?}?>	</div>
		<div class="strip" >
		
	  
		
	 
		
			
			<script type="text/javascript">
			//Get the name of the towns to and fro via JS and send back to PHP so it's passed in the URL, for SEO
			
			$('#submit-search').click(function(){ 
				var route = $( "#town_from option:selected" ).text().toLowerCase()+"-"+$( "#town_to option:selected" ).text().toLowerCase();
				$('#route').val(route);				
			  });
			  
			</script>
			
			
		</div>
	
	
	
	
	
	
  
	<div class="container" style="background-color:#fff;">
	  
	 <br/><br/> 
	<div class="row-fluid marketing" style="margin:5px 0 0px 0;">	
	<div class="home-tab span4" >
				<img src="<?=url::base()?>images/benefits.png" width="126" height="85" />
				<div class="heading">Travellers</div>
				<div class="rule-dotted"><hr/></div>
				
					Secure online bus ticket reservations and purchase
					;
					all relevant information about travel schedules, buses and ticket booking real-time. 
					Compare, reserve and buy bus tickets online 
				
		</div>
		<div class="span4 home-tab" >
			<img src="<?=url::base()?>images/bus_icon.png"  width="85" height="85" />
				<div class="heading">Travel agencies </div>
				<div class="rule-dotted"><hr/></div>
					A complete travel management solution: For
					
					
					 managing your schedules, printing tickets 
					managing parcels and expenses,
					daily reporting and analysis.
				
									 <br/>It's all here.

		</div>
		<div class="home-tab span4" >
				<img src="<?=url::base()?>images/secure_img.png" width="82" height="85" />
				<div class="heading">Flexible payment</div>
				<div class="rule-dotted"><hr/></div>
				
					We accept the majority of the major forms of electronic 
					payment in the country, among which are MTN Money, 
					Orange Money, AfrikPay, Pursar and more
				
		</div>
		
		
		
		
		<!--<div class="span4 hero-unit prof-unit" >
			<div class="heading">They trust us!<span style="float:right;font-size:12px;font-weight:normal;"><a href="#">See all</a></span></div>
			<div class="rule"><hr/></div>
				<div >
					<table>
						<tr>
						<ul>
							<?php 
								$i=0; 
								foreach ($agencies as $agency){
									if ($i%3==0){ ?>
									</tr><tr>
									<?php }	?>
								
										<td >		
													<div class="dropdown" style="">
													  <a class="dropdown-toggle" id="dLabel" role="button" data-toggle="dropdown" data-target="#" href="/page.html">
													<span style="font-size:20px;"><?=get::_parent($agency->id)->name?> </span>
														<b class="caret" style="margin-top:15px;"></b>
													  </a>
													  <ul class="dropdown-menu" role="menu" aria-labelledby="dLabel">
														<li><a href="www.hm.com">Buea</a></li>
														<li><a href="www.hm.com">Douala</a></li>
														<li><a href="www.hm.com">Yaounde</a></li>
														<li><a href="www.hm.com">Bafoussam</a></li>
													  </ul>
													</div>
													
													
										</td>
								<?php $i++;} ?>
						</ul>
						</tr>
						
					</table>
				</div>
		
		</div>-->
		

		</div>	
		
		 <div class="clear"></div>
		 <br/>
		 <br/>
		<div class="rule-dotted"><hr/></div>
		<div class="clear" style="height:15px;"></div>
	<div class="left heading">Featured on:</div>
		<div class="span12 featured" >
				
			<div class="featured-tab">
				<a href="http://techloy.com/2013/09/18/quickticket-cameroon/?utm_source=rss&utm_medium=rss&utm_campaign=quickticket-cameroon" target="_blank"><img src="<?=url::base()?>images/featured/techloy.jpg"  alt="Techloy" title="QuickTicket on Techloy" width="140" height="57"/></a>
			</div>
			<div class="featured-tab">
				<a href="http://ventureburn.com/2013/09/quickticket-launches-crowdfunding-campaign-aims-to-solve-transport-issues/" target="_blank"><img src="<?=url::base()?>images/featured/ventureburn.jpg"  alt="Ventureburn" title="QuickTicket on Ventureburn"  width="140" height="79"/></a>
			</div>
			<div class="featured-tab" style="margin-top:-40px;">
				<a href="<?=url::site('home')?>" target="_blank"><img src="<?=url::base()?>images/featured/VC4Africa_Logo.jpg"  alt="VC4Africa" title="QuickTicket on VC4Africa" width="140" height="118"/></a>
			</div>
			<div class="featured-tab ">
				<a href="http://www.thetimescameroon.com/2013/09/new-online-bus-ticket-booking-services.html" target="_blank"><img src="<?=url::base()?>images/featured/tcp.png"  alt="The Cameroon Post" title="QuickTicket on The Cameroon Post" width="140" height="54"/></a>
			</div>
			<div class="featured-tab last">
				<a href="http://www.techmoran.com/cameroons-quickticket-co-to-launch-across-africa/" target="_blank"><img src="<?=url::base()?>images/featured/tech-moran.png"  alt="TechMoran" title="QuickTicket on TechMoran" width="140" height="61"/></a>
			</div>

		</div>		 <div class="clear"></div>
		 <br/>
	<!--	<div class="rule-dotted"><hr/></div>
		<div class="clear" style="height:10px;"></div>
	<div class="left heading">Popular routes:</div>
		<div class="span12 featured" >
				
			<div class="featured-tab">
				<a href="<?=url::site("home/search_schedule?from=3&to=4&departure_time=any&departure_date=".date('d-m-Y'))?>" target="_blank">Yaounde - Douala</a>
			</div>
			<div class="featured-tab">
				<a href="<?=url::site("home/search_schedule?from=1&to=2&departure_time=any&departure_date=".date('d-m-Y'))?>" target="_blank">Buea - Bamenda</a>
			</div>
			<div class="featured-tab">
				<a href="<?=url::site("home/search_schedule?from=4&to=3&departure_time=any&departure_date=".date('d-m-Y'))?>" target="_blank">Douala - Yaounde</a>
			</div>
			<div class="featured-tab">
				<a href="<?=url::site("home/search_schedule?from=3&to=1&departure_time=any&departure_date=".date('d-m-Y'))?>" target="_blank">Yaounde - Buea</a>
			</div>
			
			<div class="featured-tab last">
				<a href="<?=url::site("home/search_schedule?from=3&to=2&departure_time=any&departure_date=".date('d-m-Y'))?>" target="_blank">Yaounde - Bamenda</a>
			</div>

		</div> -->

			