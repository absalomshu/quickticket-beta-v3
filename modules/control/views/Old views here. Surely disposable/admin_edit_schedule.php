<?php defined('SYSPATH') or die('No direct script access'); ?> 





	  
	  <div class="row-fluid " style="margin:5px 0 0px 0px;">
	  
		<form action="<?=url::site('admin/edit_schedule/'.$schedule->id)?>" method="POST" name="theform">
		
	  	<div class="span12 hero-unit prof-unit" >
		<?php no_csrf::set(); ?>
		<div class="heading"><?=Kohana::lang('backend.edit_sched_for')?> <?=$schedule->bus_number?><span style="float:right;font-size:12px;font-weight:normal;"><!--<a href="#"><?=Kohana::lang('backend.see_all')?></a>--></span></div>
			<div class="rule"><hr/></div>
			<div class="span2 spacious">
				<div class="control-group">
				<label class="control-label lab" for="inputType"><?=Kohana::lang('backend.from')?>:</label>
					
					<!--<div class="controls ">
						<select name="from" id="inputType">
							<option value="<?php //$schedule->from;?>"><h2><?php //get::town($schedule->from);?></h2></option>
							<?php //foreach ($towns as $town):?>
							<option value="<?php //$town->id?>"><?php //$town->name?></option>
							<?php //endforeach;?>
						</select>
					</div>-->
					<div class="controls ">
						<select name="from" id="inputType" disabled>
						 <?php $admins_town = get::admins_town($this->admin->agency_id);?>
							<option value="<?=$admins_town->id?>" disabled><?=$admins_town->name?></option>
							
						</select>
					</div>
				</div>
			</div>
			<div class="span2 spacious">
				<div class="control-group">
				<label class="control-label lab" for="inputType"><?=Kohana::lang('backend.to')?>:</label>
					
					<div class="controls ">
						<select name="to" id="inputType">
							<option value="<?=$schedule->to;?>"><h2><?=get::town($schedule->to);?></h2></option>
							<?php foreach ($towns as $town):?>
							<option value="<?=$town->id?>"><?=$town->name?></option>
							<?php endforeach;?>
						</select>
					</div>
				</div>
			</div>
			<div class="span2 spacious">
				<div class="control-group">
				<label class="control-label lab" for="inputType"><?=Kohana::lang('backend.time')?>:</label>
					<div class="controls ">
					
					<div class="input-append bootstrap-timepicker">
						<input name="departure_time" id="timepicker" type="text" class="input-small" value="<?=date("g:i A", strtotime($schedule->departure_time));?>">
						<span class="add-on"><i class="icon-time"></i></span>
					</div>
					</div>
				</div>
			</div>
			<div class="span2 spacious">
				<div class="control-group">
				<label class="control-label lab" for="inputType">Date:</label>
					<div class="controls ">
						<!--Convert date from Y-M-D in db -->
						<div class="input-append date">
							<input type="text" name = 'departure_date' class="span2 datepicker"  value="<?=date("d-m-Y",strtotime($schedule->departure_date))?>" id="dp1" />
							<span class="add-on"><i class="icon-th"></i></span>
						</div>
					</div>
				</div>
			</div>
			
			<div class="clear"></div>
			
			<div class="span2 spacious" >
				<div class="control-group">
				<label class="control-label lab" for="inputType"><?=Kohana::lang('backend.bus_type')?>:</label>
					<div class="controls ">
						<select name="bus_seats" id="inputType">
							
							<option value="<?=$schedule->bus_seats?>"><b><?=$schedule->bus_seats?> <?=Kohana::lang('backend.seater')?></b></option><hr/>
							
							<option value="30">30 <?=Kohana::lang('backend.seater')?></option>
							<option value="35">35 <?=Kohana::lang('backend.seater')?></option>
							<option value="39">39 <?=Kohana::lang('backend.seater')?></option>
							<option value="55">55 <?=Kohana::lang('backend.seater')?></option>
							<option value="70">70 <?=Kohana::lang('backend.seater')?></option>

						</select>
					</div>
				</div>
			</div>
			
			<div class="span2 spacious">
				<div class="control-group">
				<label class="control-label lab" for="inputType"><?=Kohana::lang('backend.bus_no')?>:</label>
					<div class="controls ">
						<input type="text" value="<?=$schedule->bus_number?>"  name="bus_number">
					</div>
				</div>
			</div>
			<div class="span2 spacious">
				<div class="control-group">
				<label class="control-label lab" for="inputType"><?=Kohana::lang('backend.ticket_price')?>:</label>
					<div class="controls input-append">
						<input type="text" value="<?=$schedule->ticket_price?>"  name="ticket_price" class="span8">
						<span class="add-on"> FCFA</span>
					</div>
				</div>
			</div>
			<div class="span2 ">	
			<div class="control-group">
				<label class="control-label lab" for="inputType"></label>
					<div class="controls large">
									<button class="btn btn-info" type="submit"  style="height:45px; width:140px;font-size:20px;" ><?=Kohana::lang('backend.next')?></button><br/>

					</div>
				</div>
				
			</div>
			
			  
		</div>
		</form>
		
	  </div>
	  <div style="height:19px;"></div>

<script type="text/javascript">
	$(document).ready(function(){
			$('#timepicker').timepicker({
				minuteStep:30,
				//defaultTime: '8:00 PM'
			});
			
			
			$('.input-append.date').datepicker({
				format: "dd-mm-yyyy",
				todayBtn: "linked",
				autoclose: true,
				todayHighlight: true
			});
	})
	</script>
