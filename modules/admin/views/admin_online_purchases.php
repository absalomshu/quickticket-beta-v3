<?php defined('SYSPATH') or die('No direct script access'); ?> 


	  
	<div class="row-fluid marketing" style="margin:5px 0 0px 0;">	
		<div class="span12 hero-unit prof-unit reload" style="margin-left:0px;">
	

				<ul class="nav nav-tabs">
					<li><a href="<?=url::site('admin/all_schedules/current')?>"> <i class="icon-time "></i> <?=Kohana::lang('backend.current')?> </a></li>
					<li><a href="<?=url::site('admin/all_schedules/departed/sort_by_date')?>"> <i class="icon-ok-sign "></i> <?=Kohana::lang('backend.departed')?> </a></li>
					<li class=""><a href="<?=url::site('admin/tickets_pending')?>"> <i class="icon-tags "></i> <?=Kohana::lang('backend.tickets_pending')?> </a></li>
					<li class=""><a href="<?=url::site('admin/remote_tickets')?>"> <i class="icon-random "></i> <?=Kohana::lang('backend.remote_tickets')?><?php if($this->total_pending_remote_tickets) {?> <span class="badge badge-important"><?=$this->total_pending_remote_tickets;?><?php }?> </a></li>
					<li class="active"><a href="#"> <i class="icon-globe "></i> <?=Kohana::lang('backend.online_purchases')?><?php if($this->total_pending_online_purchases) {?> <span class="badge badge-important"><?=$this->total_pending_online_purchases;?><?php }?> </a></li>
					
					<li ><a href="<?=url::site('admin/check_schedules')?>"><i class="icon-search"></i> <?=Kohana::lang('backend.check_schedules')?></a></li>

				</ul>
				<legend><?=Kohana::lang('backend.purchases_online')?><div class='view'><a href="<?=url::site('admin/online_purchase_history')?>"><?=Kohana::lang('backend.online_purchase_history')?></a></div></legend>
				
				<table class="table table-striped table-hover table-condensed">
					<tr>
						<th><?=Kohana::lang('backend.purchase_date')?> </th>
						<th><?=Kohana::lang('backend.name')?> </th>
						<th><?=Kohana::lang('backend.phone')?> </th>
						<th><?=Kohana::lang('backend.idc')?> </th>
						<th><?=Kohana::lang('backend.schedule_requested')?> </th>
						<th><?=Kohana::lang('backend.schedule_id')?> </th>
						<th><?=Kohana::lang('backend.amount_paid')?> </th>
						<th><?php ?> </th>
						<th>Actions </th>
					</tr>
					<?php foreach ($online_purchases as $purchase){ 
					?>
					<tr>
						<td><?=date("d-m-Y",strtotime($purchase->CreatedOn))?></td>						
						<td><?=$purchase->client_name?></td>
						<td><?=$purchase->client_phone?></td>
						<td><b><?=$purchase->client_idc?></b></td>
						<?php 
						//Only if a particular schedule was requested i.e. NOT open ticket
						if($purchase->schedule_id){
						//Since it's the global schedule id, the local id excludes the 1st 3 chars which are the agency id 
						
							$schedule=Schedule_Model::get(substr($purchase->schedule_id,3)); ?>
						
						<td><?=$schedule->bus_number?> : <?=get::town($schedule->from)?> --> <b><?=get::town($schedule->to)?></b> : <b><?=date("g:i A", strtotime($schedule->departure_time))?></td>
						<td><?="SC".$purchase->schedule_id?></td>
						<?php }else{ ?>
						
						<td><?=get::town($purchase->town_from)?> --> <?=get::town($purchase->town_to)?> : <b><?=Kohana::lang('backend.anytime')?></b></td>
						<td></td>
						<?php }?>
						<td><?=number_format($purchase->amount_paid)?></td>
						<!--If amount paid is zero, it's an open ticket reservation-->
						<td><?php if($purchase->amount_paid == 0){?> <span class="label label-warning">Reservation</span><?php }?></td>
						<td>
							<div class="btn-group ">
									  <a class="btn dropdown-toggle btn-mini" data-toggle="dropdown" href="#" >
												-- <?=Kohana::lang('backend.select')?> --
									  <span class="caret"></span>
									  </a>
									  <ul class="dropdown-menu">
										<!--Note that the ticket id is inserted as the id of the modal, and trigger/link to the modal. If not, each seat will be
										bringing up the same modal, hence only the first free ticket will be picked each time. VERY IMPORTANT-->
										<li><a data-toggle="modal" href="#confirm-free<?=$purchase->id?>" ><i class="icon-plus"></i> <?=Kohana::lang('backend.add_to_schedule')?></a></li>
										<li><a href="<?=url::site('admin/delete_online_purchase/'.$purchase->id)?>" onClick="return confirm('<?=Kohana::lang('backend.confirm_delete')?>?');"><i class="icon-trash"></i> <?=Kohana::lang('backend.delete')?></a></li>
									 </ul>
							</div>
									
									  <!-- Modal for confirm missed to free ticket-->
										<div id="confirm-free<?=$purchase->id?>" class="modal hide fade" tabindex="-1" role="dialog" aria-labelledby="myModalLabel" aria-hidden="true">
										  <div class="modal-header">
											<button type="button" class="close" data-dismiss="modal" aria-hidden="true">×</button>
											<h3 id="myModalLabel"><?=Kohana::lang('backend.confirm_purchase')?></h3>
										  </div>
										  <div class="modal-body">
											<legend><?=Kohana::lang('backend.ticket_will_be')?></legend>
										
											
												 
												  <?php 
												  foreach($current_schedules as $schedule){ 
												  
												  ?>
												  <form action="<?=url::site('admin/complete_schedule/'.$schedule->id)?>" method="POST" >
												  <!--Build a form identical to the complete_schedule form, as we'll post to the same function-->
													<input type="text" name="schedule_id" value="<?=$schedule->id?>" style="display:none" />
													<input type="text" name="ticket_id" value="<?php //$ticket->id?>" style="display:none" />
													
													<!--<input type="checkbox" name="free_ticket" id="ft" checked="checked" style="display:none"/>-->
													<!--<input type="text"  name="free_ticket_reason" value="Missed previous schedule" style="display:none" />-->
													<input type="text"  name="client_name" value="<?=$purchase->client_name?>" style="display:none" />
													<input type="text"  name="client_phone" value="<?=$purchase->client_phone?>" style="display:none" />
													<input type="text"  name="client_email" value="<?php //$purchase->client_email?>" style="display:none" />
													<input type="text"  name="client_idc" value="" style="display:none" />
													
													<!--Use this input to tell controller that if it's coming from this field, then delete the missed ticket afterwards-->
													<input type="text"  name="from_online_purchase" value="<?=$purchase->id?>" style="display:none" />
													
													<div class="span12">
													<span   class="span7"><b><?=$schedule->bus_number?></b> : <?=$this->agency_town?> -> <b><?=$schedule->town_name_to?></b> - <b><?=date("g:i A", strtotime($schedule->departure_time))?></b></span>
 
													<!--Now get an adequate non-filled, non-reserved seat for him.-->
													<div class="short-dropdown span2">
															<select name="seat_number" id="inputType" class="span10">
															<?php
															
															$seats_and_reservers = json_decode($schedule->reserved_seats,true); 
															$seats_and_occupants = json_decode($schedule->seat_occupants,true);
															$number_of_seats=$schedule->bus_seats;
															//start from 2 since seat 1 is the driver
															//$i=2;
															for($i=2; $i<= $number_of_seats; $i++)
															{
																//make sure the seat is not already occupied
																if(!isset($seats_and_occupants[$i])  AND !isset($seats_and_reservers[$i]) )
																{
																?>
																	<!--<input type="text"  name="seat_number" value="<?=$i?>" style="display:none" />-->
																	<option value="<?=$i?>"><?=$i?></option>
																<?php
																}
															} ?>
															</select>
													</div>
													
													<button  type="submit" name="check_out_form" class="btn btn-info"><?=Kohana::lang('backend.add')?></button>
													<br/><br/>
													</div>
													
													</form>									
													<?php } ?>
												
										  </div>
										  
										  
										  <div class="modal-footer">
											<a class="btn" data-dismiss="modal" aria-hidden="true"><?=Kohana::lang('backend.cancel')?></a>
										  </div>
										</div>
									
															
						</td>
						
						
						
					</tr>
					<?php 
					
					} ?>
				</table>
				
			
					<?php echo $this->pagination;?>
<?php //$this->profiler = new Profiler();?>
		</div>
	
		
		</div>

