<?php defined('SYSPATH') or die('No direct script access');

/**
 * Description:  
 
 *
 * @package		 quickticket
 * @subpackage 	 Helper
 * @author 		 Absalom Shu
 * @copyright    (c) 2013
 * @license 	 traveleasy
 */
 
class Get_Core
{
	
	public function all_towns(){
		$towns=ORM::factory('town')->find_all();
		return $towns;
	}
	
	public function all_agencies(){
		$agencies=ORM::factory('agency')->find_all();
		return $agencies;
	}
	
	public function all_parents(){
		$parents=ORM::factory('parent')->find_all();
		return $parents;
	}
	
	public function all_agency_schedules($agency_id){
		$schedules=ORM::factory('schedule')->where('agency_id',$agency_id)->find_all();
		return $schedules;
	}
	
	public function all_agency_current_schedules($agency_id){
		$schedules=ORM::factory('schedule')->where('agency_id',$agency_id)->where('status','loading')->find_all();
		return $schedules;
	}
	
	public function all_agency_departed_schedules($agency_id){
		$schedules=ORM::factory('schedule')->where('agency_id',$agency_id)->where('status','departed')->find_all();
		return $schedules;
	}
	
	public function schedule($id){
		$schedule=ORM::factory('schedule')->where('schedule_id',$id)->find();
		return $schedule;
	}
	
	public function town($id){
		$town=ORM::factory('town')->where('id',$id)->find();
		return $town->name;
	}
	public function _parent($id){
		$parent=ORM::factory('parent')->where('id',$id)->find();
		return $parent->name;
	}
	
	

}
