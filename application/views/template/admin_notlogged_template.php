<!DOCTYPE html>
<html lang="en">

	<head>
	   <!-- <meta http-equiv="content-type" content="text/html; charset=iso-8859-1" /> -->
		<title><?=$title?> - <?=Kohana::lang('backend.online_booking')?> | QuickTicket</title>
		<link rel="shortcut icon" href="<?=url::base()?>images/fav.ico" type="image/x-icon" />
		<meta name="viewport" content="width=device-width, initial-scale=1.0">
		<meta name="description" content="description goes here">
		<meta name="keywords" content="keywords go here">
		<meta name="author" content="@absalomshu">
		
		 <link rel="shortcut icon" href="<?=url::base()?>ico/favicon.ico">
		<link rel="icon" href="<?=url::base()?>icons/fav.ico" type="image/x-icon">
		<link href="<?=url::base()?>css/bootstrap.css" rel="stylesheet">
		<link href="<?=url::base()?>css/style.css" rel="stylesheet">
		<link href="<?=url::base()?>css/datepicker.css" rel="stylesheet">
		<link href="<?=url::base()?>css/bootstrap-timepicker.min.css" rel="stylesheet"/>
		<link rel="stylesheet" href="<?=url::base()?>css/print.css" type="text/css" media="print">
		<link rel="canonical" href="http://www.quickticket.com" />
		<!--<link href="<?=url::base()?>css/bootstrap-responsive.css" rel="stylesheet">-->
		
		<script src="<?=url::base()?>js/jquery.js"></script>
		
		
		
	</head>

	<body>
		<?=render::admin_header();?>	

		<div class="container" >
			
			<?=$content?>
		</div> 
		
		<?php //render::breadcrumb();?>
		
		<div class="footer" >
			<?=render::template_footer();?>
		</div>
		
	</body>
</html>
