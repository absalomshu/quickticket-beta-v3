<?php defined('SYSPATH') or die('No direct script access'); ?> 


	<?php
		//What the user entered in his previous submission attempt, in case it was a failure
		$post = $this->session->get_once('post');
		
		 
	?>
	  <div class="row-fluid">
		<div class="span12 hero-unit prof-unit">
				
				<ul class="nav nav-tabs" >
			
					
					<li <?php if ($this->uri->segment(2)=='register_parcel'){?> class="active"<?php } ?>><a href="<?=url::site('parcels/register_parcel')?>"><i class="icon-plus"></i> <?=Kohana::lang('backend.add_parcel')?></a></li>					
					<li <?php if ($this->uri->segment(3)=='incoming'){?> class="active"<?php } ?>><a href="<?=url::site('parcels/all/incoming')?>"><i class="icon-download"></i> <?=Kohana::lang('backend.incoming')?></a></li>
					<li <?php if ($this->uri->segment(3)=='sent'){?> class="active"<?php } ?>><a href="<?=url::site('parcels/all/sent')?>"><i class="icon-upload"></i> <?=Kohana::lang('backend.outgoing')?></a></li>
					<li <?php if ($this->uri->segment(2)=='check_parcels'){?> class="active"<?php } ?>><a href="<?=url::site('parcels/check_parcels')?>"><i class="icon-search"></i> <?=Kohana::lang('backend.check_parcels')?></a></li>
				
				
					
				</ul>
				
				<!--<div class="tab-content">-->
				<?php if ($this->uri->segment(2)=='register_parcel'){ ?>
				<div class="tab-pane" id="add-parcel">
								<div class="span8 offset2">
							<form action="<?=url::site('parcels/register_parcel/'.$this->admin->agency_id)?>" method="POST" class="theform">
							<?php //no_csrf::set(1)?>
							
					<legend><?=Kohana::lang('backend.reg_parcels')?></legend>
						<?php
							//if he can manage mails and parcesl at the same time, activate the radio buttons so he choses
							//else disactivate both and show him the one he's authorised to manage
							if($is_allowed_manage_mails AND $is_allowed_manage_baggage){
						?>	
						<div class="">
							<div class="span4 text-right">Type:</div>
							<div class="span2"><input type="radio" name="parcel_type" value="mail" id="mail_radio" checked = "checked" /> <?=Kohana::lang('backend.mail')?></div>
							<div class="span3"><input type="radio" name="parcel_type" value="baggage" id="baggage_radio" /> <?=Kohana::lang('backend.baggage')?> </div>
						</div>
						<?php }else{ ?>
						<div class="">
							<div class="span4 text-right">Type:</div>
							<div class="span3"><input type="radio" name="parcel_type" value="mail" id="mail_radio" <?php if($is_allowed_manage_mails){echo 'checked = "checked"';} ?> disabled/> <?=Kohana::lang('backend.mail')?></div>
							<div class="span3"><input type="radio" name="parcel_type" value="baggage" id="baggage_radio" <?php if($is_allowed_manage_baggage){echo 'checked = "checked"';} ?> disabled/> <?=Kohana::lang('backend.baggage')?> </div>
						</div>
						<?php }?>
						<div>
							<div class="span4 text-right">To: </div>
								<div class="span8">
								<select name="agency_to" id="inputType">
											<?php foreach ($siblings as $sib):?>
											<option value="<?=$sib->id?>"><?=$sib->name?></option>
											<?php endforeach;?>
								</select>
								</div>
						</div>
						
						
						
						

						
						<div >
							<div class="span4 text-right"><?=Kohana::lang('backend.sender')?>: </div>
							<div class="span8"><input type="text" placeholder="<?=Kohana::lang('backend.sender_name')?>"  name="sender_name" value="<?=@$post['sender_name']?>"></div>
						</div>
						
						<div >
							<div class="span4 text-right"><?=Kohana::lang('backend.receiver')?>: <span class="red"> *</span></div>
							<div class="span8"><input type="text" placeholder="<?=Kohana::lang('backend.rec_name')?>"  name="receiver_name" value="<?=@$post['receiver_name']?>" required></div>
						</div>
						<div >
							<div class="span4 text-right"><?=Kohana::lang('backend.phone')?>: </div>
							<div class="span8"><input type="text" placeholder="<?=Kohana::lang('backend.rec_phone')?>"  name="receiver_phone" value="<?=@$post['receiver_phone']?>"></div>
						</div>
						<div>
							<div class="span4 text-right">Description: <span class="red"> *</span></div>
							<div class="span8"><textarea placeholder="<?=Kohana::lang('backend.desc_parcel')?>"  name="parcel_description" style="height:50px;" required><?=@$post['parcel_description']?></textarea></div>
						</div>
						<div>
							<div class="span4 text-right"><?=Kohana::lang('backend.price')?>: <span class="red"> *</span> </div>
							<div class="span8 input-append"><input type="text" class="IsAmount span5" placeholder="<?=Kohana::lang('backend.price')?>"  name="price" value="<?=@$post['price']?>" required><span class="add-on"> FCFA</span></div>
						</div>
						<div class="clear"></div>
						<div class="form-actions">
							<div class="span4"></div>
							<div class="span8">
								<button type="submit" class="btn btn-info"><?=Kohana::lang('backend.register')?></button>
								<a href="<?=url::site('parcels/all/incoming')?>" class="btn" type="submit"> <?=Kohana::lang('backend.cancel')?></a>
							</div>
						</div>
						
							
							</form>		
						</div>

						<!--end the first tab -->
				</div>  <?php } ?>
				
				
				<?php if ($this->uri->segment(3)=='incoming'){ ?>
				<div class="tab-pane" id="incoming-parcel">
				<legend><?=Kohana::lang('backend.incoming_parcels')?></legend>
					<table class="table table-striped table-hover table-condensed">
						<tr>
							<th>Date</th>
							<th><?=Kohana::lang('backend.from')?></th>
							<th><?=Kohana::lang('backend.receiver')?></th>
							<th><?=Kohana::lang('backend.receiver_phone')?></th>
							<th>Description</th>
							<th><?=Kohana::lang('backend.status')?></th>
							<th><?=Kohana::lang('backend.parcel_id');?></th>
							<th><?=Kohana::lang('backend.schedule_id');?></th>
							<th><?=Kohana::lang('backend.bus_number');?></th>
						</tr>
						<?php foreach ($incoming_parcels as $parcel):?>
						<tr>	
							<td><?=date("d-m-Y",strtotime($parcel->sent_date))?></td>
							
							<td><?php // echo get::agency_town_name($parcel->from);
							
										echo $parcel->town_name_from;
								?></td>
							
							
							<td><?=$parcel->receiver_name?></td>
							<!--Don't show zeroes-->
							<td><?php if(!empty($parcel->receiver_phone)){echo $parcel->receiver_phone;}?></td>
							<!--Trim if the description is too long. -->
							<td><a href="<?=url::site('parcels/view_incoming/'.$parcel->id)?>"><?php if (strlen($parcel->description) > 25) { echo substr($parcel->description,0,35)."...";} else { echo $parcel->description;}?></a></td>
						
							<td >
								<?php		
								//if the state is sent, and destination is the admin's agency
									
									//if($parcel->state == 'sent' AND $parcel->to == $this->admin->agency_id)
										//Receive and deliver are done on the same view_incoming_parcel page
										//So "Deliver" still takes you to the "receive " page
									if ($parcel->state == 'received') { ?>
								<span class=""><?=Kohana::lang('backend.received')?>! | <a href="<?=url::site('parcels/view_incoming/'.$parcel->id)?>" title="Has the owner of this parcel collected it?"><?=Kohana::lang('backend.deliver')?></a></span>
								<?php } elseif ($parcel->state == 'sent' AND $parcel->from == $this->admin->agency_id ) { ?>
								<span class=""><?=Kohana::lang('backend.sent')?></span>
								<?php } elseif ($parcel->state == 'delivered') { ?>
								<span class=""><?=Kohana::lang('backend.rec_del')?></span>
								<?php }	elseif($parcel->to == $this->admin->agency_id){?>
								<span class=""> <a href="<?=url::site('parcels/receive/'.$parcel->id)?>" title="Has this parcel been delivered to your agency?"><?=Kohana::lang('backend.receive')?></a></span>
								<?php }

										?>
							</td>
							<td>
								<?="PC".$parcel->global_parcel_id?>
							</td><td>
								<?
								//Only show if schedule_id is not empty
								if($parcel->schedule_id){
								echo "SC".$parcel->schedule_id;
								}
								?>
							</td>
							<td>
								<?=$parcel->bus_number?>
							</td>
						</tr>
						
						
						<?php endforeach; ?>
						</table>
						<div class="pagination"><?php echo $this->incoming_pagination;?>	</div>							
				</div>
				<?php }?>
				<?php if ($this->uri->segment(3)=='sent'){ ?>
				<div class="tab-pane" id="sent-parcel">
				<legend><?=Kohana::lang('backend.outgoing_parcels')?></legend>
					<table class="table table-striped table-hover table-condensed">
						<tr>
							<th>Date</th>
							<th><?=Kohana::lang('backend.To')?></th>
							<th><?=Kohana::lang('backend.receiver')?></th>
							<th><?=Kohana::lang('backend.receiver_phone')?></th>
							<th>Description</th>
							<th><?=Kohana::lang('backend.status')?></th>
							<!--<th><?=Kohana::lang('backend.parcel_id')?></th>-->
							<th><?=Kohana::lang('backend.schedule_or_bus')?></th>
							
							<!--<th><?=Kohana::lang('backend.schedule_id')?></th>-->
							
							<th>Actions</th>
						</tr>
						<?php foreach ($sent_parcels as $parcel):?>
						<tr>	
							<td><?=date("d-m-Y",strtotime($parcel->sent_date))?></td>
							
							<td><?php echo $parcel->town_name_to;?></td>
							
							<td><?=$parcel->receiver_name?></td>
							<!--Don't show zeroes-->
							<td><?php if(!empty($parcel->receiver_phone)){echo $parcel->receiver_phone;}?></td>
							<!--Trim if the description is too long. -->
							<td><a href="<?=url::site('parcels/view/'.$parcel->id)?>"><?php if (strlen($parcel->description) > 25) { echo substr($parcel->description,0,35)."...";} else { echo $parcel->description;}?></a></td>
						
							
								<?php		
								
								
								//Firstly, if it's from here, then it's sent.
								if($parcel->from == $this->admin->agency_id){
									//If it's been assigned to be sent on a particular schedule, show the schedule
								?>
								<td>
								<span class="tiny-text"><?=$parcel->state?></span>
								</td>
								<!--<td>PC<?=$this->agency_id.$parcel->id?></td>-->
								
								 <?php
									//If parcel has been attributed a schedule, show the schedule
									if($parcel->schedule_id)
									{ 
										$sched = Schedule_Model::get($parcel->schedule_id);?>
										<td >
											<!--<span   class=""><b><?=$sched->bus_number?></b> : <?=$this->agency_town?> -> <b><?=get::town($sched->to)?></b> - <b><?=date("g:i A", strtotime($sched->departure_time))?></b></span>
											<span title="<?=$this->agency_town?>-><?=get::town($sched->to)?>-<?=date("g:i A", strtotime($sched->departure_time))?>"><?=$sched->bus_number?></span>-->
										<a href='#' class="IsSchedule" data-title="<?='SC'.$sched->agency_id.$sched->id?>" data-content="<?=$sched->bus_number." ( ".get::town($sched->from). "  -->  ". get::town($sched->to)." ) ".date("d-m-Y",strtotime($sched->departure_date))."  ".date("g:i A", strtotime($sched->departure_time))?>" ><?=$sched->bus_number?></a>
										</td>
										<!--<td>
											<?php //'SC'.$this->agency_id.$sched->id?>
										</td>-->
									<?php } else{?>
										<td></td>
									
									<?php }?>
									
								
								<?php }	?>
							
							
							<td>
							<?php
							//Only show options if the parcel has not yet been sent.
							
							//var_dump($parcel);exit;
							//Only the person who created can modify or delete.
							if($parcel->state != 'sent'){
								if(strtoupper($this->admin->username) == $parcel->CreatedBy){
										
								
							?>
							
											<div class="btn-group ">
											  <a class="btn dropdown-toggle btn-mini" data-toggle="dropdown" href="#" >
														-- <?=Kohana::lang('backend.select')?> --
											  <span class="caret"></span>
											  </a>
											  <ul class="dropdown-menu">
													<!--Note that the ticket id is inserted as the id of the modal, and trigger/link to the modal. If not, each seat will be
													bringing up the same modal, hence only the first free ticket will be picked each time. VERY IMPORTANT-->
													<li><a data-toggle="modal" href="#add-to-schedule<?=$parcel->id?>" ><i class="icon-plus"></i> <?=Kohana::lang('backend.add_to_schedule')?></a></li>
													
													<li><a href="<?=url::site("parcels/mark_as_sent/".$parcel->id)?>"> <i class="icon-share"></i> <?=Kohana::lang('backend.mark_sent');?></a></li>
													
													
													<?php if(date("d-m-Y", strtotime($parcel->CreatedOn)) == date("d-m-Y")){?>
													<li><a href="<?=url::site("parcels/edit/".$parcel->id)?>"> <i class="icon-edit"></i> <?=Kohana::lang('backend.edit');?></a></li>
													<li><a href="<?=url::site("parcels/delete/".$parcel->id)?>" onClick="return confirm('<?=Kohana::lang('backend.confirm_delete')?>?');"> <i class="icon-trash"></i> <?=Kohana::lang('backend.delete');?></a></li>
													<?php }?>
												</ul>
												</div>
											
											  <!-- Modal for confirm missed to free ticket-->
												<div id="add-to-schedule<?=$parcel->id?>" class="modal hide fade" tabindex="-1" role="dialog" aria-labelledby="myModalLabel" aria-hidden="true">
												  <div class="modal-header">
													<button type="button" class="close" data-dismiss="modal" aria-hidden="true">×</button>
													<h3 id="myModalLabel"><?=Kohana::lang('backend.add_to_schedule')?></h3>
												  </div>
												  <div class="modal-body">
													<legend><?=Kohana::lang('backend.add_parcel_to_schedule')?></legend>
												
														  <?php 
														  foreach($current_schedules as $schedule){ 
														  ?>
														  
														  <form action="<?=url::site('parcels/add_to_schedule/'.$parcel->id)?>" method="POST" >
														  <!--Build a form identical to the complete_schedule form, as we'll post to the same function-->
															<input type="text" name="schedule_id" value="<?=$schedule->id?>" style="display:none" />
															
															
															<div class="span12">
															<span   class="span7">
																<b><?=$schedule->bus_number?></b> : <?=$this->agency_town?> -> <b><?=$schedule->town_name_to?></b> - <b><?=date("g:i A", strtotime($schedule->departure_time))?></b>
																<br>Departure: <span><i><?php if(date("d-m-Y", strtotime($schedule->departure_date)) == date("d-m-Y")){echo "Today";}else{ ?><span <?php if(strtotime($schedule->departure_date) < strtotime(date("d-m-Y"))){?>class="red"<?php }?> ><?php echo date("d-m-Y", strtotime($schedule->departure_date));}?></span></i></span>
																
															</span>
															<button  type="submit" name="check_out_form" class="btn btn-info"><?=Kohana::lang('backend.add')?></button>
															
															
															
															<br/><br/>
															</div>
															<div class="rule" style="clear:both;"><hr/></div>
															</form>									
															<?php } ?>
														
												  </div>
												  
												  
												  <div class="modal-footer">
													<a class="btn" data-dismiss="modal" aria-hidden="true"><?=Kohana::lang('backend.cancel')?></a>
												  </div>
												</div>
											
												
									
							<?php 	
								}
							} ?>
							</td>
						</tr>
						
						
						<?php endforeach; ?>
						</table>
						<div class="pagination"><?php echo $this->sent_pagination;?>	</div>	
						
				</div>
				<?php }?>
				
				<?php if ($this->uri->segment(2)=='check_parcels'){ ?>
				<div class="tab-pane" id="check-parcels">
					<form action="<?=url::site('parcels/check_parcels')?>" method="POST" class=" " >		
						
						<legend><?=Kohana::lang('backend.by_bus_number')?></legend>	
						<div class="">
							<div class="span4 text-right"><?=Kohana::lang('backend.by_bus_number')?><span class="red"> *</span>: </div>
							<div class="span5">
							
							
								<select name="bus_number">
								
									<?php 
										foreach ($all_buses as $bus){?>
										<option value="<?=$bus->bus_number?>"><?=$bus->bus_number?></option>
									<?php }; ?>
								
								</select>
							
							</div>
							
						</div>
						<div class="">
							
								<div class="span4 text-right"><?=Kohana::lang('backend.from');?><span class="red"> *</span>:</div>
								<div class="span6">
									<div class="input-append date span6">
										<input type="text" name = 'start_date' class="span8 datepicker"  value="<?=date('d-m-Y')?>" id="dp1" >
										<span class="add-on"><i class="icon-calendar"></i></span>
									</div>	
								</div>
						</div>
						<div class="">
								<div class="span4 text-right"><?=Kohana::lang('backend.to');?><span class="red"> *</span>:</div>
								<div class="span6">
									<div class="input-append date span6">
										<input type="text" name = 'end_date' class="span8 datepicker"  value="<?=date('d-m-Y')?>" id="dp1" >
										<span class="add-on"><i class="icon-calendar"></i></span>
									</div>	
								</div>
						</div>	
						<div class="clear"></div>
						<div class="form-actions">	
							<div class="span4"></div>
							<div class="span4"><button class="btn  btn-info" type="submit" name="by_bus_number"  ><i class="icon-search icon-white"></i> <?=Kohana::lang('backend.check');?></button> </div>
						</div>
						
						
						<legend><?=Kohana::lang('backend.select_date')?></legend>	
						<div class="">
							<div class="span4 text-right"><?=Kohana::lang('backend.date')?><span class="red"> *</span>: </div>
							<div class="span4">
								<div class="input-append date left">
									<input type="text" name = 'date' class="span7 datepicker"  value="<?=date('d-m-Y')?>" id="dp1">
									<span class="add-on"><i class="icon-calendar"></i></span>
								</div>
							</div>	
							<div class="clear"></div>
							<div class="form-actions">
								<div class="span4"></div>	
								<div class="span4"><button class="btn  btn-info" type="submit" name="by_date"  ><i class="icon-search icon-white"></i> <?=Kohana::lang('backend.check');?></button> </div>
							</div>
						
						</div>
						
						
						<legend><?=Kohana::lang('backend.by_current_schedule')?></legend>	
						<div class="">
							<div class="span4 text-right"><?=Kohana::lang('backend.current_schedules');?>: </div>
							<div class="span8">
							<select name="current_schedule_id" id="inputType" class="span5">
															
								<?php foreach ($current_schedules as $sched):?>
								<option value="<?=$sched->id?>"><span   ><b><?=$sched->bus_number?></b> --> <?=$sched->town_name_to?></b> - <b><?=date("g:i A", strtotime($sched->departure_time))?></b></span></option>
								<?php endforeach;?>
							</select>
							</div>
							
							<div class="clear"></div>
							<div class="form-actions">
								<div class="span4"></div>	
							<div class="span4"><button class="btn  btn-info" type="submit" name="by_current_schedule"  <?php if($current_schedules->count()==0){?> disabled<?php }?>><i class="icon-search icon-white"></i> <?=Kohana::lang('backend.check');?></button> </div>
							</div>
						</div>
						
						
						<legend><?=Kohana::lang('backend.by_schedule_id')?></legend>	
						<div class="">
							<div class="span4 text-right"><?=Kohana::lang('backend.schedule_id')?><span class="red"> *</span>: </div>
							<div class="span8">
								<input type="text" name = 'schedule_id' class="span4" >
							</div>
							
							<div class="clear"></div>
							<div class="form-actions">
								<div class="span4"></div>
								<div class="span4"><button class="btn  btn-info" type="submit" name="by_schedule_id" placeholder="e.g. SC10023" ><i class="icon-search icon-white"></i> <?=Kohana::lang('backend.check');?></button> </div>
							</div>
						</div>
						
					</form>	
						
					<legend><?=Kohana::lang('backend.by_client_name')?></legend>	
					<form action="<?=url::site('parcels/client_search')?>" method="POST" class="" >
					<div class="">
						<div class="span4 text-right"><?=Kohana::lang('backend.client_name')?><span class="red"> *</span>: </div>
						<div class="span6">
							<input type="text" name = 'passenger_name' class="span7" required>
						</div>
					</div>
					<div class="" style="margin-left:0px;">
						<div class="span4 text-right"><?=Kohana::lang('backend.in')?><span class="red"> *</span>: </div>
						<div class="span8">
							<select name="parcel_classification" class="span5">
								<option value="search_incoming">Incoming parcels</option>
								<option value="search_outgoing">Outgoing parcels</option>
							</select>
							
						</div>
					</div>
					
					<div class="clear"></div>
					<div class="form-actions">	
							<div class="span4"></div>
							<div class="span4"><button class="btn  btn-info" type="submit" name="by_name" ><i class="icon-search icon-white"></i> <?=Kohana::lang('backend.check');?></button> </div>
					</div>
					</form>
				</div>
				<?php }?>
	
	
	
		
		
		<?php //$this->profiler = new Profiler;?>
	
		
		</div>
	  <div style="height:19px;"></div>
    </div> 
<script type="text/javascript">
				$(function () 
				{
					$('#myTab a').click(function (e) {
					e.preventDefault();
					$(this).tab('show');  
					})
				}) 
</script>