<?php
/**
 * Description: wasaTEXTO API v2
 * Send SMS
 * @package wasamundi.com
 * @subpackage:api
 * @license wasamundi labs 2011
 * @author Wasamundi Team
 */
//prevent the script from crashing after the default 30 seconds max execution time
ini_set("max_execution_time", "600000");
 
class Qt_email_Core
{
	
	 
	
	
	public function send($to, $from, $subject, $message) 
	{
		
		$template_and_message = '<html>
									<head></head>
									<body style="background:#eee;font-family: Century Gothic, Verdana;">
															
											
											<div style="width:600px;height:auto;border: solid 1px #BDBCBC;background-color:#fff;margin: 10px auto;">
												<div style="background-color:#3299bb;width:100%;">
													<div style="color:#fff;font-weight:bold;padding:20px;font-size:18px;">
														
															&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;
															&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;
															&nbsp;&nbsp;&nbsp;&nbsp;
														<span style="font-size:25px;font-weight:normal;">QuickTicket</span>
													</div>
													
												</div>
												<div style="padding:20px;">'.$message.'
													
													Best regards,<br/>
													The Team at QuickTicket.

												</div>
												<div style="color:#BDBCBC;font-size:11px;border-top: solid 1px #BDBCBC;width:90%;margin:5px auto;padding: 10px 0px;">
													&copy; 2015 QuickTicket
												</div>
											</div>
											
									</body>
								</html>';
		
		$headers  = "MIME-Version: 1.0" . "\r\n";
		$headers .= "Content-type: text/html; charset=iso-8859-1" . "\r\n";
		$headers .= "From: QuickTicket <info@quickticket.co>" . "\r\n" ;
		$headers .=	"X-Mailer: PHP/" . phpversion();
		$result = mail($to, $subject, $template_and_message, $headers);
		return $result;
	}
	
	public function encrypt_string($string)
	{	
		//Key changes every hour so that every password change request link is valid for one hour only
		$key = 'Encrypt-Password'.date('dmyH');
		$iv = mcrypt_create_iv(mcrypt_get_iv_size(MCRYPT_RIJNDAEL_256, MCRYPT_MODE_CBC),MCRYPT_DEV_URANDOM);
		$encrypted = base64_encode($iv .mcrypt_encrypt(MCRYPT_RIJNDAEL_256,hash('sha256', $key, true),$string,MCRYPT_MODE_CBC,$iv));
		//url base64_encode AGAIN so the forward slashes are taken care of. One time still gives slashes which interfere with URL
		$encrypted_string = base64_encode($encrypted);
		
		return $encrypted_string;

	}
	
	public function decrypt_string($encrypted_string)
	{	
		$key = 'Encrypt-Password'.date('dmyH');
		//var_dump($key);exit;
		//first decrypt the URL segment 3 which is the encrypted email
		//base64_decode so any forward slashes are put back
		$encrypted_string=base64_decode($encrypted_string);
		$data = base64_decode($encrypted_string);
		$iv = substr($data, 0, mcrypt_get_iv_size(MCRYPT_RIJNDAEL_256, MCRYPT_MODE_CBC));
		$decrypted_string = rtrim(mcrypt_decrypt(MCRYPT_RIJNDAEL_256,hash('sha256', $key, true),substr($data, mcrypt_get_iv_size(MCRYPT_RIJNDAEL_256, MCRYPT_MODE_CBC)),MCRYPT_MODE_CBC,$iv),"\0");
		
		return $decrypted_string;
	}
	
	
	
}