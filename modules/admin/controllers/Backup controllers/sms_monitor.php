<?php defined('SYSPATH') or die('No direct script access');

//Almost all functions will take as parameter, the id of the agency we are dealing with
//in order not to interfere with another's stuff.

class Sms_monitor_Controller extends Admin_Controller{
		
		public function __construct()
		{		
			$this->admin = Authlite::instance()->get_admin();
			$this->session = Session::instance();
		
			if (!empty($this->admin)) 
			{
				$this->agency_id = $this->admin->agency_id;
			}
		}
		
		/* live update of the the number of SMS sent		*/
		public function sms_today_count($agency_id)
		{
			$sms_sent = sms::get_today_count($agency_id);	
			echo "SMS sent today:  <button type='button' class='btn btn-large btn-primary btn-warning disabled' disabled='disabled'>".$sms_sent."</button>";
		}
		
		public function sms_sent_count($agency_id)
		{
			$sms_sent = sms::get_sent_count($agency_id);
			echo "Total SMS sent:  <button type='button' class='btn btn-large btn-primary btn-warning disabled' disabled='disabled'>".$sms_sent."</button>";
		}
		
		//Trigger the code online to dispatch all pending SMS
		public function trigger_dispatch()
		{	
			$host = 'http://dev.quickticket.cm/sms_dispatch/dispatch';
				
				$agency_id = $this->agency_id;
				
				$agency_id = urlencode($agency_id);
				$uri = "agency_id=".$agency_id;
			
				$data = curl_init($host); //Initialize the cURL session
				//Set the URL //Ask cURL to return the contents in a variable 
				//instead of simply echoing them to  the browser.	
				curl_setopt($data, CURLOPT_POSTFIELDS, $uri);
				curl_setopt($data, CURLOPT_RETURNTRANSFER, 1);
				//curl_setopt($data, CURLOPT_FOLLOWLOCATION, 1);
				//Execute the cURL session
				$response = curl_exec($data);
		
				//Close cURL session
				curl_close($data);
				//$result = json_decode($notice);
				echo $response;
		}
		
		public function update()
		{
			$unpushed_sms = sms::get_unpushed_sms();
			$this->push_sms($unpushed_sms);
		}
	
		public function push_sms($unpushed_sms)
		{
			$onlinedb = new Database('online_db');
			$offlinedb = new Database('offline_db');
			
			foreach($unpushed_sms as $sms)
			{
				$push = $onlinedb->insert('sms_history',array(
											
											'agency_id' => $this->admin->agency_id,
											'phone' => $sms->phone,
											'message' => $sms->message,
											'date' => $sms->date,
											'delivered' => '0'
											));
				$updatesms = $offlinedb->update('sms',array('online'=>'1'),array('id' => $sms->id, 'agency_id' => $sms->agency_id));

			}echo "All SMS sent online";
		}
		
		
		}