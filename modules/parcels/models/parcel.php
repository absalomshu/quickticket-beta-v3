<?php defined('SYSPATH') or die('No direct script access');

class Parcel_Model extends ORM{
	
	public function add_parcel($type, $sender_name, $receiver_name, $receiver_phone, $description, $receiver_name, $agency_id, $to, $price, $CreatedBy){
		$parcel = ORM::factory('parcel');
		$parcel->type = $type;
		$parcel->sender_name = $sender_name;
		$parcel->receiver_phone = $receiver_phone;
		$parcel->description = $description;
		$parcel->receiver_name = $receiver_name;
		$parcel->agency_id = $agency_id;
		$parcel->from = $agency_id;
		$parcel->to = $to;
		$parcel->registered_date = date('Y-m-d');
		//$parcel->sent_date = date('Y-m-d');
		$parcel->price = $price;
		$parcel->state = 'registered';
		$parcel->CreatedBy = strtoupper($CreatedBy);
		$parcel->save();
		
		//Update the admin's available balance
		$admin = ORM::FACTORY('admin')->where('username',$CreatedBy)->find();
		$admin->available_balance = $admin->available_balance + $price;
		$admin->save();
	}
	
	public function edit_parcel($parcel_id, $type, $sender_name, $receiver_name, $receiver_phone, $description, $receiver_name, $agency_id, $to, $price, $CreatedBy){
		$parcel=ORM::factory('parcel')->where('agency_id',$agency_id)->where('id',$parcel_id)->find();
		
		//First save the old price. So if he changes, subtract the old one and add the new one to his available balance.
		$old_price = $parcel->price ;
		
		$parcel->type = $type;
		$parcel->sender_name = $sender_name;
		$parcel->receiver_phone = $receiver_phone;
		$parcel->description = $description;
		$parcel->receiver_name = $receiver_name;
		$parcel->agency_id = $agency_id;
		$parcel->from = $agency_id;
		$parcel->to = $to;
		//$parcel->sent_date = date('Y-m-d');
		$parcel->price = $price;
		$parcel->state = 'registered';
		$parcel->CreatedBy = strtoupper($CreatedBy);
		$parcel->save();
		
		//Update the admin's available balance
		$admin = ORM::FACTORY('admin')->where('username',$CreatedBy)->find();
		$admin->available_balance = $admin->available_balance -$old_price + $price;
		$admin->save();
	}
	
	
	public function get($agency_id, $parcel_id)
	{
		$parcel=ORM::factory('parcel')->where('agency_id',$agency_id)->where('id',$parcel_id)->find();
		return $parcel;	
	}
	
	public function add_to_schedule($parcel_id, $schedule_id, $bus_number)
	{
		$parcel = ORM::FACTORY('parcel')->where('id',$parcel_id)->find();	
		$parcel->schedule_id = $schedule_id;
		$parcel->bus_number = $bus_number;
		$parcel->save();
		return $parcel;
	}
	
	public function get_parcels_on_schedule($schedule_id)
	{
		$parcels = ORM::FACTORY('parcel')->where('schedule_id',$schedule_id)->find_all();	
		return $parcels;
	}
	public function mark_sent($parcel_id)
	{
		$parcel = ORM::FACTORY('parcel')->where('id',$parcel_id)->find();	
		$parcel->sent_date = date('Y-m-d');
		$parcel->state='sent';
		$parcel->save();
	
		return $parcel;
	}
	
	public function get_outgoing_parcels_by_date($agency_id,$date)
	{	
		//All outgoing parcels by the date they were registered
		$parcels = ORM::FACTORY('parcel')->where('agency_id',$agency_id)->where('registered_date',$date)->find_all();	
		
		return $parcels;
	}
	public function get_sent_parcels_by_date($agency_id,$date)
	{
		$parcels = ORM::FACTORY('parcel')->where('agency_id',$agency_id)->where('sent_date',$date)->find_all();	
		
		return $parcels;
	}
	
	public function get_outgoing_parcels_by_current_schedule($agency_id,$current_schedule_id)
	{
		$parcels = ORM::FACTORY('parcel')->where('agency_id',$agency_id)->where('schedule_id',$current_schedule_id)->find_all();	
		
		return $parcels;
	}
	public function get_outgoing_parcels_by_schedule_id($agency_id,$schedule_id)
	{
		$parcels = ORM::FACTORY('parcel')->where('agency_id',$agency_id)->where('schedule_id',$schedule_id)->find_all();	
		
		return $parcels;
	}
	
	public function count_total_outgoing_today($agency_id)
	{	
		$today = date('Y-m-d');
		$count=ORM::factory('parcel')->where('agency_id',$agency_id)->where('registered_date',$today)->count_all();
		
		return $count;	
	}
	
	public function count_unsent_today($agency_id)
	{	
		//unsent is registered today, with state still 'registered'
		$today = date('Y-m-d');
		$count=ORM::factory('parcel')->where('agency_id',$agency_id)->where('state','registered')->where('registered_date',$today)->count_all();
		
		return $count;	
	}
	
	public function count_sent_today($agency_id)
	{	
		$today = date('Y-m-d');
		$count=ORM::factory('parcel')->where('agency_id',$agency_id)->where('state','sent')->where('registered_date',$today)->count_all();
		return $count;	
	}
	
	public function get_all_outgoing_today($agency_id)
	{	
		$today = date('Y-m-d');
		$parcels=ORM::factory('parcel')->where('agency_id',$agency_id)->where('registered_date',$today)->find_all();
		return $parcels;	
	}
	
	public function get_outgoing_parcels_by_bus_number($agency_id,$bus_number)
	{
		$parcels = ORM::FACTORY('parcel')->where('agency_id',$agency_id)->where('bus_number',$bus_number)->find_all();	
		
		return $parcels;
	}
	
	//Raw SQL used here as Kohana doesn't seem to have BETWEEN
	public function get_agency_parcels_by_period($agency_id,$start_date,$end_date,$admin)
	{
		$db = new Database();
		$parcels = $db->query("SELECT * FROM parcels WHERE agency_id= ".$agency_id." AND CreatedBy= '".$admin."' AND registered_date BETWEEN CAST('$start_date' AS DATE) AND CAST('$end_date' AS DATE) ORDER BY sent_date ASC");
		
		return $parcels;
	}
	
	//Raw SQL used here as Kohana doesn't seem to have BETWEEN
	public function get_agency_parcels_by_period_by_admin($agency_id,$start_date,$end_date,$admin)	{
		$db = new Database();
		$parcels = $db->query("SELECT * FROM parcels WHERE agency_id= ".$agency_id." AND CreatedBy= '".$admin."' AND registered_date BETWEEN CAST('$start_date' AS DATE) AND CAST('$end_date' AS DATE) ORDER BY sent_date ASC");
		
		return $parcels;
	}
	
	public function get_agency_parcel_total_by_period_by_admin($agency_id,$start_date,$end_date,$admin){
		$parcels=Parcel_Model::get_agency_parcels_by_period_by_admin($agency_id,$start_date,$end_date,$admin);
		$parcels_total = 0;
		foreach($parcels as $parcel){	
			$parcels_total += $parcel->price;
		}
		return $parcels_total;
	}
	
	public function get_outgoing_parcels_by_bus_number_and_period($agency_id,$bus_number,$start_date,$end_date)	{
		$db = new Database();
		$parcels = $db->query("SELECT * FROM parcels WHERE agency_id= ".$agency_id." AND bus_number= '".$bus_number."' AND registered_date BETWEEN CAST('$start_date' AS DATE) AND CAST('$end_date' AS DATE) ORDER BY sent_date ASC");
		return $parcels;
	}
	
	public function mark_as_sent($agency_id, $parcel_id)	{
		
		$parcel=ORM::factory('parcel')->where('agency_id',$agency_id)->where('id',$parcel_id)->find();
		$parcel->state = 'sent';
		$parcel->save();
		
		$notice="Parcel marked as sent";
		return $notice;	
	}
	
		public function delete_parcel($agency_id, $parcel_id)
	{

		$parcel=ORM::factory('parcel')->where('agency_id',$agency_id)->where('id',$parcel_id)->find();
		
		//$parcel->save();
		
		
		//Update the admin's available balance
		$admin = ORM::FACTORY('admin')->where('username',$parcel->CreatedBy)->find();
		$admin->available_balance = $admin->available_balance - $parcel->price;
		$admin->save();
		
		//Must delete only after updating admin's balance, if not, admin can't be found as parcel is already deleted.
		$parcel->delete();
		$parcel->save();
		
		$notice="Parcel deleted";
		return $notice;
		
	}
	
	public function search_client_by_name($agency_id, $client_name)
	{
		$db = new Database();
		$tickets = $db->query("SELECT * FROM parcels WHERE agency_id= ".$agency_id." AND sender_name LIKE '%".$client_name."%' ORDER BY CreatedOn DESC");
		return $tickets;
	}
	
	public function all_agency_parcels_by_date($agency_id,$date){
		//$parcels=ORM::factory('parcel')->where('from',$agency_id)->where('sent_date',$date)->find_all();
		
		//SHOULD BE REGISTERED DATE NOT SENT DATE
		$parcels=ORM::factory('parcel')->where('from',$agency_id)->where('registered_date',$date)->find_all();
		return $parcels;
	}
	
	public function get_parcel_income_by_date($agency_id,$date){
		$date = date("Y-m-d",strtotime($date));
		$period_parcels=Parcel_Model::all_agency_parcels_by_date($agency_id,$date);
		//Income from parcels
		$parcel_total = 0;
		foreach ($period_parcels as $parcel){
			$parcel_total += $parcel->price;
		}
		return $parcel_total;
	}
	
	public function get_all_agency_outgoing_parcels($agency_id){
		$parcels=ORM::factory('parcel')->where('from',$agency_id)->orderby('sent_date','DESC')->find_all();
		return $parcels;
	}
	
}
