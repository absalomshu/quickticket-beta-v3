<?php defined('SYSPATH') or die('No direct script access');


date_default_timezone_set('Africa/Douala');

class SMS_Controller extends Admin_Controller {

	public function index()
	{	
	
		
		if (empty($this->admin)) 
		{	
			url::redirect('admin/login');
		}
		else
		 $this->main();
	}
	

	public function main() 
	{	
		
		$PHPprotectV17 = array(3);
		Authlite::check_access($PHPprotectV17);
		$PHPprotectV79 = $this->admin->admin_group_id;
		$PHPprotectV3 = $this->agency_id;
		Authlite::check_admin();
		Authlite::verify_referer();		
		
		$this->template->title = Kohana::lang('backend.sms_management');
		$PHPprotectV16=new View ('sms_main');
		
		$PHPprotectV19 = get::all_towns();
		$PHPprotectV81 =get::admins_town($this->admin->agency_id);
		$PHPprotectV57 = get::_parent($this->admin->agency_id)->id;
		$PHPprotectV121 = get::all_children($PHPprotectV57);
		
		


		
		
		
		if ($PHPprotectV79 == 3){
			$PHPprotectV20=get::ten_agency_current_schedules($PHPprotectV3);
			$PHPprotectV21=get::ten_agency_departed_schedules($PHPprotectV3);
			}
		if ($PHPprotectV79 == 4){
			
			
			if($PHPprotectV120 == 0){
			$PHPprotectV20=get::all_parent_current_schedules($PHPprotectV57);
			$PHPprotectV21=get::all_parent_departed_schedules($PHPprotectV57);
			}else
			{	
				$PHPprotectV20=get::ten_agency_current_schedules($PHPprotectV120);
				$PHPprotectV21=get::ten_agency_departed_schedules($PHPprotectV120);
			}
			
			}
		$PHPprotectV26=date('d-m-Y');
		
		
		
		$PHPprotectV16->current_schedules=$PHPprotectV20;
		$PHPprotectV16->departed_schedules=$PHPprotectV21;
		
	
		
		$PHPprotectV16->set_global('children',$PHPprotectV121);
		
		$PHPprotectV16->towns=$PHPprotectV19;
		$PHPprotectV16->user_group=$PHPprotectV79;
		
		$this->template->content=$PHPprotectV16;
		
	}
	
		
	/*
	
	public function schedule_by_date()
	{	
		Authlite::check_admin();
		$PHPprotectV3 = $this->agency_id;
		$PHPprotectV57 = get::_parent($PHPprotectV3)->id;
		$PHPprotectV82 = $this->admin->admin_group_id;
		
		$this->template->title = Kohana::lang('backend.welcome_backend');
		if($_POST)
		{
			$PHPprotectV9 = new Validation($_POST);
			$PHPprotectV9->add_rules('date','required');
			if ($PHPprotectV9->validate())
			{	
				
			$PHPprotectV26 = date("Y-m-d",strtotime($_POST['date']));
			
			
			
			if ($PHPprotectV82==4){
			$PHPprotectV84 = ORM::FACTORY('schedule')->join('agencies','agencies.id','schedules.agency_id')->where('parent_id',$PHPprotectV57)->where('departure_date',$PHPprotectV26)->where('status','departed')->where('bus_seats',30)->count_all();
			$PHPprotectV122 = ORM::FACTORY('schedule')->join('agencies','agencies.id','schedules.agency_id')->where('parent_id',$PHPprotectV57)->where('departure_date',$PHPprotectV26)->where('status','departed')->where('bus_seats',35)->count_all();
			$PHPprotectV123 = ORM::FACTORY('schedule')->join('agencies','agencies.id','schedules.agency_id')->where('parent_id',$PHPprotectV57)->where('departure_date',$PHPprotectV26)->where('status','departed')->where('bus_seats',39)->count_all();
			$PHPprotectV85 = ORM::FACTORY('schedule')->join('agencies','agencies.id','schedules.agency_id')->where('parent_id',$PHPprotectV57)->where('departure_date',$PHPprotectV26)->where('status','departed')->where('bus_seats',55)->count_all();
			$PHPprotectV86 = ORM::FACTORY('schedule')->join('agencies','agencies.id','schedules.agency_id')->where('parent_id',$PHPprotectV57)->where('departure_date',$PHPprotectV26)->where('status','departed')->where('bus_seats',70)->count_all();
			
			
			$PHPprotectV21 = get::all_parent_departed_schedules_by_date($PHPprotectV57,$PHPprotectV26);				
			$PHPprotectV20 = get::all_parent_current_schedules_by_date($PHPprotectV57,$PHPprotectV26); 
			$PHPprotectV22=get::all_parent_parcels_by_date($PHPprotectV57,$PHPprotectV26);
			}
			
			else{
			$PHPprotectV84 = ORM::FACTORY('schedule')->where('agency_id',$PHPprotectV3)->where('departure_date',$PHPprotectV26)->where('status','departed')->where('bus_seats',30)->count_all();
			$PHPprotectV122 = ORM::FACTORY('schedule')->where('agency_id',$PHPprotectV3)->where('departure_date',$PHPprotectV26)->where('status','departed')->where('bus_seats',35)->count_all();
			$PHPprotectV123 = ORM::FACTORY('schedule')->where('agency_id',$PHPprotectV3)->where('departure_date',$PHPprotectV26)->where('status','departed')->where('bus_seats',39)->count_all();
			$PHPprotectV85 = ORM::FACTORY('schedule')->where('agency_id',$PHPprotectV3)->where('departure_date',$PHPprotectV26)->where('status','departed')->where('bus_seats',55)->count_all();
			$PHPprotectV86 = ORM::FACTORY('schedule')->where('agency_id',$PHPprotectV3)->where('departure_date',$PHPprotectV26)->where('status','departed')->where('bus_seats',70)->count_all();
						
			$PHPprotectV21 = get::all_agency_departed_schedules_by_date($PHPprotectV3,$PHPprotectV26);				
			$PHPprotectV20 = get::all_agency_current_schedules_by_date($PHPprotectV3,$PHPprotectV26);
			$PHPprotectV22=get::all_agency_parcels_by_date($PHPprotectV3,$PHPprotectV26);
			}
			
			

			
			$PHPprotectV87 = 0;
			foreach($PHPprotectV21 as $PHPprotectV88){
				$PHPprotectV87 += $PHPprotectV88->total_amount;
			}
			}
			else
			{
				$PHPprotectV14=$PHPprotectV9->errors('errors');
				$PHPprotectV8="";
					foreach($PHPprotectV14 as $PHPprotectV15) 
					{
						$PHPprotectV8.=$PHPprotectV15."<br />";
					}
					$this->session->set('notice', array('message'=>$PHPprotectV8,'type'=>'error'));
					url::redirect('admin/main/'.$this->admin->agency_id);
			}
	}
	$PHPprotectV16 = new View('admin_schedule_by_date');
	$PHPprotectV16->departed_schedules = $PHPprotectV21;
	$PHPprotectV16->current_schedules = $PHPprotectV20;
	$PHPprotectV16->parcels=$PHPprotectV22;
	$PHPprotectV16->_30seats = $PHPprotectV84;
	$PHPprotectV16->_35seats = $PHPprotectV122;
	$PHPprotectV16->_39seats = $PHPprotectV123;
	$PHPprotectV16->_55seats = $PHPprotectV85;
	$PHPprotectV16->_70seats = $PHPprotectV86;
	$PHPprotectV16->total = $PHPprotectV87;
	$PHPprotectV16->date = $PHPprotectV26;
		
	$this->template->content = $PHPprotectV16;
	
	}	
	
	
	public function all_schedules($PHPprotectV82)
	{	
		Authlite::check_admin();
		$PHPprotectV3 = $this->agency_id;
		
		
		$PHPprotectV91 = 10;
		if ($PHPprotectV82 == 'current'){
			$PHPprotectV92= get::all_agency_current_schedules($PHPprotectV3);}else{
			$PHPprotectV92= get::all_agency_departed_schedules($PHPprotectV3);}
		$PHPprotectV87 = count($PHPprotectV92);
		$this->pagination = new Pagination(array(
			
			'uri_segment'    => 'page', 
			'total_items'    => $PHPprotectV87, 
			'items_per_page' => $PHPprotectV91 , 
			'style'          => 'punbb', 
			
));
		if ($PHPprotectV82 == 'current'){
			
			$PHPprotectV83=ORM::factory('schedule')->where('agency_id',$PHPprotectV3)->where('status','current')->limit($PHPprotectV91,$this->pagination->sql_offset)->find_all();			
		}else{
			
			$PHPprotectV83=ORM::factory('schedule')->where('agency_id',$PHPprotectV3)->where('status','departed')->limit($PHPprotectV91,$this->pagination->sql_offset)->find_all();			
		}
		
		$this->template->title = 'Viewing all schedules';		
		$PHPprotectV16 = new View('control_all_schedules');
		$PHPprotectV16->schedules = $PHPprotectV83;
		$this->template->content = $PHPprotectV16;
	
	} */
	
		
		
		}
