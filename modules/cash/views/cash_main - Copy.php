<?php defined('SYSPATH') or die('No direct script access'); ?> 
	

 
	<div class="row-fluid ">
	  	<div class="span12 hero-unit prof-unit" >
		
		<ul class="nav nav-tabs" id="myTab">
					
					<li class="active"><a href="#collection"><i class="icon-align-justify"></i> <?=Kohana::lang('backend.collection')?></a></li>
					<li><a href="#cash-to-bank"><i class="icon-align-justify"></i> <?=Kohana::lang('backend.cash_to_bank')?></a></li>
		</ul>
		
		<div class="tab-content">
			 
			
			
			
			
			
			
			<div class="tab-pane active" id="collection">
				<div class="span8 offset2">
				
					<legend><?=Kohana::lang('backend.admin_collections')?></legend>						
						<form action="<?=url::site('cash/collection_by_admin/')?>" method="POST" >
						<div class="">
							<div class="span4 text-right"><?=Kohana::lang('backend.admin')?><span class="red"> *</span>: </div>
							<div class="span8">
								<select name="admin" id="inputType">
								
									<?php 
										foreach ($admins_excluding_current as $admin){?>
										<option value="<?=$admin->username?>"><?=$admin->username?></option>
									<?php }; ?>
								
							</select>
							
							</div>
						</div>
					
						<div class="">
							<div class="span4 text-right"><?=Kohana::lang('backend.date');?>:</div>
							
							<div class="input-append date span4">
								<input type="text" name = 'date' class="span8 datepicker"  value="<?=date('d-m-Y')?>" id="dp1" >
								<span class="add-on"><i class="icon-calendar"></i></span>
							</div>	
							
							
							<div class="clear"></div>
						<div class="form-actions">
							<div class="span4"></div>
							<div class="span8">
								<button class="btn  btn-info" type="submit" ><i class="icon-search icon-white"></i> <?=Kohana::lang('backend.view');?></button><br/>
							</div>
						</div>
						</div>
					</form>	
				</div>	
					
			</div>
			
			<div class="tab-pane" id="cash-to-bank">
				<div class="span8 offset2">
				
					<legend><?=Kohana::lang('backend.cash_to_bank')?><div class='view'><a href="<?=url::site('cash/add_cash_to_bank')?>">+ Add cash-to-bank</a></div></legend>						
						<table class="table table-striped table-hover table-condensed">
					<tr>
						
						<th><?=Kohana::lang('backend.date')?></th>
						<th><?=Kohana::lang('backend.bank_name')?></th>
						<th><?=Kohana::lang('backend.account_number')?></th>
						<th><?=Kohana::lang('backend.amount')?></th>
						<th><?=Kohana::lang('backend.done_by')?></th>
												
					</tr>
					<?php foreach ($ctb as $trx):?>
						<tr>	
							<td><?=date("d-m-Y",strtotime($trx->date_incurred))?></td>
							<td><?=$trx->bank_name?></td>
							<td><?=$trx->account_number?></td>
							<td><?=number_format($trx->amount)?></td>
							<td><?=$trx->created_by?></td>
							
						</tr>
						<?php endforeach; ?>
				</table>	
			<?php echo $this->pagination;?>	
						
						
						
						
						
						
				</div>	
					
			</div>
					
		</div>
		
			
		
			  
		</div>
		
		</div>
	  <div class="clear"></div>

<script type="text/javascript">
	$(document).ready(function(){
			$('#timepicker').timepicker({
				minuteStep:30,
				//defaultTime: '8:00 PM'
			});
			
			$('.input-append.date').datepicker({
				format: "dd-mm-yyyy",
				todayBtn: "linked",
				autoclose: true,
				todayHighlight: true
			});
			
			
				$(function () {
				$('#myTab a').click(function (e) {
					  e.preventDefault();
					  $(this).tab('show');  
			})
			  })
			
	})
	</script>