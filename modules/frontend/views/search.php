<?php defined('SYSPATH') or die('No direct script access'); ?> 


    <div class="container">		
	  	
	<div class="row-fluid marketing" style="margin:5px 0 0px 0;">	
	
		<div class="span8 " >
		
		<div class="span12 hero-unit prof-unit" id="search-title" >
			<div class="heading"><span style="color: #f89406;">Live Schedules</span>  <span class="unbold">| From:</span> <?=get::town($town_from)?> <span class="unbold">To:</span> <?=get::town($town_to)?> <span class="unbold">On:</span> <?=date("d-m-Y", strtotime($departure_date))?> <a class="view" href="#" id="modify-search-button">Modify</div></a>
		</div>	
		
		<div class="span12 hero-unit prof-unit" style="margin:5px 0 0px 0;display:none;" id="modify-search-body"  >
			<form action="<?=url::site('home/search_schedule')?>" method="GET" >
			<div class="span1">From:</div>
			<div class="span3 ">
				<select name="from" id="town_from" class="span10">
					<option value="<?=$town_from?>"><?=get::town($town_from)?></option>
					<?php foreach ($towns as $town):
						//for now, only Yaounde and buea
						if($town->id==3 OR $town->id==1){?>
							<option value="<?=$town->id?>"><?=$town->name?></option>
						<?php } endforeach;?>
				</select>
			</div>
			<div class="span1">To:</div>
			<div class="span3 ">
				<select name="to" id="town_to" class="span10">
					<option value="<?=$town_to?>"><?=get::town($town_to)?></option>
					<?php foreach ($towns as $town):
					//for now, only Yaounde and buea
						if($town->id==3 OR $town->id==1){?>
							<option value="<?=$town->id?>"><?=$town->name?></option>
						<?php }  endforeach;?>
				</select>
			</div>
			<div class="span1">Date:</div>
			<div class="control-group span3">
				
					<div class="input-append date left">
						<input type="text" name = 'departure_date' class="span8 datepicker"  value="<?=date('d-m-Y')?>" id="dp1" >
						<span class="add-on"><i class="icon-th"></i></span>
					</div>
			</div>
			<select name="departure_time" id="inputType" style="display:none">
				<option value="any">Anytime</option>
			</select>
			<input type="hidden" name="route" id="route" value="">
			<center>
			<button class="btn btn-info" type="submit" id="submit-search" >&nbsp;Search</button>
			<a class="btn" id="cancel-modify-search" >Cancel</a>
			</center>
			</form>
		</div>			
		<script type="text/javascript">
			//Get the name of the towns to and fro via JS and send back to PHP so it's passed in the URL, for SEO
			
			$('#submit-search').click(function(){ 
				var route = $( "#town_from option:selected" ).text().toLowerCase()+"-"+$( "#town_to option:selected" ).text().toLowerCase();
				$('#route').val(route);				
			  });
			  </script>



		<div class="rule"><hr/></div>
		<div class="span12 hero-unit prof-unit"  style="margin:5px 0 0px 0;">			
					<?php
					
					if(count($results) == 0){
					?>
					Sorry, no available schedules for this route. You could opt for a <b>fixed schedule</b> though.  --->
					
					<?php
					}
					else{ ?>
					<table class="table schedules-table">	
						<tr>
							<th>Travel agency</th>
							<th>Bus number</th>
							<th>Bus type</th>
							<th>Departure</th>
							<th>Price</th>
							<th></th>
						</tr>
					<?php	foreach($results as $result)
						{ ?>
							<tr>
								<td><?=get::_parent($result->agency_id)->name?></td>
								<td><?=$result->bus_number?></td>
								<td><?=$result->bus_seats?> Seater</td>
								<td><?=date("g:i A", strtotime($result->departure_time))?></td>
								<td><?=number_format($result->ticket_price)?> frs</td>
								<td><a href="<?=url::site('home/schedule_details/'.$result->id)?>" class="btn btn-info " type="submit">Select bus</a></td>
								
							</tr>
						
						<?php
						} ?>
					</table>	
					 <?php	}
					
					?>
				</div>
		</div>
		<div class="span4 hero-unit prof-unit highlighted-unit" >
			
			<div class="heading">Fixed Schedules</div>
				<div class="rule"><hr/></div>
					From: <b><?=get::town($town_from)?></b> &nbsp; To: <b><?=get::town($town_to)?></b>
				
						<?php if(count($open_tickets)==0){echo "<br/>No fixed schedules for this route at the moment. Please try again later.";}else{?>
						<table class="table">
							<!--<tr>
								<td>Agency</td>
								<td>Tickets</td>
							</tr>-->
								<?php 
									$open_tickets = Open_Ticket_Model::get_all_for_route($town_from, $town_to);
									foreach ($open_tickets as $open_ticket)
												{?>		<tr>
														<td><?=get::_parent($open_ticket->agency_id)->name?></td>
														<form action="<?=url::site('home/initiate_open_ticket')?>" method="POST" >
														<input name="agency_id" value="<?=$open_ticket->agency_id?>" hidden/>
														<input name="open_ticket_name" value="<?=$open_ticket->open_ticket_name?>" hidden/>
														<input name="open_ticket_price" value="<?=$open_ticket->price?>" hidden/>
														<input name="town_from" value="<?=$town_from?>" hidden/>
														<input name="town_to" value="<?=$town_to?>" hidden/>
														
														
															<td><?=$open_ticket->open_ticket_name?></td> 
															<td><?=number_format($open_ticket->price)?> frs</td>
															<td><button type="submit" class="btn btn-small btn-warning no-border-radius">Get</button></td>
														
														
														</form>
														</tr>
												<?php } ?>	
												
											
							
								<!--<select name="agency_from" id="inputType">
									<option value="<?php //$agency->id?>"><?php //$agency->name?></option>
								</select>-->
							
						</table>
						<?php }?>
							<!--<input type="text"  name="client_name" placeholder="Enter name">
							<input type="text"  name="client_phone" placeholder="Enter phone">
							<input type="submit" class="btn btn-success" value="Buy" name="purchase_ticket" />-->
					
					
			
		</div>

		
	
		
    </div> <br/><br/><br/><br/><br/>
<script type="text/javascript"> 
		$(document).ready(function(){
		
			//show special drop town and price if checked, if not hide it
			$('#search-title').click(function(){
				$('#search-title').hide();
				$('#modify-search-body').show();
			});
			
			$('#cancel-modify-search').click(function(){
				$('#search-title').show();
				$('#modify-search-body').hide();
			});
			
			$('#ft').change(function(){
				if(this.checked){
					//if he selects free-ticket, disactivate special drop, and hide its fields
					//Show special drop reason
					$('#ft-reason').show(150);	
					$('#special_drop').prop('checked', false);
					$('#special-drop-price').hide(150);	
					$('#special-drop-town').hide(150);
				}else
				{
					$('#ft-reason').hide(150);
				}
			
			});
			
		});
		
	</script>
