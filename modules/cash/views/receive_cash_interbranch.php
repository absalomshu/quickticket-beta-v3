<?php defined('SYSPATH') or die('No direct script access'); ?> 
	

 
	<div class="row-fluid ">
	  	<div class="span12 hero-unit prof-unit" >
		
		<ul class="nav nav-tabs">
					<li > <a href="<?=url::site('cash/interbranch')?>"><i class="icon-arrow-left"></i> <?=Kohana::lang('backend.back_cash')?></a></li>
		</ul>
		
		<div class="tab-content">
			 
			
			
			<div class="tab-pane active" id="cash-to-bank">
				<div class="span8 offset2">
				
					<legend><?=Kohana::lang('backend.receive_cash_interbranch')?></legend>						
						<form action="<?=url::site('cash/receive_cash_interbranch/')?>" method="POST" autocomplete="off">
						
						<div>
							<div class="span4 text-right">From: </div>
								<div class="span8">
								<select name="branch_to_id" id="inputType">
											<?php foreach ($siblings as $sib):?>
											<option value="<?=$sib->id?>"><?=$sib->name?></option>
											<?php endforeach;?>
								</select>
								</div>
						</div>
						<div class="">
							<div class="span4 text-right"><?=Kohana::lang('backend.amount')?><span class="red"> *</span>: </div>
							<div class="span8">
								<input type="text" name="amount" class="IsAmount" required>
							</div>
						</div>
					
						<div class="">
							<div class="span4 text-right"><?=Kohana::lang('backend.date');?>:</div>
							<div class="span8">
								<div class="input-append date span8">
									<input type="text" name = 'date' class="span8 datepicker"  value="<?=date('d-m-Y')?>" id="dp1" >
									<span class="add-on"><i class="icon-calendar"></i></span>
								</div>	
							</div>
							
						</div>
						<br/>
						
						<div class="">
							<div class="span4 text-right"><?=Kohana::lang('backend.description')?>: <span class="red"> *</span></div>
							<div class="span8"><textarea placeholder="<?=Kohana::lang('backend.description')?>"  name="description" required ></textarea></div>
						</div>
						
						
						<div class="clear"></div>
						<div class="form-actions">
							<div class="span4"></div>
							<div class="span8"><button class="btn  btn-info" type="submit" name="cash_to_bank" ><?=Kohana::lang('backend.save');?></button>
							<a class="btn " href="<?=url::site('cash')?>"><?=Kohana::lang('backend.cancel');?></a>
							</div>
						</div>
					</form>	
				</div>	
					
			</div>
					
		</div>
		
			
		
			  
		</div>
		
		</div>
	  <div class="clear"></div>

<script type="text/javascript">
	$(document).ready(function(){
			
			
			$('.input-append.date').datepicker({
				format: "dd-mm-yyyy",
				todayBtn: "linked",
				autoclose: true,
				todayHighlight: true
			});
			
			
			
	})
	</script>
