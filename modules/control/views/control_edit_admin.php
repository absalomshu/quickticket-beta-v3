<?php defined('SYSPATH') or die('No direct script access'); ?> 
<!DOCTYPE html>

<?php //var_dump($logo);?>
<div class="row-fluid">		
	<div class="span12 hero-unit prof-unit">
		<ul class="nav nav-tabs">
			<li > <a href="<?=url::site('settings/administrators')?>"><i class="icon-arrow-left"></i> <?=Kohana::lang('backend.back_settings')?></a></li>
		</ul>
	
		<div class="span8 offset2">	
			<legend><?=Kohana::lang('backend.edit_admin')?> <?=$admin->username?></legend>
				<form method="POST" action="<?=url::site('settings/edit_admin/'.$admin->id)?>" enctype="multipart/form-data" class="form-horizontal">
				
				  
				   <div>
					<label class="control-label " for="inputEmail"><?=Kohana::lang('backend.username')?>:</label>
					<div class="controls">
								<input type="text" value="<?=$admin->username?>" disabled name="user_name"/>
					</div>
				  </div>
			
			<legend><?=Kohana::lang('backend.change_password')?></legend>
				  <div>
					<label class="control-label " for="inputEmail"><?=Kohana::lang('backend.new_password')?>:</label>
					<div class="controls">
								<input type="text"  name="new_password"/>
					</div>
					<div class="">
						<label class="control-label lab" for="inputType"></label>
							<div class="controls ">
								<button class="btn btn-info" type="submit" name="change_password"><?=Kohana::lang('backend.save')?></button>					
								<a class="btn" href="<?=url::site('settings/administrators')?>" ><?=Kohana::lang('backend.cancel')?></a>					
							</div>
				  </div>
				  </div>
			<legend><?=Kohana::lang('backend.module_access')?></legend>	  
				  <div class="span6 offset2  download">
				  <?php
						
						
						
						foreach($main_modules as $module)
						{ 
							$is_allowed_main_module = Admin_Module_Model::admin_is_allowed_main_module($admin->username,$module->id);
						?>
							<input type="hidden" name="all_modules[<?=$module->id?>]" value="0" />
							<label class="checkbox">
								<input type="checkbox"  id="module<?=$module->id?>" name="all_modules[<?=$module->id?>]" value="1" <?php if($is_allowed_main_module){echo 'checked = "checked"';} ?>/><?=Kohana::lang('backend.'.$module->Name)?>
							</label>
							<div class="clear"></div>
							
							<!--Div for parcels to hide or show parcel types. Only show for module id 4 which is parcels-->
							<?php if ($module->id == 4) {
								$is_allowed_manage_mails = Admin_Module_Model::admin_is_allowed_manage_mails($admin->username);
								$is_allowed_manage_baggage = Admin_Module_Model::admin_is_allowed_manage_baggage($admin->username);
								//var_dump($is_allowed_manage_mails);
								//var_dump($is_allowed_manage_baggage);exit;
							?>
								<div class="" id="parcel-types" style="margin-left:25px;">
								<!--hidden inputs  is a technique to ensure that unchecked fields pass a 0 to the POST-->
									<input type="hidden" name="mails" value="0" />
									<input type="checkbox"  id="mail-checkbox" name="mails" value="1" <?php if($is_allowed_manage_mails){echo 'checked = "checked"';} ?> />Mails
									
									<input type="hidden" name="baggage" value="0" />
									<input type="checkbox"  id="baggage-checkbox" name="baggage" value="1" <?php if($is_allowed_manage_baggage){echo 'checked = "checked"';} ?>/>Baggage
								</div>
							<?php }?>
						<?php 
						}
				  ?>
				  
					
					</div>		
					
					
					<legend><?=Kohana::lang('backend.dashboard_items')?></legend> 
				  <div class="span6 offset2  download">
				  <?php
						
						
						
						foreach($dashboard_items as $item)
						{ 
							$get_dashboard_item = Admin_Dashboard_Item_Model::get_dashboard_item($admin->username,$item->id);
							//echo $get_dashboard_item->IsAllowed;
						?>
							<input type="hidden" name="all_dashboard_items[<?=$item->id?>]" value="0" />
							<label class="checkbox"><input type="checkbox"  name="all_dashboard_items[<?=$item->id?>]" <?php if($item->id==3){echo 'id="restrict"';} elseif($item->id==2){echo 'id="all-current"';}?> value="1" <?php if($get_dashboard_item->IsAllowed){echo 'checked = "checked"';} ?>/><?=Kohana::lang('backend.'.$item->DescriptionCode)?></label>
								<?php 
									//Check when it's restricted town, show a select
									if($item->id == 3)
									{ ?>
									<div id="restricted-town" >
										&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;<select name="restricted_town" id="inputType">
														<!--If it's 0, show none, else show the town to which he's been restricted-->
														<option value="<?=$get_dashboard_item->RestrictedTownID?>"><?=get::town($get_dashboard_item->RestrictedTownID)?></option>
														<?php foreach ($towns as $town):?>
														<option value="<?=$town->id?>"><?=$town->name?></option>
														<?php endforeach;?>
										</select><br/><br/>
									</div>
						<?php }
						}
				  ?>
				  
					
					</div>			
				  
				  <div class="">
						<label class="control-label lab" for="inputType"></label>
							<div class="controls">
								<button class="btn btn-info" type="submit" name="change_privileges"><?=Kohana::lang('backend.save')?></button>					
								<a class="btn" href="<?=url::site('settings/administrators')?>" ><?=Kohana::lang('backend.cancel')?></a>					
							</div>
				  </div>	
				</form>			
		</div>
	</div>
</div>
		
		<script type="text/javascript"> 
		$(document).ready(function(){
			
			//show special drop town and price if checked, if not hide it
			$('#restrict').change(function(){
				if(this.checked){
					$('#restricted-town').show(150);
					//if he selects restrict to town, uncheck all schedules.
					$('#all-current').prop('checked', false);
					
					}else{
					//if he deselects all restrict, check all-current and hide restricted town.
					$('#restricted-town').hide(150);
					$('#all-current').prop('checked', true);					
					}
			
			});
			
			$('#all-current').change(function(){
				if(this.checked){
					//if he selects all-current schedules, disable restrict and hide restricted-town
					$('#restricted-town').hide(150);
					$('#restrict').prop('checked', false);
					
				}else
				{
					//if he deselects all current schedules, check restrict and show restricted town.
					$('#restrict').prop('checked', true);	
					$('#restricted-town').show(150);
				}
			
			});
			
			//the div with module id 4 is parcels
			$('#module4').change(function(){
			//if he selects parcels, show him the 2 types of parcels
				if(this.checked){
					$('#mail-checkbox').prop('disabled', false);
					$('#baggage-checkbox').prop('disabled', false);
					
					}else{
					//if he deselects parcels, deselect the 2 parcel types and disable
				
					$('#mail-checkbox').prop('disabled', true);
					$('#baggage-checkbox').prop('disabled', true);
					$('#mail-checkbox').prop('checked', false);
					$('#baggage-checkbox').prop('checked', false);
					//$('#all-current').prop('checked', true);					
					}
			
			});
			
			//on page load, if parcels is not selected, disable the 2 parcel types
			if(!$("#module4").is(':checked'))
			{ 
				$('#mail-checkbox').prop('disabled', true);
				$('#baggage-checkbox').prop('disabled', true);
				//$('#parcel-types').prop('disabled', true);
			}
			
		});
		
	</script>
