<?php defined('SYSPATH') or die('No direct script access'); ?> 
	

 
	<div class="row-fluid ">
	  	<div class="span12 hero-unit prof-unit" >
		
		<ul class="nav nav-tabs" id="myTab">
					
					<li ><a href="<?=url::site('cash/collections')?>"><i class="icon-align-justify"></i> <?=Kohana::lang('backend.collection')?></a></li>
					<li class="active"><a href="#income"><i class="icon-align-justify"></i> <?=Kohana::lang('backend.other_income')?></a></li>
					<li><a href="<?=url::site('cash/cash_to_bank')?>"><i class="icon-align-justify"></i> <?=Kohana::lang('backend.cash_to_bank')?></a></li>
					<li><a href="<?=url::site('cash/balances')?>"><i class="icon-align-justify"></i> <?=Kohana::lang('backend.balances')?></a></li>
		</ul>
		
		<div class="tab-content">
			 
			
			
			
			<div class="tab-pane active" id="income">
			<div class=""">
			<legend><?=Kohana::lang('backend.other_income')?><div class='view'><a href="<?=url::site('cash/add_other_income')?>">+ <?=Kohana::lang('backend.add_other_income')?></a></div></legend>						

				<table class="table table-striped table-hover table-condensed">
					<tr>
						<th>Date</th>
						<th><i class="icon-bus" title="VIP Bus"></i><?=Kohana::lang('backend.bus_no')?></th>
						<th>Description</th>
						<th><?=Kohana::lang('backend.amount')?></th>
						<!--<th>Actions</th>-->
						
					</tr>
					<?php foreach ($other_income_per_page as $income):?>
						<tr>	
							
							<td><?=date("d-m-Y",strtotime($income->date_incurred))?></td>
							<td> <?=$income->bus_number?></td>
							<td> <?=$income->description?></td>
						
							<td class='text-right'>
								<?=number_format($income->amount)?>
							</td>
						
						
						</tr>
						<?php endforeach; ?>
				</table>	
				</div>	
			</div>
			
			
					
		</div>
		
			
		
			  
		</div>
		
		</div>
	  <div class="clear"></div>

<script type="text/javascript">
	$(document).ready(function(){
			$('#timepicker').timepicker({
				minuteStep:30,
				//defaultTime: '8:00 PM'
			});
			
			$('.input-append.date').datepicker({
				format: "dd-mm-yyyy",
				todayBtn: "linked",
				autoclose: true,
				todayHighlight: true
			});
			
			
			
	})
	</script>