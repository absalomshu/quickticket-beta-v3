<?php defined('SYSPATH') or die('No direct script access');

/**
 * Description:  
 
 *
 * @package		 quickticket
 * @subpackage 	 Helper
 * @author 		 Absalom Shu
 * @copyright    (c) 2013
 * @license 	 quickticket
 */
 
class Get_Core
{	
	
	public function all_towns(){
		$towns=ORM::factory('town')->orderby('name','asc')->find_all();
		return $towns;
	}
	
	public function agency_name($agency_id){
		$agency=ORM::factory('agency')->where('id',$agency_id)->find();
		return $agency->name;
	}
	
	public function agency_town_name($agency_id)
	{
		$agency = ORM::factory('agency')->where('id',$agency_id)->find();
		$town_id =$agency->town_id;
		$town = ORM::factory('town')->where('id',$town_id)->find();
		return $town->name;
	}
	
	public function agency_town($agency_id)
	{
		$agency = ORM::factory('agency')->where('id',$agency_id)->find();
		$town_id =$agency->town_id;
		return $town_id;
	}
	
	
	public function agency($agency_id){
		$agency=ORM::factory('agency')->where('id',$agency_id)->find();
		return $agency;
	}

	public function group_role($admin_group_id){
		$admin_group = ORM::factory('admin_group')->find($admin_group_id)->role;
		return $admin_group;
	}
	
	//Admin group id 1 is not useful for the moment.
	public function all_admingroups(){
		$groups=ORM::factory('admin_group')->where(array('id !='=>1))->find_all();
		return $groups;
	}
	
	public function all_agencies(){
		$agencies=ORM::factory('agency')->find_all();
		return $agencies;
	}
	
	public function all_parents(){
		$parents=ORM::factory('parent')->find_all();
		return $parents;
	}
	
	public function all_agency_schedules($agency_id){
		$schedules=ORM::factory('schedule')->where('agency_id',$agency_id)->where('deleted','0')->find_all();
		return $schedules;
	}
	public function all_parent_schedules($parent_id){
			
		//active records used coz ORM seems trouble some 
		$db = new Database();
		$schedule_agency =$db->from('schedules')->join('agencies','agencies.id','schedules.agency_id')->where('agencies.parent_id',$parent_id)->where('deleted','0')->getwhere();
		return $schedule_agency;
	}
	
	public function all_parent_current_schedules($parent_id){
			
		//active records used coz ORM seems trouble some 
		$db = new Database();
		$schedule_agency =$db->from('schedules')->join('agencies','agencies.id','schedules.agency_id')->where('status','current')->where('deleted','0')->where('agencies.parent_id',$parent_id)->get();
		//$schedule_agency =ORM::FACTORY('agency')->join('schedules','schedules.agency_id','agencies.id','RIGHT')->where('agencies.parent_id',$parent_id)->where('status','current')->find();
		//print_r($schedule_agency);exit;
		return $schedule_agency;
	}
	
	public function all_parent_departed_schedules($parent_id){
			
		//active records used coz ORM seems trouble some 
		$db = new Database();
		$schedule_agency =$db->select('schedules.id AS schedID','schedules.bus_number','schedules.from','schedules.to','schedules.id','schedules.checked_out_time')->from('schedules')->join('agencies','agencies.id','schedules.agency_id')->where('status','departed')->where('agencies.parent_id',$parent_id)->get();
		return $schedule_agency;
	}
	
	public function all_agency_current_schedules($agency_id){
		$schedules=ORM::factory('schedule')->where('agency_id',$agency_id)->where('status','current')->where('deleted','0')->orderby('departure_date','desc')->find_all();
		return $schedules;
	}
	public function ten_agency_current_schedules($agency_id){
		$schedules = ORM::factory('schedule')->where('agency_id',$agency_id)->where('status','current')->where('deleted','0')->orderby('schedules.departure_date','desc')->orderby('schedules.departure_time','desc')->limit(10)->find_all();
		return $schedules;
	}
	
	public function all_agency_departed_schedules($agency_id){
		$schedules=ORM::factory('schedule')->where('agency_id',$agency_id)->where('status','departed')->orderby('departure_date','desc')->find_all();
		return $schedules;
	}
	public function ten_agency_departed_schedules($agency_id){
		$schedules=ORM::factory('schedule')->where('agency_id',$agency_id)->where('status','departed')->orderby('id','desc')->limit(10)->find_all();
		return $schedules;
	}
	
	
	public function all_parent_schedules_by_date($parent_id,$date){
		$schedules=ORM::factory('schedule')->join('agencies','agencies.id','schedules.agency_id')->where('deleted','0')->where('departure_date',$date)->where('parent_id',$parent_id)->find_all();
		return $schedules;
	}
	public function all_parent_current_schedules_by_date($parent_id,$date){
		$schedules=ORM::factory('schedule')->join('agencies','agencies.id','schedules.agency_id')->where('parent_id',$parent_id)->where('deleted','0')->where('departure_date',$date)->where('status','current')->find_all();
		return $schedules;
	}public function all_parent_departed_schedules_by_date($parent_id,$date){
		$schedules=ORM::factory('schedule')->join('agencies','agencies.id','schedules.agency_id')->where('agencies.parent_id',$parent_id)->where('departure_date',$date)->where('status','departed')->find_all();
		return $schedules;
	}
	public function all_agency_departed_schedules_by_date($agency_id,$date){
		$schedules=ORM::factory('schedule')->where('agency_id',$agency_id)->where('status','departed')->where('departure_date',$date)->orderby('id','desc')->find_all();
		return $schedules;
	}
	public function all_agency_current_schedules_by_date($agency_id,$date){
		$schedules=ORM::factory('schedule')->where('agency_id',$agency_id)->where('status','current')->where('deleted','0')->where('departure_date',$date)->orderby('id','desc')->find_all();
		return $schedules;
	}
	
	public function schedule($id)
	{
		$schedule=ORM::factory('schedule')->where('id',$id)->where('deleted','0')->find();
		return $schedule;
	}
	
	public function town($id){
		$town=ORM::factory('town')->where('id',$id)->find();
		return $town->name;
	}
	
	//NB this takes the id of the branch checks the agency table for the parent's id, then goes to the parent table and picks the parent name
	public function _parent($agency_id){
		$agency = ORM::factory('agency')->where('id',$agency_id)->find();
		$parent_id = $agency->parent_id;
		$parent = ORM::factory('parent')->where('id',$parent_id)->find();
		return $parent;
	}
	
	public function _parent_name($agency_id){
		$agency = ORM::factory('agency')->where('id',$agency_id)->find();
		$parent_id = $agency->parent_id;
		$parent = ORM::factory('parent')->where('id',$parent_id)->find();
		return $parent->name;
	}
	
	public function admins_town($admin_agency_id){
	
		$admin_agency = ORM::factory('agency')->where('id',$admin_agency_id)->find();
		$admin_town_id = $admin_agency->town_id;
		
		$admin_town = ORM::factory('town')->where('id',$admin_town_id)->find();
		return $admin_town;
	
	}
	public function agency_and_town($agency_id){
	
		$agency = ORM::factory('agency')->where('id',$agency_id)->find();
		$parent_id = $agency->parent_id;
		$parent = ORM::factory('parent')->where('id',$parent_id)->find();
		
		$town_id = $agency->town_id;
		$town = ORM::factory('town')->where('id',$town_id)->find();
		
		return $parent->name ." ". $town->name;
	}
	public function client_reservations($id){
		$client_res = ORM::factory('reservation_request')->where('schedule_id',$id)->find();
		return $client_res;
	}
	public function all_agency_parcels($agency_id){
		$parcels=ORM::factory('parcel')->where('agency_id',$agency_id)->orderby('registered_date','DESC')->find_all();
		return $parcels;
	}
	
	/*Gets from the same db. This works for test purposes only
	public function all_agency_incoming_parcels($agency_id)
	{
		$parcels=ORM::factory('parcel')->where('to',$agency_id)->orderby('sent_date','DESC')->find_all();
		return $parcels;
	} 
	public function ten_agency_incoming_parcels($agency_id)
	{
		$parcels=ORM::factory('parcel')->where('to',$agency_id)->orderby('sent_date','DESC')->limit(10)->find_all();
		return $parcels;
	}
	*/
	
	public function all_agency_incoming_parcels($agency_id)
	{
		$parcels=ORM::factory('incoming_parcel')->where('to',$agency_id)->orderby('sent_date','DESC')->find_all();
		return $parcels;
	}
	
	public function ten_agency_incoming_parcels($agency_id)
	{
		$parcels=ORM::factory('incoming_parcel')->where('to',$agency_id)->orderby('sent_date','DESC')->limit(10)->find_all();
		return $parcels;
	}
	
	public function get_all_agency_outgoing_parcels($agency_id){
		$parcels=ORM::factory('parcel')->where('from',$agency_id)->orderby('registered_date','DESC')->find_all();
		return $parcels;
	}
	
	
	
	
	//Raw SQL used here as Kohana doesn't seem to have BETWEEN
	public function agency_parcels_by_period($agency_id,$start_date,$end_date)
	{
		$db = new Database();
		$parcels = $db->query("SELECT * FROM parcels WHERE agency_id= ".$agency_id." AND registered_date BETWEEN CAST('$start_date' AS DATE) AND CAST('$end_date' AS DATE) ORDER BY registered_date ASC");
		return $parcels;
	}
	
	public function parent_parcels_by_period($parent_id,$start_date,$end_date)
	{
		$db = new Database();
		$parcels = $db->query("SELECT * FROM parcels JOIN agencies ON agencies.id = parcels.agency_id WHERE parent_id= ".$parent_id." AND registered_date BETWEEN CAST('$start_date' AS DATE) AND CAST('$end_date' AS DATE) ORDER BY registered_date ASC");
		return $parcels;
	}
	
	
	
	public function parent_expenses_by_period($parent_id,$start_date,$end_date)
	{
		$db = new Database();
		$expenses = $db->query("SELECT * FROM expenses JOIN agencies ON agencies.id = expenses.agency_id WHERE parent_id= ".$parent_id." AND deleted = '0' AND date_incurred BETWEEN CAST('$start_date' AS DATE) AND CAST('$end_date' AS DATE) ORDER BY date_incurred ASC");
		return $expenses;
	}
	
	
	
	public function parent_schedules_by_period($parent_id,$start_date,$end_date)
	{
		$db = new Database();
		$schedules = $db->query("SELECT * FROM schedules JOIN agencies ON agencies.id = schedules.agency_id WHERE parent_id= ".$parent_id." AND deleted = '0' AND departure_date BETWEEN CAST('$start_date' AS DATE) AND CAST('$end_date' AS DATE) ORDER BY departure_date ASC");
		return $schedules;
	}
	
	//All agency activity for a period: expenses, ticket_sales & parcels
	public function agency_activity_by_period($agency_id,$start_date,$end_date){
		$db = new Database();
		$activity = $db->query("SELECT * FROM parcels WHERE agency_id= ".$agency_id." AND sent_date BETWEEN CAST('$start_date' AS DATE) AND CAST('$end_date' AS DATE)
							   UNION SELECT * FROM expenses WHERE agency_id= ".$agency_id." AND date_incurred BETWEEN CAST('$start_date' AS DATE) AND CAST('$end_date' AS DATE)
							   UNION SELECT * FROM schedules WHERE agency_id= ".$agency_id." AND departure_date BETWEEN CAST('$start_date' AS DATE) AND CAST('$end_date' AS DATE) AND status = 'departed' ORDER BY departure_date ASC
					");
		return $activity;
	}
	
	public function all_parent_parcels_by_date($parent_id,$date){
		//$parcels=ORM::factory('parcel')->where('from',$agency_id)->where('sent_date',$date)->find_all();
		$parcels=ORM::factory('parcel')->join('agencies','agencies.id','parcels.agency_id')->where('parent_id',$parent_id)->where('registered_date',$date)->find_all();
		return $parcels;
	}
	
	
	public function agency_siblings($agency_id,$parent){
		//get all branches of the mother agency other than the current one
		$parcels=ORM::factory('agency')->where('id !=',$agency_id)->where('parent_id',$parent)->find_all();
		return $parcels;
	}
	public function all_children($parent_id){
		$children=ORM::factory('agency')->where('parent_id',$parent_id)->find_all();
		return $children;
	}
	
	//Get any parcel
	public function parcel($parcel_id)
	{
		$parcel = ORM::factory('parcel')->find($parcel_id);
		return $parcel;
	}
	
	//Get specifically an incoming parcel
	public function incoming_parcel($parcel_id)
	{
		$parcel = ORM::factory('incoming_parcel')->find($parcel_id);
		return $parcel;
	} 

	public function schedule_status($schedule_id){
		//check if a schedule is current or departed
		$schedule = ORM::factory('schedule')->where('id',$schedule_id)->find();
		return $schedule->status;
	}
		
	public function expense($expense_id){
		$parcel = ORM::factory('expense')->where('deleted','0')->find($expense_id);
		return $parcel;
	}
	
	public function expenses_for_bus($bus_number){
	
		$bus_expenses = ORM::factory('expense')->where('bus_number',$bus_number)->where('deleted','0')->find_all();		
		return $bus_expenses;
	}
	
	//Differs from expenses_for_bus as this only shows the expenses in one day
	public function expenses_for_bus_and_date($agency_id,$bus_number,$date)
	{
	
		$sched_expenses = ORM::factory('expense')->where('agency_id',$agency_id)->where('bus_number',$bus_number)->where('deleted','0')->where('date_incurred',$date)->find_all();		
		return $sched_expenses;
	}
	
	public function expenses_for_schedule($agency_id,$schedule_id)
	{
	
		$sched_expenses = ORM::factory('expense')->where('agency_id',$agency_id)->where('schedule_id',$schedule_id)->where('deleted','0')->find_all();		
		return $sched_expenses;
	}
	
	public function get_expenses_total_by_schedule($agency_id,$schedule_id)
	{
			$schedule_expenses=get::expenses_for_schedule($agency_id,$schedule_id);
			$schedule_expenses_total = 0;
			foreach($schedule_expenses as $expense)
			{
				$schedule_expenses_total += $expense->amount;
			}
			
			return $schedule_expenses_total;
	}
	
	
	public function parent_expenses_by_date($parent_id,$date)
	{
		$expenses = ORM::FACTORY('expense')->join('agencies','agencies.id','expenses.agency_id')->where('parent_id',$parent_id)->where('deleted','0')->where('date_incurred',$date)->find_all();		
		return $expenses;
	}
	
	public function company($id){
		$company = ORM::factory('parent')->where('id',$id)->find();
		return $company;
	}
	
	public function company_logo($company_id){
		//first get the company name, which would be the name of the photo
		$company = ORM::factory('parent')->where('id',$company_id)->find();
		
		$logo = "images/ticket/agency-logos/" .$company->name .".jpg";
		$logo = strtolower(str_replace(" ",'', $logo));
		
		//if there's no logo, return the default
		if(file_exists($logo)){
			return $logo;
		}else{ 
			return "images/ticket/agency-logos/sample_logo.jpg";
		}
	}
	public function agency_logo($agency_id)
	{
		//first get the company name, which would be the name of the photo
		$agency = ORM::factory('agency')->where('id',$agency_id)->find();
		
		$logo = "images/ticket/agency-logos/" .$agency->name .".jpg";
		$logo = strtolower(str_replace(" ",'', $logo));
		
		//if there's no logo, return the default
		if(file_exists($logo)){
			return $logo;
		}else{ 
			return "images/ticket/agency-logos/sample_logo.jpg";
		}
	}
	
	public function bus_photo($bus_number)
	{
		//first get the company name, which would be the name of the photo
		$agency = ORM::factory('bus')->where('bus_number',$bus_number)->find();
		
		$photo = "images/buses/" .$bus_number .".jpg";
		
		//if there's no logo, return the default
		if(file_exists($photo)){
			return $photo;
		}else{ 
			return "images/ticket/agency-logos/sample_logo.jpg";
		}
	}
	
	
	public function agency_ticket_background($agency_id)
	{
		//first get the company name, which would be the name of the photo
		$agency = ORM::factory('agency')->where('id',$agency_id)->find();
		
		$logo = "images/ticket/" ."ticket.".$agency->name .".jpg";
		$logo = strtolower(str_replace(" ",'', $logo));
		
		//if there's no logo, return the default
		if(file_exists($logo)){
			return $logo;
		}else{ 
			return "images/ticket/agency-logos/sample_logo.jpg";
		}
	}
	
	
	public function all_company_buses($company_id){
		$buses=ORM::factory('bus')->where('agency_id',$parent_id)->where('deleted','0')->find_all();
		return $buses;
	}
	

	//go through all schedules done by the agency/parent and get the bus numbers
	//Params: agency_id, parent_id, admin_group_id
	//based on the admin's group, we'll know if to give results for one agency or the whole parent/company
	//ie if it's a branch manager, he can only get for his branch/agency.
	//ie if it's a general manager, he gets for the whole parent/company
	//If it's at the branch level but he needs access to the whole agencies' buses, simply pass 4 as admin_group_id
	
	public function all_buses($agency_id, $parent_id, $admin_group_id=3)
	{	
		/*$db=new Database();
		if($admin_group_id ==4){
			
			$all_buses = $db->from('buses')->join('agencies','agencies.id','buses.agency_id')->where('agencies.parent_id',$parent_id)->where('buses.deleted',0)->get();
			var_dump($all_buses);exit;
		}else{  */
			//$all_buses = $db->from('buses')->where('agency_id',$agency_id)->where('deleted','0')->getwhere();
			$all_buses = ORM::factory('bus')->where('agency_id',$agency_id)->where('deleted','0')->where('is_available','1')->find_all();
			
		//}
		return $all_buses;
	}
	
	
	public function all_expenses($agency_id, $parent_id, $admin_group_id=3)
	{
		if($admin_group_id ==4){
			$expenses=ORM::factory('expense')->join('agencies','agencies.id','expenses.agency_id')->where('parent_id',$parent_id)->where('deleted','0')->orderby('date_incurred','DESC')->find_all();
		}else{
			$expenses=ORM::factory('expense')->where('agency_id',$agency_id)->where('Deleted','0')->orderby('date_incurred','DESC')->find_all();
		}
		return $expenses;
	}
	
	public function bus($bus_number){
		$bus=ORM::factory('bus')->where('bus_number',$bus_number)->find();
		return $bus;
	}
	
	public function vip_bus($bus_number){
		$bus=ORM::factory('bus')->where('bus_number',$bus_number)->find();
		return $bus->vip;
	}
	
	public function agency_admins($agency_id){
		$admins=ORM::factory('admin')->where('agency_id',$agency_id)->where('deleted','0')->find_all();
		return $admins;
	}
	
	public function unpushed_transactions(){
		//check if a transaction has been pushed online or not
		//order by changetime ASC so that later changes take precedence over earlier ones.
		$unpushed = ORM::factory('transaction')->where('online','0')->orderby('change_time','ASC')->find_all();	
		//$unpushed = ORM::factory('schedule')->find_all();	
		return $unpushed;
	}
	
	public function unpushed_transactions_parcels(){
		//check if a parcel transaction has been pushed online or not
		$unpushed_parcels = ORM::factory('transactions_parcel')->where('online','0')->orderby('change_time','DESC')->find_all();	
		return $unpushed_parcels;
	}
	public function admin($admin_id){
		$admin = ORM::factory('admin')->where('id',$admin_id)->find();	
		return $admin;
	}
	
	
	
	//Income table only handles other income. Overall income includes parcels, schedules, other_income, and remote_tickets
	public function agency_overall_income_by_date($agency_id,$date){	
		
		//$agency_id = $this->agency_id;
		$date = date("Y-m-d",strtotime($date));
		//$end_date = date("Y-m-d",strtotime($end_date));
		
		$parcel_total = Parcel_Model::get_parcel_income_by_date($agency_id,$date);
		$schedule_income_total=Schedule_Model::get_departed_schedule_income_by_date($agency_id,$date);
		$period_other_income=Income_Model::get_other_income_by_date($agency_id,$date);
		//Remote tickets don't feature under schedules, hence they are not included in this report. They need to be.
		$remote_outgoing_tickets=Ticket_Detail_Model::get_agency_outgoing_remote_tickets_by_period_all_admins($agency_id,$date,$date);
	
		$period_expenses=Expense_Model::get_expenses_by_date($agency_id,$date);	
		
		
		
		//Income from schedules. DEPARTED BUSES ONLY
		
		//Other income
		$other_income_total = 0;
		foreach($period_other_income as $income)
		{
			$other_income_total += $income->amount;
		}
		
		//Remote outgoing tickets
		$remote_outgoing_ticket_income_total =0;
		foreach ($remote_outgoing_tickets as $ticket){
			$remote_outgoing_ticket_income_total += $ticket->Price;
		}
		//Expenses
		$expenses_total = 0;
		foreach($period_expenses as $exp )
		{
			$expenses_total += $exp->amount;
		}
	
	$overall_income = $schedule_income_total + $parcel_total + $other_income_total + $remote_outgoing_ticket_income_total;
	return $overall_income;

	}
	
	public function agency_overall_expenditure_by_date($agency_id,$date)
	{	
		

		$date = date("Y-m-d",strtotime($date));
		
		$period_expenses=Expense_Model::get_expenses_by_date($agency_id,$date);	
		
		//Expenses
		$expenses_total = 0;
		foreach($period_expenses as $exp )
		{
			$expenses_total += $exp->amount;
		}
	

	return $expenses_total;

	}
	
	public function build_line_chart_data($agency_id, $start_date, $end_date, $filename){
		//get all days between start date and end date
					
		//intitialize the array with the headers for the chart
		//  $chart_array=array('0'=>"date \t income \t expenditure \t net");
		  $chart_array=array();
		  $start=strtotime($start_date);
		  $end=strtotime($end_date);
		  while($start <= $end)
		  {
			//$dates[]=date("Y-m-d",$start);
			$chart_array[date("Y-m-d",$start)]=0;
			$start=strtotime("+1 day",$start);
		  }
			// build the tsv for the chart
			$chart_data ="date\tincome\texpenditure\t net\n";
			
		  //for each date, insert the date, then total income , total expenditure, and net into the array
			foreach($chart_array as $date => $income_total){
				$income_total = get::agency_overall_income_by_date($agency_id,$date);
				$expenses_total=get::agency_overall_expenditure_by_date($agency_id,$date);	
				$net = $income_total-$expenses_total;
				
				$chart_array[$date] = array('total_income'=>$income_total, "total_expenses"=>$expenses_total,"net"=> $net);
				
				$chart_data .= date("Ymd",strtotime($date))."\t". $income_total. "\t".$expenses_total."\t". $net."\n";
			} 
			
			$filepath = "C://wamp/www/quickticket/files/graph-data/".$filename;
			//var_dump($report_name);exit;
			//$filepath = "C://wamp/www/quickticket/downloads/report-".$current_time.".csv";
			$handle = fopen($filepath, "w") or die("Unable to open file!");
			fwrite($handle, $chart_data);
			fclose($handle);
			
			//return an array with key: date, values income total, expenses total and net
			return $chart_array;
				
						
						
	}
}
