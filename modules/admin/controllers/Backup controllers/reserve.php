<?php defined('SYSPATH') or die('No direct script access');

//Almost all functions will take as parameter, the id of the agency we are dealing with
//in order not to interfere with another's stuff.

class Reserve_Controller{
		
		public function reservation_details($schedule_id){	
		//$admin = Authlite::instance()->get_admin();
		$client_reservations=get::client_reservations($schedule_id);
		$client_reservations=json_decode($client_reservations->reserved_seats,true);
		if (!empty ($client_reservations)){
				?>
				<form action="<?=url::site('admin/complete_schedule/'.$schedule_id)?>" method='POST'>
				<table class='table table-bordered'>
										<tr>
												<th><?=Kohana::lang('backend.seat_no')?></th>
												<th><?=Kohana::lang('backend.occupant')?></th>
												<th><?=Kohana::lang('backend.phone')?></th>
												<th>Actions</th>
										</tr> 
										
				<?php						
					foreach($client_reservations as $seat => $details){
					$serial = serialize($client_reservations);
											?>
													
							<tr>
								<td><?=$seat?></td>
								<td><?=$details['name']?></td>
								<td><?=$details['phone']?></td>
								
								<td><input type='hidden' value='<?php echo htmlentities($serial)?>' name='arr'/>
									<!--simple hidden field to notify that this is from the reservation request form (client)-->
									<input type='hidden' value='1' name='request_from_frontend'/>
									<button type='submit' id='accept'name='res_to_occ' value="<?=$seat?>" title='Confirm'></button> | 
									<button type='submit' id='reject' name='reject_reserve' value="<?=$seat?>" title='Delete'></button>
								</td>
							</tr>	
						<?php					
		
					}
					}
					else{ echo Kohana::lang('backend.no_reservation');}
		}
		
		/* live update of the the number of reservation requests		*/
		public function reservation_count($schedule_id){
			$client_reservations=get::client_reservations($schedule_id);
			$res_count = count(json_decode($client_reservations->reserved_seats,true));
			if($res_count != 0){ echo "<span class='notification'>".$res_count."</span>";}
		}
		
		}