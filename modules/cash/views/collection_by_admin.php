<?php defined('SYSPATH') or die('No direct script access'); ?>
	
    <div class="container">
		
		
		<div class="row-fluid  span12 hero-unit prof-unit reload" style="margin:5px 0 0px 0;">	
		
			<div class="span12" >
			<div class="no-print">
			<?php 
			//instantiate variable to total the prices in the loop below
				$parcel_total = 0;
				foreach ($parcels as $parcel)
				{
				$parcel_total += $parcel->price;
				}
			?>
			<ul class="nav nav-tabs">
					<li > <a href="<?=url::site('cash/collections')?>"><i class="icon-arrow-left"></i> <?=Kohana::lang('backend.back_cash')?></a></li>
				</ul>
			</div>	
			
			<div class="span8 offset2 print-area" >
			
				
				<legend><?=Kohana::lang('backend.collections_for')?> <b><?=strtoupper($admin->username)?></b><button class="view" onClick="window.print()"><i class="icon-print"></i> <?=Kohana::lang('backend.print')?></button></legend>
				<!--<table class="table table-bordered table-condensed">
					<tr><th><?=Kohana::lang('backend.ticket_sales')?></th></tr>
					<?php 
					foreach ($period_schedules as $sched)
					{?>
					<tr>
						<td class="span2"><?=date("d-m-Y",strtotime($sched->CreatedOn))?></td>
						<td><?=$sched->ClientName?> <?php if($sched->FreeTicket){echo " <i>Kohana::lang('backend.free')</i>";}?></td>
						<td></td>
						<td class="text-right"><?=number_format($sched->Price)?></td>
					</tr>
					<?php }?>
					<tr>		
						<td class="span2"></td>
						<th ><?=Kohana::lang('backend.total_ticket_sales')?></th>
						<td class="span2"></td>
						<th class="span2 text-right"><?=number_format($total)?> FCFA</th>
					</tr>
				</table>
				<table class="table table-bordered table-condensed">	
					<tr><th><?=Kohana::lang('backend.parcels')?></th></tr>
					<?php 
					foreach ($period_parcels as $parcel)
					{?>
					<tr>
						<td class="span2"><?=date("d-m-Y",strtotime($parcel->sent_date))?></td>
						<td><?=$parcel->description?></td>
						<td></td>
						<td class="text-right"><?=number_format($parcel->price)?></td>
					</tr>
					<?php }?>
					
					
					<tr>		
						<td class="span2"></td>
						<th><?=Kohana::lang('backend.total_from_parcels')?></th>
						<td class="span2"></td>
						<th class="span2 text-right"><?=number_format($parcel_total)?> FCFA</th>
					</tr>
				</table>
				<table class="table table-bordered table-condensed">	
					<tr><th><?=Kohana::lang('backend.other_income')?></th></tr>
					<?php 
					$income_total = 0;
					foreach ($period_income as $income)
					{?>
					<tr>
						<td class="span2"><?=date("d-m-Y",strtotime($income->date_incurred))?></td>
						<td><?=Kohana::lang("backend.$income->purpose")?></td>
						<td></td>
						<td class="text-right"><?=number_format($income->amount)?></td>
					</tr>
					<?php $income_total += $income->amount; }?>
					
					
					<tr>		
						<td class="span2"></td>
						<th><?=Kohana::lang('backend.total_income')?></th>
						<td class="span2"></td>
						<th class="span2 text-right"><?=number_format($income_total)?> FCFA</th>
					</tr>
				</table>
				<table class="table table-bordered table-condensed">
					<tr><th><?=Kohana::lang('backend.expenses')?></th></tr>
					<?php
					foreach ($period_expenses as $exp)
					{?>
					<tr>
						<td class="span2"><?=date("d-m-Y",strtotime($exp->date_incurred))?></td>
						<td><?=$exp->purpose?></td>
						<td class="text-right" ><?=number_format($exp->amount)?></td>
						<td></td>
					</tr>
					<?php }?>
					<tr>		
						<td></td>
						<th><?=Kohana::lang('backend.total_expenditure')?></th>
						<th class="span2 text-right"><?=number_format($total_expenditure)?> FCFA</th>
						<td class="span2"></td>
					</tr>
				</table>	-->
					
				<form action="<?=url::site('cash/register_collection/')?>" method="POST" >
					
						<input type="hidden" name="admin_username" value="<?=$admin->username?>" />
						<input type="hidden" name="admin_available_balance" value="<?=$admin->available_balance?>" />
						
						
						<div class="" >
							<div class="span4 text-right"><?=Kohana::lang('backend.todays_net_income')?>: </div>
							<div class=" span8">
								<?=number_format($total + $parcel_total +$income_total - $total_expenditure)?>
							</div>
						</div>
						<div class="" >
							<div class="span4 text-right"><?=Kohana::lang('backend.available_balance')?>: </div>
							<div class=" span8">
								<?=number_format($admin->available_balance)?>
							</div>
						</div>
						<div class="" >
							<div class="span4 text-right"><?=Kohana::lang('backend.add_collection')?>: </div>
							<div class=" span8">
								<input type="text" name="amount" class="IsAmount" required/>
								<br/><button type="submit" name="add_collection"  class="btn btn-success"><?=Kohana::lang('backend.save')?></button>
							
							</div>
						</div>
						
					</form>
				
				
					
				<legend><?=Kohana::lang('backend.recent_collections')?></legend>
				<table class="table table-bordered">	
					<tr>		
						<td>Date</th>
						<td><?=Kohana::lang('backend.name')?></th>
					
						<td><?=Kohana::lang('backend.previous_balance')?></th>
						<td><?=Kohana::lang('backend.amount_collected')?></th>
						<td><?=Kohana::lang('backend.collected_by')?></th>
					</tr>
					<?php foreach($collections as $collection) {?>
					<tr>		
						<td><?=$collection->CreatedOn?></td>
						<td><?=$collection->admin_id?></td>
						<td><?=$collection->previous_balance?></td>
						<td><?=$collection->amount?></td>
						<td><?=$collection->CreatedBy?></td>
					
					</tr>
					<?php } ?>
					
				</table>
				<div class="pagination"><?php echo @$this->pagination;?>	</div>	
			</div>
			
		</div>
			
		
		
		
		
		
		
		</div>
    </div> 
