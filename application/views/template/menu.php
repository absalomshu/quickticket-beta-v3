<?php defined('SYSPATH') or die('No direct script access'); 


	if ($this->uri->segment(2) != 'login'  AND $this->uri->segment(2) != 'add_admin' AND $this->uri->segment(1) != 'manage'){
	$admin=Authlite::instance()->get_admin(); 

	$agency_id = $admin->agency_id;
	?>  <!--no-print prevents this section from creating whitespace when printing any page -->
		<div >
				<div class="nav-collapse sidebar-nav" >
					<ul class="nav nav-tabs nav-stacked main-menu">
						<li <?php if(1) {if($this->uri->segment(2) == 'dashboard') {?> class="active"<?php }?>><a href="<?=url::site('dashboard')?>"><i class="icon-picture"></i><span class="hidden-tablet"> Dashboard</span></a><?php }?></li>	
						<li <?php if($_SESSION['ModuleAccess']['1']) {if($this->uri->segment(2) == 'main') {?> class="active"<?php }?>><a href="<?=url::site('admin')?>"><i class="icon-home"></i><span class="hidden-tablet"> Home</span></a><?php }?></li>	
						<li <?php if($_SESSION['ModuleAccess']['2']){ if($this->uri->segment(2) == 'all_schedules') {?> class="active"<?php }?>><a href="<?=url::site('admin/all_schedules/current')?>"><i class="icon-list"></i><span class="hidden-tablet"> Schedules</span></a><?php }?></li>	
						<li <?php if($_SESSION['ModuleAccess']['3']) { if($this->uri->segment(1) == 'expenses') {?> class="active"<?php }?>><a href="<?=url::site('expenses/all')?>"><i class="icon-wrench"></i><span class="hidden-tablet"> Expenses</span></a><?php }?></li>
						<li <?php if($_SESSION['ModuleAccess']['4'])  { if($this->uri->segment(1) == 'parcels') {?> class="active"<?php }?>><a href="<?=url::site('parcels/all/incoming')?>"><i class="icon-gift"></i><span class="hidden-tablet"> Parcels</span></a><?php }?></li>
						
						<li <?php if($_SESSION['ModuleAccess']['5']) { if ($this->uri->segment(1) == 'buses') {?> class="active"<?php }?>><a href="<?=url::site('buses/all')?>"><i class="icon-bus"></i><span class="hidden-tablet"> Buses</span></a><?php }?></li>
						<li <?php if($_SESSION['ModuleAccess']['6']) {if($this->uri->segment(1) == 'reminders') {?> class="active"<?php }?>><a href="<?=url::site('reminders/all')?>"><i class="icon-check"></i><span class="hidden-tablet"> Reminders</span></a><?php }?></li>
						<li <?php if($_SESSION['ModuleAccess']['7']) { if($this->uri->segment(1) == 'reports') {?> class="active"<?php }?>><a href="<?=url::site('reports')?>"><i class="icon-info-sign"></i><span class="hidden-tablet"> Reports</span></a><?php }?></li>
						<li <?php if($_SESSION['ModuleAccess']['8']) { if($this->uri->segment(1) == 'cash') {?> class="active"<?php }?>><a href="<?=url::site('cash/collections')?>"><i class="icon-money"></i><span class="hidden-tablet"> Collections</span></a><?php }?></li>
						<li class="disabled"<?php if(1) { if($this->uri->segment(1) == 'invoices') {?> class="active"<?php }?>><a href="<?=url::site('cash/collections')?>" ><i class="icon-inbox"></i><span class="hidden-tablet"> Invoices</span></a><?php }?></li>
						<li <?php if($_SESSION['ModuleAccess']['9']) { if($this->uri->segment(1) == 'settings') {?> class="active"<?php }?>><a href="<?=url::site('settings/general')?>"><i class="icon-cog"></i><span class="hidden-tablet"> Settings</span></a><?php }?></li>
						<!--<li>
							<a class="dropmenu" href="#"><i class="icon-folder-close-alt"></i><span class="hidden-tablet"> Dropdown</span><span class="label label-important"> 3 </span></a>
							<ul>
								<li><a class="submenu" href="submenu.html"><i class="icon-file-alt"></i><span class="hidden-tablet"> Sub Menu 1</span></a></li>
								<li><a class="submenu" href="submenu2.html"><i class="icon-file-alt"></i><span class="hidden-tablet"> Sub Menu 2</span></a></li>
								<li><a class="submenu" href="submenu3.html"><i class="icon-file-alt"></i><span class="hidden-tablet"> Sub Menu 3</span></a></li>
							</ul>	
						</li>
						<li><a href="form.html"><i class="icon-edit"></i><span class="hidden-tablet"> Forms</span></a></li>
						<li><a href="chart.html"><i class="icon-list-alt"></i><span class="hidden-tablet"> Charts</span></a></li>
						<li><a href="typography.html"><i class="icon-font"></i><span class="hidden-tablet"> Typography</span></a></li>
						<li><a href="gallery.html"><i class="icon-picture"></i><span class="hidden-tablet"> Gallery</span></a></li>
						<li><a href="table.html"><i class="icon-align-justify"></i><span class="hidden-tablet"> Tables</span></a></li>
						<li><a href="calendar.html"><i class="icon-calendar"></i><span class="hidden-tablet"> Calendar</span></a></li>
						<li><a href="file-manager.html"><i class="icon-folder-open"></i><span class="hidden-tablet"> File Manager</span></a></li>
						<li><a href="icon.html"><i class="icon-star"></i><span class="hidden-tablet"> Icons</span></a></li>
						<li><a href="login.html"><i class="icon-lock"></i><span class="hidden-tablet"> Login Page</span></a></li>-->
					</ul>
				</div>
			</div>
		<?php }
		//Now, the menu for the MANAGE section online, since all us the same menu
		
		if ($this->uri->segment(1) == 'manage'){ ?>
			<div class="row-fluid">
			<div class="navbar">
			  <div class="navbar-inner">
				<ul class="nav bold">
				  <li  <?php if($this->uri->segment(2) == 'main') {?> class="active"<?php }?>><a href="<?=url::site('manage')?>"><i class="icon-home"></i> <?=Kohana::lang('backend.home')?> </a> </li> 
				  <li  <?php if($this->uri->segment(2) == 'expenses') {?> class="active"<?php }?>><a href="<?=url::site('manage/expenses')?>"><i class="icon-wrench"></i></i> <?=Kohana::lang('backend.expenses')?> </a> </li> 
				  <li  <?php if($this->uri->segment(2) == 'online_purchases') {?> class="active"<?php }?>><a href="<?=url::site('manage/online_purchases')?>"><i class="icon-shopping-cart"></i></i> <?=Kohana::lang('backend.online_purchases')?> </a> </li> 
				  <li  <?php if($this->uri->segment(2) == 'cash') {?> class="active"<?php }?>><a href="<?=url::site('manage/cash')?>"><i class="icon-money"></i></i> <?=Kohana::lang('backend.cash')?> </a> </li> 
				  <li  <?php if($this->uri->segment(2) == 'reports') {?> class="active"<?php }?>><a href="<?=url::site('manage/reports')?>"><i class="icon-info-sign"></i></i> <?=Kohana::lang('backend.reports')?> </a> </li> 
				  
				  <li  <?php if($this->uri->segment(2) == 'online_settings') {?> class="active"<?php }?>><a href="<?=url::site('manage/online_settings')?>"><i class="icon-cog"></i> <?=Kohana::lang('backend.online_settings')?> </a> </li> 
		
				</ul>
				
			  </div>
			</div>
					
		</div>
		
		
		<?php
		}
		?>
		
		<script type="text/javascript">
			$(".nav-tabs .disabled").on("click", function(e) {
				e.preventDefault();
				alert('Coming soon');
				return false;
				
	  
			});
			
		</script>