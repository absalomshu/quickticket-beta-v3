<?php defined('SYSPATH') or die('No direct script access'); ?> 
	
	
	<div class="row-fluid marketing" style="margin:5px 0 0px 0;">	
		<div class="span12 hero-unit prof-unit reload" style="margin-left:0px;">
	

				<ul class="nav nav-tabs">
					<?php //if ($this->uri->segment(3)=='current') {
						//$sched_typ is to verify if on current or departed tab. Will be used to adjust the sorting links adequately
						
					?>
						<li <?php if ($this->uri->segment(3)=='current'){ $sched_type = 'current'; ?> class="active"<?php } ?>> <a href="<?=url::site('admin/all_schedules/current')?>"> <i class="icon-time "></i> <?=Kohana::lang('backend.current')?> </a></li>
						<li <?php if ($this->uri->segment(3)=='departed'){ $sched_type = 'departed'; ?> class="active"<?php } ?>> <a href="<?=url::site('admin/all_schedules/departed/sort_by_date')?>"> <i class="icon-ok-sign "></i> <?=Kohana::lang('backend.departed')?> </a></li>
						<li <?php if ($this->uri->segment(3)=='incoming'){ ?> class="active"<?php } ?>> <a href="<?=url::site('admin/all_schedules/incoming')?>"> <i class="icon-ok-sign "></i> <?=Kohana::lang('backend.incoming')?> </a></li>
						
					
					
					<li class=""><a href="<?=url::site('admin/tickets_pending')?>"> <i class="icon-tags "></i> <?=Kohana::lang('backend.tickets_pending')?> </a></li>
					<li class=""><a href="<?=url::site('admin/remote_tickets')?>"> <i class="icon-random "></i> <?=Kohana::lang('backend.remote_tickets')?>  <?php if($this->total_pending_remote_tickets) {?> <span class="badge badge-important"><?=$this->total_pending_remote_tickets;?><?php }?></a></li>
					<li class=""><a href="<?=url::site('admin/online_purchases')?>"> <i class="icon-globe "></i> <?=Kohana::lang('backend.online_purchases')?> <?php if($this->total_pending_online_purchases) {?> <span class="badge badge-important"><?=$this->total_pending_online_purchases;?><?php }?></a></li>

					<li <?php if ($this->uri->segment(2)=='check_schedules'){?> class="active"<?php } ?>><a href="<?=url::site('admin/check_schedules')?>"><i class="icon-search"></i> <?=Kohana::lang('backend.check_schedules')?></a></li>
				</ul>
				
				<?php
				if ($this->uri->segment(3)=='incoming'){ ?>
				<legend><?=Kohana::lang('backend.sched_incoming_today'). ", ". date("d-m-Y")?></legend>
				<table class="table table-striped table-hover table-condensed">
					<tr>
						<th>Date </th>
						<th><?=Kohana::lang('backend.bus_no')?></th>
						<th><?= Kohana::lang('backend.agency_from')?> </th>
						<th><?php //Kohana::lang('backend.from')?> </th>
						<th><?=Kohana::lang('backend.to')?></th>
						<th><?=Kohana::lang('backend.status')?></th>
						<th><?=Kohana::lang('backend.departure_time')?> </th> 	
						<th><?=Kohana::lang('backend.checked_out_at')?> </th> 	
						<th><?=Kohana::lang('backend.created_by')?> </th> 	
					</tr>
					<?php foreach ($incoming_schedules as $in_schedule):?>
						<tr>
							<td><?=date("d-m-Y",strtotime($in_schedule->departure_date))?></td>
							
							<td><?=$in_schedule->bus_number?></a></td> 
							
							<td><?=get::agency_name($in_schedule->from_agency_id)?></td>
							<td><?php //get::town($in_schedule->from)?></td>
							<td><?=get::town($in_schedule->to)?></td>
							<td><?=$in_schedule->status?></td>
							<td><?=date("h:i A",strtotime($in_schedule->departure_time))?></td>
							<td><?=date("h:i A",strtotime($in_schedule->checked_out_time))?></td>
							<td><?=$in_schedule->CreatedBy?></td>
						</tr>
					<?php endforeach; ?>
				</table>
				
				<!-- refresh incoming schedules -->
				<a onclick="RefreshIncomingSchedules();" href="#" class="btn" id="refresh">Refresh now</a>
				<script type="text/javascript">
					//triggers, waits one second, then reloads page
					function RefreshIncomingSchedules(){
						$('#js_messages').load('<?=url::site('online/pull_schedules');?>');
						$("#refresh").addClass("disabled");
						$("#refresh").text("Refreshing...");
						setTimeout(function(){
							location.reload();
						}, 2000);
					}
				</script>
				
				
				
				
				<?php } elseif ($this->uri->segment(2)=='check_schedules'){ ?>
					<div class="tab-pane" id="">
					<form action="<?=url::site('admin/check_schedules')?>" method="POST" class="span8 offset2" >		
					<legend><?=Kohana::lang('backend.check_schedules')?></legend>	
						<div class="">
							<div class="span4"><?=Kohana::lang('backend.schedule_id')?><span class="red"> *</span>: </div>
							<div class="span8">
								<div >
									<input type="text" name = 'schedule_id' class="span3" placeholder='e.g. SC10010' >
								</div>
							
							</div>
						</div>
						<div class="clear"></div>
						<div class="">
							<div class="span4"> </div>
							<div class="form-actions">
								<button class="btn  btn-info" type="submit" name="by_date"  ><i class="icon-search icon-white"></i> <?=Kohana::lang('backend.check')?></button><br/>
							</div>
						</div>
						
						
									
					</form>	
				</div>
				<?php }else { ?>
				<table class="table table-striped table-hover table-condensed">
					<tr>
						<?php
						if ($this->uri->segment(3)=='current') { ?>
							<legend><?=Kohana::lang('backend.current_schedules')?></legend>	
						<?php }else{?>
							<legend><?=Kohana::lang('backend.past_schedules')?></legend>	
						<?php }?>
						<th>Date <!--<a href="<?=url::site('admin/all_schedules/departed/sort_by_date')?>"> <i class="icon-chevron-down" title="Sort descending by date"></i></a>--></th>
						<th><?=Kohana::lang('backend.bus_no')?> <a href="<?=url::site('admin/all_schedules/'.$sched_type.'/sort_by_bus')?>"> <i class="icon-chevron-down" title="Sort descending by bus number"></i></a></th>
						<!--<th><?php //Kohana::lang('backend.from')?>  <a href="<?php //url::site('admin/all_schedules/'.$sched_type.'/sort_by_origin')?>"><i class="icon-chevron-down" title="Sort descending by origin"></i></a></th>-->
						<th><?=Kohana::lang('backend.to')?> <a href="<?=url::site('admin/all_schedules/'.$sched_type.'/sort_by_destination')?>">  <i class="icon-chevron-down" title="Sort descending by destination"></i></a></th>
						<?php if ($this->uri->segment(3)=='current') {?><th><?=Kohana::lang('backend.departure_time')?> <a href="<?=url::site('admin/all_schedules/departed/sort_by_time')?>"><i class="icon-chevron-down" title="Sort descending by departure time"></i></a></th><?php } else {?>
						<th><?=Kohana::lang('backend.departed_at')?> <a href="<?=url::site('admin/all_schedules/'.$sched_type.'/sort_by_time')?>">  <i class="icon-chevron-down" title="Sort descending by departure time"></i></a></th> <?php } ?>
					</tr>
					<?php foreach ($schedules as $schedule):?>
					<tr>
						<td><?=date("d-m-Y",strtotime($schedule->departure_date))?></td>
						
						<td>
							<!--make a link only if it's a simple admin lest a manager should go to a current schedule-->
							<?php	if($this->admin->admin_group_id == 2){  ?>
								<a href="<?php 
									if ($this->uri->segment(3)=='current') { 
										echo url::site('admin/complete_schedule').'/'.$schedule->id;
									}
									else{echo url::site('admin/past_schedule').'/'.$schedule->id;}?>">
									<?php }?>
							<?=$schedule->bus_number?></a></td> 
						
						<!--<td><?php //get::town($schedule->from)?></td>-->
						<td><?=$schedule->town_name?></td>
						<td><!--show either the time it has to depart or the time it departed-->
							<?php if ($this->uri->segment(3)=='current') { echo date("h:i a", strtotime($schedule->departure_time)); } else {echo date("h:i a", strtotime($schedule->checked_out_time));}?>
						</td>
					</tr>
					<!--<input type="hidden"  class="schedule_id" value="<?=$schedule->id?>"></input>-->
					<?php endforeach; ?>
				</table>
					<?php echo $this->pagination;?>
					<?php }?>
					
					
<?php //$this->profiler = new Profiler();?>
		</div>
	
		
		</div>
	 
	  <div style="height:19px;"></div>

