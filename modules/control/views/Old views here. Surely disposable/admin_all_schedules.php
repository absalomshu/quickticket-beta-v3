<?php defined('SYSPATH') or die('No direct script access'); ?> 
	
	<div>
	<?php
		//determine what type of notice to display if at all
		$notice = $this->session->get_once('notice');
			if(!empty($notice)){ 
				if($notice['type'] == 'success'){?><div class="alert general success"><a class="close" data-dismiss="alert" href="#">&times;</a><?php echo $notice['message']; }?></div>
			<?	if($notice['type'] == 'error'){?><div class="alert general error"><a class="close" data-dismiss="alert" href="#">&times;</a><?php echo $notice['message']; }?></div>
		<?}?>
 
	  </div>
	  <div class="row-fluid marketing" style="margin:5px 0 0px 0px;">
	  
	<div class="row-fluid marketing" style="margin:5px 0 0px 0;">	
		<div class="span12 hero-unit prof-unit reload" style="margin-left:0px;">
	

				<ul class="nav nav-tabs">
				<?php if ($this->uri->segment(3)=='current') {?>
					<li class="active"> <a href=""> <i class="icon-time "></i> <?=Kohana::lang('backend.current')?> </a></li>
					<li><a href="<?=url::site('admin/all_schedules/departed/sort_by_time')?>"> <i class="icon-ok-sign "></i> <?=Kohana::lang('backend.departed')?> </a></li>
				<?php	}else { ?>
					<li><a href="<?=url::site('admin/all_schedules/current')?>"> <i class="icon-time "></i> <?=Kohana::lang('backend.current')?> </a></li>
					<li class="active"><a href="#"> <i class="icon-ok-sign "></i> <?=Kohana::lang('backend.departed')?> </a></li>
					<?php } ?>
				</ul>
				
				<table class="table table-striped table-hover">
					<tr>
						<th>Date <a href="<?=url::site('admin/all_schedules/departed/sort_by_date')?>"> <i class="icon-chevron-down" title="Sort descending by date"></i></a></th>
						<th><?=Kohana::lang('backend.bus_no')?> <a href="<?=url::site('admin/all_schedules/departed/sort_by_bus')?>"> <i class="icon-chevron-down" title="Sort descending by bus number"></i></a></th>
						<th><?=Kohana::lang('backend.from')?>  <a href="<?=url::site('admin/all_schedules/departed/sort_by_origin')?>"><i class="icon-chevron-down" title="Sort descending by origin"></i></a></th>
						<th><?=Kohana::lang('backend.to')?> <a href="<?=url::site('admin/all_schedules/departed/sort_by_destination')?>">  <i class="icon-chevron-down" title="Sort descending by destination"></i></a></th>
						<?php if ($this->uri->segment(3)=='current') {?><th><?=Kohana::lang('backend.departure_time')?> <a href="<?=url::site('admin/all_schedules/departed/sort_by_time')?>"><i class="icon-chevron-down" title="Sort descending by departure time"></i></a></th><?php } else {?>
						<th><?=Kohana::lang('backend.departed_at')?> <a href="<?=url::site('admin/all_schedules/departed/sort_by_time')?>">  <i class="icon-chevron-down" title="Sort descending by departure time"></i></a></th> <?php } ?>
					</tr>
					<?php foreach ($schedules as $schedule):?>
					<tr>
						<td><?=date("d-m-Y",strtotime($schedule->departure_date))?></td>
						
						<td>
							<!--make a link only if it's a simple admin lest a manager should go to a current schedule-->
							<?php	if($this->admin->admin_group_id == 2){  ?>
								<a href="<?php 
									if ($this->uri->segment(3)=='current') { 
										echo url::site('admin/complete_schedule').'/'.$schedule->id;
									}
									else{echo url::site('admin/past_schedule').'/'.$schedule->id;}?>">
									<?php }?>
							<?=$schedule->bus_number?></a></td> 
						
						<td><?=get::town($schedule->from)?></td>
						<td><?=get::town($schedule->to)?></td>
						<td><!--show either the time it has to depart or the time it departed-->
							<?php if ($this->uri->segment(3)=='current') { echo $schedule->departure_time; } else {echo $schedule->checked_out_time;}?>
						</td>
					</tr>
					<!--<input type="hidden"  class="schedule_id" value="<?=$schedule->id?>"></input>-->
					<?php endforeach; ?>
				</table>
					<?php echo $this->pagination;?>
<?php //$this->profiler = new Profiler();?>
		</div>
	
		
		</div>
	  </div>
	  <div style="height:19px;"></div>

