<?php defined('SYSPATH') or die('No direct script access'); ?> 
	
	<div class="notice">
		<?php
			//determine what type of notice to display if at all
			$notice = $this->session->get_once('notice');
				if(!empty($notice)){ 
					if($notice['type'] == 'success'){?><div class="alert general success"><a class="close" data-dismiss="alert" href="#">&times;</a><?php echo $notice['message']; ?></div><?php }?>
				<?	if($notice['type'] == 'error'){?><div class="alert general error"><a class="close" data-dismiss="alert" href="#">&times;</a><?php echo $notice['message'];?></div><?php }?>
		<?}?>
	</div>
 
	<div class="row-fluid " style="margin:5px 0 0px 0px;">
	  
		
		
	  	<div class="span12 hero-unit prof-unit" >
		
		<ul class="nav nav-tabs" id="myTab">
					<li><a href="#add-expense"><i class="icon-plus"></i> <?=Kohana::lang('backend.add_expense')?></a></li>
					<li ><a href="#expenses"><i class="icon-list"></i> <?=Kohana::lang('backend.exp_list')?></a></li>
					<li class="active"><a href="#check-expenses"><i class="icon-search"></i> <?=Kohana::lang('backend.check_exp')?></a></li>
		</ul>
		
		<div class="tab-content">
			<div class="tab-pane" id="add-expense">
				<form action="<?=url::site('expenses')?>" method="POST" name="theform">
					<div class="span6" >
						<div class="simple-form span12" style="margin-left:10px;">
							<div class="sf_label">Date: </div>
								<div class="input-append date">
									<input type="text" name = 'date_incurred' class="span5 datepicker"  value="<?=date('d-m-Y')?>" id="dp1" required>
									<span class="add-on"><i class="icon-th"></i></span>
								</div>
						</div>
						
						<div class="simple-form span12" >
							<div class="sf_label"><?=Kohana::lang('backend.bus_no')?>: </div>
							<div class="sf_input"><input type="text"  name="bus_number"></div>
						</div>
						
						<div class="simple-form span12">
							<div class="sf_label"><?=Kohana::lang('backend.authorised_by')?>: </div>
							<div class="sf_input"><input type="text"  name="authorised_by" required></div>
						</div>
						
						<div class="simple-form span12">
							<div class="sf_label"><?=Kohana::lang('backend.amount')?>: </div>
							<div class="sf_input controls input-append"><input type="text"  name="amount" required><span class="add-on"> FCFA</span></div>
						</div>
						
						<div class="simple-form span12">
											<div class="sf_label"><?=Kohana::lang('backend.purpose')?>: </div>
											<div class="sf_text"><textarea placeholder="<?=Kohana::lang('backend.spent_on')?>"  name="purpose" class="span11" required ></textarea></div>
						</div>
								
						
					
						<div class="clear"></div>
						<div class="clear"><br/><br/></div>
						
						<div class="simple-form span12">
							<div class="sf_label">&nbsp; </div>
							<div class="sf_input"><button class="btn btn-info " type="submit"><?=Kohana::lang('backend.register')?></button></div>
						</div>
						
									
					</div>
						
				</form>
					<!--end the first tab-->
			</div>  
			 
			<div class="tab-pane" id="expenses">
				<table class="table table-bordered table-striped table-hover">
					<tr>
						<th>Date</th>
						<th><?=Kohana::lang('backend.amount')?></th>
						<th><?=Kohana::lang('backend.purpose')?></th>
						<th><?=Kohana::lang('backend.bus_no')?></th>
						<th><?=Kohana::lang('backend.reg_at')?></th>
						<th><?=Kohana::lang('backend.authorised_by')?></th>
					</tr>
					<?php foreach ($all_expenses as $expense):?>
						<tr>	
							<td><?=date("d-m-Y",strtotime($expense->date_incurred))?></td>
							
							<td><?=$expense->amount?></td>
							
							<!--Trim if the description is too long. -->
								<td><a href="<?=url::site('expenses/details/'.$expense->id)?>"> 
									<?php if (strlen($expense->purpose) > 25) { echo substr($expense->purpose,0,35)."...";} else { echo $expense->purpose;}?>
								</a></td>
							
							
							<td><?=$expense->bus_number?></td>
							<td><?=get::agency_name($expense->agency_id)?></td>
							<td><?=$expense->authorised_by?></td>
						</tr>
						<?php endforeach; ?>
				</table>	
			
			</div>
				
			<div class="tab-pane active" id="check-expenses">
			<legend><?=Kohana::lang('backend.which_bus_exp')?>?</legend>
				<form action="<?=url::site('expenses/check_bus')?>" method="POST">
									
					<div class="accordion" id="accordion2">
					  <div class="accordion-group">
						<div class="accordion-heading">
						  <a class="accordion-toggle" data-toggle="collapse" data-parent="#accordion2" href="#collapseOne">
							<?=Kohana::lang('backend.select_bus')?>:
						  </a>
						</div>
						<div id="collapseOne" class="accordion-body collapse in">
						  <div class="accordion-inner">
						  
							<div class="sf_input">
												<select name="existing_bus_number" id="inputType" class="span3">
														<?php foreach ($all_buses as $bus):?>
														<option value="<?=$bus->bus_number?>"><?=$bus->bus_number?></option>
														<?php endforeach;?>
													
												</select>
										<button type="submit" class="btn btn-success" name="existing_bus"><?=Kohana::lang('backend.search')?></button>
										
							</div>
						  
						  </div>
						</div>
					  </div>
					  <div class="accordion-group">
						<div class="accordion-heading">
						  <a class="accordion-toggle" data-toggle="collapse" data-parent="#accordion2" href="#collapseTwo">
							<?=Kohana::lang('backend.enter_bus_search')?>:
						  </a>
						</div>
						<div id="collapseTwo" class="accordion-body collapse">
						  <div class="accordion-inner">
							
							<input type="text" class="span3 search-query" name="search_bus_number"/>
							<button type="submit" class="btn btn-success" name="search_bus"><?=Kohana::lang('backend.search')?></button>
							
							
							
							
						  </div>
						</div>
					  </div>
					</div>
						
				</form>
				<?php 
						//if it was a search, and if the result wasn't zero
						if((isset($bus_expenses)) AND ($count_expenses != 0)){
							$total_expenses = 0;
						?>
							<pre><?=Kohana::lang('backend.search_results')?>: <?=Kohana::lang('backend.exp_for')?> <b><?=$bus_number?></b></pre>
							
							<div class="clear"></div>
							<table class="table  table-striped table-hover">
										<tr>
											<th>Date</th>
											<th><?=Kohana::lang('backend.amount')?></th>
											<th><?=Kohana::lang('backend.purpose')?></th>
											<th><?=Kohana::lang('backend.bus_no')?></th>
											<th><?=Kohana::lang('backend.authorised_by')?></th>
										</tr>
										<?php foreach ($bus_expenses as $expense):?>
											<tr>	
												<td><?=date("d-m-Y",strtotime($expense->date_incurred))?></td>
												
												<td><?=$expense->amount?></td>
												
												<!--Trim if the description is too long. -->
													<td><a href="<?=url::site('expenses/details/'.$expense->id)?>"> 
														<?php if (strlen($expense->purpose) > 25) { echo substr($expense->purpose,0,35)."...";} else { echo $expense->purpose;}?>
													</a></td>
												
												
												<td><?=$expense->bus_number?></td>
												<td><?=$expense->authorised_by?></td>
												<?php 
													//sum the expenses
													$total_expenses += $expense->amount; 
												?>
											</tr>
										<?php endforeach; ?>
											<tr>
												<th><?=Kohana::lang('backend.total')?></th>
												<th><?=$total_expenses?> FCFA </th>
												
											</tr>
							</table>
								
									<?php }?>		
					
			</div>
				
			  			
		</div>
		
			
		
			  
		</div>
		
		</div>
	  <div class="clear"></div>

<script type="text/javascript">
	$(document).ready(function(){
			$('#timepicker').timepicker({
				minuteStep:30,
				//defaultTime: '8:00 PM'
			});
			
			
			$('.input-append.date').datepicker({
				format: "dd-mm-yyyy",
				todayBtn: "linked",
				autoclose: true,
				todayHighlight: true
			});
			
			
				$(function () {
				$('#myTab a').click(function (e) {
					  e.preventDefault();
					  $(this).tab('show');  
			})
			  })
			  
		
			  
			
	})
	</script>
