<?php defined('SYSPATH') or die('No direct script access');

class Incoming_schedule_Model extends ORM{

	public function get_all_today($agency_id)
	{	
		$today = date("Y-m-d");
		$schedules=ORM::factory('incoming_schedule')->where('agency_id',$agency_id)->where('departure_date',$today)->where('deleted','0')->find_all();
		//var_dump($schedules);exit;
		return $schedules;
	}
	
	

}
