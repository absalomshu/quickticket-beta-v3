<?php defined('SYSPATH') or die('No direct script access'); ?> 
	
	<div class="row-fluid ">
	  	<div class="span12 hero-unit prof-unit" >
		
		<ul class="nav nav-tabs" id="myTab">
					<li <?php if ($this->uri->segment(2)=='add_bus'){?> class="active"<?php } ?>><a href="<?=url::site('buses/add_bus')?>"><i class="icon-plus"></i> <?=Kohana::lang('backend.add_bus')?></a></li>
					<li <?php if ($this->uri->segment(2)=='all'){?> class="active"<?php } ?>><a href="<?=url::site('buses/all')?>"><i class="icon-list"></i> <?=Kohana::lang('backend.all_buses')?></a></li>
					<li <?php if ($this->uri->segment(2)=='rentals'){?> class="active"<?php } ?>><a href="<?=url::site('buses/rentals')?>"><i class="icon-share-alt"></i> <?=Kohana::lang('backend.rentals')?></a></li>
					<li <?php if ($this->uri->segment(2)=='check_buses'){?> class="active"<?php } ?>><a href="<?=url::site('buses/check_buses')?>"><i class="icon-search"></i> <?=Kohana::lang('backend.check_buses')?></a></li>
		</ul>
		
	
		<?php if ($this->uri->segment(2)=='add_bus'){ ?>
			<div class="tab-pane" id="add-bus">
				<form action="<?=url::site('buses/add_bus')?>" method="POST" enctype="multipart/form-data" class="span8 offset2">
						<legend><?=Kohana::lang('backend.add_bus')?></legend>
						<div class="">
							<div class="span4 text-right"><?=Kohana::lang('backend.bus_no')?><span class="red"> *</span>: </div>
							<div class="span8"><input type="text"  name="bus_number" required></div>
						</div>
						
						<div class="">
							<div class="span4 text-right"><?=Kohana::lang('backend.serial_no')?>: </div>
							<div class="span8"><input type="text"  name="serial_number" ></div>
						</div>
						
						<div class="">
							<div class="span4 text-right"><?=Kohana::lang('backend.bus_name')?>: </div>
							<div class="span8"><input type="text"  name="bus_name"></div>
						</div>
						
						<div class="">
							<div class="span4 text-right"><?=Kohana::lang('backend.number_seats')?><span class="red"> *</span>: </div>
								<div class="span8">
									<select name="bus_seats" id="inputType" required>
										
										<?php for($i=30;$i<=80;$i++){ ?>
											<option value="<?=$i?>"><?=$i?> <?=Kohana::lang('backend.seater')?></option>
										<?php } ?>
										
									</select>
								</div>
							
						</div>
						
						<div class="">
							<div class="span4 text-right"><?=Kohana::lang('backend.drivers_name')?>: </div>
							<div class="span8"><input type="text"  name="driver" ></div>
						</div>
						
						<div class="">
							<div class="span4 text-right"><?=Kohana::lang('backend.motorboys_name')?>: </div>
							<div class="span8"><input type="text"  name="motorboy" ></div>
						</div>
						
						<div class="">
							<div class="span4 text-right"><?=Kohana::lang('backend.other_information')?>: </div>
							<div class="span8"><textarea  name="other_infos" style="height:100px;"></textarea></div>
						</div>
						
						<div class="">
							<div class="span4 text-right"><?=Kohana::lang('backend.vip_bus')?>: </div>
							<div class="span8">
								<input type="hidden" name="vip" value="0" />
								<label class="checkbox"><input type="checkbox" name="vip"/></label>
								
							</div>
						</div>
						
						 <div >
								<div class="span4 text-right" >Photo:</div>
								<div class="span8">
											<input type="file"  name="bus_photo" />
								</div>
						</div>
						
						<div class="clear"></div>
						<div class="clear"><br/><br/></div>
						
						<div class="form-actions">
							<div class="span4">&nbsp; </div>
							<div class="span8">
								<button class="btn btn-info " type="submit"><?=Kohana::lang('backend.register')?></button>
								<a href="<?=url::site('buses/all')?>" class="btn" type="submit"> <?=Kohana::lang('backend.cancel')?></a>
							</div>
							
						</div>
						
									
						
				</form>
					<!--end the first tab-->
			</div>  
			<?php }?>
			 
			 
			<?php if ($this->uri->segment(2)=='all'){ ?>
			<div class="tab-pane active" id="buses">
			<legend><?=Kohana::lang('backend.all_buses')?></legend>
				<table class="table table-striped table-hover table-condensed">
					<tr>
						
						<th><?=Kohana::lang('backend.bus_no')?></th>
						<th><?=Kohana::lang('backend.serial')?></th>
						<th><?=Kohana::lang('backend.name')?></th>
						<th><?=Kohana::lang('backend.seats')?></th>
						<th><?=Kohana::lang('backend.driver')?></th>
						<th><?=Kohana::lang('backend.status')?></th>
						<th><?=Kohana::lang('backend.documents')?></th>
						<th>Actions</th>
						
					</tr>
					<?php foreach ($all_buses as $bus):?>
					<?php 
					//Do not show the UNKNOWN bus details
					if($bus->bus_number != "UNKNOWN"){	?>
						<tr>	
							
							
							<td>
								<i class="icon-bus"></i> 
								
								<a  href="<?=url::site('buses/maintain/'.$bus->bus_number)?>" >
									<?=$bus->bus_number?><?php if($bus->vip==1){?> <i class="icon-star" title="VIP Bus"></i> <?php }?>
								</a>
							</td>
							<td><?=$bus->serial_number?></td>
							<td><?=$bus->bus_name?></td>
							<td><?=$bus->bus_seats?></td>
							<td><?=$bus->driver?></td>
							<td><?php if ($bus->is_available){ ?>
								<span class="text-muted tiny-text"> <?=Kohana::lang('backend.available')?> </span>
							<?php }else{ ?> <span class="text-error tiny-text"><?=Kohana::lang('backend.unavailable')?></span><?php }?>
							</td>
							<td><?php
								$no_expired_docs = Bus_requirements_status_Model::count_bus_expired_requirements($this->agency_id,$bus->bus_number);
								$no_expiring_docs = Bus_requirements_status_Model::count_bus_expiring_requirements($this->agency_id,$bus->bus_number);
								$no_undone_docs = Bus_requirements_status_Model::count_all_undone_for_bus($this->agency_id,$bus->bus_number);
								if($no_expired_docs > 0){echo "<span class='text-error'> $no_expired_docs expired </span> , ";}
								if($no_expiring_docs > 0){echo " <span class='text-warning'> $no_expiring_docs expiring</span> ,";}
								if($no_undone_docs > 0){echo " <span class=''> $no_undone_docs unavailable</span>";}
							?></td>
							<td>
								
								<div class="btn-group ">
									  <a class="btn dropdown-toggle btn-mini" data-toggle="dropdown" href="#" >
										-- <?=Kohana::lang('backend.select')?> --
										<span class="caret"></span>
									  </a>
									  <ul class="dropdown-menu">
										<li><a href="<?=url::site('buses/maintain/'.$bus->bus_number)?>"> <?=Kohana::lang('backend.maintain')?></a></li>
										<li><a href="<?=url::site('buses/add_bus_rental/'.$bus->bus_number)?>"> <?=Kohana::lang('backend.add_bus_rental')?></a></li>
										<li><a href="<?=url::site('buses/edit_bus/'.$bus->bus_number)?>"><?=Kohana::lang('backend.edit')?></a></li>
										
										<!--<li>
											<?php if ($bus->is_available){ ?>
												<a href="<?=url::site('buses/make_unavailable/'.$bus->bus_number)?>"> <?=Kohana::lang('backend.make_unavailable')?></a>
											<?php }else{?>
											<a href="<?=url::site('buses/make_available/'.$bus->bus_number)?>"> <?=Kohana::lang('backend.make_available')?></a>
											<?php }?>
										</li>
										<li><a href="<?=url::site('buses/availability_details/'.$bus->bus_number)?>">Availability history</a></li>
										-->
									  </ul>
								</div>
							
							</td>
						
							<td>
								
							</td>
						
						
						</tr><?php } ?>
						<?php endforeach; ?>
				</table>	
			<?php echo $this->pagination;?>	  	
			</div>
			<?php }?>
			
			<?php if ($this->uri->segment(2)=='rentals'){ ?>
			<div class="tab-pane active" id="buses">
			<legend><?=Kohana::lang('backend.rentals')?></legend>
				<table class="table table-striped table-hover table-condensed">
					<tr>
						<th>Date</th>
						<th><i class="icon-bus" title="VIP Bus"></i><?=Kohana::lang('backend.bus_no')?></th>
						<th>To</th>
						<th><?=Kohana::lang('backend.start_date')?></th>
						<th><?=Kohana::lang('backend.end_date')?></th>
						<th><?=Kohana::lang('backend.amount')?></th>
						<!--<th>Actions</th>-->
						
					</tr>
					<?php foreach ($all_rentals as $rental):?>
						<tr>	
							
							<td><?=date("d-m-Y",strtotime($rental->date_incurred))?></td>
							<td> <?=$rental->bus_number?></td>
							<td> <?=$rental->rented_to?></td>
						
							<td>
								<?=date("d-m-Y",strtotime($rental->start_date))?>
							</td>
							<td>
								<?=date("d-m-Y",strtotime($rental->end_date))?>
							</td>
							<td>
								<?=number_format($rental->amount)?>
							</td>
						
						
						</tr>
						<?php endforeach; ?>
				</table>	
			<?php echo $this->pagination;?>	  	
			</div>
			<?php }?>


			<?php if ($this->uri->segment(2)=='check_buses'){ ?>

				
				<div class="tab-pane" id="check-bus">
				
					<legend><?=Kohana::lang('backend.bus_activity')?><!--<a href="#" class="view">See all activity</a>--></legend>						
						<form action="<?=url::site('buses/check_buses/')?>" method="POST" >
						<div class="">
							<div class="span4 text-right"><?=Kohana::lang('backend.bus_no')?><span class="red"> *</span>: </div>
							<div class="span8">
								<select name="bus_number" id="inputType">
								
									<?php 
										foreach ($all_buses as $bus){?>
										<option value="<?=$bus->bus_number?>"><?=$bus->bus_number?></option>
									<?php }; ?>
								
								</select>
							
							</div>
						</div>
					
						<!--
							<div class="span12"><?=Kohana::lang('backend.on');?>:</div>
							<div class="input-append date span4">
								<input type="text" name = 'date' class="span8 datepicker"  value="<?=date('d-m-Y')?>" id="dp1" >
								<span class="add-on"><i class="icon-calendar"></i></span>
							</div>				
							<button class="btn  btn-info" type="submit" name="by_date"  ><i class="icon-search icon-white"></i> <?=Kohana::lang('backend.view');?></button><br/>
							
							<div class="clear"></div>
								<legend></legend>
						-->
						
						<div class="">
							<div class="span4 text-right"><?=Kohana::lang('backend.from')?><span class="red"> *</span>: </div>
							<div class="span8">
								<div class="input-append date span6">
								<input type="text" name = 'start_date' class="span8 datepicker"  value="<?=date('d-m-Y')?>" id="dp1" >
								<span class="add-on"><i class="icon-calendar"></i></span>
								</div>	
							</div>
						</div>
						<div class="">
							<div class="span4 text-right"><?=Kohana::lang('backend.to')?><span class="red"> *</span>: </div>
							<div class="span8">
								<div class="input-append date span6">
								<input type="text" name = 'end_date' class="span8 datepicker"  value="<?=date('d-m-Y')?>" id="dp1" >
								<span class="add-on"><i class="icon-calendar"></i></span>
								</div>	
							</div>
						</div>
						
						
							<!--
							<div class="span4"><?=Kohana::lang('backend.from');?>:</div>
							<div class="span4"><?=Kohana::lang('backend.to');?>:</div>
							<div class="input-append date span4">
								<input type="text" name = 'start_date' class="span8 datepicker"  value="<?=date('d-m-Y')?>" id="dp1" >
								<span class="add-on"><i class="icon-calendar"></i></span>
							</div>	
							
							<div class="input-append date span4">
								<input type="text" name = 'end_date' class="span8 datepicker"  value="<?=date('d-m-Y')?>" id="dp1" >
								<span class="add-on"><i class="icon-calendar"></i></span>
							</div>	
							<div class="span3">
								<button class="btn  btn-info" type="submit" name="by_period" ><i class="icon-search icon-white"></i> <?=Kohana::lang('backend.view');?></button><br/>
							</div> -->
							
							
							
						<div class="">
							<div class="span4 text-right"><?=Kohana::lang('backend.output_format')?><span class="red"> *</span>: </div>
							<!--<div class="span8">
								<label class="checkbox inline">
								<input type="radio" name="display_options" value="grouped" checked="checked"> Grouped
								</label>
								<label class="checkbox inline">
								<input type="radio" name="display_options" value="timeline"> Timeline
								</label>
							
							</div>-->
							<div class="span8">
									<select name="display_options" id="inputType">
											<option value="grouped">Grouped</option>
											<option value="timeline">Timeline</option>
											
								</select>
								
							</div>
						</div>	
						<div class="clear"></div>
						<div class="form-actions">
							<div class="span4"></div>
							<div class="span8"><button class="btn  btn-info" type="submit" name="by_period" ><i class="icon-search icon-white"></i> <?=Kohana::lang('backend.view');?></button>
							</div>
						</div>
					</form>	
					
					<legend>Ranking (All buses, by income & expenses)</legend>						
					<form action="<?=url::site('buses/check_buses/')?>" method="POST" >
						
						<div class="">
							<div class="span4 text-right"><?=Kohana::lang('backend.from')?><span class="red"> *</span>: </div>
							<div class="span8">
								<div class="input-append date span6">
								<input type="text" name = 'start_date' class="span8 datepicker"  value="<?=date('d-m-Y')?>" id="dp1" >
								<span class="add-on"><i class="icon-calendar"></i></span>
								</div>	
							</div>
						</div>
						<div class="">
							<div class="span4 text-right"><?=Kohana::lang('backend.to')?><span class="red"> *</span>: </div>
							<div class="span8">
								<div class="input-append date span6">
								<input type="text" name = 'end_date' class="span8 datepicker"  value="<?=date('d-m-Y')?>" id="dp1" >
								<span class="add-on"><i class="icon-calendar"></i></span>
								</div>	
							</div>
						</div>
						
						<div class="clear"></div>
						<div class="form-actions">
							<div class="span4"></div>
							<div class="span8"><button class="btn  btn-info" type="submit" name="compare_buses" ><i class="icon-search icon-white"></i> <?=Kohana::lang('backend.view');?></button>
							</div>
						</div>
					</form>	
					
					
					
					
					
					<legend><?=Kohana::lang('backend.availability')?><!--<a href="#" class="view">See all activity</a>--></legend>						
						<form action="<?=url::site('buses/check_buses/')?>" method="POST" >
						<div class="">
							<div class="span4 text-right"><?=Kohana::lang('backend.bus_no')?><span class="red"> *</span>: </div>
							<div class="span8">
								<select name="bus_number" id="inputType">
								
									<?php 
										foreach ($all_buses as $bus){?>
										<option value="<?=$bus->bus_number?>"><?=$bus->bus_number?></option>
									<?php }; ?>
								
								</select>
							
							</div>
						</div>
					
						<div class="">
							<div class="span4 text-right"><?=Kohana::lang('backend.from')?><span class="red"> *</span>: </div>
							<div class="span8">
								<div class="input-append date span6">
								<input type="text" name = 'start_date' class="span8 datepicker"  value="<?=date('d-m-Y')?>" id="dp1" >
								<span class="add-on"><i class="icon-calendar"></i></span>
								</div>	
							</div>
						</div>
						<div class="">
							<div class="span4 text-right"><?=Kohana::lang('backend.to')?><span class="red"> *</span>: </div>
							<div class="span8">
								<div class="input-append date span6">
								<input type="text" name = 'end_date' class="span8 datepicker"  value="<?=date('d-m-Y')?>" id="dp1" >
								<span class="add-on"><i class="icon-calendar"></i></span>
								</div>	
							</div>
						</div>
						
						
						<div class="clear"></div>
						<div class="form-actions">
							<div class="span4"></div>
							<div class="span8"><button class="btn  btn-info" type="submit" name="inavailability" ><i class="icon-search icon-white"></i> <?=Kohana::lang('backend.view');?></button>
							</div>
						</div>
					</form>	
					
					
					
					
					
					
					
					
				</div>	
			
				
					
		
		
			<?php }?>
		
			  
		</div>
		
		</div>
	  <div class="clear"></div>

<script type="text/javascript">
	$(document).ready(function(){
			$('#timepicker').timepicker({
				minuteStep:30,
				//defaultTime: '8:00 PM'
			});
			
			
			$('.input-append.date').datepicker({
				format: "dd-mm-yyyy",
				todayBtn: "linked",
				autoclose: true,
				todayHighlight: true
			});
			
			
			/*	$(function () {
				$('#myTab a').click(function (e) {
					  e.preventDefault();
					  
					  $(this).tab('show');  
			})
			  }) */
			  
		
			  
			
	})
	</script>
