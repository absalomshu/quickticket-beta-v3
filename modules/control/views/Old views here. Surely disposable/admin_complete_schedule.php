<?php defined('SYSPATH') or die('No direct script access'); ?> 


	<!--
			For each input element, we check if it's a manager
			i.e. if it's the 'control' function. 
			If so, disable the input fields so they can't change anything. For now.		
		-->
	<?php //$manager = ((substr($_SERVER['PATH_INFO'],7,7))=='control') ? 1:0;
			//print_r($manager);exit;
	?>
 
	  <script>
  function preventBack(){window.history.forward();}
  setTimeout("preventBack()", 0);
  window.onunload=function(){null};
</script>


	  <div class="row-fluid marketing" style="margin:5px 0 0px 0px;">
	  
	  	<div class="span12 hero-unit prof-unit" >
		<div class="heading"><?=Kohana::lang('backend.sched_for')?> <?=$schedule->bus_number?> | <span class="badge badge-warning">Tickets sold: <?=$seats_taken?></span>
			<span style="float:right;font-size:12px;font-weight:normal;">
				<?php if ($schedule->loading == 0){ ?>
					<button class="btn plain"  onClick="startloading(<?=$schedule->id?>)" type="submit" id="start-loading" style="color:red;" >
					<i class="icon-bullhorn"></i><?=Kohana::lang('backend.start_loading')?>
					</button>
				<?php  ;} else {?>
					<button class="btn disabled"   type="submit" id="start-loading" style="height:30px; width:110px;font-size:12px;float:right;font-style:italic;color:green;" >
						<?=Kohana::lang('backend.loading')?>...
					</button>
				<?php } ?>
				<a href="<?=url::site('admin/edit_schedule').'/'.$schedule->id?>"> <?=Kohana::lang('backend.edit')?> </a> &nbsp;   |  &nbsp;  
			</span>
		</div>
			<div class="rule"><hr/></div>
			<div class="span2 spacious">
				<div class="control-group">
				<label class="control-label lab" for="inputType"><?=Kohana::lang('backend.from')?>:</label>
					<div class="controls ">
						<h3><?=get::town($schedule->from);?></h3>
					</div>
				</div>
			</div>
			<div class="span2 spacious">
				<div class="control-group">
				<label class="control-label lab" for="inputType"><?=Kohana::lang('backend.to')?>:</label>
				<div class="controls ">	
				<h3><?=get::town($schedule->to);?></h3>
				</div>
					</div>
				</div>
			<div class="span2 spacious">
				<label class="control-label lab" for="inputType"><?=Kohana::lang('backend.time')?>:</label>
					<div class="controls ">
					<h3><?=date("g:i A", strtotime($schedule->departure_time));?></h3>
					</div>
			</div>
			<div class="span2 spacious">
				<label class="control-label lab" for="inputType">Date:</label>
				<div class="controls ">
									<h3><?=date("d-m-Y",strtotime($schedule->departure_date));?></h3>
				</div>
			</div>
			
			<div class="span2 spacious" >
				<div class="control-group">
				<label class="control-label lab" for="inputType"><?=Kohana::lang('backend.bus_type')?>:</label>
						<div class="controls ">	
							<h3><?=$schedule->bus_seats?> <?=Kohana::lang('backend.seater')?></h3>
						</div>	

				</div>
			</div>
			
			<div class="span2 spacious">
				<label class="control-label lab" for="inputType"><?=Kohana::lang('backend.bus_no')?>:</label>
					<div class="controls ">
						<h3><?=$schedule->bus_number?></h3>
					</div>
			</div>
			<div class="span2 spacious">
				<label class="control-label lab" for="inputType"><?=Kohana::lang('backend.ticket_price')?> (FCFA):</label>
					<div class="controls ">	
						<h3><?=$schedule->ticket_price?>FCFA</h3>
					</div>
			</div>
			<div class="span2 spacious">
				<label class="control-label lab" for="inputType"><?=Kohana::lang('backend.done')?>?</label>
					<div class="controls ">	
						<a class="btn btn-large btn-success" href="#" onClick="checkout(<?=$schedule->id?>)"><i class="icon-road"></i> <?=Kohana::lang('backend.checkout')?></a>
						<!--<a href="<?php //url::site('admin/checkout_schedule/'.$schedule->id)?>" ><i class="icon-road"></i> Check out</a>-->
					</div>
			</div>
			
		
			
			  
		</div>
	 </div>	
		
		
		
	<div class="notice">
		<?php
			//What the user entered in his previous submission attempt, in case it was a failure
			$post = $this->session->get_once('post');
			//determine what type of notice to display if at all
			$notice = $this->session->get_once('notice');
				if(!empty($notice)){ 
					if($notice['type'] == 'success'){?><div class="alert general success"><a class="close" data-dismiss="alert" href="#">&times;</a><?php echo $notice['message']; ?></div><?php }?>
				<?	if($notice['type'] == 'error'){?><div class="alert general error"><a class="close" data-dismiss="alert" href="#">&times;</a><?php echo $notice['message'];?></div><?php }?>
		<?}?>
	</div>
		
				
			<div class="row-fluid marketing" style="margin:5px 0 0px 0;">	
					
				<div class="span8 hero-unit prof-unit" style="margin-left:0px;" >
					<form action="<?=url::site('admin/complete_schedule/'.$schedule->id)?>" method="POST">
						<?php no_csrf::set(1); ?>
					
							<ul class="nav nav-tabs" id="myTab">
							  <li class="active"><a href="#seat-tab"><?=Kohana::lang('backend.seats')?></a></li>
							  <li><a href="#reservation-tab">Reservations</a></li>
							  <?php 
							  //count and display the number of reservations
							  $res_count = count($client_reservations);
							  ?>
							  <!-- autoloaded by jquery, but still done here so it appears when page is loaded manually, then jquery takes over-->
							   <li><a href="#request-tab"><?=Kohana::lang('backend.requests')?> 
								<span class="res_count">
									<?php if($res_count != 0){ ?>
										<span class="notification"><?=$res_count?></span>
									<?php }?>
								</span></a></li>

							</ul>
							 
							<div class="tab-content">
							  <div class="tab-pane active" id="seat-tab">
								<div class="heading"><?=Kohana::lang('backend.checkout_seat')?><div class="view"><input type="checkbox" name="free_ticket"/>&nbsp;   Free ticket <span class="badge badge-warning">F</span></div></div>
								<div class="rule"><hr/></div>
											
								<div class="span3 spacious">
									<div class="control-group">
									<label class="control-label lab" for="inputType"  ><?=Kohana::lang('backend.client_name')?>:</label>
										<div class="controls ">
											<!--Putting required in the following input causes the reservations from clients not to be able to be accepted or rejected-->
											<input type="text" class="span12"  name="client_name" value="<?=@$post['client_name']?>"/>
										</div>
									</div>
								</div>
								<div class="span3 spacious">
									<div class="control-group">
									<label class="control-label lab" for="inputType"><?=Kohana::lang('backend.client_id')?>:</label>
										<div class="controls ">
											<input type="text" class="span12"   name="client_idc" value="<?=@$post['client_idc']?>">
										</div>
									</div>
								</div>
								<div class="span3 spacious">
									<div class="control-group">
									<label class="control-label lab" for="inputType"><?=Kohana::lang('backend.client_phone')?>:</label>
										<div class="controls ">
											<input type="text" class="span12"  name="client_phone" value="<?=@$post['client_phone']?>">
										</div>
									</div>
								</div>
								<div class="clear"></div>
								<div class="span3 spacious">
									<div class="control-group">
									<label class="control-label lab" for="inputType"><?=Kohana::lang('backend.client_email')?>:</label>
										<div class="controls ">
											<input  type="email" class="span12" name="client_email" value="<?=@$post['client_email']?>">
										</div>
									</div>
								</div>
								
								
								<div class="span3 spacious">
									<div class="control-group">
										<label class="control-label lab" for="inputType"><?=Kohana::lang('backend.spec_drop')?>  &nbsp;    <input type="checkbox" id="special_drop" name="spec_drop_check"/>   </label>
											
										<div class="controls"  id="special-drop-town" style="display:none;">
											<select name="special_drop_town" id="inputType" >
												<?php foreach ($towns as $town):?>
												<option value="<?=$town->id?>"><?=$town->name?></option>
												<?php endforeach;?>
											
										</select>
										</div>											
									</div>
								</div>
								<div class="span3 spacious " id="special-drop-price" style="display:none;">
									<div class="control-group">
										<label class="control-label lab" for="inputType"><?=Kohana::lang('backend.drop_price')?>:</label>
											<div>
												<input type="text"  class="span12" name="special_drop_price" placeholder="" value="<?=@$post['special_drop_price']?>"/>
											</div>
									</div>
								</div>
							
							
							
								<div class="clear"></div>
								
								
								
								<div class="span3 spacious" >
									<div class="control-group">
										<label class="control-label lab" for="inputType"><?=Kohana::lang('backend.seat')?>:</label>
										<div class="short-dropdown">
											<select name="seat_number" id="inputType">
											<?php 
											//array to hold all occupied seats for highlighting  on bus plan
											echo "<script> var jsar = new Array();</script>";
											$number=$schedule->bus_seats;
											
											//start from 2 since seat 1 is the driver
											$i=2;
											for($i; $i<= $number; $i++){
											//make sure the seat is not already occupied
											if(!isset($seats_and_occupants[$i])  AND !isset($seats_and_reservers[$i]) )
											{
											?>
												<option value="<?=$i?>"><?=$i?></option>
											<?php 
											}if(isset($seats_and_occupants[$i])) 
											{
												echo "<script> jsar[".$i."] = ".$i.";</script>";
											}
											//$i++;
											}
											?>
		
											</select>
										</div>
										
										<div >
											<button class="btn btn-info" type="submit" name="check_out_form"><i class="icon-check"></i> <?=Kohana::lang('backend.done')?></button><br/>
										</div>
																
									</div>
								
								</div>
								
								
								<!--<div class="span3" >
									<div class="control-group">
									<label class="control-label lab" for="inputType">&nbsp;</label>
										<div class="short-dropdown">
											<button class="btn btn-info" type="submit" name="check_out_form" style="height:30px;margin-top:-5px;" ><i class="icon-check"></i> Done</button><br/>

										</div>
									</div>
								</div>-->
							
						
						<div class="clear"></div>
						
							
						
						</form>
						

					 </div>
			  
			  
			  
			  
			  <div class="tab-pane" id="reservation-tab">

				<form action="<?=url::site('admin/complete_schedule/'.$schedule->id)?>" method="POST">
					<?php no_csrf::set(3); ?>
					
						<div class="heading"><?=Kohana::lang('backend.reserve_seat')?></div>
						
							
							<div class="span3 spacious">
								<div class="control-group">
								<label class="control-label lab" for="inputType"><?=Kohana::lang('backend.name')?>:</label>
									<div class="controls ">
										<input type="text" class="span12" name="res_name" value="<?=@$post['res_name']?>" required />
									</div>
								</div>
							</div>
							<div class="span3 spacious">
								<div class="control-group">
								<label class="control-label lab" for="inputType"><?=Kohana::lang('backend.phone')?>:</label>
									<div class="controls ">
										<input type="text" class="span12" name="res_phone" value="<?=@$post['res_phone']?>">
									</div>
								</div>
							</div>
							<div class="span3 spacious">
								<div class="control-group">
								<label class="control-label lab" for="inputType"><?=Kohana::lang('backend.email')?>:</label>
									<div class="controls ">
										<input type="text" class="span12" name="res_email" value="<?=@$post['res_email']?>">
									</div>
								</div>
							</div>
							<div class="span3 spacious">
									<div class="control-group">
									<label class="control-label lab" for="inputType"><?=Kohana::lang('backend.id_card')?>:</label>
										<div class="controls ">
											<input type="text" class="span12"  name="res_idc" value="<?=@$post['res_idc']?>">
										</div>
									</div>
								</div>
							
							<?php //print_r($seats_and_occupants);exit;?>
							<div class="span3 spacious" >
							<div class="control-group">
							<label class="control-label lab" for="inputType"><?=Kohana::lang('backend.seats_av')?>:</label>
								<div class="short-dropdown">
									<select name="res_seat" id="inputType">
									<?php $number=$schedule->bus_seats;
									$i=2;
									for($i; $i<= $number; $i){
									//make sure the seat is not already occupied and not reserved either.
									
									if(!isset($seats_and_occupants[$i])  AND !isset($seats_and_reservers[$i]) )
									{
									?>
										<option value="<?=$i?>"><?=$i?></option>
									<?php 
									}
									$i++;
									}
									?>
				
									</select>
								</div>
							</div>
						</div>
							<div class="span3 spacious">
								<div class="control-group">
								<label class="control-label lab" for="inputType">&nbsp;</label>
										<button type="submit" name="reserve_seat_form" class="btn btn-info"><?=Kohana::lang('backend.reserve')?></button>
								</div>
							</div>
								

					</form>	
				
				
				
			  </div>
				
			<div class="tab-pane" id="request-tab">
					
					<div class="heading"><?=Kohana::lang('backend.res_requests')?></div>
					 <!-- autoloaded by jquery, but still done here so it appears when page is loaded manually, then jquery takes over-->					
						<div id="res_requests">
						
							<?php if (!empty ($client_reservations)){				
								foreach($client_reservations as $seat => $details){					
							echo $details['name'] ." | ". $details['phone'] ." | Seat ". $seat."<br/>";
								}
								}else{ echo "No client reservation request for the moment.";} ?>
							
						</div>

			</div>			
					</div>
					
			</div>
					
				
					
			<script>
			  $(function () {
				$('#myTab a').click(function (e) {
			  e.preventDefault();
			  $(this).tab('show');
			})
			  })
			  
			</script>
					
						
					<div class="span4 hero-unit prof-unit" style="float:right;"  >				
							<div class="heading"><?=Kohana::lang('backend.bus_plan')?> <span class="badge badge-important" style="float:right;"><?=$seats_left?></span></div>
							<div class="rule"><hr/></div>
							<?php $number=$schedule->bus_seats;
					//check number of seats to know which bus plan to display
					if ($number == 70){include 'application/views/bus_plans/70_seater.php';}
					elseif ($number == 55){include 'application/views/bus_plans/55_seater.php';}
					elseif ($number == 39){include 'application/views/bus_plans/39_seater.php';}
					elseif ($number == 35){include 'application/views/bus_plans/35_seater.php';}
					elseif ($number == 30){include 'application/views/bus_plans/30_seater.php';}
							?>
						
							
					</div>
				
						
						<?php 
						
						//If there are occupants or reservers, show the table
						if (!empty($seats_and_occupants) OR !empty($seats_and_reservers))
						{?>
							
						<div class="span8 hero-unit prof-unit" style="margin-left:0px; " >
	
							
								<div class="control-group" >
									<label class="control-label lab heading" for="inputType"  >
									BORDEREAU
									
									</label>
									
									
									<?php 
									//combine reserved and taken
									//COMBINING INEFFICIENT AS IT IS A 2D AND A 3D
									//$all_seats = $seats_and_reservers + $seats_and_occupants;
									//ksort($all_seats);
									//foreach($seats_and_reservers as $key=>$subkey){print_r($subkey['name']);echo"<br/>";}?>
										
								</div>
								<div class="rule"><hr/></div>
							
						
						<form action="<?=url::site('admin/complete_schedule/'.$schedule->id)?>" method="POST">
						<?php no_csrf::set(2); ?>
						<table class="table table-striped table-hover table-condensed">
										<tr>
												<th><?=Kohana::lang('backend.seat')?></th>
												<th>Name</th>
												<th>IDC No.</th>
												<th>Phone</th>
												<th><?=Kohana::lang('backend.state')?></th>
												<th>Actions</th>
										</tr>
										<?php 
										//sort the array in ascending order
										ksort($seats_and_occupants);
										foreach ($seats_and_occupants as $key=>$value)
										{ ?>
												<tr>
													<td><?=$key?></td>
													<td><?=$value['name']?></td>
													<td><?=$value['idc']?></td>
													<td><?=$value['phone']?></td>
													<td><?php if(isset($value['sd_price'])){echo "*SD-". get::town($value['sd_town']);}?></td>
													<?php
													$serial1 = serialize($seats_and_occupants);
													?>
													<input type="hidden" value="<?php echo htmlentities($serial1)?>" name="arr1"/>										
													<td>
														<button type="submit" class="" name="print_single_ticket" value="<?=$key?>" title="Print this ticket"><i class="icon-print"></i></button>
														<button type="submit" class="trash" name="empty_seat" value="<?=$key?>" onClick="return confirm('<?=Kohana::lang('backend.confirm_emptyseat')?>?');" title="Empty this seat"><i class="icon-trash"></i></button>
													
													</td>
												</tr>
											
											
										<?php } ;?><?php 
										//sort the array in ascending order
										ksort($seats_and_reservers); 
										//serialize so it can be posted
										$serial = serialize($seats_and_reservers); ?>
													
										<input type="hidden" value="<?php echo htmlentities($serial)?>" name="arr"/>
										
										<?php
										foreach ($seats_and_reservers as $r_key=>$value)
										{ ?>
												<tr>
													<td><?=$r_key?></td>
													<td><?=$value['name']?></td>
													<td><?=$value['idc']?></td>
													<td><?=$value['phone']?></td>
													
														<td><?=Kohana::lang('backend.reserved')?> </td> 
														<td>
														<button type="submit" id="accept"name="res_to_occ" value="<?=$r_key?>" title="Confirm"></button> | 
														<button type="submit" id="reject" name="reject_reserve" value="<?=$r_key?>" onClick="return confirm('<?=Kohana::lang('backend.refuse_reserve')?>?');" title="Delete"></button>
														
														</td>
												</tr>
											
										<?php } ;?>
									</table>
								</form>
						</div>
					 <?php }?>	
					
					<span class="loaddiv">
						<!-- DON'T DELETE THIS DIV. IT'S FOR THE ONLINE TRANSACTIONS-->	
					</span>
					
					</div>
	  

	  <div style="height:19px;"></div>
	  
	<script type="text/javascript"> 
		$(document).ready(function(){
			$('#start-loading').click(function(){
				
			});
			
			$('#special_drop').change(function(){
				if(this.checked){
					$('#special-drop-price').show(150);					
					$('#special-drop-town').show(150);					
					}else{
					$('#special-drop-price').hide(150);	
					$('#special-drop-town').hide(150);					
					}
			
			});
			
		});
		
		function startloading(schedule_id){
			var domain = 'http://localhost/quickticket/';
			//alert(schedule_id);
			$.ajax ({
				type: 'POST',
				url: domain+'admin/start_loading/'+schedule_id,
				data: 'schedule_id='+schedule_id,
				success: function()
				{
				$('#start-loading').css({"color":"green","font-style":"italic"}).addClass('disabled').html('Loading...');
				//$("#return").html(' '+html+'');
				
				}
			
			});		
		}



	</script>
	<script type="text/javascript">
	$(document).ready(function(){
		$('.datepicker').datepicker();
		
			setInterval(function(){
			//$('#res_requests').empty();
			$('#res_requests').load('<?=url::site('reserve/reservation_details/'.$schedule->id);?>');
			$('.res_count').load('<?=url::site('reserve/reservation_count/'.$schedule->id);?>');
			//$('.loaddiv').load('<?=url::site('online/index');?>');
			}, 60000);
		
		
	})
	function checkout(schedule_id)
	{	
		var domain = '<?=url::base()?>';
		var answer = confirm("<?=Kohana::lang('backend.confirm_checkout')?>");
		if (answer == true)
		{
			window.location.href=domain+'admin/checkout_schedule/'+schedule_id;
		}
		else{
			return;}
		
	};
	$(document).ready(function(){
	
	});




</script>
<script type="text/javascript">
	$(document).ready(function(){
		var number_seats = "<?php echo $number; ?>";
		for (i=0; i <= number_seats; i++){
		if( (i!=1) && typeof jsar[i] != 'undefined'  ){
			$("td.plan:eq("+(i-1)+")").addClass('taken-seat');
			}
			} 		
	})
</script>
  

