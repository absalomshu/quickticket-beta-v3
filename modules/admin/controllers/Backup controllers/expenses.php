<?php defined('SYSPATH') or die('No direct script access');

//Almost all functions will take as parameter, the id of the agency we are dealing with
//in order not to interfere with another's stuff.

class Expenses_Controller extends Admin_Controller {

	//public $template = 'template/admin_template';

	public function __construct()
	{	
		parent::__construct();
		$this->admin = Authlite::instance()->get_admin();
		$this->session = Session::instance();
		//if (!empty($this->admin)) $this->key = $this->admin->agency_id;
		
	}  

	
	public function index()
	{	
		//ensure a logged in user can't land on the signup page
		if (empty($this->admin)) 
		{
			url::redirect('admin/login');
		}
		$this->all();
	}
	
	
	public function all()
	{
		Authlite::check_admin();
		Authlite::verify_referer();
		$agency_id = $this->agency_id;
		$parent_id = get::_parent($agency_id)->id;
		$admin_group_id = $this->admin->admin_group_id;
		$this->template->title=Kohana::lang('backend.register_expense');
		//Authlite::check_agency_integrity($agency_id);
		$agency_name = get::agency_name($agency_id);
		$expense = ORM::FACTORY('expense');		
		
		$all_buses = get::all_buses($agency_id, $parent_id, $admin_group_id);
			
		//pagination
		$per_page = 15;
		
		$all_expenses = get::all_expenses($agency_id, $parent_id,$admin_group_id);
				
		$all_expenses_total = count($all_expenses);
		
		$this->pagination = new Pagination(array(
			// 'base_url'    => 'welcome/pagination_example/page/', // base_url will default to current uri
			'uri_segment'    => 'page', // pass a string as uri_segment to trigger former 'label' functionality
			'total_items'    => $all_expenses_total, // use db count query here of course
			'items_per_page' => $per_page , // it may be handy to set defaults for stuff like this in config/pagination.php
			'style'          => 'punbb', // pick one from: classic (default), digg, extended, punbb, or add your own!
			//'query_string'   => "ORM::factory('schedule')->where('agency_id',$agency_id)->where('status','current')->find_all()"
		));
			
		$all_expenses=ORM::factory('expense')->where('agency_id',$agency_id)->where('deleted','0')->orderby('CreatedOn','DESC')->limit($per_page,$this->pagination->sql_offset)->find_all();

		$view = new View('admin_expenses');
		$view->all_buses = $all_buses;
		$view->all_expenses = $all_expenses;
		$this->template->content = $view;
			
	}
	
	
	public function register_expense()
	{	
		Authlite::check_admin();
		Authlite::verify_referer();
		
		$agency_id = $this->agency_id;
		$parent_id = get::_parent($agency_id)->id;
		$admin_group_id = $this->admin->admin_group_id;
		$this->template->title=Kohana::lang('backend.register_expense');
		//Authlite::check_agency_integrity($agency_id);
		$agency_name = get::agency_name($agency_id);
		
		$all_buses = get::all_buses($agency_id, $parent_id, $admin_group_id);
	
		if($_POST)
		{	
			//no_csrf::check();
			
			$post = new Validation($_POST);
			$post->add_rules('purpose','required');
			$post->add_rules('date_incurred','required');
			$post->add_rules('amount','required','valid::numeric');
			//$post->add_rules('authorised_by','required');
						
			if ($post->validate())
			{	
				$bus_number = strtoupper(str_replace(" ","",$_POST['bus_number']));
				$amount = $_POST['amount'];
				$purpose = htmlentities($_POST['purpose'],ENT_QUOTES);
				$date_incurred = date("Y-m-d",strtotime($_POST['date_incurred']));
				$authorised_by = $_POST['authorised_by'];
				$CreatedBy = strtoupper($this->admin->username);			
			
				Expense_Model::register_expense($this->agency_id, $bus_number, $amount, $purpose, $date_incurred, $CreatedBy, $authorised_by);
					
				$this->session->set('notice', array('message'=>Kohana::lang('backend.expense_added') ,'type'=>'success'));
				url::redirect('expenses');	
							
			}
			else
			{
				$errors=$post->errors('errors');
				$notice="";
					foreach($errors as $error) 
					{
						$notice.=$error."<br />";
					}
					$this->session->set('notice', array('message'=>$notice,'type'=>'error'));
					url::redirect('expenses');	
			}
		}
		
		$view = new View('admin_expenses');
		$view->all_buses = $all_buses;
		$view->all_expenses = $all_expenses;
		$this->template->content = $view;
	
	}	
	
	
	public function quick_add_expense()
	{	
	
		Authlite::check_admin();
		Authlite::verify_referer();
		
		$agency_id = $this->agency_id;
		$parent_id = get::_parent($agency_id)->id;
		$agency_name = get::agency_name($agency_id);
		//$all_buses = get::all_buses($agency_id, $parent_id, $admin_group_id);
		
	
		if($_POST)
		{	
			$post = new Validation($_POST);
			$post->add_rules('exp_purpose','required');
			$post->add_rules('exp_amount','required','valid::numeric');
			$sched_id = $_POST['sched_id'];
			
			if ($post->validate())
			{	
				//Get bus number from hidden form field
				$bus_number = strtoupper(str_replace(" ","",$_POST['bus_number']));
				
				//Always remove commas and . from amounts
				//$amount = str_replace(array('.', ','), '' , $_POST['exp_amount']);
				$amount = $_POST['exp_amount'];
				$purpose = htmlentities($_POST['exp_purpose'],ENT_QUOTES);
				$date_incurred = date("Y-m-d");
				$CreatedBy = strtoupper($this->admin->username);			
				
				Expense_Model::register_expense($this->agency_id, $bus_number, $amount, $purpose, $date_incurred, $CreatedBy, $authorised_by='', $sched_id);
					
				$this->session->set('notice', array('message'=>Kohana::lang('backend.expense_added') ,'type'=>'success'));
				//go back to the page we came from
				//url::redirect('expenses');
				url::redirect('admin/complete_schedule/'.$sched_id);
							
			}
			else
			{
				$errors=$post->errors('errors');
				$notice="";
					foreach($errors as $error) 
					{
						$notice.=$error."<br />";
					}
					$this->session->set('notice', array('message'=>$notice,'type'=>'error'));
					url::redirect('admin/complete_schedule/'.$sched_id);	
					
			}
		}
	
	}
	
	
	public function details($expense_id)
		{	
			Authlite::check_admin();
			Authlite::verify_referer();
			$this->template->title = '';
			$expense = get::expense($expense_id);
			
			
			$view = new View('admin_expense_details');
			$view->expense = $expense;
			$this->template->content = $view;
		
		}
		
		
	public function check_expenses()
	{	
		Authlite::check_admin();
		Authlite::verify_referer();
		$agency_id = $this->agency_id;
		$parent_id = get::_parent($agency_id)->id;
		$admin_group = $this->admin->admin_group_id;
		$this->template->title = Kohana::lang('backend.check_expense');
		//$expense = get::expense($expense_id);
		
		$all_buses = get::all_buses($agency_id, $parent_id, $admin_group);
		
		$all_expenses = get::all_expenses($agency_id, $parent_id, $admin_group);
		
		if($_POST)
		{	
			//no_csrf::check();
			$post = new Validation($_POST);
			//bus number is whichever comes from the form, either the existing or the one to be searched
			//if it's the search, then it shouldn't be empty
			if(isset($_POST['search_bus']))
			{	
				$post->add_rules('search_bus_number','required','valid::alpha_numeric');
				$bus_number = strtoupper(str_replace(" ","",$_POST['search_bus_number']));
			}elseif(isset($_POST['existing_bus']))
			{	
				if(empty($_POST['existing_bus_number']))
				{
					$this->session->set('notice', array('message'=>Kohana::lang('errors.search_bus_number.default'),'type'=>'error'));
					url::redirect('expenses/all');
				}
				$post->add_rules('existing_bus_number','required');
				$bus_number = $_POST['existing_bus_number'];
			}elseif(isset($_POST['by_date']))
			{	
				$post->add_rules('date','required');
				$date = date("Y-m-d",strtotime($_POST['date']));	
			}
		
			if ($post->validate())
			{		
				//if it's by date
				if(isset($_POST['by_date'])) 
				{
					$agency_expenses_by_date = get::agency_expenses_by_date($agency_id,$date);
					$count_expenses = count($agency_expenses_by_date);
				}
				else
				{
					$bus_expenses = get::expenses_for_bus($bus_number);	
					$count_expenses = count($bus_expenses);
				}
				if($count_expenses == 0)
				{
					$this->session->set('notice', array('message'=>Kohana::lang('backend.no_expense'),'type'=>'error'));
				} 
			}
			else
			{
				$errors=$post->errors('errors');
				$notice="";
				foreach($errors as $error) 
				{
					$notice.=$error."<br />";
				}
				$this->session->set('notice', array('message'=>$notice,'type'=>'error'));
				url::redirect('expenses');	
			}
		}
			
		$view = new View('admin_check_expenses');
		//if there was a search, pass the results to the view.
		//Use @ bc either $bus_expenses or @agency_expenses_by_date is set at once. Not both.
		$view->date = @$date;
		$view->bus_expenses = @$bus_expenses;
		$view->agency_expenses_by_date = @$agency_expenses_by_date;
		$view->bus_number = @$bus_number;
		$view->count_expenses = $count_expenses;
		$view->all_buses = $all_buses;
		$view->all_expenses = $all_expenses;
		$this->template->content = $view;
	}
		
		
	public function expenses_by_date()
	{	
		Authlite::check_admin();
		Authlite::verify_referer();
		
		$agency_id = $this->agency_id;
		$parent_id = get::_parent($agency_id)->id;
		$type = $this->admin->admin_group_id;
		$all_expenses = get::all_expenses($agency_id, $parent_id, $admin_group);
		//Authlite::check_agency_integrity($agency_id);
		$this->template->title = Kohana::lang('backend.welcome_backend');
		
		if($_POST)
		{
			$post = new Validation($_POST);
			$post->add_rules('date','required');
			if ($post->validate())
			{	
				$date = $_POST['date'];
				//$schedules = ORM::FACTORY('schedule')->where('agency_id',$agency_id)->where('departure_date',$date)->find_all();
				$expenses = get::agency_expenses_by_date($agency_id,$date);		

			}
			else
			{
				$errors=$post->errors('errors');
				$notice="";
				foreach($errors as $error) 
				{
					$notice.=$error."<br />";
				}
				$this->session->set('notice', array('message'=>$notice,'type'=>'error'));
				url::redirect('admin/main/'.$this->admin->agency_id);
			}
		}
		$view = new View('admin_expenses');
		$view->expenses = $expenses;
		$view->all_expenses = $all_expenses;
		//set a variable that informs that the search was by date
		$view->expenses_by_date = 1;
		$view->date = $date;
			
		$this->template->content = $view;
	
	}
		
	
		
		
		}