-- phpMyAdmin SQL Dump
-- version 3.4.10.1
-- http://www.phpmyadmin.net
--
-- Host: localhost
-- Generation Time: Jan 09, 2017 at 02:55 AM
-- Server version: 5.5.20
-- PHP Version: 5.3.10

SET SQL_MODE="NO_AUTO_VALUE_ON_ZERO";
SET time_zone = "+00:00";


/*!40101 SET @OLD_CHARACTER_SET_CLIENT=@@CHARACTER_SET_CLIENT */;
/*!40101 SET @OLD_CHARACTER_SET_RESULTS=@@CHARACTER_SET_RESULTS */;
/*!40101 SET @OLD_COLLATION_CONNECTION=@@COLLATION_CONNECTION */;
/*!40101 SET NAMES utf8 */;

--
-- Database: `quickticket`
--

-- --------------------------------------------------------

--
-- Table structure for table `admins`
--

CREATE TABLE IF NOT EXISTS `admins` (
  `id` int(12) unsigned NOT NULL AUTO_INCREMENT,
  `username` varchar(50) NOT NULL,
  `password` varchar(255) NOT NULL,
  `admin_group_id` tinyint(4) NOT NULL,
  `agency_id` int(12) NOT NULL,
  `available_balance` int(11) NOT NULL,
  `LanguageID` enum('en','fr') NOT NULL DEFAULT 'en',
  `CreatedBy` varchar(25) NOT NULL,
  `CreatedOn` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP,
  `Deleted` enum('0','1') NOT NULL DEFAULT '0',
  `DeletedBy` varchar(25) NOT NULL,
  PRIMARY KEY (`id`),
  KEY `agency_id` (`agency_id`),
  KEY `admin_group_id` (`admin_group_id`),
  KEY `username` (`username`)
) ENGINE=InnoDB  DEFAULT CHARSET=latin1 AUTO_INCREMENT=55 ;

--
-- Dumping data for table `admins`
--

INSERT INTO `admins` (`id`, `username`, `password`, `admin_group_id`, `agency_id`, `available_balance`, `LanguageID`, `CreatedBy`, `CreatedOn`, `Deleted`, `DeletedBy`) VALUES
(32, 'absa', '226c096e795854eb48bd226b9cde2f7bae2ba106', 2, 105, 310695, 'en', '', '2015-02-23 07:13:39', '0', ''),
(50, 'acha', 'bbb30c4842e3cffd532ba27c0d1dafde01111286', 2, 106, 7200, 'en', '', '2015-05-13 12:44:11', '0', ''),
(51, 'nelsonacha', 'f4145704995aec437875f75937cf88770e2bd1fc', 2, 104, 0, 'en', '', '2015-05-13 12:44:49', '0', ''),
(52, 'admin', '226c096e795854eb48bd226b9cde2f7bae2ba106', 2, 105, 5000, 'en', 'absa', '2015-06-15 12:08:13', '0', ''),
(53, 'musbueabm', 'dc853ce4d49132427d6baddfdf8f8285e74c5841', 2, 102, 0, 'en', '', '2015-09-09 15:48:57', '0', ''),
(54, 'noparcel', '226c096e795854eb48bd226b9cde2f7bae2ba106', 2, 105, 0, 'en', 'absa', '2016-10-06 11:48:48', '0', '');

-- --------------------------------------------------------

--
-- Table structure for table `admin_dashboard_items`
--

CREATE TABLE IF NOT EXISTS `admin_dashboard_items` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `AdminID` varchar(50) NOT NULL,
  `DashboardItemID` int(11) NOT NULL,
  `IsAllowed` enum('0','1') NOT NULL DEFAULT '1',
  `RestrictedTownID` int(11) NOT NULL,
  `Parameter1` int(11) NOT NULL,
  `Parameter2` int(11) NOT NULL,
  PRIMARY KEY (`id`),
  KEY `DashboardItemID` (`DashboardItemID`)
) ENGINE=InnoDB  DEFAULT CHARSET=latin1 AUTO_INCREMENT=137 ;

--
-- Dumping data for table `admin_dashboard_items`
--

INSERT INTO `admin_dashboard_items` (`id`, `AdminID`, `DashboardItemID`, `IsAllowed`, `RestrictedTownID`, `Parameter1`, `Parameter2`) VALUES
(5, 'sfsf', 1, '1', 0, 0, 0),
(6, 'sfsf', 2, '1', 0, 0, 0),
(7, 'sfsf', 3, '0', 0, 0, 0),
(8, 'sfsf', 4, '0', 0, 0, 0),
(9, 'fff', 1, '1', 0, 0, 0),
(10, 'fff', 2, '1', 0, 0, 0),
(11, 'fff', 3, '0', 0, 0, 0),
(12, 'fff', 4, '1', 0, 0, 0),
(17, 'absa2', 1, '0', 5, 0, 0),
(18, 'absa2', 2, '0', 5, 0, 0),
(19, 'absa2', 3, '1', 5, 0, 0),
(20, 'absa2', 4, '1', 5, 0, 0),
(21, 'ngwa', 1, '1', 0, 0, 0),
(22, 'ngwa', 2, '1', 0, 0, 0),
(23, 'ngwa', 3, '1', 0, 0, 0),
(24, 'ngwa', 4, '1', 0, 0, 0),
(25, 'paul', 1, '1', 0, 0, 0),
(26, 'paul', 2, '1', 0, 0, 0),
(27, 'paul', 3, '1', 0, 0, 0),
(28, 'paul', 4, '1', 0, 0, 0),
(29, 'shuabunka', 1, '1', 0, 0, 0),
(30, 'shuabunka', 2, '1', 0, 0, 0),
(31, 'shuabunka', 3, '0', 0, 0, 0),
(32, 'shuabunka', 4, '1', 0, 0, 0),
(33, 'absa', 1, '1', 0, 0, 0),
(34, 'absa', 2, '1', 0, 0, 0),
(35, 'absa', 3, '0', 0, 0, 0),
(36, 'absa', 4, '1', 0, 0, 0),
(37, 'grimba', 1, '1', 0, 0, 0),
(38, 'grimba', 2, '1', 0, 0, 0),
(39, 'grimba', 3, '0', 0, 0, 0),
(40, 'grimba', 4, '1', 0, 0, 0),
(41, 'grimban', 1, '0', 4, 0, 0),
(42, 'grimban', 2, '0', 4, 0, 0),
(43, 'grimban', 3, '1', 4, 0, 0),
(44, 'grimban', 4, '0', 4, 0, 0),
(45, 'nelson', 1, '1', 0, 0, 0),
(46, 'nelson', 2, '1', 0, 0, 0),
(47, 'nelson', 3, '0', 0, 0, 0),
(48, 'nelson', 4, '1', 0, 0, 0),
(49, 'acha', 1, '1', 17, 0, 0),
(50, 'acha', 2, '0', 17, 0, 0),
(51, 'acha', 3, '1', 17, 0, 0),
(52, 'acha', 4, '1', 17, 0, 0),
(53, 'badmin', 1, '1', 0, 0, 0),
(54, 'badmin', 2, '1', 0, 0, 0),
(55, 'badmin', 3, '0', 0, 0, 0),
(56, 'badmin', 4, '1', 0, 0, 0),
(57, 'badmin1', 1, '1', 0, 0, 0),
(58, 'badmin1', 2, '1', 0, 0, 0),
(59, 'badmin1', 3, '0', 0, 0, 0),
(60, 'badmin1', 4, '1', 0, 0, 0),
(61, 'eadmin', 1, '1', 0, 0, 0),
(62, 'eadmin', 2, '1', 0, 0, 0),
(63, 'eadmin', 3, '0', 0, 0, 0),
(64, 'eadmin', 4, '1', 0, 0, 0),
(65, 'eadmin1', 1, '1', 0, 0, 0),
(66, 'eadmin1', 2, '1', 0, 0, 0),
(67, 'eadmin1', 3, '0', 0, 0, 0),
(68, 'eadmin1', 4, '1', 0, 0, 0),
(69, 'badmin1', 1, '1', 0, 0, 0),
(70, 'badmin1', 2, '1', 0, 0, 0),
(71, 'badmin1', 3, '0', 0, 0, 0),
(72, 'badmin1', 4, '1', 0, 0, 0),
(73, 'shu', 1, '0', 29, 0, 0),
(74, 'shu', 2, '0', 29, 0, 0),
(75, 'shu', 3, '1', 29, 0, 0),
(76, 'shu', 4, '1', 29, 0, 0),
(77, 'grim', 1, '1', 0, 0, 0),
(78, 'grim', 2, '1', 0, 0, 0),
(79, 'grim', 3, '0', 0, 0, 0),
(80, 'grim', 4, '1', 0, 0, 0),
(81, 'mus1', 1, '1', 0, 0, 0),
(82, 'mus1', 2, '1', 0, 0, 0),
(83, 'mus1', 3, '0', 0, 0, 0),
(84, 'mus1', 4, '1', 0, 0, 0),
(85, 'mus13', 1, '1', 0, 0, 0),
(86, 'mus13', 2, '1', 0, 0, 0),
(87, 'mus13', 3, '0', 0, 0, 0),
(88, 'mus13', 4, '1', 0, 0, 0),
(89, 'mus2', 1, '1', 0, 0, 0),
(90, 'mus2', 2, '1', 0, 0, 0),
(91, 'mus2', 3, '1', 0, 0, 0),
(92, 'mus2', 4, '1', 0, 0, 0),
(93, 'absalom', 1, '1', 0, 0, 0),
(94, 'absalom', 2, '1', 0, 0, 0),
(95, 'absalom', 3, '0', 0, 0, 0),
(96, 'absalom', 4, '1', 0, 0, 0),
(97, 'nelson', 1, '1', 0, 0, 0),
(98, 'nelson', 2, '1', 0, 0, 0),
(99, 'nelson', 3, '0', 0, 0, 0),
(100, 'nelson', 4, '1', 0, 0, 0),
(101, 'acha', 1, '1', 0, 0, 0),
(102, 'acha', 2, '1', 0, 0, 0),
(103, 'acha', 3, '0', 0, 0, 0),
(104, 'acha', 4, '1', 0, 0, 0),
(105, 'nelsonacha', 1, '1', 0, 0, 0),
(106, 'nelsonacha', 2, '1', 0, 0, 0),
(107, 'nelsonacha', 3, '0', 0, 0, 0),
(108, 'nelsonacha', 4, '1', 0, 0, 0),
(109, 'admin', 1, '1', 0, 0, 0),
(110, 'admin', 2, '1', 0, 0, 0),
(111, 'admin', 3, '0', 0, 0, 0),
(112, 'admin', 4, '0', 0, 0, 0),
(113, 'ngwa', 1, '1', 0, 0, 0),
(114, 'ngwa', 2, '1', 0, 0, 0),
(115, 'ngwa', 3, '0', 0, 0, 0),
(116, 'ngwa', 4, '1', 0, 0, 0),
(117, 'voyageur1', 1, '1', 0, 0, 0),
(118, 'voyageur1', 2, '1', 0, 0, 0),
(119, 'voyageur1', 3, '0', 0, 0, 0),
(120, 'voyageur1', 4, '1', 0, 0, 0),
(121, 'voyageur2', 1, '1', 0, 0, 0),
(122, 'voyageur2', 2, '1', 0, 0, 0),
(123, 'voyageur2', 3, '0', 0, 0, 0),
(124, 'voyageur2', 4, '1', 0, 0, 0),
(125, 'jeanne', 1, '1', 0, 0, 0),
(126, 'jeanne', 2, '1', 0, 0, 0),
(127, 'jeanne', 3, '0', 0, 0, 0),
(128, 'jeanne', 4, '1', 0, 0, 0),
(129, 'musbueabm', 1, '1', 0, 0, 0),
(130, 'musbueabm', 2, '1', 0, 0, 0),
(131, 'musbueabm', 3, '0', 0, 0, 0),
(132, 'musbueabm', 4, '1', 0, 0, 0),
(133, 'noparcel', 1, '1', 0, 0, 0),
(134, 'noparcel', 2, '1', 0, 0, 0),
(135, 'noparcel', 3, '0', 0, 0, 0),
(136, 'noparcel', 4, '1', 0, 0, 0);

-- --------------------------------------------------------

--
-- Table structure for table `admin_groups`
--

CREATE TABLE IF NOT EXISTS `admin_groups` (
  `id` tinyint(11) NOT NULL AUTO_INCREMENT,
  `role` varchar(20) NOT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB  DEFAULT CHARSET=latin1 AUTO_INCREMENT=5 ;

--
-- Dumping data for table `admin_groups`
--

INSERT INTO `admin_groups` (`id`, `role`) VALUES
(1, 'Parcels Only'),
(2, 'Administrator'),
(3, 'Branch Manager'),
(4, 'General Manager');

-- --------------------------------------------------------

--
-- Table structure for table `admin_modules`
--

CREATE TABLE IF NOT EXISTS `admin_modules` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `AdminID` varchar(255) NOT NULL,
  `MainModuleID` int(11) NOT NULL,
  `IsAllowed` enum('0','1') NOT NULL DEFAULT '0',
  `AllowAdd` enum('0','1') NOT NULL DEFAULT '0',
  `AllowEdit` enum('0','1') NOT NULL DEFAULT '0',
  `AllowDelete` enum('0','1') NOT NULL DEFAULT '0',
  `ManageMails` enum('0','1') NOT NULL DEFAULT '1',
  `ManageBaggage` enum('0','1') NOT NULL DEFAULT '1',
  PRIMARY KEY (`id`),
  KEY `AdminID` (`AdminID`),
  KEY `MainModuleID` (`MainModuleID`)
) ENGINE=InnoDB  DEFAULT CHARSET=latin1 AUTO_INCREMENT=313 ;

--
-- Dumping data for table `admin_modules`
--

INSERT INTO `admin_modules` (`id`, `AdminID`, `MainModuleID`, `IsAllowed`, `AllowAdd`, `AllowEdit`, `AllowDelete`, `ManageMails`, `ManageBaggage`) VALUES
(25, 'absa', 1, '1', '0', '0', '0', '1', '1'),
(26, 'absa', 2, '1', '0', '0', '0', '1', '1'),
(27, 'absa', 3, '1', '0', '0', '0', '1', '1'),
(28, 'absa', 4, '1', '0', '0', '0', '1', '1'),
(29, 'absa', 5, '1', '0', '0', '0', '1', '1'),
(30, 'absa', 6, '1', '0', '0', '0', '1', '1'),
(31, 'absa', 7, '1', '0', '0', '0', '1', '1'),
(32, 'absa', 8, '1', '0', '0', '0', '1', '1'),
(113, 'absa', 9, '1', '0', '0', '0', '1', '1'),
(223, 'nelson', 1, '1', '0', '0', '0', '1', '1'),
(224, 'nelson', 2, '1', '0', '0', '0', '1', '1'),
(225, 'nelson', 3, '1', '0', '0', '0', '1', '1'),
(226, 'nelson', 4, '1', '0', '0', '0', '1', '1'),
(227, 'nelson', 5, '1', '0', '0', '0', '1', '1'),
(228, 'nelson', 6, '1', '0', '0', '0', '1', '1'),
(229, 'nelson', 7, '1', '0', '0', '0', '1', '1'),
(230, 'nelson', 8, '1', '0', '0', '0', '1', '1'),
(231, 'nelson', 9, '1', '0', '0', '0', '1', '1'),
(232, 'acha', 1, '1', '0', '0', '0', '1', '1'),
(233, 'acha', 2, '1', '0', '0', '0', '1', '1'),
(234, 'acha', 3, '1', '0', '0', '0', '1', '1'),
(235, 'acha', 4, '1', '0', '0', '0', '1', '1'),
(236, 'acha', 5, '1', '0', '0', '0', '1', '1'),
(237, 'acha', 6, '1', '0', '0', '0', '1', '1'),
(238, 'acha', 7, '1', '0', '0', '0', '1', '1'),
(239, 'acha', 8, '1', '0', '0', '0', '1', '1'),
(240, 'acha', 9, '1', '0', '0', '0', '1', '1'),
(241, 'nelsonacha', 1, '1', '0', '0', '0', '1', '1'),
(242, 'nelsonacha', 2, '1', '0', '0', '0', '1', '1'),
(243, 'nelsonacha', 3, '1', '0', '0', '0', '1', '1'),
(244, 'nelsonacha', 4, '1', '0', '0', '0', '1', '1'),
(245, 'nelsonacha', 5, '1', '0', '0', '0', '1', '1'),
(246, 'nelsonacha', 6, '1', '0', '0', '0', '1', '1'),
(247, 'nelsonacha', 7, '1', '0', '0', '0', '1', '1'),
(248, 'nelsonacha', 8, '1', '0', '0', '0', '1', '1'),
(249, 'nelsonacha', 9, '1', '0', '0', '0', '1', '1'),
(250, 'admin', 1, '1', '0', '0', '0', '1', '1'),
(251, 'admin', 2, '1', '0', '0', '0', '1', '1'),
(252, 'admin', 3, '1', '0', '0', '0', '1', '1'),
(253, 'admin', 4, '1', '0', '0', '0', '1', '1'),
(254, 'admin', 5, '1', '0', '0', '0', '1', '1'),
(255, 'admin', 6, '1', '0', '0', '0', '1', '1'),
(256, 'admin', 7, '1', '0', '0', '0', '1', '1'),
(257, 'admin', 8, '1', '0', '0', '0', '1', '1'),
(258, 'admin', 9, '1', '0', '0', '0', '1', '1'),
(259, 'ngwa', 1, '1', '0', '0', '0', '1', '1'),
(260, 'ngwa', 2, '0', '0', '0', '0', '1', '1'),
(261, 'ngwa', 3, '0', '0', '0', '0', '1', '1'),
(262, 'ngwa', 4, '0', '0', '0', '0', '1', '1'),
(263, 'ngwa', 5, '0', '0', '0', '0', '1', '1'),
(264, 'ngwa', 6, '0', '0', '0', '0', '1', '1'),
(265, 'ngwa', 7, '0', '0', '0', '0', '1', '1'),
(266, 'ngwa', 8, '1', '0', '0', '0', '1', '1'),
(267, 'ngwa', 9, '0', '0', '0', '0', '1', '1'),
(268, 'voyageur1', 1, '1', '0', '0', '0', '1', '1'),
(269, 'voyageur1', 2, '1', '0', '0', '0', '1', '1'),
(270, 'voyageur1', 3, '1', '0', '0', '0', '1', '1'),
(271, 'voyageur1', 4, '1', '0', '0', '0', '1', '1'),
(272, 'voyageur1', 5, '1', '0', '0', '0', '1', '1'),
(273, 'voyageur1', 6, '1', '0', '0', '0', '1', '1'),
(274, 'voyageur1', 7, '1', '0', '0', '0', '1', '1'),
(275, 'voyageur1', 8, '1', '0', '0', '0', '1', '1'),
(276, 'voyageur1', 9, '1', '0', '0', '0', '1', '1'),
(277, 'voyageur2', 1, '1', '0', '0', '0', '1', '1'),
(278, 'voyageur2', 2, '1', '0', '0', '0', '1', '1'),
(279, 'voyageur2', 3, '1', '0', '0', '0', '1', '1'),
(280, 'voyageur2', 4, '1', '0', '0', '0', '1', '1'),
(281, 'voyageur2', 5, '1', '0', '0', '0', '1', '1'),
(282, 'voyageur2', 6, '1', '0', '0', '0', '1', '1'),
(283, 'voyageur2', 7, '1', '0', '0', '0', '1', '1'),
(284, 'voyageur2', 8, '1', '0', '0', '0', '1', '1'),
(285, 'voyageur2', 9, '1', '0', '0', '0', '1', '1'),
(286, 'jeanne', 1, '0', '0', '0', '0', '1', '1'),
(287, 'jeanne', 2, '0', '0', '0', '0', '1', '1'),
(288, 'jeanne', 3, '0', '0', '0', '0', '1', '1'),
(289, 'jeanne', 4, '0', '0', '0', '0', '1', '1'),
(290, 'jeanne', 5, '0', '0', '0', '0', '1', '1'),
(291, 'jeanne', 6, '0', '0', '0', '0', '1', '1'),
(292, 'jeanne', 7, '0', '0', '0', '0', '1', '1'),
(293, 'jeanne', 8, '1', '0', '0', '0', '1', '1'),
(294, 'jeanne', 9, '0', '0', '0', '0', '1', '1'),
(295, 'musbueabm', 1, '1', '0', '0', '0', '1', '1'),
(296, 'musbueabm', 2, '1', '0', '0', '0', '1', '1'),
(297, 'musbueabm', 3, '1', '0', '0', '0', '1', '1'),
(298, 'musbueabm', 4, '1', '0', '0', '0', '1', '1'),
(299, 'musbueabm', 5, '1', '0', '0', '0', '1', '1'),
(300, 'musbueabm', 6, '1', '0', '0', '0', '1', '1'),
(301, 'musbueabm', 7, '1', '0', '0', '0', '1', '1'),
(302, 'musbueabm', 8, '1', '0', '0', '0', '1', '1'),
(303, 'musbueabm', 9, '1', '0', '0', '0', '1', '1'),
(304, 'noparcel', 1, '0', '0', '0', '0', '1', '1'),
(305, 'noparcel', 2, '0', '0', '0', '0', '1', '1'),
(306, 'noparcel', 3, '0', '0', '0', '0', '1', '1'),
(307, 'noparcel', 4, '0', '0', '0', '0', '0', '1'),
(308, 'noparcel', 5, '0', '0', '0', '0', '1', '1'),
(309, 'noparcel', 6, '1', '0', '0', '0', '1', '1'),
(310, 'noparcel', 7, '0', '0', '0', '0', '1', '1'),
(311, 'noparcel', 8, '0', '0', '0', '0', '1', '1'),
(312, 'noparcel', 9, '0', '0', '0', '0', '1', '1');

-- --------------------------------------------------------

--
-- Table structure for table `agencies`
--

CREATE TABLE IF NOT EXISTS `agencies` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `name` varchar(255) NOT NULL,
  `parent_id` int(12) NOT NULL,
  `town_id` int(112) NOT NULL,
  `contacts` varchar(255) NOT NULL,
  `reservation_contact` int(11) NOT NULL,
  `TicketInfo` varchar(255) NOT NULL,
  `Logo` varchar(300) NOT NULL,
  `ticket_background` varchar(255) NOT NULL,
  `allow_open_tickets` enum('0','1') NOT NULL DEFAULT '0',
  PRIMARY KEY (`id`),
  KEY `parent_id` (`parent_id`),
  KEY `town_id` (`town_id`)
) ENGINE=InnoDB  DEFAULT CHARSET=latin1 AUTO_INCREMENT=115 ;

--
-- Dumping data for table `agencies`
--

INSERT INTO `agencies` (`id`, `name`, `parent_id`, `town_id`, `contacts`, `reservation_contact`, `TicketInfo`, `Logo`, `ticket_background`, `allow_open_tickets`) VALUES
(100, 'Touristique - Yaounde', 10, 3, '696362464', 0, 'Additional infos here', 'images/ticket/agency-logos/touristique-yaounde.jpg', 'images/ticket/ticket.touristique-yaounde.jpg', '0'),
(101, 'Touristique - Douala', 10, 4, '', 0, '', '', '', '0'),
(102, 'Musango - Buea', 3, 1, '', 0, '', 'images/ticket/agency-logos/musango-buea.jpg', 'images/ticket/ticket.musango-buea.jpg', '0'),
(103, 'Musango - Bamenda', 3, 2, '', 0, '', '', '', '0'),
(104, 'Viasso - Buea', 1, 1, '', 0, '', '', '', '1'),
(105, 'Viasso - Yaounde', 1, 3, 'Conts', 696362464, 'Addtional', 'images/ticket/agency-logos/viasso-yaounde.jpg', 'images/ticket/ticket.viasso-yaounde.jpg', '1'),
(106, 'Viasso - Douala', 1, 4, '', 0, '', '', '', '1'),
(107, 'Bucca - Douala', 13, 4, '', 0, '', '', '', '0'),
(108, 'Euroline - Yaounde', 14, 3, '', 0, '', '', '', '0'),
(109, 'Euroline - Douala', 14, 4, '', 0, '', '', '', '0'),
(110, 'Bucca - Yaounde', 13, 3, '', 0, '', '', '', '0'),
(111, 'Musango - Yaounde Prestige', 3, 3, '', 0, '', '', '', '0'),
(112, 'Musango - Yaounde Classic', 3, 3, '', 0, '', '', '', '0'),
(113, 'Le Voyageur - Douala', 15, 4, '', 0, '', '', '', '0'),
(114, 'Le Voyageur - Yaounde', 15, 3, '', 0, '', '', '', '0');

-- --------------------------------------------------------

--
-- Table structure for table `buses`
--

CREATE TABLE IF NOT EXISTS `buses` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `bus_number` varchar(15) NOT NULL,
  `serial_number` varchar(25) NOT NULL,
  `bus_name` varchar(255) NOT NULL,
  `agency_id` int(11) NOT NULL,
  `bus_seats` int(11) NOT NULL,
  `vip` enum('0','1') NOT NULL DEFAULT '0',
  `driver` varchar(255) NOT NULL,
  `motorboy` varchar(255) NOT NULL,
  `other_infos` varchar(255) NOT NULL COMMENT 'Any additional information about the bus',
  `photo` varchar(255) NOT NULL,
  `is_available` enum('0','1') NOT NULL DEFAULT '1',
  `deleted` enum('0','1') NOT NULL,
  `CreatedBy` varchar(25) NOT NULL,
  `CreatedOn` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP,
  UNIQUE KEY `id` (`id`),
  UNIQUE KEY `bus_number` (`bus_number`),
  KEY `agency_id` (`agency_id`)
) ENGINE=InnoDB  DEFAULT CHARSET=latin1 AUTO_INCREMENT=55 ;

--
-- Dumping data for table `buses`
--

INSERT INTO `buses` (`id`, `bus_number`, `serial_number`, `bus_name`, `agency_id`, `bus_seats`, `vip`, `driver`, `motorboy`, `other_infos`, `photo`, `is_available`, `deleted`, `CreatedBy`, `CreatedOn`) VALUES
(6, 'CE123AB', '', '', 105, 48, '1', '', '', '', '', '1', '0', '', '2015-08-31 07:12:35'),
(7, 'NW989MO', '', '', 105, 30, '1', '', '', '', '', '1', '0', '', '2015-09-08 08:39:31'),
(8, 'SW218AJ', '', 'CLASSIC', 102, 30, '0', '', '', '', '', '1', '0', '', '2015-09-13 14:46:46'),
(9, 'SW005AO', '', 'CLASSIC', 102, 57, '0', '', '', '', '', '1', '0', '', '2015-09-13 14:47:16'),
(10, 'SW002AH', '', 'CLASSIC', 102, 30, '0', '', '', '', '', '1', '0', '', '2015-09-13 14:47:37'),
(11, 'SW679AQ', '', 'CLASSIC', 102, 35, '0', '', '', '', '', '1', '0', '', '2015-09-13 14:48:09'),
(12, 'SW282AE', '', 'CLASSIC', 102, 30, '0', '', '', '', '', '1', '0', '', '2015-09-13 14:49:14'),
(13, 'SW780AL', '', 'CLASSIC', 102, 30, '0', '', '', '', '', '1', '0', '', '2015-09-13 14:49:32'),
(14, 'SW301AK', '', 'CLASSIC', 102, 30, '0', '', '', '', '', '1', '0', '', '2015-09-13 14:49:52'),
(15, 'SW845AT', '', 'CLASSIC', 102, 55, '0', '', '', '', '', '1', '0', '', '2015-09-13 14:50:57'),
(16, 'SW395AR', '', 'VIP', 102, 70, '0', '', '', '', '', '1', '0', '', '2015-09-13 14:51:33'),
(17, 'SW055AU', '', 'CLASSIC', 102, 57, '0', '', '', '', '', '1', '0', '', '2015-09-13 14:53:43'),
(18, 'SW302AK', '', 'CLASSIC', 102, 30, '0', '', '', '', '', '1', '0', '', '2015-09-13 14:54:08'),
(19, 'SW736AO', '', 'CLASSIC', 102, 58, '0', '', '', '', '', '1', '0', '', '2015-09-13 14:55:01'),
(20, 'SW240AO', '', 'CLASSIC', 102, 61, '0', '', '', '', '', '1', '0', '', '2015-09-13 14:55:33'),
(21, 'SW036AM', '', 'CLASSIC', 102, 30, '0', '', '', '', '', '1', '0', '', '2015-09-13 14:56:04'),
(22, 'SW473AE', '', 'CLASSIC', 102, 70, '0', '', '', '', '', '1', '0', '', '2015-09-13 14:56:34'),
(23, 'SW940AR', '', 'CLASSIC', 102, 57, '0', '', '', '', '', '1', '0', '', '2015-09-13 14:57:11'),
(24, 'SW280AS', '', 'CLASSIC', 102, 56, '0', '', '', '', '', '1', '0', '', '2015-09-13 14:57:52'),
(25, 'SW200AS', '', 'CLASSIC', 102, 57, '0', '', '', '', '', '1', '0', '', '2015-09-13 14:58:25'),
(26, 'SW872AF', '', 'CLASSIC', 102, 30, '0', '', '', '', '', '1', '0', '', '2015-09-13 14:59:03'),
(27, 'SW500AD', '', 'CLASSIC', 102, 30, '0', '', '', '', '', '1', '0', '', '2015-09-13 14:59:29'),
(28, 'SW505AD', '', 'CLASSIC', 102, 30, '0', '', '', '', '', '1', '0', '', '2015-09-13 14:59:59'),
(29, 'SW740AP', '', 'CLASSIC', 102, 35, '0', '', '', '', '', '1', '0', '', '2015-09-13 15:00:28'),
(30, 'SW735AP', '', 'CLASSIC', 102, 35, '0', '', '', '', '', '1', '0', '', '2015-09-13 15:01:04'),
(31, 'SW089AV', '', 'CLASSIC', 102, 70, '0', '', '', '', '', '1', '0', '', '2015-09-13 15:01:42'),
(32, 'SW020AV', '', 'CLASSIC', 102, 30, '0', '', '', '', '', '1', '0', '', '2015-09-13 15:02:07'),
(33, 'SW015AV', '', 'CLASSIC', 102, 30, '0', '', '', '', '', '1', '0', '', '2015-09-13 15:02:24'),
(34, 'SW275AS', '', 'CLASSIC', 102, 57, '0', '', '', '', '', '1', '0', '', '2015-09-13 15:02:50'),
(35, 'SW705AQ', '', 'CLASSIC', 102, 70, '0', '', '', '', '', '1', '0', '', '2015-09-13 15:03:54'),
(36, 'SW184AQ', '', 'CLASSIC', 102, 35, '0', '', '', '', '', '1', '0', '', '2015-09-13 15:04:17'),
(37, 'SW183AQ', '', 'CLASSIC', 102, 35, '0', '', '', '', '', '1', '0', '', '2015-09-13 15:04:35'),
(38, 'SW182AQ', '', 'CLASSIC', 102, 35, '0', '', '', '', '', '1', '0', '', '2015-09-13 15:04:53'),
(39, 'SW215AJ', '', 'CLASSIC', 102, 30, '0', '', '', '', '', '1', '0', '', '2015-09-13 15:05:10'),
(40, 'SW395AS', '', 'CLASSIC', 102, 57, '1', '', '', '', '', '1', '0', '', '2015-09-13 15:05:31'),
(41, 'SW954AS', '', 'CLASSIC', 102, 70, '0', '', '', '', '', '1', '0', '', '2015-09-13 15:05:48'),
(42, 'CH082645', '', 'VIP', 102, 47, '1', '', '', '', '', '1', '0', '', '2015-09-13 15:06:22'),
(43, 'CH082765', '', 'VIP', 102, 57, '1', '', '', '', '', '1', '0', '', '2015-09-13 15:06:46'),
(44, 'SW214AJ', '', 'CLASSIC', 102, 30, '0', '', '', '', '', '1', '0', '', '2015-09-13 15:07:13'),
(45, 'SW121AO', '', 'CLASSIC', 102, 30, '0', '', '', '', '', '1', '0', '', '2015-09-13 15:07:29'),
(46, 'SW216AJ', '', 'CLASSIC', 102, 30, '0', '', '', '', '', '1', '0', '', '2015-09-13 15:07:46'),
(47, 'SW003AH', '', 'CLASSIC', 102, 30, '0', '', '', '', '', '1', '0', '', '2015-09-13 15:08:02'),
(48, 'SW500AC', '', 'CLASSIC', 102, 30, '0', '', '', '', '', '1', '0', '', '2015-09-13 15:08:23'),
(49, 'SW090AV', '', 'CLASSIC', 102, 70, '0', '', '', '', '', '1', '0', '', '2015-09-13 15:08:43'),
(50, 'CH618215', '', '', 102, 57, '1', '', '', '', '', '1', '0', '', '2015-09-14 04:15:30'),
(51, 'NW111', '', '', 106, 70, '0', '', '', '', '', '1', '0', '', '2015-09-13 06:01:31'),
(52, 'AB111A', '', '', 105, 80, '0', '', '', '', '', '1', '0', '', '2015-09-17 16:01:15'),
(53, 'UNKNOWN', '', '', 105, 70, '0', '', '', '', '', '1', '0', '', '2015-09-23 13:50:02'),
(54, 'NW222', '', '', 105, 30, '0', '', '', '', '', '0', '0', '', '2016-12-20 13:54:40');

--
-- Triggers `buses`
--
DROP TRIGGER IF EXISTS `insert_bus_transaction`;
DELIMITER //
CREATE TRIGGER `insert_bus_transaction` AFTER INSERT ON `buses`
 FOR EACH ROW BEGIN
	SET @change_type = 'insert';
	INSERT INTO transactions (	`table`, agency_id, change_id, change_type, bus_number, bus_seats, vip, driver, deleted) 	
						VALUES ( 'buses', NEW.agency_id, NEW.id, @change_type, NEW.bus_number, NEW.bus_seats, NEW.vip, NEW.driver, NEW.deleted);
END
//
DELIMITER ;
DROP TRIGGER IF EXISTS `update_bus_transaction`;
DELIMITER //
CREATE TRIGGER `update_bus_transaction` AFTER UPDATE ON `buses`
 FOR EACH ROW BEGIN
	SET @change_type = 'update';
	INSERT INTO transactions (	`table`, agency_id, change_id, change_type, bus_number, bus_seats, vip, driver, deleted) 	
						VALUES ('buses',  NEW.agency_id, NEW.id, @change_type, NEW.bus_number, NEW.bus_seats, NEW.vip, NEW.driver, NEW.deleted);
END
//
DELIMITER ;

-- --------------------------------------------------------

--
-- Table structure for table `bus_inavailability_details`
--

CREATE TABLE IF NOT EXISTS `bus_inavailability_details` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `agency_id` int(11) NOT NULL,
  `bus_number` varchar(50) NOT NULL,
  `reason` varchar(255) NOT NULL,
  `start_date` date NOT NULL,
  `start_time` time NOT NULL,
  `end_date` date DEFAULT NULL,
  `end_time` time NOT NULL,
  `created_by` varchar(50) NOT NULL,
  `modified_by` varchar(50) NOT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB  DEFAULT CHARSET=latin1 AUTO_INCREMENT=8 ;

--
-- Dumping data for table `bus_inavailability_details`
--

INSERT INTO `bus_inavailability_details` (`id`, `agency_id`, `bus_number`, `reason`, `start_date`, `start_time`, `end_date`, `end_time`, `created_by`, `modified_by`) VALUES
(1, 105, 'NW989MO', '', '2016-12-23', '14:08:10', '2016-12-23', '14:08:38', '', ''),
(2, 105, 'NW222', '', '2016-12-23', '14:09:03', NULL, '00:00:00', '', ''),
(3, 105, 'AB111A', '', '2016-12-23', '14:09:12', '2016-12-23', '14:53:26', '', ''),
(4, 105, 'NW989MO', '', '0000-00-00', '00:00:00', '2016-12-23', '14:53:08', '', ''),
(6, 105, 'AB111A', 'change tyre', '2016-12-23', '14:54:29', '2016-12-23', '14:59:19', '', ''),
(7, 105, 'NW989MO', 'trauma', '2016-12-23', '15:44:09', '2016-12-23', '15:45:19', 'ABSA', 'ABSA');

-- --------------------------------------------------------

--
-- Table structure for table `bus_requirements`
--

CREATE TABLE IF NOT EXISTS `bus_requirements` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `AgencyID` int(11) NOT NULL,
  `Name` varchar(255) NOT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB  DEFAULT CHARSET=latin1 AUTO_INCREMENT=7 ;

--
-- Dumping data for table `bus_requirements`
--

INSERT INTO `bus_requirements` (`id`, `AgencyID`, `Name`) VALUES
(2, 105, 'Carte grise'),
(3, 105, 'Vignette'),
(4, 105, 'Insurance'),
(5, 105, 'Carte bleu'),
(6, 105, 'Carte rose');

-- --------------------------------------------------------

--
-- Table structure for table `bus_requirements_statuses`
--

CREATE TABLE IF NOT EXISTS `bus_requirements_statuses` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `AgencyID` int(11) NOT NULL,
  `BusNumber` varchar(15) NOT NULL,
  `BusRequirementID` int(11) NOT NULL,
  `IsDone` enum('0','1') NOT NULL DEFAULT '0',
  `ExpiryDate` date DEFAULT NULL,
  PRIMARY KEY (`id`),
  KEY `BusNumber` (`BusNumber`),
  KEY `BusRequirementID` (`BusRequirementID`)
) ENGINE=InnoDB  DEFAULT CHARSET=latin1 AUTO_INCREMENT=22 ;

--
-- Dumping data for table `bus_requirements_statuses`
--

INSERT INTO `bus_requirements_statuses` (`id`, `AgencyID`, `BusNumber`, `BusRequirementID`, `IsDone`, `ExpiryDate`) VALUES
(2, 105, 'NW989MO', 2, '1', '2016-12-23'),
(3, 105, 'NW989MO', 3, '0', NULL),
(4, 105, 'NW989MO', 4, '1', '2016-10-11'),
(5, 105, 'NW989MO', 5, '0', NULL),
(6, 105, 'NW989MO', 6, '1', '2017-01-10'),
(7, 105, 'CE123AB', 2, '1', '2016-12-29'),
(8, 105, 'CE123AB', 3, '0', NULL),
(9, 105, 'CE123AB', 4, '0', NULL),
(10, 105, 'CE123AB', 5, '0', NULL),
(11, 105, 'CE123AB', 6, '0', NULL),
(12, 105, 'NW222', 2, '1', '2016-12-30'),
(13, 105, 'NW222', 3, '0', NULL),
(14, 105, 'NW222', 4, '0', NULL),
(15, 105, 'NW222', 5, '0', NULL),
(16, 105, 'NW222', 6, '0', NULL),
(17, 105, 'AB111A', 2, '', NULL),
(18, 105, 'AB111A', 3, '', NULL),
(19, 105, 'AB111A', 4, '', NULL),
(20, 105, 'AB111A', 5, '', NULL),
(21, 105, 'AB111A', 6, '', NULL);

-- --------------------------------------------------------

--
-- Table structure for table `cash_interbranches`
--

CREATE TABLE IF NOT EXISTS `cash_interbranches` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `agency_id` int(12) NOT NULL,
  `sender_agency_id` int(11) NOT NULL,
  `receiver_agency_id` int(11) NOT NULL,
  `amount` int(11) NOT NULL,
  `description` text NOT NULL,
  `date_incurred` date NOT NULL,
  `created_by` varchar(25) NOT NULL,
  `created_on` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP,
  `online` enum('0','1') NOT NULL,
  `deleted` enum('0','1') NOT NULL,
  PRIMARY KEY (`id`),
  KEY `agency_id` (`agency_id`)
) ENGINE=InnoDB DEFAULT CHARSET=latin1 AUTO_INCREMENT=1 ;

-- --------------------------------------------------------

--
-- Table structure for table `cash_to_banks`
--

CREATE TABLE IF NOT EXISTS `cash_to_banks` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `agency_id` int(12) NOT NULL,
  `bank_name` varchar(255) NOT NULL,
  `account_number` varchar(255) NOT NULL,
  `amount` int(11) NOT NULL,
  `purpose` text NOT NULL,
  `date_incurred` date NOT NULL,
  `created_by` varchar(25) NOT NULL,
  `created_on` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP,
  `online` enum('0','1') NOT NULL,
  `deleted` enum('0','1') NOT NULL,
  PRIMARY KEY (`id`),
  KEY `agency_id` (`agency_id`)
) ENGINE=InnoDB DEFAULT CHARSET=latin1 AUTO_INCREMENT=1 ;

-- --------------------------------------------------------

--
-- Table structure for table `collections`
--

CREATE TABLE IF NOT EXISTS `collections` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `agency_id` int(12) NOT NULL,
  `admin_id` varchar(255) NOT NULL,
  `previous_balance` int(11) NOT NULL,
  `amount` int(11) NOT NULL,
  `purpose` text NOT NULL,
  `date_incurred` date NOT NULL,
  `authorised_by` varchar(255) NOT NULL,
  `CreatedBy` varchar(25) NOT NULL,
  `CreatedOn` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP,
  `deleted` enum('0','1') NOT NULL DEFAULT '0',
  PRIMARY KEY (`id`),
  KEY `agency_id` (`agency_id`)
) ENGINE=InnoDB  DEFAULT CHARSET=latin1 AUTO_INCREMENT=2 ;

--
-- Dumping data for table `collections`
--

INSERT INTO `collections` (`id`, `agency_id`, `admin_id`, `previous_balance`, `amount`, `purpose`, `date_incurred`, `authorised_by`, `CreatedBy`, `CreatedOn`, `deleted`) VALUES
(1, 105, 'absa', 26000, 1000, '', '0000-00-00', '', 'ADMIN', '2015-09-08 15:32:42', '0');

-- --------------------------------------------------------

--
-- Table structure for table `daily_summary`
--

CREATE TABLE IF NOT EXISTS `daily_summary` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `date` date NOT NULL,
  `buses_income` int(11) NOT NULL,
  `buses_expenditure` int(11) NOT NULL,
  `schedule_income` int(11) NOT NULL,
  `schedule_expenditure` int(11) NOT NULL,
  `bus_rental_income` int(11) NOT NULL,
  `parcel_income` int(11) NOT NULL,
  `total_income` int(11) NOT NULL,
  `total_expenditure` int(11) NOT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB DEFAULT CHARSET=latin1 AUTO_INCREMENT=1 ;

-- --------------------------------------------------------

--
-- Table structure for table `dashboard_items`
--

CREATE TABLE IF NOT EXISTS `dashboard_items` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `DescriptionCode` varchar(255) NOT NULL COMMENT 'A string that helps translation',
  `Description` varchar(255) NOT NULL,
  `RequiresParameters` enum('0','1') NOT NULL DEFAULT '0',
  PRIMARY KEY (`id`)
) ENGINE=InnoDB  DEFAULT CHARSET=latin1 AUTO_INCREMENT=5 ;

--
-- Dumping data for table `dashboard_items`
--

INSERT INTO `dashboard_items` (`id`, `DescriptionCode`, `Description`, `RequiresParameters`) VALUES
(1, 'create_schedules', 'Create schedules', '0'),
(2, 'all_current_schedules', 'All current schedules', '0'),
(3, 'schedules_for_town', 'Schedules for particular town', '0'),
(4, 'past_schedules', 'Past schedules', '0');

-- --------------------------------------------------------

--
-- Table structure for table `expenses`
--

CREATE TABLE IF NOT EXISTS `expenses` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `agency_id` int(12) NOT NULL,
  `bus_number` varchar(15) NOT NULL,
  `schedule_id` int(11) NOT NULL,
  `amount` int(11) NOT NULL,
  `purpose` text NOT NULL,
  `category_id` int(11) NOT NULL DEFAULT '1',
  `is_cash_interbranch` enum('0','1') NOT NULL DEFAULT '0',
  `cash_interbranch_agency_id_to` int(11) NOT NULL,
  `date_incurred` date NOT NULL,
  `authorised_by` varchar(255) NOT NULL,
  `CreatedBy` varchar(25) NOT NULL,
  `CreatedOn` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP,
  `deleted` enum('0','1') NOT NULL DEFAULT '0',
  PRIMARY KEY (`id`),
  KEY `agency_id` (`agency_id`),
  KEY `schedule_id` (`schedule_id`)
) ENGINE=InnoDB  DEFAULT CHARSET=latin1 AUTO_INCREMENT=67 ;

--
-- Dumping data for table `expenses`
--

INSERT INTO `expenses` (`id`, `agency_id`, `bus_number`, `schedule_id`, `amount`, `purpose`, `category_id`, `is_cash_interbranch`, `cash_interbranch_agency_id_to`, `date_incurred`, `authorised_by`, `CreatedBy`, `CreatedOn`, `deleted`) VALUES
(1, 105, '', 0, 150000, 'Fuel', 2, '', 0, '2016-12-06', '', 'ABSA', '2015-08-28 09:06:25', '0'),
(2, 105, '', 0, 150000, 'Fuel', 1, '', 0, '2016-12-06', '', 'ABSA', '2015-09-04 12:01:27', '0'),
(3, 105, 'NW989MO', 36, 10000, 'Loading', 1, '0', 0, '2015-09-17', '', 'ABSA', '2015-09-20 05:18:30', '0'),
(4, 105, 'CE123AB', 44, 10000, 'Loading', 1, '0', 0, '2015-09-20', '', 'ABSA', '2015-09-20 05:28:34', '0'),
(5, 105, 'CE123AB', 42, 10000, 'Loading', 1, '0', 0, '2015-09-20', '', 'ABSA', '2015-09-20 11:10:54', '0'),
(6, 105, 'CE123AB', 45, 10000, 'Loading', 1, '0', 0, '2015-09-22', '', 'ABSA', '2015-09-22 09:30:37', '0'),
(7, 105, '', 0, 14000, 'Net income from schedule sent via schedule', 1, '0', 0, '2015-09-28', '', 'ABSA', '2015-09-28 14:29:46', '0'),
(8, 105, '', 0, 14000, 'Net income from schedule SC10545 sent via bus driver', 1, '0', 0, '2015-09-28', '', 'ABSA', '2015-09-28 15:01:34', '0'),
(9, 105, '', 0, 14000, 'Net income from schedule SC10545 sent via bus driver', 1, '0', 0, '2015-09-28', '', 'ABSA', '2015-09-28 15:04:26', '0'),
(10, 105, '', 0, 14000, 'Net income from schedule SC10545 sent via bus driver', 1, '1', 106, '2015-09-28', '', 'ABSA', '2015-09-28 15:05:19', '0'),
(11, 105, '', 0, 14000, 'Net income from schedule SC10545 sent via bus driver', 1, '1', 106, '2015-09-28', '', 'ABSA', '2015-09-28 15:06:54', '0'),
(12, 105, '', 0, 14000, 'Net income from schedule SC10545 sent via bus driver', 1, '1', 106, '2015-09-28', '', 'ABSA', '2015-09-28 15:07:40', '0'),
(13, 105, '', 0, 14000, 'Net income from schedule SC10545 sent via bus driver', 1, '1', 106, '2015-09-28', '', 'ABSA', '2015-09-28 15:47:07', '0'),
(14, 105, '', 0, 14000, 'Net income from schedule SC10545 sent via bus driver', 1, '1', 106, '2015-09-28', '', 'ABSA', '2015-09-28 15:48:15', '0'),
(15, 105, '', 0, 5000, 'Net income from schedule SC10538 sent via bus driver', 1, '1', 106, '2015-09-30', '', 'ABSA', '2015-09-30 12:43:20', '1'),
(16, 105, 'CE123AB', 52, 500, 'Puller', 1, '', 0, '2015-09-30', '', 'ABSA', '2015-09-30 12:45:39', '1'),
(17, 105, 'CE123AB', 52, 10000, 'Loading', 1, '', 0, '2015-09-30', '', 'ABSA', '2015-09-30 12:47:05', '1'),
(18, 105, '', 0, 2500, 'Ramis', 1, '', 0, '2015-09-30', 'Ngandon', 'ABSA', '2015-09-30 13:38:00', '1'),
(19, 105, '', 0, 1000, 'Tyre repairs', 1, '', 0, '2015-09-22', '', 'ABSA', '2015-09-30 13:43:51', '1'),
(20, 105, '', 0, 10000, 'test', 1, '', 0, '2015-09-30', 'Killian', 'ABSA', '2015-09-30 14:30:05', '1'),
(21, 105, 'CE123AB', 53, 10000, 'Loading', 1, '', 0, '2015-10-01', '', 'ABSA', '2015-10-01 07:51:16', '0'),
(22, 105, 'CE123AB', 53, 2000, 'Tire repair', 1, '', 0, '2015-10-01', '', 'ABSA', '2015-10-01 07:56:18', '0'),
(23, 105, 'CE123AB', 56, 10000, 'Loading', 1, '', 0, '2015-10-01', '', 'ABSA', '2015-10-01 08:52:18', '0'),
(24, 105, '', 0, 8000, 'Net income from schedule SC10556 sent via bus driver', 1, '1', 106, '2015-10-01', '', 'ABSA', '2015-10-01 08:53:11', '0'),
(25, 105, 'CE123AB', 59, 10000, 'Fuel', 1, '', 0, '2015-10-07', '', 'ABSA', '2015-10-07 08:21:47', '1'),
(26, 105, 'CE123AB', 59, 10000, 'Loading', 1, '', 0, '2015-10-07', '', 'ABSA', '2015-10-07 08:25:12', '0'),
(27, 105, 'CE123AB', 61, 10000, 'Loading', 1, '', 0, '2015-10-07', '', 'ABSA', '2015-10-08 09:03:18', '0'),
(28, 105, 'CE123AB', 62, 10000, 'Loading', 1, '', 0, '2015-10-08', '', 'ABSA', '2015-10-08 10:02:05', '0'),
(29, 105, 'CE123AB', 63, 10000, 'Loading', 1, '', 0, '2015-10-14', '', 'ABSA', '2015-10-14 15:27:37', '0'),
(30, 105, 'CE123AB', 64, 10000, 'Loading', 1, '', 0, '2015-10-14', '', 'ABSA', '2015-10-14 15:28:17', '0'),
(31, 105, '', 0, 14000, 'Net income from schedule SC10564 sent via bus driver', 1, '1', 106, '2015-10-14', '', 'ABSA', '2015-10-14 15:28:24', '0'),
(32, 105, 'CE123AB', 65, 10000, 'Loading', 1, '', 0, '2015-10-14', '', 'ABSA', '2015-10-14 15:39:27', '0'),
(33, 105, '', 0, 5000, 'Net income from schedule SC10565 sent toViasso - Douala', 1, '1', 106, '2015-10-14', '', 'ABSA', '2015-10-14 15:39:40', '0'),
(34, 105, 'CE123AB', 67, 10000, 'Loading', 1, '', 0, '2015-10-20', '', 'ABSA', '2015-10-20 10:57:37', '0'),
(35, 105, 'CE123AB', 68, 10000, 'Loading', 1, '', 0, '2015-10-23', '', 'ABSA', '2015-10-23 13:34:51', '0'),
(36, 105, 'CE123AB', 72, 10000, 'Loading', 1, '', 0, '2015-10-29', '', 'ABSA', '2015-10-29 10:40:10', '0'),
(37, 105, 'CE123AB', 73, 10000, 'Loading', 1, '', 0, '2015-11-01', '', 'ABSA', '2015-10-31 19:34:22', '0'),
(38, 105, 'CE123AB', 74, 10000, 'Loading', 1, '', 0, '2015-11-01', '', 'ABSA', '2015-10-31 19:34:33', '0'),
(39, 105, 'CE123AB', 71, 10000, 'Loading', 1, '', 0, '2015-10-29', '', 'ABSA', '2015-10-31 19:35:12', '0'),
(40, 105, 'CE123AB', 76, 10000, 'Loading', 1, '', 0, '2015-11-02', '', 'ABSA', '2015-11-02 15:27:28', '0'),
(41, 105, 'CE123AB', 70, 10000, 'Loading', 1, '', 0, '2015-10-23', '', 'ABSA', '2016-02-26 15:07:32', '0'),
(42, 105, 'CE123AB', 93, 10000, 'Loading', 1, '', 0, '2016-05-19', '', 'ABSA', '2016-05-19 02:21:45', '0'),
(43, 105, 'CE123AB', 99, 10000, 'Loading', 1, '', 0, '2016-08-29', '', 'ABSA', '2016-08-29 09:50:59', '0'),
(44, 105, 'CE123AB', 98, 10000, 'Loading', 1, '', 0, '2016-08-12', '', 'ABSA', '2016-09-21 02:16:58', '0'),
(45, 105, 'CE123AB', 97, 10000, 'Loading', 1, '', 0, '2016-07-29', '', 'ABSA', '2016-09-21 02:21:54', '0'),
(46, 105, 'CE123AB', 96, 10000, 'Loading', 1, '', 0, '2016-07-29', '', 'ABSA', '2016-09-21 02:23:45', '0'),
(47, 105, 'CE123AB', 83, 10000, 'Loading', 1, '', 0, '2016-02-25', '', 'ABSA', '2016-09-21 02:25:01', '0'),
(48, 105, '', 0, 500, 'Nothing\n', 1, '', 0, '2016-09-21', '', 'ABSA', '2016-09-21 06:18:36', '1'),
(49, 105, 'CE123AB', 102, 10000, 'Loading', 1, '', 0, '2016-09-22', '', 'ABSA', '2016-09-22 01:59:19', '0'),
(50, 105, '', 0, 400, 'Buying tyres', 1, '', 0, '2016-10-08', 'Fatima', 'ABSA', '2016-10-08 11:44:30', '0'),
(51, 105, 'AB111A', 105, 4000, 'Paid motorboy', 1, '', 0, '2016-10-08', '', 'ABSA', '2016-10-08 11:46:26', '1'),
(52, 105, '', 0, 500, 'Billing\n', 1, '', 0, '2016-10-09', '', 'ABSA', '2016-10-09 00:13:37', '0'),
(53, 105, '', 0, 580, 'Bills', 1, '', 0, '2016-10-10', '', 'ABSA', '2016-10-10 00:31:06', '0'),
(54, 105, '', 0, 2322, 'water', 1, '', 0, '2016-10-10', '', 'ABSA', '2016-10-10 00:31:36', '1'),
(55, 105, '', 0, 2231, 'bill', 1, '', 0, '2016-10-10', '', 'ABSA', '2016-10-10 00:32:26', '0'),
(56, 105, '', 0, 15000, 'test', 2, '', 0, '2016-12-06', '', 'ABSA', '2016-12-06 04:17:09', '0'),
(57, 105, '', 0, 10000, 'fu', 2, '', 0, '2016-12-06', '', 'ABSA', '2016-12-06 04:29:23', '0'),
(58, 105, 'AA123AA', 0, 1000, 'More', 2016, '', 0, '0000-00-00', '107', '', '2016-12-06 04:46:13', '0'),
(59, 105, 'AA123AA', 0, 150, 'more', 2, '', 0, '2016-12-06', '', 'ABSA', '2016-12-06 04:48:54', '0'),
(60, 105, 'AA123AA', 0, 10000, 'Loading', 2016, '', 0, '0000-00-00', '107', '', '2016-12-14 04:53:51', '0'),
(61, 105, 'AA123AA', 107, 5000, 'More', 1, '', 0, '2016-12-14', '', 'ABSA', '2016-12-14 04:55:43', '0'),
(62, 105, 'CE123AB', 0, 10000, 'Loading', 2016, '', 0, '0000-00-00', '79', '', '2016-12-16 12:50:05', '0'),
(63, 105, '', 0, 5200, 'nnn', 1, '', 0, '2016-12-16', '', 'ABSA', '2016-12-16 15:26:47', '0'),
(64, 105, '', 0, 2500, 'Lol', 2, '', 0, '2016-12-17', '', 'ABSA', '2016-12-17 04:43:30', '0'),
(65, 105, '', 0, 20000, 'Fishing', 2, '', 0, '2016-12-18', 'Clement', 'ABSA', '2016-12-18 09:25:40', '0'),
(66, 105, '', 0, 15000, 'sss', 1, '', 0, '2017-01-08', 'Clement', 'ABSA', '2017-01-08 14:44:44', '0');

--
-- Triggers `expenses`
--
DROP TRIGGER IF EXISTS `insert_expense_transaction`;
DELIMITER //
CREATE TRIGGER `insert_expense_transaction` AFTER INSERT ON `expenses`
 FOR EACH ROW BEGIN
	SET @change_type = 'insert';
	INSERT INTO transactions (	`table`, agency_id, change_id, change_type, bus_number, schedule_id, amount, purpose, date_incurred, authorised_by, CreatedBy, CreatedOn, deleted) 			
				VALUES (	'expenses', NEW.agency_id, NEW.id, @change_type, NEW.bus_number, NEW.schedule_id, NEW.amount, NEW.purpose, NEW.date_incurred, NEW.authorised_by, NEW.CreatedBy, NEW.CreatedOn, NEW.deleted);
END
//
DELIMITER ;
DROP TRIGGER IF EXISTS `update_expense_transaction`;
DELIMITER //
CREATE TRIGGER `update_expense_transaction` AFTER UPDATE ON `expenses`
 FOR EACH ROW BEGIN
	SET @change_type = 'update';
	INSERT INTO transactions ( `table`, agency_id, change_id, change_type, bus_number, amount, purpose, date_incurred, authorised_by, CreatedBy, CreatedOn, deleted)
				VALUES (	'expenses', NEW.agency_id, NEW.id, @change_type, NEW.bus_number, NEW.amount, NEW.purpose, NEW.date_incurred, NEW.authorised_by, NEW.CreatedBy, NEW.CreatedOn, NEW.deleted);
END
//
DELIMITER ;

-- --------------------------------------------------------

--
-- Table structure for table `expense_categories`
--

CREATE TABLE IF NOT EXISTS `expense_categories` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `agency_id` int(11) NOT NULL,
  `name` varchar(255) NOT NULL,
  `description` varchar(255) NOT NULL,
  `deleted` enum('0','1') NOT NULL DEFAULT '0',
  `created_by` varchar(50) NOT NULL,
  `created_on` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP,
  `modified_by` varchar(50) NOT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB  DEFAULT CHARSET=latin1 AUTO_INCREMENT=3 ;

--
-- Dumping data for table `expense_categories`
--

INSERT INTO `expense_categories` (`id`, `agency_id`, `name`, `description`, `deleted`, `created_by`, `created_on`, `modified_by`) VALUES
(1, 105, 'Sub', 'Default expense category', '0', 'SYS', '2016-12-06 03:27:00', 'ABSA'),
(2, 105, 'Fuel', 'Test', '0', 'ABSA', '2016-12-06 04:23:20', '');

-- --------------------------------------------------------

--
-- Table structure for table `fixed_expense_details`
--

CREATE TABLE IF NOT EXISTS `fixed_expense_details` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `AgencyID` int(11) NOT NULL,
  `Name` varchar(255) NOT NULL,
  `IsPercentage` enum('0','1') NOT NULL,
  `IsFixedAmount` enum('0','1') NOT NULL,
  `Value` int(11) NOT NULL,
  `CreatedBy` varchar(255) NOT NULL,
  `CreatedOn` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB  DEFAULT CHARSET=latin1 AUTO_INCREMENT=3 ;

--
-- Dumping data for table `fixed_expense_details`
--

INSERT INTO `fixed_expense_details` (`id`, `AgencyID`, `Name`, `IsPercentage`, `IsFixedAmount`, `Value`, `CreatedBy`, `CreatedOn`) VALUES
(2, 105, 'Loading', '0', '1', 10000, 'absa', '2015-09-20 05:25:00');

-- --------------------------------------------------------

--
-- Table structure for table `incomes`
--

CREATE TABLE IF NOT EXISTS `incomes` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `agency_id` int(12) NOT NULL,
  `bus_number` varchar(15) NOT NULL,
  `rented_to` varchar(255) NOT NULL,
  `schedule_id` int(11) NOT NULL,
  `amount` int(11) NOT NULL,
  `purpose` enum('bus_rental','other') NOT NULL,
  `description` text NOT NULL,
  `is_cash_interbranch` enum('0','1') NOT NULL DEFAULT '0',
  `cash_interbranch_agency_id_from` int(11) NOT NULL,
  `date_incurred` date NOT NULL,
  `start_date` date DEFAULT NULL,
  `end_date` date DEFAULT NULL,
  `authorised_by` varchar(255) NOT NULL,
  `CreatedBy` varchar(25) NOT NULL,
  `CreatedOn` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP,
  `online` enum('0','1') NOT NULL DEFAULT '0',
  `deleted` enum('0','1') NOT NULL DEFAULT '0',
  PRIMARY KEY (`id`),
  KEY `agency_id` (`agency_id`),
  KEY `schedule_id` (`schedule_id`)
) ENGINE=InnoDB  DEFAULT CHARSET=latin1 AUTO_INCREMENT=7 ;

--
-- Dumping data for table `incomes`
--

INSERT INTO `incomes` (`id`, `agency_id`, `bus_number`, `rented_to`, `schedule_id`, `amount`, `purpose`, `description`, `is_cash_interbranch`, `cash_interbranch_agency_id_from`, `date_incurred`, `start_date`, `end_date`, `authorised_by`, `CreatedBy`, `CreatedOn`, `online`, `deleted`) VALUES
(2, 105, '', '', 0, 950, 'other', 'Bus arrival', '0', 0, '2015-09-08', NULL, NULL, '', 'ADMIN', '2015-09-08 14:48:04', '1', '0'),
(3, 105, 'CE123AB', '', 0, 10000, 'other', 'Bus arrival', '0', 0, '2015-09-08', NULL, NULL, '', 'ADMIN', '2015-09-08 15:05:39', '1', '0'),
(4, 105, 'NW989MO', 'Nformi', 0, 50000, 'bus_rental', 'For marriage', '0', 0, '2016-11-30', '2016-11-30', '2016-12-08', '', 'ABSA', '2016-11-30 00:11:48', '1', '0'),
(5, 105, 'NW989MO', 'Kiali', 0, 5000, 'bus_rental', '', '0', 0, '2016-12-16', '2016-12-16', '2016-12-17', '', 'ABSA', '2016-12-16 13:24:35', '1', '0'),
(6, 105, '', '', 0, 100, 'other', 'kkk', '0', 0, '2016-12-16', NULL, NULL, '', 'ABSA', '2016-12-16 14:17:15', '1', '0');

-- --------------------------------------------------------

--
-- Table structure for table `incoming_parcels`
--

CREATE TABLE IF NOT EXISTS `incoming_parcels` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `online_id` int(11) NOT NULL COMMENT 'Parcel ID from online',
  `agency_id` int(11) NOT NULL,
  `global_parcel_id` int(11) NOT NULL,
  `schedule_id` int(11) NOT NULL,
  `bus_number` varchar(25) NOT NULL,
  `receiver_name` varchar(255) NOT NULL,
  `receiver_phone` int(11) NOT NULL,
  `collected_by` varchar(500) NOT NULL,
  `collected_on` datetime NOT NULL,
  `description` text NOT NULL,
  `from` int(11) NOT NULL COMMENT 'id of agency of origin of parcel',
  `to` int(11) NOT NULL COMMENT 'id of destination agency of parcel',
  `price` int(255) NOT NULL,
  `sent_date` date NOT NULL,
  `state` enum('sent','received','delivered') NOT NULL DEFAULT 'sent',
  `CreatedBy` varchar(25) NOT NULL,
  `CreatedOn` datetime NOT NULL,
  PRIMARY KEY (`id`),
  KEY `agency_id` (`agency_id`),
  KEY `from` (`from`),
  KEY `to` (`to`)
) ENGINE=InnoDB  DEFAULT CHARSET=latin1 AUTO_INCREMENT=5 ;

--
-- Dumping data for table `incoming_parcels`
--

INSERT INTO `incoming_parcels` (`id`, `online_id`, `agency_id`, `global_parcel_id`, `schedule_id`, `bus_number`, `receiver_name`, `receiver_phone`, `collected_by`, `collected_on`, `description`, `from`, `to`, `price`, `sent_date`, `state`, `CreatedBy`, `CreatedOn`) VALUES
(1, 0, 105, 1055, 1050, '', 'Acha Nelson Tekeh', 674815874, '', '0000-00-00 00:00:00', 'bag', 105, 104, 1000, '2015-06-15', '', 'NELSON', '2015-06-15 11:35:14'),
(2, 0, 105, 1052, 1050, '', 'Nelson', 696362464, '{"name":"Nelson Tekeh","idc":"5212020221","phone":"6965522233"}', '2015-09-12 14:08:05', 'Tricks', 105, 104, 500, '2015-09-12', 'delivered', 'ABSA', '2015-09-12 13:31:28'),
(3, 0, 106, 10612, 1060, '', 'Absalom Ngwabinkaa', 696362464, '{"name":"Absa","idc":"102322121","phone":"696362554"}', '2016-10-10 02:37:32', 'Wrist watch', 106, 105, 3000, '2016-10-08', 'delivered', 'ACHA', '2016-10-08 09:49:11'),
(4, 0, 106, 10615, 1060, '', 'Kilo', 0, '', '2016-10-10 07:17:59', 'Beans', 106, 105, 200, '2016-10-10', 'received', 'ACHA', '2016-10-10 10:46:59');

-- --------------------------------------------------------

--
-- Table structure for table `incoming_schedules`
--

CREATE TABLE IF NOT EXISTS `incoming_schedules` (
  `id` int(12) NOT NULL,
  `agency_id` int(12) NOT NULL,
  `from_agency_id` int(12) NOT NULL,
  `agency_code` varchar(255) NOT NULL,
  `bus_number` varchar(255) NOT NULL,
  `bus_seats` int(255) NOT NULL COMMENT '?? seater',
  `from` int(11) NOT NULL,
  `to` int(11) NOT NULL,
  `ticket_price` int(12) NOT NULL,
  `seat_occupants` mediumtext NOT NULL,
  `reserved_seats` mediumtext NOT NULL,
  `status` enum('current','departed') NOT NULL DEFAULT 'current',
  `loading` enum('0','1','2') NOT NULL DEFAULT '0' COMMENT '1-Boarding 2-Delayed',
  `departure_date` date NOT NULL,
  `checked_out_on` datetime NOT NULL,
  `departure_time` time NOT NULL,
  `checked_out_time` time NOT NULL COMMENT 'Time bus was checked out',
  `total_amount` int(10) NOT NULL COMMENT 'Total amount got from this schedule',
  `CreatedBy` varchar(255) NOT NULL,
  `CreatedOn` datetime NOT NULL,
  `CheckedOutBy` varchar(255) NOT NULL,
  `deleted` enum('0','1') NOT NULL DEFAULT '0',
  PRIMARY KEY (`id`),
  KEY `agency_id` (`from_agency_id`),
  KEY `to` (`to`),
  KEY `from` (`from`)
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Dumping data for table `incoming_schedules`
--

INSERT INTO `incoming_schedules` (`id`, `agency_id`, `from_agency_id`, `agency_code`, `bus_number`, `bus_seats`, `from`, `to`, `ticket_price`, `seat_occupants`, `reserved_seats`, `status`, `loading`, `departure_date`, `checked_out_on`, `departure_time`, `checked_out_time`, `total_amount`, `CreatedBy`, `CreatedOn`, `CheckedOutBy`, `deleted`) VALUES
(106109, 105, 106, '', 'NW111', 70, 4, 3, 5000, '', '', 'current', '0', '2017-01-07', '0000-00-00 00:00:00', '20:00:00', '00:00:00', 0, 'ACHA', '0000-00-00 00:00:00', '', '1'),
(106110, 105, 106, '', 'NW111', 70, 4, 3, 2000, '{"2":{"name":"Ss","phone":"","email":"","idc":"","free_ticket":0}}', '', 'departed', '0', '2017-01-07', '2017-01-07 00:00:00', '20:00:00', '05:20:17', 2000, 'ACHA', '0000-00-00 00:00:00', 'ACHA', '0');

-- --------------------------------------------------------

--
-- Table structure for table `main_modules`
--

CREATE TABLE IF NOT EXISTS `main_modules` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `Name` varchar(255) NOT NULL,
  `Url` varchar(255) NOT NULL,
  `IsActive` enum('0','1') NOT NULL DEFAULT '0',
  PRIMARY KEY (`id`)
) ENGINE=InnoDB  DEFAULT CHARSET=latin1 AUTO_INCREMENT=10 ;

--
-- Dumping data for table `main_modules`
--

INSERT INTO `main_modules` (`id`, `Name`, `Url`, `IsActive`) VALUES
(1, 'Home', 'admin', '1'),
(2, 'Schedules', 'admin/all_schedules/current', '1'),
(3, 'Expenses', 'expenses/all', '1'),
(4, 'Parcels', 'parcels/all/incoming', '1'),
(5, 'Buses', 'buses/all', '1'),
(6, 'Reminders', 'reminders/all', '1'),
(7, 'Reports', 'reports', '1'),
(8, 'Cash', 'cash', '1'),
(9, 'Settings', 'settings', '1');

-- --------------------------------------------------------

--
-- Table structure for table `modules`
--

CREATE TABLE IF NOT EXISTS `modules` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `Name` varchar(255) NOT NULL,
  `MainModuleID` int(11) NOT NULL,
  `IsActive` enum('0','1') NOT NULL DEFAULT '0',
  PRIMARY KEY (`id`)
) ENGINE=InnoDB DEFAULT CHARSET=latin1 AUTO_INCREMENT=1 ;

-- --------------------------------------------------------

--
-- Table structure for table `online_purchases`
--

CREATE TABLE IF NOT EXISTS `online_purchases` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `agency_id` int(11) NOT NULL,
  `is_open_ticket` enum('0','1') NOT NULL,
  `schedule_id` int(11) NOT NULL,
  `town_from` int(11) NOT NULL,
  `town_to` int(11) NOT NULL,
  `ot_ticket_name` varchar(255) NOT NULL,
  `ot_ticket_price` int(11) NOT NULL,
  `client_name` varchar(255) NOT NULL,
  `client_phone` int(11) NOT NULL,
  `client_email` varchar(255) NOT NULL,
  `client_idc` int(11) NOT NULL,
  `amount_paid` int(11) NOT NULL,
  `status` enum('active','closed','cancelled') NOT NULL,
  `CreatedBy` varchar(25) NOT NULL,
  `CreatedOn` datetime NOT NULL,
  `ModifiedBy` varchar(25) NOT NULL,
  `ModifiedOn` datetime NOT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB  DEFAULT CHARSET=latin1 AUTO_INCREMENT=6 ;

--
-- Dumping data for table `online_purchases`
--

INSERT INTO `online_purchases` (`id`, `agency_id`, `is_open_ticket`, `schedule_id`, `town_from`, `town_to`, `ot_ticket_name`, `ot_ticket_price`, `client_name`, `client_phone`, `client_email`, `client_idc`, `amount_paid`, `status`, `CreatedBy`, `CreatedOn`, `ModifiedBy`, `ModifiedOn`) VALUES
(1, 105, '0', 10520, 3, 4, '', 0, 'Quincy Kwende', 658789652, 'quicyk@gmail.com', 123025485, 3000, 'cancelled', '', '2015-09-02 00:00:00', '', '0000-00-00 00:00:00'),
(2, 105, '1', 0, 3, 4, 'Classic', 2500, 'Chamba Alice', 685956322, 'absalomshu@gmail.com', 108310175, 2500, 'cancelled', '', '2015-09-02 10:47:11', '', '0000-00-00 00:00:00'),
(3, 105, '0', 10520, 3, 4, '', 0, 'Quincy Kwende', 658789652, 'Ngan@doh.com', 2147483647, 3000, 'cancelled', '', '2015-09-03 00:00:00', '', '0000-00-00 00:00:00'),
(4, 105, '1', 0, 3, 4, 'Classic', 2500, 'Shu Absalom', 696362464, 'solo@ngongi.com', 1085623322, 2500, 'cancelled', '', '2015-11-16 12:06:31', '', '0000-00-00 00:00:00'),
(5, 105, '1', 0, 3, 4, 'Classic', 2500, 'Halil', 696362464, 'ha@li.l', 102365545, 0, 'cancelled', '', '2016-02-26 09:36:43', '', '0000-00-00 00:00:00');

-- --------------------------------------------------------

--
-- Table structure for table `open_tickets`
--

CREATE TABLE IF NOT EXISTS `open_tickets` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `agency_id` int(11) NOT NULL,
  `open_ticket_name` varchar(255) NOT NULL,
  `town_from` int(11) NOT NULL,
  `town_to` int(11) NOT NULL,
  `price` int(11) NOT NULL,
  `deleted` enum('0','1') NOT NULL DEFAULT '0',
  `CreatedBy` varchar(255) NOT NULL,
  `CreatedOn` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB  DEFAULT CHARSET=latin1 AUTO_INCREMENT=4 ;

--
-- Dumping data for table `open_tickets`
--

INSERT INTO `open_tickets` (`id`, `agency_id`, `open_ticket_name`, `town_from`, `town_to`, `price`, `deleted`, `CreatedBy`, `CreatedOn`) VALUES
(1, 105, 'Viasso VIP', 3, 4, 8000, '0', '', '2016-02-03 10:46:33'),
(2, 105, 'Viasso Classic', 3, 4, 4000, '0', '', '2016-05-07 03:59:39'),
(3, 106, 'Classic', 4, 3, 4000, '0', '', '2016-05-19 03:50:50');

-- --------------------------------------------------------

--
-- Table structure for table `parcels`
--

CREATE TABLE IF NOT EXISTS `parcels` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `agency_id` int(11) NOT NULL,
  `schedule_id` int(11) NOT NULL,
  `bus_number` varchar(25) NOT NULL,
  `sender_name` varchar(255) NOT NULL,
  `receiver_name` varchar(255) NOT NULL,
  `receiver_phone` int(11) NOT NULL,
  `collected_by` varchar(500) NOT NULL,
  `collected_on` datetime NOT NULL,
  `description` text NOT NULL,
  `from` int(11) NOT NULL COMMENT 'id of agency of origin of parcel',
  `to` int(11) NOT NULL COMMENT 'id of destination agency of parcel',
  `price` int(255) NOT NULL,
  `type` enum('mail','baggage') NOT NULL,
  `registered_date` date NOT NULL,
  `sent_date` date NOT NULL,
  `state` enum('registered','sent','received','delivered') NOT NULL,
  `CreatedBy` varchar(25) NOT NULL,
  `CreatedOn` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP,
  PRIMARY KEY (`id`),
  KEY `agency_id` (`agency_id`),
  KEY `to` (`to`),
  KEY `from` (`from`)
) ENGINE=InnoDB  DEFAULT CHARSET=latin1 AUTO_INCREMENT=24 ;

--
-- Dumping data for table `parcels`
--

INSERT INTO `parcels` (`id`, `agency_id`, `schedule_id`, `bus_number`, `sender_name`, `receiver_name`, `receiver_phone`, `collected_by`, `collected_on`, `description`, `from`, `to`, `price`, `type`, `registered_date`, `sent_date`, `state`, `CreatedBy`, `CreatedOn`) VALUES
(1, 105, 32, 'CE123AB', 'Absalom Shu', 'Mapoli Ngono', 695874584, '', '0000-00-00 00:00:00', 'Beans', 105, 104, 2500, 'mail', '0000-00-00', '2015-09-12', 'registered', 'ABSA', '2015-09-12 11:11:30'),
(2, 105, 0, '', 'Absalom', 'Nelson', 696362464, '', '0000-00-00 00:00:00', 'Tricks', 105, 104, 500, 'mail', '2015-09-12', '2015-09-12', 'sent', 'ABSA', '2015-09-12 12:31:28'),
(3, 105, 83, 'CE123AB', 'Absalom Shu', 'Mapoli Ngono', 696362464, '', '0000-00-00 00:00:00', 'none', 105, 104, 2500, 'mail', '2015-09-15', '2015-09-15', 'sent', 'ABSA', '2015-09-15 09:02:25'),
(4, 105, 96, 'CE123AB', 'Absalom', 'Nelson', 0, '', '0000-00-00 00:00:00', 'Cellular phone', 105, 104, 2500, 'mail', '2016-09-08', '2016-09-08', 'sent', 'ABSA', '2016-09-08 00:23:58'),
(5, 105, 98, 'CE123AB', 'Absalom', 'Nelson', 687878787, '', '0000-00-00 00:00:00', 'Big blue band', 105, 104, 1000, 'mail', '2016-09-20', '2016-09-20', 'sent', 'ABSA', '2016-09-20 10:40:06'),
(8, 105, 101, 'CE123AB', 'Shu', 'Binka', 0, '', '0000-00-00 00:00:00', 'shirts', 105, 104, 2000, 'mail', '0000-00-00', '2016-09-21', 'registered', 'ABSA', '2016-09-21 06:53:46'),
(9, 105, 0, '', '', 'Pilo', 696362646, '', '0000-00-00 00:00:00', 'Bananas', 105, 104, 500, 'baggage', '0000-00-00', '2016-09-22', 'registered', 'ABSA', '2016-09-22 02:27:06'),
(10, 105, 0, '', 'Shich', 'Aha', 0, '', '0000-00-00 00:00:00', 'Noda', 105, 104, 2000, 'mail', '0000-00-00', '2016-10-06', 'registered', 'ABSA', '2016-10-06 10:48:00'),
(11, 106, 0, '', 'Nelson', 'Absalom Ngwabinkaa', 696362464, '', '0000-00-00 00:00:00', 'Wrist watch', 106, 104, 2000, 'baggage', '2016-10-08', '2016-10-08', 'sent', 'ACHA', '2016-10-08 00:46:11'),
(12, 106, 0, '', 'Nelson', 'Absalom Ngwabinkaa', 696362464, '', '0000-00-00 00:00:00', 'Wrist watch', 106, 105, 3000, 'mail', '2016-10-08', '2016-10-08', 'sent', 'ACHA', '2016-10-08 00:49:11'),
(13, 105, 0, '', 'Absalom', 'Kilo', 0, '', '0000-00-00 00:00:00', 'Log', 105, 104, 500, 'mail', '0000-00-00', '2016-10-08', 'registered', 'ABSA', '2016-10-08 11:55:28'),
(14, 105, 0, '', '', 'Nelson', 0, '', '0000-00-00 00:00:00', 'b', 105, 104, 12, 'mail', '0000-00-00', '2016-10-09', 'registered', 'ABSA', '2016-10-09 00:42:37'),
(15, 106, 0, '', '', 'Kilo', 0, '', '0000-00-00 00:00:00', 'Beans', 106, 105, 200, 'mail', '2016-10-10', '2016-10-10', 'sent', 'ACHA', '2016-10-10 01:46:59'),
(16, 105, 0, '', '', 'Nelson', 0, '', '0000-00-00 00:00:00', 'b', 105, 104, 100, 'mail', '0000-00-00', '2016-10-10', 'registered', 'ABSA', '2016-10-10 02:52:10'),
(17, 105, 0, '', 'Absalom', 'Kilo', 0, '', '0000-00-00 00:00:00', '2308, Somei House, University of Aizu', 105, 104, 2000, 'mail', '0000-00-00', '2016-10-18', 'registered', 'ABSA', '2016-10-18 04:45:44'),
(18, 105, 0, '', 'Absalom', 'Kilo', 0, '', '0000-00-00 00:00:00', '2308, Somei House, University of Aizu', 105, 104, 344, 'mail', '0000-00-00', '2016-10-18', 'registered', 'ABSA', '2016-10-18 04:45:54'),
(19, 105, 0, '', 'Milo', 'Raonic', 0, '', '0000-00-00 00:00:00', 'Yes', 105, 104, 2500, 'mail', '2016-12-07', '2016-12-07', 'registered', 'ABSA', '2016-12-07 14:15:52'),
(20, 105, 0, '', 'Milo', 'Raonic', 0, '', '0000-00-00 00:00:00', 'Yes', 105, 104, 2500, 'mail', '2016-12-07', '0000-00-00', 'registered', 'ABSA', '2016-12-07 14:16:44'),
(21, 105, 0, '', 'Milo', 'Raonic', 0, '', '0000-00-00 00:00:00', 'Test', 105, 104, 2500, 'mail', '2016-12-07', '0000-00-00', 'registered', 'ABSA', '2016-12-07 14:17:13'),
(22, 105, 0, '', 'Shu', 'Absalom', 0, '', '0000-00-00 00:00:00', 'Trouser', 105, 104, 500, 'mail', '2016-12-16', '0000-00-00', 'sent', 'ABSA', '2016-12-16 12:55:36'),
(23, 105, 0, '', 'Mali', 'Guinea', 0, '', '0000-00-00 00:00:00', 'Porto', 105, 104, 50000, 'mail', '2016-12-16', '0000-00-00', 'registered', 'ABSA', '2016-12-16 13:23:24');

--
-- Triggers `parcels`
--
DROP TRIGGER IF EXISTS `insert_parcel_transaction`;
DELIMITER //
CREATE TRIGGER `insert_parcel_transaction` AFTER INSERT ON `parcels`
 FOR EACH ROW BEGIN
	SET @change_type = 'insert';
	INSERT INTO transactions (	`table`, agency_id, change_id, change_type, receiver_name, receiver_phone, collected_by, collected_on, description, `from`, `to`, price, sent_date, parcel_state, CreatedBy, CreatedOn, bus_number, schedule_id) 			
					VALUES (	'parcels', NEW.agency_id, NEW.id, @change_type, NEW.receiver_name, NEW.receiver_phone, NEW.collected_by, NEW.collected_on, NEW.description, NEW.from, NEW.to, NEW.price, NEW.sent_date, 
								NEW.state, NEW.CreatedBy, NEW.CreatedOn, NEW.bus_number, NEW.schedule_id);
END
//
DELIMITER ;
DROP TRIGGER IF EXISTS `update_parcel_transaction`;
DELIMITER //
CREATE TRIGGER `update_parcel_transaction` AFTER UPDATE ON `parcels`
 FOR EACH ROW BEGIN
	SET @change_type = 'update';
	INSERT INTO transactions (	`table`, agency_id, change_id, change_type, receiver_name, receiver_phone, collected_by, collected_on, description, `from`, `to`, price, sent_date, parcel_state, CreatedBy, CreatedOn, bus_number, schedule_id) 			
					VALUES (	'parcels', NEW.agency_id, NEW.id, @change_type, NEW.receiver_name, NEW.receiver_phone, NEW.collected_by, NEW.collected_on, NEW.description, NEW.from, NEW.to, NEW.price, NEW.sent_date, 
								NEW.state, NEW.CreatedBy, NEW.CreatedOn, NEW.bus_number, NEW.schedule_id);
END
//
DELIMITER ;

-- --------------------------------------------------------

--
-- Table structure for table `parents`
--

CREATE TABLE IF NOT EXISTS `parents` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `name` varchar(255) NOT NULL,
  `logo` varchar(30) NOT NULL,
  `contacts` varchar(255) NOT NULL,
  `ticket_info` varchar(255) NOT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB  DEFAULT CHARSET=latin1 AUTO_INCREMENT=16 ;

--
-- Dumping data for table `parents`
--

INSERT INTO `parents` (`id`, `name`, `logo`, `contacts`, `ticket_info`) VALUES
(1, 'Viasso', '', '', ''),
(2, 'Amour Mezam', '', '', ''),
(3, 'Musango', '', '', ''),
(4, 'Grand Jeannot', '', '', ''),
(5, 'Original Jean Jeannot', '', '', ''),
(6, 'Vatican', '', '', ''),
(7, 'Patience', '', '', ''),
(8, 'Moghamo', '', '', ''),
(9, 'Binam', '', '', ''),
(10, 'Touristique', '', '', 'Something something'),
(11, 'Mondial', '', '', ''),
(12, 'Garanti', '', '', ''),
(13, 'Bucca', '', '', ''),
(14, 'Euroline', '', '', ''),
(15, 'Le Voyageur', '', '', '');

-- --------------------------------------------------------

--
-- Table structure for table `purchases`
--

CREATE TABLE IF NOT EXISTS `purchases` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `agency_id` int(11) NOT NULL,
  `is_open_ticket` enum('0','1') NOT NULL DEFAULT '0',
  `schedule_id` int(11) NOT NULL,
  `town_from` int(11) NOT NULL,
  `town_to` int(11) NOT NULL COMMENT 'Open ticket town to',
  `ot_ticket_name` varchar(255) NOT NULL,
  `ot_ticket_price` int(11) NOT NULL,
  `client_name` varchar(255) NOT NULL,
  `client_phone` int(11) NOT NULL,
  `client_email` varchar(255) NOT NULL,
  `client_idc` int(11) NOT NULL,
  `amount_paid` int(11) NOT NULL,
  `status` enum('initiated','active','closed','cancelled') NOT NULL,
  `downloaded` enum('0','1') NOT NULL DEFAULT '0',
  `CreatedBy` varchar(25) NOT NULL,
  `CreatedOn` datetime NOT NULL,
  `ModifiedBy` varchar(25) NOT NULL,
  `ModifiedOn` datetime NOT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB  DEFAULT CHARSET=latin1 AUTO_INCREMENT=20 ;

--
-- Dumping data for table `purchases`
--

INSERT INTO `purchases` (`id`, `agency_id`, `is_open_ticket`, `schedule_id`, `town_from`, `town_to`, `ot_ticket_name`, `ot_ticket_price`, `client_name`, `client_phone`, `client_email`, `client_idc`, `amount_paid`, `status`, `downloaded`, `CreatedBy`, `CreatedOn`, `ModifiedBy`, `ModifiedOn`) VALUES
(1, 105, '0', 20, 3, 4, '', 0, 'John Paul', 989856555, 'abs@gmail.com', 666565556, 3000, 'active', '0', '', '2015-09-02 00:00:00', '', '0000-00-00 00:00:00'),
(2, 105, '1', 0, 3, 4, 'Viasso VIP', 8000, '', 0, '', 0, 0, 'initiated', '0', '', '2016-02-03 11:46:45', '', '0000-00-00 00:00:00'),
(3, 105, '1', 0, 3, 4, 'Viasso VIP', 8000, 'abas', 696326854, 'absalomshu@gmail.com', 102510114, 8000, 'active', '0', '', '2016-02-26 07:48:08', 'ABAS', '2016-02-26 08:09:55'),
(4, 105, '0', 84, 3, 4, '', 0, 'Suh', 687878787, 'peter@crouch.com', 102547885, 5000, 'active', '0', '', '2016-02-26 00:00:00', '', '0000-00-00 00:00:00'),
(5, 105, '1', 0, 3, 4, 'Viasso VIP', 8000, 'jjj', 687878787, 'peter@crouch.com', 1111111111, 0, 'initiated', '0', '', '2016-02-26 08:32:10', 'JJJ', '2016-02-26 08:41:48'),
(6, 105, '1', 0, 3, 4, 'Viasso VIP', 8000, 'fff', 687878787, 'peter@crouch.com', 111111111, 0, 'initiated', '0', '', '2016-02-26 08:50:50', 'FFF', '2016-02-26 08:51:01'),
(7, 105, '1', 0, 3, 4, 'Viasso VIP', 8000, '', 0, '', 0, 0, 'initiated', '0', '', '2016-04-24 00:56:35', '', '0000-00-00 00:00:00'),
(8, 105, '1', 0, 3, 4, 'Viasso VIP', 8000, '', 0, '', 0, 0, 'initiated', '0', '', '2016-04-24 01:24:50', '', '0000-00-00 00:00:00'),
(9, 105, '1', 0, 3, 4, 'Viasso VIP', 8000, '', 0, '', 0, 0, 'initiated', '0', '', '2016-04-24 01:28:50', '', '0000-00-00 00:00:00'),
(10, 105, '1', 0, 3, 4, 'Viasso VIP', 8000, 'Pope', 632145875, '', 120120122, 0, 'active', '0', '', '2016-04-24 01:31:45', 'POPE', '2016-04-24 01:32:58'),
(11, 105, '1', 0, 3, 4, 'Viasso VIP', 8000, '', 0, '', 0, 0, 'initiated', '0', '', '2016-05-07 04:22:52', '', '0000-00-00 00:00:00'),
(12, 105, '1', 0, 3, 4, 'Viasso VIP', 8000, '', 0, '', 0, 0, 'initiated', '0', '', '2016-05-07 05:17:01', '', '0000-00-00 00:00:00'),
(13, 105, '1', 0, 3, 4, 'Viasso VIP', 8000, '', 0, '', 0, 0, 'initiated', '0', '', '2016-05-07 05:22:07', '', '0000-00-00 00:00:00'),
(14, 105, '1', 0, 3, 4, 'Viasso VIP', 8000, '', 0, '', 0, 0, 'initiated', '0', '', '2016-05-07 05:26:18', '', '0000-00-00 00:00:00'),
(15, 105, '1', 0, 3, 4, 'Viasso VIP', 8000, '', 0, '', 0, 0, 'initiated', '0', '', '2016-05-13 07:40:22', '', '0000-00-00 00:00:00'),
(16, 105, '1', 0, 3, 4, 'Viasso VIP', 8000, '', 0, '', 0, 0, 'initiated', '0', '', '2016-05-16 09:36:04', '', '0000-00-00 00:00:00'),
(17, 105, '1', 0, 3, 4, 'Viasso VIP', 8000, '', 0, '', 0, 0, 'initiated', '0', '', '2016-05-16 09:43:08', '', '0000-00-00 00:00:00'),
(18, 105, '1', 0, 3, 4, 'Viasso VIP', 8000, '', 0, '', 0, 0, 'initiated', '0', '', '2016-05-16 09:47:11', '', '0000-00-00 00:00:00'),
(19, 105, '1', 0, 3, 4, 'Viasso VIP', 8000, '', 0, '', 0, 0, 'initiated', '0', '', '2016-05-16 10:04:50', '', '0000-00-00 00:00:00');

-- --------------------------------------------------------

--
-- Table structure for table `reminders`
--

CREATE TABLE IF NOT EXISTS `reminders` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `Name` varchar(255) NOT NULL,
  `AgencyID` int(12) NOT NULL,
  `BusNumber` varchar(15) DEFAULT NULL,
  `ScheduleID` int(11) NOT NULL,
  `Amount` int(11) NOT NULL,
  `Description` text NOT NULL,
  `StartDate` date DEFAULT NULL,
  `DueDate` date DEFAULT NULL,
  `Recurring` enum('0','1') NOT NULL,
  `IntervalValue` int(11) NOT NULL COMMENT 'Number of days/months',
  `IntervalType` enum('D','M') NOT NULL COMMENT 'Type: D-Days or M-Months',
  `DaysTo` int(11) NOT NULL,
  `IsPersonal` enum('0','1') NOT NULL,
  `Status` enum('0','1') NOT NULL DEFAULT '1' COMMENT '1 is active, 0 is completed',
  `CreatedBy` varchar(25) NOT NULL,
  `CreatedOn` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP,
  PRIMARY KEY (`id`),
  KEY `agency_id` (`AgencyID`)
) ENGINE=InnoDB  DEFAULT CHARSET=latin1 AUTO_INCREMENT=11 ;

--
-- Dumping data for table `reminders`
--

INSERT INTO `reminders` (`id`, `Name`, `AgencyID`, `BusNumber`, `ScheduleID`, `Amount`, `Description`, `StartDate`, `DueDate`, `Recurring`, `IntervalValue`, `IntervalType`, `DaysTo`, `IsPersonal`, `Status`, `CreatedBy`, `CreatedOn`) VALUES
(4, 'Garage work', 105, 'CE123AB', 0, 15000, 'TEst', NULL, '2016-12-23', '0', 0, '', 0, '', '1', 'absa', '2016-12-23 06:02:41'),
(5, 'Garage work', 105, 'NW989MO', 0, 15000, 'TEst', NULL, '2016-12-23', '0', 0, '', 0, '', '1', 'absa', '2016-12-23 06:02:41'),
(6, 'Garage work', 105, 'AB111A', 0, 15000, 'TEst', NULL, '2016-12-23', '0', 0, '', 0, '', '1', 'absa', '2016-12-23 06:02:41'),
(7, 'Garage work', 105, 'UNKNOWN', 0, 15000, 'TEst', NULL, '2016-12-23', '0', 0, '', 0, '', '1', 'absa', '2016-12-23 06:02:41'),
(8, 'Garage work', 105, 'NW222', 0, 15000, 'TEst', NULL, '2016-12-23', '0', 0, '', 0, '', '1', 'absa', '2016-12-23 06:02:41'),
(9, 'More', 105, 'NW989MO', 0, 0, '', NULL, '2016-12-23', '0', 0, '', 0, '', '1', 'absa', '2016-12-23 06:04:03'),
(10, 'More', 105, 'NW222', 0, 0, '', NULL, '2016-12-23', '0', 0, '', 0, '', '1', 'absa', '2016-12-23 06:04:03');

-- --------------------------------------------------------

--
-- Table structure for table `reminder_schedules`
--

CREATE TABLE IF NOT EXISTS `reminder_schedules` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `AgencyID` int(11) NOT NULL,
  `ReminderID` int(11) NOT NULL,
  `DueDate` date NOT NULL,
  `IsCompleted` enum('0','1') NOT NULL DEFAULT '0',
  `ModifiedBy` varchar(255) NOT NULL,
  PRIMARY KEY (`id`),
  KEY `AgencyID` (`AgencyID`),
  KEY `ReminderID` (`ReminderID`)
) ENGINE=InnoDB  DEFAULT CHARSET=latin1 AUTO_INCREMENT=11 ;

--
-- Dumping data for table `reminder_schedules`
--

INSERT INTO `reminder_schedules` (`id`, `AgencyID`, `ReminderID`, `DueDate`, `IsCompleted`, `ModifiedBy`) VALUES
(4, 105, 4, '2016-12-23', '0', ''),
(5, 105, 5, '2016-12-23', '0', ''),
(6, 105, 6, '2016-12-23', '0', ''),
(7, 105, 7, '2016-12-23', '0', ''),
(8, 105, 8, '2016-12-23', '0', ''),
(9, 105, 9, '2016-12-23', '0', ''),
(10, 105, 10, '2016-12-23', '0', '');

-- --------------------------------------------------------

--
-- Table structure for table `reservation_requests`
--

CREATE TABLE IF NOT EXISTS `reservation_requests` (
  `id` int(12) NOT NULL AUTO_INCREMENT,
  `agency_id` int(11) NOT NULL,
  `schedule_id` int(12) NOT NULL,
  `reserved_seats` mediumtext NOT NULL,
  `reserved_seats_permanent` mediumtext NOT NULL,
  `from_mobile_app` mediumtext NOT NULL,
  PRIMARY KEY (`id`),
  KEY `schedule_id` (`schedule_id`)
) ENGINE=InnoDB  DEFAULT CHARSET=latin1 AUTO_INCREMENT=37 ;

--
-- Dumping data for table `reservation_requests`
--

INSERT INTO `reservation_requests` (`id`, `agency_id`, `schedule_id`, `reserved_seats`, `reserved_seats_permanent`, `from_mobile_app`) VALUES
(32, 105, 96, '{"2.1":{"name":"Absalom Shu","phone":"696362545","email":"absalomshu@gmail.com","idc":"108310175"}}', '{"16":{"name":"Shu Absalom","phone":"696362464","email":"absalomshu@gmail.com","idc":"108310175"},"2":{"name":"Mary Chen","phone":"696365254","email":"mary@chen.com","idc":"102458563"},"2.1":{"name":"Absalom Shu","phone":"696362545","email":"absalomshu@gmail.com","idc":"108310175"}}', ''),
(35, 105, 98, '{"0":{"name":"Shu","phone":"696362464","email":"","idc":""},"2.1":{"name":"Shu","phone":"696362464","email":"","idc":""},"2.2":{"name":"Shu","phone":"696362464","email":"","idc":""},"2.3":{"name":"Shu","phone":"696362464","email":"","idc":""},"2.4":{"name":"Shu","phone":"696362464","email":"","idc":""},"2.5":{"name":"Shu","phone":"696362464","email":"","idc":""},"2.6":{"name":"Shu","phone":"696362464","email":"","idc":""},"2.7":{"name":"Shu","phone":"696362464","email":"","idc":""},"2.8":{"name":"Shu","phone":"696362464","email":"","idc":""},"2.9":{"name":"Shu","phone":"696362464","email":"","idc":""},"2.10":{"name":"Shu","phone":"696362464","email":"","idc":""},"2.11":{"name":"Che","phone":"696584788","email":"","idc":""},"2.12":{"name":"Che","phone":"696584788","email":"","idc":""},"2.13":{"name":"Che","phone":"696584788","email":"","idc":""},"2.14":{"name":"Che","phone":"696584788","email":"","idc":""}}', '{"2":{"name":"Shu","phone":"696362464","email":"","idc":""},"2.1":{"name":"Shu","phone":"696362464","email":"","idc":""},"2.2":{"name":"Shu","phone":"696362464","email":"","idc":""},"2.3":{"name":"Shu","phone":"696362464","email":"","idc":""},"2.4":{"name":"Shu","phone":"696362464","email":"","idc":""},"2.5":{"name":"Shu","phone":"696362464","email":"","idc":""},"2.6":{"name":"Shu","phone":"696362464","email":"","idc":""},"2.7":{"name":"Shu","phone":"696362464","email":"","idc":""},"2.8":{"name":"Shu","phone":"696362464","email":"","idc":""},"2.9":{"name":"Shu","phone":"696362464","email":"","idc":""},"2.10":{"name":"Shu","phone":"696362464","email":"","idc":""},"2.11":{"name":"Che","phone":"696584788","email":"","idc":""},"2.12":{"name":"Che","phone":"696584788","email":"","idc":""},"2.13":{"name":"Che","phone":"696584788","email":"","idc":""},"2.14":{"name":"Che","phone":"696584788","email":"","idc":""}}', ''),
(36, 105, 107, '{"0":{"name":"shu","phone":"7043963923","email":"absalomshu@gmail.com","idc":"108310175"},"2.1":{"name":"shu","phone":"7043963923","email":"absalomshu@gmail.com","idc":"dds"},"2.2":{"name":"shu","phone":"7043963923","email":"absalomshu@gmail.com","idc":"dds"},"2.3":{"name":"shu","phone":"7043963923","email":"absalomshu@gmail.com","idc":"dds"},"1":{"name":"sHU","phone":"696362464","email":"absalomshu@gmail.com","idc":"108310175"},"2.4":{"name":"shu","phone":"7043963923","email":"absalomshu@gmail.com","idc":"dds"},"2.5":{"name":"shu","phone":"7043963923","email":"absalomshu@gmail.com","idc":"dds"},"2.6":{"name":"shu","phone":"7043963923","email":"absalomshu@gmail.com","idc":"dds"},"2.7":{"name":"shu","phone":"7043963923","email":"absalomshu@gmail.com","idc":"dds"},"2.8":{"name":"shu","phone":"7043963923","email":"absalomshu@gmail.com","idc":"dds"},"2.9":{"name":"shu","phone":"66666666","email":"absalomshu@gmail.com","idc":"108310175"},"2.10":{"name":"absa","phone":"7043963923","email":"absalomshu@gmail.com","idc":"dds"},"2.11":{"name":"absa","phone":"7043963923","email":"absalomshu@gmail.com","idc":"asdf"},"2.12":{"name":"Absalom","phone":"696362464","email":"absalomshu@gmail.com","idc":"108322222"},"2.13":{"name":"Absalom","phone":"696362464","email":"absalomshu@gmail.com","idc":"108310122"},"2.14":{"name":"absalom","phone":"696362464","email":"absalomshu@gmail.com","idc":"108310175"},"2.15":{"name":"Shu","phone":"898989898","email":"absalomshu@gmail.com","idc":"108562321"},"2.16":{"name":"absalom","phone":"999999999","email":"","idc":"999999999"},"2.17":{"name":"absalom","phone":"999999999","email":"","idc":"999999999"},"2.18":{"name":"absalom","phone":"999999999","email":"","idc":"999999999"},"2.19":{"name":"absalom","phone":"999999999","email":"","idc":"999999999"},"2.20":{"name":"absalom","phone":"999999999","email":"","idc":"999999999"},"2.21":{"name":"absalom","phone":"999999999","email":"","idc":"999999999"},"2.22":{"name":"absalom","phone":"999999999","email":"","idc":"999999999"},"2.23":{"name":"absalom","phone":"999999999","email":"","idc":"999999999"},"2.24":{"name":"absalom","phone":"999999999","email":"","idc":"999999999"},"2.25":{"name":"absalom","phone":"55555555","email":"","idc":"55555555"}}', '{"2":{"name":"sHU","phone":"696362464","email":"absalomshu@gmail.com","idc":"108310175"},"2.1":{"name":"shu","phone":"7043963923","email":"absalomshu@gmail.com","idc":"dds"},"2.2":{"name":"shu","phone":"7043963923","email":"absalomshu@gmail.com","idc":"dds"},"2.3":{"name":"shu","phone":"7043963923","email":"absalomshu@gmail.com","idc":"dds"},"2.4":{"name":"shu","phone":"7043963923","email":"absalomshu@gmail.com","idc":"dds"},"2.5":{"name":"shu","phone":"7043963923","email":"absalomshu@gmail.com","idc":"dds"},"2.6":{"name":"shu","phone":"7043963923","email":"absalomshu@gmail.com","idc":"dds"},"2.7":{"name":"shu","phone":"7043963923","email":"absalomshu@gmail.com","idc":"dds"},"2.8":{"name":"shu","phone":"7043963923","email":"absalomshu@gmail.com","idc":"dds"},"2.9":{"name":"shu","phone":"66666666","email":"absalomshu@gmail.com","idc":"108310175"},"2.10":{"name":"absa","phone":"7043963923","email":"absalomshu@gmail.com","idc":"dds"},"2.11":{"name":"absa","phone":"7043963923","email":"absalomshu@gmail.com","idc":"asdf"},"2.12":{"name":"Absalom","phone":"696362464","email":"absalomshu@gmail.com","idc":"108322222"},"2.13":{"name":"Absalom","phone":"696362464","email":"absalomshu@gmail.com","idc":"108310122"},"2.14":{"name":"absalom","phone":"696362464","email":"absalomshu@gmail.com","idc":"108310175"},"2.15":{"name":"Shu","phone":"898989898","email":"absalomshu@gmail.com","idc":"108562321"},"2.16":{"name":"absalom","phone":"999999999","email":"","idc":"999999999"},"2.17":{"name":"absalom","phone":"999999999","email":"","idc":"999999999"},"2.18":{"name":"absalom","phone":"999999999","email":"","idc":"999999999"},"2.19":{"name":"absalom","phone":"999999999","email":"","idc":"999999999"},"2.20":{"name":"absalom","phone":"999999999","email":"","idc":"999999999"},"2.21":{"name":"absalom","phone":"999999999","email":"","idc":"999999999"},"2.22":{"name":"absalom","phone":"999999999","email":"","idc":"999999999"},"2.23":{"name":"absalom","phone":"999999999","email":"","idc":"999999999"},"2.24":{"name":"absalom","phone":"999999999","email":"","idc":"999999999"},"2.25":{"name":"absalom","phone":"55555555","email":"","idc":"55555555"}}', '');

-- --------------------------------------------------------

--
-- Table structure for table `schedules`
--

CREATE TABLE IF NOT EXISTS `schedules` (
  `id` int(12) NOT NULL AUTO_INCREMENT,
  `agency_id` int(12) NOT NULL,
  `agency_code` varchar(255) NOT NULL,
  `bus_number` varchar(255) NOT NULL,
  `bus_seats` int(255) NOT NULL COMMENT '?? seater',
  `from` int(11) NOT NULL COMMENT 'Town from',
  `to` int(11) NOT NULL COMMENT 'Town to',
  `ticket_price` int(12) NOT NULL,
  `seat_occupants` mediumtext NOT NULL,
  `reserved_seats` mediumtext NOT NULL,
  `status` enum('current','departed') NOT NULL DEFAULT 'current',
  `loading` enum('0','1','2') NOT NULL DEFAULT '0' COMMENT '1-Boarding 2-Delayed',
  `departure_date` date NOT NULL,
  `departure_time` time NOT NULL,
  `checked_out_on` date NOT NULL,
  `checked_out_time` time NOT NULL COMMENT 'Time bus was checked out',
  `total_amount` int(10) NOT NULL COMMENT 'Total amount got from this schedule',
  `net_income_disbursed` int(11) NOT NULL COMMENT '0 means not disbursed. Anything else is the amount disbursed',
  `update_count` int(11) NOT NULL DEFAULT '0' COMMENT 'How many times it was re-made ''current'' from ''past''',
  `driven_by` varchar(50) NOT NULL,
  `CreatedBy` varchar(25) NOT NULL,
  `CreatedOn` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP,
  `CheckedOutBy` varchar(255) NOT NULL,
  `modified_by` varchar(25) NOT NULL,
  `deleted` enum('0','1') NOT NULL DEFAULT '0',
  PRIMARY KEY (`id`),
  KEY `agency_id` (`agency_id`),
  KEY `from` (`from`),
  KEY `to` (`to`)
) ENGINE=InnoDB  DEFAULT CHARSET=latin1 AUTO_INCREMENT=111 ;

--
-- Dumping data for table `schedules`
--

INSERT INTO `schedules` (`id`, `agency_id`, `agency_code`, `bus_number`, `bus_seats`, `from`, `to`, `ticket_price`, `seat_occupants`, `reserved_seats`, `status`, `loading`, `departure_date`, `departure_time`, `checked_out_on`, `checked_out_time`, `total_amount`, `net_income_disbursed`, `update_count`, `driven_by`, `CreatedBy`, `CreatedOn`, `CheckedOutBy`, `modified_by`, `deleted`) VALUES
(17, 105, '', 'CE123AA', 30, 3, 4, 3000, '{"2":{"name":"Arnod","phone":"","email":"","idc":"","free_ticket":0},"3":{"name":"Borno","phone":"","email":"","idc":"","free_ticket":0},"5":{"name":"Dalia","phone":"","email":"","idc":"","free_ticket":0},"6":{"name":"Ernest","phone":"","email":"","idc":"","free_ticket":0},"7":{"name":"Falone","phone":"","email":"","idc":"","free_ticket":0},"8":{"name":"Gaelle","phone":"","email":"","idc":"","free_ticket":1},"4":{"name":"Ssal","phone":"","email":"","idc":"","free_ticket":0},"9":{"name":"Ssop","phone":"","email":"","idc":"","free_ticket":1}}', '', 'current', '0', '2015-08-28', '20:00:00', '2015-08-28', '16:06:37', 15000, 0, 1, '', 'ABSA', '2015-08-28 09:05:47', 'ABSA', '', '0'),
(18, 105, '', 'CE123AA', 30, 3, 4, 5000, '[]', '', 'current', '0', '2015-08-28', '20:00:00', '0000-00-00', '00:00:00', 0, 0, 0, '', 'ABSA', '2015-08-28 09:09:18', '', '', '0'),
(21, 105, '', 'CE123AB', 48, 3, 4, 5000, '{"1":{"name":"FAJONG","phone":"693652587","email":"faj@gmail.com","idc":"104846494"}}', '', 'departed', '0', '2015-09-03', '20:00:00', '2015-09-04', '09:12:03', 5000, 0, 0, '', 'ADMIN', '2015-09-03 12:39:54', 'ADMIN', '', '0'),
(22, 105, '', 'CE123AB', 48, 3, 4, 3000, '{"2":{"name":"Abongwa Tantoh","phone":"","email":"","idc":"1085623322","free_ticket":0}}', '', 'departed', '0', '2015-09-04', '20:00:00', '2015-09-17', '09:57:28', 3000, 0, 1, '', 'ADMIN', '2015-09-04 07:53:01', 'ABSA', '', '0'),
(23, 105, '', 'CE123AB', 48, 3, 4, 8000, '{"2":{"name":"Abongwa Tantoh","phone":"","email":"abongwendy@yahoo.com","idc":"","free_ticket":0}}', '', 'departed', '0', '2015-09-04', '00:00:00', '2015-09-17', '10:07:43', 8000, 0, 1, '', 'ABSA', '2015-09-04 11:45:45', 'ABSA', '', '0'),
(36, 105, '', 'NW989MO', 30, 3, 4, 7500, '{"2":{"name":"Abongwa Tantoh","phone":"","email":"","idc":"","free_ticket":0},"3":{"name":"Reeva Steenkamp","phone":"","email":"","idc":"","free_ticket":0}}', '', 'departed', '0', '2015-09-17', '18:00:00', '2015-09-20', '06:18:30', 15000, 0, 0, '', 'ABSA', '2015-09-17 07:57:37', 'ABSA', '', '0'),
(37, 105, '', 'CE123AB', 48, 3, 10, 5000, '{"2":{"name":"Abongwa Tantoh","phone":"","email":"","idc":"1085623322","free_ticket":0},"3":{"name":"Abongwa Tantoh","phone":"","email":"","idc":"","free_ticket":0}}', '', 'departed', '0', '2015-09-17', '20:00:00', '2015-09-17', '10:27:30', 10000, 0, 4, '', 'ABSA', '2015-09-17 09:08:06', 'ADMIN', 'ABSA', '0'),
(38, 105, '', 'CE123AB', 48, 3, 2, 5000, '{"2":{"name":"Abongwa Tantoh","phone":"","email":"absalomshu@gmail.com","idc":"","free_ticket":0}}', '', 'departed', '0', '2015-09-17', '20:00:00', '2015-09-17', '10:31:25', 5000, 5000, 3, '', 'ADMIN', '2015-09-17 09:31:10', 'ADMIN', 'ADMIN', '0'),
(40, 105, '', 'CE123AB', 48, 3, 4, 6000, '{"2":{"name":"Babylon","phone":"","email":"","idc":"","free_ticket":0},"3":{"name":"Reeva Steenkamp","phone":"","email":"","idc":"","free_ticket":0}}', '', 'departed', '0', '2015-09-19', '20:00:00', '2015-09-19', '16:52:42', 12000, 0, 1, '', 'ABSA', '2015-09-19 15:52:27', 'ABSA', 'ABSA', '0'),
(42, 105, '', 'CE123AB', 48, 3, 4, 6000, '{"2":{"name":"Abongwa Tantoh","phone":"","email":"","idc":"","free_ticket":0}}', '', 'departed', '0', '2015-09-20', '17:00:00', '2015-09-20', '12:10:54', 6000, 0, 0, '', 'ABSA', '2015-09-20 05:00:55', 'ABSA', '', '0'),
(43, 105, '', 'CE123AB', 48, 3, 10, 6000, '{"2":{"name":"Abongwa Tantoh","phone":"","email":"","idc":"","free_ticket":0},"3":{"name":"Reeva Steenkamp","phone":"","email":"","idc":"","free_ticket":0}}', '', 'departed', '0', '2015-09-20', '20:00:00', '2015-09-20', '06:05:51', 12000, 0, 3, '', 'ABSA', '2015-09-20 05:05:41', 'ABSA', 'ABSA', '0'),
(44, 105, '', 'CE123AB', 48, 3, 10, 5500, '{"2":{"name":"Abongwa Tantoh","phone":"","email":"","idc":"","free_ticket":0}}', '', 'departed', '0', '2015-09-20', '20:00:00', '2015-09-20', '06:28:34', 5500, 0, 0, '', 'ABSA', '2015-09-20 05:28:05', 'ABSA', '', '0'),
(45, 105, '', 'CE123AB', 48, 3, 4, 8000, '{"2":{"name":"Abongwa Tantoh","phone":"","email":"","idc":"","free_ticket":0},"3":{"name":"Nelson Faro","phone":"","email":"","idc":"","free_ticket":0},"4":{"name":"Fusi Manasseh","phone":"","email":"","idc":"","free_ticket":0}}', '[]', 'departed', '0', '2015-09-22', '20:00:00', '2015-09-22', '10:30:37', 24000, 14000, 6, '', 'ABSA', '2015-09-22 08:49:26', 'ABSA', 'ABSA', '0'),
(52, 105, '', 'CE123AB', 48, 3, 4, 8000, '{"18":{"name":"Abongwa Tantoh","phone":"","email":"","idc":"","free_ticket":0},"2":{"name":"Nelson Faro","phone":"","email":"","idc":"","free_ticket":0},"3":{"name":"Palo Alto","phone":"","email":"","idc":"","free_ticket":0}}', '', 'departed', '0', '2015-09-30', '20:00:00', '2015-09-30', '13:47:04', 24000, 0, 0, '', 'ABSA', '2015-09-30 12:45:18', 'ABSA', '', '0'),
(53, 105, '', 'CE123AB', 48, 3, 4, 8000, '{"2":{"name":"Abongwa Tantoh","phone":"","email":"","idc":"","free_ticket":0}}', '', 'departed', '0', '2015-10-01', '20:00:00', '2015-10-01', '08:51:16', 8000, 0, 4, '', 'ABSA', '2015-10-01 07:10:01', 'ABSA', 'ABSA', '0'),
(56, 105, '', 'CE123AB', 48, 3, 1, 6000, '{"2":{"name":"Reeva Steenkamp","phone":"","email":"","idc":"","free_ticket":0},"3":{"name":"Shu Absalom","phone":"","email":"","idc":"","free_ticket":0},"15":{"name":"Reeva Steenkamp","phone":"","email":"","idc":"","free_ticket":0},"4":{"name":"Nelson Faro","phone":"","email":"","idc":"","free_ticket":0}}', '', 'departed', '0', '2015-10-01', '20:00:00', '2015-10-01', '09:52:18', 24000, 8000, 2, '', 'ABSA', '2015-10-01 08:52:00', 'ABSA', 'ABSA', '0'),
(57, 105, '', 'AB111A', 1, 3, 4, 5000, '', '', 'current', '0', '2015-10-05', '20:00:00', '0000-00-00', '00:00:00', 0, 0, 0, '', 'ABSA', '2015-10-05 08:29:02', '', '', '1'),
(59, 105, '', 'CE123AB', 48, 3, 4, 5000, '{"2":{"name":"Abongwa Tantoh","phone":"","email":"","idc":"","free_ticket":0}}', '', 'departed', '0', '2015-10-07', '20:00:00', '2015-10-07', '09:25:12', 5000, 0, 1, '', 'ABSA', '2015-10-07 08:21:25', 'ABSA', 'ABSA', '0'),
(60, 105, '', 'NW989MO', 30, 3, 10, 6000, '{"2":{"name":"Reeva ","phone":"33333333","email":"ree@va.com","idc":"22222"},"3":{"name":"Nelson Faro","phone":"5555215","email":"","idc":"1085623322"},"5":{"name":"Fusi Manasseh","phone":"999999","email":"","idc":"111111"},"4":{"name":"Palo Alto","phone":"777777777","email":"","idc":"8888888"}}', '', 'current', '0', '2015-10-07', '20:00:00', '0000-00-00', '00:00:00', 0, 0, 0, '', 'ABSA', '2015-10-07 08:34:43', '', '', '1'),
(61, 105, '', 'CE123AB', 48, 3, 12, 3500, '{"2":{"name":"Lowe Alino","phone":"","email":"","idc":""},"3":{"name":"Jules Nana","phone":"222","email":"","idc":"11","free_ticket":0},"4":{"name":"Oj Simpson","phone":"66","email":"tan@to.ho","idc":"55","free_ticket":0},"5":{"name":"Nelson Faro","phone":"696362464","email":"","idc":"108"}}', '', 'departed', '1', '2015-10-07', '20:00:00', '2015-10-08', '10:03:17', 14000, 0, 1, '', 'ABSA', '2015-10-07 09:21:25', 'ABSA', 'ABSA', '0'),
(62, 105, '', 'CE123AB', 48, 3, 4, 8000, '{"2":{"name":"Absalom","phone":"696362464","email":"","idc":"","free_ticket":0}}', '', 'departed', '0', '2015-10-08', '20:00:00', '2015-10-08', '11:02:05', 8000, 0, 0, '', 'ABSA', '2015-10-08 10:01:26', 'ABSA', '', '0'),
(63, 105, '', 'CE123AB', 48, 3, 4, 8000, '{"2":{"name":"Reeva Steenkamp","phone":"","email":"absalomshu@gmail.com","idc":"","free_ticket":0}}', '', 'departed', '0', '2015-10-14', '20:00:00', '2015-10-14', '16:27:37', 8000, 0, 0, '', 'ABSA', '2015-10-14 15:27:26', 'ABSA', '', '0'),
(64, 105, '', 'CE123AB', 48, 3, 4, 8000, '{"2":{"name":"Reeva Steenkamp","phone":"","email":"","idc":"","free_ticket":0},"3":{"name":"Shu Absalom","phone":"","email":"","idc":"","free_ticket":0},"4":{"name":"Shu Absalom","phone":"","email":"","idc":"","free_ticket":0}}', '', 'departed', '0', '2015-10-14', '20:00:00', '2015-10-14', '16:28:17', 24000, 14000, 0, '', 'ABSA', '2015-10-14 15:27:55', 'ABSA', '', '0'),
(65, 105, '', 'CE123AB', 48, 3, 10, 5000, '{"2":{"name":"Nelson Faro","phone":"","email":"","idc":"","free_ticket":0},"3":{"name":"Nelson Faro","phone":"","email":"","idc":"","free_ticket":0},"4":{"name":"Shu Absalom","phone":"","email":"","idc":"","free_ticket":0}}', '', 'departed', '0', '2015-10-14', '20:00:00', '2015-10-14', '16:39:27', 15000, 5000, 0, '', 'ABSA', '2015-10-14 15:38:03', 'ABSA', '', '0'),
(67, 105, '', 'CE123AB', 48, 3, 4, 6000, '{"2":{"name":"Reeva Steenkamp","phone":"","email":"","idc":"","free_ticket":0}}', '', 'departed', '0', '2015-10-20', '20:00:00', '2015-10-20', '11:57:37', 6000, 0, 0, '', 'ABSA', '2015-10-20 10:56:35', 'ABSA', '', '0'),
(68, 105, '', 'CE123AB', 48, 3, 4, 10000, '{"2":{"name":"Shu Absalom","phone":"632547896","email":"absalomshu@gmail.com","idc":"108562321","free_ticket":0},"3":{"name":"Shu Absalom","phone":"696362464","email":"absalomshu@gmail.com","idc":"108310175","free_ticket":0},"4":{"name":"Absalom Shu","phone":"696362464","email":"absalomshu@gmail.com","idc":"108310175","free_ticket":0},"5":{"name":"Niba Tafor","phone":"678956321","email":"niba@tafor.com","idc":"","free_ticket":0},"6":{"name":"Niba Tafor","phone":"678956321","email":"niba@tafor.com","idc":"111111111","free_ticket":0},"7":{"name":"Niba Tafor","phone":"","email":"","idc":"","free_ticket":0},"8":{"name":"Fusi Manasseh","phone":"632547896","email":"","idc":"111111125","free_ticket":0},"9":{"name":"Fusi Manasseh","phone":"666666666","email":"fusi@manasseh.com","idc":"102545636","free_ticket":0},"10":{"name":"Shu Mabel","phone":"674777444","email":"","idc":"","free_ticket":0},"11":{"name":"Reeva Steenkamp","phone":"333333444444","email":"","idc":"1085623322","free_ticket":0},"12":{"name":"Absalom Shu","phone":"696362464","email":"","idc":"108310175","free_ticket":0},"14":{"name":"Absalom Shu","phone":"696362464","email":"absalomshu@gmail.com","idc":"108310175","free_ticket":0},"15":{"name":"Koolio","phone":"696969696","email":"koo@lio.com","idc":"","free_ticket":0},"13":{"name":"Absalom Shu","phone":"","email":"","idc":"","free_ticket":0}}', '', 'departed', '0', '2015-10-23', '20:00:00', '2015-10-23', '14:34:50', 140000, 0, 0, '', 'ABSA', '2015-10-23 10:54:25', 'ABSA', '', '0'),
(70, 105, '', 'CE123AB', 48, 3, 4, 20000, '[]', '', 'departed', '0', '2015-10-23', '20:00:00', '2016-02-26', '16:07:32', 0, 0, 0, '', 'ABSA', '2015-10-23 13:35:03', 'ABSA', '', '0'),
(71, 105, '', 'CE123AB', 48, 3, 4, 5000, '{"2":{"name":"Absalom Shu","phone":"696362464","email":"absalomshu@fmail.ciom","idc":"108523658","free_ticket":0},"3":{"name":"Krazic","phone":"632145878","email":"kras@sic.com","idc":"102333333","free_ticket":0},"5":{"name":"Krazics","phone":"","email":"","idc":"","free_ticket":0},"6":{"name":"krazi","phone":"","email":"","idc":"","free_ticket":0},"7":{"name":"krazic shu","phone":"","email":"","idc":"","free_ticket":0},"8":{"name":"shu","phone":"","email":"","idc":"","free_ticket":0},"9":{"name":"shu mabel","phone":"","email":"","idc":"","free_ticket":0},"10":{"name":"mabel","phone":"","email":"","idc":"","free_ticket":0},"11":{"name":"Krazics","phone":"","email":"","idc":"","free_ticket":0}}', '', 'departed', '1', '2015-10-29', '20:00:00', '2015-10-31', '20:35:12', 45000, 0, 0, '', 'ABSA', '2015-10-29 07:21:09', 'ABSA', '', '0'),
(72, 105, '', 'CE123AB', 48, 3, 11, 8000, '{"2":{"name":"shu mabel","phone":"","email":"","idc":"","free_ticket":0},"3":{"name":"shu","phone":"","email":"","idc":"","free_ticket":0},"4":{"name":"Shu","phone":"","email":"","idc":"","free_ticket":0},"5":{"name":"Shu","phone":"","email":"","idc":"","free_ticket":0},"6":{"name":"Shu","phone":"","email":"","idc":"","free_ticket":0},"7":{"name":"Sssss","phone":"","email":"","idc":"","free_ticket":0},"8":{"name":"Shu Mabel","phone":"689898989","email":"solo@ngongi.com","idc":""}}', '[]', 'departed', '0', '2015-10-29', '20:00:00', '2015-10-29', '11:40:10', 56000, 0, 1, '', 'ABSA', '2015-10-29 10:09:15', 'ABSA', 'ABSA', '0'),
(73, 105, '', 'CE123AB', 48, 3, 4, 5000, '{"2":{"name":"Absalom Shu","phone":"696362464","email":"absalomshu@fmail.ciom","idc":"108523658","free_ticket":0}}', '', 'departed', '0', '2015-11-01', '20:00:00', '2015-10-31', '20:34:22', 5000, 0, 0, '', 'ABSA', '2015-10-31 19:25:31', 'ABSA', '', '0'),
(74, 105, '', 'CE123AB', 48, 3, 11, 5000, '{"2":{"name":"Absalom Shu","phone":"696362464","email":"absalomshu@fmail.ciom","idc":"108523658","free_ticket":0}}', '', 'departed', '0', '2015-11-01', '08:00:00', '2015-10-31', '20:34:33', 5000, 0, 0, '', 'ABSA', '2015-10-31 19:26:04', 'ABSA', '', '0'),
(76, 105, '', 'CE123AB', 48, 3, 4, 8000, '{"2":{"name":"Absalom Shu","phone":"696362464","email":"absalomshu@fmail.ciom","idc":"108523658","free_ticket":0},"3":{"name":"As","phone":"","email":"","idc":"","free_ticket":0},"4":{"name":"Ss","phone":"","email":"","idc":"","free_ticket":0}}', '', 'departed', '1', '2015-11-02', '20:00:00', '2015-11-02', '16:27:28', 24000, 0, 1, '', 'ABSA', '2015-11-02 10:37:38', 'ABSA', 'ABSA', '0'),
(79, 105, '', 'CE123AB', 48, 3, 4, 10000, '{"2":{"name":"Shu","phone":"","email":"","idc":"","free_ticket":0},"3":{"name":"Absal","phone":"0","email":"","idc":"10233232","free_ticket":0},"4":{"name":"Ch","phone":"","email":"","sd_price":"6000","sd_town":"28","idc":"","free_ticket":0}}', '', 'departed', '0', '2016-02-16', '20:00:00', '2016-12-16', '13:50:05', 26000, 0, 0, '', 'ABSA', '2016-02-16 12:03:14', 'ABSA', '', '0'),
(83, 105, '', 'CE123AB', 48, 3, 1, 8000, '{"2":{"name":"Hij","phone":"","email":"","idc":"","free_ticket":0}}', '', 'departed', '0', '2016-02-25', '20:00:00', '2016-09-21', '03:25:01', 8000, 0, 0, '', 'ABSA', '2016-02-25 12:07:58', 'ABSA', '', '0'),
(91, 105, '', 'NW989MO', 30, 3, 10, 5000, '', '', 'current', '0', '2016-05-16', '20:00:00', '0000-00-00', '00:00:00', 0, 0, 0, '', 'ABSA', '2016-05-16 08:12:32', '', '', '1'),
(93, 105, '', 'CE123AB', 48, 3, 4, 5000, '{"2":{"name":"John Doe","phone":"698577784","email":"","idc":"152523456","free_ticket":0},"3":{"name":"Jacky Lee","phone":"","email":"","sd_price":"3000","sd_town":"17","idc":"108566636","free_ticket":0}}', '', 'departed', '0', '2016-05-19', '20:00:00', '2016-05-19', '03:21:45', 8000, 0, 0, '', 'ABSA', '2016-05-19 02:20:41', 'ABSA', '', '0'),
(95, 106, '', 'NW111', 70, 4, 3, 5000, '', '', 'current', '0', '2016-05-19', '20:00:00', '0000-00-00', '00:00:00', 0, 0, 0, '', 'ACHA', '2016-05-19 02:50:22', '', '', '1'),
(96, 105, '', 'CE123AB', 48, 3, 1, 10000, '{"2":{"name":"Ih","phone":"","email":"","idc":"","free_ticket":0}}', '', 'departed', '0', '2016-07-29', '20:00:00', '2016-09-21', '03:23:45', 10000, 0, 0, '', 'ABSA', '2016-07-29 01:21:16', 'ABSA', '', '0'),
(97, 105, '', 'CE123AB', 48, 3, 1, 6000, '{"2":{"name":"Op","phone":"","email":"","idc":"","free_ticket":0}}', '', 'departed', '0', '2016-07-29', '20:00:00', '2016-09-21', '03:21:54', 6000, 0, 0, '', 'ABSA', '2016-07-29 01:57:16', 'ABSA', '', '0'),
(98, 105, '', 'CE123AB', 48, 3, 1, 6000, '{"2":{"name":"Y","phone":"","email":"","idc":"","free_ticket":0}}', '', 'departed', '0', '2016-08-12', '20:00:00', '2016-09-21', '03:16:58', 6000, 0, 0, '', 'ABSA', '2016-08-12 00:44:24', 'ABSA', '', '0'),
(99, 105, '', 'CE123AB', 48, 3, 1, 10000, '{"2":{"name":"Shu","phone":"","email":"","idc":"","free_ticket":0}}', '', 'departed', '0', '2016-08-29', '20:00:00', '2016-08-29', '10:50:59', 10000, 0, 1, '', 'ABSA', '2016-08-29 09:50:40', 'ABSA', 'ABSA', '0'),
(102, 105, '', 'CE123AB', 48, 3, 10, 6000, '{"2":{"name":"Kabila","phone":"","email":"","idc":"","free_ticket":0}}', '', 'departed', '0', '2016-09-22', '20:00:00', '2016-09-22', '02:59:19', 6000, 0, 0, '', 'ABSA', '2016-09-22 01:48:30', 'ABSA', '', '0'),
(103, 105, '', 'UNKNOWN', 70, 3, 10, 2300, '', '', 'current', '0', '2016-09-22', '20:00:00', '0000-00-00', '00:00:00', 0, 0, 0, '', 'ABSA', '2016-09-22 01:53:41', '', '', '1'),
(104, 105, '', 'NW989MO', 30, 3, 2, 500, '{"2":{"name":"Absal","phone":"","email":"","sd_price":"100","sd_town":"10","idc":"10233232","free_ticket":0}}', '', 'current', '0', '2016-09-30', '20:00:00', '0000-00-00', '00:00:00', 0, 0, 0, '', 'ABSA', '2016-09-30 14:25:01', '', '', '1'),
(105, 105, '', 'AB111A', 1, 3, 1, 10000, '', '', 'current', '0', '2016-10-01', '20:00:00', '0000-00-00', '00:00:00', 0, 0, 0, '', 'ABSA', '2016-10-01 03:49:48', '', '', '1'),
(106, 105, '', 'UNKNOWN', 70, 3, 1, 5000, '', '', 'current', '0', '2016-10-01', '20:00:00', '0000-00-00', '00:00:00', 0, 0, 0, '', 'ABSA', '2016-10-01 03:55:22', '', '', '1'),
(107, 105, '', 'AA123AA', 80, 3, 1, 10000, '{"2":{"name":"Absalom Shu","phone":"696362464","email":"absalomshu@fmail.ciom","idc":"108523658","free_ticket":0},"3":{"name":"Absalom Shu","phone":"696362464","email":"absalomshu@fmail.ciom","idc":"108523658","free_ticket":0},"4":{"name":"Asong","phone":"","email":"","idc":"","free_ticket":0}}', '', 'departed', '2', '2016-11-09', '20:00:00', '2016-12-14', '05:53:51', 30000, 0, 1, '', 'ABSA', '2016-11-09 02:09:54', 'ABSA', 'ABSA', '0'),
(108, 105, '', 'CE123AB', 48, 3, 4, 2000, '', '', 'current', '0', '2017-01-07', '20:00:00', '0000-00-00', '00:00:00', 0, 0, 0, '', 'ABSA', '2017-01-07 02:31:14', '', '', '0'),
(109, 106, '', 'NW111', 70, 4, 3, 5000, '', '', 'current', '0', '2017-01-07', '20:00:00', '0000-00-00', '00:00:00', 0, 0, 0, '', 'ACHA', '2017-01-07 03:21:05', '', '', '1'),
(110, 106, '', 'NW111', 70, 4, 3, 2000, '{"2":{"name":"Ss","phone":"","email":"","idc":"","free_ticket":0}}', '', 'departed', '0', '2017-01-07', '20:00:00', '2017-01-07', '05:20:17', 2000, 0, 0, '', 'ACHA', '2017-01-07 03:28:08', 'ACHA', '', '0');

--
-- Triggers `schedules`
--
DROP TRIGGER IF EXISTS `insert_schedule_transaction`;
DELIMITER //
CREATE TRIGGER `insert_schedule_transaction` AFTER INSERT ON `schedules`
 FOR EACH ROW BEGIN
	SET @change_type = 'insert';
	INSERT INTO transactions (	`table`, agency_id, change_id, change_type, bus_number, bus_seats, `from`, `to`, ticket_price, seat_occupants, reserved_seats, status, loading,
								departure_date, departure_time, checked_out_on, checked_out_time,  total_amount, deleted, CreatedBy, CheckedOutBy) 
					VALUES (	'schedules', NEW.agency_id, NEW.id, @change_type, NEW.bus_number, NEW.bus_seats, NEW.from, NEW.to, NEW.ticket_price, NEW.seat_occupants, 
								NEW.reserved_seats, NEW.status, NEW.loading, NEW.departure_date, NEW.departure_time, NEW.checked_out_on, NEW.checked_out_time, NEW.total_amount, NEW.deleted, NEW.CreatedBy, NEW.CheckedOutBy);
	END
//
DELIMITER ;
DROP TRIGGER IF EXISTS `update_schedule_transaction`;
DELIMITER //
CREATE TRIGGER `update_schedule_transaction` AFTER UPDATE ON `schedules`
 FOR EACH ROW BEGIN
	SET @change_type = 'update';
	INSERT INTO transactions (	`table`, agency_id, change_id, change_type, bus_number, bus_seats, `from`, `to`, ticket_price, seat_occupants, reserved_seats, status, loading,
								departure_date, departure_time, checked_out_on, checked_out_time, total_amount, deleted, CreatedBy, CheckedOutBy) 
					VALUES (	'schedules', NEW.agency_id, NEW.id, @change_type, NEW.bus_number, NEW.bus_seats, NEW.from, NEW.to, NEW.ticket_price, NEW.seat_occupants, 
								NEW.reserved_seats, NEW.status, NEW.loading, NEW.departure_date, NEW.departure_time, NEW.checked_out_on, NEW.checked_out_time, NEW.total_amount, NEW.deleted, NEW.CreatedBy, NEW.CheckedOutBy);
	END
//
DELIMITER ;

-- --------------------------------------------------------

--
-- Table structure for table `settings`
--

CREATE TABLE IF NOT EXISTS `settings` (
  `id` tinyint(4) NOT NULL AUTO_INCREMENT,
  `agency_id` int(11) NOT NULL,
  `auto_ticket` enum('0','1') COLLATE latin1_general_ci NOT NULL DEFAULT '0',
  `ticket_type_1` enum('0','1') COLLATE latin1_general_ci NOT NULL DEFAULT '0',
  `ticket_type_2` enum('0','1') COLLATE latin1_general_ci NOT NULL,
  `auto_disburse_cash_to_agency_id` int(11) NOT NULL COMMENT 'The branch to which the "auto-disburse cash" for checked out schedules goes to.',
  PRIMARY KEY (`id`)
) ENGINE=MyISAM  DEFAULT CHARSET=latin1 COLLATE=latin1_general_ci AUTO_INCREMENT=5 ;

--
-- Dumping data for table `settings`
--

INSERT INTO `settings` (`id`, `agency_id`, `auto_ticket`, `ticket_type_1`, `ticket_type_2`, `auto_disburse_cash_to_agency_id`) VALUES
(1, 105, '1', '1', '0', 0),
(2, 104, '0', '0', '0', 0),
(3, 0, '0', '0', '0', 0),
(4, 0, '0', '0', '0', 0);

-- --------------------------------------------------------

--
-- Table structure for table `sms`
--

CREATE TABLE IF NOT EXISTS `sms` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `agency_id` int(11) NOT NULL,
  `date` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP,
  `phone` varchar(15) COLLATE latin1_general_ci NOT NULL,
  `message` varchar(255) COLLATE latin1_general_ci NOT NULL,
  `online` enum('0','1') COLLATE latin1_general_ci NOT NULL,
  PRIMARY KEY (`id`),
  KEY `agency_id` (`agency_id`)
) ENGINE=InnoDB  DEFAULT CHARSET=latin1 COLLATE=latin1_general_ci AUTO_INCREMENT=31 ;

--
-- Dumping data for table `sms`
--

INSERT INTO `sms` (`id`, `agency_id`, `date`, `phone`, `message`, `online`) VALUES
(1, 105, '2015-09-12 12:31:28', '237696362464', 'Hello Nelson. A parcel has been deposited for you at Viasso - Yaounde. You will be informed when it''s available in your town.', '1'),
(2, 104, '2015-09-12 13:06:15', '237696362464', 'Hello Nelson. Your parcel is now available at Viasso - Buea. Please, pass by to collect it.', '1'),
(3, 105, '2015-09-15 09:02:26', '237696362464', 'Hello Mapoli Ngono. A parcel has been deposited for you at Viasso - Yaounde. You will be informed when it''s available in your town.', '1'),
(4, 105, '2015-10-08 09:32:38', '237222', 'Hi Jules Nana, your bus will take off in a few moments. Thanks for trusting Viasso.', '1'),
(5, 105, '2015-10-08 09:32:38', '23766', 'Hi Oj Simpson, your bus will take off in a few moments. Thanks for trusting Viasso.', '1'),
(6, 105, '2015-10-08 09:32:39', '237696362464', 'Hi Nelson Faro, your bus will take off in a few moments. Thanks for trusting Viasso.', '1'),
(7, 105, '2015-10-08 09:32:39', '237222', 'Hi Jules Nana, your bus will take off in a few moments. Thanks for trusting Viasso.', '1'),
(8, 105, '2015-10-08 09:32:39', '23766', 'Hi Oj Simpson, your bus will take off in a few moments. Thanks for trusting Viasso.', '1'),
(9, 105, '2015-10-08 09:32:39', '237696362464', 'Hi Nelson Faro, your bus will take off in a few moments. Thanks for trusting Viasso.', '1'),
(10, 105, '2015-10-08 10:02:40', '237696362464', 'Musango VIP Bus Service wishes to thank you for travelling with us. Have a wonderful day and hope to see you again. ', '1'),
(11, 105, '2015-10-29 10:36:59', '237689898989', 'Hello Shu Mabel. Your reservation with VIASSO is confirmed. Bus: CE123AB Time: 8:00 PM', '1'),
(12, 105, '2015-11-02 10:37:50', '237696362464', 'Hi Absalom Shu, your bus will take off in a few moments. Thanks for trusting Viasso.', '1'),
(13, 105, '2015-11-02 10:46:08', '237696362464', 'Hi Absalom Shu, your bus will take off in a few moments. Thanks for trusting Viasso.', '1'),
(14, 105, '2015-11-02 10:50:26', '237696362464', 'Hi Absalom Shu, your bus will take off in a few moments. Thanks for trusting Viasso.', '1'),
(15, 105, '2015-11-02 10:57:42', '237696362464', 'Hi Absalom Shu, your bus will take off in a few moments. Thanks for trusting Viasso.', '1'),
(16, 105, '2015-11-02 10:59:33', '237696362464', 'Hi Absalom Shu, your bus will take off in a few moments. Thanks for trusting Viasso.', '1'),
(17, 105, '2015-11-03 08:43:26', '237696362464', 'Hi Absalom Shu, your bus will take off in a few moments. Thanks for trusting Viasso.', '1'),
(18, 105, '2015-11-18 15:22:21', '237696362464', 'Hi Absalom Shu, your bus will take off in a few moments. Thanks for trusting Viasso.', '1'),
(19, 105, '2016-02-01 08:38:34', '237696362464', 'Hi Absalom Shu, your bus will take off in a few moments. Thanks for trusting Viasso.', '1'),
(20, 105, '2016-02-05 08:13:10', '237696362464', 'Hi Absalom Shu, your bus will take off in a few moments. Thanks for trusting Viasso.', '1'),
(21, 105, '2016-02-05 08:13:36', '237696362464', 'Hi Absalom Shu, your bus will take off in a few moments. Thanks for trusting Viasso.', '1'),
(22, 105, '2016-02-05 08:51:12', '237696362464', 'Hi Absalom Shu, your bus will take off in a few moments. Thanks for trusting Viasso.', '1'),
(23, 105, '2016-02-18 14:37:26', '237696362464', 'Hi Absalom Shu, your bus will take off in a few moments. Thanks for trusting Viasso.', '1'),
(24, 105, '2016-09-20 10:40:06', '237687878787', 'Hello Nelson. A parcel has been deposited for you at Viasso - Yaounde. You will be informed when it''s available in your town.', '1'),
(25, 105, '2016-09-21 06:04:18', '237689898989', 'Hello Kilo. A parcel has been deposited for you at Viasso - Yaounde. You will be informed when it''s available in your town.', '1'),
(26, 105, '2016-09-22 12:03:16', '237687878787', 'Hello Nelson. A parcel has been deposited for you at Viasso - Yaounde. You will be informed when it''s available in your town.', '1'),
(27, 106, '2016-10-08 00:46:11', '237696362464', 'Hello Absalom Ngwabinkaa. A parcel has been deposited for you at Viasso - Douala. You will be informed when it''s available in your town.', '1'),
(28, 106, '2016-10-08 00:49:11', '237696362464', 'Hello Absalom Ngwabinkaa. A parcel has been deposited for you at Viasso - Douala. You will be informed when it''s available in your town.', '1'),
(29, 105, '2016-10-10 01:11:40', '237696362464', 'Hello Absalom Ngwabinkaa. Your parcel is now available at Viasso - Yaounde. Please, pass by to collect it.', '1'),
(30, 105, '2016-10-10 01:34:48', '237696362464', 'Hello Absalom Ngwabinkaa. Your parcel is now available at Viasso - Yaounde. Please, pass by to collect it.', '1');

-- --------------------------------------------------------

--
-- Table structure for table `ticket_details`
--

CREATE TABLE IF NOT EXISTS `ticket_details` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `AgencyID` int(11) NOT NULL,
  `ScheduleID` int(11) NOT NULL,
  `ClientName` varchar(255) NOT NULL,
  `ClientPhone` int(11) NOT NULL,
  `ClientEmail` varchar(255) NOT NULL,
  `ClientIDC` int(11) NOT NULL,
  `SeatNumber` int(11) NOT NULL,
  `Price` int(11) NOT NULL,
  `IsPending` enum('0','1') NOT NULL,
  `MissedStatus` enum('0','1') NOT NULL DEFAULT '0',
  `FreeTicket` enum('0','1') NOT NULL DEFAULT '0',
  `FreeTicketReason` varchar(255) NOT NULL,
  `OtherInformation` varchar(255) NOT NULL,
  `IsRemote` enum('0','1') NOT NULL,
  `RemoteTicketOutgoing` enum('0','1') NOT NULL COMMENT 'Ticket was registered here, to be used elsewhere',
  `RemoteTicketIncoming` enum('0','1') NOT NULL COMMENT 'Ticket was registered elsewhere to be used here',
  `RemoteTicketAgencyFrom` int(11) NOT NULL,
  `RemoteTicketAgencyTo` int(11) NOT NULL,
  `RemoteTicketStatus` enum('saved','active','closed') NOT NULL,
  `Online` enum('0','1') NOT NULL,
  `CreatedOn` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP,
  `CreatedBy` varchar(255) NOT NULL,
  `ModifiedOn` timestamp NOT NULL DEFAULT '0000-00-00 00:00:00',
  `ModifiedBy` varchar(255) NOT NULL,
  PRIMARY KEY (`id`),
  KEY `AgencyID` (`AgencyID`)
) ENGINE=InnoDB  DEFAULT CHARSET=latin1 AUTO_INCREMENT=186 ;

--
-- Dumping data for table `ticket_details`
--

INSERT INTO `ticket_details` (`id`, `AgencyID`, `ScheduleID`, `ClientName`, `ClientPhone`, `ClientEmail`, `ClientIDC`, `SeatNumber`, `Price`, `IsPending`, `MissedStatus`, `FreeTicket`, `FreeTicketReason`, `OtherInformation`, `IsRemote`, `RemoteTicketOutgoing`, `RemoteTicketIncoming`, `RemoteTicketAgencyFrom`, `RemoteTicketAgencyTo`, `RemoteTicketStatus`, `Online`, `CreatedOn`, `CreatedBy`, `ModifiedOn`, `ModifiedBy`) VALUES
(18, 105, 17, 'Arnod', 0, '', 0, 2, 3000, '0', '0', '0', '', '', '0', '0', '0', 0, 0, 'saved', '0', '2015-08-28 09:06:45', 'ABSA', '0000-00-00 00:00:00', ''),
(19, 105, 17, 'Borno', 0, '', 0, 3, 3000, '0', '0', '0', '', '', '0', '0', '0', 0, 0, 'saved', '0', '2015-08-28 09:07:10', 'ABSA', '0000-00-00 00:00:00', ''),
(21, 105, 17, 'Dalia', 0, '', 0, 5, 3000, '0', '0', '0', '', '', '0', '0', '0', 0, 0, 'saved', '0', '2015-08-28 09:07:30', 'ABSA', '0000-00-00 00:00:00', ''),
(22, 105, 17, 'Ernest', 0, '', 0, 6, 3000, '0', '0', '0', '', '', '0', '0', '0', 0, 0, 'saved', '0', '2015-08-28 09:07:39', 'ABSA', '0000-00-00 00:00:00', ''),
(23, 105, 17, 'Falone', 0, '', 0, 7, 3000, '0', '0', '0', '', '', '0', '0', '0', 0, 0, 'saved', '0', '2015-08-28 09:07:47', 'ABSA', '0000-00-00 00:00:00', ''),
(24, 105, 17, 'Gaelle', 0, '', 0, 8, 0, '0', '0', '1', '10 tickets', '', '0', '0', '0', 0, 0, 'saved', '0', '2015-08-28 09:08:13', 'ABSA', '0000-00-00 00:00:00', ''),
(26, 105, 0, 'Rasta', 698654212, '', 0, 0, 5000, '0', '0', '0', '', '', '1', '1', '0', 104, 105, 'saved', '1', '2015-08-28 09:11:46', 'ABSA', '0000-00-00 00:00:00', ''),
(27, 105, 19, 'Absalom Shu', 696362464, 'absalomshu@fmail.ciom', 108523658, 5, 8000, '1', '1', '0', '', '', '0', '0', '0', 0, 0, 'saved', '0', '2015-08-31 14:20:05', 'ABSA', '0000-00-00 00:00:00', ''),
(28, 105, 20, '6500', 698547851, 'absalomshu@gmail.com', 1085623322, 2, 3000, '1', '1', '0', '', '', '0', '0', '0', 0, 0, 'saved', '0', '2015-09-02 08:30:08', 'ABSA', '0000-00-00 00:00:00', ''),
(29, 105, 21, 'FAJONG', 693652587, 'faj@gmail.com', 104846494, 1, 5000, '0', '0', '0', '', '', '0', '0', '0', 0, 0, 'saved', '0', '2015-09-04 08:11:56', 'ADMIN', '0000-00-00 00:00:00', ''),
(30, 105, 22, 'Abongwa Tantoh', 0, '', 1085623322, 2, 3000, '0', '0', '0', '', '', '0', '0', '0', 0, 0, 'saved', '0', '2015-09-04 11:45:11', 'ABSA', '0000-00-00 00:00:00', ''),
(31, 105, 23, 'Abongwa Tantoh', 0, 'abongwendy@yahoo.com', 0, 2, 8000, '0', '0', '0', '', '', '0', '0', '0', 0, 0, 'saved', '0', '2015-09-04 11:46:24', 'ABSA', '0000-00-00 00:00:00', ''),
(33, 104, 0, 'Rasta', 0, '', 0, 0, 5000, '1', '0', '0', 'Paid for at Viasso - Yaounde', '', '1', '0', '1', 104, 105, 'saved', '0', '2015-08-28 09:11:46', 'ABSA', '0000-00-00 00:00:00', ''),
(34, 104, 0, 'Kilo Penasol', 0, '', 0, 0, 6000, '1', '0', '0', 'Paid for at Viasso - Yaounde', '', '1', '0', '1', 104, 105, 'saved', '0', '2015-06-17 09:50:51', 'ABSA', '0000-00-00 00:00:00', ''),
(35, 105, 37, 'Abongwa Tantoh', 0, '', 1085623322, 2, 5000, '0', '0', '0', '', '', '0', '0', '0', 0, 0, 'saved', '0', '2015-09-17 09:08:09', 'ABSA', '0000-00-00 00:00:00', ''),
(36, 105, 37, 'Abongwa Tantoh', 0, '', 0, 3, 5000, '0', '0', '0', '', '', '0', '0', '0', 0, 0, 'saved', '0', '2015-09-17 09:25:16', 'ABSA', '0000-00-00 00:00:00', ''),
(37, 105, 38, 'Abongwa Tantoh', 0, 'absalomshu@gmail.com', 0, 2, 5000, '0', '0', '0', '', '', '0', '0', '0', 0, 0, 'saved', '0', '2015-09-17 09:31:14', 'ADMIN', '0000-00-00 00:00:00', ''),
(38, 105, 36, 'Abongwa Tantoh', 0, '', 0, 2, 7500, '0', '0', '0', '', '', '0', '0', '0', 0, 0, 'saved', '0', '2015-09-17 16:01:28', 'ABSA', '0000-00-00 00:00:00', ''),
(39, 105, 36, 'Reeva Steenkamp', 0, '', 0, 3, 7500, '0', '0', '0', '', '', '0', '0', '0', 0, 0, 'saved', '0', '2015-09-17 16:01:43', 'ABSA', '0000-00-00 00:00:00', ''),
(40, 105, 40, 'Babylon', 0, '', 0, 2, 6000, '0', '0', '0', '', '', '0', '0', '0', 0, 0, 'saved', '0', '2015-09-19 15:52:33', 'ABSA', '0000-00-00 00:00:00', ''),
(41, 105, 40, 'Reeva Steenkamp', 0, '', 0, 3, 6000, '0', '0', '0', '', '', '0', '0', '0', 0, 0, 'saved', '0', '2015-09-19 16:00:01', 'ABSA', '0000-00-00 00:00:00', ''),
(42, 105, 43, 'Abongwa Tantoh', 0, '', 0, 2, 6000, '0', '0', '0', '', '', '0', '0', '0', 0, 0, 'saved', '0', '2015-09-20 05:05:46', 'ABSA', '0000-00-00 00:00:00', ''),
(43, 105, 44, 'Abongwa Tantoh', 0, '', 0, 2, 5500, '0', '0', '0', '', '', '0', '0', '0', 0, 0, 'saved', '0', '2015-09-20 05:28:13', 'ABSA', '0000-00-00 00:00:00', ''),
(44, 105, 43, 'Reeva Steenkamp', 0, '', 0, 3, 6000, '0', '0', '0', '', '', '0', '0', '0', 0, 0, 'saved', '0', '2015-09-20 05:56:38', 'ABSA', '0000-00-00 00:00:00', ''),
(45, 105, 42, 'Abongwa Tantoh', 0, '', 0, 2, 6000, '0', '0', '0', '', '', '0', '0', '0', 0, 0, 'saved', '0', '2015-09-20 11:10:33', 'ABSA', '0000-00-00 00:00:00', ''),
(46, 105, 45, 'Abongwa Tantoh', 0, '', 0, 2, 8000, '0', '0', '0', '', '', '0', '0', '0', 0, 0, 'saved', '0', '2015-09-22 08:49:37', 'ABSA', '0000-00-00 00:00:00', ''),
(47, 105, 45, 'Nelson Faro', 0, '', 0, 3, 8000, '0', '0', '0', '', '', '0', '0', '0', 0, 0, 'saved', '0', '2015-09-22 09:37:43', 'ABSA', '0000-00-00 00:00:00', ''),
(48, 105, 45, 'Fusi Manasseh', 0, '', 0, 4, 8000, '0', '0', '0', '', '', '0', '0', '0', 0, 0, 'saved', '0', '2015-09-22 09:37:48', 'ABSA', '0000-00-00 00:00:00', ''),
(49, 105, 52, 'Abongwa Tantoh', 0, '', 0, 18, 8000, '0', '0', '0', '', '', '0', '0', '0', 0, 0, 'saved', '0', '2015-09-30 12:45:27', 'ABSA', '0000-00-00 00:00:00', ''),
(50, 105, 52, 'Nelson Faro', 0, '', 0, 2, 8000, '0', '0', '0', '', '', '0', '0', '0', 0, 0, 'saved', '0', '2015-09-30 12:46:28', 'ABSA', '0000-00-00 00:00:00', ''),
(51, 105, 52, 'Palo Alto', 0, '', 0, 3, 8000, '0', '0', '0', '', '', '0', '0', '0', 0, 0, 'saved', '0', '2015-09-30 12:46:50', 'ABSA', '0000-00-00 00:00:00', ''),
(54, 105, 55, 'Abongwa Tantoh', 0, '', 0, 2, 6000, '1', '1', '0', '', '', '0', '0', '0', 0, 0, 'saved', '0', '2015-10-01 07:42:44', 'ABSA', '0000-00-00 00:00:00', ''),
(55, 105, 55, 'Reeva Steenkamp', 0, '', 0, 3, 6000, '1', '1', '0', '', '', '0', '0', '0', 0, 0, 'saved', '0', '2015-10-01 07:43:55', 'ABSA', '0000-00-00 00:00:00', ''),
(56, 105, 53, 'Abongwa Tantoh', 0, '', 0, 2, 8000, '0', '0', '0', '', '', '0', '0', '0', 0, 0, 'saved', '0', '2015-10-01 07:51:00', 'ABSA', '0000-00-00 00:00:00', ''),
(57, 105, 56, 'Reeva Steenkamp', 0, '', 0, 2, 6000, '0', '0', '0', '', '', '0', '0', '0', 0, 0, 'saved', '0', '2015-10-01 08:52:04', 'ABSA', '0000-00-00 00:00:00', ''),
(58, 105, 56, 'Shu Absalom', 0, '', 0, 3, 6000, '0', '0', '0', '', '', '0', '0', '0', 0, 0, 'saved', '0', '2015-10-01 08:52:36', 'ABSA', '0000-00-00 00:00:00', ''),
(59, 105, 56, 'Reeva Steenkamp', 0, '', 0, 15, 6000, '0', '0', '0', '', '', '0', '0', '0', 0, 0, 'saved', '0', '2015-10-01 08:52:45', 'ABSA', '0000-00-00 00:00:00', ''),
(60, 105, 56, 'Nelson Faro', 0, '', 0, 4, 6000, '0', '0', '0', '', '', '0', '0', '0', 0, 0, 'saved', '0', '2015-10-01 08:53:38', 'ABSA', '0000-00-00 00:00:00', ''),
(61, 105, 58, 'Reeva Steenkamp', 965221452, 'tan@to.h', 105898524, 2, 6000, '0', '0', '0', '', '', '0', '0', '0', 0, 0, 'saved', '0', '2015-10-05 08:29:32', 'ABSA', '0000-00-00 00:00:00', ''),
(63, 105, 58, 'Aberengwaaa', 698545214, '10@gmail.com4', 105897845, 3, 6000, '0', '0', '0', '', '', '0', '0', '0', 0, 0, 'saved', '0', '2015-10-06 10:29:13', 'ABSA', '0000-00-00 00:00:00', ''),
(64, 105, 59, 'Abongwa Tantoh', 0, '', 0, 2, 5000, '0', '0', '0', '', '', '0', '0', '0', 0, 0, 'saved', '0', '2015-10-07 08:24:57', 'ABSA', '0000-00-00 00:00:00', ''),
(65, 105, 60, 'Reeva ', 33333333, 'ree@va.com', 22222, 2, 6000, '1', '1', '0', '', '', '0', '0', '0', 0, 0, 'saved', '0', '2015-10-07 08:34:48', 'ABSA', '0000-00-00 00:00:00', ''),
(66, 105, 60, 'Nelson Faro', 5555215, '', 1085623322, 3, 6000, '1', '1', '0', '', '', '0', '0', '0', 0, 0, 'saved', '0', '2015-10-07 09:04:49', 'ABSA', '0000-00-00 00:00:00', ''),
(70, 105, 61, 'Lowe Alino', 0, '', 0, 2, 3500, '0', '0', '0', '', '', '0', '0', '0', 0, 0, 'saved', '0', '2015-10-07 09:21:43', 'ABSA', '0000-00-00 00:00:00', ''),
(71, 105, 61, 'Jules Nana', 222, '', 11, 3, 3500, '0', '0', '0', '', '', '0', '0', '0', 0, 0, 'saved', '0', '2015-10-07 09:22:18', 'ABSA', '0000-00-00 00:00:00', ''),
(72, 105, 61, 'Oj Simpson', 66, 'tan@to.ho', 55, 4, 3500, '0', '0', '0', '', '', '0', '0', '0', 0, 0, 'saved', '0', '2015-10-07 09:22:51', 'ABSA', '0000-00-00 00:00:00', ''),
(73, 105, 61, 'Nelson Faro', 696362464, '', 108, 5, 3500, '0', '0', '0', '', '', '0', '0', '0', 0, 0, 'saved', '0', '2015-10-07 09:33:36', 'ABSA', '0000-00-00 00:00:00', ''),
(74, 105, 62, 'Absalom', 696362464, '', 0, 2, 8000, '0', '0', '0', '', '', '0', '0', '0', 0, 0, 'saved', '0', '2015-10-08 10:01:39', 'ABSA', '0000-00-00 00:00:00', ''),
(75, 105, 63, 'Reeva Steenkamp', 0, 'absalomshu@gmail.com', 0, 2, 8000, '0', '0', '0', '', '', '0', '0', '0', 0, 0, 'saved', '0', '2015-10-14 15:27:30', 'ABSA', '0000-00-00 00:00:00', ''),
(76, 105, 64, 'Reeva Steenkamp', 0, '', 0, 2, 8000, '0', '0', '0', '', '', '0', '0', '0', 0, 0, 'saved', '0', '2015-10-14 15:27:59', 'ABSA', '0000-00-00 00:00:00', ''),
(77, 105, 64, 'Shu Absalom', 0, '', 0, 3, 8000, '0', '0', '0', '', '', '0', '0', '0', 0, 0, 'saved', '0', '2015-10-14 15:28:04', 'ABSA', '0000-00-00 00:00:00', ''),
(78, 105, 64, 'Shu Absalom', 0, '', 0, 4, 8000, '0', '0', '0', '', '', '0', '0', '0', 0, 0, 'saved', '0', '2015-10-14 15:28:10', 'ABSA', '0000-00-00 00:00:00', ''),
(79, 105, 65, 'Nelson Faro', 0, '', 0, 2, 5000, '0', '0', '0', '', '', '0', '0', '0', 0, 0, 'saved', '0', '2015-10-14 15:38:07', 'ABSA', '0000-00-00 00:00:00', ''),
(80, 105, 65, 'Nelson Faro', 0, '', 0, 3, 5000, '0', '0', '0', '', '', '0', '0', '0', 0, 0, 'saved', '0', '2015-10-14 15:38:13', 'ABSA', '0000-00-00 00:00:00', ''),
(81, 105, 65, 'Shu Absalom', 0, '', 0, 4, 5000, '0', '0', '0', '', '', '0', '0', '0', 0, 0, 'saved', '0', '2015-10-14 15:39:22', 'ABSA', '0000-00-00 00:00:00', ''),
(82, 105, 66, 'Abongwa Tantoh', 696362454, 'abongtantoh@gmail.com', 567, 2, 6000, '0', '0', '0', '', '', '0', '0', '0', 0, 0, 'saved', '0', '2015-10-19 15:23:22', 'ABSA', '0000-00-00 00:00:00', ''),
(83, 105, 67, 'Reeva Steenkamp', 0, '', 0, 2, 6000, '0', '0', '0', '', '', '0', '0', '0', 0, 0, 'saved', '0', '2015-10-20 10:57:08', 'ABSA', '0000-00-00 00:00:00', ''),
(84, 105, 68, 'Shu Absalom', 632547896, 'absalomshu@gmail.com', 108562321, 2, 10000, '0', '0', '0', '', '', '0', '0', '0', 0, 0, 'saved', '0', '2015-10-23 10:54:47', 'ABSA', '0000-00-00 00:00:00', ''),
(85, 105, 68, 'Shu Absalom', 696362464, 'absalomshu@gmail.com', 108310175, 3, 10000, '0', '0', '0', '', '', '0', '0', '0', 0, 0, 'saved', '0', '2015-10-23 10:56:49', 'ABSA', '0000-00-00 00:00:00', ''),
(86, 105, 68, 'Absalom Shu', 696362464, 'absalomshu@gmail.com', 108310175, 4, 10000, '0', '0', '0', '', '', '0', '0', '0', 0, 0, 'saved', '0', '2015-10-23 11:35:29', 'ABSA', '0000-00-00 00:00:00', ''),
(87, 105, 68, 'Niba Tafor', 678956321, 'niba@tafor.com', 0, 5, 10000, '0', '0', '0', '', '', '0', '0', '0', 0, 0, 'saved', '0', '2015-10-23 11:43:34', 'ABSA', '0000-00-00 00:00:00', ''),
(88, 105, 68, 'Niba Tafor', 678956321, 'niba@tafor.com', 111111111, 6, 10000, '0', '0', '0', '', '', '0', '0', '0', 0, 0, 'saved', '0', '2015-10-23 11:44:17', 'ABSA', '0000-00-00 00:00:00', ''),
(89, 105, 68, 'Niba Tafor', 0, '', 0, 7, 10000, '0', '0', '0', '', '', '0', '0', '0', 0, 0, 'saved', '0', '2015-10-23 11:44:34', 'ABSA', '0000-00-00 00:00:00', ''),
(90, 105, 68, 'Fusi Manasseh', 632547896, '', 111111125, 8, 10000, '0', '0', '0', '', '', '0', '0', '0', 0, 0, 'saved', '0', '2015-10-23 11:51:10', 'ABSA', '0000-00-00 00:00:00', ''),
(91, 105, 68, 'Fusi Manasseh', 666666666, 'fusi@manasseh.com', 102545636, 9, 10000, '0', '0', '0', '', '', '0', '0', '0', 0, 0, 'saved', '0', '2015-10-23 11:53:21', 'ABSA', '0000-00-00 00:00:00', ''),
(92, 105, 68, 'Shu Mabel', 674777444, '', 0, 10, 10000, '0', '0', '0', '', '', '0', '0', '0', 0, 0, 'saved', '0', '2015-10-23 11:55:36', 'ABSA', '0000-00-00 00:00:00', ''),
(93, 105, 68, 'Reeva Steenkamp', 2147483647, '', 1085623322, 11, 10000, '0', '0', '0', '', '', '0', '0', '0', 0, 0, 'saved', '0', '2015-10-23 11:59:20', 'ABSA', '0000-00-00 00:00:00', ''),
(94, 105, 68, 'Absalom Shu', 696362464, '', 108310175, 12, 10000, '0', '0', '0', '', '', '0', '0', '0', 0, 0, 'saved', '0', '2015-10-23 12:01:11', 'ABSA', '0000-00-00 00:00:00', ''),
(96, 105, 68, 'Absalom Shu', 696362464, 'absalomshu@gmail.com', 108310175, 14, 10000, '0', '0', '0', '', '', '0', '0', '0', 0, 0, 'saved', '0', '2015-10-23 12:05:19', 'ABSA', '0000-00-00 00:00:00', ''),
(97, 105, 68, 'Koolio', 696969696, 'koo@lio.com', 0, 15, 10000, '0', '0', '0', '', '', '0', '0', '0', 0, 0, 'saved', '0', '2015-10-23 12:13:53', 'ABSA', '0000-00-00 00:00:00', ''),
(98, 105, 68, 'Absalom Shu', 0, '', 0, 13, 10000, '0', '0', '0', '', '', '0', '0', '0', 0, 0, 'saved', '0', '2015-10-23 13:33:52', 'ABSA', '0000-00-00 00:00:00', ''),
(100, 105, 69, 'Ruler', 632145698, 'ruler@crouch.com', 105236548, 2, 6000, '0', '0', '0', '', '', '0', '0', '0', 0, 0, 'saved', '0', '2015-10-23 13:50:02', 'ABSA', '0000-00-00 00:00:00', ''),
(101, 105, 69, 'Absalom', 696362646, 'absalomshu@gmail.com', 108532654, 3, 6000, '0', '0', '0', '', '', '0', '0', '0', 0, 0, 'saved', '0', '2015-10-28 14:30:13', 'ABSA', '0000-00-00 00:00:00', ''),
(102, 105, 71, 'Absalom Shu', 696362464, 'absalomshu@fmail.ciom', 108523658, 2, 5000, '0', '0', '0', '', '', '0', '0', '0', 0, 0, 'saved', '0', '2015-10-29 07:21:20', 'ABSA', '0000-00-00 00:00:00', ''),
(103, 105, 71, 'Krazic', 632145878, 'kras@sic.com', 102333333, 3, 5000, '0', '0', '0', '', '', '0', '0', '0', 0, 0, 'saved', '0', '2015-10-29 09:25:36', 'ABSA', '0000-00-00 00:00:00', ''),
(105, 105, 71, 'Krazics', 0, '', 0, 5, 5000, '0', '0', '0', '', '', '0', '0', '0', 0, 0, 'saved', '0', '2015-10-29 10:04:08', 'ABSA', '0000-00-00 00:00:00', ''),
(106, 105, 71, 'krazi', 0, '', 0, 6, 5000, '0', '0', '0', '', '', '0', '0', '0', 0, 0, 'saved', '0', '2015-10-29 10:04:38', 'ABSA', '0000-00-00 00:00:00', ''),
(107, 105, 71, 'krazic shu', 0, '', 0, 7, 5000, '0', '0', '0', '', '', '0', '0', '0', 0, 0, 'saved', '0', '2015-10-29 10:04:59', 'ABSA', '0000-00-00 00:00:00', ''),
(108, 105, 71, 'shu', 0, '', 0, 8, 5000, '0', '0', '0', '', '', '0', '0', '0', 0, 0, 'saved', '0', '2015-10-29 10:05:17', 'ABSA', '0000-00-00 00:00:00', ''),
(109, 105, 71, 'shu mabel', 0, '', 0, 9, 5000, '0', '0', '0', '', '', '0', '0', '0', 0, 0, 'saved', '0', '2015-10-29 10:05:31', 'ABSA', '0000-00-00 00:00:00', ''),
(110, 105, 71, 'mabel', 0, '', 0, 10, 5000, '0', '0', '0', '', '', '0', '0', '0', 0, 0, 'saved', '0', '2015-10-29 10:05:53', 'ABSA', '0000-00-00 00:00:00', ''),
(111, 105, 71, 'Krazics', 0, '', 0, 11, 5000, '0', '0', '0', '', '', '0', '0', '0', 0, 0, 'saved', '0', '2015-10-29 10:06:43', 'ABSA', '0000-00-00 00:00:00', ''),
(112, 105, 72, 'shu mabel', 0, '', 0, 2, 8000, '0', '0', '0', '', '', '0', '0', '0', 0, 0, 'saved', '0', '2015-10-29 10:09:34', 'ABSA', '0000-00-00 00:00:00', ''),
(113, 105, 72, 'shu', 0, '', 0, 3, 8000, '0', '0', '0', '', '', '0', '0', '0', 0, 0, 'saved', '0', '2015-10-29 10:09:41', 'ABSA', '0000-00-00 00:00:00', ''),
(114, 105, 72, 'Shu', 0, '', 0, 4, 8000, '0', '0', '0', '', '', '0', '0', '0', 0, 0, 'saved', '0', '2015-10-29 10:17:22', 'ABSA', '0000-00-00 00:00:00', ''),
(115, 105, 72, 'Shu', 0, '', 0, 5, 8000, '0', '0', '0', '', '', '0', '0', '0', 0, 0, 'saved', '0', '2015-10-29 10:18:50', 'ABSA', '0000-00-00 00:00:00', ''),
(116, 105, 72, 'Shu', 0, '', 0, 6, 8000, '0', '0', '0', '', '', '0', '0', '0', 0, 0, 'saved', '0', '2015-10-29 10:19:07', 'ABSA', '0000-00-00 00:00:00', ''),
(117, 105, 72, 'Sssss', 0, '', 0, 7, 8000, '0', '0', '0', '', '', '0', '0', '0', 0, 0, 'saved', '0', '2015-10-29 10:28:01', 'ABSA', '0000-00-00 00:00:00', ''),
(118, 105, 72, 'Shu Mabel', 689898989, 'solo@ngongi.com', 0, 8, 8000, '0', '0', '0', '', '', '0', '0', '0', 0, 0, 'saved', '0', '2015-10-29 10:36:58', 'ABSA', '0000-00-00 00:00:00', ''),
(119, 105, 73, 'Absalom Shu', 696362464, 'absalomshu@fmail.ciom', 108523658, 2, 5000, '0', '0', '0', '', '', '0', '0', '0', 0, 0, 'saved', '0', '2015-10-31 19:34:14', 'ABSA', '0000-00-00 00:00:00', ''),
(120, 105, 74, 'Absalom Shu', 696362464, 'absalomshu@fmail.ciom', 108523658, 2, 5000, '0', '0', '0', '', '', '0', '0', '0', 0, 0, 'saved', '0', '2015-10-31 19:34:28', 'ABSA', '0000-00-00 00:00:00', ''),
(121, 105, 69, 'Absalom Shu', 696362464, 'absalomshu@fmail.ciom', 108523658, 4, 6000, '0', '0', '0', '', '', '0', '0', '0', 0, 0, 'saved', '0', '2015-10-31 20:05:51', 'ABSA', '0000-00-00 00:00:00', ''),
(122, 105, 69, 'Absalom Shu', 696362464, 'absalomshu@fmail.ciom', 108523658, 5, 6000, '0', '0', '0', '', '', '0', '0', '0', 0, 0, 'saved', '0', '2015-10-31 20:05:59', 'ABSA', '0000-00-00 00:00:00', ''),
(123, 105, 69, 'Absalom Shu', 696362464, 'absalomshu@fmail.ciom', 108523658, 6, 6000, '0', '0', '0', '', '', '0', '0', '0', 0, 0, 'saved', '0', '2015-10-31 20:07:20', 'ABSA', '0000-00-00 00:00:00', ''),
(124, 105, 69, 'Absalom Shu', 696362464, 'absalomshu@fmail.ciom', 108523658, 7, 6000, '0', '0', '0', '', '', '0', '0', '0', 0, 0, 'saved', '0', '2015-10-31 20:08:36', 'ABSA', '0000-00-00 00:00:00', ''),
(125, 105, 69, 'Absalom Shu', 696362464, 'absalomshu@fmail.ciom', 108523658, 8, 6000, '0', '0', '0', '', '', '0', '0', '0', 0, 0, 'saved', '0', '2015-10-31 20:09:09', 'ABSA', '0000-00-00 00:00:00', ''),
(126, 105, 69, 'Wrtewrt', 0, '', 0, 9, 6000, '0', '0', '0', '', '', '0', '0', '0', 0, 0, 'saved', '0', '2015-10-31 20:09:24', 'ABSA', '0000-00-00 00:00:00', ''),
(127, 105, 69, 'Absalom Shu', 696362464, 'absalomshu@fmail.ciom', 108523658, 10, 6000, '0', '0', '0', '', '', '0', '0', '0', 0, 0, 'saved', '0', '2015-10-31 20:12:23', 'ABSA', '0000-00-00 00:00:00', ''),
(128, 105, 69, 'Sss', 0, '', 0, 11, 6000, '0', '0', '0', '', '', '0', '0', '0', 0, 0, 'saved', '0', '2015-10-31 20:12:33', 'ABSA', '0000-00-00 00:00:00', ''),
(129, 105, 69, 'Ss', 0, '', 0, 12, 6000, '0', '0', '0', '', '', '0', '0', '0', 0, 0, 'saved', '0', '2015-10-31 20:12:40', 'ABSA', '0000-00-00 00:00:00', ''),
(130, 105, 69, 'Sdfd', 0, '', 0, 13, 6000, '0', '0', '0', '', '', '0', '0', '0', 0, 0, 'saved', '0', '2015-10-31 20:12:45', 'ABSA', '0000-00-00 00:00:00', ''),
(131, 105, 75, 'Absalom Shu', 696362464, 'absalomshu@fmail.ciom', 108523658, 2, 6000, '0', '0', '0', '', '', '0', '0', '0', 0, 0, 'saved', '0', '2015-11-01 08:27:52', 'ABSA', '0000-00-00 00:00:00', ''),
(132, 105, 75, 'Absalom Shu', 696362464, 'absalomshu@fmail.ciom', 108523658, 3, 6000, '0', '0', '0', '', '', '0', '0', '0', 0, 0, 'saved', '0', '2015-11-01 08:28:12', 'ABSA', '0000-00-00 00:00:00', ''),
(133, 105, 75, 'Absalom Shu', 696362464, 'absalomshu@fmail.ciom', 108523658, 4, 6000, '0', '0', '0', '', '', '0', '0', '0', 0, 0, 'saved', '0', '2015-11-01 08:30:11', 'ABSA', '0000-00-00 00:00:00', ''),
(134, 105, 75, 'Absalom Shu', 696362464, 'absalomshu@fmail.ciom', 108523658, 5, 6000, '0', '0', '0', '', '', '0', '0', '0', 0, 0, 'saved', '0', '2015-11-01 08:32:09', 'ABSA', '0000-00-00 00:00:00', ''),
(135, 105, 75, 'Absalom Shu', 696362464, 'absalomshu@fmail.ciom', 108523658, 6, 6000, '0', '0', '0', '', '', '0', '0', '0', 0, 0, 'saved', '0', '2015-11-01 08:32:20', 'ABSA', '0000-00-00 00:00:00', ''),
(136, 105, 75, 'Absalom Shu', 696362464, 'absalomshu@fmail.ciom', 108523658, 7, 6000, '0', '0', '0', '', '', '0', '0', '0', 0, 0, 'saved', '0', '2015-11-01 08:35:07', 'ABSA', '0000-00-00 00:00:00', ''),
(137, 105, 75, 'Absalom Shu', 696362464, 'absalomshu@fmail.ciom', 108523658, 8, 6000, '0', '0', '0', '', '', '0', '0', '0', 0, 0, 'saved', '0', '2015-11-01 08:35:57', 'ABSA', '0000-00-00 00:00:00', ''),
(138, 105, 75, 'Absalom Shu', 696362464, 'absalomshu@fmail.ciom', 108523658, 9, 6000, '0', '0', '0', '', '', '0', '0', '0', 0, 0, 'saved', '0', '2015-11-01 08:36:12', 'ABSA', '0000-00-00 00:00:00', ''),
(139, 105, 75, 'Absalom Shu', 696362464, 'absalomshu@fmail.ciom', 108523658, 10, 6000, '0', '0', '0', '', '', '0', '0', '0', 0, 0, 'saved', '0', '2015-11-01 08:38:35', 'ABSA', '0000-00-00 00:00:00', ''),
(140, 105, 75, 'Absalom Shu', 696362464, 'absalomshu@fmail.ciom', 108523658, 11, 6000, '0', '0', '0', '', '', '0', '0', '0', 0, 0, 'saved', '0', '2015-11-01 08:38:41', 'ABSA', '0000-00-00 00:00:00', ''),
(141, 105, 75, 'Absalom Shu', 696362464, 'absalomshu@fmail.ciom', 108523658, 12, 6000, '0', '0', '0', '', '', '0', '0', '0', 0, 0, 'saved', '0', '2015-11-01 08:39:14', 'ABSA', '0000-00-00 00:00:00', ''),
(142, 105, 75, 'Absalom Shu', 696362464, 'absalomshu@fmail.ciom', 108523658, 13, 6000, '0', '0', '0', '', '', '0', '0', '0', 0, 0, 'saved', '0', '2015-11-01 08:51:19', 'ABSA', '0000-00-00 00:00:00', ''),
(143, 105, 75, 'Absalom Shu', 696362464, 'absalomshu@fmail.ciom', 108523658, 14, 6000, '0', '0', '0', '', '', '0', '0', '0', 0, 0, 'saved', '0', '2015-11-01 08:51:25', 'ABSA', '0000-00-00 00:00:00', ''),
(144, 105, 75, 'Absalom Shu', 696362464, 'absalomshu@fmail.ciom', 108523658, 15, 6000, '0', '0', '0', '', '', '0', '0', '0', 0, 0, 'saved', '0', '2015-11-01 08:51:40', 'ABSA', '0000-00-00 00:00:00', ''),
(145, 105, 75, 'Absalom Shu', 696362464, 'absalomshu@fmail.ciom', 108523658, 16, 6000, '0', '0', '0', '', '', '0', '0', '0', 0, 0, 'saved', '0', '2015-11-01 08:51:45', 'ABSA', '0000-00-00 00:00:00', ''),
(146, 105, 75, 'Absalom Shu', 696362464, 'absalomshu@fmail.ciom', 108523658, 17, 6000, '0', '0', '0', '', '', '0', '0', '0', 0, 0, 'saved', '0', '2015-11-01 08:51:52', 'ABSA', '0000-00-00 00:00:00', ''),
(147, 105, 75, 'Absalom Shu', 696362464, 'absalomshu@fmail.ciom', 108523658, 18, 6000, '0', '0', '0', '', '', '0', '0', '0', 0, 0, 'saved', '0', '2015-11-01 08:51:58', 'ABSA', '0000-00-00 00:00:00', ''),
(148, 105, 75, 'Absalom Shu', 696362464, 'absalomshu@fmail.ciom', 108523658, 19, 6000, '0', '0', '0', '', '', '0', '0', '0', 0, 0, 'saved', '0', '2015-11-01 08:52:03', 'ABSA', '0000-00-00 00:00:00', ''),
(149, 105, 75, 'Absalom Shu', 696362464, 'absalomshu@fmail.ciom', 108523658, 20, 6000, '0', '0', '0', '', '', '0', '0', '0', 0, 0, 'saved', '0', '2015-11-01 08:52:13', 'ABSA', '0000-00-00 00:00:00', ''),
(150, 105, 75, 'Ab', 0, '', 0, 21, 6000, '0', '0', '0', '', '', '0', '0', '0', 0, 0, 'saved', '0', '2015-11-01 08:52:16', 'ABSA', '0000-00-00 00:00:00', ''),
(151, 105, 75, 'Ab', 0, '', 0, 22, 6000, '0', '0', '0', '', '', '0', '0', '0', 0, 0, 'saved', '0', '2015-11-01 08:52:19', 'ABSA', '0000-00-00 00:00:00', ''),
(152, 105, 75, 'Absalom Shu', 696362464, 'absalomshu@fmail.ciom', 108523658, 23, 6000, '0', '0', '0', '', '', '0', '0', '0', 0, 0, 'saved', '0', '2015-11-01 08:52:26', 'ABSA', '0000-00-00 00:00:00', ''),
(153, 105, 75, 'Sss', 0, '', 0, 24, 6000, '0', '0', '0', '', '', '0', '0', '0', 0, 0, 'saved', '0', '2015-11-01 08:52:34', 'ABSA', '0000-00-00 00:00:00', ''),
(154, 105, 75, 'Ab', 0, '', 0, 25, 6000, '0', '0', '0', '', '', '0', '0', '0', 0, 0, 'saved', '0', '2015-11-01 08:54:48', 'ABSA', '0000-00-00 00:00:00', ''),
(155, 105, 75, 'Absalom Shu', 696362464, 'absalomshu@fmail.ciom', 108523658, 26, 6000, '0', '0', '0', '', '', '0', '0', '0', 0, 0, 'saved', '0', '2015-11-01 08:55:45', 'ABSA', '0000-00-00 00:00:00', ''),
(156, 105, 75, 'Absalom Shu', 696362464, 'absalomshu@fmail.ciom', 108523658, 27, 6000, '0', '0', '0', '', '', '0', '0', '0', 0, 0, 'saved', '0', '2015-11-01 08:56:55', 'ABSA', '0000-00-00 00:00:00', ''),
(157, 105, 75, 'Killian', 0, '', 0, 28, 6000, '0', '0', '0', '', '', '0', '0', '0', 0, 0, 'saved', '0', '2015-11-01 08:57:07', 'ABSA', '0000-00-00 00:00:00', ''),
(158, 105, 76, 'Absalom Shu', 696362464, 'absalomshu@fmail.ciom', 108523658, 2, 8000, '0', '0', '0', '', '', '0', '0', '0', 0, 0, 'saved', '0', '2015-11-02 10:37:42', 'ABSA', '0000-00-00 00:00:00', ''),
(159, 105, 76, 'As', 0, '', 0, 3, 8000, '0', '0', '0', '', '', '0', '0', '0', 0, 0, 'saved', '0', '2015-11-02 15:27:51', 'ABSA', '0000-00-00 00:00:00', ''),
(160, 105, 76, 'Ss', 0, '', 0, 4, 8000, '0', '0', '0', '', '', '0', '0', '0', 0, 0, 'saved', '0', '2015-11-02 15:27:55', 'ABSA', '0000-00-00 00:00:00', ''),
(162, 105, 77, 'Absalom Shu', 696362464, 'absalomshu@fmail.ciom', 108523658, 3, 5000, '0', '0', '0', '', '', '0', '0', '0', 0, 0, 'saved', '0', '2015-11-18 15:22:07', 'ABSA', '0000-00-00 00:00:00', ''),
(163, 105, 78, 'Absalom Shu', 696362464, 'absalomshu@fmail.ciom', 108523658, 2, 5000, '0', '0', '0', '', '', '0', '0', '0', 0, 0, 'saved', '0', '2016-02-05 08:12:37', 'ABSA', '0000-00-00 00:00:00', ''),
(164, 105, 81, 'Absa', 0, '', 0, 2, 3000, '0', '0', '0', '', '', '0', '0', '0', 0, 0, 'saved', '0', '2016-02-18 14:36:46', 'ABSA', '0000-00-00 00:00:00', ''),
(165, 105, 81, 'Absalom Shu', 696362464, 'absalomshu@fmail.ciom', 108523658, 3, 3000, '0', '0', '0', '', '', '0', '0', '0', 0, 0, 'saved', '0', '2016-02-18 14:37:09', 'ABSA', '0000-00-00 00:00:00', ''),
(166, 105, 93, 'John Doe', 698577784, '', 152523456, 2, 5000, '0', '0', '0', '', '', '0', '0', '0', 0, 0, 'saved', '0', '2016-05-19 02:21:08', 'ABSA', '0000-00-00 00:00:00', ''),
(167, 105, 93, 'Jacky Lee', 0, '', 108566636, 3, 3000, '0', '0', '0', '', '', '0', '0', '0', 0, 0, 'saved', '0', '2016-05-19 02:21:34', 'ABSA', '0000-00-00 00:00:00', ''),
(168, 105, 94, 'Souleymanou Ousman', 0, '', 102547856, 2, 5000, '0', '0', '0', '', '', '0', '0', '0', 0, 0, 'saved', '0', '2016-05-19 03:25:46', 'ABSA', '0000-00-00 00:00:00', ''),
(169, 105, 94, 'John Doe', 698562147, 'johndoe@gmail.com', 108563214, 3, 3000, '0', '0', '0', '', '', '0', '0', '0', 0, 0, 'saved', '0', '2016-05-19 03:26:32', 'ABSA', '0000-00-00 00:00:00', ''),
(170, 105, 99, 'Shu', 0, '', 0, 2, 10000, '0', '0', '0', '', '', '0', '0', '0', 0, 0, 'saved', '0', '2016-08-29 09:50:47', 'ABSA', '0000-00-00 00:00:00', ''),
(171, 105, 98, 'Y', 0, '', 0, 2, 6000, '0', '0', '0', '', '', '0', '0', '0', 0, 0, 'saved', '0', '2016-09-21 02:16:20', 'ABSA', '0000-00-00 00:00:00', ''),
(172, 105, 97, 'Op', 0, '', 0, 2, 6000, '0', '0', '0', '', '', '0', '0', '0', 0, 0, 'saved', '0', '2016-09-21 02:21:47', 'ABSA', '0000-00-00 00:00:00', ''),
(173, 105, 96, 'Ih', 0, '', 0, 2, 10000, '0', '0', '0', '', '', '0', '0', '0', 0, 0, 'saved', '0', '2016-09-21 02:23:31', 'ABSA', '0000-00-00 00:00:00', ''),
(174, 105, 83, 'Hij', 0, '', 0, 2, 8000, '0', '0', '0', '', '', '0', '0', '0', 0, 0, 'saved', '0', '2016-09-21 02:24:50', 'ABSA', '0000-00-00 00:00:00', ''),
(175, 105, 102, 'Kabila', 0, '', 0, 2, 6000, '0', '0', '0', '', '', '0', '0', '0', 0, 0, 'saved', '0', '2016-09-22 01:59:10', 'ABSA', '0000-00-00 00:00:00', ''),
(176, 105, 79, 'Shu', 0, '', 0, 2, 10000, '0', '0', '0', '', '', '0', '0', '0', 0, 0, 'saved', '0', '2016-09-30 11:43:14', 'ABSA', '0000-00-00 00:00:00', ''),
(178, 105, 79, 'Absal', 0, '', 10233232, 3, 10000, '0', '0', '0', '', '', '0', '0', '0', 0, 0, 'saved', '0', '2016-10-07 06:47:40', 'ABSA', '0000-00-00 00:00:00', ''),
(179, 105, 79, 'Ch', 0, '', 0, 4, 6000, '0', '0', '0', '', '', '0', '0', '0', 0, 0, 'saved', '0', '2016-10-10 03:12:38', 'ABSA', '0000-00-00 00:00:00', ''),
(180, 105, 107, 'Absalom Shu', 696362464, 'absalomshu@fmail.ciom', 108523658, 2, 10000, '0', '0', '0', '', '', '0', '0', '0', 0, 0, 'saved', '0', '2016-11-30 01:42:07', 'ABSA', '0000-00-00 00:00:00', ''),
(181, 105, 107, 'Absalom Shu', 696362464, 'absalomshu@fmail.ciom', 108523658, 3, 10000, '0', '0', '0', '', '', '0', '0', '0', 0, 0, 'saved', '0', '2016-12-14 04:53:20', 'ABSA', '0000-00-00 00:00:00', ''),
(182, 105, 107, 'Asong', 0, '', 0, 4, 10000, '0', '0', '0', '', '', '0', '0', '0', 0, 0, 'saved', '0', '2016-12-14 04:53:41', 'ABSA', '0000-00-00 00:00:00', ''),
(183, 105, 17, 'Ssal', 0, '', 0, 4, 3000, '0', '0', '0', '', '', '0', '0', '0', 0, 0, 'saved', '0', '2016-12-16 13:21:20', 'ABSA', '0000-00-00 00:00:00', ''),
(184, 105, 17, 'Ssop', 0, '', 0, 9, 0, '0', '0', '1', '', '', '0', '0', '0', 0, 0, 'saved', '0', '2016-12-16 13:21:54', 'ABSA', '0000-00-00 00:00:00', ''),
(185, 106, 110, 'Ss', 0, '', 0, 2, 2000, '0', '0', '0', '', '', '0', '0', '0', 0, 0, 'saved', '0', '2017-01-07 04:20:10', 'ACHA', '0000-00-00 00:00:00', '');

-- --------------------------------------------------------

--
-- Table structure for table `towns`
--

CREATE TABLE IF NOT EXISTS `towns` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `name` varchar(255) NOT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB  DEFAULT CHARSET=latin1 AUTO_INCREMENT=31 ;

--
-- Dumping data for table `towns`
--

INSERT INTO `towns` (`id`, `name`) VALUES
(1, 'Buea'),
(2, 'Bamenda'),
(3, 'Yaounde'),
(4, 'Douala'),
(5, 'Bafoussam'),
(6, 'Dschang'),
(7, 'Limbe'),
(8, 'Melong'),
(9, 'Kribi'),
(10, 'Bafang'),
(11, 'Baham'),
(12, 'Bandja'),
(13, 'Bandjoun'),
(14, 'Batie'),
(15, 'Nkongsamba'),
(17, 'Edea'),
(18, 'Loum'),
(19, 'Mandjo'),
(20, 'Matom'),
(21, 'Mbanga'),
(22, 'Mbounda'),
(23, 'Mutengene'),
(24, 'Njombe'),
(25, 'Penja'),
(26, 'Boumyebel'),
(27, 'Pouma'),
(28, 'Sancho'),
(29, 'Kumba'),
(30, 'Matonde');

-- --------------------------------------------------------

--
-- Table structure for table `transactions`
--

CREATE TABLE IF NOT EXISTS `transactions` (
  `online` enum('0','1','2') NOT NULL DEFAULT '0' COMMENT 'Whether this data is available online or not. 2 is when the date causes an error, or wasn''t uploaded, but should not be come back to, as if so, it will overwrite a change made after it.',
  `table` enum('buses','expenses','parcels','schedules') NOT NULL DEFAULT 'schedules',
  `agency_id` int(11) NOT NULL,
  `change_id` int(11) NOT NULL,
  `change_type` enum('insert','update','delete') NOT NULL,
  `change_time` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP,
  `agency_code` varchar(255) NOT NULL,
  `bus_number` varchar(255) NOT NULL,
  `bus_seats` int(255) NOT NULL COMMENT '?? seater',
  `vip` enum('0','1') NOT NULL DEFAULT '0',
  `from` varchar(255) NOT NULL,
  `to` varchar(255) NOT NULL,
  `ticket_price` int(12) NOT NULL,
  `seat_occupants` mediumtext NOT NULL,
  `reserved_seats` mediumtext NOT NULL,
  `status` enum('current','departed') NOT NULL DEFAULT 'current',
  `loading` enum('0','1','2') NOT NULL DEFAULT '0',
  `departure_date` date NOT NULL,
  `departure_time` time NOT NULL,
  `checked_out_on` datetime NOT NULL,
  `checked_out_time` time NOT NULL COMMENT 'Time bus was checked out',
  `total_amount` int(10) NOT NULL COMMENT 'Total amount got from this schedule',
  `driver` varchar(255) NOT NULL,
  `deleted` enum('0','1') NOT NULL,
  `amount` int(11) NOT NULL,
  `purpose` text NOT NULL,
  `date_incurred` date NOT NULL,
  `authorised_by` varchar(255) NOT NULL,
  `receiver_name` varchar(255) NOT NULL,
  `receiver_phone` int(11) NOT NULL,
  `collected_by` varchar(255) NOT NULL,
  `collected_on` datetime NOT NULL,
  `description` text NOT NULL,
  `price` int(11) NOT NULL,
  `sent_date` date NOT NULL,
  `parcel_state` enum('sent','received','delivered') DEFAULT NULL,
  `schedule_id` int(11) NOT NULL,
  `CreatedBy` varchar(25) NOT NULL,
  `CreatedOn` timestamp NOT NULL DEFAULT '0000-00-00 00:00:00',
  `CheckedOutBy` varchar(255) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Dumping data for table `transactions`
--

INSERT INTO `transactions` (`online`, `table`, `agency_id`, `change_id`, `change_type`, `change_time`, `agency_code`, `bus_number`, `bus_seats`, `vip`, `from`, `to`, `ticket_price`, `seat_occupants`, `reserved_seats`, `status`, `loading`, `departure_date`, `departure_time`, `checked_out_on`, `checked_out_time`, `total_amount`, `driver`, `deleted`, `amount`, `purpose`, `date_incurred`, `authorised_by`, `receiver_name`, `receiver_phone`, `collected_by`, `collected_on`, `description`, `price`, `sent_date`, `parcel_state`, `schedule_id`, `CreatedBy`, `CreatedOn`, `CheckedOutBy`) VALUES
('1', 'schedules', 106, 109, 'update', '2017-01-07 03:28:00', '', 'NW111', 70, '0', '4', '3', 5000, '', '', 'current', '0', '2017-01-07', '20:00:00', '0000-00-00 00:00:00', '00:00:00', 0, '', '1', 0, '', '0000-00-00', '', '', 0, '', '0000-00-00 00:00:00', '', 0, '0000-00-00', NULL, 0, 'ACHA', '0000-00-00 00:00:00', ''),
('1', 'schedules', 106, 110, 'insert', '2017-01-07 03:28:08', '', 'NW111', 70, '0', '4', '3', 2000, '', '', 'current', '0', '2017-01-07', '20:00:00', '0000-00-00 00:00:00', '00:00:00', 0, '', '0', 0, '', '0000-00-00', '', '', 0, '', '0000-00-00 00:00:00', '', 0, '0000-00-00', NULL, 0, 'ACHA', '0000-00-00 00:00:00', ''),
('1', 'schedules', 106, 110, 'update', '2017-01-07 04:20:10', '', 'NW111', 70, '0', '4', '3', 2000, '{"2":{"name":"Ss","phone":"","email":"","idc":"","free_ticket":0}}', '', 'current', '0', '2017-01-07', '20:00:00', '0000-00-00 00:00:00', '00:00:00', 0, '', '0', 0, '', '0000-00-00', '', '', 0, '', '0000-00-00 00:00:00', '', 0, '0000-00-00', NULL, 0, 'ACHA', '0000-00-00 00:00:00', ''),
('1', 'schedules', 106, 110, 'update', '2017-01-07 04:20:17', '', 'NW111', 70, '0', '4', '3', 2000, '{"2":{"name":"Ss","phone":"","email":"","idc":"","free_ticket":0}}', '', 'departed', '0', '2017-01-07', '20:00:00', '2017-01-07 00:00:00', '05:20:17', 2000, '', '0', 0, '', '0000-00-00', '', '', 0, '', '0000-00-00 00:00:00', '', 0, '0000-00-00', NULL, 0, 'ACHA', '0000-00-00 00:00:00', 'ACHA'),
('1', 'expenses', 105, 66, 'insert', '2017-01-08 14:44:44', '', '', 0, '0', '', '', 0, '', '', 'current', '0', '0000-00-00', '00:00:00', '0000-00-00 00:00:00', '00:00:00', 0, '', '0', 15000, 'sss', '2017-01-08', 'Clement', '', 0, '', '0000-00-00 00:00:00', '', 0, '0000-00-00', NULL, 0, 'ABSA', '2017-01-08 14:44:44', '');

-- --------------------------------------------------------

--
-- Table structure for table `users`
--

CREATE TABLE IF NOT EXISTS `users` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `username` varchar(20) NOT NULL,
  `names` varchar(255) NOT NULL,
  `email` varchar(120) NOT NULL,
  `phone` int(11) NOT NULL,
  `password` varchar(40) NOT NULL,
  `user_group_id` enum('1','2','3') NOT NULL DEFAULT '1',
  `is_email_confirmed` enum('0','1') NOT NULL DEFAULT '0' COMMENT 'Whether user confirmed by clicking link in his email',
  `gender` enum('m','f') NOT NULL,
  `dob` varchar(45) NOT NULL,
  `image` varchar(255) NOT NULL,
  `timezone` varchar(45) NOT NULL,
  `reset_hash` varchar(40) NOT NULL,
  `salt` varchar(7) NOT NULL,
  `sso_fb` varchar(255) NOT NULL,
  `status` tinyint(1) NOT NULL,
  `deleted` tinyint(1) NOT NULL,
  `reset_by` int(10) NOT NULL,
  `last_login` datetime NOT NULL,
  `last_ip` varchar(40) NOT NULL,
  `created_on` datetime NOT NULL,
  `created_by` bigint(20) NOT NULL,
  `updated_on` datetime NOT NULL,
  `updated_by` bigint(20) NOT NULL,
  PRIMARY KEY (`id`)
) ENGINE=MyISAM  DEFAULT CHARSET=latin1 AUTO_INCREMENT=22 ;

--
-- Constraints for dumped tables
--

--
-- Constraints for table `admins`
--
ALTER TABLE `admins`
  ADD CONSTRAINT `admins_ibfk_1` FOREIGN KEY (`agency_id`) REFERENCES `agencies` (`id`),
  ADD CONSTRAINT `admins_ibfk_2` FOREIGN KEY (`admin_group_id`) REFERENCES `admin_groups` (`id`);

--
-- Constraints for table `admin_dashboard_items`
--
ALTER TABLE `admin_dashboard_items`
  ADD CONSTRAINT `admin_dashboard_items_ibfk_1` FOREIGN KEY (`DashboardItemID`) REFERENCES `dashboard_items` (`id`);

--
-- Constraints for table `admin_modules`
--
ALTER TABLE `admin_modules`
  ADD CONSTRAINT `admin_modules_ibfk_1` FOREIGN KEY (`MainModuleID`) REFERENCES `main_modules` (`id`);

--
-- Constraints for table `agencies`
--
ALTER TABLE `agencies`
  ADD CONSTRAINT `agencies_ibfk_1` FOREIGN KEY (`parent_id`) REFERENCES `parents` (`id`),
  ADD CONSTRAINT `agencies_ibfk_2` FOREIGN KEY (`town_id`) REFERENCES `towns` (`id`);

--
-- Constraints for table `buses`
--
ALTER TABLE `buses`
  ADD CONSTRAINT `buses_ibfk_1` FOREIGN KEY (`agency_id`) REFERENCES `agencies` (`id`);

--
-- Constraints for table `bus_requirements_statuses`
--
ALTER TABLE `bus_requirements_statuses`
  ADD CONSTRAINT `bus_requirements_statuses_ibfk_1` FOREIGN KEY (`BusNumber`) REFERENCES `buses` (`bus_number`) ON DELETE CASCADE ON UPDATE CASCADE,
  ADD CONSTRAINT `bus_requirements_statuses_ibfk_2` FOREIGN KEY (`BusRequirementID`) REFERENCES `bus_requirements` (`id`) ON DELETE CASCADE ON UPDATE CASCADE;

--
-- Constraints for table `expenses`
--
ALTER TABLE `expenses`
  ADD CONSTRAINT `expenses_ibfk_1` FOREIGN KEY (`agency_id`) REFERENCES `agencies` (`id`);

--
-- Constraints for table `incoming_parcels`
--
ALTER TABLE `incoming_parcels`
  ADD CONSTRAINT `incoming_parcels_ibfk_1` FOREIGN KEY (`agency_id`) REFERENCES `agencies` (`id`),
  ADD CONSTRAINT `incoming_parcels_ibfk_4` FOREIGN KEY (`from`) REFERENCES `agencies` (`id`),
  ADD CONSTRAINT `incoming_parcels_ibfk_5` FOREIGN KEY (`to`) REFERENCES `agencies` (`id`);

--
-- Constraints for table `parcels`
--
ALTER TABLE `parcels`
  ADD CONSTRAINT `parcels_ibfk_1` FOREIGN KEY (`agency_id`) REFERENCES `agencies` (`id`),
  ADD CONSTRAINT `parcels_ibfk_2` FOREIGN KEY (`from`) REFERENCES `agencies` (`id`),
  ADD CONSTRAINT `parcels_ibfk_3` FOREIGN KEY (`to`) REFERENCES `agencies` (`id`);

--
-- Constraints for table `reminder_schedules`
--
ALTER TABLE `reminder_schedules`
  ADD CONSTRAINT `reminder_schedules_ibfk_1` FOREIGN KEY (`AgencyID`) REFERENCES `agencies` (`id`),
  ADD CONSTRAINT `reminder_schedules_ibfk_2` FOREIGN KEY (`ReminderID`) REFERENCES `reminders` (`id`) ON DELETE CASCADE ON UPDATE CASCADE;

--
-- Constraints for table `schedules`
--
ALTER TABLE `schedules`
  ADD CONSTRAINT `schedules_ibfk_1` FOREIGN KEY (`from`) REFERENCES `towns` (`id`),
  ADD CONSTRAINT `schedules_ibfk_2` FOREIGN KEY (`to`) REFERENCES `towns` (`id`);

--
-- Constraints for table `sms`
--
ALTER TABLE `sms`
  ADD CONSTRAINT `sms_ibfk_1` FOREIGN KEY (`agency_id`) REFERENCES `agencies` (`id`);

--
-- Constraints for table `ticket_details`
--
ALTER TABLE `ticket_details`
  ADD CONSTRAINT `ticket_details_ibfk_1` FOREIGN KEY (`AgencyID`) REFERENCES `agencies` (`id`);

/*!40101 SET CHARACTER_SET_CLIENT=@OLD_CHARACTER_SET_CLIENT */;
/*!40101 SET CHARACTER_SET_RESULTS=@OLD_CHARACTER_SET_RESULTS */;
/*!40101 SET COLLATION_CONNECTION=@OLD_COLLATION_CONNECTION */;
