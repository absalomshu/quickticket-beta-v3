<?php defined('SYSPATH') or die('No direct script access');

class Remote_Ticket_Detail_Model extends ORM{

	public function add_remote_ticket($agency_id, $schedule_id, $seat_number, $client_name, $price, $missed_status=0, $free_ticket, $free_ticket_reason, $created_by)
	{	
		$ticket = ORM::FACTORY('remote_ticket_detail');
		$ticket->AgencyID = $agency_id;
		$ticket->ScheduleID = $schedule_id;
		$ticket->SeatNumber = $seat_number;
		$ticket->ClientName = $client_name;
		$ticket->Price = $price;
		$ticket->MissedStatus = $missed_status;
		$ticket->FreeTicket = $free_ticket;
		$ticket->FreeTicketReason = $free_ticket_reason;
		$ticket->CreatedBy = $created_by;
	
		$ticket->save();
		return $ticket->id;
	}
	
	public function get_all($agency_id)
	{
		$purchases=ORM::factory('remote_ticket_detail')->where('agency_id',$agency_id)->find_all();
		return $purchases;	
	}
	
	public function get_all_active($agency_id)
	{
		$purchases=ORM::factory('remote_ticket_detail')->where('agency_id',$agency_id)->where('status','active')->find_all();
		return $purchases;	
	}
	
	
	public function mark_closed($agency_id,$purchase_id)
	{
		$purchase=ORM::factory('remote_ticket_detail')->where('agency_id',$agency_id)->where('id',$purchase_id)->find();
		$purchase->status = 'closed';
		$purchase->save();
		
		$notice="Purchase confimed and marked closed";
		return $notice;	
	}
	


	

}
