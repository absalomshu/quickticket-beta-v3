<?php defined('SYSPATH') or die('No direct script access');

class Cash_Interbranch_Model extends ORM{

	public function add($agency_id, $receiver_agency_id, $amount, $description, $date, $admin_username, $current_date )
	{
		$cash_interbranch = ORM::FACTORY('cash_interbranch');
		$cash_interbranch->agency_id = $agency_id;
		$cash_interbranch->receiver_agency_id = $receiver_agency_id;
		$cash_interbranch->amount = $amount;
		$cash_interbranch->description = $description;
		$cash_interbranch->date_incurred = $date;
		$cash_interbranch->created_by = STRTOUPPER($admin_username);
		$cash_interbranch->created_on = $current_date;
		$cash_interbranch->save();
		
		//Update the admin's availaible balance
		Admin_Model::subtract_balance($admin_username, $amount);
		//Update the collector's available balance
		//Admin_Model::add_balance($createdBy, $amount);	
	}
	
	
	public function get_all($agency_id)
	{
		$cib = new Database();
		$cib=ORM::factory('cash_interbranch')->where('deleted','0')->find_all();
		return $cib;
	}
	
	public function get_all_unpushed($agency_id)
	{
		$ctb = new Database();
		$ctb=ORM::factory('cash_to_bank')->where('agency_id',$agency_id)->where('deleted','0')->where('online','0')->find_all();
		return $ctb;
	}
	
	public function get_ctb_from_admin($admin_username)
	{
		$db = new Database();
		$collections_for_admin=ORM::factory('cash_to_bank')->where('deleted','0')->where('admin_id',$admin_username)->find_all();
		return $collections_for_admin;
	}
	
	public function get_agency_expenses_by_period_by_bus($agency_id,$start_date,$end_date,$bus_number)
	{
		$db = new Database();
		$expenses = $db->query("SELECT * FROM expenses WHERE agency_id= ".$agency_id." AND bus_number= '".$bus_number."' AND deleted = '0' AND date_incurred BETWEEN CAST('$start_date' AS DATE) AND CAST('$end_date' AS DATE) ORDER BY date_incurred ASC");
		return $expenses;
	}
	

}
