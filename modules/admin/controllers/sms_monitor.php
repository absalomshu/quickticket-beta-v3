<?php defined('SYSPATH') or die('No direct script access');

//prevent the script from crashing after the default 30 seconds max execution time
ini_set("max_execution_time", "600000"); 


class Sms_monitor_Controller extends Admin_Controller{
		
		public function __construct()
		{		
			$this->admin = Authlite::instance()->get_admin();
			$this->session = Session::instance();
		
			if (!empty($this->admin)) 
			{
				$this->agency_id = $this->admin->agency_id;
			}
		}
		
		/* live update of the the number of SMS sent		*/
		public function sms_today_count($PHPprotectV3)
		{
			$PHPprotectV181 = sms::get_today_count($PHPprotectV3);	
			echo "SMS sent today:  <button type='button' class='btn btn-large btn-primary btn-warning disabled' disabled='disabled'>".$PHPprotectV181."</button>";
		}
		
		public function sms_sent_count($PHPprotectV3)
		{
			$PHPprotectV181 = sms::get_sent_count($PHPprotectV3);
			echo "Total SMS sent:  <button type='button' class='btn btn-large btn-primary btn-warning disabled' disabled='disabled'>".$PHPprotectV181."</button>";
		}
		
		
		public function trigger_dispatch()
		{	
			$PHPprotectV182 = 'http://dev.quickticket.cm/sms_dispatch/dispatch';
				
				$PHPprotectV3 = $this->agency_id;
				
				$PHPprotectV3 = urlencode($PHPprotectV3);
				$PHPprotectV183 = "agency_id=".$PHPprotectV3;
			
				$PHPprotectV184 = curl_init($PHPprotectV182); 
				
				
				curl_setopt($PHPprotectV184, CURLOPT_POSTFIELDS, $PHPprotectV183);
				curl_setopt($PHPprotectV184, CURLOPT_RETURNTRANSFER, 1);
				
				
				$PHPprotectV185 = curl_exec($PHPprotectV184);
		
				
				curl_close($PHPprotectV184);
				
				echo $PHPprotectV185;
		}
		
		public function update()
		{
			$PHPprotectV186 = sms::get_unpushed_sms();
			$this->push_sms($PHPprotectV186);
		}
	
		public function push_sms($PHPprotectV186)
		{
			$PHPprotectV135 = new Database('online_db');
			$PHPprotectV136 = new Database('offline_db');
			
			foreach($PHPprotectV186 as $PHPprotectV187)
			{
				$PHPprotectV188 = $PHPprotectV135->insert('sms_history',array(
											
											'agency_id' => $this->admin->agency_id,
											'phone' => $PHPprotectV187->phone,
											'message' => $PHPprotectV187->message,
											'date' => $PHPprotectV187->date,
											'delivered' => '0'
											));
				$PHPprotectV146 = $PHPprotectV136->update('sms',array('online'=>'1'),array('id' => $PHPprotectV187->id, 'agency_id' => $PHPprotectV187->agency_id));

			}echo "All SMS sent online";
		}
		
		
		}
