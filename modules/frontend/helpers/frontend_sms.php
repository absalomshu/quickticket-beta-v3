<?php
/**
 * Description: wasaTEXTO API v2
 * Send SMS
 * @package wasamundi.com
 * @subpackage:api
 * @license wasamundi labs 2011
 * @author Wasamundi Team
 */
 //set default timezone, otherwise it's UTC which is an hour late
date_default_timezone_set('Africa/Douala');
 
//prevent the script from crashing after the default 30 seconds max execution time
ini_set("max_execution_time", "600000");
 
class Frontend_sms_Core
{
	public function save_and_send($msg,$to,$agency_id){	
		//Send_sms controller sends SMS and saves to db, so just pass it the parameters.
		//Send_sms::save_and_send($msg,$to,$agency_id);
		
		//var_dump($_REQUEST);EXIT;
		$texto = 'http://api.wasamundi.com/v2/texto/';
		//$msg = $_POST['msg'];
		//$to = $_POST['to'];
		//$agency_id = $_POST['agency_id'];
		// Build the post data
		$api_user = "Quickticket";
		$api_key = 'Srbx0Mpi';
		$from = 'QuickTicket';
		$msg = $msg;
		//msg is left unencoded so it's saved in DB without pluses, but needs to be encoded to send via texto
		$post = $texto."send_sms?user=".$api_user."&api_key=".$api_key."&from=".$from."&to=".$to."&msg=".urlencode($msg);
			

		
		$data = curl_init(); //Initialize the cURL session
		curl_setopt($data, CURLOPT_URL, $post);	//Set the URL	
		//Ask cURL to return the contents in a variable 
		//instead of simply echoing them to  the browser.
		curl_setopt($data, CURLOPT_RETURNTRANSFER, 1);
		//Execute the cURL session
		$notice = curl_exec ($data);
			
		//Close cURL session
		curl_close ($data);
		$result = json_decode($notice);
		//return $result;
		//var_dump($result);EXIT;
		//Save to history
		$onlinedb = new Database();
		//if status is 200, sms was delivered. set delivered to 1
		$delivered = '0';
		if(substr($notice, 10, 3) == "200"){ $delivered = '1';}
		$register_sms = $onlinedb->insert('sms_history',array(
							
							
							'agency_id' => $agency_id,
							'message' => $msg,
							'phone' => $to,
							'date' => date("Y-m-d H:i:s"),
							'delivered_on' => date("Y-m-d H:i:s"),
							'delivered' => $delivered
							
							));
		//var_dump($notice);exit;
		 
	}
		
	
	public function save($msg,$to,$agency_id){
		$sms = ORM::FACTORY('sms_history');
		$sms->agency_id = $agency_id;
		$sms->date = date('Y-m-d H:i:s');
		$sms->phone = $to;
		
		//Don't url encode here as it's done by the sender online. Else it's done twice and messages come with +
		//$sms->message = urlencode($msg);
		$sms->message = $msg;
		if($sms->save())
		{
			return "Saved";
		}else
		{
			return "Error. Please try again";
		};
		
	
	}
	
	public function get_history($agency_id){
		$onlinedb = new Database('online_db');
		$total_sms = $onlinedb->from('sms_history')->where('agency_id' , $agency_id)->getwhere();
		return $total_sms;
	}
	
	public function get_sent_history($agency_id){
		$onlinedb = new Database('online_db');
		$sent_sms = $onlinedb->from('sms_history')->where('agency_id' , $agency_id)->where('delivered','1')->getwhere();
		return $sent_sms;
	}
	
	public function get_sent_count($agency_id){
		$onlinedb = new Database('online_db');
		$sent_sms = $onlinedb->from('sms_history')->where('agency_id' , $agency_id)->where('delivered','1')->getwhere();
		return count($sent_sms);
	}
	
	
	
	//Get the number of SMS sent today.
	public function get_today_count($agency_id){
		//date of today
		$today=date('d-m-Y');
		
		$onlinedb = new Database('online_db');
		$sent_sms = $onlinedb->from('sms_history')->where('agency_id' , $agency_id)->where('delivered','1')->getwhere();
		//var_dump($sent_sms);exit;
		$count = 0;
		foreach($sent_sms as $sms)
		{	//var_dump($sms->date);exit;
			$sent_date = date('d-m-Y',strtotime($sms->date));
			if ($sent_date == $today)
			{
				$count++;
			}
		}
		return $count;
	}
	
	public function get_unpushed_sms(){
		//check if an SMS has been pushed online or not
		$unpushed = ORM::factory('sms')->where('online','0')->orderby('date','DESC')->find_all();	
		return $unpushed;
	}
	
	
	
}