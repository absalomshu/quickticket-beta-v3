<?php defined('SYSPATH') or die('No direct script access'); ?>
	
    <div class="container">
		
		
		<div class="row-fluid  span12 hero-unit prof-unit reload" style="margin:5px 0 0px 0;">	
			<div class="span12" >
			<div class="no-print">
			<ul class="nav nav-tabs">
					<li > <a href="<?=url::site('reports')?>"><i class="icon-arrow-left"></i> <?=Kohana::lang('backend.back_reports')?></a></li>
				</ul>
			</div>
			
			<div class="span8 offset2 print-area" >
				<legend><?=Kohana::lang('backend.search_results') . "  for "?><i><?="\"".$passenger_name."\""?></i><button class="view" onClick="window.print()"><i class="icon-print"></i> <?=Kohana::lang('backend.print')?></button></legend>
				<?php if(count($search_results)!=0){?>
				<table class="table table-bordered table-condensed">
					<tr>
						<th>Name</th>
						<th>IDC</th>
						<th>Phone</th>
					
					
					</tr>
					<?php 
					foreach ($search_results as $result)
					{
						//var_dump($result);exit;
						?>
					<tr>
						<td class=""><a href="<?="passenger_activity/".$result->ClientName."/".$result->ClientIDC?>"><?=$result->ClientName?></a></td>
						
						<td class=""><?php if(!empty($result->ClientIDC)){echo $result->ClientIDC;}?></td>
						<td class=""><?php if(!empty($result->ClientPhone)){echo $result->ClientPhone;}?></td>
					</tr>
					<?php }?>
					
				</table>
				<?php } else echo "No match found.";?>
				
			</div>
			
		</div>
			
		
		
		
		
		
		
		</div>
    </div> 
