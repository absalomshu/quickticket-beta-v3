<?php defined('SYSPATH') or die('No direct script access'); ?> 


	<?php
		//determine what type of notice to display if at all
		$notice = $this->session->get_once('notice');
			if(!empty($notice)){ 
				if($notice['type'] == 'success'){?><div class="alert general success"><a class="close" data-dismiss="alert" href="#">&times;</a><?php echo $notice['message']; }?></div>
			<?	if($notice['type'] == 'error'){?><div class="alert general error"><a class="close" data-dismiss="alert" href="#">&times;</a><?php echo $notice['message']; }?></div>
		<?}?>
    <div class="container">
	  
	  <div class="row-fluid marketing" style="margin:5px 0 0px 0px;"> 
			<div class="span6 hero-unit prof-unit reload" style="margin-left:0px;">
				<div class="heading">
				<div class="heading"><?=Kohana::lang('backend.sms_sent_today')?>	</div>
				</div>
				<div class="rule"><hr/></div>
					<table class="table">
					<?php 
					
					//$no_sms_sent = sms::get_today_history($this->admin->agency_id);
					//	echo "Total SMS sent today = $no_sms_sent ";
					
					?>
					</table>
					<span class="sms_today">
						<!-- DON'T DELETE THIS DIV. IT'S FOR THE SMS HISTORY-->	
						Loading...
					</span>
			</div>
			<div class="span6 hero-unit prof-unit">
				<div class="heading"><?=Kohana::lang('backend.total_sms_sent')?></div>
				<div class="rule"><hr/></div>
				
					<table class="table table-striped">
						<?php 						
						
						//$sms_sent = sms::get_sent_history($this->admin->agency_id);
						//echo "Total SMS sent = ".count($sms_sent);?>
					</table>	
					<span class="sms_sent">
						<!-- DON'T DELETE THIS DIV. IT'S FOR THE SMS HISTORY-->	
						Loading...
					</span>
				
		</div>	
		<div class="span6 hero-unit prof-unit" style="margin-left:0px;" >
		<form action="<?=url::site('control/schedule_by_date/')?>" method="POST">
				<div class="heading">&nbsp;</div>
				<div class="rule"><hr/></div>
				
				
					
								
					
		</form>		
		</div>
		<div class="span6 hero-unit prof-unit" >
				
					<!--Reloads the page. Still to find better way to reload only div. Calling the function doesn't work-->		
					<button class="btn  btn-info btn-large" type="submit" onclick="sms_statistics()" ><i class="icon-refresh icon-white"></i> <?=Kohana::lang('backend.refresh_stats')?></button><br/>
		</div>
		
	
		
		
	  </div>
	  <div style="height:19px;"></div>
    </div> 
	<?php //$this->profiler = new Profiler();?>

<script type="text/javascript">

	/*$(document).ready(
	function(){	
			$('.sms_today').load('<?=url::site('sms_monitor/sms_today_count/'.$this->admin->agency_id);?>');
			$('.sms_sent').load('<?=url::site('sms_monitor/sms_sent_count/'.$this->admin->agency_id);?>');	
	}); */
	
	//easier way to load function upon page load
	window.onload = sms_statistics;
	function sms_statistics(){	
			//$('.sms_today').empty();
			//$('.sms_sent').empty();
			$('.sms_today').load('<?=url::site('sms_monitor/sms_today_count/'.$this->admin->agency_id);?>');
			$('.sms_sent').load('<?=url::site('sms_monitor/sms_sent_count/'.$this->admin->agency_id);?>');	
	}
	
</script>
