<?php defined('SYSPATH') or die('No direct script access'); ?> 
	
	<div class="notice">
		<?php
			//determine what type of notice to display if at all
			$notice = $this->session->get_once('notice');
				if(!empty($notice)){ 
					if($notice['type'] == 'success'){?><div class="alert general success"><a class="close" data-dismiss="alert" href="#">&times;</a><?php echo $notice['message']; ?></div><?php }?>
				<?	if($notice['type'] == 'error'){?><div class="alert general error"><a class="close" data-dismiss="alert" href="#">&times;</a><?php echo $notice['message'];?></div><?php }?>
		<?}?>
	</div>
 
	<div class="row-fluid " >
	  
						
	  	<div class="span12 hero-unit prof-unit" >
				<ul class="nav nav-tabs">
					<li > <a href="<?=url::site('reminders/all')?>"><i class="icon-arrow-left"></i> <?=Kohana::lang('backend.back_reminders')?></a></li>
				</ul>
		
				<form action="<?=url::site('reminders/edit_reminder/'.$reminder->id)?>" method="POST" enctype="multipart/form-data" class="span8 offset2">
				<legend><?=Kohana::lang('backend.edit_reminder')?></legend>
						<div class="">
							<div class="span4"><?=Kohana::lang('backend.name')?><span class="red"> *</span>: </div>
							<div class="span8"><input type="text"  name="name" value="<?=$reminder->Name?>" required></div>
						</div>
						
						<div class="">
							<div class="span4"><?=Kohana::lang('backend.bus_number')?>: </div>
							<div class="span8">
							<select name="bus_number" id="inputType">
									<option value="<?=$reminder->BusNumber?>"><b><?=$reminder->BusNumber?></b></option>
									<option value=""> None</option>
									<?php foreach($all_buses as $bus){?>
										<option value="<?=$bus->bus_number?>"> <?=$bus->bus_number?></option>
										
									<?php }?>
							</select>
							</div>
						</div>
						
						
						<div class="">
							<div class="span4"><?=Kohana::lang('backend.due_date')?><span class="red"> *</span>: </div>
							<div class="span8"><div class="input-append date">
						<input type="text" name = 'due_date' class="span6 datepicker"  value="<?=date('d-m-Y')?>" id="dp1">
						<span class="add-on"><i class="icon-th"></i></span>
						</div></div>
						</div>
						
						<div class="">
							<div class="span4"><?=Kohana::lang('backend.start_reminder')?>: </div>
							<div class="span8"><input type="text"  name="days_to" value="<?=$reminder->DaysTo?>" class="span4"> days before.</div>
						</div>
						
						
						
						
						<div class="">
							<div class="span4"><?=Kohana::lang('backend.other_information')?>: </div>
							<div class="span8"><textarea  name="description" style="height:100px;"><?=$reminder->Description?></textarea></div>
						</div>
						
						<div class="">
							<div class="span4"><?=Kohana::lang('backend.only_me')?>: </div>
							<div class="span8">
								<input type="hidden" name="is_personal" value="0" />
								<label class="checkbox"><input type="checkbox" name="is_personal" value="1" <?php if($reminder->IsPersonal == 1){echo 'checked = "checked"';}?>/></label>
								
							</div>
						</div>
						
										
						<div class="clear"></div>
						<div class="clear"><br/><br/></div>
						
						<div class="form-actions">
							<div class="span4">&nbsp; </div>
							<div class="span8">
								<button class="btn btn-info " type="submit"><?=Kohana::lang('backend.register')?></button>
								<a href="<?=url::site('reminders/all')?>" class="btn" type="submit"> <?=Kohana::lang('backend.cancel')?></a>
							</div>
							
						</div>
									
				
						
				</form>
			 
			
				
			  					
			
		
			  
		</div>
		
		</div>
	  <div class="clear"></div>

<script type="text/javascript">
	$(document).ready(function(){
			$('#timepicker').timepicker({
				minuteStep:30,
				//defaultTime: '8:00 PM'
			});
			
			
			$('.input-append.date').datepicker({
				format: "dd-mm-yyyy",
				todayBtn: "linked",
				autoclose: true,
				todayHighlight: true
			});
			
			
				$(function () {
				$('#myTab a').click(function (e) {
					  e.preventDefault();
					  $(this).tab('show');  
			})
			  })
			  
		
			  
			
	})
	</script>
