<?php  defined('SYSPATH') or die('No direct script access');

$lang=array(
	'client' => array('required' => 'Please enter the name of the seat occupant'),
	'client_name' => array('required' => 'Please enter the name of the seat occupant'),
	'bus_number' => array('required' => 'Please enter the number of the bus','default'=>'Please enter a valid bus number, without spaces'),
	'ticket_price' => array('required' => 'Please enter the price for each ticket'),
	'res_name' => array('required' => 'Please enter the name of the person who will occupy the seat'),
	'res_phone' => array('required' => 'Please enter the phone number of the person who will occupy the seat'),
	'client' => array('required' => 'Please enter the name of the seat occupant'),
	'receiver_name' => array('required' => 'Please enter the name of the recipient for the parcel'),
	'receiver_phone' => array('required' => 'Please enter the phone number of the recipient for the parcel','default'=>'Invalid phone number. Try again', 'length' => 'Invalid phone number. Verify that it is 9 digits',),
	'parcel_description' => array('required' => 'Please enter a description for the parcel'),
	'price' => array('required' => 'Please enter a price for the parcel','default'=>'Please enter a valid price without commas or spaces'),
	'ticket_price' => array('required' => 'Please enter a price for the ticket','default'=>'Please enter a valid ticket price'),
	'phone' => array('required' => 'Please enter a phone number','default'=>'Please enter a valid phone number'),
	'purpose' => array('required' => 'Please enter the purpose of the expense'),
	'exp_purpose' => array('required' => 'Please enter the purpose of the expense'),
	'amount' => array('required' => 'Please enter the amount spent', 'default'=>'Please enter a valid amount without commas and spaces'),
	'exp_amount' => array('required' => 'Please enter the amount spent','default'=>'Please enter a valid amount for the expense without commas and spaces'),
	'authorised_by' => array('required' => 'Please enter the authoriser of the expense'),
	'departure_date' => array('required' => 'Please enter the departure date'),
	'departure_time' => array('required' => 'Please enter the departure time'),
	'unregistered_bus_number' => array('required' => 'Please enter the number for the unregistered bus'),
	'bus_number_and_seats' => array('required' => 'No free bus. Checkout current schedules to have some free buses.'),
	
	'client_phone' => array('default' => 'Please enter a valid phone number.'),
	'client_email' => array('default' => 'Please enter a valid email address.'),
	'client_idc' => array('default' => 'Please enter a valid ID card number.'),
	'special_drop_price' => array('required' => 'Please enter the price for the special drop seat.','default' => 'Invalid price for the special drop.'),
	
	'res_phone' => array('default' => 'Please enter a valid phone number.'),
	'res_email' => array('default' => 'Please enter a valid email address.'),
	'res_idc' => array('default' => 'Please enter a valid ID card number.'),
    'username'=>array('required'=>'Your username  is required'),
    'password'=>array('required'=>'Your password is required', 'length'=>'Your password must be between 6 to 30 characters','matches'=>' Passwords don\'t match'),
	'name'=>array('required'=>'Your name is required'),
    'email'=>array('required'=>'Your email is required','default'=>'The email field must contain a valid email'),
	

    )
?>
