<?php defined('SYSPATH') or die('No direct script access'); ?> 

		<?php
		//set a variable depending on whether the parcel has been delivered or not
		//also check if the current admin is the sender. if so, disable the form , as he's not the recieving agency, hence shouldn't be allowed to fill
		//if it has, disable all input fields through javascript
		
		
		if($parcel->state == "delivered" || $parcel->from ==$this->admin->agency_id){
			$is_delivered = true;
		}else{ $is_delivered = false;}
				
		?>
	  	
	<div class="row-fluid">	
		
		<div class="span12 hero-unit prof-unit">
		
		<ul class="nav nav-tabs">
					<li > <a href="<?=url::site('parcels/all/incoming')?>"><i class="icon-arrow-left"></i> <?=Kohana::lang('backend.back_parcels')?></a></li>
		</ul>
		
		<div class="row-fluid">	
		
			<div class="span6 hero-unit prof-unit">
				
				
				<form action="<?=url::site('parcels/print_ticket')?>" method="POST">	
				<div class="heading"><?=Kohana::lang('backend.parcel_details')?>:
					<?php
					//if it's incoming, show the global parcel id. else compose it
					//if(isset($parcel->global_parcel_id)){ 
						echo "PC".$parcel->global_parcel_id;
					//}
					//else{ echo "PC".$this->agency_id.$parcel->id;}
				
					?>
					
				
				</form>
				</div>
				
				<div class="span12">
								<div class="span4 text-right"><?=Kohana::lang('backend.status')?>: </div>
								<div class="span8"><b>
									<?php 
										if ($parcel->state == 'received') {echo Kohana::lang('backend.received');}
										elseif ($parcel->state == 'delivered') {echo Kohana::lang('backend.rec_del');}
										else {echo Kohana::lang('backend.incoming');}
									?></b>
								</div>
				</div>
				<div class="span12">
								<div class="span4 text-right"><?=Kohana::lang('backend.sent_from')?>: </div>
								<div class="span8"><?=get::agency_name($parcel->from);?></div>
				</div>
				<div class=" span12">
								<div class="span4 text-right"><?=Kohana::lang('backend.sent_on')?>: </div>
								<div class="span8"><?=date("d-m-Y",strtotime($parcel->sent_date))?></div>
				</div>
				<div class="span12">
								<div class="span4 text-right"><?=Kohana::lang('backend.sent_to')?>: </div>
								<div class="span8"><?=get::agency_name($parcel->to);?></div>
				</div>
				
				<div class="span12">
								<div class="span4 text-right"><?=Kohana::lang('backend.receiver')?>: </div>
								<div class="span8"><?=$parcel->receiver_name;?></div>
				</div>
				
				<div class="span12">
								<div class="span4 text-right"><?=Kohana::lang('backend.receiver_phone')?>: </div>
								<div class="span8"><?=$parcel->receiver_phone;?></div>
				</div>
				<div class="span12">
								<div class="span4 text-right">Description: </div>
								<div class="span8"><?=$parcel->description;?></div>
				</div>
				<div class="span12">
								<div class="span4 text-right">Price: </div>
								<div class="span8"><?=number_format($parcel->price);?> frs</div>
				</div>
				<!--
				<div class="clear rule"><hr/></div>
				<div class="span12">
								<div class="span4 text-right">Created by: </div>
								<div class="span8"><i><?php //$parcel->CreatedBy;?></i></div>
				</div>-->
				<div class="clear"></div>
						<div class="form-actions">
							<div class="span4"></div>
							<div class="span8">
								
								<?php		
								//if the state is sent, and destination is the admin's agency
									
								if ($parcel->state == 'received') { ?>
								<span class=""> <a href="#" class="btn btn-info disabled" title="Parcel has been received at your branch." ><?=Kohana::lang('backend.received')?></a></span>
								<?php } 
								//Received and delivered
								elseif ($parcel->state == 'delivered') { ?>
								<span class=""><a href="#" class="btn btn-info disabled"><?=Kohana::lang('backend.rec_del')?></a></span>
								<?php }	
								//All incoming
								elseif($parcel->to == $this->admin->agency_id){?>
								<span class=""> <a href="<?=url::site('parcels/receive/'.$parcel->id)?>" class="btn btn-info" title="Has this parcel been delivered to your agency?"><?=Kohana::lang('backend.receive')?></a></span>
								<?php } ?>
								
								<a href="<?=url::site('parcels/all/incoming')?>" class="btn" type="submit"> <?=Kohana::lang('backend.cancel')?></a>
							</div>
				</div>
				
			
								
			
			
			
			
			</div>
		
		
			<!--
			<div class="span6">
			
			<div class="heading"><?php if(!$is_delivered){ echo "Who is collecting this parcel?";}else{echo Kohana::lang('backend.coll_by');}?></div>
				<?php 
					$collected_by = json_decode($parcel->collected_by);
				?>
				<form action="<?=url::site('parcels/deliver/'.$parcel->id)?>" method="POST" >
								<div class="clear"></div>

					<div class="simple-form span12">
								<div class="sf_label"><?=Kohana::lang('backend.name')?>: </div>
								<div class="sf_input"><input type="text"  name="collector_name" <?php if(1){echo "value='". @$collected_by->name."' disabled";}?>></div>
					</div>
					<div class="simple-form span12">
								<div class="sf_label"><?=Kohana::lang('backend.idc')?>: </div>
								<input type="text"  name="collector_idc" <?php if(1){echo "value='". @$collected_by->idc. "' disabled";}?>>
					</div>				
					<div class="simple-form span12">
								<div class="sf_label"><?=Kohana::lang('backend.phone')?>: </div>
								<div class="sf_input"><input type="text"  name="collector_phone" <?php if(1){echo "value='".@$collected_by->phone. "' disabled";}?>></div>
					</div>
					
					<?php
					//only show deliver button if it's not delivered
						if(!$is_delivered){
					?>	
							
					<?php
						}else{
					?>
					
						<div class="simple-form span12">
								<div class="sf_label"><?=Kohana::lang('backend.collected_on')?>: </div>
								<div class="sf_input"><input type="text"  name="collector_phone" <?php if($is_delivered){echo "value='". @$parcel->collected_on."' disabled";}?>></div>
						</div>

					<?php } ?>
					
				</form>
			</div>	-->
			
			<?php
				//Only show for received or delivered parcels. don't show for incoming
				if($parcel->state == "received" OR $parcel->state == "delivered" ){
			?>
			<div class="span6">
			
				<div class="heading"><?php if(!$is_delivered){ echo "Who is collecting this parcel?";}else{echo Kohana::lang('backend.coll_by');}?></div>
				<?php 
					$collected_by = json_decode($parcel->collected_by);
				?>
				<form action="<?=url::site('parcels/deliver/'.$parcel->id)?>" method="POST" >
								<div class="clear"></div>

					<div class="simple-form span12">
								<div class="sf_label"><?=Kohana::lang('backend.coll_name')?>: </div>
								<div class="sf_input"><input type="text" required  name="collector_name" <?php if($is_delivered){echo "value='". @$collected_by->name."' disabled";}?>></div>
					</div>
					<div class="simple-form span12">
								<div class="sf_label"><?=Kohana::lang('backend.coll_id')?>: </div>
								<input type="text"  name="collector_idc" <?php if($is_delivered){echo "value='". @$collected_by->idc. "' disabled";}?>>
					</div>				
					<div class="simple-form span12">
								<div class="sf_label"><?=Kohana::lang('backend.coll_phone')?>: </div>
								<div class="sf_input"><input type="text"  name="collector_phone" <?php if($is_delivered){echo "value='".@$collected_by->phone. "' disabled";}?>></div>
					</div>
					
					<?php
					//only show deliver button if it's not delivered
						if(!$is_delivered){
					?>	
							<div class="simple-form span12">
										<div class="sf_label">&nbsp;</div>
										<div class="sf_input"><button class="btn create-btn btn-success" type="submit" ><i class="icon-ok icon-white"></i> <?=Kohana::lang('backend.deliver')?></button></div>
							</div>
					<?php
						}else{
					?>
					
						<div class="simple-form span12">
								<div class="sf_label"><?=Kohana::lang('backend.collected_on')?>: </div>
								<div class="sf_input"><input type="text"  name="collector_phone" <?php if($is_delivered){echo "value='". date("d-m-Y  g:i A",strtotime(@$parcel->collected_on))."' disabled";}?>></div>
						</div>

					<?php } ?>
					
				</form>
			</div>	
			<?php } ?>
			
				
				
		</div>
	
		
		</div>
	
    </div> 

	
	<script type="text/javascript">
	
	$(document).ready(function(){
		var is_delivered = '<?=$is_delivered?>';
		$('.sf_input').prop('disabled',true);
	
	})
	
	</script>