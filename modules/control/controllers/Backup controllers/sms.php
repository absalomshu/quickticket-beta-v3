<?php defined('SYSPATH') or die('No direct script access');

//set default timezone, otherwise it's UTC which is an hour late
date_default_timezone_set('Africa/Douala');

class SMS_Controller extends Admin_Controller {

	public function index()
	{	
	
		//ensure a logged in user can't land on the signup page
		if (empty($this->admin)) 
		{	
			url::redirect('admin/login');
		}
		else
		 $this->main();
	}
	

	public function main() 
	{	
		//var_dump($_SESSION);EXIT;
		$permitted_levels = array(3);
		Authlite::check_access($permitted_levels);
		$user_group = $this->admin->admin_group_id;
		$agency_id = $this->agency_id;
		Authlite::check_admin();
		Authlite::verify_referer();		
		
		$this->template->title = Kohana::lang('backend.sms_management');
		$view=new View ('sms_main');
		
		$towns = get::all_towns();
		$admin_town =get::admins_town($this->admin->agency_id);
		$parent_id = get::_parent($this->admin->agency_id)->id;
		$all_children = get::all_children($parent_id);
		//$current_schedules = get::all_parent_current_schedules($parent_id);
		//$departed_schedules = get::all_parent_departed_schedules($parent_id);


		//print_r($parent_id);exit;
		//now, if it's a branch manager, limit his query to his town
		//show all only for general manager
		if ($user_group == 3){
			$current_schedules=get::ten_agency_current_schedules($agency_id);
			$departed_schedules=get::ten_agency_departed_schedules($agency_id);
			}
		if ($user_group == 4){
			//enable the GM to surf various agencies by setting a session variable that determines which particular branch to process
			//if 0, then he's viewing the whole company.
			if($current_branch == 0){
			$current_schedules=get::all_parent_current_schedules($parent_id);
			$departed_schedules=get::all_parent_departed_schedules($parent_id);
			}else
			{	//die('not parent');
				$current_schedules=get::ten_agency_current_schedules($current_branch);
				$departed_schedules=get::ten_agency_departed_schedules($current_branch);
			}
			
			}
		$date=date('d-m-Y');
		
		//var_dump($departed_schedules);	exit;
		
		$view->current_schedules=$current_schedules;
		$view->departed_schedules=$departed_schedules;
		//set current branch in session as it's needed in the submenu, which is not part of the view.
	
		//Needs to be global here, else these variables will not be available in the sub-menu
		$view->set_global('children',$all_children);
		//$view->set_global('current_branch',$current_branch);
		$view->towns=$towns;
		$view->user_group=$user_group;
		//$this->template->children=$all_children;
		$this->template->content=$view;
		
	}
	
		
	/*
	//$type: either parent or agency. Helps distinguish which the BM or GM sees.
	public function schedule_by_date()
	{	
		Authlite::check_admin();
		$agency_id = $this->agency_id;
		$parent_id = get::_parent($agency_id)->id;
		$type = $this->admin->admin_group_id;
		//Authlite::check_agency_integrity($agency_id);
		$this->template->title = Kohana::lang('backend.welcome_backend');
		if($_POST)
		{
			$post = new Validation($_POST);
			$post->add_rules('date','required');
			if ($post->validate())
			{	
				
			$date = date("Y-m-d",strtotime($_POST['date']));
			//$schedules = ORM::FACTORY('schedule')->where('agency_id',$agency_id)->where('departure_date',$date)->find_all();
			
			//if it's a GM, type 2
			if ($type==4){
			$_30seats = ORM::FACTORY('schedule')->join('agencies','agencies.id','schedules.agency_id')->where('parent_id',$parent_id)->where('departure_date',$date)->where('status','departed')->where('bus_seats',30)->count_all();
			$_35seats = ORM::FACTORY('schedule')->join('agencies','agencies.id','schedules.agency_id')->where('parent_id',$parent_id)->where('departure_date',$date)->where('status','departed')->where('bus_seats',35)->count_all();
			$_39seats = ORM::FACTORY('schedule')->join('agencies','agencies.id','schedules.agency_id')->where('parent_id',$parent_id)->where('departure_date',$date)->where('status','departed')->where('bus_seats',39)->count_all();
			$_55seats = ORM::FACTORY('schedule')->join('agencies','agencies.id','schedules.agency_id')->where('parent_id',$parent_id)->where('departure_date',$date)->where('status','departed')->where('bus_seats',55)->count_all();
			$_70seats = ORM::FACTORY('schedule')->join('agencies','agencies.id','schedules.agency_id')->where('parent_id',$parent_id)->where('departure_date',$date)->where('status','departed')->where('bus_seats',70)->count_all();
			
			//print_r("HAPPY");exit;
			$departed_schedules = get::all_parent_departed_schedules_by_date($parent_id,$date);				
			$current_schedules = get::all_parent_current_schedules_by_date($parent_id,$date); 
			$parcels=get::all_parent_parcels_by_date($parent_id,$date);
			}
			
			else{
			$_30seats = ORM::FACTORY('schedule')->where('agency_id',$agency_id)->where('departure_date',$date)->where('status','departed')->where('bus_seats',30)->count_all();
			$_35seats = ORM::FACTORY('schedule')->where('agency_id',$agency_id)->where('departure_date',$date)->where('status','departed')->where('bus_seats',35)->count_all();
			$_39seats = ORM::FACTORY('schedule')->where('agency_id',$agency_id)->where('departure_date',$date)->where('status','departed')->where('bus_seats',39)->count_all();
			$_55seats = ORM::FACTORY('schedule')->where('agency_id',$agency_id)->where('departure_date',$date)->where('status','departed')->where('bus_seats',55)->count_all();
			$_70seats = ORM::FACTORY('schedule')->where('agency_id',$agency_id)->where('departure_date',$date)->where('status','departed')->where('bus_seats',70)->count_all();
						
			$departed_schedules = get::all_agency_departed_schedules_by_date($agency_id,$date);				
			$current_schedules = get::all_agency_current_schedules_by_date($agency_id,$date);
			$parcels=get::all_agency_parcels_by_date($agency_id,$date);
			}
			
			

			//Calculate total amount expected from departed buses.
			$total = 0;
			foreach($departed_schedules as $ds){
				$total += $ds->total_amount;
			}
			}
			else
			{
				$errors=$post->errors('errors');
				$notice="";
					foreach($errors as $error) 
					{
						$notice.=$error."<br />";
					}
					$this->session->set('notice', array('message'=>$notice,'type'=>'error'));
					url::redirect('admin/main/'.$this->admin->agency_id);
			}
	}
	$view = new View('admin_schedule_by_date');
	$view->departed_schedules = $departed_schedules;
	$view->current_schedules = $current_schedules;
	$view->parcels=$parcels;
	$view->_30seats = $_30seats;
	$view->_35seats = $_35seats;
	$view->_39seats = $_39seats;
	$view->_55seats = $_55seats;
	$view->_70seats = $_70seats;
	$view->total = $total;
	$view->date = $date;
		
	$this->template->content = $view;
	
	}	
	
	//type is either departed or current
	public function all_schedules($type)
	{	
		Authlite::check_admin();
		$agency_id = $this->agency_id;
		
		//pagination
		$per_page = 10;
		if ($type == 'current'){
			$all= get::all_agency_current_schedules($agency_id);}else{
			$all= get::all_agency_departed_schedules($agency_id);}
		$total = count($all);
		$this->pagination = new Pagination(array(
			// 'base_url'    => 'welcome/pagination_example/page/', // base_url will default to current uri
			'uri_segment'    => 'page', // pass a string as uri_segment to trigger former 'label' functionality
			'total_items'    => $total, // use db count query here of course
			'items_per_page' => $per_page , // it may be handy to set defaults for stuff like this in config/pagination.php
			'style'          => 'punbb', // pick one from: classic (default), digg, extended, punbb, or add your own!
			//'query_string'   => "ORM::factory('schedule')->where('agency_id',$agency_id)->where('status','current')->find_all()"
));
		if ($type == 'current'){
			//$schedules = get::all_agency_current_schedules($agency_id);
			$schedules=ORM::factory('schedule')->where('agency_id',$agency_id)->where('status','current')->limit($per_page,$this->pagination->sql_offset)->find_all();			
		}else{
			//$schedules = get::all_agency_departed_schedules($agency_id);
			$schedules=ORM::factory('schedule')->where('agency_id',$agency_id)->where('status','departed')->limit($per_page,$this->pagination->sql_offset)->find_all();			
		}
		
		$this->template->title = 'Viewing all schedules';		
		$view = new View('control_all_schedules');
		$view->schedules = $schedules;
		$this->template->content = $view;
	
	} */
	
		
		
		}