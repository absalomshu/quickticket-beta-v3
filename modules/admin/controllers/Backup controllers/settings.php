<?php defined('SYSPATH') or die('No direct script access');

//Almost all functions will take as parameter, the id of the agency we are dealing with
//in order not to interfere with another's stuff.

class Settings_Controller extends Admin_Controller {
		
	protected $logo_directory = 'images/ticket/agency-logos/';
	protected $ticket_directory = 'images/ticket/';
	
	
	public function __construct()
	{	
		parent::__construct();
		$this->session = Session::instance();
		
	}  
	
	
	public function index()
	{	
		Authlite::check_admin();
		Authlite::verify_referer();
		
		$this->template->title = Kohana::lang('backend.settings');
		$settings = ORM::FACTORY('setting')->where('id',1)->find();
		$this->template->title='Edit company details';
		$agency=get::agency($this->agency_id);
		$logo=get::agency_logo($this->agency_id);
		$ticket_background=get::agency_ticket_background($this->agency_id);
		$admins=get::agency_admins($this->agency_id);
		
			
		if($_POST)
		{
		
			if(isset($_POST['general']))
			{
				//unset the 'general' which is part of the post, if not the for each will also search for a 'general' column in the db table
				unset($_POST['general']);
					foreach($_POST as $key => $value)
					{
						$settings->$key = $value;
						$settings->save();
					
					}
					$this->session->set('notice', array('message'=>Kohana::lang('backend.preferences_saved'),'type'=>'success'));
			}	
				
			if(isset($_POST['company_info']))
			{

				$post = new Validation($_POST);
				//$post->add_rules('company_name','required');
				
				if ($post->validate())
				{
					$contacts = $_POST['contacts'];
					$ticket_info = $_POST['ticket_info'];
					
					$agency->contacts = $contacts;
					$agency->TicketInfo = $ticket_info;
					$agency->save();	
					$this->session->set('notice', array('message'=>'agency information modified','type'=>'success'));
				}
				else
				{
					$errors=$post->errors('errors');
					$notice="";
						foreach($errors as $error) 
						{
							$notice.=$error."<br />";
						}
						$this->session->set('notice', array('message'=>$notice,'type'=>'error'));
				}
				
				
				//if a logo/picture is posted and it's not nameless
				if($_FILES['agency_logo']['error'] == 0 AND strlen($_FILES['agency_logo']['name'] )>0)
				{
		
					$files = new Validation($_FILES);
					$files->add_rules('agency_logo', 'upload::valid', 'upload::required', 'upload::type[gif,jpg,png]', 'upload::size[5M]');
						if ($files->validate())
							{	
								$logo = $_FILES['agency_logo'];
								
								// Temporary file name - the name of the company without whitespaces
								$filename = upload::save($logo, strtolower(str_replace(" ",'', $agency->name)));
								
								// Resize, sharpen, and save the image
								Image::factory($filename)
									->resize(100, 100, Image::WIDTH)
									->save($this->logo_directory.basename($filename).'.jpg');
									$agency->Logo = $this->logo_directory.basename($filename).'.jpg';
									$agency->save();
								// Remove the temporary file
								unlink($filename);
								$this->session->set('notice', array('message'=>'Photo uploaded successfully','type'=>'success'));
								//url::redirect('profiles/edit_company/'.$company_id);
							}
				}
				
				//if a logo/picture is posted and it's not nameless
				if($_FILES['new_ticket_photo']['error'] == 0 AND strlen($_FILES['new_ticket_photo']['name'] )>0)
				{
		
					$files = new Validation($_FILES);
					$files->add_rules('new_ticket_photo', 'upload::valid', 'upload::required', 'upload::type[gif,jpg,png]', 'upload::size[5M]');
						if ($files->validate())
							{	
								$ticket_background = $_FILES['new_ticket_photo'];
								
								// Temporary file name - the name of the company without whitespaces
								$filename = upload::save($ticket_background, strtolower(str_replace(" ",'', "ticket.".$agency->name)));
								
								// Resize, sharpen, and save the image
								Image::factory($filename)
									->resize(100, 100, Image::WIDTH)
									->save($this->ticket_directory.basename($filename).'.jpg');
									$agency->ticket_background = $this->ticket_directory.basename($filename).'.jpg';
									$agency->save();
								// Remove the temporary file
								unlink($filename);
								$this->session->set('notice', array('message'=>'Photo uploaded successfully','type'=>'success'));
								//url::redirect('profiles/edit_company/'.$company_id);
							}
				}
				
				url::redirect('settings');
			}	
		}
		
		$view = new View('settings');
		$view->settings = $settings;
		$view->admins = $admins;
		$view->agency=$agency;
		$view->logo = $logo;
		$view->ticket_background = $ticket_background;
		$this->template->content = $view;
	}
	
	
	function edit_company($company_id)
	{
		Authlite::check_admin();
		Authlite::verify_referer();
		
		//Authlite::check_integrity($schedule_id);
		$this->template->title='Edit company details';
		$agency=get::agency($this->agency_id);
		$logo=get::agency_logo($company_id);
		
		if($_POST)
		{
			$post = new Validation($_POST);
			//$post->add_rules('company_name','required');
			
			if ($post->validate())
			{
				$contacts = $_POST['contacts'];
				$ticket_info = $_POST['ticket_info'];
				
				$company->contacts = $contacts;
				$company->ticket_info = $ticket_info;
				$company->save();	
				$this->session->set('notice', array('message'=>'Company information modified','type'=>'success'));
			}
			else
			{
				$errors=$post->errors('errors');
				$notice="";
					foreach($errors as $error) 
					{
						$notice.=$error."<br />";
					}
					$this->session->set('notice', array('message'=>$notice,'type'=>'error'));
			}
			
			
			//if a logo/picture is posted and it's not nameless
			if($_FILES['agency_logo']['error'] == 0 AND strlen($_FILES['agency_logo']['name'] )>0)
			{
				
				
				//chmod($this->logo_directory, 0744);
				//var_dump(substr(sprintf('%o', fileperms($this->logo_directory)), -4));
					
					/*if(is_writable($this->logo_directory)){
						die('directory is writeable');
					}else{
						die('DIR NOT WRITEABEL');
					} */

				$files = new Validation($_FILES);
				$files->add_rules('agency_logo', 'upload::valid', 'upload::required', 'upload::type[gif,jpg,png]', 'upload::size[1M]');
					if ($files->validate())
						{	
							$logo = $_FILES['agency_logo'];
							
							// Temporary file name - the name of the company without whitespaces
							$filename = upload::save($logo, strtolower(str_replace(" ",'', $company->name)));
							
							// Resize, sharpen, and save the image
							Image::factory($filename)
								->resize(100, 100, Image::WIDTH)
								->save($this->logo_directory.basename($filename).'.jpg');
						 
							// Remove the temporary file
							unlink($filename);
							$this->session->set('notice', array('message'=>'Photo uploaded successfully','type'=>'success'));
							//url::redirect('profiles/edit_company/'.$company_id);
						}
			}
			url::redirect('profiles/edit_company/'.$company_id);
			}
		
			
			$view=new View('profiles_edit_company');
			$view->agency=$agency;
			$view->logo = $logo;
			$this->template->content=$view;
			
	} 
	
	public function add_admin()
	 {
			Authlite::check_admin();
			Authlite::verify_referer();
			
			//Authlite::check_integrity($schedule_id);
			$this->template->title='Add admin details';
			$admin=ORM::FACTORY('admin');
			$groups = get::all_admingroups();
			
		if($_POST){
			$post = new Validation($_POST);
			$post->add_rules('username','required','valid::alpha_numeric');
			$post->add_rules('password','required');
			
			if ($post->validate())
			{
				
				$username = $_POST['username'];
				$password = $_POST['password'];
				$AllowBusManagement = $_POST['AllowBusManagement'];
				$AllowParcelManagement = $_POST['AllowParcelManagement'];
				$AllowExpenseManagement = $_POST['AllowExpenseManagement'];
				$AllowScheduleManagement = $_POST['AllowScheduleManagement'];
			
				$user_exists = ORM::FACTORY('admin')->where('username',$username)->count_all();
				
				if ($user_exists){
					$this->session->set('notice', array('message'=>Kohana::lang('backend.username_taken'),'type'=>'error'));
					url::redirect('settings/add_admin');
				}else
				{
					//Make sure that at least one privilege is set for each created user
					if($AllowBusManagement==0 AND $AllowParcelManagement ==0 AND $AllowExpenseManagement==0 AND $AllowScheduleManagement==0)
					{
					$this->session->set('notice', array('message'=>'Admin has no privilege. Please select at least one.','type'=>'error'));	
					url::redirect('settings/add_admin');
					}else
					{
					// Admin group id is 2 by default. ie. BM CAN only create a regular admin
					$notice = Admin_Model::safe_add_admin($username, $password, $this->agency_id, $this->admin->username, $AllowParcelManagement,$AllowScheduleManagement,$AllowExpenseManagement, $AllowBusManagement);
					$this->session->set('notice', array('message'=>$notice,'type'=>'success'));			
					}
				}
					
					//$this->session->set('notice', array('message'=>'Admin information modified','type'=>'success'));
				}
				else
				{
					$errors=$post->errors('errors');
					$notice="";
						foreach($errors as $error) 
						{
							$notice.=$error."<br />";
						}
						$this->session->set('notice', array('message'=>$notice,'type'=>'error'));
				}
			
				url::redirect('settings');
			}
		
			
			$view=new View('control_add_admin');
			$view->admin=$admin;
			//$view->groups = $groups;
			//$view->logo = $logo;
			$this->template->content=$view;
			
	}
	
	function edit_admin($admin_id)
	 {
			Authlite::check_admin();
			Authlite::verify_referer();
			
			//Authlite::check_integrity($schedule_id);
			$this->template->title='Edit admin details';
			$admin=get::admin($admin_id);
			

		if($_POST)
		{
			
			
			if(isset($_POST['change_privileges']))
			{
			$post = new Validation($_POST);
			if ($post->validate())	
				{
					$AllowScheduleManagement = $_POST['AllowScheduleManagement'];
					$AllowBusManagement = $_POST['AllowBusManagement'];
					$AllowParcelManagement = $_POST['AllowParcelManagement'];
					$AllowExpenseManagement = $_POST['AllowExpenseManagement'];
					
					//Make sure that at least one privilege is set for each created user
						if($AllowBusManagement==0 AND $AllowParcelManagement ==0 AND $AllowExpenseManagement==0 AND $AllowScheduleManagement==0)
						{
						$this->session->set('notice', array('message'=>'Admin has no privilege. Please select at least one.','type'=>'error'));	
						url::redirect('settings/edit_admin/'.$admin_id);
						}else{
					
					$admin->AllowScheduleManagement = $AllowScheduleManagement;
					$admin->AllowBusManagement = $AllowBusManagement;
					$admin->AllowParcelManagement = $AllowParcelManagement;
					$admin->AllowExpenseManagement = $AllowExpenseManagement;
					$admin->save();	
					
					$this->session->set('notice', array('message'=>'Admin information modified','type'=>'success'));}
				}
				else
				{
					$errors=$post->errors('errors');
					$notice="";
						foreach($errors as $error) 
						{
							$notice.=$error."<br />";
						}
						$this->session->set('notice', array('message'=>$notice,'type'=>'error'));
				}		
			}
			if(isset($_POST['change_password']))
			{	 
				$post = new Validation($_POST);
				$post->add_rules('new_password','required');
				if ($post->validate())	
				{	
					$new_password = $_POST['new_password'];
					$notice = Admin_model::change_password($admin_id,$new_password);
					$this->session->set('notice', array('message'=>$notice,'type'=>'success'));		
				}else
				{
					$errors=$post->errors('errors');
					$notice="";
						foreach($errors as $error) 
						{
							$notice.=$error."<br />";
						}
						$this->session->set('notice', array('message'=>$notice,'type'=>'error'));
				}
			}
			
			url::redirect('settings');
			}	
			$view=new View('control_edit_admin');
			$view->admin=$admin;
			//$view->logo = $logo;
			$this->template->content=$view;
			
	}
	public function delete_admin($admin_id)
	 {		
			Authlite::check_admin();
			Authlite::verify_referer();
			
			$this->template->title="Delete admin";
			$deleted_by = $this->admin->username;
			$notice = Admin_Model::safe_delete_admin($admin_id,$deleted_by);		
			
			$this->session->set('notice', array('message'=>$notice,'type'=>'success'));
			url::redirect('settings');
			
			$this->template->content='';
	 }
	
		
		}