<?php defined('SYSPATH') or die('No direct script access');

class Expense_Model extends ORM{

	public function register_expense($agency_id, $bus_number, $amount, $purpose,$category_id, $date_incurred, $CreatedBy, $authorised_by, $schedule_id='', $is_cash_interbranch='', $cash_interbranch_agency_id_to='')
	{	//var_dump($cash_interbranch_agency_id_to);exit;
		$expense = ORM::FACTORY('expense');
		$expense->agency_id = $agency_id;
		$expense->bus_number = $bus_number;
		$expense->schedule_id = $schedule_id;
		$expense->amount = $amount;
		$expense->purpose = $purpose;
		$expense->category_id = $category_id;
		$expense->date_incurred = $date_incurred;
		$expense->CreatedBy = $CreatedBy;
		$expense->authorised_by = $authorised_by;
		$expense->is_cash_interbranch = $is_cash_interbranch;
		$expense->cash_interbranch_agency_id_to = $cash_interbranch_agency_id_to;
		$expense->save();
		
		//Update the admin's available balance
		$admin = ORM::FACTORY('admin')->where('username',$CreatedBy)->find();
		$admin->available_balance = $admin->available_balance - $amount;
		$admin->save();
		
	}
	
	public function edit_expense($agency_id, $expense_id, $bus_number, $amount, $purpose, $category_id, $date_incurred, $ModifiedBy, $authorised_by, $schedule_id='', $is_cash_interbranch='', $cash_interbranch_agency_id_to='')
	{	//var_dump($category_id);exit;
		$expense = ORM::FACTORY('expense')->where('agency_id',$agency_id)->where('id',$expense_id)->find();
		$old_amount = $expense->amount;
		$expense->agency_id = $agency_id;
		$expense->bus_number = $bus_number;
		$expense->schedule_id = $schedule_id;
		$expense->amount = $amount;
		$expense->purpose = $purpose;
		$expense->category_id = $category_id;
		$expense->date_incurred = $date_incurred;
		//$expense->ModifiedBy = $ModifiedBy;
		$expense->authorised_by = $authorised_by;
		$expense->is_cash_interbranch = $is_cash_interbranch;
		$expense->cash_interbranch_agency_id_to = $cash_interbranch_agency_id_to;
		$expense->save();
		
		//Update the admin's available balance
		$admin = ORM::FACTORY('admin')->where('username',$ModifiedBy)->find();
		$admin->available_balance = $admin->available_balance + $old_amount - $amount;
		$admin->save();
		
	}
	
	public function get($agency_id, $expense_id)
	{
		//$expense=ORM::factory('expense')->where('agency_id',$agency_id)->where('id',$expense_id)->find();
		$expense=$this->db->select('expenses.id AS id, expenses.agency_id, expenses.bus_number, expenses.amount, expenses.category_id, expenses.purpose, expenses.date_incurred, expenses.authorised_by,expenses.CreatedBy,expenses.CreatedOn, expense_categories.name AS category_name')->from('expenses')->join('expense_categories','expense_categories.id=expenses.category_id')->where('expenses.agency_id',$agency_id)->where('expenses.id',$expense_id)->get();
		return $expense;	
	}
	
	public function safe_delete_expense($agency_id, $expense_id)
	{

		$expense=ORM::factory('expense')->where('agency_id',$agency_id)->where('id',$expense_id)->find();
		$expense->deleted = 1;
		
		
		//Update the admin's available balance
		$admin = ORM::FACTORY('admin')->where('username',$expense->CreatedBy)->find();
		$admin->available_balance = $admin->available_balance + $expense->amount;
		$admin->save();
		
		$expense->save();
		
		$notice="Expense deleted";
		return $notice;
		
	}
	
	public function get_agency_expenses_by_period_by_admin($agency_id,$start_date,$end_date,$admin)
	{
		$db = new Database();
		$expenses = $db->query("SELECT * FROM expenses WHERE agency_id= ".$agency_id." AND CreatedBy= '".$admin."' AND deleted = '0' AND date_incurred BETWEEN CAST('$start_date' AS DATE) AND CAST('$end_date' AS DATE) ORDER BY date_incurred ASC");
		return $expenses;
	}
	
	public function get_agency_expenses_total_by_period_by_admin($agency_id,$start_date,$end_date,$admin)
	{
		$expenses=Expense_Model::get_agency_expenses_by_period_by_admin($agency_id,$start_date,$end_date,$admin);
		$expenses_total = 0;
		foreach($expenses as $expense){	
			$expenses_total += $expense->amount;
		}
		return $expenses_total;
	}
	
	public function get_agency_expenses_by_period_by_bus($agency_id,$start_date,$end_date,$bus_number)
	{
		$db = new Database();
		$expenses = $db->query("SELECT * FROM expenses WHERE agency_id= ".$agency_id." AND bus_number= '".$bus_number."' AND deleted = '0' AND date_incurred BETWEEN CAST('$start_date' AS DATE) AND CAST('$end_date' AS DATE) ORDER BY date_incurred ASC");
		return $expenses;
	}
	
	public function get_expenses_by_date($agency_id,$date){
		//$expenses = ORM::FACTORY('expense')->join('expense_categories','expense_categories.id','expenses.category_id')->where('expenses.agency_id',$agency_id)->where('expenses.date_incurred',$date)->where('expenses.deleted','0')->find_all();		
		$db = new Database();
		$expenses = $db->query("SELECT * FROM expenses JOIN expense_categories ON expense_categories.id = expenses.category_id WHERE expenses.agency_id= ".$agency_id." AND expenses.deleted = '0' AND date_incurred= '".$date."'");
		//var_dump($expenses);exit;
		return $expenses;
	}
	
	public function agency_expenses_by_period($agency_id,$start_date,$end_date)
	{
		$db = new Database();
		$expenses = $db->query("SELECT * FROM expenses JOIN expense_categories ON expense_categories.id = expenses.category_id WHERE expenses.agency_id= ".$agency_id." AND expenses.deleted = '0' AND date_incurred BETWEEN CAST('$start_date' AS DATE) AND CAST('$end_date' AS DATE) ORDER BY date_incurred ASC");
		return $expenses;
	}
	
}
