<?php defined('SYSPATH') or die('No direct script access');

//Almost all functions will take as parameter, the id of the agency we are dealing with
//in order not to interfere with another's stuff.

class Settings_Controller extends Admin_Controller {
		
	protected $logo_directory = 'images/ticket/agency-logos/';
	protected $ticket_directory = 'images/ticket/';
	
	
	public function __construct()
	{	
		parent::__construct();
		$this->session = Session::instance();
		
		//ensure that settings data exists for that agency if not create it
		$settings_exists = ORM::FACTORY('setting')->where('agency_id',$this->agency_id)->count_all();
		if(!$settings_exists){
			$settings = ORM::FACTORY('setting');
			$settings->agency_id=$this->agency_id;
			$settings->save();
		}
		
	}  
	
	
	
	public function index()
	{	
		Authlite::check_admin();
		Authlite::verify_referer();
		
		$this->template->title = Kohana::lang('backend.settings');
		
		$this->template->title='Edit company details';
		$agency=get::agency($this->agency_id);
		$logo=get::agency_logo($this->agency_id);
		$ticket_background=get::agency_ticket_background($this->agency_id);
		$admins=Admin_Model::get_admins_excluding_current($this->agency_id, $this->admin->username);
		$fixed_expenses=Fixed_Expense_Detail_Model::get_all($this->agency_id);
		$bus_requirements=Bus_Requirement_Model::get_all($this->agency_id);
		$dashboard_items = Dashboard_Item_Model::get_all();
		$open_tickets = Open_ticket_Model::get_all_for_agency($this->agency_id);
		$parent = get::_parent($this->agency_id);
		$siblings = get::agency_siblings($this->agency_id,$parent);	
		
		
		
		$settings = Setting_Model::get($this->agency_id);
			
		if($_POST)
		{
		
			if(isset($_POST['general']))
			{
				//unset the 'general' which is part of the post, if not the for each will also search for a 'general' column in the db table
				unset($_POST['general']);
					foreach($_POST as $key => $value)
					{
						$settings->$key = $value;
						$settings->agency_id = $this->agency_id;
						$settings->save();
					
					}
					$this->session->set('notice', array('message'=>Kohana::lang('backend.preferences_saved'),'type'=>'success'));
			}	
				
			if(isset($_POST['company_info']))
			{

				$post = new Validation($_POST);
				//$post->add_rules('company_name','required');
				
				if ($post->validate())
				{
					$contacts = $_POST['contacts'];
					$ticket_info = $_POST['ticket_info'];
					
					$agency->contacts = $contacts;
					$agency->TicketInfo = $ticket_info;
					$agency->save();	
					$this->session->set('notice', array('message'=>'agency information modified','type'=>'success'));
				}
				else
				{
					$errors=$post->errors('errors');
					$notice="";
						foreach($errors as $error) 
						{
							$notice.=$error."<br />";
						}
						$this->session->set('notice', array('message'=>$notice,'type'=>'error'));
				}
				
				
				//if a logo/picture is posted and it's not nameless
				if($_FILES['agency_logo']['error'] == 0 AND strlen($_FILES['agency_logo']['name'] )>0)
				{
		
					$files = new Validation($_FILES);
					$files->add_rules('agency_logo', 'upload::valid', 'upload::required', 'upload::type[gif,jpg,png]', 'upload::size[5M]');
						if ($files->validate())
							{	
								$logo = $_FILES['agency_logo'];
								
								// Temporary file name - the name of the company without whitespaces
								$filename = upload::save($logo, strtolower(str_replace(" ",'', $agency->name)));
								
								// Resize, sharpen, and save the image
								Image::factory($filename)
									->resize(100, 100, Image::WIDTH)
									->save($this->logo_directory.basename($filename).'.jpg');
									$agency->Logo = $this->logo_directory.basename($filename).'.jpg';
									$agency->save();
								// Remove the temporary file
								unlink($filename);
								$this->session->set('notice', array('message'=>'Photo uploaded successfully','type'=>'success'));
								//url::redirect('profiles/edit_company/'.$company_id);
							}
				}
				
				//if a logo/picture is posted and it's not nameless
				if($_FILES['new_ticket_photo']['error'] == 0 AND strlen($_FILES['new_ticket_photo']['name'] )>0)
				{
		
					$files = new Validation($_FILES);
					$files->add_rules('new_ticket_photo', 'upload::valid', 'upload::required', 'upload::type[gif,jpg,png]', 'upload::size[5M]');
						if ($files->validate())
							{	
								$ticket_background = $_FILES['new_ticket_photo'];
								
								// Temporary file name - the name of the company without whitespaces
								$filename = upload::save($ticket_background, strtolower(str_replace(" ",'', "ticket.".$agency->name)));
								
								// Resize, sharpen, and save the image
								Image::factory($filename)
									->resize(100, 100, Image::WIDTH)
									->save($this->ticket_directory.basename($filename).'.jpg');
									$agency->ticket_background = $this->ticket_directory.basename($filename).'.jpg';
									$agency->save();
								// Remove the temporary file
								unlink($filename);
								$this->session->set('notice', array('message'=>'Photo uploaded successfully','type'=>'success'));
								//url::redirect('profiles/edit_company/'.$company_id);
							}
				}
				
				url::redirect('settings/company');
			}	
		}
		
		$view = new View('settings');
		$view->settings = $settings;
		$view->siblings = $siblings;
		$view->admins = $admins;
		$view->agency=$agency;
		$view->logo = $logo;
		$view->ticket_background = $ticket_background;
		$view->fixed_expenses = $fixed_expenses;
		$view->bus_requirements = $bus_requirements;
		$view->open_tickets = $open_tickets;
		//$view->towns =  Town_Model::get_all_excluding_town($this->agency_town_id);;
		$view->towns =  Town_Model::get_all();
		
		$this->template->content = $view;
	}
	
	public function general()
	{	
		Authlite::check_admin();
		Authlite::verify_referer();
		
		$this->template->title = Kohana::lang('backend.settings');
		
		$this->template->title='Edit company details';
		$agency=get::agency($this->agency_id);
		$settings = Setting_Model::get($this->agency_id);
		//$logo=get::agency_logo($this->agency_id);
		//$ticket_background=get::agency_ticket_background($this->agency_id);
		//$admins=Admin_Model::get_admins_excluding_current($this->agency_id, $this->admin->username);
		//$fixed_expenses=Fixed_Expense_Detail_Model::get_all($this->agency_id);
		//$bus_requirements=Bus_Requirement_Model::get_all($this->agency_id);
		//$dashboard_items = Dashboard_Item_Model::get_all();
		//$open_tickets = Open_ticket_Model::get_all_for_agency($this->agency_id);
		//$parent = get::_parent($this->agency_id);
		//$siblings = get::agency_siblings($this->agency_id,$parent);	
		
		if($_POST)
		{
		
			if(isset($_POST['general']))
			{
				//unset the 'general' which is part of the post, if not the for each will also search for a 'general' column in the db table
				unset($_POST['general']);
					foreach($_POST as $key => $value)
					{
						$settings->$key = $value;
						$settings->agency_id = $this->agency_id;
						$settings->save();
					
					}
					$this->session->set('notice', array('message'=>Kohana::lang('backend.preferences_saved'),'type'=>'success'));
					url::redirect('settings/general');
					die('here');
			}	
				
				
		}
		
		$view = new View('settings');
		$view->settings = $settings;
		//$view->siblings = $siblings;
		//$view->admins = $admins;
		$view->agency=$agency;
		//$view->logo = $logo;
		//$view->ticket_background = $ticket_background;
		//$view->fixed_expenses = $fixed_expenses;
		//$view->bus_requirements = $bus_requirements;
		//$view->open_tickets = $open_tickets;
		//$view->towns =  Town_Model::get_all_excluding_town($this->agency_town_id);;
		//$view->towns =  Town_Model::get_all();
		
		$this->template->content = $view;
	}
	
	public function administrators()
	{	
		Authlite::check_admin();
		Authlite::verify_referer();
		
		$this->template->title = Kohana::lang('backend.settings');
		$this->template->title='Edit company details';
		
		$agency=get::agency($this->agency_id);
		$settings = Setting_Model::get($this->agency_id);
		$admins=Admin_Model::get_admins_excluding_current($this->agency_id, $this->admin->username);
		
		$view = new View('settings');
		$view->settings = $settings;
		$view->admins = $admins;
		$view->agency=$agency;
		$this->template->content = $view;
	}
	
	public function expenses()
	{	
		Authlite::check_admin();
		Authlite::verify_referer();
		
		$this->template->title = Kohana::lang('backend.settings');
		$this->template->title='Edit company details';
		
		
		$agency=get::agency($this->agency_id);
		$settings = Setting_Model::get($this->agency_id);
		$expense_categories = Expense_category_Model::get_all($this->agency_id);
		//Check, if there no category, create a default 'general' category
		//if(count($expense_categories)==0){
			
			$default_category = Expense_category_Model::get($this->agency_id, '1');
			$default_category_exists= ORM::FACTORY('expense_category')->where('agency_id',$this->agency_id)->where('id','1')->count_all();
			
			//If default category (with id 1 doesn't exist) add it
			if(!$default_category_exists){
				Expense_category_Model::add_default_category($this->agency_id);
				//Reload so the added category is displayed instantly, and not on next page load.
				url::redirect('settings/expenses');
			}
			//if somehow it exists but is not the general category, modify it.
			//if($default_category->name != 'General'){
				//die($default_category->name );
				//Expense_category_Model::reset_default_category($this->agency_id);
			//}
					
			
		//}
		
		$view = new View('settings');
		$view->settings = $settings;
		$view->expense_categories = $expense_categories;
		$view->agency=$agency;
		$this->template->content = $view;
	}
	
	public function advanced()
	{	
		Authlite::check_admin();
		Authlite::verify_referer();
		
		$this->template->title = Kohana::lang('backend.settings');
		
		$this->template->title='Edit company details';
		$agency=get::agency($this->agency_id);
		$settings = Setting_Model::get($this->agency_id);
		//$logo=get::agency_logo($this->agency_id);
		//$ticket_background=get::agency_ticket_background($this->agency_id);
		$admins=Admin_Model::get_admins_excluding_current($this->agency_id, $this->admin->username);
		$fixed_expenses=Fixed_Expense_Detail_Model::get_all($this->agency_id);
		$bus_requirements=Bus_Requirement_Model::get_all($this->agency_id);
		//$dashboard_items = Dashboard_Item_Model::get_all();
		//$open_tickets = Open_ticket_Model::get_all_for_agency($this->agency_id);
		//$parent = get::_parent($this->agency_id);
		//$siblings = get::agency_siblings($this->agency_id,$parent);	
				
		$view = new View('settings');
		$view->settings = $settings;
		//$view->siblings = $siblings;
		$view->admins = $admins;
		$view->agency=$agency;
		//$view->logo = $logo;
		//$view->ticket_background = $ticket_background;
		$view->fixed_expenses = $fixed_expenses;
		$view->bus_requirements = $bus_requirements;
		//$view->open_tickets = $open_tickets;
		//$view->towns =  Town_Model::get_all_excluding_town($this->agency_town_id);;
		//$view->towns =  Town_Model::get_all();
		
		$this->template->content = $view;
	}
	
	public function cash()
	{	
		Authlite::check_admin();
		Authlite::verify_referer();
		
		$this->template->title = Kohana::lang('backend.settings');
		
		$this->template->title='Edit company details';
		$agency=get::agency($this->agency_id);
		$settings = Setting_Model::get($this->agency_id);
		//$logo=get::agency_logo($this->agency_id);
		//$ticket_background=get::agency_ticket_background($this->agency_id);
		$admins=Admin_Model::get_admins_excluding_current($this->agency_id, $this->admin->username);
		$fixed_expenses=Fixed_Expense_Detail_Model::get_all($this->agency_id);
		$bus_requirements=Bus_Requirement_Model::get_all($this->agency_id);
		//$dashboard_items = Dashboard_Item_Model::get_all();
		//$open_tickets = Open_ticket_Model::get_all_for_agency($this->agency_id);
		$parent = get::_parent($this->agency_id);
		$siblings = get::agency_siblings($this->agency_id,$parent);	
				
		$view = new View('settings');
		$view->settings = $settings;
		$view->siblings = $siblings;
		$view->admins = $admins;
		$view->agency=$agency;
		$view->parent = $parent;
		//$view->ticket_background = $ticket_background;
		$view->fixed_expenses = $fixed_expenses;
		$view->bus_requirements = $bus_requirements;
		//$view->open_tickets = $open_tickets;
		//$view->towns =  Town_Model::get_all_excluding_town($this->agency_town_id);;
		//$view->towns =  Town_Model::get_all();
		
		$this->template->content = $view;
	}	
	
	public function company()
	{	
		Authlite::check_admin();
		//Authlite::verify_referer();
		
		$this->template->title = Kohana::lang('backend.settings');
		
		$this->template->title='Edit company details';
		$agency=get::agency($this->agency_id);
		$logo=get::agency_logo($this->agency_id);
		$ticket_background=get::agency_ticket_background($this->agency_id);
	
		$settings = Setting_Model::get($this->agency_id);
			
		if($_POST)
		{
				
			if(isset($_POST['company_info']))
			{

				$post = new Validation($_POST);
				//$post->add_rules('company_name','required');
				
				if ($post->validate())
				{
					$contacts = $_POST['contacts'];
					$ticket_info = $_POST['ticket_info'];
					
					$agency->contacts = $contacts;
					$agency->TicketInfo = $ticket_info;
					$agency->save();	
					$this->session->set('notice', array('message'=>'agency information modified','type'=>'success'));
				}
				else
				{
					$errors=$post->errors('errors');
					$notice="";
						foreach($errors as $error) 
						{
							$notice.=$error."<br />";
						}
						$this->session->set('notice', array('message'=>$notice,'type'=>'error'));
				}
				
				
				//if a logo/picture is posted and it's not nameless
				if($_FILES['agency_logo']['error'] == 0 AND strlen($_FILES['agency_logo']['name'] )>0)
				{
		
					$files = new Validation($_FILES);
					$files->add_rules('agency_logo', 'upload::valid', 'upload::required', 'upload::type[gif,jpg,png]', 'upload::size[5M]');
						if ($files->validate())
							{	
								$logo = $_FILES['agency_logo'];
								
								// Temporary file name - the name of the company without whitespaces
								$filename = upload::save($logo, strtolower(str_replace(" ",'', $agency->name)));
								
								// Resize, sharpen, and save the image
								Image::factory($filename)
									->resize(100, 100, Image::WIDTH)
									->save($this->logo_directory.basename($filename).'.jpg');
									$agency->Logo = $this->logo_directory.basename($filename).'.jpg';
									$agency->save();
								// Remove the temporary file
								unlink($filename);
								$this->session->set('notice', array('message'=>'Photo uploaded successfully','type'=>'success'));
								//url::redirect('profiles/edit_company/'.$company_id);
							}
				}
				
				//if a logo/picture is posted and it's not nameless
				if($_FILES['new_ticket_photo']['error'] == 0 AND strlen($_FILES['new_ticket_photo']['name'] )>0)
				{
		
					$files = new Validation($_FILES);
					$files->add_rules('new_ticket_photo', 'upload::valid', 'upload::required', 'upload::type[gif,jpg,png]', 'upload::size[5M]');
						if ($files->validate())
							{	
								$ticket_background = $_FILES['new_ticket_photo'];
								
								// Temporary file name - the name of the company without whitespaces
								$filename = upload::save($ticket_background, strtolower(str_replace(" ",'', "ticket.".$agency->name)));
								
								// Resize, sharpen, and save the image
								Image::factory($filename)
									->resize(100, 100, Image::WIDTH)
									->save($this->ticket_directory.basename($filename).'.jpg');
									$agency->ticket_background = $this->ticket_directory.basename($filename).'.jpg';
									$agency->save();
								// Remove the temporary file
								unlink($filename);
								$this->session->set('notice', array('message'=>'Photo uploaded successfully','type'=>'success'));
								//url::redirect('profiles/edit_company/'.$company_id);
							}
				}
				
				url::redirect('settings/company');
			}	
		}
		
		$view = new View('settings');
		//$view->settings = $settings;
		//$view->siblings = $siblings;
		//$view->admins = $admins;
		$view->agency=$agency;
		$view->logo = $logo;
		$view->ticket_background = $ticket_background;
		//$view->fixed_expenses = $fixed_expenses;
		//$view->bus_requirements = $bus_requirements;
		//$view->open_tickets = $open_tickets;
		//$view->towns =  Town_Model::get_all_excluding_town($this->agency_town_id);;
		$view->towns =  Town_Model::get_all();
		
		$this->template->content = $view;
	}
	
	
	function edit_company($company_id)
	{
		Authlite::check_admin();
		Authlite::verify_referer();
		
		//Authlite::check_integrity($schedule_id);
		$this->template->title='Edit company details';
		$agency=get::agency($this->agency_id);
		$logo=get::agency_logo($company_id);
		
		if($_POST)
		{
			$post = new Validation($_POST);
			//$post->add_rules('company_name','required');
			
			if ($post->validate())
			{
				$contacts = $_POST['contacts'];
				$ticket_info = $_POST['ticket_info'];
				
				$company->contacts = $contacts;
				$company->ticket_info = $ticket_info;
				$company->save();	
				$this->session->set('notice', array('message'=>'Company information modified','type'=>'success'));
			}
			else
			{
				$errors=$post->errors('errors');
				$notice="";
					foreach($errors as $error) 
					{
						$notice.=$error."<br />";
					}
					$this->session->set('notice', array('message'=>$notice,'type'=>'error'));
			}
			
			
			//if a logo/picture is posted and it's not nameless
			if($_FILES['agency_logo']['error'] == 0 AND strlen($_FILES['agency_logo']['name'] )>0)
			{
				
				
				//chmod($this->logo_directory, 0744);
				//var_dump(substr(sprintf('%o', fileperms($this->logo_directory)), -4));
					
					/*if(is_writable($this->logo_directory)){
						die('directory is writeable');
					}else{
						die('DIR NOT WRITEABEL');
					} */

				$files = new Validation($_FILES);
				$files->add_rules('agency_logo', 'upload::valid', 'upload::required', 'upload::type[gif,jpg,png]', 'upload::size[1M]');
					if ($files->validate())
						{	
							$logo = $_FILES['agency_logo'];
							
							// Temporary file name - the name of the company without whitespaces
							$filename = upload::save($logo, strtolower(str_replace(" ",'', $company->name)));
							
							// Resize, sharpen, and save the image
							Image::factory($filename)
								->resize(100, 100, Image::WIDTH)
								->save($this->logo_directory.basename($filename).'.jpg');
						 
							// Remove the temporary file
							unlink($filename);
							$this->session->set('notice', array('message'=>'Photo uploaded successfully','type'=>'success'));
							//url::redirect('profiles/edit_company/'.$company_id);
						}
			}
			url::redirect('profiles/edit_company/'.$company_id);
			}
		
			
			$view=new View('profiles_edit_company');
			$view->agency=$agency;
			$view->logo = $logo;
			$this->template->content=$view;
			
	} 
	
	public function add_admin()
	{
		Authlite::check_admin();
		Authlite::verify_referer();
		
		//Authlite::check_integrity($schedule_id);
		$this->template->title='Add admin details';
		$admin=ORM::FACTORY('admin');
		$groups = get::all_admingroups();
		$dashboard_items = Dashboard_Item_Model::get_all();
		
		if($_POST)
		{
			$post = new Validation($_POST);
			$post->add_rules('username','required','valid::alpha_numeric');
			$post->add_rules('password','required');
			
			if ($post->validate())
			{
				
				$username = $_POST['username'];
				$password = $_POST['password'];
							
				$user_exists = ORM::FACTORY('admin')->where('username',$username)->count_all();
				
				if ($user_exists)
				{
					$this->session->set('notice', array('message'=>Kohana::lang('backend.username_taken'),'type'=>'error'));
					url::redirect('settings/add_admin');
				}else
				{	
					//Ensure that user is allowed to use at least one module
					if(array_sum($_POST['all_modules'])==0)
					{
						$this->session->set('notice', array('message'=>'Select at least 1 module','type'=>'error'));
						url::redirect('settings/add_admin');
					}else
					{
					
						// Admin group id is 2 by default. ie. BM CAN only create a regular admin
						$notice = Admin_Model::safe_add_admin($username, $password, $this->agency_id, $this->admin->username);
						
						//After adding the admin, add all main modules for him in the admin_modules table with corresponding values
						foreach($_POST['all_modules'] as $module_id=>$value) 
						{	
							
							Admin_Module_Model::add_admin_modules($username, $module_id, $value);
						}
						//After adding the admin, add all dashboard items with value 1
						foreach($dashboard_items as $item) 
						{	//Set 1 by default
							$value =1;
							//For restrict user to town, set by default to 0
							if($item->id==3){$value=0;}
							Admin_Dashboard_Item_Model::add_dashboard_items($username, $item->id, $value);
						}
						
						
						$this->session->set('notice', array('message'=>$notice,'type'=>'success'));			
					}
				}
					
				}
				else
				{
					$errors=$post->errors('errors');
					$notice="";
						foreach($errors as $error) 
						{
							$notice.=$error."<br />";
						}
						$this->session->set('notice', array('message'=>$notice,'type'=>'error'));
				}
			
				url::redirect('settings');
			}
		
			
			$view=new View('control_add_admin');
			$view->admin=$admin;
			//$view->groups = $groups;
			//$view->logo = $logo;
			$this->template->content=$view;
			
	}
	
	public function add_fixed_expense()
	{
		Authlite::check_admin();
		Authlite::verify_referer();
		$this->template->title='Add fixed expenses';
		
		if($_POST)
		{
			if (isset($_POST['expense_is_percent']))
			{
				
				$post = new Validation($_POST);
				$post->add_rules('expense_name','required');
				$post->add_rules('expense_value','required');
				
				if ($post->validate())
				{	
					$expense_name=$_POST['expense_name'];
					$expense_value=$_POST['expense_value'];
					
					$expense_exists = ORM::FACTORY('fixed_expense_detail')->where('name',$expense_name)->count_all();
					if ($expense_exists)
					{
						$this->session->set('notice', array('message'=>Kohana::lang('backend.expense_exists'),'type'=>'error'));
						url::redirect('settings/add_fixed_expense');
					}else
					{	//if it was a percentage
							Fixed_Expense_Detail_Model::add_fixed_expense($this->agency_id, $expense_name, $expense_value, $expense_is_percent=1, $expense_is_amount=0, $this->admin->username);
							$this->session->set('notice', array('message'=>'Fixed expense added','type'=>'success'));
					}
				}
				else
				{
					$errors=$post->errors('errors');
					$notice="";
						foreach($errors as $error) 
						{
							$notice.=$error."<br />";
						}
						$this->session->set('notice', array('message'=>$notice,'type'=>'error'));
				}
				url::redirect('settings');
			}
			elseif (isset($_POST['expense_is_amount']))
			{
				$post = new Validation($_POST);
				$post->add_rules('expense_name2','required');
				$post->add_rules('expense_value2','required');
				
				if ($post->validate())
				{	
					$expense_name2=$_POST['expense_name2'];
					$expense_value2=$_POST['expense_value2'];
					
					$expense_exists = ORM::FACTORY('fixed_expense_detail')->where('name',$expense_name2)->count_all();
					if ($expense_exists)
					{
						$this->session->set('notice', array('message'=>Kohana::lang('backend.expense_exists'),'type'=>'error'));
						url::redirect('settings/add_fixed_expense');
					}else
					{	//if it was a percentage
							Fixed_Expense_Detail_Model::add_fixed_expense($this->agency_id, $expense_name2, $expense_value2, $expense_is_percent=0, $expense_is_amount=1, $this->admin->username);
							$this->session->set('notice', array('message'=>'Fixed expense added','type'=>'success'));
					}
				}
				else
				{
					$errors=$post->errors('errors');
					$notice="";
						foreach($errors as $error) 
						{
							$notice.=$error."<br />";
						}
						$this->session->set('notice', array('message'=>$notice,'type'=>'error'));
				}
				url::redirect('settings');
			}
			
	}
		$view=new View('add_fixed_expense');
		$this->template->content=$view;
	}
	
	public function add_open_ticket()
	{
		Authlite::check_admin();
		Authlite::verify_referer();
		$this->template->title='Add fixed expenses';
		
		if($_POST)
		{
			
				$post = new Validation($_POST);
				$post->add_rules('open_ticket_name','required');
				$post->add_rules('open_ticket_price','required');
				$post->add_rules('open_ticket_town_from','required');
				$post->add_rules('open_ticket_town_to','required');
				
				if ($post->validate())
				{	
					$open_ticket_name=$_POST['open_ticket_name'];
					$open_ticket_price=$_POST['open_ticket_price'];
					$open_ticket_town_from=$_POST['open_ticket_town_from'];
					$open_ticket_town_to=$_POST['open_ticket_town_to'];
					
					if ($open_ticket_town_from == $open_ticket_town_to)
					{
						$this->session->set('notice', array('message'=>Kohana::lang('same_origin_destination'),'type'=>'error'));
						//url::redirect('admin/main/'.$this->admin->agency_id);
					}else
					{
					$notice = Open_Ticket_Model::add_open_ticket($this->agency_id,$open_ticket_name,$open_ticket_price,$open_ticket_town_from,$open_ticket_town_to);		
					
						$this->session->set('notice', array('message'=>Kohana::lang('backend.saved'),'type'=>'notice'));
						//url::redirect('settings/add_fixed_expense');
					}
				}
				else
				{
					$errors=$post->errors('errors');
					$notice="";
						foreach($errors as $error) 
						{
							$notice.=$error."<br />";
						}
						$this->session->set('notice', array('message'=>$notice,'type'=>'error'));
				}
				url::redirect('settings');
			
			
			
		}
		//$view=new View('add_fixed_expense');
		//$this->template->content=$view;
	}
	
	public function add_expense_category()
	{
		Authlite::check_admin();
		Authlite::verify_referer();
		$this->template->title='Add expense category';
		
		if($_POST)
		{
			
				$post = new Validation($_POST);
				$post->add_rules('category_name','required');
				
				if ($post->validate())
				{	
					$category_name=$_POST['category_name'];
					$category_description=$_POST['description'];
					
					$category_exists = ORM::FACTORY('expense_category')->where('name',$category_name)->count_all();
					
					if ($category_exists){
						$this->session->set('notice', array('message'=>Kohana::lang('backend.category_exists'),'type'=>'error'));
					}else
					{
						Expense_category_Model::add_category($this->agency_id, $category_name, $category_description, strtoupper($this->admin->username));
						$this->session->set('notice', array('message'=>Kohana::lang('backend.category_added') ,'type'=>'success'));
						url::redirect('settings/expenses');
					}
				}
				else
				{
					$errors=$post->errors('errors');
					$notice="";
						foreach($errors as $error) 
						{
							$notice.=$error."<br />";
						}
						$this->session->set('notice', array('message'=>$notice,'type'=>'error'));
				}
				url::redirect('settings/expenses');
			
			
			
		}
		$view=new View('add_expense_category');
		$this->template->content=$view;
	}
	
	public function edit_expense_category($expense_category_id)
	{
		Authlite::check_admin();
		Authlite::verify_referer();
		$this->template->title='Add expense category';
			$expense_category=Expense_category_Model::get($this->agency_id,$expense_category_id);
		
		if($_POST)
		{
			
				$post = new Validation($_POST);
				$post->add_rules('category_name','required');
				
				if ($post->validate())
				{	
					$new_category_name=$_POST['category_name'];
					$category_description=$_POST['description'];
					
					//check if there's a new name, and a category already existing with that name
					if($new_category_name != $expense_category->name){
						
						$category_exists = ORM::FACTORY('expense_category')->where('name',$new_category_name)->count_all();
						if ($category_exists){
							$this->session->set('notice', array('message'=>Kohana::lang('backend.category_exists'),'type'=>'error'));
							url::redirect('settings/expenses/edit_expense_category/'.$expense_category_id);
						}
					}
					
						$notice = Expense_category_Model::edit_category($this->agency_id,$expense_category_id,$new_category_name, $category_description, strtoupper($this->admin->username));	
						$this->session->set('notice', array('message'=>Kohana::lang('backend.saved') ,'type'=>'success'));
						url::redirect('settings/expenses/edit_expense_category/'.$expense_category_id);
					
				}
				else
				{
					$errors=$post->errors('errors');
					$notice="";
						foreach($errors as $error) 
						{
							$notice.=$error."<br />";
						}
						$this->session->set('notice', array('message'=>$notice,'type'=>'error'));
				}
				url::redirect('settings/expenses');
			
			
			
		}
		$view=new View('edit_expense_category');
		$view->expense_category=$expense_category;
		$this->template->content=$view;
	}
	
	public function delete_expense_category($expense_category_id)
	{		
			Authlite::check_admin();
			Authlite::verify_referer();
			
			//$this->template->title="Edit expense category";
			//Ensure the default category cannot be deleted
			if($expense_category_id !='1'){
				$notice = Expense_category_Model::safe_delete_category($this->agency_id,$expense_category_id,strtoupper($this->admin->username));		
				$this->session->set('notice', array('message'=>$notice,'type'=>'success'));
			}else{
				$this->session->set('notice', array('message'=>Kohana::lang('backend.default_no_delete'),'type'=>'error'));
			}
			url::redirect('settings/expenses');
			
			$this->template->content='';
	}

	public function delete_open_ticket($open_ticket_id)
	{		
			Authlite::check_admin();
			Authlite::verify_referer();
			
			$this->template->title="Delete open expense";
			
			$notice = Open_Ticket_Model::delete_open_ticket($this->agency_id,$open_ticket_id);		
			
			$this->session->set('notice', array('message'=>$notice,'type'=>'success'));
			//url::redirect('settings');
			
			$this->template->content='';
	}
	public function delete_fixed_expense($expense_id)
	{		
			Authlite::check_admin();
			Authlite::verify_referer();
			
			$this->template->title="Delete fixed expense";
			
			$notice = Fixed_Expense_Detail_Model::delete_expense($this->agency_id,$expense_id);		
			
			$this->session->set('notice', array('message'=>$notice,'type'=>'success'));
			url::redirect('settings');
			
			$this->template->content='';
	}
	
	function view_fixed_expense($expense_id)
	{
			Authlite::check_admin();
			Authlite::verify_referer();
			
			//Authlite::check_integrity($schedule_id);
			$this->template->title='Edit admin details';
			$fixed_expense=Fixed_Expense_Detail_Model::get_expense($this->agency_id,$expense_id);
			

		if($_POST)
		{
			if (isset($_POST['expense_is_percent']))
			{
				
				$post = new Validation($_POST);
				$post->add_rules('expense_name','required');
				$post->add_rules('expense_value','required');
				
				if ($post->validate())
				{	
					$expense_name=$_POST['expense_name'];
					$expense_value=$_POST['expense_value'];
					
						//if it was a percentage
							Fixed_Expense_Detail_Model::edit_fixed_expense($this->agency_id, $expense_name, $expense_value, $expense_is_percent=1, $expense_is_amount=0, $expense_id);
							$this->session->set('notice', array('message'=>'Fixed expense added','type'=>'success'));
					
				}
				else
				{
					$errors=$post->errors('errors');
					$notice="";
						foreach($errors as $error) 
						{
							$notice.=$error."<br />";
						}
						$this->session->set('notice', array('message'=>$notice,'type'=>'error'));
				}
				url::redirect('settings');
			}
			elseif (isset($_POST['expense_is_amount']))
			{
				$post = new Validation($_POST);
				$post->add_rules('expense_name2','required');
				$post->add_rules('expense_value2','required');
				
				if ($post->validate())
				{	
					$expense_name2=$_POST['expense_name2'];
					$expense_value2=$_POST['expense_value2'];
					
						//if it was a percentage
							Fixed_Expense_Detail_Model::edit_fixed_expense($this->agency_id, $expense_name2, $expense_value2, $expense_is_percent=0, $expense_is_amount=1, $expense_id);
							$this->session->set('notice', array('message'=>Kohana::lang('backend.setting_saved'),'type'=>'success'));
					
				}
				else
				{
					$errors=$post->errors('errors');
					$notice="";
						foreach($errors as $error) 
						{
							$notice.=$error."<br />";
						}
						$this->session->set('notice', array('message'=>$notice,'type'=>'error'));
				}
				url::redirect('settings');
			}
	}	
			$view=new View('view_fixed_expense');
			$view->fixed_expense=$fixed_expense;
			//$view->logo = $logo;
			$this->template->content=$view;
			
	}
	function edit_admin($admin_id)
	{
			Authlite::check_admin();
			Authlite::verify_referer();
			
			//Authlite::check_integrity($schedule_id);
			$this->template->title='Edit admin details';
			$admin=get::admin($admin_id);
			$dashboard_items = Dashboard_Item_Model::get_all();
			$main_modules = Main_Module_Model::get_all();
			$towns = Town_Model::get_all();

		if($_POST)
		{
			
			if(isset($_POST['change_privileges']))
			{
			
			$post = new Validation($_POST);
			if ($post->validate())	
				{
					//Ensure admin is allowed to at least one module
					if(array_sum($_POST['all_modules'])==0)
					{
						$this->session->set('notice', array('message'=>'Select at least 1 module','type'=>'error'));
						url::redirect('settings/edit_admin/'.$admin_id);
					}		
					else	
					{	
						foreach($_POST['all_modules'] as $module_id=>$value) 
						{	
							//If he checks parcels, get which types of parcels he selected.
							if($_POST['all_modules'][4] == 1)
							{
								$manage_mails = $_POST['mails'];
								$manage_baggage = $_POST['baggage'];
								
								
								//He must select either mails or baggage if he's selected parcels
								if($manage_baggage==0 AND $manage_mails==0 )
								{//var_dump($manage_baggage + $manage_mails);exit;
									$this->session->set('notice', array('message'=>'Select at least 1 parcel type.','type'=>'error'));
									url::redirect('settings/edit_admin/'.$admin_id);
								}else
								{
									Admin_Module_Model::update_admin_module_parcel_types($admin->username, $manage_mails, $manage_baggage);
								}
																
							}
							Admin_Module_Model::update_admin_module($admin->username, $module_id, $value);
						}
						$this->session->set('notice', array('message'=>'Admin information modified','type'=>'success'));
					}
					
					//Set the dashboard items
					foreach($_POST['all_dashboard_items'] as $item_id=>$value) 
						{	
							//By default, set restricted town_id to 0
							$restricted_town_id=0;
							//If he checks Restricted town, get the id.
							if($_POST['all_dashboard_items'][3] == 1)
							{
								$restricted_town_id = $_POST['restricted_town'];
								
								//If the id is 0, then he selected 'none'. So he's not restricted to any town. Set value to 0
								if($restricted_town_id==0)
								{
									$this->session->set('notice', array('message'=>'You must select a town to restrict user to.','type'=>'error'));
									url::redirect('settings');
								}
																
							}
							//var_dump($restricted_town_id);exit;
							Admin_Dashboard_Item_Model::update_admin_dashboard_item($admin->username, $item_id, $value, $restricted_town_id);
						}
						$this->session->set('notice', array('message'=>'Admin information modified','type'=>'success'));
					
				}
				else
				{
					$errors=$post->errors('errors');
					$notice="";
						foreach($errors as $error) 
						{
							$notice.=$error."<br />";
						}
						$this->session->set('notice', array('message'=>$notice,'type'=>'error'));
				}		
			}
			if(isset($_POST['change_password']))
			{	 
				$post = new Validation($_POST);
				$post->add_rules('new_password','required');
				if ($post->validate())	
				{	
					$new_password = $_POST['new_password'];
					$notice = Admin_model::change_password($admin_id,$new_password);
					$this->session->set('notice', array('message'=>$notice,'type'=>'success'));		
				}else
				{
					$errors=$post->errors('errors');
					$notice="";
						foreach($errors as $error) 
						{
							$notice.=$error."<br />";
						}
						$this->session->set('notice', array('message'=>$notice,'type'=>'error'));
				}
			}
			
			url::redirect('settings/administrators');
			}	
			$view=new View('control_edit_admin');
			$view->main_modules=$main_modules;
			$view->dashboard_items=$dashboard_items;
			$view->admin=$admin;
			$view->towns = $towns;
			$this->template->content=$view;
			
	}
	public function delete_admin($admin_id)
	{		
			Authlite::check_admin();
			Authlite::verify_referer();
			
			$this->template->title="Delete admin";
			$deleted_by = $this->admin->username;
			$notice = Admin_Model::safe_delete_admin($admin_id,$deleted_by);		
			
			$this->session->set('notice', array('message'=>$notice,'type'=>'success'));
			url::redirect('settings');
			
			$this->template->content='';
	}
	
	public function expense_category()
	{
		Authlite::check_admin();
		Authlite::verify_referer();
		
		$this->template->title='Add expense category';
			
		if($_POST)
		{
			$post = new Validation($_POST);
			$post->add_rules('category_name','required');
			
			if ($post->validate())
			{
				
				$category_name = $_POST['category_name'];
			
				$category_exists = ORM::FACTORY('expense_category')->where('name',$category_name)->count_all();
				
				if ($category_exists)
				{
					$this->session->set('notice', array('message'=>Kohana::lang('backend.category_exists'),'type'=>'error'));
					url::redirect('settings');
				}else
				{
					Expense_category_Model::add_category($this->agency_id, $category_name, $category_description);
				}
					
					//$this->session->set('notice', array('message'=>'Admin information modified','type'=>'success'));
				}
				else
				{
					$errors=$post->errors('errors');
					$notice="";
						foreach($errors as $error) 
						{
							$notice.=$error."<br />";
						}
						$this->session->set('notice', array('message'=>$notice,'type'=>'error'));
				}
			
				url::redirect('settings');
			}
		
			
			$view=new View('add_expense_category');
			
			$this->template->content=$view;
			
	}public function add_bus_requirement()
	{
		Authlite::check_admin();
		Authlite::verify_referer();
		
		//Authlite::check_integrity($schedule_id);
		$this->template->title='Add bus requirment';
			
		if($_POST)
		{
			$post = new Validation($_POST);
			$post->add_rules('req_name','required');
			
			if ($post->validate())
			{
				
				$req_name = $_POST['req_name'];
			
				$requirement_exists = ORM::FACTORY('bus_requirement')->where('Name',$req_name)->count_all();
				
				if ($requirement_exists)				{
					$this->session->set('notice', array('message'=>Kohana::lang('backend.requirement_exists'),'type'=>'error'));
					url::redirect('settings');
				}else				{
					Bus_Requirement_Model::add_requirement($this->agency_id, $req_name);
					$this->session->set('notice', array('message'=>'saved','type'=>'success'));
					
				}
					
					
				}
				else
				{
					$errors=$post->errors('errors');
					$notice="";
						foreach($errors as $error) 
						{
							$notice.=$error."<br />";
						}
						$this->session->set('notice', array('message'=>$notice,'type'=>'error'));
				}
			
				url::redirect('settings/advanced');
			}
		
			
			$view=new View('add_bus_requirement');
			
			$this->template->content=$view;
			
	}
	
	public function delete_bus_requirement($requirement_id)
	{		
			Authlite::check_admin();
			Authlite::verify_referer();
			
			//$this->template->title="Delete fixed expense";
			
			$notice = Bus_Requirement_Model::delete_requirement($this->agency_id,$requirement_id);		
			
			$this->session->set('notice', array('message'=>$notice,'type'=>'success'));
			url::redirect('settings');
			
			$this->template->content='';
	}
	
	public function auto_disburse_cash()
	{
		Authlite::check_admin();
		Authlite::verify_referer();
		
		//Authlite::check_integrity($schedule_id);
		$this->template->title='Auto-disburse cash';
			
		if($_POST)
		{
			$post = new Validation($_POST);
			//$post->add_rules('req_name','required');
			
			if ($post->validate())
			{
				
				$auto_disburse_cash_to_agency_id = $_POST['auto_disburse_cash_to_agency_id'];

				$settings = Setting_Model::get($this->agency_id);
				//var_dump($settings);exit;
				$settings->auto_disburse_cash_to_agency_id = $auto_disburse_cash_to_agency_id;
				$settings->save();
					
					$this->session->set('notice', array('message'=>'Saved','type'=>'success'));
				}
				else
				{
					$errors=$post->errors('errors');
					$notice="";
						foreach($errors as $error) 
						{
							$notice.=$error."<br />";
						}
						$this->session->set('notice', array('message'=>$notice,'type'=>'error'));
				}
			
				url::redirect('settings');
			}
		
			
			$view=new View('add_bus_requirement');
			
			$this->template->content=$view;
			
	}
	
		
		}