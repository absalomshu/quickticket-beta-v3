<?php defined('SYSPATH') or die('No direct script access'); ?> 

	  
	<div class="row-fluid marketing" style="margin:5px 0 0px 0;">	
		<div class="span12 hero-unit prof-unit reload" style="margin-left:0px;">
	

				<ul class="nav nav-tabs">
					<li><a href="<?=url::site('admin/all_schedules/current')?>"> <i class="icon-time "></i> <?=Kohana::lang('backend.current')?> </a></li>
					<li><a href="<?=url::site('admin/all_schedules/departed/sort_by_date')?>"> <i class="icon-ok-sign "></i> <?=Kohana::lang('backend.departed')?> </a></li>
					
					<li class="active"><a href="#"> <i class="icon-tags "></i> <?=Kohana::lang('backend.tickets_pending')?> </a></li>
					<li class=""><a href="<?=url::site('admin/remote_tickets')?>"> <i class="icon-random "></i> <?=Kohana::lang('backend.remote_tickets')?><?php if($this->total_pending_remote_tickets) {?> <span class="badge badge-important"><?=$this->total_pending_remote_tickets;?><?php }?> </a></li>
										<li class=""><a href="<?=url::site('admin/online_purchases')?>"> <i class="icon-globe "></i> <?=Kohana::lang('backend.online_purchases')?> <?php if($this->total_pending_online_purchases) {?> <span class="badge badge-important"><?=$this->total_pending_online_purchases;?><?php }?></a></li>
					
					<li <?php if ($this->uri->segment(2)=='check_schedules'){?> class="active"<?php } ?>><a href="<?=url::site('admin/check_schedules')?>"><i class="icon-search"></i> <?=Kohana::lang('backend.check_schedules')?></a></li>

				</ul>
				<legend><?=Kohana::lang('backend.clients_who_missed_schedule')?></legend>
				
				<table class="table table-striped table-hover table-condensed">
					<tr>
						<th><?=Kohana::lang('backend.schedule_date')?> </th>
						<th><?=Kohana::lang('backend.name')?> </th>
						<th><?=Kohana::lang('backend.original_schedule')?> </th>
						<th><?=Kohana::lang('backend.price')?> </th>
						<th><?=Kohana::lang('backend.checked_out')?> </th>
						<th>Actions </th>
					</tr>
					<?php foreach ($missed_tickets as $ticket){
					 //$schedule = Schedule_Model::get($ticket->ScheduleID);
					
					 //var_dump($missed_tickets);exit;
					
					?>
					<tr>
						<td><?=date("d-m-Y",strtotime($ticket->departure_date))?></td>						
						<td><?=$ticket->ClientName?></td>
						<td><?=$ticket->bus_number?></a> : <?=$this->agency_town?> -> <b><?=$ticket->town_name_to?></b> : <b><?=date("g:i A", strtotime($ticket->departure_time))?></td>
						<td><?=number_format($ticket->Price)?></td>
						<td><?php if($ticket->status=="departed") {echo date("g:i A", strtotime($ticket->checked_out_time));}?></td>
						<td>
							<div class="btn-group ">
									  <a class="btn dropdown-toggle btn-mini" data-toggle="dropdown" href="#" >
												-- <?=Kohana::lang('backend.select')?> --
									  <span class="caret"></span>
									  </a>
									  <ul class="dropdown-menu">
										<!--Note that the ticket id is inserted as the id of the modal, and trigger/link to the modal. If not, each seat will be
										bringing up the same modal, hence only the first free ticket will be picked each time. VERY IMPORTANT-->
										<li><a data-toggle="modal" href="#confirm-free<?=$ticket->id?>" ><i class="icon-plus"></i> <?=Kohana::lang('backend.add_to_schedule')?></a></li>
										<li><a href="<?=url::site('admin/delete_pending_ticket/'.$ticket->id)?>" name="empty_seat" onClick="return confirm('<?=Kohana::lang('backend.confirm_delete')?>?');"><i class="icon-trash"></i> <?=Kohana::lang('backend.delete')?></a></li>
									 </ul>
							</div>
									
									  <!-- Modal for confirm missed to free ticket-->
										<div id="confirm-free<?=$ticket->id?>" class="modal hide fade" tabindex="-1" role="dialog" aria-labelledby="myModalLabel" aria-hidden="true">
										  <div class="modal-header">
											<button type="button" class="close" data-dismiss="modal" aria-hidden="true">×</button>
											<h3 id="myModalLabel"><?=Kohana::lang('backend.confirm_free_ticket')?></h3>
										  </div>
										  <div class="modal-body">
											<legend><?=Kohana::lang('backend.ticket_will_be')?></legend>
										
											
												 
												  <?php 
												  
												  foreach($current_schedules as $schedule){ 
												  
												  
												  ?>
												  
												  <form action="<?=url::site('admin/complete_schedule/'.$schedule->id)?>" method="POST" >
												  <!--Build a form identical to the complete_schedule form, as we'll post to the same function-->
												  <input type="text" name="schedule_id" value="<?=$schedule->id?>" style="display:none" />
												  <input type="text" name="ticket_id" value="<?=$ticket->id?>" style="display:none" />
												  <?php
													//Check if the ticket is being added to the same schedule from which it was removed in which case, it's not a free ticket.
													//
													//if($ticket->ScheduleID != $schedule->id)
													//If ticket is free, still activate this as previoius will be deleted and a new one created.
													if($ticket->IsRemote OR $ticket->FreeTicket)
													{
													  ?>
														<input type="checkbox" name="free_ticket" id="ft" checked="checked" style="display:none"/>
														<?php
															//Set a variable reason for the ticket pending to be used when adding as a free ticket
															//If it's a missed ticket...else if it's just pending, maybe as it's a remote ticket
															//If it's a free ticket, maintain the free_ticket_reason
															if($ticket->FreeTicket){$free_ticket_reason = $ticket->FreeTicketReason;}
															//if($ticket->MissedStatus){$free_ticket_reason = "Marked absent earlier.";}
															//If it's remote, then specify paid in another branch
															if($ticket->IsRemote){$free_ticket_reason = "Paid in another branch.";}
														?>
														<input type="text"  name="free_ticket_reason" value="<?=$free_ticket_reason?>" style="display:none" />
														<?php 
													} ?>
													
													<input type="text"  name="client_name" value="<?=$ticket->ClientName?>" style="display:none" />
													<input type="text"  name="client_phone" value="<?=$ticket->ClientPhone?>" style="display:none" />
													<input type="text"  name="client_email" value="<?=$ticket->ClientEmail?>" style="display:none" />
													<input type="text"  name="client_idc" value="<?=$ticket->ClientIDC?>" style="display:none" />
													
													<!--Use this input to tell controller that if it's coming from this field, then delete the missed ticket afterwards-->
													<input type="text"  name="from_missed_ticket" value="<?=$ticket->id?>" style="display:none" />
													
													<div class="span12">
													<span   class="span7"><b><?=$schedule->bus_number?></b> : <?=$this->agency_town?> -> <b><?=$schedule->town_name_to?></b> - <b><?=date("g:i A", strtotime($schedule->departure_time))?></b> : <?=number_format($schedule->ticket_price)?>frs
													<span class="text-error tiny-text">
													<?php
															//Show admin difference in prices IF it's not a free ticket
														
															if(!$ticket->FreeTicket AND $ticket->Price > $schedule->ticket_price){echo "Reimburse: ". number_format($ticket->Price - $schedule->ticket_price);}
															if(!$ticket->FreeTicket AND $ticket->Price < $schedule->ticket_price){echo "Collect: ". number_format($schedule->ticket_price - $ticket->Price );}
															
														?></span>
													</span>
														
													<!--Now get an adequate non-filled, non-reserved seat for him.-->
													<div class="short-dropdown span2">
															<select name="seat_number" id="inputType" class="span10">
															<?php
															//first check if the field in the db is not empty. If it is, create the array. If not, decode the array and populate			
															/*if (empty ($schedule->reserved_seats))
															{
																$seats_and_reservers = array();			
															}
															else
															{
																$seats_and_reservers = json_decode($schedule->reserved_seats,true); //true converts the object to an array
															}
															if (empty ($schedule->seat_occupants))
															{
																$seats_and_occupants = array();			
															}
															else
															{
																$seats_and_occupants = json_decode($schedule->seat_occupants,true);
															} */
															$seats_and_reservers = json_decode($schedule->reserved_seats,true); 
															$seats_and_occupants = json_decode($schedule->seat_occupants,true);
															$number_of_seats=$schedule->bus_seats;
															//start from 2 since seat 1 is the driver
															//$i=2;
															for($i=2; $i<= $number_of_seats; $i++)
															{
																//make sure the seat is not already occupied
																if(!isset($seats_and_occupants[$i])  AND !isset($seats_and_reservers[$i]) )
																{
																?>
																	<!--<input type="text"  name="seat_number" value="<?=$i?>" style="display:none" />-->
																	<option value="<?=$i?>"><?=$i?></option>
																<?php
																}
															} ?>
															</select>
													</div>
													
													<button  type="submit" name="check_out_form" class="btn btn-info"><?=Kohana::lang('backend.add')?></button>
													<br/>
													<hr>
													</div>
													
													</form>									
													<?php } ?>
													
													
												
										  </div>
										  
										  
										  <div class="modal-footer">
											<a class="btn" data-dismiss="modal" aria-hidden="true"><?=Kohana::lang('backend.cancel')?></a>
										  </div>
										</div>
									
															
						</td>
						
						
						
					</tr>
					<?php 
					
					} ?>
				</table>
				
			
					<?php echo $this->pagination;?>
					<?php //$this->profiler = new Profiler();?>
		</div>
	
		
		</div>

