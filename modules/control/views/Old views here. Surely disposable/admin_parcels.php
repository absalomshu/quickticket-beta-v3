<?php defined('SYSPATH') or die('No direct script access'); ?> 


	<?php
		//What the user entered in his previous submission attempt, in case it was a failure
		$post = $this->session->get_once('post');
		//determine what type of notice to display if at all
		$notice = $this->session->get_once('notice');
			if(!empty($notice)){ 
				if($notice['type'] == 'success'){?><div class="alert general success"><a class="close" data-dismiss="alert" href="#">&times;</a><?php echo $notice['message']; }?></div>
			<?	if($notice['type'] == 'error'){?><div class="alert general error"><a class="close" data-dismiss="alert" href="#">&times;</a><?php echo $notice['message']; }?></div>
		<?}?>
    <div class="container">
		
	  
	  <div class="row-fluid marketing" style="margin:5px 0 0px 0px;">
	  
		
	<div class="row-fluid marketing" style="margin:5px 0 0px 0;">	
		<div class="span12 hero-unit prof-unit">
				
				<ul class="nav nav-tabs" id="myTab">
					<li  class="active"><a href="#add-parcel"><i class="icon-plus"></i> <?=Kohana::lang('backend.add_parcel')?></a></li>
					<li><a href="#incoming-parcel"><i class="icon-download"></i> <?=Kohana::lang('backend.incoming')?></a></li>
					<li><a href="#sent-parcel"><i class="icon-upload"></i> <?=Kohana::lang('backend.sent')?></a></li>
				
				
					
				</ul>
				
				<div class="tab-content">
				<div class="tab-pane active" id="add-parcel">
								<div class="span6">
							<form action="<?=url::site('parcels/register_parcel/'.$this->admin->agency_id)?>" method="POST" class="theform">
							<?php //no_csrf::set(1)?>
							
					<legend><?=Kohana::lang('backend.reg_parcels')?></legend>
							
						
						<div class="simple-form span10" >
							<div class="sf_label">To: </div>
								<select name="agency_to" id="inputType">
											<?php foreach ($siblings as $sib):?>
											<option value="<?=$sib->id?>"><?=$sib->name?></option>
											<?php endforeach;?>
								</select>
						</div>
						
						<div class="simple-form span10" >
							<div class="sf_label"><?=Kohana::lang('backend.price')?>: </div>
							<div class="sf_input"><input type="text" placeholder="<?=Kohana::lang('backend.price')?>"  name="price" value="<?=@$post['price']?>"></div>
						</div>
						
						<div class="simple-form span10">
							<div class="sf_label"><?=Kohana::lang('backend.name')?>: </div>
							<div class="sf_input"><input type="text" placeholder="<?=Kohana::lang('backend.rec_name')?>"  name="receiver_name" value="<?=@$post['receiver_name']?>"></div>
						</div>
						<div class="simple-form span10">
							<div class="sf_label"><?=Kohana::lang('backend.phone')?>: </div>
							<div class="sf_input"><input type="text" placeholder="<?=Kohana::lang('backend.rec_phone')?>"  name="receiver_phone" value="<?=@$post['receiver_phone']?>"></div>
						</div>
						<div class="simple-form span10">
							<div class="sf_label">Description: </div>
							<div class="sf_input"><textarea placeholder="<?=Kohana::lang('backend.desc_parcel')?>"  name="parcel_description" style="height:100px;"><?=@$post['parcel_description']?></textarea></div>
						</div>
							<div class="simple-form span10">
							<div class="sf_label"> &nbsp;</div>
							<div class="sf_input"><button type="submit" class="btn btn-info"><?=Kohana::lang('backend.register')?></button></div>
						</div>
							
							
								
							</form>		
						</div>

						<!--end the first tab-->
				</div>  
				<div class="tab-pane" id="incoming-parcel">
					<table class="table table-striped table-hover">
						<tr>
							<th>Date</th>
							<th><?=Kohana::lang('backend.from')?></th>
							<th><?=Kohana::lang('backend.owner')?></th>
							<th>Description</th>
							<th><?=Kohana::lang('backend.status')?></th>
						</tr>
						<?php foreach ($incoming_parcels as $parcel):?>
						<tr>	
							<td><?=date("d-m-Y",strtotime($parcel->sent_date))?></td>
							
							<td><?php echo get::agency_name($parcel->from);?></td>
							
							<td><?=$parcel->receiver_name?></td>
							<!--Trim if the description is too long. -->
							<td><a href="<?=url::site('parcels/deliver/'.$parcel->id)?>"><?php if (strlen($parcel->description) > 25) { echo substr($parcel->description,0,35)."...";} else { echo $parcel->description;}?></a></td>
						
							<td >
								<?php		
								//if the state is sent, and destination is the admin's agency
									
									if($parcel->state == 'sent' AND $parcel->to == $this->admin->agency_id){?>
								<span class=""> <a href="<?=url::site('parcels/receive/'.$parcel->id)?>" title="Has this parcel been delivered to your agency?"><?=Kohana::lang('backend.receive')?></a></span>
								<?php }elseif ($parcel->state == 'received') { ?>
								<span class=""><?=Kohana::lang('backend.received')?>! | <a href="<?=url::site('parcels/deliver/'.$parcel->id)?>" title="Has the owner of this parcel collected it?"><?=Kohana::lang('backend.deliver')?></a></span>
								<?php } elseif ($parcel->state == 'sent' AND $parcel->from == $this->admin->agency_id ) { ?>
								<span class=""><?=Kohana::lang('backend.sent')?></span>
								<?php } elseif ($parcel->state == 'delivered') { ?>
								<span class=""><?=Kohana::lang('backend.rec_del')?>! </span>
								<?php }

										?>
							</td>
						</tr>
						
						
						<?php endforeach; ?>
						</table>
						<div class="pagination"><?php echo $this->incoming_pagination;?>	</div>							
				</div>
				<div class="tab-pane" id="sent-parcel">
				
				
				
					<table class="table table-striped table-hover">
						<tr>
							<th>Date</th>
							<th><?=Kohana::lang('backend.from')?></th>
							<th><?=Kohana::lang('backend.owner')?></th>
							<th>Description</th>
							<th><?=Kohana::lang('backend.status')?></th>
						</tr>
						<?php foreach ($sent_parcels as $parcel):?>
						<tr>	
							<td><?=date("d-m-Y",strtotime($parcel->sent_date))?></td>
							
							<td><?php echo"-";?></td>
							
							<td><?=$parcel->receiver_name?></td>
							<!--Trim if the description is too long. -->
							<td><a href="<?=url::site('parcels/deliver/'.$parcel->id)?>"><?php if (strlen($parcel->description) > 25) { echo substr($parcel->description,0,35)."...";} else { echo $parcel->description;}?></a></td>
						
							<td >
								<?php		
								
								
								//Firstly, if it's from here, then it's sent.
								if($parcel->from == $this->admin->agency_id){?>
								<span class="">Sent</span>
								<?php }
								?>
							</td>
						</tr>
						
						
						<?php endforeach; ?>
						</table>
						<div class="pagination"><?php echo $this->sent_pagination;?>	</div>	
						
				</div>
				
		</div>
		
		
		
	
		
		</div>
	  </div>
	  <div style="height:19px;"></div>
    </div> 
<script type="text/javascript">
	$(document).ready(function(){
						
				$(function () {
				$('#myTab a').click(function (e) {
					  e.preventDefault();
					  $(this).tab('show');  
			})
			  })
			  
		
			  
			
	})
	</script>