<div style="clear:both;height:50px;"></div>

		<div class="container-fluid footer-content">
					
					
		<span class="footer-left"><span class="heading"><?=Kohana::lang('backend.follow')?>:</span> 
			<a href="https://www.facebook.com/getquickticket" target="_blank"><img src="<?=url::base()?>images/social/facebook.png" width="48" height="48"/></a>
			<a href="https://twitter.com/getquickticket" target="_blank"><img src="<?=url::base()?>images/social/twitter.png"  width="48" height="48"/></a>
			<a href="#" ><img src="<?=url::base()?>images/social/google.png"  width="48" height="48"/></a>
		</span>
	
		       <span class="footer-right">
					<!--<a href="<?=url::site('terms/conditions')?>"><?=Kohana::lang('backend.terms')?></a> | <a href="<?=url::site('terms/contact')?>">Contact</a>
					<br/>-->
					
					QuickTicket &copy; 2016 
				</span>
		</div>
	
    <!-- Le javascript
    ================================================== -->
    <!-- Placed at the end of the document so the pages load faster -->
  
	<script src="<?=url::base()?>js/bjqs-1.3.min.js"></script>
	<script src="<?=url::base()?>js/bootstrap-datepicker.js"></script>
	<script src="<?=url::base()?>js/bootstrap-timepicker.min.js"></script>
    <script src="<?=url::base()?>js/bootstrap-transition.js"></script>
    <script src="<?=url::base()?>js/bootstrap-alert.js"></script>
    <script src="<?=url::base()?>js/bootstrap-modal.js"></script>
    <script src="<?=url::base()?>js/bootstrap-tooltip.js"></script>
    <script src="<?=url::base()?>js/bootstrap-popover.js"></script>
    <script src="<?=url::base()?>js/bootstrap-dropdown.js"></script>
    <script src="<?=url::base()?>js/bootstrap-scrollspy.js"></script>
    <script src="<?=url::base()?>js/bootstrap-tab.js"></script>
    <script src="<?=url::base()?>js/bootstrap-collapse.js"></script>
	
<script type="text/javascript">
	//js for the datepicker. done in the footer so it doesn't slow lower page load, and inline since it's not much
	$(document).ready(function(){
	$('.input-append.date').datepicker({
	format: "dd-mm-yyyy",
    todayBtn: "linked",
    autoclose: true,
    todayHighlight: true
	
});
	}) 
	
	
</script>

 