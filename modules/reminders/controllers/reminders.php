<?php defined('SYSPATH') or die('No direct script access');

//Almost all functions will take as parameter, the id of the agency we are dealing with
//in order not to interfere with another's stuff.

class Reminders_Controller extends Admin_Controller {
			
	
	public function __construct()
	{	
		parent::__construct();
		$this->session = Session::instance();
		
	}  
	
	
	public function all()
	{	
		Authlite::check_admin();
		Authlite::verify_referer();
		
		$this->template->title = Kohana::lang('backend.settings');
		$this->template->title='Reminders';
		
		$all_buses = Bus_Model::get_all($this->agency_id);
		
		$all_reminders = Reminder_Model::get_all($this->agency_id);
		$all_reminders_total = count($all_reminders);
		//pagination
		$per_page = 15;
		
		$this->pagination = new Pagination(array(
			// 'base_url'    => 'welcome/pagination_example/page/', // base_url will default to current uri
			'uri_segment'    => 'page', // pass a string as uri_segment to trigger former 'label' functionality
			'total_items'    => $all_reminders_total, // use db count query here of course
			'items_per_page' => $per_page , // it may be handy to set defaults for stuff like this in config/pagination.php
			'style'          => 'punbb', // pick one from: classic (default), digg, extended, punbb, or add your own!
			//'query_string'   => "ORM::factory('schedule')->where('agency_id',$agency_id)->where('status','current')->find_all()"
		));
			
		$all_reminders=ORM::factory('reminder')->where('AgencyID',$this->agency_id)->orderby('id','desc')->limit($per_page,$this->pagination->sql_offset)->find_all();

		
		if($_POST)
		{	//first, a reminder type must be selected. Either one time or recurring
			if(isset($_POST['reminder_type']))
			{
				
				
				
				$post = new Validation($_POST);
				$post->add_rules('name','required');
				
				
				if($_POST['reminder_type'] == 'occurs_once')
				{
					$post->add_rules('due_date','required');
					$recurring=0;
				}
				if($_POST['reminder_type'] == 'recurring')
				{
					$post->add_rules('start_date','required');
					$post->add_rules('interval_value','required');
					$post->add_rules('interval_type','required');
					$recurring=1;
				}
				
				if ($post->validate())
				{
					$name = $_POST['name'];
					$amount = $_POST['amount'];
					$description = $_POST['description'];
					//This is to ensure that 01-01-1970 is not saved in db when a date is not available
					if(isset($_POST['start_date'])){$start_date = date("Y-m-d",strtotime($_POST['start_date']));}
					if(isset($_POST['due_date']))
					{
						$due_date = date("Y-m-d",strtotime($_POST['due_date']));
						
						
						$today = date_create(date("Y-m-d"));
						$interval=date_diff($today,date_create(date("Y-m-d",strtotime($_POST['due_date']))));
						$days_left = $interval->format('%R%a');
						//Make sure due is not in the past
						if($days_left <0)
						{
							$this->session->set('notice', array('message'=>'Due date must not be past','type'=>'error'));
							url::redirect('reminders/all');
						}
					}
					
					@$interval_value = $_POST['interval_value'];
					@$interval_type = $_POST['interval_type'];
					@$days_to = $_POST['days_to'];
					@$is_personal = $_POST['is_personal'];
					
					if(isset($_POST['bus_number'])){
						foreach($_POST['bus_number'] as $b_number){
							Reminder_Model::add_reminder($this->agency_id, $b_number, $name, $description, $days_to, @$start_date, @$due_date, $interval_value, $interval_type, $is_personal, $this->admin->username, $amount);	
						}
					}else{
						//In case of no bus number
						$bus_number='';
						Reminder_Model::add_reminder($this->agency_id, $bus_number, $name, $description, $days_to, @$start_date, @$due_date, $interval_value, $interval_type, $is_personal, $this->admin->username, $amount);	
					
					}
					
					
					
					$this->session->set('notice', array('message'=>'Reminder added','type'=>'success'));
				}
				else
				{
					$errors=$post->errors('errors');
					$notice="";
					foreach($errors as $error) 
					{
						$notice.=$error."<br />";
					}
					$this->session->set('notice', array('message'=>$notice,'type'=>'error'));
				}
					
					url::redirect('reminders/all');
			}
		}
		
		$view = new View('reminders');
		$view->all_reminders = $all_reminders;
		$view->all_buses = $all_buses;
		$this->template->content = $view;
	}
	
	public function edit_reminder($reminder_id)
	{	
		Authlite::check_admin();
		Authlite::verify_referer();
		
		$this->template->title = Kohana::lang('backend.settings');
		$this->template->title='Reminders';
		$all_buses = Bus_Model::get_all($this->agency_id);
		$reminder = Reminder_Model::get_reminder($this->agency_id,$reminder_id);
		
			
		if($_POST)
		{		
			$post = new Validation($_POST);
			//$post->add_rules('company_name','required');
			
			if ($post->validate())
			{
				$name = $_POST['name'];
				$description = $_POST['description'];
				$due_date = date("Y-m-d",strtotime($_POST['due_date']));
				$bus_number = $_POST['bus_number'];
				$days_to = $_POST['days_to'];
				$is_personal = $_POST['is_personal'];
				
				Reminder_Model::edit_reminder($reminder_id, $this->agency_id, $bus_number, $name, $description, $days_to, $due_date, $is_personal, $this->admin->username);	
				$this->session->set('notice', array('message'=>'Reminder modified','type'=>'success'));
			}
			else
			{
				$errors=$post->errors('errors');
				$notice="";
				foreach($errors as $error) 
				{
					$notice.=$error."<br />";
				}
				$this->session->set('notice', array('message'=>$notice,'type'=>'error'));
			}
				
				url::redirect('reminders/all');
		}
		
		$view = new View('edit_reminder');
		$view->reminder = $reminder;
		$view->all_buses = $all_buses;
		$this->template->content = $view;
	}
	
	public function view_reminder($reminder_id)
	{	
		Authlite::check_admin();
		Authlite::verify_referer();
		
		$this->template->title = Kohana::lang('backend.settings');
		$this->template->title='Reminders';
		$all_buses = Bus_Model::get_all($this->agency_id);
		$reminder = Reminder_Model::get_reminder($this->agency_id,$reminder_id);
		$reminder_schedules = Reminder_Schedule_Model::get_schedules_for_reminder($this->agency_id,$reminder_id);
		
		$view = new View('view_reminder');
		$view->reminder = $reminder;
		$view->reminder_schedules = $reminder_schedules;
		$view->all_buses = $all_buses;
		$this->template->content = $view;
	}
	
	
	public function delete_reminder($reminder_id)
	{		
			Authlite::check_admin();
			Authlite::verify_referer();
			
			$notice = Reminder_Model::delete_reminder($this->agency_id,$reminder_id);		
			$this->session->set('notice', array('message'=>$notice,'type'=>'success'));
			
			$this->template->content='';
			url::redirect('reminders/all');
	}	
	
	public function complete_reminder_schedule($reminder_schedule_id)
	{		
			Authlite::check_admin();
			Authlite::verify_referer();
			
			$notice = Reminder_Schedule_Model::complete_reminder_schedule($this->agency_id,$reminder_schedule_id);		
			$this->session->set('notice', array('message'=>$notice,'type'=>'success'));
			
			$this->template->content='';
			url::redirect('reminders/all');
	}
	//Complete reminder schedule and add the expense
	public function complete_reminder_schedule_add_expense($reminder_schedule_id)
	{		
		Authlite::check_admin();
		Authlite::verify_referer();
		$reminder_schedule = Reminder_Schedule_Model::get_reminder_schedule($this->agency_id,$reminder_schedule_id);
		
		//Complete the reminder
		$notice = Reminder_Schedule_Model::complete_reminder_schedule($this->agency_id,$reminder_schedule_id);	
		$reminder = Reminder_Model::get_reminder($this->agency_id,$reminder_schedule->ReminderID);
		
		//Add the expense
		Expense_Model::register_expense($this->agency_id, $reminder->BusNumber, $reminder->Amount, $reminder->Name, date("Y-m-d"), $this->admin->username, '', $schedule_id='');
		
		$this->session->set('notice', array('message'=>'Reminder and expense added','type'=>'success'));
		
		$this->template->content='';
		url::redirect('reminders/all');
	}
	
	public function add_admin()
	{
		Authlite::check_admin();
		Authlite::verify_referer();
		
		//Authlite::check_integrity($schedule_id);
		$this->template->title='Add admin details';
		$admin=ORM::FACTORY('admin');
		$groups = get::all_admingroups();
			
		if($_POST)
		{
			$post = new Validation($_POST);
			$post->add_rules('username','required','valid::alpha_numeric');
			$post->add_rules('password','required');
			
			if ($post->validate())
			{
				
				$username = $_POST['username'];
				$password = $_POST['password'];
				$AllowBusManagement = $_POST['AllowBusManagement'];
				$AllowParcelManagement = $_POST['AllowParcelManagement'];
				$AllowExpenseManagement = $_POST['AllowExpenseManagement'];
				$AllowScheduleManagement = $_POST['AllowScheduleManagement'];
			
				$user_exists = ORM::FACTORY('admin')->where('username',$username)->count_all();
				
				if ($user_exists){
					$this->session->set('notice', array('message'=>Kohana::lang('backend.username_taken'),'type'=>'error'));
					url::redirect('settings/add_admin');
				}else
				{
					//Make sure that at least one privilege is set for each created user
					if($AllowBusManagement==0 AND $AllowParcelManagement ==0 AND $AllowExpenseManagement==0 AND $AllowScheduleManagement==0)
					{
					$this->session->set('notice', array('message'=>'Admin has no privilege. Please select at least one.','type'=>'error'));	
					url::redirect('settings/add_admin');
					}else
					{
					// Admin group id is 2 by default. ie. BM CAN only create a regular admin
					$notice = Admin_Model::safe_add_admin($username, $password, $this->agency_id, $this->admin->username, $AllowParcelManagement,$AllowScheduleManagement,$AllowExpenseManagement, $AllowBusManagement);
					$this->session->set('notice', array('message'=>$notice,'type'=>'success'));			
					}
				}
					
					//$this->session->set('notice', array('message'=>'Admin information modified','type'=>'success'));
				}
				else
				{
					$errors=$post->errors('errors');
					$notice="";
						foreach($errors as $error) 
						{
							$notice.=$error."<br />";
						}
						$this->session->set('notice', array('message'=>$notice,'type'=>'error'));
				}
			
				url::redirect('settings');
			}
		
			
			$view=new View('control_add_admin');
			$view->admin=$admin;
			//$view->groups = $groups;
			//$view->logo = $logo;
			$this->template->content=$view;
			
	}
	
	public function add_fixed_expense()
	 {
		Authlite::check_admin();
		Authlite::verify_referer();
		$this->template->title='Add fixed expenses';
		
		if($_POST)
		{
			if (isset($_POST['expense_is_percent']))
			{
				
				$post = new Validation($_POST);
				$post->add_rules('expense_name','required');
				$post->add_rules('expense_value','required');
				
				if ($post->validate())
				{	
					$expense_name=$_POST['expense_name'];
					$expense_value=$_POST['expense_value'];
					
					$expense_exists = ORM::FACTORY('fixed_expense_detail')->where('name',$expense_name)->count_all();
					if ($expense_exists)
					{
						$this->session->set('notice', array('message'=>Kohana::lang('backend.expense_exists'),'type'=>'error'));
						url::redirect('settings/add_fixed_expense');
					}else
					{	//if it was a percentage
							Fixed_Expense_Detail_Model::add_fixed_expense($this->agency_id, $expense_name, $expense_value, $expense_is_percent=1, $expense_is_amount=0);
							$this->session->set('notice', array('message'=>'Fixed expense added','type'=>'success'));
					}
				}
				else
				{
					$errors=$post->errors('errors');
					$notice="";
						foreach($errors as $error) 
						{
							$notice.=$error."<br />";
						}
						$this->session->set('notice', array('message'=>$notice,'type'=>'error'));
				}
				url::redirect('settings');
			}
			elseif (isset($_POST['expense_is_amount']))
			{
				$post = new Validation($_POST);
				$post->add_rules('expense_name2','required');
				$post->add_rules('expense_value2','required');
				
				if ($post->validate())
				{	
					$expense_name2=$_POST['expense_name2'];
					$expense_value2=$_POST['expense_value2'];
					
					$expense_exists = ORM::FACTORY('fixed_expense_detail')->where('name',$expense_name2)->count_all();
					if ($expense_exists)
					{
						$this->session->set('notice', array('message'=>Kohana::lang('backend.expense_exists'),'type'=>'error'));
						url::redirect('settings/add_fixed_expense');
					}else
					{	//if it was a percentage
							Fixed_Expense_Detail_Model::add_fixed_expense($this->agency_id, $expense_name2, $expense_value2, $expense_is_percent=0, $expense_is_amount=1);
							$this->session->set('notice', array('message'=>'Fixed expense added','type'=>'success'));
					}
				}
				else
				{
					$errors=$post->errors('errors');
					$notice="";
						foreach($errors as $error) 
						{
							$notice.=$error."<br />";
						}
						$this->session->set('notice', array('message'=>$notice,'type'=>'error'));
				}
				url::redirect('settings');
			}
			
	}
		$view=new View('add_fixed_expense');
		$this->template->content=$view;
	}
	
	public function delete_fixed_expense($expense_id)
	 {		
			Authlite::check_admin();
			Authlite::verify_referer();
			
			$this->template->title="Delete fixed expense";
			
			$notice = Fixed_Expense_Detail_Model::delete_expense($this->agency_id,$expense_id);		
			
			$this->session->set('notice', array('message'=>$notice,'type'=>'success'));
			url::redirect('settings');
			
			$this->template->content='';
	 }
	
	function view_fixed_expense($expense_id)
	{
			Authlite::check_admin();
			Authlite::verify_referer();
			
			//Authlite::check_integrity($schedule_id);
			$this->template->title='Edit admin details';
			$fixed_expense=Fixed_Expense_Detail_Model::get_expense($this->agency_id,$expense_id);
			

		if($_POST)
		{
			if (isset($_POST['expense_is_percent']))
			{
				
				$post = new Validation($_POST);
				$post->add_rules('expense_name','required');
				$post->add_rules('expense_value','required');
				
				if ($post->validate())
				{	
					$expense_name=$_POST['expense_name'];
					$expense_value=$_POST['expense_value'];
					
						//if it was a percentage
							Fixed_Expense_Detail_Model::edit_fixed_expense($this->agency_id, $expense_name, $expense_value, $expense_is_percent=1, $expense_is_amount=0, $expense_id);
							$this->session->set('notice', array('message'=>'Fixed expense added','type'=>'success'));
					
				}
				else
				{
					$errors=$post->errors('errors');
					$notice="";
						foreach($errors as $error) 
						{
							$notice.=$error."<br />";
						}
						$this->session->set('notice', array('message'=>$notice,'type'=>'error'));
				}
				url::redirect('settings');
			}
			elseif (isset($_POST['expense_is_amount']))
			{
				$post = new Validation($_POST);
				$post->add_rules('expense_name2','required');
				$post->add_rules('expense_value2','required');
				
				if ($post->validate())
				{	
					$expense_name2=$_POST['expense_name2'];
					$expense_value2=$_POST['expense_value2'];
					
						//if it was a percentage
							Fixed_Expense_Detail_Model::edit_fixed_expense($this->agency_id, $expense_name2, $expense_value2, $expense_is_percent=0, $expense_is_amount=1, $expense_id);
							$this->session->set('notice', array('message'=>Kohana::lang('backend.setting_saved'),'type'=>'success'));
					
				}
				else
				{
					$errors=$post->errors('errors');
					$notice="";
						foreach($errors as $error) 
						{
							$notice.=$error."<br />";
						}
						$this->session->set('notice', array('message'=>$notice,'type'=>'error'));
				}
				url::redirect('settings');
			}
	}	
			$view=new View('view_fixed_expense');
			$view->fixed_expense=$fixed_expense;
			//$view->logo = $logo;
			$this->template->content=$view;
			
	}
	function edit_admin($admin_id)
	{
			Authlite::check_admin();
			Authlite::verify_referer();
			
			//Authlite::check_integrity($schedule_id);
			$this->template->title='Edit admin details';
			$admin=get::admin($admin_id);
			

		if($_POST)
		{
			
			
			if(isset($_POST['change_privileges']))
			{
			$post = new Validation($_POST);
			if ($post->validate())	
				{
					$AllowScheduleManagement = $_POST['AllowScheduleManagement'];
					$AllowBusManagement = $_POST['AllowBusManagement'];
					$AllowParcelManagement = $_POST['AllowParcelManagement'];
					$AllowExpenseManagement = $_POST['AllowExpenseManagement'];
					
					//Make sure that at least one privilege is set for each created user
						if($AllowBusManagement==0 AND $AllowParcelManagement ==0 AND $AllowExpenseManagement==0 AND $AllowScheduleManagement==0)
						{
						$this->session->set('notice', array('message'=>'Admin has no privilege. Please select at least one.','type'=>'error'));	
						url::redirect('settings/edit_admin/'.$admin_id);
						}else{
					
					$admin->AllowScheduleManagement = $AllowScheduleManagement;
					$admin->AllowBusManagement = $AllowBusManagement;
					$admin->AllowParcelManagement = $AllowParcelManagement;
					$admin->AllowExpenseManagement = $AllowExpenseManagement;
					$admin->save();	
					
					$this->session->set('notice', array('message'=>'Admin information modified','type'=>'success'));}
				}
				else
				{
					$errors=$post->errors('errors');
					$notice="";
						foreach($errors as $error) 
						{
							$notice.=$error."<br />";
						}
						$this->session->set('notice', array('message'=>$notice,'type'=>'error'));
				}		
			}
			if(isset($_POST['change_password']))
			{	 
				$post = new Validation($_POST);
				$post->add_rules('new_password','required');
				if ($post->validate())	
				{	
					$new_password = $_POST['new_password'];
					$notice = Admin_model::change_password($admin_id,$new_password);
					$this->session->set('notice', array('message'=>$notice,'type'=>'success'));		
				}else
				{
					$errors=$post->errors('errors');
					$notice="";
						foreach($errors as $error) 
						{
							$notice.=$error."<br />";
						}
						$this->session->set('notice', array('message'=>$notice,'type'=>'error'));
				}
			}
			
			url::redirect('settings');
			}	
			$view=new View('control_edit_admin');
			$view->admin=$admin;
			//$view->logo = $logo;
			$this->template->content=$view;
			
	}
	public function delete_admin($admin_id)
	{		
			Authlite::check_admin();
			Authlite::verify_referer();
			
			$this->template->title="Delete admin";
			$deleted_by = $this->admin->username;
			$notice = Admin_Model::safe_delete_admin($admin_id,$deleted_by);		
			
			$this->session->set('notice', array('message'=>$notice,'type'=>'success'));
			url::redirect('settings');
			
			$this->template->content='';
	}
	
	public function add_bus_requirement()
	{
		Authlite::check_admin();
		Authlite::verify_referer();
		
		//Authlite::check_integrity($schedule_id);
		$this->template->title='Add bus requirment';
			
		if($_POST)
		{
			$post = new Validation($_POST);
			$post->add_rules('req_name','required');
			
			if ($post->validate())
			{
				
				$req_name = $_POST['req_name'];
			
				$requirement_exists = ORM::FACTORY('bus_requirement')->where('Name',$req_name)->count_all();
				
				if ($requirement_exists)
				{
					$this->session->set('notice', array('message'=>Kohana::lang('backend.requirement_exists'),'type'=>'error'));
					url::redirect('settings');
				}else
				{
					Bus_Requirement_Model::add_requirement($this->agency_id, $req_name);
				}
					
					//$this->session->set('notice', array('message'=>'Admin information modified','type'=>'success'));
				}
				else
				{
					$errors=$post->errors('errors');
					$notice="";
						foreach($errors as $error) 
						{
							$notice.=$error."<br />";
						}
						$this->session->set('notice', array('message'=>$notice,'type'=>'error'));
				}
			
				url::redirect('settings');
			}
		
			
			$view=new View('add_bus_requirement');
			
			$this->template->content=$view;
			
	}
	
	public function delete_bus_requirement($requirement_id)
	{		
			Authlite::check_admin();
			Authlite::verify_referer();
			
			//$this->template->title="Delete fixed expense";
			
			$notice = Bus_Requirement_Model::delete_requirement($this->agency_id,$requirement_id);		
			
			$this->session->set('notice', array('message'=>$notice,'type'=>'success'));
			url::redirect('settings');
			
			$this->template->content='';
	}
	
		
		}