<?php defined('SYSPATH') or die('No direct script access'); ?>
	
		
		
		<div class="row-fluid  span12 hero-unit prof-unit reload" style="margin:5px 0 0px 0;">	
		
			
			
			<div class="no-print">
			
			<ul class="nav nav-tabs">
					<li > <a href="<?=url::site('reports')?>"><i class="icon-arrow-left"></i> <?=Kohana::lang('backend.back_routes')?></a></li>
					<button class="view" onClick="window.print()"><i class="icon-print"></i> <?=Kohana::lang('backend.print')?></button>
			</ul>
			</div>
			<div class="  print-area" >
								
							
				<legend>
					<div class="print-head">
						<?php $admin=Authlite::instance()->get_admin();?>
						<?=strtoupper(get::_parent($admin->agency_id)->name);?><br/>
						<?=get::agency_name($admin->agency_id);?>
					</div>
					Report: All admins - Income and expenses ranking	<br/>
					Start date: <?=date("d-m-Y",strtotime($start_date))?> &nbsp;&nbsp;&nbsp;&nbsp; 
					End date: <?=date("d-m-Y",strtotime($end_date))?>&nbsp;&nbsp;&nbsp;&nbsp;
				</legend>
				<div class="span4" style="margin:0 0 0 0;">
				<div class="heading">Expenses [High to low]</div>
					<table class="table table-striped table-condensed">
						<tr><th><?=Kohana::lang('backend.destination')?></th><th class="text-right"><?=Kohana::lang('backend.amount')?></th></tr>
						<?php 
						$overall_expenses =0;
						foreach ($admins_and_expenses as $admin_name => $expenses)
						{//var_dump($details);exit;?>
						<tr>
							<td><?=$admin_name?></td>
							<td class="text-right"><?=number_format($expenses)?></td>
						</tr>
						<?php $overall_expenses += $expenses; }?>
					</table>
				</div>
				<div class="span4">
				<div class="heading">Income [High to low]</div>
					<table class="table table-striped table-condensed">
						<tr><th><?=Kohana::lang('backend.destination')?></th><th class="text-right"><?=Kohana::lang('backend.amount')?></th></tr>
						<?php 
						$overall_income =0;
						foreach ($admins_and_income as $admin_name => $income)
						{?>
						<tr>
							<td><?=$admin_name?></td>
							<td class="text-right"><?=number_format($income)?></td>
						</tr>
						<?php $overall_income += $income; }?>
					</table>
				</div>
				
				<div class="span4">
				<div class="heading">Net [High to low]</div>
					<table class="table table-striped table-condensed">
						<tr><th><?=Kohana::lang('backend.destination')?></th>
						<th class="text-right"><?=Kohana::lang('backend.amount')?></th></tr>
						<?php 
						$overall_net =0;
						foreach ($admins_and_net as $admin_name => $net)
						{?>
						<tr>
							<td><?=$admin_name?></td>
							<td class="text-right"><?=number_format($net)?></td>
						</tr>
						<?php $overall_net += $net;}?>
					</table>
				</div>
					
					
				
				<div class="clear">&nbsp;<br/><br/><br/><br/><br/>
				</div>
				
				<!--NET SUMMARY TABLE-->
				<table class="table table-condensed table-striped ">
					<tr>
						<th class="span2">GRAND TOTAL</th>
						<td>&nbsp;</td>
						<th class="text-right" >EXPENSES</th>
						<th class="text-right">INCOME</th>
					</tr>
					
					<tr>		
						<td></td>
						<th></th>
						<th class="span2 text-right"><?php echo number_format($overall_expenses)?></th>
						<th class="span2 text-right"><?php echo number_format($overall_income)?></th>
					</tr>
					<tr>		
						<td></td>
						<th></th>
						<th class="span2 text-right">NET</th>
						<th class="span2 text-right"><?php echo number_format($overall_net)?> </th>
					</tr>
				</table>
					
				<div>Printed by: <?=STRTOUPPER($this->admin->username);?></div> 
				<div>Printed on: <?=date('d-m-Y h:i:s A')?></div>
				<!--END OF NET SUMMARY-->
				
			</div>

		
		
		
		
		
		
		</div>
