<?php defined('SYSPATH') or die('No direct script access'); ?> 
	
 
	<div class="row-fluid " >
	  
						
	  	<div class="span12 hero-unit prof-unit" >
				<ul class="nav nav-tabs">
					<li > <a href="<?=url::site('buses/all')?>"><i class="icon-arrow-left"></i> <?=Kohana::lang('backend.back_buses')?></a></li>
				</ul>
		
				<form action="<?=url::site('buses/edit_bus/'.$bus->bus_number)?>" method="POST" enctype="multipart/form-data" class="span8 offset2">
				<legend><?=Kohana::lang('backend.edit_bus')?> <?= $bus->bus_number?></legend>
					
						
						<div class="">
							<div class="span4 text-right"><?=Kohana::lang('backend.bus_no')?><span class="red"> *</span>: </div>
							<div class="span8"><input type="text"  name="bus_number" value="<?=$bus->bus_number?>" disabled /></div>
						</div>
						
						<div class="">
							<div class="span4 text-right"><?=Kohana::lang('backend.serial_no')?>: </div>
							<div class="span8"><input type="text"  name="serial_number" value="<?=$bus->serial_number?>"></div>
						</div>
						
						<div class="">
							<div class="span4 text-right"><?=Kohana::lang('backend.bus_name')?>: </div>
							<div class="span8"><input type="text"  name="bus_name" value="<?=$bus->bus_name?>" ></div>
						</div>
						
						<div class="">
							<div class="span4 text-right"><?=Kohana::lang('backend.number_seats')?><span class="red"> *</span>: </div>
								<div class="span8">
									<select name="bus_seats" id="inputType" >

										<option value="<?=$bus->bus_seats?>"><?=$bus->bus_seats?> <?=Kohana::lang('backend.seater')?></option>
										<?php for($i=30;$i<=80;$i++){ ?>
											<option value="<?=$i?>"><?=$i?> <?=Kohana::lang('backend.seater')?></option>
										<?php } ?>
										

									</select>
								</div>
						</div>
						
						<div class="">
							<div class="span4 text-right"><?=Kohana::lang('backend.drivers_name')?>: </div>
							<div class="span8"><input type="text"  name="driver" value="<?=$bus->driver?>"></div>
						</div>
						
						<div class="">
							<div class="span4 text-right"><?=Kohana::lang('backend.motorboys_name')?>: </div>
							<div class="span8"><input type="text"  name="motorboy" value="<?=$bus->motorboy?>" ></div>
						</div>
						
						<div class="">
							<div class="span4 text-right"><?=Kohana::lang('backend.other_information')?>: </div>
							<div class="span8"><textarea placeholder=""  name="other_infos" style="height:100px;"><?=@$bus->other_infos?></textarea></div>
						</div>
						
						<div class="">
							<div class="span4 text-right"><?=Kohana::lang('backend.vip_bus')?>: </div>
							<div class="span8">
								<input type="hidden" name="vip" value="0" />
								<label class="checkbox"><input type="checkbox" name="vip" value="1" <?php if($bus->vip == 1){echo 'checked = "checked"';} ?>/></label>
								
							</div>
						</div>
						
						<div class="">
								<div class="span4 text-right" >Photo:</div>
								<div class="span8">
											<img src="<?php echo url::base(). $current_photo;?>" class="img-polaroid">
								</div>
						</div>
							 
							  
					  <div>
						<label class="span4" >&nbsp;</label>
						<div class="span8">
									<input type="file"  name="bus_photo" />
						</div>
					  </div>
									
					
						<div class="clear"></div>
						<div class="clear"><br/><br/></div>
						
						<div class="">
							<div class="span4"> </div>
							<div class="form-actions">
								<button class="btn btn-info" type="submit"> <?=Kohana::lang('backend.save')?></button>
								<a href="<?=url::site('buses/all')?>" class="btn" type="submit"> <?=Kohana::lang('backend.cancel')?></a>
								<a href="<?=url::site('buses/delete_bus/'.$bus->bus_number)?>"  onClick="return confirm('<?=Kohana::lang('backend.confirm_delete')?>');" type="submit"> <?=Kohana::lang('backend.delete')?></a>
							</div>
						</div>
						
									
				
						
				</form>
			 
			
				
			  					
			
		
			  
		</div>
		
		</div>
	  <div class="clear"></div>

<script type="text/javascript">
	$(document).ready(function(){
			$('#timepicker').timepicker({
				minuteStep:30,
				//defaultTime: '8:00 PM'
			});
			
			
			$('.input-append.date').datepicker({
				format: "dd-mm-yyyy",
				todayBtn: "linked",
				autoclose: true,
				todayHighlight: true
			});
			
			
				$(function () {
				$('#myTab a').click(function (e) {
					  e.preventDefault();
					  $(this).tab('show');  
			})
			  })
			  
		
			  
			
	})
	</script>
